// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const webpackConfig = require('@leanup/stack-react/webpack.config');

const lessLoader = {
  loader: 'less-loader',
  options: {
    lessOptions: {
      javascriptEnabled: true,
    },
  },
};

const createWebpackConfigForSubmodules = (env, argv) => {
  const conf = webpackConfig(env, argv);
  conf.module.rules.push({
    test: /\.theme\.(less|css)$/i,
    use: [
      {
        loader: 'style-loader',
        options: { injectType: 'lazyStyleTag' },
      },
      'css-loader',
      lessLoader,
    ],
  });

  return conf;
};

module.exports = createWebpackConfigForSubmodules;
