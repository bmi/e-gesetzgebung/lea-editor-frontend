// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const leanupNycConfig = require('@leanup/stack/nyc.config');
leanupNycConfig.reporter.push('text-summary');
leanupNycConfig.exclude.push('src/main.ts', 'src/react.main.tsx');

module.exports = {
  extends: "@istanbuljs/nyc-config-typescript",
  ...leanupNycConfig,
  sourceMap: true
}