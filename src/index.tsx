// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { createRoot } from 'react-dom/client';

//
import './i18n';
import { Application } from './components/App';
//
import { ConfigProvider } from 'antd';
import deDE from 'antd/lib/locale/de_DE';
import { plategThemeConfig } from '@plateg/theme';
import { StoreProvider, store } from '@plateg/theme/src/components/store';
import { StoreInit } from '@plateg/theme/src/components/storeInit/component.react';
import { KeycloakAuth } from '@plateg/theme/src/components/auth/KeycloakAuth';

const root = createRoot(document.getElementById('root') as HTMLElement);
root.render(
  // <React.StrictMode>
  <ConfigProvider theme={plategThemeConfig} locale={deDE}>
    <StoreProvider store={store}>
      <KeycloakAuth forceLogin>
        <StoreInit>
          <Application isStandalone />
        </StoreInit>
      </KeycloakAuth>
    </StoreProvider>
  </ConfigProvider>,
  // </React.StrictMode>
);
