// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import { EditableQueries as Queries } from '../../plugin-core/utils/EditableQueries';
import { AuthorialNoteCommands } from '../authorialNote/AuthorialNoteCommands';

import { Plugin, PluginProps } from '../../plugin-core';
import { getPlainTextOfSelection } from './utils';
import { SearchReplaceEditor } from '../searchAndReplace/SearchReplaceEditor';

export class CopyAndPastePlugin extends Plugin {
  public static CopyAndPastePlugin: any;

  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T {
    return e;
  }

  private renumberAuthorialNotes(editor: Editor) {
    setTimeout(() => {
      AuthorialNoteCommands.renumberAuthorialNotes(editor);
    });
  }

  public copy = (editor: SearchReplaceEditor | null) => {
    const copiedContent = getPlainTextOfSelection(editor, this.isDynAenderungsvergleich, false);
    copiedContent && void navigator.clipboard.writeText(copiedContent);
  };

  public paste = (editor: Editor, event: ClipboardEvent) => {
    event.preventDefault();
    event.stopPropagation();

    if (
      this.editorDocument.isReadonly ||
      !editor.selection ||
      (this.editorDocument && !Queries.isEditable(editor, this.editorDocument.documentType, this.isAuthorialNotePlugin))
    )
      return;
    const text = event.clipboardData?.getData('text/plain').replace(/(\r)/g, '');
    if (!this.isAuthorialNotePlugin) {
      editor.insertText(text ?? '');
      this.renumberAuthorialNotes(editor);
    } else {
      const selectionNull: boolean = editor.selection.anchor === editor.selection.focus;
      if (selectionNull) {
        editor.insertText(text ?? '');
      } else {
        //If the selection spans a range in the authorial note page, the new text can not be immediately inserted
        editor.insertText('');
        setTimeout(() => {
          editor.insertText(text ?? '');
        }, 100);
      }
    }
  };

  public cut = (editor: Editor, event: ClipboardEvent) => {
    event.preventDefault();
    event.stopPropagation();

    if (
      this.editorDocument.isReadonly ||
      !editor.selection ||
      (this.editorDocument && !Queries.isEditable(editor, this.editorDocument.documentType, this.isAuthorialNotePlugin))
    ) {
      void navigator.clipboard.writeText('');
      return;
    }

    const copiedContent = getPlainTextOfSelection(editor, this.isDynAenderungsvergleich, true);
    copiedContent && void navigator.clipboard.writeText(copiedContent);
    editor.insertText('');
    if (!this.isAuthorialNotePlugin) this.renumberAuthorialNotes(editor);
  };
}
