// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { BaseEditor, BasePoint, NodeEntry, Range } from 'slate';
import { SearchReplaceEditor } from '../searchAndReplace/SearchReplaceEditor';
import { getNodesOfTypeInRange } from '../../plugin-core/utils';
import { Element as LeaElement } from '../../../interfaces';

const Serialize = (element: Element): string => {
  const elementChildren = element.children;
  const elementChildrenArray = Array.from(elementChildren);
  const elementClass = element.classList[0];
  const isTextNode =
    !elementChildrenArray.length ||
    elementClass === STRUCTURE_ELEMENTS.TEXTWRAPPER ||
    elementClass === STRUCTURE_ELEMENTS.AUTHORIALNOTE ||
    element.tagName === 'span';
  const hasTextNode = elementChildrenArray.some((child) => {
    const childClass = child.classList[0];
    return childClass === STRUCTURE_ELEMENTS.TEXTWRAPPER || childClass === STRUCTURE_ELEMENTS.AUTHORIALNOTE;
  });
  if (hasTextNode || isTextNode) {
    return element.textContent ?? '';
  } else {
    return elementChildrenArray
      .map((child) => {
        return `${Serialize(child)}`;
      })
      .join(' ');
  }
};

export function getPlainTextOfSelection(
  editor: SearchReplaceEditor | BaseEditor | null,
  isDynAenderungsvergleich: boolean,
  isCutPaste: boolean,
) {
  if (isDynAenderungsvergleich) {
    return plainTextDynAenderungsvergleich(editor, isCutPaste);
  } else {
    let range;
    if (window.getSelection) {
      const selection = window.getSelection();
      if (selection?.rangeCount) {
        range = selection.getRangeAt(0);
        const clonedSelection = range.cloneContents();
        const div = document.createElement('div');
        div.appendChild(clonedSelection);
        const serializedDiv = Serialize(div);
        return serializedDiv;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}

function plainTextDynAenderungsvergleich(editor: SearchReplaceEditor | BaseEditor | null, isCutPaste: boolean) {
  if (editor && editor.selection) {
    const selectionStart = Range.start(editor.selection);
    const selectionEnd = Range.end(editor.selection);
    const nodesInRange = getNodesOfTypeInRange(
      editor,
      [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
      [selectionStart.path, selectionEnd.path],
    );

    if (nodesInRange.length <= 0) return;
    const wrappersTextToBeCopied: Array<string> = [];
    for (let index = 0; index <= nodesInRange?.length - 1; index++) {
      //check nodes between starting and ending node
      getTextofNodeInRange(nodesInRange, index, wrappersTextToBeCopied, selectionStart, selectionEnd);
    }

    let concatenatedText: string = '';
    wrappersTextToBeCopied.forEach((text: string) => {
      if (text?.trim().length > 0) {
        concatenatedText += ` ${text}`;
      }
    });
    if (isCutPaste) {
      editor.deleteFragment();
    }

    return concatenatedText.trim();
  }
  return '';
}

function getTextofNodeInRange(
  nodesInRange: NodeEntry<LeaElement>[],
  index: number,
  wrappersTextToBeCopied: Array<string>,
  selectionStart: BasePoint,
  selectionEnd: BasePoint,
) {
  if (nodesInRange[index][0]['lea:changeType'] !== 'ct_deleted') {
    let text = nodesInRange[index][0].children[0].text;
    if (!text) return;
    if (index == 0 || index === nodesInRange.length - 1) {
      const offsetStartIndex = index === 0 ? selectionStart.offset : 0;
      const offsetEndIndex =
        index === 0 && selectionStart.path.toString() !== selectionEnd.path.toString()
          ? text?.length - 1
          : selectionEnd.offset;
      text = text.substring(offsetStartIndex, offsetEndIndex);
    }
    wrappersTextToBeCopied.push(text || '');
  }
}
