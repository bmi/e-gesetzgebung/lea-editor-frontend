// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  CombinedPluginRenderProps,
  Plugin,
  PluginProps,
  useSatzZaehlungState,
  useSatzzahlungDispatch,
} from '../../plugin-core';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import React, { useEffect, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { Checkbox } from 'antd';
import { ItemType } from 'antd/es/menu/interface';

import { Element } from '../../../interfaces';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { HistoryEditor } from 'slate-history';

export class SatzZaehlungPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    return editor;
  }

  public useEffect(editor: ReactEditor & HistoryEditor): void {
    const [firstSupNode] = Editor.nodes(editor, {
      match: (node) => (node as Element).type === STRUCTURE_ELEMENTS.SUP,
      at: [0],
    });
    if (firstSupNode) {
      this.documentHasSatzZaehlung = true;
    }
  }

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private satzZahlungVisible = true;
  private documentHasSatzZaehlung = false;

  public ViewTools = (): ReactElement | null => {
    const { t } = useTranslation();
    const satzZahlungVisible = useSatzZaehlungState();
    const { toggleSatzZaehlungVisible } = useSatzzahlungDispatch();
    useEffect(() => {
      this.satzZahlungVisible = satzZahlungVisible;
    }, [satzZahlungVisible]);

    if (this.documentHasSatzZaehlung) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox className="check-all" checked={satzZahlungVisible} onClick={toggleSatzZaehlungVisible}>
              {t('lea.toolbar.labels.satzZaehlung.title')}
            </Checkbox>
          ),
          key: 'viewTools-satz-zaehlung',
          title: t('lea.toolbar.labels.satzZaehlung.title'),
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const element = props.element as Element;
    const satzZaehlungVisible = useSatzZaehlungState();

    if (element.type === STRUCTURE_ELEMENTS.SUP) {
      return (
        <div className={`${element.type}`} {...props.attributes}>
          {' '}
          {satzZaehlungVisible ? props.children : <></>}
        </div>
      );
    }
    return null;
  };
}
