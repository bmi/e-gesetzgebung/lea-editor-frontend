// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useRef, useState } from 'react';
import { BaseOperation, BaseRange, Editor, SelectionOperation, Transforms } from 'slate';
import { ReactEditor } from 'slate-react';
import { Plugin, PluginProps, IPluginToolbarProps, useEditorOnFocusState } from '../../plugin-core';
import { History, HistoryEditor } from 'slate-history';
import { UndoIcon, RedoIcon, IconButton } from '@lea/ui';
import './UndoRedo.less';
import { isRedoEvent, isUndoEvent } from '../../plugin-core/utils';

export class UndoRedoPlugin extends Plugin {
  public constructor(
    props: PluginProps,
    private history?: History,
  ) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    return e as T & ReactEditor;
  }

  public keyDown(historyEditor: HistoryEditor, event: KeyboardEvent) {
    if (isUndoEvent(event)) {
      event.preventDefault();
      event.stopPropagation();
      this.handleUndo(historyEditor);
    } else if (isRedoEvent(event)) {
      event.preventDefault();
      event.stopPropagation();
      this.handleRedo(historyEditor);
    }
    return false;
  }

  private handleUndo(historyEditor: HistoryEditor) {
    const undos = historyEditor.history.undos;
    if (undos.length === 0) return;
    const lastUndo = undos[undos.length - 1];
    const lastSelection = lastUndo[-1] as SelectionOperation;
    undos[undos.length - 1] = lastUndo.filter((r) => r.type !== 'set_selection');
    historyEditor.undo();
    if (lastSelection.properties) {
      HistoryEditor.withoutSaving(historyEditor, () =>
        Transforms.setSelection(historyEditor, lastSelection.properties),
      );
    } else {
      Transforms.deselect(historyEditor);
    }
    const redos = historyEditor.history.redos;
    const lastRedo = redos[redos.length - 1];
    lastRedo[-1] = lastSelection;
  }

  private handleRedo(historyEditor: HistoryEditor) {
    const redos = historyEditor.history.redos;
    if (redos.length === 0) return;
    const lastRedo = redos[redos.length - 1];
    const lastSelection = lastRedo[-1] as SelectionOperation;
    redos[redos.length - 1] = redos[redos.length - 1].filter((r) => r.type !== 'set_selection');
    historyEditor.redo();
    if (lastSelection.newProperties) {
      HistoryEditor.withoutSaving(historyEditor, () =>
        Transforms.setSelection(historyEditor, lastSelection.newProperties),
      );
    } else {
      Transforms.deselect(historyEditor);
    }
    const undos = historyEditor.history.undos;
    const lastUndo = undos[undos.length - 1];
    lastUndo[-1] = lastSelection;
  }

  public ToolbarTools = ({ editor }: IPluginToolbarProps) => {
    const historyEditor = editor as ReactEditor & HistoryEditor;
    const editorOnFocus = useEditorOnFocusState();
    const [canUndo, setCanUndo] = useState(false);
    const [canRedo, setCanRedo] = useState(false);
    const [hasUndoHover, setHasUndoHover] = useState(false);
    const [hasRedoHover, setHasRedoHover] = useState(false);
    const lastSelectionRef = useRef<BaseRange | null>(historyEditor.selection);
    const lastUndosLengthRef = useRef<number>(historyEditor.history.undos.length);
    const lastRedosLengthRef = useRef<number>(historyEditor.history.undos.length);

    useEffect(() => {
      const { onChange } = historyEditor;
      if (this.history && historyEditor.history.undos.length === 0) {
        historyEditor.history.undos = JSON.parse(JSON.stringify([...this.history.undos])) as BaseOperation[][];
        historyEditor.history.redos = JSON.parse(JSON.stringify([...this.history.redos])) as BaseOperation[][];
      }
      setCanUndo(historyEditor.history.undos.length !== 0);
      setCanRedo(historyEditor.history.redos.length !== 0);

      historyEditor.onChange = () => {
        onChange();
        const undos = historyEditor.history.undos;
        const redos = historyEditor.history.redos;
        const lastUndo = undos[undos.length - 1];
        if (undos.length === 1 && undos[0].every((operation) => operation.type === 'set_selection')) {
          undos.pop();
        }
        const isLatestChange = redos.length === 0;
        if (lastUndo && isLatestChange) {
          const isSelectionOperation = lastUndo[lastUndo.length - 1].type === 'set_selection';
          if (undos.length > lastUndosLengthRef.current) {
            // Die Selektion wird von slate nach einem undo bzw. redo per default nicht immer richtig gesetzt.
            // Als workaround wird deshalb die korrekte selektion nach einem undo bzw.
            // redo separat unter dem -1-ten index des Operation-Arrays gespeichert
            // Dabei ist "properties" die selektion nach einem undo und "newProperties" die Selektion nach einem redo.
            lastUndo[-1] = { ...lastUndo[-1], properties: lastSelectionRef.current } as SelectionOperation;
          }
          if (undos.length > lastUndosLengthRef.current || !isSelectionOperation) {
            lastUndo[-1] = { ...lastUndo[-1], newProperties: historyEditor.selection } as SelectionOperation;
          }
        }
        setCanUndo(undos.length !== 0);
        setCanRedo(redos.length !== 0);
        lastSelectionRef.current = historyEditor.selection;
        lastUndosLengthRef.current = undos.length;
        lastRedosLengthRef.current = redos.length;
      };
      return () => {
        historyEditor.onChange = onChange;
      };
    }, []);

    return editorOnFocus.index === this.editorDocument.index && !this.editorDocument.isReadonly ? (
      <>
        <IconButton
          id="undo-button"
          onHover={setHasUndoHover}
          onClick={() => this.handleUndo(historyEditor)}
          icon={<UndoIcon hasHover={hasUndoHover && canUndo} />}
          disabled={!canUndo}
        ></IconButton>
        <IconButton
          id="redo-button"
          onHover={setHasRedoHover}
          onClick={() => this.handleRedo(historyEditor)}
          icon={<RedoIcon hasHover={hasRedoHover && canRedo} />}
          disabled={!canRedo}
        ></IconButton>
      </>
    ) : null;
  };
}
