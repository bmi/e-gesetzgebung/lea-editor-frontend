// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseEditor, Path } from 'slate';
import { Element } from '../../../interfaces';
import { HdrAssistentPlugin, HdrFinding, findingCategory, findingType } from './HdrAssistentPlugin';
import { getChildrenOfTypeAt, isNextNeighborOfType, isPreviousNeighborOfType } from '../../plugin-core/utils';
import { REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY, REGELUNGSTEXT_STRUCTURE } from '../regelungstext';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { jumpToPath } from '../../Editor';
import { ReactEditor } from 'slate-react';
import { t } from 'i18next';

export class HdrAssistentCommands {
  public static isMoreThanOneOfType(
    editor: BaseEditor,
    gliederungsEbenenElement: Element,
    gliederungsEbenenElementPath: Path,
    parentType?: string,
  ): HdrFinding | undefined {
    const neighbor =
      isNextNeighborOfType(editor, gliederungsEbenenElement.type, gliederungsEbenenElementPath) ||
      isPreviousNeighborOfType(editor, gliederungsEbenenElement.type, gliederungsEbenenElementPath);
    if (!neighbor) {
      return {
        gliederungsEbenePath: gliederungsEbenenElementPath,
        findingPath: gliederungsEbenenElementPath,
        elementType: gliederungsEbenenElement.type,
        guid: gliederungsEbenenElement.GUID ?? '',
        type: findingType.onlyOneofType,
        parent: parentType,
        category: findingCategory.error,
      };
    }
    return undefined;
  }

  public static hasChildren(
    editor: BaseEditor,
    gliederungsEbenenElement: Element,
    gliederungsEbenenElementPath: Path,
    parentType?: string,
  ): HdrFinding | undefined {
    const children = getChildrenOfTypeAt(
      editor,
      REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY.map((gliederungsebene) => gliederungsebene.type),
      gliederungsEbenenElementPath,
    );

    if (children.length === 0 && gliederungsEbenenElement.type !== STRUCTURE_ELEMENTS.ARTICLE) {
      return {
        gliederungsEbenePath: gliederungsEbenenElementPath,
        findingPath: gliederungsEbenenElementPath,
        elementType: gliederungsEbenenElement.type,
        guid: gliederungsEbenenElement.GUID ?? '',
        type: findingType.noChildren,
        parent: parentType,
        category: findingCategory.error,
      };
    }
    return undefined;
  }

  public static hasHeadingText(
    editor: BaseEditor,
    gliederungsEbenenElement: Element,
    gliederungsEbenenElementPath: Path,
    parentType?: string,
  ): HdrFinding | undefined {
    const gliederungsEbeneHeader = getChildrenOfTypeAt(
      editor,
      [STRUCTURE_ELEMENTS.HEADING],
      gliederungsEbenenElementPath,
    );
    const [heading, headingPath] = gliederungsEbeneHeader[0];
    const textWrapper = getChildrenOfTypeAt(
      editor,
      [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
      headingPath,
    );
    const textElement = textWrapper.find((element) => element[0].children[0].text !== '');
    if (!textElement) {
      return {
        gliederungsEbenePath: gliederungsEbenenElementPath,
        findingPath: headingPath,
        elementType: gliederungsEbenenElement.type,
        guid: heading.GUID ?? '',
        type: findingType.noHeaderText,
        parent: parentType,
        category: findingCategory.error,
      };
    }
    return undefined;
  }

  public static hasMoreThanFiveAbsaetze(
    _editor: BaseEditor,
    gliederungsEbenenElement: Element,
    gliederungsEbenenElementPath: Path,
  ): HdrFinding | undefined {
    if (gliederungsEbenenElement.type !== STRUCTURE_ELEMENTS.ARTICLE) return undefined;
    const numberOfAbsaetze = gliederungsEbenenElement.children.filter(
      (element) => element.type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    ).length;
    if (numberOfAbsaetze > 5) {
      return {
        gliederungsEbenePath: gliederungsEbenenElementPath,
        findingPath: gliederungsEbenenElementPath,
        elementType: gliederungsEbenenElement.type,
        guid: gliederungsEbenenElement.GUID ?? '',
        type: findingType.moreThanFiveAbsaetze,
        category: findingCategory.warning,
      };
    }
    return undefined;
  }

  public static navigateHdrFinding = (
    editor: ReactEditor,
    reverse: boolean,
    currentFindingIndex: number,
    setCurrentFindingIndex: React.Dispatch<React.SetStateAction<number | undefined>>,
    hdrFindings: HdrFinding[],
  ) => {
    let finding;
    if (currentFindingIndex !== undefined) {
      if (reverse) {
        if (currentFindingIndex === 0) {
          setCurrentFindingIndex(hdrFindings.length - 1);
          finding = hdrFindings[hdrFindings.length - 1];
        } else {
          setCurrentFindingIndex(currentFindingIndex - 1);
          finding = hdrFindings[currentFindingIndex - 1];
        }
      } else {
        if (currentFindingIndex === hdrFindings.length - 1) {
          setCurrentFindingIndex(0);
          finding = hdrFindings[0];
        } else {
          setCurrentFindingIndex(currentFindingIndex + 1);
          finding = hdrFindings[currentFindingIndex + 1];
        }
      }
    } else if (reverse) {
      setCurrentFindingIndex(hdrFindings.length - 1);
      finding = hdrFindings[hdrFindings.length - 1];
    } else {
      setCurrentFindingIndex(0);
      finding = hdrFindings[0];
    }
    jumpToPath(editor, finding.findingPath.toString().replaceAll(',', '-'), 0);
  };

  public static getHdrFindingText = (item: HdrFinding): string => {
    const elementType = this.getGliederungsebene(item.elementType);
    const parentType = this.getGliederungsebene(item.parent ?? '');
    switch (item.type) {
      case findingType.noChildren:
        return t('lea.hdrAssistent.finding.noChildren', { elementType: elementType });
      case findingType.onlyOneofType:
        return t('lea.hdrAssistent.finding.onlyOneofType', { elementType: elementType, parentType: parentType });
      case findingType.noHeaderText:
        return t('lea.hdrAssistent.finding.noHeaderText', { elementType: elementType });
      case findingType.moreThanFiveAbsaetze:
        return t('lea.hdrAssistent.finding.moreThanFiveAbsaetze');
      default:
        return 'Muss noch definiert werden!';
    }
  };

  private static getGliederungsebene = (type: string) => {
    let name = '';
    const gliederungsEbene = HdrAssistentPlugin.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    const typeSplit = type.split(':');
    type = typeSplit[typeSplit.length - 1];
    if (gliederungsEbene) {
      if (gliederungsEbene.isEinzelvorschrift) {
        name = t(`lea.contextMenu.tableOfContents.options.stammform.${type}`);
      } else {
        name = t(`lea.contextMenu.tableOfContents.options.types.${type}`);
      }
    }
    return name !== '' ? name : 'Regelungstext';
  };
}
