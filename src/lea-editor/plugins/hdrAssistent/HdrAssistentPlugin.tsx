// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, NodeEntry, Path } from 'slate';
import {
  IPluginOptionDialogProps,
  Plugin,
  PluginProps,
  useHdrCheckStartetDispatch,
  useHdrCheckStartetState,
} from '../../plugin-core';
import { LeaDrawer, AriaController as AriaControllerLea } from '@lea/ui';
import React, { ReactElement, useRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  useEditorOptionsVisibleState,
  EditorOptions,
  useEditorOptionsVisibleChange,
} from '../../plugin-core/editor-store/editor-options';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { Button, Divider, List } from 'antd';
import { Element } from '../../../interfaces';
import { GliederungsEbenenQueries, getNodesOfTypeAt, getParentElement } from '../../plugin-core/utils';
import { Typography } from 'antd/lib';
import { REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY } from '../regelungstext';
import { jumpToPath } from '../../Editor';
import { HdrAssistentCommands as Commands } from './HdrAssistentCommands';
const { Title } = Typography;
import './HdrAssistent.less';
import { useFirstDocumentState } from '../../../api';
import { ReactEditor } from 'slate-react';
export interface HdrFinding {
  gliederungsEbenePath: Path;
  findingPath: Path;
  elementType: string;
  guid: string;
  type: findingType;
  parent?: string;
  category: findingCategory;
}

export enum findingType {
  onlyOneofType,
  noChildren,
  noHeaderText,
  moreThanFiveAbsaetze,
}

export enum findingCategory {
  none = '',
  error = 'error',
  warning = 'warning',
}

export class HdrAssistentPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(editor: T): T & ReactEditor {
    return editor as T & ReactEditor;
  }

  public static gliederungsEbenenQueries = new GliederungsEbenenQueries(
    REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY,
  );

  public OptionDialog = ({ editor }: IPluginOptionDialogProps): ReactElement => {
    const { t } = useTranslation();
    const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.HDR_ASSISTENT);
    const { closeOptions } = useEditorOptionsVisibleChange(EditorOptions.HDR_ASSISTENT);
    const hdrAssistentRef = useRef<HTMLDivElement>(null);
    const [gliederungsEbenenElements, setGliederungsEbenenElements] = useState<NodeEntry<Element>[]>();
    const [hdrFindings, setHdrFindings] = useState<HdrFinding[]>([]);
    const firstDocument = useFirstDocumentState();
    const { hdrCheckStartet } = useHdrCheckStartetState();
    const { setHdrCheckStartet } = useHdrCheckStartetDispatch();
    const [currentFindingIndex, setCurrentFindingIndex] = useState<number>();

    useEffect(() => {
      resetHdrCheck(gliederungsEbenenElements ?? []);
    }, []);

    const getGliederungsEbenenElements = (): NodeEntry<Element>[] => {
      return getNodesOfTypeAt(
        editor,
        REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY.map((gliederungsebene) => gliederungsebene.type),
        [0],
      );
    };

    const startHdrCheck = () => {
      const elements = getGliederungsEbenenElements();
      setGliederungsEbenenElements(elements);
      resetHdrCheck(elements);
      setHdrCheckStartet(true);
    };

    const resetHdrCheck = (elements: NodeEntry<Element>[]) => {
      setHdrFindings([]);
      setHdrCheckStartet(false);
      setCurrentFindingIndex(undefined);
      resetNodes(elements);
      const errorElements = document.querySelectorAll('.editorColumn .hdr-error');
      const warningElements = document.querySelectorAll('.editorColumn .hdr-warning');
      for (let i = errorElements.length - 1; i >= 0; i--) {
        errorElements[i].classList.remove('hdr-error');
      }
      for (let i = warningElements.length - 1; i >= 0; i--) {
        warningElements[i].classList.remove('hdr-warning');
      }
    };

    const resetNodes = (elements: NodeEntry<Element>[]) => {
      if (elements) {
        for (const [, gliederungsEbenenElementPath] of elements) {
          editor.setNodes({ 'lea:hdrIssue': undefined } as Partial<Element>, {
            at: gliederungsEbenenElementPath,
          });
        }
      }
    };

    useEffect(() => {
      if (gliederungsEbenenElements) {
        const findings: HdrFinding[] = [];
        for (const [gliederungsEbenenElement, gliederungsEbenenElementPath] of gliederungsEbenenElements) {
          const parent = getParentElement(editor, gliederungsEbenenElementPath);
          let finding: findingCategory | undefined = undefined;

          // Check if there are more than five Juristische Absätze inside an article
          const numberOfAbsatzFinding = Commands.hasMoreThanFiveAbsaetze(
            editor,
            gliederungsEbenenElement,
            gliederungsEbenenElementPath,
          );
          if (numberOfAbsatzFinding) {
            findings.push(numberOfAbsatzFinding);
            finding = findingCategory.warning;
          }

          // Check if Gliederungsebene has more than one sibling of type
          const neighborFinding = Commands.isMoreThanOneOfType(
            editor,
            gliederungsEbenenElement,
            gliederungsEbenenElementPath,
            parent?.type,
          );
          if (neighborFinding) {
            findings.push(neighborFinding);
            finding = findingCategory.error;
          }

          // Check if Gliederungsebene has children, except of Einzelvorschrift
          const childrenFinding = Commands.hasChildren(
            editor,
            gliederungsEbenenElement,
            gliederungsEbenenElementPath,
            parent?.type,
          );
          if (childrenFinding) {
            findings.push(childrenFinding);
            finding = findingCategory.error;
          }

          // Check if Gliederungseben Header has Text Content
          const headingFinding = Commands.hasHeadingText(
            editor,
            gliederungsEbenenElement,
            gliederungsEbenenElementPath,
            parent?.type,
          );
          if (headingFinding) {
            findings.push(headingFinding);
            finding = findingCategory.error;
          }
          if (finding) {
            editor.setNodes({ 'lea:hdrIssue': finding } as Partial<Element>, {
              at: gliederungsEbenenElementPath,
            });
          } else {
            editor.setNodes({ 'lea:hdrIssue': undefined } as Partial<Element>, {
              at: gliederungsEbenenElementPath,
            });
          }
        }
        setHdrFindings(findings);
      }
    }, [gliederungsEbenenElements]);

    useEffect(() => {
      AriaControllerPlateg.setAriaLabelsByQuery(
        '.lea-hdr-assistent-drawer .ant-drawer-close',
        'HdR-Assistent schließen',
      );
      setTimeout(
        () =>
          hdrAssistentRef.current &&
          AriaControllerLea.setElementIdByClassNameInElement(
            'ant-drawer-close',
            'lea-drawer-comment-option-close-button',
            hdrAssistentRef.current,
          ),
      );
    }, [optionsVisible]);

    const focusElement = (position: Path) => {
      jumpToPath(editor, position.toString().replaceAll(',', '-'), 0);
    };

    const renderFinding = (item: HdrFinding) => {
      const foundElementText = Commands.getHdrFindingText(item);
      const element = document.getElementById(`0:${item.guid}`);
      element?.classList.add(`hdr-${item.category}`);
      return (
        <>
          <li onClick={() => focusElement(item.findingPath)}>{foundElementText}</li>
          <Divider orientation="center" type="horizontal" />
        </>
      );
    };

    return (
      <LeaDrawer
        placement={'right'}
        className="lea-hdr-assistent-drawer"
        open={optionsVisible}
        onClose={closeOptions}
        title={t('lea.hdrAssistent.title')}
        expandable={true}
        refProp={hdrAssistentRef}
        ariaLabel={t('lea.hdrAssistent.ariaLabel')}
      >
        <Title>{t('lea.hdrAssistent.buttonLabel')}</Title>
        <Button id="hdrTestButton" type="primary" className="hdr-test-button" onClick={startHdrCheck}>
          {t('lea.hdrAssistent.buttonStart')}{' '}
        </Button>
        {hdrCheckStartet && (
          <Button
            id="hdrResetButton"
            type="primary"
            className="hdr-reset-button"
            onClick={() => resetHdrCheck(getGliederungsEbenenElements())}
          >
            {t('lea.hdrAssistent.buttonReset')}{' '}
          </Button>
        )}
        {hdrFindings.length > 0 ? (
          <>
            <div className="number-of-hdr-findings">
              {t('lea.hdrAssistent.hdrFindingsText')}
              {`${currentFindingIndex !== undefined ? currentFindingIndex + 1 + ' von ' : ''}`}
              {hdrFindings.length}
            </div>
            <div>{t('lea.hdrAssistent.jumpBetweenLabel')}</div>
            <div className="hdr-assistent-navigation-buttons">
              <Button
                id="hdrPrevButton"
                className="prev-button"
                type="primary"
                onClick={() =>
                  Commands.navigateHdrFinding(
                    editor,
                    true,
                    currentFindingIndex ?? 0,
                    setCurrentFindingIndex,
                    hdrFindings,
                  )
                }
              >
                {t('lea.hdrAssistent.backButton')}
              </Button>
              <Button
                id="hdrNextButton"
                className="next-button"
                type="primary"
                onClick={() =>
                  Commands.navigateHdrFinding(
                    editor,
                    false,
                    currentFindingIndex ?? 0,
                    setCurrentFindingIndex,
                    hdrFindings,
                  )
                }
              >
                {t('lea.hdrAssistent.nextButton')}
              </Button>
            </div>
            <List
              dataSource={hdrFindings}
              key="test"
              itemLayout="horizontal"
              id="Hdr_Findings_List"
              renderItem={(item) => renderFinding(item)}
            ></List>
          </>
        ) : (
          hdrCheckStartet && <div>{t('lea.hdrAssistent.noFindings', { title: firstDocument.document?.title })}</div>
        )}
      </LeaDrawer>
    );
  };
}
