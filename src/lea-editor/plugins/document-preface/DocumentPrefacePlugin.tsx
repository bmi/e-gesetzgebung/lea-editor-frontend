// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { Plugin, PluginProps } from '../../plugin-core';
import { DocumentPrefaceProps } from '../../../interfaces';
import { changePreface } from '../../plugin-core/utils';
import { HistoryEditor, History } from 'slate-history';
import { DocumentType } from '../../../api';

export class DocumentPrefacePlugin extends Plugin {
  public constructor(
    private readonly props: DocumentPrefaceProps,
    pluginProps: PluginProps,
    private history?: History,
  ) {
    super(pluginProps);
  }

  public create<T extends Editor>(editor: T): T {
    return editor;
  }

  public useEffect = (editor: ReactEditor & HistoryEditor): void => {
    // Wenn history vorhanden ist, wurde das Dokument gespeichert,
    // weshalb das Preface schon geladen wurde und nicht nochmal eingefügt werden darf,
    // da sonst alle redos zurückgesetzt werden.
    if (!this.history) {
      this.props.type !== DocumentType.SYNOPSE && changePreface(editor, this.props);
      editor.history.undos.pop();
    }
  };
}
