// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Plugin, PluginProps } from '../plugin-core';
import { DocumentDTO, DocumentType } from '../../api';

import * as RegelungstextPlugin from './regelungstext/stammform';
import * as RegelungstextMantelformPlugin from './regelungstext/mantelform';
import * as VorblattPlugin from './vorblatt';
import * as BegruendungPlugin from './begruendung';
import * as PraeambelPlugin from './regelungstext/preambel';
import * as AnschreibenPlugin from './anschreiben';
import * as AnlagePlugin from './anlage';
import * as CommentPlugin from './comment';
import * as AuthorialNotePlugin from './authorialNote';
import * as AuthorialNotePagePlugin from '../Editor/AuthorialNote/plugins';
import * as CopyAndPastePlugin from './copyAndPaste';
import * as SearchReplacePlugin from './searchAndReplace/';
import * as DynAenderungsVergleichPlugin from './dynAenderungsVergleich/';
import { ProtectedWhiteSpacePlugin } from '../plugin-core/generic-plugins/protected-whitespace';
import { DocumentPrefaceProps } from '../../interfaces';
import { ContentEditablePlugin } from './contentEditable';
import { MarkerPlugin } from '../plugin-core/generic-plugins/marker';
import { TabellePlugin } from '../plugin-core/generic-plugins/tabelle';
import { DocumentPrefacePlugin } from './document-preface';
import { CellStructurePlugin } from './synopsis/cellStructure/CellStructurePlugin';
import { SynopsisOptionsPlugin } from './synopsis/synopsis-options';
import { UndoRedoPlugin } from './undoRedo';
import { SynopsisPlugin } from './synopsis/SynopsisPlugin';
import { HdrAssistentPlugin } from './hdrAssistent/HdrAssistentPlugin';
import { SatzZaehlungPlugin } from './satzZaehlungPlugin/SatzZaehlungPlugin';

export class PluginFactory {
  public static getPlugins(document: DocumentDTO, props: PluginProps): Plugin[] {
    const documentPrefaceProps: DocumentPrefaceProps = {
      title: document.proposition?.title ?? '',
      shortTitle: document.proposition?.shortTitle ?? '',
      proponent:
        (!document.type.includes('ANSCHREIBEN')
          ? document.proposition?.proponent?.designationGenitive
          : document.proposition?.initiant) ?? '',
      abbreviation: document.proposition?.abbreviation ?? '',
      type: document.type,
    };

    const documentPlugins: Plugin[] = [
      new CommentPlugin.CommentPlugin(props),
      new SatzZaehlungPlugin(props),
      new AuthorialNotePlugin.AuthorialNotePlugin(props),
      new CommentPlugin.CommentOptionsPlugin(props),
      new CopyAndPastePlugin.CopyAndPastePlugin(props),
      new ContentEditablePlugin(props),
      new CellStructurePlugin(props),
      new SynopsisOptionsPlugin(props),
      new SearchReplacePlugin.SearchReplacePlugin(props),
      new ProtectedWhiteSpacePlugin(props),
      new DynAenderungsVergleichPlugin.DynAenderungsVergleichPlugin(props),
      // removed for initial Dynamische Änderungsnachverfolgung new TrimSpacesPlugin(props),
    ];
    if (!document.type.includes('BEGRUENDUNG')) {
      documentPlugins.unshift(new DocumentPrefacePlugin(documentPrefaceProps, props, document.history));
    }
    if (!document.type.includes('ANSCHREIBEN') && document.type !== DocumentType.ANLAGE) {
      documentPlugins.unshift(new MarkerPlugin(props, document.history), new TabellePlugin(props));
    }

    switch (document.type) {
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
        documentPlugins.unshift(
          new RegelungstextPlugin.JuristischerAbsatzPlugin(props),
          new RegelungstextPlugin.ListeMitUntergliederungPlugin(props),
          new PraeambelPlugin.GesetzPraeambelPlugin(props),
          new RegelungstextPlugin.TableOfContentsPlugin(props),
          new RegelungstextPlugin.HeaderPlugin(props),
          new HdrAssistentPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
        documentPlugins.unshift(
          new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(props),
          new PraeambelPlugin.GesetzPraeambelPlugin(props),
          new RegelungstextMantelformPlugin.TableOfContentsPlugin(props),
          new RegelungstextMantelformPlugin.HeaderPlugin(props),
          new HdrAssistentPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
        documentPlugins.unshift(
          new RegelungstextPlugin.JuristischerAbsatzPlugin(props),
          new RegelungstextPlugin.ListeMitUntergliederungPlugin(props),
          new PraeambelPlugin.VerordnungPraeambelPlugin(props),
          new RegelungstextPlugin.TableOfContentsPlugin(props),
          new RegelungstextPlugin.HeaderPlugin(props),
          new HdrAssistentPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(props),
          new PraeambelPlugin.VerordnungPraeambelPlugin(props),
          new RegelungstextMantelformPlugin.TableOfContentsPlugin(props),
          new RegelungstextMantelformPlugin.HeaderPlugin(props),
          new HdrAssistentPlugin(props),
        );
        break;
      case DocumentType.VORBLATT_STAMMGESETZ:
      case DocumentType.VORBLATT_MANTELGESETZ:
      case DocumentType.VORBLATT_STAMMVERORDNUNG:
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new VorblattPlugin.VorblattAbsatzPlugin(props),
          new VorblattPlugin.TableOfContentsPlugin(props),
          new VorblattPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new BegruendungPlugin.BegruendungAbsatzPlugin(props),
          new BegruendungPlugin.BegruendungAbsatzListePlugin(props),
          new BegruendungPlugin.TableOfContentsPlugin(props),
          new BegruendungPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
        documentPlugins.unshift(new AnschreibenPlugin.AnschreibenAbsatzPlugin(props));
        break;
      case DocumentType.ANLAGE:
        documentPlugins.unshift(new AnlagePlugin.AnlageAbsatzPlugin(props));
        break;
      case DocumentType.SYNOPSE:
        documentPlugins.unshift(new SynopsisPlugin(props));
        break;
      default:
        break;
    }
    documentPlugins.unshift(new UndoRedoPlugin(props, document.history));
    return documentPlugins;
  }

  public static getSynopsisPagePlugins(document: DocumentDTO | null, props: PluginProps): Plugin[] {
    if (!document) return [];
    const documentPrefaceProps: DocumentPrefaceProps = {
      title: document.proposition?.title ?? '',
      shortTitle: document.proposition?.shortTitle ?? '',
      proponent:
        (!document.type.includes('ANSCHREIBEN')
          ? document.proposition?.proponent?.designationGenitive
          : document.proposition?.initiant) ?? '',
      abbreviation: document.proposition?.abbreviation ?? '',
      type: document.type,
    };

    const documentPlugins: Plugin[] = [
      new CommentPlugin.CommentPlugin(props),
      new AuthorialNotePlugin.AuthorialNotePlugin(props),
      new SatzZaehlungPlugin(props),
      new CopyAndPastePlugin.CopyAndPastePlugin(props),
      new ContentEditablePlugin(props),
      new CellStructurePlugin(props),
      new SynopsisOptionsPlugin(props),
      new SearchReplacePlugin.SearchReplacePlugin(props),
      new ProtectedWhiteSpacePlugin(props),
      // removed for initial Dynamische Änderungsnachverfolgung new TrimSpacesPlugin(props),
    ];
    if (!document.type.includes('BEGRUENDUNG')) {
      documentPlugins.unshift(new DocumentPrefacePlugin(documentPrefaceProps, props, document.history));
    }
    if (!document.type.includes('ANSCHREIBEN') && document.type !== DocumentType.ANLAGE) {
      documentPlugins.unshift(new MarkerPlugin(props, document.history), new TabellePlugin(props));
    }

    switch (document.type) {
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
        documentPlugins.unshift(
          new RegelungstextPlugin.JuristischerAbsatzPlugin(props),
          new RegelungstextPlugin.ListeMitUntergliederungPlugin(props),
          new PraeambelPlugin.GesetzPraeambelPlugin(props),
          new RegelungstextPlugin.TableOfContentsPlugin(props),
          new RegelungstextPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
        documentPlugins.unshift(
          new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(props),
          new PraeambelPlugin.GesetzPraeambelPlugin(props),
          new RegelungstextMantelformPlugin.TableOfContentsPlugin(props),
          new RegelungstextMantelformPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
        documentPlugins.unshift(
          new RegelungstextPlugin.JuristischerAbsatzPlugin(props),
          new RegelungstextPlugin.ListeMitUntergliederungPlugin(props),
          new PraeambelPlugin.VerordnungPraeambelPlugin(props),
          new RegelungstextPlugin.TableOfContentsPlugin(props),
          new RegelungstextPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(props),
          new PraeambelPlugin.VerordnungPraeambelPlugin(props),
          new RegelungstextMantelformPlugin.TableOfContentsPlugin(props),
          new RegelungstextMantelformPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.VORBLATT_STAMMGESETZ:
      case DocumentType.VORBLATT_MANTELGESETZ:
      case DocumentType.VORBLATT_STAMMVERORDNUNG:
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new VorblattPlugin.VorblattAbsatzPlugin(props),
          new VorblattPlugin.TableOfContentsPlugin(props),
          new VorblattPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
        documentPlugins.unshift(
          new BegruendungPlugin.BegruendungAbsatzPlugin(props),
          new BegruendungPlugin.BegruendungAbsatzListePlugin(props),
          new BegruendungPlugin.TableOfContentsPlugin(props),
          new BegruendungPlugin.HeaderPlugin(props),
        );
        break;
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
        documentPlugins.unshift(new AnschreibenPlugin.AnschreibenAbsatzPlugin(props));
        break;
      case DocumentType.ANLAGE:
        documentPlugins.unshift(new AnlagePlugin.AnlageAbsatzPlugin(props));
        break;
      default:
        break;
    }
    documentPlugins.unshift(new UndoRedoPlugin(props, document.history));
    return documentPlugins;
  }

  public static getAuthorialNotePagePlugins(document: DocumentDTO, props: PluginProps): Plugin[] {
    props = { ...props, isAuthorialNotePlugin: true };
    switch (document.type) {
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
      case DocumentType.VORBLATT_STAMMGESETZ:
      case DocumentType.VORBLATT_MANTELGESETZ:
      case DocumentType.VORBLATT_STAMMVERORDNUNG:
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
      case DocumentType.ANLAGE:
        return [
          new AuthorialNotePagePlugin.AuthorialPagePlugin(props),
          new CopyAndPastePlugin.CopyAndPastePlugin(props),
          new ContentEditablePlugin(props),
          new SearchReplacePlugin.SearchReplacePlugin(props),
          new ProtectedWhiteSpacePlugin(props),
          // removed for initial Dynamische Änderungsnachverfolgung new TrimSpacesPlugin(props),
        ];
      default:
        return [];
    }
  }
}
