// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, NodeEntry, Path } from 'slate';
import { Element } from '../../../../../interfaces';

import { getLowestNodeAt, isOfType, isWithinNodeOfType, isWithinTypeHierarchy } from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { TABELLE_STRUCTURE } from '../../../../plugin-core/generic-plugins/tabelle';

export class JuristischerAbsatzQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(editor, [REGELUNGSTEXT_STRUCTURE.CONTENT, REGELUNGSTEXT_STRUCTURE.PARAGRAPH], path) &&
      !isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.LIST, path) &&
      !isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE, path)
    );
  }

  public static isEmpty(editor: Editor) {
    // Get currently selected p
    const [selectedPElement] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    //Get text
    const node = getLowestNodeAt(editor);
    const text = node ? node.text : '';
    return selectedPElement.children.length > 1 ? false : text.length === 0;
  }
}
