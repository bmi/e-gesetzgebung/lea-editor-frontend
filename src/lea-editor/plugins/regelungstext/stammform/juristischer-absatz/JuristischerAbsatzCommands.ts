// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms } from 'slate';

import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLastLocationAt,
  getLowestNodeAt,
  getNodeAt,
  getSiblings,
} from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { ListeMitUntergliederungCommands } from './liste-mit-untergliederung';

export class JuristischerAbsatzCommands {
  public static mergeParagraphs(editor: Editor, isDynAenderungsVergleich = false): void {
    const [, selectedParagraphPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    }) as NodeEntry<Element>;

    const [selectedPElement, selectedPElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedPElementChildren = [...selectedPElement.children];

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if paragraph to be deleted is empty.
    // Since merge of paragraphs containing text will be a future demand it's handled within here
    if (selectedPElementChildren.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }
    // Add text to previous paragraph's text

    const [previousPElement, previousPElementPath] = Editor.previous(editor, {
      at: selectedPElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    //if last node is deleted go further down until finding else
    let previousTextNode = null;
    let previousNode = null;

    for (let index = previousPElement.children.length - 1; index >= 0; index--) {
      previousNode = getNodeAt(editor, [...previousPElementPath, index]);

      if (previousNode) {
        if (
          (!isDynAenderungsVergleich && previousNode[0]['lea:changeType'] !== 'ct_deleted') ||
          isDynAenderungsVergleich
        ) {
          previousTextNode = getLowestNodeAt(editor, [...previousPElementPath, index]);
          break;
        }
      }
    }

    if (previousTextNode && selectedTextNode) {
      //Merge TextWrapper and remove parts from nodes
      Transforms.removeNodes(editor, { at: selectedParagraphPath });

      JuristischerAbsatzCommands.renumber(editor);
      if (previousNode) {
        Transforms.select(editor, getLastLocationAt(editor, previousNode[1]));
      }
    }
  }

  public static insertParagraph(editor: Editor): void {
    const [selectedPElement] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;
    const selectedTextNode = getLowestNodeAt(editor);
    const text = selectedTextNode ? selectedTextNode.text : '';

    // Currently insertion is not allowed if current paragraph is empty.
    // Since this could be a future demand it's handled within here.
    if (text.length === 0 && selectedPElement.children.length === 1) {
      return;
    }

    const [selectedParagraph] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    });
    const [, path] = selectedParagraph as NodeEntry<Ancestor>;

    // Create next sibling's path
    const newPath = path.map((x, y) => (y === path.length - 1 ? ++x : x));

    // Create and insert new paragraph
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    const newParagraph = createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
    Transforms.insertNodes(editor, newParagraph, { at: newPath });

    // Renumber
    JuristischerAbsatzCommands.renumber(editor);

    // Find and select new content node
    const [newContent] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
      at: newPath,
    });
    const [, newContentPath] = newContent as NodeEntry<Ancestor>;
    Transforms.select(editor, newContentPath);
  }

  public static insertListeMitUntergliederung(editor: Editor): void {
    // Get current paragraph
    const [currentParagraph] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    });
    const [, currentParagraphPath] = currentParagraph as NodeEntry<Element>;

    // Get previous paragraph
    const previousParagraph = Editor.previous(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
      at: currentParagraphPath,
    });
    const [, previousParagraphPath] = previousParagraph as NodeEntry<Element>;

    // If previous paragraph already has a list -> return
    const [existingList] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      at: previousParagraphPath,
    });
    if (existingList) {
      return;
    }

    // Remove current paragraph
    Transforms.removeNodes(editor, { at: currentParagraphPath });

    // If previous paragraph does not have a list -> remove its content and insert new list
    const [existingContent] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
      at: previousParagraphPath,
    });

    if (existingContent) {
      const [existingContentNode, existingContentPath] = existingContent as NodeEntry<Element>;
      const existingContentChilds = [...existingContentNode.children];

      const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
      const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
        name: '1',
      });
      const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('1.')]);
      const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
      const point = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
      const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, existingContentChilds);
      const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);

      Transforms.delete(editor, { at: existingContentPath });
      Transforms.insertNodes(editor, list, { at: existingContentPath });

      JuristischerAbsatzCommands.renumber(editor);

      const pointPath = [...existingContentPath, 1];
      ListeMitUntergliederungCommands.renumber(editor, pointPath);

      // Determine list item content path and select it
      const [listContent] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
        at: existingContentPath,
      });
      const [, listContentPath] = listContent as NodeEntry<Element>;
      Transforms.select(editor, listContentPath);
    }
  }

  public static renumber(editor: Editor): void {
    const [selectedParagraph] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    });
    const [, selectedNodePath] = selectedParagraph as NodeEntry<Ancestor>;
    const siblings = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.PARAGRAPH, selectedNodePath);
    // Only renumber if more than one paragraph exists; otherwise clear numbers
    const renumber = siblings.length > 1;
    siblings.forEach((paragraph, index) => {
      const [, paragraphPath] = paragraph;
      const [num] = Editor.nodes(editor, {
        at: paragraphPath,
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.NUM,
      });
      const [, numPath] = num;
      Transforms.insertText(editor, renumber ? `(${index + 1})` : '', { at: [...numPath, 1] });
      // Set name attribute of Marker Element
      Transforms.setNodes(editor, { name: `${index + 1}`, children: [] }, { at: [...numPath, 0] });
    });
  }
}
