// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../../../plugin-core';

import { REGELUNGSTEXT_STRUCTURE } from '../../../RegelungstextStructure';
import { ListeMitUntergliederungCommands as Commands } from './ListeMItUntergliederungCommands';
import { ListeMitUntergliederungQueries as Queries } from './ListeMitUntergliederungQueries';

import {
  Unit,
  isFirstPosition,
  isFirstSiblingOfType,
  isLastPosition,
  isEnter,
  getSiblings,
  isWithinNodeOfType,
  isListCommand,
  isLastSiblingOfType,
  isOfType,
} from '../../../../../plugin-core/utils';
import { Element } from '../../../../../../interfaces';

export class ListeMitUntergliederungPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;
    if (this.editorDocument.isReadonly) {
      return editor;
    }
    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (
        isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT) &&
        isFirstPosition(editor) &&
        !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT)
      ) {
        Commands.mergePoints(editor);
      } else if (
        isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.P) &&
        isFirstPosition(editor) &&
        !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.P)
      ) {
        Commands.mergeParagraphs(editor);
      } else if (
        isFirstPosition(editor) &&
        getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT).length === 1 &&
        !Queries.hasList(editor)
      ) {
        Commands.deleteList(editor);
      }
    };

    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isEnter(event) && !isLastPosition(editor)) {
      event.preventDefault();
      return false;
    }

    if (isEnter(event) && isLastPosition(editor)) {
      if (isOfType(editor, REGELUNGSTEXT_STRUCTURE.WRAPUP)) {
        Commands.closeWrapUp(editor);
      } else if (!Queries.isEmpty(editor) && !Queries.hasList(editor)) {
        Commands.insertPoint(editor);
      } else if (
        Queries.isEmpty(editor) &&
        Queries.getListItems(editor).length > 2 &&
        isLastSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT)
      ) {
        Commands.closeList(editor);
      }
      event.preventDefault();
      return false;
    }

    if (isListCommand(event) && Queries.isEmpty(editor)) {
      Commands.insertListeMitUntergliederung(editor);
      event.preventDefault();
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
