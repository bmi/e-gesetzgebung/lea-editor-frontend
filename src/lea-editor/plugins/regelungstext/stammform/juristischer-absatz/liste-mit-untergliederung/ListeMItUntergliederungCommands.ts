// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms, Path } from 'slate';

import { Element } from '../../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLowestNodeAt,
  getPreviousSibling,
  getSiblings,
  getLastLocationAt,
} from '../../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../RegelungstextStructure';

import { ListeMitUntergliederungQueries as Queries } from './ListeMitUntergliederungQueries';

import { JuristischerAbsatzCommands } from '../JuristischerAbsatzCommands';

export class ListeMitUntergliederungCommands {
  private static readonly CHARACTERS = 'abcdefghijklmnopqrstuvwxyz';
  private static wrapUpText = '';
  public static mergePoints(editor: Editor): void {
    const [, selectedPointPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
    }) as NodeEntry<Element>;

    const [selectedPElement, selectedPElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedPElementChildren = [...selectedPElement.children];

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if point to be deleted is empty.
    // Since merge of points containing text will be a future demand it's handled within here
    if (selectedPElementChildren.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous paragraph's text
    const [previousPElement, previousPElementPath] = Editor.previous(editor, {
      at: selectedPElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, [...previousPElementPath, previousPElement.children.length - 1]);

    if (previousTextNode && selectedTextNode) {
      //Merge TextWrapper and remove parts from nodes
      Transforms.removeNodes(editor, { at: selectedPointPath });
      const newPath = selectedPointPath.map((x, y) => (y === selectedPointPath.length - 1 ? --x : x));
      ListeMitUntergliederungCommands.renumber(editor, newPath);
      Transforms.select(editor, getLastLocationAt(editor, previousPElementPath));
    }
  }

  public static mergeParagraphs(editor: Editor): void {
    const [selectedPElement, selectedPElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedPElementChildren = [...selectedPElement.children];

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if point to be deleted is empty.
    // Since merge of points containing text will be a future demand it's handled within here
    if (selectedPElementChildren.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous paragraph's text
    const [previousPElement, previousPElementPath] = Editor.previous(editor, {
      at: selectedPElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, [...previousPElementPath, previousPElement.children.length - 1]);

    if (previousTextNode && selectedTextNode) {
      //Merge TextWrapper and remove parts from nodes
      Transforms.removeNodes(editor, { at: selectedPElementPath });
      ListeMitUntergliederungCommands.renumber(editor, previousPElementPath);
      Transforms.select(editor, getLastLocationAt(editor, previousPElementPath));
    }
  }

  public static insertListeMitUntergliederung(editor: Editor): void {
    // Get current point
    const [currentPoint] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, currentPointPath] = currentPoint as NodeEntry<Element>;

    const currentLevel = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, currentPointPath);

    // Only four list levels are allowed
    if (currentLevel >= 4) {
      return;
    }

    // Get previous point
    const previousPoint = getPreviousSibling(editor, REGELUNGSTEXT_STRUCTURE.POINT, currentPointPath);

    if (!previousPoint) {
      return;
    }

    const [, previousPointPath] = previousPoint;

    // If previous point already has a list -> return
    const [existingList] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      at: previousPointPath,
      mode: 'lowest',
    });
    const [, listPath] = existingList as NodeEntry<Element>;

    // Check if previous point already contains a list
    if (existingList && listPath.length > previousPointPath.length) {
      return;
    }

    // If previous paragraph does not have a list -> remove its content and insert new list
    const [existingContent] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
      at: previousPointPath,
    });

    if (existingContent) {
      const [existingContentNode, existingContentPath] = existingContent as NodeEntry<Element>;
      const existingContentChilds = [...existingContentNode.children];

      const point = ListeMitUntergliederungCommands.createPoint('');
      const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, existingContentChilds);
      const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);

      Transforms.delete(editor, { at: existingContentPath });
      Transforms.insertNodes(editor, list, { at: existingContentPath });

      const [newPoint] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
        at: existingContentPath,
        mode: 'lowest',
      });
      const [, newPointPath] = newPoint as NodeEntry<Element>;

      // Determine list item content path and select it
      const [listContent] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
        at: existingContentPath,
      });
      const [, listContentPath] = listContent as NodeEntry<Element>;

      // Remove current point and renumber
      Transforms.removeNodes(editor, { at: currentPointPath });

      const newPointParentPath = currentPointPath.map((x, y) => (y === currentPointPath.length - 1 ? --x : x));

      ListeMitUntergliederungCommands.renumber(editor, newPointPath);
      ListeMitUntergliederungCommands.renumber(editor, newPointParentPath);

      Transforms.select(editor, listContentPath);
    }
  }

  public static deleteList(editor: Editor): void {
    // Remember intro text
    const [selectedList] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      mode: 'lowest',
    });
    const [, listPath] = selectedList as NodeEntry<Ancestor>;

    const [selectedIntroPNode] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
      at: listPath,
    });
    const [selectedIntroP, selectedIntroPPath] = selectedIntroPNode as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    if (selectedTextNode && selectedTextNode.text.length > 0) {
      return;
    }

    // Delete selected list
    Transforms.removeNodes(editor, { at: listPath });

    // Insert content instead
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, selectedIntroP.children);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    Transforms.insertNodes(editor, content, { at: listPath });

    //select and focus
    Transforms.select(editor, getLastLocationAt(editor, selectedIntroPPath));
  }

  public static insertPoint(editor: Editor): void {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Check number of list items depending on list level
    const listItems = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);
    const level = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);

    // Cancel if end of character list has been reached
    if (level > 1 && listItems.length >= ListeMitUntergliederungCommands.CHARACTERS.length) {
      return;
    }

    // Currently insertion is not allowed if current point is empty.
    // Since this could be a future demand it's handled within here.
    if (Queries.isEmpty(editor)) {
      return;
    }

    // Create next sibling's path
    const newPath = pointPath.map((x, y) => (y === pointPath.length - 1 ? ++x : x));

    // Create and insert new point
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    const newPoint = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
    Transforms.insertNodes(editor, newPoint, { at: newPath });

    // Renumber
    ListeMitUntergliederungCommands.renumber(editor, newPath);

    Transforms.select(editor, getLastLocationAt(editor, newPath));
  }

  public static closeList(editor: Editor): void {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Find list item in list above
    const listItemAbove = Editor.above(editor, {
      at: pointPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
    });

    // If highest list level has been reached -> create wrap up element at current path
    if (!listItemAbove) {
      ListeMitUntergliederungCommands.addWrapUp(editor, pointPath);
      return;
    }
    const [, listItemPath] = listItemAbove as NodeEntry<Element>;

    // Create next sibling's path
    const nextPath = listItemPath.map((x, y) => (y === listItemPath.length - 1 ? ++x : x));

    // Delete selected list item
    Transforms.removeNodes(editor, { at: pointPath, hanging: false });

    // Insert new point above
    const point = ListeMitUntergliederungCommands.createPoint('');
    Transforms.insertNodes(editor, point, { at: nextPath });

    // Renumber
    ListeMitUntergliederungCommands.renumber(editor, nextPath);

    Transforms.select(editor, getLastLocationAt(editor, nextPath));
  }

  public static addWrapUp(editor: Editor, path: Path): void {
    // Get parent path
    const listPath = path.filter((_, y) => y < path.length - 1);

    // Check if WrapUp Element already exists
    const [existingWrapUpElement] = Editor.nodes(editor, {
      at: listPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.WRAPUP,
      mode: 'lowest',
    });

    // Delete selected list item
    Transforms.removeNodes(editor, { at: path, hanging: false });

    if (!existingWrapUpElement) {
      // Create WrapUp Element
      const wrapUpElement: Element = ListeMitUntergliederungCommands.createWrapUp(
        ListeMitUntergliederungCommands.wrapUpText,
      );
      Transforms.insertNodes(editor, wrapUpElement, { at: path });
    }

    // Select and set focus
    Transforms.select(editor, getLastLocationAt(editor, path));
  }

  public static closeWrapUp(editor: Editor) {
    // get current WrapUp Element
    const [wrapUpElement] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.WRAPUP,
      mode: 'lowest',
    });
    const [, wrapUpElementPath] = wrapUpElement as NodeEntry<Element>;

    // Get WrapUp Text
    const [wrapUpPElement] = Editor.node(editor, [...wrapUpElementPath, 0]) as NodeEntry<Element>;
    const selectedWrapUpTextNode = getLowestNodeAt(editor, wrapUpElementPath);
    const selectedWrapUpText = selectedWrapUpTextNode ? selectedWrapUpTextNode.text : '';

    // Find parent paragraph item above
    const paragraphItemAbove = Editor.above(editor, {
      at: wrapUpElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
    });
    const [, paragraphPath] = paragraphItemAbove as NodeEntry<Ancestor>;

    // Create next sibling's path
    const nextParagraphPath = paragraphPath.map((x, y) => (y === paragraphPath.length - 1 ? ++x : x));
    const siblings = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.PARAGRAPH, paragraphPath);

    //Checks if a next paragraph already exists
    const nextParagraphExists = siblings.find(
      (sibling) => JSON.stringify(sibling[1]) === JSON.stringify(nextParagraphPath),
    );

    // Create Paragraph if needed
    if (!nextParagraphExists) {
      const paragraph: Element = ListeMitUntergliederungCommands.createParagraph('');
      Transforms.insertNodes(editor, paragraph, { at: nextParagraphPath });
      JuristischerAbsatzCommands.renumber(editor);
    }

    // Get Content path of Paragraph
    const [paragraphContentElement] = Editor.nodes(editor, {
      at: nextParagraphPath,
      match: (n) =>
        (n as Element).type === REGELUNGSTEXT_STRUCTURE.INTRO ||
        (n as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
      mode: 'highest',
    });
    const [, paragraphContentPath] = paragraphContentElement as NodeEntry<Ancestor>;

    // Select and set focus
    Transforms.select(editor, getLastLocationAt(editor, paragraphContentPath));

    // Check if WrapUp text was changed
    if (
      selectedWrapUpText === ListeMitUntergliederungCommands.wrapUpText ||
      (selectedWrapUpText === '' && wrapUpPElement.children.length === 1)
    ) {
      // Remove WrapUp
      Transforms.removeNodes(editor, { at: wrapUpElementPath, hanging: false });
    }
  }

  public static renumber(editor: Editor, at: Path): void {
    const level = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, at);
    const siblings = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, at);

    siblings.forEach((point, index) => {
      const [, pointPath] = point;
      const [num] = Editor.nodes(editor, {
        at: pointPath,
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.NUM,
      });
      const [, numPath] = num;
      let number = '';

      if (level === 1) {
        number = (++index).toString();
      } else {
        const array = Array.from(ListeMitUntergliederungCommands.CHARACTERS);
        for (let i = 1; i < level; i++) {
          number += array[index];
        }
      }

      Transforms.insertText(editor, `${number}${level === 1 ? '.' : ')'}`, { at: [...numPath, 1] });

      // Set name attribute of Marker Element
      Transforms.setNodes(editor, { name: number, children: [] }, { at: [...numPath, 0] });
    });
  }

  private static createWrapUp(text: string): Element {
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
    return createElementOfType(REGELUNGSTEXT_STRUCTURE.WRAPUP, [p]);
  }

  private static createParagraph(text: string): Element {
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
  }

  private static createPoint(text: string): Element {
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    return createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
  }
}
