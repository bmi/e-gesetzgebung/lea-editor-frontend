// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Path } from 'slate';

import { Element } from '../../../../../../interfaces';
import {
  getLowestNodeAt,
  getSiblings,
  isOfType,
  isWithinTypeHierarchy,
  isWithinNodeOfType,
} from '../../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../RegelungstextStructure';
import { TABELLE_STRUCTURE } from '../../../../../plugin-core/generic-plugins/tabelle';

export class ListeMitUntergliederungQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      !isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE, path) &&
      (isWithinTypeHierarchy(
        editor,
        [
          REGELUNGSTEXT_STRUCTURE.CONTENT,
          REGELUNGSTEXT_STRUCTURE.POINT,
          REGELUNGSTEXT_STRUCTURE.LIST,
          REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
        ],
        path,
      ) ||
        isWithinTypeHierarchy(
          editor,
          [REGELUNGSTEXT_STRUCTURE.INTRO, REGELUNGSTEXT_STRUCTURE.LIST, REGELUNGSTEXT_STRUCTURE.PARAGRAPH],
          path,
        ) ||
        isWithinTypeHierarchy(
          editor,
          [REGELUNGSTEXT_STRUCTURE.WRAPUP, REGELUNGSTEXT_STRUCTURE.LIST, REGELUNGSTEXT_STRUCTURE.PARAGRAPH],
          path,
        ))
    );
  }

  public static getListLevel(editor: Editor, type: string, location: Path): number {
    const result: NodeEntry<Ancestor>[] = [];
    let above = Editor.above(editor, { at: location, match: (n) => (n as Element).type === type, mode: 'lowest' });

    while (above) {
      result.push(above as NodeEntry<Element>);
      const [, path] = above as NodeEntry<Ancestor>;
      above = Editor.above(editor, { at: path, match: (n) => (n as Element).type === type, mode: 'lowest' });
    }

    return result.length + 1;
  }

  public static getListItems(editor: Editor): NodeEntry<Element>[] {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Determine how many list items exist
    return getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);
  }

  public static hasList(editor: Editor) {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });

    if (!selectedPoint) {
      return true;
    }

    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Check if list exists below
    const [list] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      at: pointPath,
      mode: 'lowest',
    });
    const [, listPath] = list as NodeEntry<Ancestor>;

    return listPath.length > pointPath.length;
  }

  public static isEmpty(editor: Editor) {
    // Get currently selected p
    const [selectedPElement] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    //Get text
    const node = getLowestNodeAt(editor);
    const text = node ? node.text : '';
    return selectedPElement.children.length > 1 ? false : text.length === 0;
  }
}
