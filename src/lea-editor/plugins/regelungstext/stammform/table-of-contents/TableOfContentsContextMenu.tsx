// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useTranslation } from 'react-i18next';

import { IPluginContextMenuProps, useEditorOnFocusState } from '../../../../plugin-core';
import React, { ReactElement, useMemo } from 'react';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsQueries as Queries, TableOfContentsQueries } from './TableOfContentsQueries';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import {
  useContextMenuChange,
  useContextMenuNodeEntryState,
} from '../../../../plugin-core/editor-store/workspace/contextMenu';
import { isFirstOfType, isLastSiblingOfType } from '../../../../plugin-core/utils';
import { ItemType } from 'antd/lib/menu/interface';
import { TableOfContentsPlugin } from './TableOfContentsPlugin';
import { Editor, NodeEntry } from 'slate';
import { ContextMenuOption, Element } from '../../../../../interfaces';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';

interface ITableOfContentsContextMenuProps extends IPluginContextMenuProps {
  plugin: TableOfContentsPlugin;
}

export const TableOfContentsContextMenu = ({
  keyName,
  plugin,
  editor,
}: ITableOfContentsContextMenuProps): ReactElement => {
  const { setTargetNodeEntry } = useContextMenuChange();
  const { nodeEntry } = useContextMenuNodeEntryState();
  const { t } = useTranslation();
  const editorOnFocus = useEditorOnFocusState();
  const renderType = (type: string, asNext?: boolean): string => {
    let name = '';
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    const typeSplit = type.split(':');
    type = typeSplit[typeSplit.length - 1];
    if (gliederungsEbene) {
      if (gliederungsEbene.isEinzelvorschrift) {
        name = asNext
          ? t(`lea.contextMenu.tableOfContents.options.stammform.asNext.${type}`)
          : t(`lea.contextMenu.tableOfContents.options.stammform.${type}`);
      } else {
        name = asNext
          ? t(`lea.contextMenu.tableOfContents.options.types.asNext.${type}`)
          : t(`lea.contextMenu.tableOfContents.options.types.${type}`);
      }
    }
    return name;
  };

  const renderDeleteType = (type: string | undefined): string => {
    if (type === REGELUNGSTEXT_STRUCTURE.ARTICLE) {
      return t('lea.contextMenu.tableOfContents.deleteElement.article');
    } else {
      return t('lea.contextMenu.tableOfContents.deleteElement.onlyElement');
    }
  };

  const onSubMenuTitleClick = (e: React.MouseEvent<HTMLElement, MouseEvent> | React.KeyboardEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const getAddElementGroup = (options: ContextMenuOption[], selectedType: string): ItemType => ({
    key: `${keyName}-tableOfContents-regelungstext-stammform-addElement-group`,
    label: t('lea.contextMenu.tableOfContents.addElement'),
    popupClassName: 'submenu-container',
    children: options.map(({ type, onClick }) => ({
      key: `${keyName}-tableOfContents-regelungstext-stammform-addElement-${type.replace(':', '-')}`,
      title: renderType(type, selectedType === type),
      label: renderType(type, selectedType === type),
      onClick,
    })),
  });

  const getExpandLevelsGroup = (
    subdivideElementOptions: ContextMenuOption[],
    wrappElementOptions: ContextMenuOption[],
  ): ItemType => ({
    key: `${keyName}-tableOfContents-regelungstext-stammform-expandLevels-group`,
    label: t('lea.contextMenu.tableOfContents.expandLevels.title'),
    popupClassName: 'submenu-container',
    children: [
      {
        key: `${keyName}-tableOfContents-regelungstext-stammform-subdivideElement-submenu`,
        label: t('lea.contextMenu.tableOfContents.expandLevels.subdivideElement'),
        onTitleClick: (e) => onSubMenuTitleClick(e.domEvent),
        popupClassName: 'submenu-container',
        disabled: subdivideElementOptions.length === 0,
        children: subdivideElementOptions.map(({ type, onClick }) => {
          return {
            key: `${keyName}-tableOfContents-regelungstext-stammform-subdivideElement-${type.replace(':', '-')}`,
            title: renderType(type),
            label: renderType(type),
            onClick,
          };
        }),
      },
      {
        key: `${keyName}-tableOfContents-regelungstext-stammform-wrappElement-submenu`,
        onTitleClick: (e) => onSubMenuTitleClick(e.domEvent),
        popupClassName: 'submenu-container',
        label: t('lea.contextMenu.tableOfContents.expandLevels.wrappElement'),
        disabled: wrappElementOptions.length === 0,
        children: wrappElementOptions.map(({ type, onClick }) => {
          return {
            key: `${keyName}-tableOfContents-regelungstext-stammform-wrappElement-${type.replace(':', '-')}`,
            title: renderType(type),
            label: renderType(type),
            onClick,
          };
        }),
      },
    ],
  });

  const getDragOptions = (draggable: boolean, nodeEntry: NodeEntry<Element>): ItemType => {
    const nextArticle = Editor.next(editor, {
      at: nodeEntry[1],
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.ARTICLE,
    });
    const canMoveDown =
      nextArticle &&
      ((nextArticle[0] as Element).refersTo !== 'geltungszeitregel' ||
        isLastSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]));
    return {
      key: `${keyName}-tableOfContents-regelungstext-stammform-dragElement`,
      title: t('lea.contextMenu.tableOfContents.drag.title', {
        type: renderType(nodeEntry[0].type),
      }),
      label: t('lea.contextMenu.tableOfContents.drag.title', {
        type: renderType(nodeEntry[0].type),
      }),
      onTitleClick: (e) => onSubMenuTitleClick(e.domEvent),
      popupClassName: 'submenu-container',
      disabled: !draggable,
      children: [
        {
          key: `${keyName}-tableOfContents-regelungstext-stammform-dragUp`,
          title: t('lea.contextMenu.tableOfContents.drag.up', {
            type: renderType(nodeEntry[0].type),
          }),
          label: t('lea.contextMenu.tableOfContents.drag.up', {
            type: renderType(nodeEntry[0].type),
          }),
          disabled: isFirstOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]),
          onClick: () => {
            const newNodeEntry = Commands.moveOneUp(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
            AuthorialNoteCommands.renumberAuthorialNotes(editor);
          },
        },
        {
          key: `${keyName}-tableOfContents-regelungstext-stammform-dragDown`,
          title: t('lea.contextMenu.tableOfContents.drag.down', {
            type: renderType(nodeEntry[0].type),
          }),
          label: t('lea.contextMenu.tableOfContents.drag.down', {
            type: renderType(nodeEntry[0].type),
          }),
          disabled: !canMoveDown,
          onClick: () => {
            const newNodeEntry = Commands.moveOneDown(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
            AuthorialNoteCommands.renumberAuthorialNotes(editor);
          },
        },
        {
          key: `${keyName}-tableOfContents-regelungstext-stammform-dragToTop`,
          title: t('lea.contextMenu.tableOfContents.drag.toTop', {
            type: renderType(nodeEntry[0].type),
          }),
          label: t('lea.contextMenu.tableOfContents.drag.toTop', {
            type: renderType(nodeEntry[0].type),
          }),
          disabled: isFirstOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]),
          onClick: () => {
            const newNodeEntry = Commands.moveAsFirstOfType(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
            AuthorialNoteCommands.renumberAuthorialNotes(editor);
          },
        },
        // {
        //   key: `${keyName}-tableOfContents-regelungstext-stammform-dragToBottom`,
        //   title: t('lea.contextMenu.tableOfContents.drag.toBottom', {
        //     type: renderType(nodeEntry[0].type),
        //   }),
        //   label: t('lea.contextMenu.tableOfContents.drag.toBottom', {
        //     type: renderType(nodeEntry[0].type),
        //   }),
        //   disabled: isLastOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]),
        //   onClick: () => {
        //     const newNodeEntry = Commands.moveAsLastOfType(editor, nodeEntry);
        //     setTargetNodeEntry(newNodeEntry);
        //     AuthorialNoteCommands.renumberAuthorialNotes(editor);
        //   },
        // },
      ],
    };
  };

  const getDeleteGroup = (
    deleteElementOption: ContextMenuOption,
    deleteElementWithContentOption: ContextMenuOption,
    deleteAllElementsOption: ContextMenuOption,
  ): ItemType => ({
    key: `${keyName}-tableOfContents-regelungstext-stammform-deleteElement-group`,
    label: t('lea.contextMenu.tableOfContents.deleteElement.title'),
    popupClassName: 'submenu-container',
    children: [
      {
        key: `${keyName}-tableOfContents-regelungstext-stammform-deleteElement`,
        title: renderDeleteType(deleteElementOption.type),
        label: renderDeleteType(deleteElementOption.type),
        disabled: !deleteElementOption.onClick,
        onClick: deleteElementOption.onClick,
      },
      {
        key: `${keyName}-tableOfContents-regelungstext-stammform-deleteElementWithContent`,
        title: t('lea.contextMenu.tableOfContents.deleteElement.elementWithContent'),
        label: t('lea.contextMenu.tableOfContents.deleteElement.elementWithContent'),
        disabled: !deleteElementWithContentOption.onClick,
        onClick: deleteElementWithContentOption.onClick,
      },
      {
        key: `${keyName}-tableOfContents-regelungstext-stammform-deleteAllElements`,
        title: t('lea.contextMenu.tableOfContents.deleteElement.allElements'),
        label: t('lea.contextMenu.tableOfContents.deleteElement.allElements'),
        disabled: !deleteAllElementsOption.onClick,
        onClick: deleteAllElementsOption.onClick,
      },
    ],
  });

  const getItemTypes = (nodeEntry: NodeEntry<Element>): ItemType[] => {
    const selectedType = nodeEntry[0].type;
    const addElement = Queries.getOptionsForAddElement(editor, nodeEntry, setTargetNodeEntry);
    const subdivideElementOptions = Queries.getOptionsForSubdivideElement(editor, nodeEntry, setTargetNodeEntry);
    const wrappElementOptions = Queries.getOptionsForWrappElement(editor, nodeEntry, setTargetNodeEntry);
    const deleteElement = Queries.getOptionForDeleteElement(editor, nodeEntry, setTargetNodeEntry);
    const deleteElementWithContent = Queries.getOptionForDeleteElementWithContent(
      editor,
      nodeEntry,
      setTargetNodeEntry,
    );
    const deleteAllElements = Queries.getOptionForDeleteAllElements(editor, nodeEntry, setTargetNodeEntry);
    const draggable = Queries.nodeDraggable(editor, nodeEntry[1]);
    if (nodeEntry[0].refersTo === 'geltungszeitregel') return [];
    const newItemTypes: ItemType[] = [
      getAddElementGroup(addElement, selectedType),
      getExpandLevelsGroup(subdivideElementOptions, wrappElementOptions),
      getDragOptions(draggable, nodeEntry),
      getDeleteGroup(deleteElement, deleteElementWithContent, deleteAllElements),
    ].reduce((combinedTypes, itemType) => {
      if (itemType && combinedTypes.find((item) => !!item)) {
        return [...combinedTypes, { type: 'divider' }, itemType];
      }
      return [...combinedTypes, itemType];
    }, [] as ItemType[]);

    return newItemTypes;
  };

  useMemo(() => {
    if (nodeEntry && !plugin.editorDocument.isReadonly && plugin.editorDocument.index === editorOnFocus.index) {
      plugin.contextMenuItemTypes = getItemTypes(nodeEntry);
    } else {
      plugin.contextMenuItemTypes = null;
    }
  }, [nodeEntry]);
  return <></>;
};
