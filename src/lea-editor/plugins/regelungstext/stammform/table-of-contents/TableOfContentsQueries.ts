// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseEditor, Editor, NodeEntry, Path } from 'slate';
import { ContextMenuOption, Element } from '../../../../../interfaces';
import { getAllOfType, getChildrenOfTypeAt } from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY } from '../../RegelungstextStructure';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { GliederungsEbenenQueries } from '../../../../plugin-core/utils/GliederungsEbenenQueries';
import { STRUCTURE_ELEMENTS } from '../../../../plugin-core/utils/StructureElements';

export class TableOfContentsQueries {
  public static gliederungsEbenenQueries = new GliederungsEbenenQueries(
    REGELUNGSTEXT_STAMMFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY,
  );

  public static getOptionsForAddElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption[] {
    const [element] = nodeEntry;
    const options: ContextMenuOption[] = [];
    const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (gliederungsEbene && einzelvorschrift) {
      //Add the same type of selected gliederungsebene as next element
      options.push({
        type: gliederungsEbene.type,
        onClick: () => {
          const newNodeEntry = Commands.addBelow(editor, nodeEntry, gliederungsEbene.type);
          setTargetNodeEntry(newNodeEntry);
        },
      });
      if (element.children.length === 2) {
        const optionalGliederungsEbenen =
          TableOfContentsQueries.gliederungsEbenenQueries.getOptionalGliederungsEbenenBelow(element.type);
        //Add all gliederungsebenen below
        optionalGliederungsEbenen.forEach((optionalGliederungsEbene) =>
          options.push({
            type: optionalGliederungsEbene.type,
            onClick: () => {
              const newNodeEntry = Commands.addGliederungsEbene(editor, nodeEntry, optionalGliederungsEbene.type);
              setTargetNodeEntry(newNodeEntry);
            },
          }),
        );
      } else {
        //Add the same gliederungsebenen as first child as new first child
        const childGliederungsEbene = element.children
          .slice(2, element.children.length - 1)
          .every((child) => child.type === element.children[2].type && child.type)
          ? element.children[2]
          : null;
        if (
          childGliederungsEbene &&
          TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(childGliederungsEbene.type)
        ) {
          options.push({
            type: childGliederungsEbene.type,
            onClick: () => {
              const newNodeEntry = Commands.addGliederungsEbene(editor, nodeEntry, childGliederungsEbene.type);
              setTargetNodeEntry(newNodeEntry);
            },
          });
        }
      }
    }
    return options;
  }

  public static getOptionsForSubdivideElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption[] {
    const [element] = nodeEntry;
    const options: ContextMenuOption[] = [];
    const canSubdivide =
      element.children.length > 2 &&
      element.children.slice(2, element.children.length).every((child) => child.type === element.children[2].type);
    if (canSubdivide) {
      const gliederungsEbenenBetween =
        TableOfContentsQueries.gliederungsEbenenQueries.getOptionalGliederungsEbenenBetween(
          element.type,
          element.children[2].type,
        );
      gliederungsEbenenBetween.forEach((gliederungsEbene) => {
        options.push({
          type: gliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.addWrapperGliederungsEbene(editor, nodeEntry, gliederungsEbene.type, true);
            setTargetNodeEntry(newNodeEntry);
          },
        });
      });
    }

    return options;
  }

  public static getOptionsForWrappElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption[] {
    const [element, elementPath] = nodeEntry;
    const options: ContextMenuOption[] = [];
    const [parentElement] = Editor.parent(editor, elementPath) as NodeEntry<Element>;

    if (
      !parentElement ||
      !TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(parentElement.type)
    ) {
      const gliederungsEbenenBetween =
        TableOfContentsQueries.gliederungsEbenenQueries.getOptionalGliederungsEbenenAbove(element.type);
      gliederungsEbenenBetween.forEach((gliederungsEbene) => {
        options.push({
          type: gliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.addWrapperGliederungsEbene(editor, nodeEntry, gliederungsEbene.type);
            setTargetNodeEntry(newNodeEntry);
          },
        });
      });
    }

    return options;
  }

  public static getOptionForDeleteElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption {
    const [element] = nodeEntry;
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    const childGliederungsEbene =
      element.children.length > 2
        ? TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.children[2].type)
        : null;
    if (gliederungsEbene) {
      const canDeleteEinzelvorschrift =
        gliederungsEbene.isEinzelvorschrift &&
        (element.refersTo !== 'geltungszeitregel' || getAllOfType(editor, gliederungsEbene.type).length > 1);
      const canDeleteGliederungsEbene =
        !gliederungsEbene.isEinzelvorschrift &&
        (!childGliederungsEbene || childGliederungsEbene.dependingOn !== element.type);
      if (canDeleteEinzelvorschrift) {
        return {
          type: gliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.deleteEinzelvorschrift(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
          },
        };
      } else if (canDeleteGliederungsEbene) {
        return {
          type: gliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.deleteGliederungsEbene(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
          },
        };
      }
    }
    return { type: gliederungsEbene?.type || '' };
  }

  public static getOptionForDeleteElementWithContent(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption {
    const [element, path] = nodeEntry;
    const [...geltungszeitregel] = Editor.nodes(editor, {
      at: path,
      match: (element) => (element as Element).refersTo === 'geltungszeitregel',
    });
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (gliederungsEbene) {
      const canDeleteGliederungsEbene = !gliederungsEbene.isEinzelvorschrift && geltungszeitregel.length === 0;
      if (canDeleteGliederungsEbene) {
        return {
          type: gliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.deleteGliederungsEbene(editor, nodeEntry, true);
            setTargetNodeEntry(newNodeEntry);
          },
        };
      }
    }
    return { type: gliederungsEbene?.type || '' };
  }

  public static getOptionForDeleteAllElements(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption {
    const [element] = nodeEntry;
    const selectedGliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(
      element.type,
    );
    if (selectedGliederungsEbene && !selectedGliederungsEbene.isEinzelvorschrift) {
      const allGliederungsebenen = getAllOfType(editor, selectedGliederungsEbene.type);
      const canDelete = allGliederungsebenen.every(([gliederungsEbene]) =>
        gliederungsEbene.children.every(
          (child) =>
            TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(child.type)?.dependingOn !=
            gliederungsEbene.type,
        ),
      );
      if (canDelete) {
        return {
          type: selectedGliederungsEbene.type,
          onClick: () => {
            const newNodeEntry = Commands.deleteAllGliederungsEbenen(editor, nodeEntry);
            setTargetNodeEntry(newNodeEntry);
          },
        };
      }
    }
    return { type: selectedGliederungsEbene?.type || '' };
  }

  private static isEinzelvorschrift(editor: BaseEditor, path: Path, includeGeltungszeitregel: boolean) {
    try {
      const [node] = Editor.node(editor, path) as NodeEntry<Element>;
      const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
      return (
        node.type === einzelvorschrift?.type && (node.refersTo !== 'geltungszeitregel' || includeGeltungszeitregel)
      );
    } catch (e) {
      return false;
    }
  }

  public static nodeDraggable(editor: Editor, path: Path, onDragging?: boolean): boolean {
    if (onDragging) {
      return true;
    } else {
      return TableOfContentsQueries.isEinzelvorschrift(editor, path, false);
    }
  }

  public static nodeDroppable(editor: Editor, path: Path): boolean {
    return (
      TableOfContentsQueries.isEinzelvorschrift(editor, path, false) ||
      TableOfContentsQueries.canDropAsChild(editor, path)
    );
  }

  public static canDropAsChild(editor: Editor, path: Path): boolean {
    try {
      const [node] = Editor.node(editor, path) as NodeEntry<Element>;
      const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(node.type);
      if (gliederungsEbene && !gliederungsEbene.isEinzelvorschrift) {
        // Die ersten 2 children sind die benamung (z.b. Abschnitt 2) und die Überschrift der ebene
        // Es existieren entweder keine elemente innerhalb der Ebene
        // oder erste Element der darunter liegenden Ebenen ist eine Einzelvorschrift
        return node.children.length <= 2 || node.children[2].type === STRUCTURE_ELEMENTS.ARTICLE;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  public static canDropToExistingChildren(editor: Editor, path: Path): boolean {
    try {
      const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
      if (einzelvorschrift) {
        return (
          getChildrenOfTypeAt(
            editor,
            TableOfContentsQueries.gliederungsEbenenQueries.gliederungsEbenen.map(
              (gliederungsebene) => gliederungsebene.type,
            ),
            path,
          )[0][0].type === einzelvorschrift.type
        );
      }
      return false;
    } catch (e) {
      return false;
    }
  }
}
