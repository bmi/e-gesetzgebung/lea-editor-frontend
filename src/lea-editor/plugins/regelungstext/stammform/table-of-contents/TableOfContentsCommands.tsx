// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Node, NodeEntry, Path, Transforms } from 'slate';
import { DataNode } from 'antd/lib/tree';
import { Element, TextWrapper } from '../../../../../interfaces';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import {
  createElementOfType,
  createTextWrapper,
  getSiblings,
  isFirstSiblingOfType,
  getAllOfType,
  getChildrenOfTypeAt,
  getHeadingTextFromNode,
} from '../../../../plugin-core/utils';
import { TableOfContentsQueries } from './TableOfContentsQueries';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';
import { findingCategory } from '../../../hdrAssistent/HdrAssistentPlugin';
import { STRUCTURE_ELEMENTS } from '../../../../plugin-core/utils/StructureElements';

export class TableOfContentsCommands {
  public static getContentTableData(e: Editor, hdrCheckStartet: boolean): DataNode[] {
    const treeData: DataNode[] = [];
    if (e.children && e.children.length > 0) {
      (e.children as Element[]).forEach((n: Element, index: number) => {
        TableOfContentsCommands.createContentTable(n, treeData, [index], hdrCheckStartet);
      });
    }
    return treeData;
  }

  private static getClassName(hdrIssue?: findingCategory) {
    switch (hdrIssue) {
      case findingCategory.error:
        return 'hdr-error';
      case findingCategory.warning:
        return 'hdr-warning';
      default:
        return '';
    }
  }

  private static createContentTable(
    node: Element,
    array: DataNode[] | undefined,
    path: number[],
    hdrCheckStartet: boolean,
  ) {
    if (!node.children || !array) {
      return;
    }

    const key = path.join('-');
    if (TableOfContentsQueries.gliederungsEbenenQueries.gliederungsEbenen.some((x) => x.type === node.type)) {
      if (
        node.children.length > 1 &&
        (node.children[0] as Element).type === REGELUNGSTEXT_STRUCTURE.NUM &&
        (node.children[1] as Element).type === REGELUNGSTEXT_STRUCTURE.HEADING
      ) {
        const numText = Node.string(node.children[0]);
        const headingText = getHeadingTextFromNode(node.children[1] as Element);
        array.push({
          title: `${numText} ${headingText}`,
          key,
          className: hdrCheckStartet ? this.getClassName(node['lea:hdrIssue']) : '',
          children: [],
        });
      }
    }

    const i = array.length - 1;
    node.children.forEach((n: Element | TextWrapper, index: number) => {
      TableOfContentsCommands.createContentTable(
        n as Element,
        array[i]?.children && key.startsWith(array[i].key.toString()) ? array[i].children : array,
        [...path, index],
        hdrCheckStartet,
      );
    });
  }

  public static addEinzelvorschrift(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }
    const [, path] = nodeEntry;
    const newPath: Path = path.concat(2);

    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);

    if (gliederungsEbene) {
      Transforms.insertNodes(editor, newElement, { at: newPath });
      TableOfContentsCommands.changeLastRefersTo(editor);
      TableOfContentsCommands.renumberTypeWithTextAt(editor, gliederungsEbene.type, gliederungsEbene.title, newPath);
      return Editor.node(editor, newPath) as NodeEntry<Element>;
    }
    return null;
  }

  public static addGliederungsEbene(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }
    const [, path] = nodeEntry;
    const newPath: Path = path.concat(2);

    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);

    if (gliederungsEbene) {
      Transforms.insertNodes(editor, newElement, { at: newPath });
      TableOfContentsCommands.renumberTypeWithTextAt(editor, gliederungsEbene.type, gliederungsEbene.title, newPath);
      return Editor.node(editor, newPath) as NodeEntry<Element>;
    }
    return null;
  }

  public static addWrapperGliederungsEbene(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    toChild?: boolean,
  ): NodeEntry<Element> | null {
    const newElement = TableOfContentsCommands.getElementForType(type);
    if (!newElement) return null;

    //wrap children into type
    if (toChild && nodeEntry[0].children.length > 2) {
      nodeEntry = Editor.node(editor, nodeEntry[1].concat(2)) as NodeEntry<Element>;
    }
    const [selectedElement, path] = nodeEntry;
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    if (gliederungsEbene) {
      const [, parentElementPath] = Editor.parent(editor, path) as NodeEntry<Element>;
      const siblings = getSiblings(editor, selectedElement.type, path);
      Transforms.insertNodes(editor, newElement, { at: siblings[0][1] });
      TableOfContentsCommands.moveGliederungsebenenFromTo(editor, parentElementPath, siblings[0][1], [
        selectedElement.type,
      ]);
      TableOfContentsCommands.renumberTypeWithTextAt(
        editor,
        gliederungsEbene.type,
        gliederungsEbene.title,
        siblings[0][1],
      );
      return Editor.node(editor, siblings[0][1]) as NodeEntry<Element>;
    }
    return null;
  }

  public static addAbove(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    const [, path] = nodeEntry;

    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    if (gliederungsEbene) {
      Transforms.insertNodes(editor, newElement, { at: path });
      TableOfContentsCommands.renumberTypeWithTextAt(editor, gliederungsEbene.type, gliederungsEbene.title, path);
      return Editor.node(editor, path) as NodeEntry<Element>;
    }
    return null;
  }

  public static addBelow(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    const [, path] = nodeEntry;
    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }

    const newPath: Path = path.map((x, y) => (y === path.length - 1 ? ++x : x));
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
    if (gliederungsEbene && einzelvorschrift) {
      const [geltungszeitregelNodeEntryAtSelection] = Editor.nodes<Element>(editor, {
        at: path,
        match: (element) =>
          (element as Element).type === einzelvorschrift.type && (element as Element).refersTo === 'geltungszeitregel',
      });
      Transforms.insertNodes(editor, newElement, { at: newPath });
      const newNodeEntry = Editor.node(editor, newPath) as NodeEntry<Element>;
      if (gliederungsEbene.isEinzelvorschrift) {
        TableOfContentsCommands.changeLastRefersTo(editor);
      } else if (geltungszeitregelNodeEntryAtSelection) {
        TableOfContentsCommands.moveElement(editor, geltungszeitregelNodeEntryAtSelection, newNodeEntry, true, true);
      }
      TableOfContentsCommands.renumberTypeWithTextAt(editor, gliederungsEbene.type, gliederungsEbene.title, newPath);
      return newNodeEntry;
    }
    return null;
  }

  public static moveAsFirstOfType = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element] = from;
    const allOfType = getAllOfType(editor, element.type);
    return TableOfContentsCommands.moveElement(editor, from, allOfType[0], false, false);
  };

  public static moveAsLastOfType = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element] = from;
    const allOfType = getAllOfType(editor, element.type);
    return TableOfContentsCommands.moveElement(editor, from, allOfType[allOfType.length - 1], true, false);
  };

  public static moveOneUp = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element, elementPath] = from;
    const allOfType = getAllOfType(editor, element.type);
    const previousElement = allOfType[allOfType.findIndex((node) => node[1].toString() === elementPath.toString()) - 1];
    return TableOfContentsCommands.moveElement(
      editor,
      from,
      previousElement,
      elementPath[elementPath.length - 1] === 2,
      false,
    );
  };

  public static moveOneDown = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element, elementPath] = from;
    const allOfType = getAllOfType(editor, element.type);
    const [parent] = Editor.parent(editor, elementPath);
    const previousElement = allOfType[allOfType.findIndex((node) => node[1].toString() === elementPath.toString()) + 1];
    return TableOfContentsCommands.moveElement(
      editor,
      from,
      previousElement,
      parent.children.length - 1 !== elementPath[elementPath.length - 1],
      false,
    );
  };

  public static moveElement(
    editor: Editor,
    from: NodeEntry<Element>,
    to: NodeEntry<Element>,
    below: boolean,
    moveAsChildren: boolean,
  ): NodeEntry<Element> | null {
    const [fromNode, fromNodePath] = from;
    const [toNode, toNodePath] = to;
    if (fromNode.GUID === toNode.GUID) return null;

    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(fromNode.type);
    let parentPath = fromNodePath.slice(0, fromNodePath.length - 1);
    if (!parentPath.every((pathNumber, index) => toNodePath[index] === pathNumber)) {
      parentPath = toNodePath.slice(0, fromNodePath.length - 1);
    }
    Transforms.removeNodes(editor, { at: fromNodePath });
    const [...newToNodeEntry] = Editor.nodes<Element>(editor, {
      at: parentPath,
      match: (n) => (n as Element).GUID === toNode.GUID,
    });
    if (newToNodeEntry.length < 1) return null;
    if (moveAsChildren && gliederungsEbene) {
      return gliederungsEbene.isEinzelvorschrift
        ? TableOfContentsCommands.addEinzelvorschrift(editor, newToNodeEntry[0], fromNode.type, fromNode)
        : TableOfContentsCommands.addGliederungsEbene(editor, newToNodeEntry[0], fromNode.type, fromNode);
    } else if (below) {
      return TableOfContentsCommands.addBelow(editor, newToNodeEntry[0], fromNode.type, fromNode);
    } else {
      return TableOfContentsCommands.addAbove(editor, newToNodeEntry[0], fromNode.type, fromNode);
    }
  }

  public static deleteEinzelvorschrift(editor: Editor, nodeEntry: NodeEntry<Element>): NodeEntry<Element> | null {
    const [element, path] = nodeEntry;
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (gliederungsEbene?.isEinzelvorschrift) {
      const [parent] = Editor.parent(editor, path) as NodeEntry<Element>;
      Transforms.delete(editor, { at: path });
      TableOfContentsCommands.changeLastRefersTo(editor);

      //renumber new Einzelvorschrift
      const newPath =
        parent.children.length > 3
          ? path.map((x, y) => {
              if (y === path.length - 1 && path[path.length - 1] === parent.children.length - 1) {
                return --x;
              }
              return x;
            })
          : path.slice(0, path.length - 1);
      TableOfContentsCommands.renumberTypeWithTextAt(editor, gliederungsEbene.type, gliederungsEbene.title, newPath);
      return Editor.node(editor, newPath) as NodeEntry<Element>;
    }
    return null;
  }

  private static isFirstGliederungsEbeneSibling(editor: Editor, nodeEntry: NodeEntry<Element>): boolean {
    const [, path] = nodeEntry;
    const [parentElement] = Editor.parent(editor, path) as NodeEntry<Element>;
    const firstGliederungsEbenenIndex = parentElement.children.findIndex((gliederungsEbene) =>
      TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(gliederungsEbene.type),
    );
    return path[path.length - 1] === firstGliederungsEbenenIndex;
  }

  private static isOnlyGliederungsEbenenSibling(editor: Editor, nodeEntry: NodeEntry<Element>): boolean {
    const [, path] = nodeEntry;
    const [parentElement] = Editor.parent(editor, path) as NodeEntry<Element>;
    const gliederungsEbenenCount = parentElement.children.reduce(
      (gliederungsEbenenCount, child) =>
        TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(child.type)
          ? ++gliederungsEbenenCount
          : gliederungsEbenenCount,
      0,
    );
    return gliederungsEbenenCount === 1;
  }

  private static changeMoveToPath(editor: Editor, nodeEntry: NodeEntry<Element>, deleteContent?: boolean): Path {
    const [element, path] = nodeEntry;
    const isOnlyGliederungsEbenenSibling = TableOfContentsCommands.isOnlyGliederungsEbenenSibling(editor, nodeEntry);
    const isFirstGliederungsEbenenSibling = TableOfContentsCommands.isFirstGliederungsEbeneSibling(editor, nodeEntry);
    const siblingsOfSameType = getSiblings(editor, element.type, path);
    const isFirstSibling = isFirstSiblingOfType(editor, element.type, path);

    const childGliederungsEbene =
      element.children.length > 2 &&
      element.children.slice(2, element.children.length).every((child) => child.type === element.children[2].type)
        ? TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.children[2].type)
        : undefined;

    //add to gliederungsEbene above
    if (
      isOnlyGliederungsEbenenSibling ||
      (siblingsOfSameType.length === 1 && !childGliederungsEbene?.isEinzelvorschrift)
    ) {
      return path.slice(0, path.length - 1);
    }

    let newPath = [
      ...path.slice(0, -1),
      isFirstGliederungsEbenenSibling ? path[path.length - 1] + 1 : path[path.length - 1] - 1,
    ];

    //get path to move einzelvorschrift to other lowest gliederungsebene or next to other einzelvorschrift
    if (!deleteContent) {
      if (childGliederungsEbene?.isEinzelvorschrift) {
        const [...lowestElements] = Editor.nodes<Element>(editor, {
          at: newPath,
          mode: 'lowest',
          match: (lowestGliederungsEbene) =>
            !!TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(
              (lowestGliederungsEbene as Element).type,
            ),
        });
        if (lowestElements.length > 0) {
          const [lowestMoveToElement, lowestMoveToElementPath] =
            lowestElements[isFirstSibling ? 0 : lowestElements.length - 1];
          newPath =
            childGliederungsEbene.isEinzelvorschrift &&
            !TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(lowestMoveToElement.type)
              ?.isEinzelvorschrift
              ? lowestMoveToElementPath
              : lowestMoveToElementPath.slice(0, lowestMoveToElementPath.length - 1);
          return newPath;
        }
      }
    }
    return newPath;
  }

  public static deleteAllGliederungsEbenen(editor: Editor, nodeEntry: NodeEntry<Element>): NodeEntry<Element> | null {
    const [selectedElement, selectedElementPath] = nodeEntry;
    const allElementsCount = getAllOfType(editor, selectedElement.type).length;
    for (let i = 0; i < allElementsCount; i++) {
      const allNewElements = getAllOfType(editor, selectedElement.type);
      allNewElements[0] && TableOfContentsCommands.deleteGliederungsEbene(editor, allNewElements[0], false, true);
    }
    return Editor.node(editor, selectedElementPath.slice(0, selectedElementPath.length - 1)) as NodeEntry<Element>;
  }

  public static deleteGliederungsEbene(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    deleteContent?: boolean,
    deleteAll?: boolean,
  ): NodeEntry<Element> | null {
    const [element, path] = nodeEntry;

    const isFirstGliederungsebene = TableOfContentsCommands.isFirstGliederungsEbeneSibling(editor, nodeEntry);
    const newPath: Path = TableOfContentsCommands.changeMoveToPath(editor, nodeEntry, deleteContent);
    let deletePath = [...path];

    //move children and get back new path for delete
    if (!deleteContent) {
      deletePath = TableOfContentsCommands.moveGliederungsebenenFromTo(
        editor,
        path,
        newPath,
        TableOfContentsQueries.gliederungsEbenenQueries.gliederungsEbenen.map(
          (gliederungsEbene) => gliederungsEbene.type,
        ),
        !isFirstGliederungsebene,
        deleteAll,
      );
    }

    Transforms.delete(editor, { at: deletePath });

    //renumber at deletion point
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (gliederungsEbene) {
      const renumberChilds = getChildrenOfTypeAt(
        editor,
        [gliederungsEbene.type],
        deletePath.slice(0, deletePath.length - 1),
      );
      renumberChilds.length > 0 &&
        TableOfContentsCommands.renumberTypeWithTextAt(
          editor,
          gliederungsEbene.type,
          gliederungsEbene.title,
          renumberChilds[0][1],
        );
    }
    return Editor.node(
      editor,
      isFirstGliederungsebene ? newPath.map((x, y) => (y === path.length - 1 ? --x : x)) : newPath,
    ) as NodeEntry<Element>;
  }

  //returns new path of origin
  private static moveGliederungsebenenFromTo(
    editor: Editor,
    originPath: Path,
    targetPath: Path,
    types: string[],
    addToEnd?: boolean,
    deleteAll?: boolean,
  ): Path {
    //get sections only from first hierarchy level
    const sections: NodeEntry<Element>[] = getChildrenOfTypeAt(editor, types, originPath);

    if (sections.length === 0) return originPath;

    const [targetElement] = Editor.node(editor, targetPath);

    const [parentElement, parentPath] = Editor.parent(editor, originPath) as NodeEntry<Element>;

    let newOriginPath = [...originPath];

    // offset for move to front and change origin path if sections are moved to same hierarchy as origin before origin itself
    let offset = 2;
    if (JSON.stringify(parentPath) === JSON.stringify(targetPath) && !addToEnd) {
      newOriginPath = originPath.map((x, index) =>
        index === originPath.length - 1 ? originPath[originPath.length - 1] + sections.length : x,
      );
      if (parentElement.type === REGELUNGSTEXT_STRUCTURE.BODY) {
        offset = 0;
      }
    }

    //Add sections to target
    sections.forEach((sectionSibling, index) => {
      const [sectionElement] = sectionSibling;
      const newSectionPath = [
        ...targetPath,
        addToEnd ? (targetElement as Element).children.length + index : offset + index,
      ];
      Transforms.insertNodes(editor, sectionElement, { at: newSectionPath });
    });

    //Delete articles from source
    sections.forEach((sectionSibling, index) => {
      const [, sectionPath] = sectionSibling;
      const deletePath = [...newOriginPath];
      deletePath.push(sectionPath[sectionPath.length - 1] - index);
      Transforms.delete(editor, { at: deletePath });
    });
    if (!deleteAll) {
      //renumber moved parts at new position
      const renumberChilds = getChildrenOfTypeAt(editor, types, targetPath);
      const renumberElements = renumberChilds.reduce(
        (renumberElements, childEntry) =>
          renumberElements.find(([renumberElement]) => renumberElement.type === childEntry[0].type)
            ? renumberElements
            : renumberElements.concat([childEntry]),
        [] as NodeEntry<Element>[],
      );
      renumberElements.forEach(([renumberElement, renumberPath]) => {
        const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(
          renumberElement.type,
        );
        if (gliederungsEbene) {
          TableOfContentsCommands.renumberTypeWithTextAt(
            editor,
            gliederungsEbene.type,
            gliederungsEbene.title,
            renumberPath,
          );
        }
      });
    }
    TableOfContentsCommands.changeLastRefersTo(editor);
    return newOriginPath;
  }

  private static changeLastRefersTo(editor: Editor): void {
    const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
    if (einzelvorschrift) {
      const articles = getAllOfType(editor, einzelvorschrift.type);
      if (articles.length === 0) return;

      //last element refersTo change
      const lastArticle = articles[articles.length - 1];
      const [lastArticleElement, lastArticleElementPath] = lastArticle;
      if (lastArticleElement.refersTo === 'stammform') {
        const lastElementCopy = Object.assign({}, lastArticleElement);
        Transforms.delete(editor, { at: lastArticleElementPath });
        lastElementCopy.refersTo = 'geltungszeitregel';
        Transforms.insertNodes(editor, lastElementCopy, { at: lastArticleElementPath });
      }

      if (articles.length < 2) return;

      //penultimate element refersTo change
      const penultimateArticle = articles[articles.length - 2];
      const [penultimateArticleElement, penultimateArticleElementPath] = penultimateArticle;
      if (penultimateArticleElement.refersTo === 'geltungszeitregel') {
        const penultimateElementCopy = Object.assign({}, penultimateArticleElement);
        Transforms.delete(editor, { at: penultimateArticleElementPath });
        penultimateElementCopy.refersTo = 'stammform';
        Transforms.insertNodes(editor, penultimateElementCopy, { at: penultimateArticleElementPath });
      }
    }
  }

  private static renumberTypeWithTextAt(editor: Editor, type: string, text: string, at: Path): void {
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    if (!gliederungsEbene) return;
    let elements = [];
    // get all articles if renumber articles
    if (gliederungsEbene.isEinzelvorschrift) {
      elements = getAllOfType(editor, type);
    } else {
      //get siblings at given path
      elements = getSiblings(editor, type, at);
    }
    if (elements.length === 0) return;

    elements.forEach((element, index) => {
      const [, elementPath] = element;
      const [num] = Editor.nodes<Element>(editor, {
        at: elementPath,
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.NUM,
      });
      const [numElement, numPath] = num;

      // find correct num-Element if additional Wrapper e.g. comments are in number
      const textWrapper = numElement.children.filter(
        (element) => element.type === REGELUNGSTEXT_STRUCTURE.TEXTWRAPPER && element.children[0].text !== '',
      );
      const textAndWrapper = textWrapper
        .flatMap((wrapper) => {
          const text = wrapper.children[0].text;
          if (!text) return;
          const lastCharacter = text.substring(text.length - 2);
          const isNumber = parseInt(lastCharacter);
          if (isNumber) {
            return { text: text.substring(0, text.length - 2), wrapper: wrapper };
          }
        })
        .find((wrapperElement) => wrapperElement !== undefined);
      const numIndex = textAndWrapper?.wrapper
        ? numElement.children.findIndex((element) => JSON.stringify(element) === JSON.stringify(textAndWrapper.wrapper))
        : 1;
      if (textAndWrapper) {
        text = textAndWrapper.text;
      }

      Transforms.insertText(editor, `${text} ${++index}`, { at: [...numPath, numIndex] });

      // Set name attribute of Marker Element
      const markerIndex = numElement.children.findIndex((element) => element.type === STRUCTURE_ELEMENTS.MARKER);
      Transforms.setNodes(editor, { name: `${index}`, children: [] }, { at: [...numPath, markerIndex] });
      AuthorialNoteCommands.renumberAuthorialNotes(editor);
    });
  }

  private static getElementForType(type: string): Element | undefined {
    if (TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type)?.isEinzelvorschrift) {
      return TableOfContentsCommands.createArticle();
    }
    return TableOfContentsCommands.createGliederungsEbene(type);
  }

  private static createArticle(): Element {
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    const contentMarker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const contentNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [contentMarker, createTextWrapper('')]);
    const paragraph = createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [contentNum, content]);
    const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('')]);
    const articleMarker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const articleNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [articleMarker, createTextWrapper('')]);
    const article = createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [articleNum, heading, paragraph]);
    article.period = '#meta-1_geltzeiten-1_geltungszeitgr-1';
    return article;
  }

  private static createGliederungsEbene(type: string): Element {
    const subsectionMarker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [subsectionMarker, createTextWrapper('')]);
    const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('')]);
    const subsection = createElementOfType(type, [num, heading]);
    return subsection;
  }
}
