// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useState } from 'react';
import { Editor, Location, NodeEntry, Operation } from 'slate';
import { ReactEditor } from 'slate-react';
import {
  IPluginContextMenuProps,
  IPluginNavigationDialogProps,
  Plugin,
  PluginProps,
  useEditorOnFocusState,
  useHdrCheckStartetState,
} from '../../../../plugin-core';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsQueries as Queries } from './TableOfContentsQueries';
import { TableOfContentsContextMenu } from './TableOfContentsContextMenu';
import { useTableOfContentsDispatch, useTableOfContentsState } from '../../../../plugin-core/editor-store/workspace';
import {
  TableOfContentsDataSlice,
  TableOfContentsManager,
  useTableOfContentsDataDispatch,
  useTableOfContentsDataState,
} from '../../../../Editor';
import { ReducersMapObject } from '@reduxjs/toolkit';
import { Element } from '../../../../../interfaces';
import { NodeDragEventParams } from 'rc-tree/lib/contextTypes';
import { EventDataNode, Key } from 'rc-tree/lib/interface';
import { DataNode } from 'antd/lib/tree';
import { ItemType } from 'antd/lib/menu/interface';
import { Checkbox } from 'antd';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';
import { AllowDropProps } from '@lea/ui';

export class TableOfContentsPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { apply } = editor;

    // Check if structure has changed
    editor.apply = (op: Operation) => {
      apply(op);
      if (
        ['remove_node', 'insert_node', 'set_node', 'split_node', 'move_node', 'insert_text', 'remove_text'].some(
          (x) => x === op.type,
        )
      ) {
        this.setTableOfContentsData(Commands.getContentTableData(editor, this.hdrCheckStartet));
      }
    };

    return editor;
  }

  private setTableOfContentsData: (dataNodes: DataNode[]) => void = (_dataNodes: DataNode[]) => {};

  private hdrCheckStartet: boolean = false;

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public set contextMenuItemTypes(itemTypes: ItemType[] | null) {
    this._contextMenuItemTypes = itemTypes;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  public useEffect(editor: ReactEditor): void {
    this.setTableOfContentsData(Commands.getContentTableData(editor, this.hdrCheckStartet));
  }

  public NavigationDialog = ({ editor }: IPluginNavigationDialogProps): ReactElement => {
    const editorOnFocus = useEditorOnFocusState();
    const { tableOfContentsData } = useTableOfContentsDataState(this.editorDocument.index);

    const [onDragging, setOnDragging] = useState<boolean>(false);

    const nodeDraggable = (dataNode: DataNode): boolean => {
      const key = dataNode.key.toString();
      const path: Location = key.split('-').map((x) => parseInt(x, 10));
      return !this.editorDocument.isReadonly && Queries.nodeDraggable(editor, path, onDragging);
    };

    const allowDrop = ({ dropNode }: AllowDropProps) => {
      const key = dropNode.key.toString();
      const path: Location = key.split('-').map((x) => parseInt(x, 10));
      return !this.editorDocument.isReadonly && Queries.nodeDroppable(editor, path);
    };

    const canAddChildren = (dataNode: DataNode) => {
      const key = dataNode.key.toString();
      const path: Location = key.split('-').map((x) => parseInt(x, 10));
      return Queries.canDropAsChild(editor, path) || Queries.canDropToExistingChildren(editor, path);
    };

    const onDrop = (
      info: NodeDragEventParams<DataNode, HTMLDivElement> & {
        dragNode: EventDataNode<DataNode>;
        dragNodesKeys: Key[];
        dropPosition: number;
        dropToGap: boolean;
      },
    ) => {
      if (!info.node.dragOver && !info.node.dragOverGapBottom && !info.node.dragOverGapTop) return;
      const dragNodeKey = (info.dragNode as DataNode).key.toString();
      const dragNodePath: Location = dragNodeKey.split('-').map((x) => parseInt(x, 10));
      const dragNodeEntry = Editor.node(editor, dragNodePath) as NodeEntry<Element>;
      let dropNodeKey = (info.node as DataNode).key.toString();
      let dropNodePath: Location = dropNodeKey.split('-').map((x) => parseInt(x, 10));
      let dropNodeEntry = Editor.node(editor, dropNodePath) as NodeEntry<Element>;
      const dropToChild =
        !info.dropToGap &&
        info.node.children &&
        info.node.children.length > 0 &&
        Queries.canDropToExistingChildren(editor, dropNodePath)
          ? (info.node.children[0] as DataNode)
          : null;

      if (dropToChild) {
        dropNodeKey = dropToChild.key.toString();
        dropNodePath = dropNodeKey.split('-').map((x) => parseInt(x, 10));
        dropNodeEntry = Editor.node(editor, dropNodePath) as NodeEntry<Element>;
      }
      const dropAsChild =
        (info.node.children && info.node.children.length === 0 && Queries.canDropAsChild(editor, dropNodePath)) ||
        false;
      if (!dropAsChild && !dropToChild && dragNodeEntry[0].type !== dropNodeEntry[0].type) return;
      setTimeout(
        () =>
          Commands.moveElement(
            editor,
            dragNodeEntry,
            dropNodeEntry,
            !dropToChild && info.dropPosition >= 0,
            dropAsChild,
          ) && AuthorialNoteCommands.renumberAuthorialNotes(editor),
      );
    };

    return (
      <TableOfContentsManager
        editor={editor}
        index={this.editorDocument.index}
        isReadonly={this.editorDocument.isReadonly}
        nodeDraggable={nodeDraggable}
        onDrop={onDrop}
        tableOfContentsData={tableOfContentsData}
        onDragging={setOnDragging}
        canAddChildren={canAddChildren}
        allowDrop={allowDrop}
        isAvailable={editorOnFocus.index === this.editorDocument.index}
        documentTitle={this.editorDocument.documentTitle}
      />
    );
  };

  public ViewTools = (): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const { visible } = useTableOfContentsState();
    const { toggelTableOfContents } = useTableOfContentsDispatch();

    if (editorOnFocus.index === this.editorDocument.index) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox onChange={toggelTableOfContents} checked={visible} className="check-all">
              Strukturanzeige
            </Checkbox>
          ),
          key: 'viewTools-regelungstext-stammform-tableOfContents',
          title: 'Strukturanzeige',
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { setTableOfContentsData } = useTableOfContentsDataDispatch(this.editorDocument.index);
    const { hdrCheckStartet } = useHdrCheckStartetState();

    useEffect(() => {
      this.setTableOfContentsData = setTableOfContentsData;
    }, []);

    useEffect(() => {
      this.hdrCheckStartet = hdrCheckStartet;
    }, [hdrCheckStartet]);
    return <></>;
  };

  public reducer(): ReducersMapObject {
    const slice = TableOfContentsDataSlice(this.editorDocument.index);
    return { [slice.name]: slice.reducer };
  }

  public ContextMenu = ({ keyName, editor }: IPluginContextMenuProps) =>
    TableOfContentsContextMenu({
      editor,
      keyName,
      plugin: this,
    });
}
