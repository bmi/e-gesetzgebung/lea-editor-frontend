// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useTranslation } from 'react-i18next';

import { IPluginContextMenuProps, useEditorOnFocusState } from '../../../../plugin-core';
import React, { ReactElement, useMemo } from 'react';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsQueries as Queries, TableOfContentsQueries } from './TableOfContentsQueries';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import {
  useContextMenuChange,
  useContextMenuNodeEntryState,
} from '../../../../plugin-core/editor-store/workspace/contextMenu';
import { ItemType } from 'antd/lib/menu/interface';
import { TableOfContentsPlugin } from './TableOfContentsPlugin';
import { NodeEntry } from 'slate';
import { Element } from '../../../../../interfaces';
import { getSiblings, isFirstOfType } from '../../../../plugin-core/utils';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';

interface ITableOfContentsContextMenuProps extends IPluginContextMenuProps {
  plugin: TableOfContentsPlugin;
}

export const TableOfContentsContextMenu = ({
  keyName,
  plugin,
  editor,
}: ITableOfContentsContextMenuProps): ReactElement => {
  const { t } = useTranslation();
  const { setTargetNodeEntry } = useContextMenuChange();
  const editorOnFocus = useEditorOnFocusState();
  const { nodeEntry } = useContextMenuNodeEntryState();

  const renderType = (type: string): string => {
    let name = '';
    const gliederungsEbene = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(type);
    const typeSplit = type.split(':');
    type = typeSplit[typeSplit.length - 1];
    if (gliederungsEbene) {
      if (gliederungsEbene.isEinzelvorschrift) {
        name = t(`lea.contextMenu.tableOfContents.options.mantelform.${type}`);
      } else {
        name = t(`lea.contextMenu.tableOfContents.options.types.${type}`);
      }
    }
    return name;
  };

  const onSubMenuTitleClick = (e: React.MouseEvent<HTMLElement, MouseEvent> | React.KeyboardEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const getItemTypes = (nodeEntry: NodeEntry<Element>): ItemType[] => {
    const addElement = Queries.getOptionsForAddElement(editor, nodeEntry, setTargetNodeEntry);
    const deleteElement = Queries.getOptionForDeleteElement(editor, nodeEntry, setTargetNodeEntry);
    const draggable = Queries.nodeDraggable(editor, nodeEntry[1]);
    const newItemTypes: ItemType[] = [];
    if (addElement.length > 0) {
      const children: ItemType[] = [];
      addElement.forEach(({ type, onClick }) => {
        children.push({
          key: `${keyName}-tableOfContents-regelungstext-mantelform-addElement-${type.replace(':', '-')}`,
          title: renderType(type),
          label: renderType(type),
          onClick,
        });
      });
      newItemTypes.push({
        key: `${keyName}-tableOfContents-regelungstext-mantelform-addElement-group`,
        label: t('lea.contextMenu.tableOfContents.addElement'),
        popupClassName: 'submenu-container',
        children,
      });
    }
    if (draggable) {
      if (addElement.length > 0) {
        newItemTypes.push({ type: 'divider' });
      }
      newItemTypes.push({
        key: `${keyName}-tableOfContents-regelungstext-mantelform-dragElement`,
        title: t('lea.contextMenu.tableOfContents.drag.title', {
          type: renderType(nodeEntry[0].type),
        }),
        label: t('lea.contextMenu.tableOfContents.drag.title', {
          type: renderType(nodeEntry[0].type),
        }),
        onTitleClick: (e) => onSubMenuTitleClick(e.domEvent),
        popupClassName: 'submenu-container',
        children: [
          {
            key: `${keyName}-tableOfContents-regelungstext-mantelform-dragUp`,
            title: t('lea.contextMenu.tableOfContents.drag.up', {
              type: renderType(nodeEntry[0].type),
            }),
            label: t('lea.contextMenu.tableOfContents.drag.up', {
              type: renderType(nodeEntry[0].type),
            }),
            disabled: isFirstOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]),
            onClick: () => {
              const newNodeEntry = Commands.moveOneUp(editor, nodeEntry);
              setTargetNodeEntry(newNodeEntry);
              AuthorialNoteCommands.renumberAuthorialNotes(editor);
            },
          },
          {
            key: `${keyName}-tableOfContents-regelungstext-mantelform-dragDown`,
            title: t('lea.contextMenu.tableOfContents.drag.down', {
              type: renderType(nodeEntry[0].type),
            }),
            label: t('lea.contextMenu.tableOfContents.drag.down', {
              type: renderType(nodeEntry[0].type),
            }),
            disabled:
              nodeEntry[1][nodeEntry[1].length - 1] >=
              getSiblings(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]).length - 2,
            onClick: () => {
              const newNodeEntry = Commands.moveOneDown(editor, nodeEntry);
              setTargetNodeEntry(newNodeEntry);
              AuthorialNoteCommands.renumberAuthorialNotes(editor);
            },
          },
          {
            key: `${keyName}-tableOfContents-regelungstext-mantelform-dragToTop`,
            title: t('lea.contextMenu.tableOfContents.drag.toTop', {
              type: renderType(nodeEntry[0].type),
            }),
            label: t('lea.contextMenu.tableOfContents.drag.toTop', {
              type: renderType(nodeEntry[0].type),
            }),
            disabled: isFirstOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, nodeEntry[1]),
            onClick: () => {
              const newNodeEntry = Commands.moveAsFirstOfType(editor, nodeEntry);
              setTargetNodeEntry(newNodeEntry);
              AuthorialNoteCommands.renumberAuthorialNotes(editor);
            },
          },
        ],
      });
    }
    if (deleteElement) {
      if (addElement.length > 0 || draggable) {
        newItemTypes.push({ type: 'divider' });
      }
      const children: ItemType[] = [];
      children.push({
        key: `${keyName}-tableOfContents-regelungstext-mantelform-deleteElement`,
        title: renderType(deleteElement.type),
        label: renderType(deleteElement.type),
        onClick: deleteElement.onClick,
      });
      newItemTypes.push({
        key: `${keyName}-tableOfContents-regelungstext-mantelform-deleteElement-group`,
        label: t('lea.contextMenu.tableOfContents.deleteElement.title'),
        popupClassName: 'submenu-container',
        children,
      });
    }
    return newItemTypes;
  };

  useMemo(() => {
    if (nodeEntry && !plugin.editorDocument.isReadonly && plugin.editorDocument.index === editorOnFocus.index) {
      plugin.contextMenuItemTypes = getItemTypes(nodeEntry);
    } else {
      plugin.contextMenuItemTypes = null;
    }
  }, [nodeEntry]);

  return <></>;
};
