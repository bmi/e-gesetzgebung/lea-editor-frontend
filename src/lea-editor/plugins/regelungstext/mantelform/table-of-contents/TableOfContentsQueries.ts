// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseEditor, Editor, NodeEntry, Path } from 'slate';
import { ContextMenuOption, Element } from '../../../../../interfaces';
import { GliederungsEbenenQueries, getAllOfType } from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_MANTELFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY } from '../../RegelungstextStructure';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';
export class TableOfContentsQueries {
  public static gliederungsEbenenQueries = new GliederungsEbenenQueries(
    REGELUNGSTEXT_MANTELFORM_GLIEDERUNGSEBENEN_IN_HIERARCHY,
  );

  public static nodeDraggable(editor: Editor, path: Path): boolean {
    try {
      const [node] = Editor.node(editor, path) as NodeEntry<Element>;
      const einzelvorschrift = TableOfContentsQueries.gliederungsEbenenQueries.getEinzelvorschrift();
      return node.type === einzelvorschrift?.type && node.refersTo !== 'geltungszeitregel';
    } catch (e) {
      return false;
    }
  }

  public static getOptionsForAddElement(
    editor: BaseEditor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption[] {
    const [element] = nodeEntry;
    const options: ContextMenuOption[] = [];
    const gliederungsEbenen = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (gliederungsEbenen?.isEinzelvorschrift && element.refersTo !== 'geltungszeitregel') {
      options.push({
        type: gliederungsEbenen.type,
        onClick: () => {
          const newNodeEntry = Commands.addBelow(editor, nodeEntry, gliederungsEbenen.type);
          setTargetNodeEntry(newNodeEntry);
        },
      });
    }
    return options;
  }

  public static getOptionForDeleteElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
  ): ContextMenuOption | null {
    const [element, path] = nodeEntry;
    const gliederungsEbenen = TableOfContentsQueries.gliederungsEbenenQueries.getGliederungsEbeneFromType(element.type);
    if (
      gliederungsEbenen?.isEinzelvorschrift &&
      element.refersTo === 'hauptaenderung' &&
      getAllOfType(editor, gliederungsEbenen.type).length > 1
    ) {
      return {
        type: gliederungsEbenen.type,
        onClick: () => {
          const newNodeEntry = Commands.handleDeleteArticle(editor, path);
          setTargetNodeEntry(newNodeEntry);
          AuthorialNoteCommands.renumberAuthorialNotes(editor);
        },
      };
    }
    return null;
  }
}
