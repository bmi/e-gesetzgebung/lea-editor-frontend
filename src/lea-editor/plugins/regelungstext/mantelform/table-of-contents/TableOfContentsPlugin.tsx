// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useReducer } from 'react';
import { Editor, Location, NodeEntry, Operation } from 'slate';
import { ReactEditor } from 'slate-react';
import { DataNode } from 'antd/lib/tree';
import {
  IPluginContextMenuProps,
  IPluginNavigationDialogProps,
  Plugin,
  PluginProps,
  useEditorOnFocusState,
} from '../../../../plugin-core';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsContextMenu } from './TableOfContentsContextMenu';
import { useTableOfContentsDispatch, useTableOfContentsState } from '../../../../plugin-core/editor-store/workspace';
import { Element } from '../../../../../interfaces';
import { NodeDragEventParams } from 'rc-tree/lib/contextTypes';
import { EventDataNode, Key } from 'rc-tree/lib/interface';
import {
  TableOfContentsDataSlice,
  TableOfContentsManager,
  useTableOfContentsDataDispatch,
  useTableOfContentsDataState,
} from '../../../../Editor';
import { ReducersMapObject } from '@reduxjs/toolkit';

import { TableOfContentsQueries as Queries } from './TableOfContentsQueries';
import { ItemType } from 'antd/lib/menu/interface';
import { Checkbox } from 'antd';
import { AuthorialNoteCommands } from '../../../authorialNote/AuthorialNoteCommands';

export class TableOfContentsPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { apply } = editor;

    // Check if structure has changed
    editor.apply = (op: Operation) => {
      apply(op);
      if (
        ['remove_node', 'insert_node', 'split_node', 'move_node', 'insert_text', 'remove_text'].some(
          (x) => x === op.type,
        )
      ) {
        this.setTableOfContentsData(Commands.getContentTableData(editor));
      }
    };

    return editor;
  }

  private setTableOfContentsData: (dataNodes: DataNode[]) => void = (_dataNodes: DataNode[]) => {};

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public set contextMenuItemTypes(itemTypes: ItemType[] | null) {
    this._contextMenuItemTypes = itemTypes;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  public useEffect(editor: ReactEditor): void {
    this.setTableOfContentsData(Commands.getContentTableData(editor));
  }

  public NavigationDialog = ({ editor }: IPluginNavigationDialogProps): ReactElement => {
    const editorOnFocus = useEditorOnFocusState();
    const { tableOfContentsData } = useTableOfContentsDataState(this.editorDocument.index);
    const [, forceUpdate] = useReducer((x: number) => x + 1, 0);

    const nodeDraggable = (dataNode: DataNode): boolean => {
      const key = dataNode.key.toString();
      const path: Location = key.split('-').map((x) => parseInt(x, 10));
      return !this.editorDocument.isReadonly && Queries.nodeDraggable(editor, path);
    };

    const onDrop = (
      info: NodeDragEventParams<DataNode, HTMLDivElement> & {
        dragNode: EventDataNode<DataNode>;
        dragNodesKeys: Key[];
        dropPosition: number;
        dropToGap: boolean;
      },
    ) => {
      const dragNodeKey = (info.dragNode as DataNode).key.toString();
      const dragNodePath: Location = dragNodeKey.split('-').map((x) => parseInt(x, 10));
      const dragNodeEntry = Editor.node(editor, dragNodePath) as NodeEntry<Element>;
      let dropNodeKey = (info.node as DataNode).key.toString();
      const dropToChild =
        !info.dropToGap && info.node.children && info.node.children.length > 0
          ? (info.node.children[0] as DataNode)
          : null;
      if (dropToChild) {
        dropNodeKey = dropToChild.key.toString();
      }
      const dropNodePath: Location = dropNodeKey.split('-').map((x) => parseInt(x, 10));
      const dropNodeEntry = Editor.node(editor, dropNodePath) as NodeEntry<Element>;
      Commands.moveElement(editor, dragNodeEntry, dropNodeEntry, !dropToChild && info.dropPosition >= 0);
      AuthorialNoteCommands.renumberAuthorialNotes(editor);
      setTimeout(() => forceUpdate());
    };

    return (
      <TableOfContentsManager
        editor={editor}
        index={this.editorDocument.index}
        nodeDraggable={nodeDraggable}
        onDrop={onDrop}
        isReadonly={this.editorDocument.isReadonly}
        tableOfContentsData={tableOfContentsData}
        isAvailable={editorOnFocus.index === this.editorDocument.index}
        documentTitle={this.editorDocument.documentTitle}
      />
    );
  };

  public ViewTools = (): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const { visible } = useTableOfContentsState();
    const { toggelTableOfContents } = useTableOfContentsDispatch();

    if (editorOnFocus.index === this.editorDocument.index) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox onChange={toggelTableOfContents} checked={visible} className="check-all">
              Strukturanzeige
            </Checkbox>
          ),
          key: 'viewTools-regelungstext-mantelform-tableOfContents',
          title: 'Strukturanzeige',
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { setTableOfContentsData } = useTableOfContentsDataDispatch(this.editorDocument.index);

    useEffect(() => {
      this.setTableOfContentsData = setTableOfContentsData;
    }, []);
    return <></>;
  };

  public reducer(): ReducersMapObject {
    const slice = TableOfContentsDataSlice(this.editorDocument.index);
    return { [slice.name]: slice.reducer };
  }

  public ContextMenu = ({ keyName, editor }: IPluginContextMenuProps) =>
    TableOfContentsContextMenu({
      editor,
      keyName,
      plugin: this,
    });
}
