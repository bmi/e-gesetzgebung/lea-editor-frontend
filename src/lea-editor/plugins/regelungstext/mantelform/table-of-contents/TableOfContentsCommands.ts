// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Node, NodeEntry, Path, Transforms } from 'slate';
import { DataNode } from 'antd/lib/tree';

import { Element, TextWrapper } from '../../../../../interfaces';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import {
  createElementOfType,
  createTextWrapper,
  getAllOfType,
  getHeadingTextFromNode,
  getLastLocationAt,
  getSiblings,
} from '../../../../plugin-core/utils';
import { TableOfContentsQueries } from './TableOfContentsQueries';

export class TableOfContentsCommands {
  public static getContentTableData(e: Editor): DataNode[] {
    const treeData: DataNode[] = [];
    if (e.children && e.children.length > 0) {
      (e.children as Element[]).forEach((n: Element, index: number) => {
        TableOfContentsCommands.createContentTable(n, treeData, [index]);
      });
    }
    return treeData;
  }

  private static createContentTable(node: Element, array: DataNode[] | undefined, path: number[]) {
    if (!node.children || !array) {
      return;
    }

    const key = path.join('-');
    if (TableOfContentsQueries.gliederungsEbenenQueries.gliederungsEbenen.some((x) => x.type === node.type)) {
      if (
        node.children.length > 1 &&
        (node.children[0] as Element).type === REGELUNGSTEXT_STRUCTURE.NUM &&
        (node.children[1] as Element).type === REGELUNGSTEXT_STRUCTURE.HEADING
      ) {
        const numText = Node.string(node.children[0]);
        const headingText = getHeadingTextFromNode(node.children[1] as Element);

        array.push({
          title: `${numText} ${headingText}`,
          key,
          children: [],
        });
      }
    }

    const i = array.length - 1;
    node.children.forEach((n: Element | TextWrapper, index: number) => {
      TableOfContentsCommands.createContentTable(
        n as Element,
        array[i]?.children && key.startsWith(array[i].key.toString()) ? array[i].children : array,
        [...path, index],
      );
    });
  }

  public static addAbove(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    const [, path] = nodeEntry;
    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }
    Transforms.insertNodes(editor, newElement, { at: path });
    Transforms.select(editor, getLastLocationAt(editor, path));
    if (type === REGELUNGSTEXT_STRUCTURE.ARTICLE) {
      TableOfContentsCommands.renumberTypeWithTextAt(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, 'Artikel', path);
    }
    return Editor.node(editor, path) as NodeEntry<Element>;
  }

  public static addBelow(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    newElement?: Element,
  ): NodeEntry<Element> | null {
    const [, path] = nodeEntry;

    if (!newElement) {
      newElement = TableOfContentsCommands.getElementForType(type);
    }
    if (!newElement) {
      return null;
    }

    const newPath: Path = path.map((x, y) => (y === path.length - 1 ? ++x : x));
    Transforms.insertNodes(editor, newElement, { at: newPath });
    Transforms.select(editor, getLastLocationAt(editor, newPath));
    if (type === REGELUNGSTEXT_STRUCTURE.ARTICLE) {
      TableOfContentsCommands.changeLastRefersTo(editor);
      TableOfContentsCommands.renumberTypeWithTextAt(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, 'Artikel', newPath);
    }
    return Editor.node(editor, newPath) as NodeEntry<Element>;
  }

  public static moveAsFirstOfType = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element] = from;
    const allOfType = getAllOfType(editor, element.type);
    return TableOfContentsCommands.moveElement(editor, from, allOfType[0], false);
  };

  public static moveAsLastOfType = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element] = from;
    const allOfType = getAllOfType(editor, element.type);
    return TableOfContentsCommands.moveElement(editor, from, allOfType[allOfType.length - 1], true);
  };

  public static moveOneUp = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element, elementPath] = from;
    const allOfType = getAllOfType(editor, element.type);
    const previousElement = allOfType[allOfType.findIndex((node) => node[1].toString() === elementPath.toString()) - 1];
    return TableOfContentsCommands.moveElement(editor, from, previousElement, false);
  };

  public static moveOneDown = (editor: Editor, from: NodeEntry<Element>): NodeEntry<Element> | null => {
    const [element, elementPath] = from;
    const allOfType = getAllOfType(editor, element.type);
    const previousElement = allOfType[allOfType.findIndex((node) => node[1].toString() === elementPath.toString()) + 1];
    return TableOfContentsCommands.moveElement(editor, from, previousElement, true);
  };

  public static moveElement(
    editor: Editor,
    from: NodeEntry<Element>,
    to: NodeEntry<Element>,
    below: boolean,
  ): NodeEntry<Element> | null {
    const [fromNode, fromNodePath] = from;
    const [toNode, toNodePath] = to;
    if (fromNode.GUID === toNode.GUID) return null;
    const [, parentPath] = Editor.above(editor, { at: toNodePath }) as NodeEntry<Element>;
    Transforms.removeNodes(editor, { at: fromNodePath });
    const [...newToNodeEntry] = Editor.nodes<Element>(editor, {
      at: parentPath,
      match: (n) => (n as Element).GUID === toNode.GUID,
    });
    if (newToNodeEntry.length < 1) return null;
    if (below) {
      return TableOfContentsCommands.addBelow(editor, newToNodeEntry[0], fromNode.type, fromNode);
    } else {
      return TableOfContentsCommands.addAbove(editor, newToNodeEntry[0], fromNode.type, fromNode);
    }
  }

  private static getElementForType(type: string): Element | undefined {
    if (type === REGELUNGSTEXT_STRUCTURE.ARTICLE) {
      return TableOfContentsCommands.createArticle();
    }
    return undefined;
  }

  public static handleDeleteArticle(editor: Editor, path: Path): NodeEntry<Element> | null {
    Transforms.delete(editor, { at: path });
    const newPath = path.slice(0, path.length - 1);
    TableOfContentsCommands.changeLastRefersTo(editor);
    TableOfContentsCommands.renumberTypeWithTextAt(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE, 'Artikel', newPath);
    return Editor.node(editor, newPath) as NodeEntry<Element>;
  }

  private static changeLastRefersTo(editor: Editor): void {
    const articles = getAllOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE);
    if (articles.length === 0) return;

    //last element refersTo change
    const lastArticle = articles[articles.length - 1];
    const [lastArticleElement, lastArticleElementPath] = lastArticle;
    if (lastArticleElement.refersTo === 'mantelform') {
      const lastElementCopy = Object.assign({}, lastArticleElement);
      Transforms.delete(editor, { at: lastArticleElementPath });
      lastElementCopy.refersTo = 'geltungszeitregel';
      Transforms.insertNodes(editor, lastElementCopy, { at: lastArticleElementPath });
    }

    if (articles.length < 2) return;

    //penultimate element refersTo change
    const penultimateArticle = articles[articles.length - 2];
    const [penultimateArticleElement, penultimateArticleElementPath] = penultimateArticle;
    if (penultimateArticleElement.refersTo === 'geltungszeitregel') {
      const penultimateElementCopy = Object.assign({}, penultimateArticleElement);
      Transforms.delete(editor, { at: penultimateArticleElementPath });
      penultimateElementCopy.refersTo = 'mantelform';
      Transforms.insertNodes(editor, penultimateElementCopy, { at: penultimateArticleElementPath });
    }
  }

  private static renumberTypeWithTextAt(editor: Editor, type: string, text: string, at: Path): void {
    let elements = [];
    // get all articles if renumber articles
    if (type === REGELUNGSTEXT_STRUCTURE.ARTICLE) {
      elements = getAllOfType(editor, REGELUNGSTEXT_STRUCTURE.ARTICLE);
    } else {
      //get siblings at given path
      elements = getSiblings(editor, type, at);
    }
    if (elements.length === 0) return;

    elements.forEach((element, index) => {
      const [, elementPath] = element;
      const [num] = Editor.nodes<Element>(editor, {
        at: elementPath,
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.NUM,
      });
      const [numElement, numPath] = num;
      // find correct num-Element if additional Wrapper e.g. comments are in number
      const textWrapper = numElement.children.filter(
        (element) => element.type === REGELUNGSTEXT_STRUCTURE.TEXTWRAPPER && element.children[0].text !== '',
      );
      const textAndWrapper = textWrapper
        .flatMap((wrapper) => {
          const text = wrapper.children[0].text;
          if (text) {
            const lastCharacter = text.substring(text.length - 2);
            const isNumber = parseInt(lastCharacter);
            if (isNumber) {
              return { text: text.substring(0, text.length - 2), wrapper: wrapper };
            }
          }
          return undefined;
        })
        .find((wrapperElement) => wrapperElement !== undefined);
      const numIndex = textAndWrapper?.wrapper
        ? numElement.children.findIndex((element) => JSON.stringify(element) === JSON.stringify(textAndWrapper.wrapper))
        : 1;
      if (textAndWrapper) {
        text = textAndWrapper.text;
      }
      Transforms.insertText(editor, `${text} ${index + 1}`, { at: [...numPath, numIndex] });

      // Set name attribute of Marker Element
      const markerIndex = numElement.children.findIndex((element) => element.type === REGELUNGSTEXT_STRUCTURE.MARKER);
      Transforms.setNodes(editor, { name: `${index + 1}`, children: [] }, { at: [...numPath, markerIndex] });
    });
  }

  private static createArticle(): Element {
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const [point, intro] = TableOfContentsCommands.createAenderungsbefehl();
    const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);
    const listNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const paragraph = createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [listNum, list]);
    const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('')]);

    const articleNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const article = createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [articleNum, heading, paragraph]);
    article.refersTo = 'hauptaenderung';
    return article;
  }

  private static createAenderungsbefehl(): Element[] {
    const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, [createTextWrapper('')]);
    const modP = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [modP]);
    const pointMarker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const pointNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [pointMarker, createTextWrapper('')]);
    const point = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [pointNum, content]);
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
    return [point, intro];
  }
}
