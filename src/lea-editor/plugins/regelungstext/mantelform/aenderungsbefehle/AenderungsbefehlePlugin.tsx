// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../../plugin-core';

import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { AenderungsbefehleCommands as Commands } from './AenderungsbefehleCommands';
import { AenderungsbefehleQueries as Queries } from './AenderungsbefehleQueries';

import {
  Unit,
  isFirstPosition,
  isFirstSiblingOfType,
  isLastPosition,
  isEnter,
  getSiblings,
  isWithinNodeOfType,
  isListCommand,
  isLastSiblingOfType,
  hasNodeOfType,
} from '../../../../plugin-core/utils';
import { Element } from '../../../../../interfaces';
import { STRUCTURE_ELEMENTS } from '../../../../plugin-core/utils/StructureElements';
import { isGZR } from '../../../../plugin-core/utils/DocumentUtils';
export class AenderungsbefehlePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;
    if (this.editorDocument.isReadonly) {
      return editor;
    }
    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      const previousPointPath = Queries.getPreviousPointPath(editor);
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (
        Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, previousPointPath) <= 1 &&
        getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT).length === 1
      ) {
        return;
      } else if (
        isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT) &&
        isFirstPosition(editor) &&
        !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT)
      ) {
        Commands.mergePoints(editor);
      } else if (
        isFirstPosition(editor) &&
        getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT).length === 1 &&
        !Queries.hasList(editor)
      ) {
        Commands.deleteList(editor);
      }
    };

    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isEnter(event) && !isLastPosition(editor)) {
      event.preventDefault();
      return false;
    }

    if (isEnter(event) && !hasNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.MOD)) {
      event.preventDefault();
      return false;
    }

    if (isEnter(event) && isLastPosition(editor)) {
      if (!Queries.isEmpty(editor) && !Queries.hasList(editor)) {
        Commands.insertPoint(editor);
      } else if (
        Queries.isEmpty(editor) &&
        Queries.getListItems(editor).length > 2 &&
        isLastSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT)
      ) {
        Commands.closeList(editor);
      }
      event.preventDefault();
      return false;
    }

    if (isListCommand(event) && Queries.isEmpty(editor)) {
      Commands.insertListeMitUntergliederung(editor);
      event.preventDefault();
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly && !isGZR(props.editor, elementPath) ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else if (elementType === STRUCTURE_ELEMENTS.TEXTWRAPPER) {
      const { startQuote, endQuote } = Queries.getQuotes(props.editor, elementPath);
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {startQuote ?? ''}
          {props.children}
          {endQuote ?? ''}
        </div>
      );
    }
    return null;
  };
}
