// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Path } from 'slate';

import { Element } from '../../../../../interfaces';
import {
  getLowestNodeAt,
  getSiblings,
  hasNodeOfType,
  isOfType,
  isWithinTypeHierarchy,
} from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { STRUCTURE_ELEMENTS } from '../../../../plugin-core/utils/StructureElements';

export class AenderungsbefehleQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    let isEditable = false;
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    const isModElement = type ? type === REGELUNGSTEXT_STRUCTURE.MOD : isOfType(editor, REGELUNGSTEXT_STRUCTURE.MOD);
    const hasModElement = hasNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.MOD, path);
    const currentPath = path || editor.selection?.anchor.path || [];
    if (isPElement && !hasModElement) {
      isEditable =
        (!isOfType(editor, REGELUNGSTEXT_STRUCTURE.MOD, currentPath) &&
          isWithinTypeHierarchy(editor, [REGELUNGSTEXT_STRUCTURE.LIST, REGELUNGSTEXT_STRUCTURE.PARAGRAPH], path)) ||
        isWithinTypeHierarchy(editor, [REGELUNGSTEXT_STRUCTURE.CONTENT, REGELUNGSTEXT_STRUCTURE.PARAGRAPH], path);
    } else if (isModElement) {
      isEditable = isWithinTypeHierarchy(
        editor,
        [
          REGELUNGSTEXT_STRUCTURE.P,
          REGELUNGSTEXT_STRUCTURE.CONTENT,
          REGELUNGSTEXT_STRUCTURE.LIST,
          REGELUNGSTEXT_STRUCTURE.PARAGRAPH,
        ],
        path,
      );
    }
    return isEditable;
  }

  public static getListLevel(editor: Editor, type: string, location: Path): number {
    const result: NodeEntry<Ancestor>[] = [];
    let above = Editor.above(editor, { at: location, match: (n) => (n as Element).type === type, mode: 'lowest' });

    while (above) {
      result.push(above as NodeEntry<Element>);
      const [, path] = above as NodeEntry<Ancestor>;
      above = Editor.above(editor, { at: path, match: (n) => (n as Element).type === type, mode: 'lowest' });
    }

    return result.length + 1;
  }

  public static getPreviousPointPath(editor: Editor): Path {
    const previousPoint = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });

    if (!previousPoint) {
      return [];
    }

    const [, previousPointPath] = previousPoint;

    return previousPointPath;
  }

  public static getListItems(editor: Editor): NodeEntry<Element>[] {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Determine how many list items exist
    return getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);
  }

  public static hasList(editor: Editor) {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Check if list exists below
    const [list] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      at: pointPath,
      mode: 'lowest',
    });
    const [, listPath] = list as NodeEntry<Ancestor>;

    return listPath.length > pointPath.length;
  }

  public static isEmpty(editor: Editor) {
    // Get currently selected p
    const [selectedModElement] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.MOD,
    }) as NodeEntry<Element>;

    //Get text
    const node = getLowestNodeAt(editor);
    const text = node ? node.text : '';
    return selectedModElement.children.length > 1 ? false : text.length === 0;
  }

  public static getQuote(editor: Editor, path: Path, mode: 'start' | 'end'): string | undefined {
    const [parentNode] = Editor.parent(editor, path) as NodeEntry<Element>;
    const lastPathEntry = path[path.length - 1];
    let isOutermostSibling: boolean;
    if (mode === 'start') {
      // Ausnahme bei akn:num als parent-element: In diesem Fall ist der erste child (index 0) ein unsichtbarer akn:marker.
      // Deshalb werden die Anführungszeichen beim zweiten child (index 1) gesetzt.
      isOutermostSibling = lastPathEntry === 0 || (parentNode.type === STRUCTURE_ELEMENTS.NUM && lastPathEntry === 1);
    } else {
      isOutermostSibling = lastPathEntry === parentNode.children.length - 1;
    }
    if (isOutermostSibling) {
      if (parentNode.type === REGELUNGSTEXT_STRUCTURE.QUOTED_STRUCTURE) {
        const quote = mode === 'start' ? parentNode.startQuote : parentNode.endQuote;
        return quote;
      } else {
        return this.getQuote(editor, path.slice(0, -1), mode);
      }
    } else {
      return undefined;
    }
  }

  public static getQuotes(editor: Editor, path: Path): { startQuote?: string; endQuote?: string } {
    const isWithinQuotedStructure = isWithinTypeHierarchy(editor, [REGELUNGSTEXT_STRUCTURE.QUOTED_STRUCTURE], path);
    if (isWithinQuotedStructure) {
      const startQuote = this.getQuote(editor, path, 'start');
      const endQuote = this.getQuote(editor, path, 'end');
      return {
        startQuote: startQuote,
        endQuote: endQuote,
      };
    } else {
      return { startQuote: undefined, endQuote: undefined };
    }
  }
}
