// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms, Path } from 'slate';

import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLastLocationAt,
  getLowestNodeAt,
  getPreviousSibling,
  getSiblings,
  hasNodeOfType,
} from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';

import { AenderungsbefehleQueries as Queries } from './AenderungsbefehleQueries';

export class AenderungsbefehleCommands {
  private static readonly CHARACTERS = 'abcdefghijklmnopqrstuvwxyz';
  public static mergePoints(editor: Editor): void {
    const [, selectedPointPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
    }) as NodeEntry<Element>;

    const [selectedModElement, selectedModElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.MOD,
    }) as NodeEntry<Element>;

    const selectedModElementChildren = [...selectedModElement.children];

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if point to be deleted is empty.
    // Since merge of points containing text will be a future demand it's handled within here
    if (selectedModElementChildren.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous paragraph's text
    const [previousModElement, previousModElementPath] = Editor.previous(editor, {
      at: selectedModElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.MOD,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, [
      ...previousModElementPath,
      previousModElement.children.length - 1,
    ]);

    if (previousTextNode && selectedTextNode) {
      //Merge TextWrapper and remove parts from nodes
      Transforms.removeNodes(editor, { at: selectedPointPath });
      const newPath = selectedPointPath.map((x, y) => (y === selectedPointPath.length - 1 ? --x : x));
      AenderungsbefehleCommands.renumber(editor, newPath);
      Transforms.select(editor, getLastLocationAt(editor, previousModElementPath));
    }
  }

  public static insertListeMitUntergliederung(editor: Editor): void {
    // Get current point
    const [currentPoint] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, currentPointPath] = currentPoint as NodeEntry<Element>;

    const currentLevel = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, currentPointPath);

    // Only four list levels are allowed
    if (currentLevel >= 4) {
      return;
    }

    // Get previous point
    const previousPoint = getPreviousSibling(editor, REGELUNGSTEXT_STRUCTURE.POINT, currentPointPath);

    if (!previousPoint) {
      return;
    }

    const [, previousPointPath] = previousPoint;

    // If previous point already has a list -> return
    const [existingList] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      at: previousPointPath,
      mode: 'lowest',
    });
    const [, listPath] = existingList as NodeEntry<Element>;

    // Check if previous point already contains a list
    if (existingList && listPath.length > previousPointPath.length) {
      return;
    }

    // If previous paragraph does not have a list -> remove its content and insert new list
    const [existingContent] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
      at: previousPointPath,
    });

    if (existingContent) {
      const [existingContentNode, existingContentPath] = existingContent as NodeEntry<Element>;
      let existingContentChilds = [...existingContentNode.children];

      if (hasNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.MOD, existingContentPath)) {
        const introNode = getLowestNodeAt(editor, existingContentPath);
        const newIntro = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introNode?.text || '')]);
        existingContentChilds = [newIntro];
      }

      const point = AenderungsbefehleCommands.createPoint('');
      const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, existingContentChilds);
      const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);

      Transforms.delete(editor, { at: existingContentPath });
      Transforms.insertNodes(editor, list, { at: existingContentPath });

      const [newPoint] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
        at: existingContentPath,
        mode: 'lowest',
      });
      const [, newPointPath] = newPoint as NodeEntry<Element>;

      // Determine list item content path and select it
      const [listContent] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CONTENT,
        at: existingContentPath,
      });
      const [, listContentPath] = listContent as NodeEntry<Element>;

      // Remove current point and renumber
      Transforms.removeNodes(editor, { at: currentPointPath });

      const newPointParentPath = currentPointPath.map((x, y) => (y === currentPointPath.length - 1 ? --x : x));

      AenderungsbefehleCommands.renumber(editor, newPointPath);
      AenderungsbefehleCommands.renumber(editor, newPointParentPath);

      Transforms.select(editor, listContentPath);
    }
  }

  public static deleteList(editor: Editor): void {
    // Remember intro text
    const [selectedList] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.LIST,
      mode: 'lowest',
    });
    const [, listPath] = selectedList as NodeEntry<Ancestor>;

    const [selectedIntroPNode] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
      at: listPath,
    });
    const [selectedIntroP, selectedIntroPPath] = selectedIntroPNode as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    if (selectedTextNode && selectedTextNode.text.length > 0) {
      return;
    }

    // Delete selected list
    Transforms.removeNodes(editor, { at: listPath });

    // Insert content instead
    const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, selectedIntroP.children);
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    Transforms.insertNodes(editor, content, { at: listPath });

    //select and focus
    Transforms.select(editor, getLastLocationAt(editor, selectedIntroPPath));
  }

  public static insertPoint(editor: Editor): void {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Check number of list items depending on list level
    const listItems = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);
    const level = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, pointPath);

    // Cancel if end of character list has been reached
    if (level > 1 && listItems.length >= AenderungsbefehleCommands.CHARACTERS.length) {
      return;
    }

    // Currently insertion is not allowed if current point is empty.
    // Since this could be a future demand it's handled within here.
    if (Queries.isEmpty(editor)) {
      return;
    }

    // Create next sibling's path
    const newPath = pointPath.map((x, y) => (y === pointPath.length - 1 ? ++x : x));

    // Create and insert new point
    const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, [createTextWrapper('')]);
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    const newPoint = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
    Transforms.insertNodes(editor, newPoint, { at: newPath });

    // Renumber
    AenderungsbefehleCommands.renumber(editor, newPath);

    Transforms.select(editor, getLastLocationAt(editor, newPath));
  }

  public static closeList(editor: Editor): void {
    // Get currently selected point
    const [selectedPoint] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
      mode: 'lowest',
    });
    const [, pointPath] = selectedPoint as NodeEntry<Ancestor>;

    // Find list item in list above
    const listItemAbove = Editor.above(editor, {
      at: pointPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.POINT,
    });

    if (!listItemAbove) {
      return;
    }

    const [, listItemPath] = listItemAbove as NodeEntry<Element>;

    // Create next sibling's path
    const nextPath = listItemPath.map((x, y) => (y === listItemPath.length - 1 ? ++x : x));

    // Delete selected list item
    Transforms.removeNodes(editor, { at: pointPath, hanging: false });

    Transforms.select(editor, { path: nextPath, offset: 0 });

    // Insert new point above
    const point = AenderungsbefehleCommands.createPoint('');
    Transforms.insertNodes(editor, point, { at: nextPath });

    // Renumber
    AenderungsbefehleCommands.renumber(editor, nextPath);

    Transforms.select(editor, getLastLocationAt(editor, nextPath));
  }

  public static renumber(editor: Editor, at: Path): void {
    const level = Queries.getListLevel(editor, REGELUNGSTEXT_STRUCTURE.POINT, at);
    const siblings = getSiblings(editor, REGELUNGSTEXT_STRUCTURE.POINT, at);
    const isFirstElement = siblings.length === 1;

    siblings.forEach((point, index) => {
      const [, pointPath] = point;
      const [num] = Editor.nodes(editor, {
        at: pointPath,
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.NUM,
      });
      const [, numPath] = num;
      let number = '';

      if (level === 1) {
        number = (++index).toString();
      } else {
        const array = Array.from(AenderungsbefehleCommands.CHARACTERS);
        for (let i = 1; i < level; i++) {
          number += array[index];
        }
      }

      Transforms.insertText(editor, AenderungsbefehleCommands.getNumber(number, level, isFirstElement), {
        at: [...numPath, 1],
      });

      // Set name attribute of Marker Element
      Transforms.setNodes(editor, { name: number, children: [] }, { at: [...numPath, 0] });
    });
  }

  private static getNumber(number: string, level: number, isFirstElement: boolean): string {
    if (level === 1 && isFirstElement) return '';
    return `${number}${level === 1 ? '.' : ')'}`;
  }

  private static createPoint(text: string): Element {
    const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, [createTextWrapper(text)]);
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
    const marker = createElementOfType(REGELUNGSTEXT_STRUCTURE.MARKER, [createTextWrapper('')], true, {
      name: '1',
    });
    const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [marker, createTextWrapper('')]);
    const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
    return createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
  }
}
