// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms } from 'slate';

import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLowestNodeAt,
  getLastLocationAt,
} from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';

export class GesetzPraeambelCommands {
  public static mergeRecitals(editor: Editor): void {
    const [, selectedRecitalPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.RECITAL,
    }) as NodeEntry<Element>;

    const [, selectedPElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    // Add text to previous paragraph's text
    const [, previousPElementPath] = Editor.previous(editor, {
      at: selectedPElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, previousPElementPath);

    if (previousTextNode) {
      Transforms.removeNodes(editor, { at: selectedRecitalPath });

      Transforms.select(editor, getLastLocationAt(editor, previousPElementPath));
    }
  }

  public static insertRecitals(editor: Editor, addPreambel: () => void): void {
    // Get preamble
    const [preamble] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.PREAMBLE,
    });

    if (preamble) {
      const [, preamblePath] = preamble as NodeEntry<Element>;

      // If preamble already contains recitals -> return
      const [existingRecitals] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.RECITALS,
        at: preamblePath,
      });
      if (existingRecitals) {
        return;
      }

      // Get formular path
      const [formula] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.FORMULA,
        at: preamblePath,
      });
      if (!formula) {
        return;
      }

      const [, formulaPath] = formula as NodeEntry<Element>;

      const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
      const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Präambel')]);
      const recital = createElementOfType(REGELUNGSTEXT_STRUCTURE.RECITAL, [p]);
      const recitals = createElementOfType(REGELUNGSTEXT_STRUCTURE.RECITALS, [heading, recital]);
      formulaPath.pop();
      formulaPath.push(1);
      Transforms.insertNodes(editor, recitals, { at: formulaPath });
      addPreambel();

      Transforms.select(editor, getLastLocationAt(editor, formulaPath));
    }
  }

  public static insertRecital(editor: Editor): void {
    const [selectedRecital] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.RECITAL,
    });
    const [, path] = selectedRecital as NodeEntry<Ancestor>;

    // Create next sibling's path
    const newPath = path.map((x, y) => (y === path.length - 1 ? ++x : x));

    // Create and insert new paragraph
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const newRecital = createElementOfType(REGELUNGSTEXT_STRUCTURE.RECITAL, [p]);
    Transforms.insertNodes(editor, newRecital, { at: newPath });

    Transforms.select(editor, getLastLocationAt(editor, newPath));
  }

  public static deleteRecitals(editor: Editor, removePreambel: () => void): void {
    // Remember intro text
    const [recitals] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.RECITALS,
    });
    const [, recitalsPath] = recitals as NodeEntry<Ancestor>;

    // Delete recitals
    Transforms.removeNodes(editor, { at: recitalsPath });
    removePreambel();

    // Determine formula and select it
    const [formula] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.FORMULA,
    });

    if (formula) {
      const [, formulaPath] = formula as NodeEntry<Element>;

      Transforms.select(editor, getLastLocationAt(editor, formulaPath));
    }
  }
}
