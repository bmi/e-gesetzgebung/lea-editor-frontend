// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect } from 'react';
import { Editor, Transforms } from 'slate';
import { ReactEditor } from 'slate-react';

import {
  CombinedPluginRenderProps,
  Plugin,
  PluginProps,
  useEditorOnFocusState,
  useDocumentHasPreambelState,
  useDocumentHasPreambelChange,
} from '../../../../plugin-core';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { GesetzPraeambelCommands as Commands } from './GesetzPraeambelCommands';
import { GesetzPraeambelQueries as Queries } from './GesetzPraeambelQueries';

import {
  Unit,
  isFirstPosition,
  isLastPosition,
  isEnter,
  getSiblings,
  isFirstSiblingOfType,
  isLeftOrRightNavigation,
  getLastLocationAt,
  hasNodeOfType,
  isTabOrTabBack,
} from '../../../../plugin-core/utils';
import { Element } from '../../../../../interfaces';
import { ItemType } from 'antd/lib/menu/interface';

export class GesetzPraeambelPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  private addPreambel: () => void = () => {};
  private removePreambel: () => void = () => {};

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (Queries.isFormula(editor)) {
        return;
      }
      if (!isFirstPosition(editor) || !Queries.isRecital(editor)) {
        deleteBackward(unit);
      } else if (
        isFirstPosition(editor) &&
        getSiblings(editor, REGELUNGSTEXT_STRUCTURE.RECITAL).length === 1 &&
        isLastPosition(editor)
      ) {
        Commands.deleteRecitals(editor, this.removePreambel);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.RECITAL)) {
        Commands.mergeRecitals(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (Queries.isFormula(editor)) {
        return;
      }
      if (!Queries.isRecital(editor) || !isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isRecital(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (isTabOrTabBack(event)) {
      return true;
    }

    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (Queries.isFormula(editor) && !isEnter(event) && !isLeftOrRightNavigation(event)) {
      return false;
    }

    if (isEnter(event) && !isLastPosition(editor)) {
      return false;
    }

    if (Queries.isFormula(editor) && isEnter(event) && isLastPosition(editor)) {
      Commands.insertRecitals(editor, this.addPreambel);
      event.preventDefault();
      return false;
    }

    if (Queries.isRecital(editor) && isEnter(event)) {
      if (!Queries.hasEmptyRecitals(editor) || (!isLastPosition(editor) && !isFirstPosition(editor))) {
        Commands.insertRecital(editor);
        event.preventDefault();
      }
      return false;
    }

    return true;
  }

  private _insertToolItemTypes: ItemType[] | null = [];

  public set insertToolItemTypes(itemTypes: ItemType[] | null) {
    this._insertToolItemTypes = itemTypes;
  }

  public get insertToolItemTypes(): ItemType[] | null {
    return this._insertToolItemTypes;
  }

  public InsertTools = ({ editor }: { editor: Editor }): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const documentHasPreambel = useDocumentHasPreambelState(this.editorDocument.index);

    useEffect(() => {
      setTimeout(() => {
        hasNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.RECITALS, []) ? this.addPreambel() : this.removePreambel();
      }, 200);
    }, []);

    if (editorOnFocus.index === this.editorDocument.index) {
      this.insertToolItemTypes = [
        {
          key: 'insertTools-preambel',
          label: 'Präambel',
          title: 'Präambel',
          onClick: () => {
            const [formula] = Editor.nodes(editor, {
              match: (n) => {
                return (n as Element).type === REGELUNGSTEXT_STRUCTURE.FORMULA;
              },
              at: [0],
            });
            const [, formulaPath] = formula;
            const lastFormulaLocation = getLastLocationAt(editor, formulaPath);
            Transforms.select(editor, lastFormulaLocation);

            Commands.insertRecitals(editor, this.addPreambel);
          },
          disabled: documentHasPreambel || this.editorDocument.isReadonly,
        },
      ];
    } else {
      this.insertToolItemTypes = null;
    }
    return <></>;
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { setDocumentHasPreambel } = useDocumentHasPreambelChange();

    useEffect(() => {
      this.addPreambel = () => setDocumentHasPreambel(this.editorDocument.index, true);
      this.removePreambel = () => setDocumentHasPreambel(this.editorDocument.index, false);
    }, []);
    return <></>;
  };

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
