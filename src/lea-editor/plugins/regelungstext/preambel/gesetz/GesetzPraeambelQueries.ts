// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, NodeEntry, Path } from 'slate';

import { Element } from '../../../../../interfaces';
import { getLowestNodeAt, isOfType, isWithinTypeHierarchy } from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';

export class GesetzPraeambelQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    return this.isFormula(editor, path, type) || this.isRecital(editor, path, type);
  }

  public static isFormula(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(
        editor,
        [REGELUNGSTEXT_STRUCTURE.FORMULA, REGELUNGSTEXT_STRUCTURE.PREAMBLE, REGELUNGSTEXT_STRUCTURE.BILL],
        path,
      )
    );
  }

  public static isRecital(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(
        editor,
        [
          REGELUNGSTEXT_STRUCTURE.RECITAL,
          REGELUNGSTEXT_STRUCTURE.RECITALS,
          REGELUNGSTEXT_STRUCTURE.PREAMBLE,
          REGELUNGSTEXT_STRUCTURE.BILL,
        ],
        path,
      )
    );
  }

  public static hasEmptyRecitals(editor: Editor): boolean {
    const recitals = this.getRecitals(editor);

    return recitals.some((recital) => {
      const [, recitalPath] = recital as NodeEntry<Element>;
      const node = getLowestNodeAt(editor, recitalPath);
      const text = node ? node.text : '';
      return text.length === 0;
    });
  }

  public static getRecitals(editor: Editor): NodeEntry<Element>[] {
    // Get root element recitals
    const [selectedRecitals] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.RECITALS,
    });

    const [, recitalsPath] = selectedRecitals;
    // Get children within rectials element
    const selectedRecitalElements = Array.from(
      Editor.nodes(editor, {
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.RECITAL,
        at: recitalsPath,
      }),
    );
    return selectedRecitalElements as NodeEntry<Element>[];
  }
}
