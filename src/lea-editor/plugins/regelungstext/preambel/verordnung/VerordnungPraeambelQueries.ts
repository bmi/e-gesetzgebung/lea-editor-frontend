// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, NodeEntry, Path } from 'slate';

import { Element } from '../../../../../interfaces';
import { getLowestNodeAt, isOfType, isWithinTypeHierarchy } from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';

export class VerordnungPraeambelQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    return this.isFormula(editor, path, type) || this.isCitation(editor, path, type);
  }

  public static isFormula(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(
        editor,
        [REGELUNGSTEXT_STRUCTURE.FORMULA, REGELUNGSTEXT_STRUCTURE.PREAMBLE, REGELUNGSTEXT_STRUCTURE.BILL],
        path,
      )
    );
  }

  public static isCitation(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === REGELUNGSTEXT_STRUCTURE.P : isOfType(editor, REGELUNGSTEXT_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(
        editor,
        [
          REGELUNGSTEXT_STRUCTURE.CITATION,
          REGELUNGSTEXT_STRUCTURE.CITATIONS,
          REGELUNGSTEXT_STRUCTURE.PREAMBLE,
          REGELUNGSTEXT_STRUCTURE.BILL,
        ],
        path,
      )
    );
  }

  public static hasEmptyCitations(editor: Editor): boolean {
    const citations = this.getCitations(editor);

    return citations.some((citation) => {
      const [, citationPath] = citation as NodeEntry<Element>;
      const node = getLowestNodeAt(editor, citationPath);
      const text = node ? node.text : '';
      return text.length === 0;
    });
  }

  public static getCitations(editor: Editor): NodeEntry<Element>[] {
    // Get root element citations
    const [selectedCitations] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CITATIONS,
    });

    const [, citationsPath] = selectedCitations;
    // Get children within rectials element
    const selectedCitationElements = Array.from(
      Editor.nodes(editor, {
        match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CITATION,
        at: citationsPath,
      }),
    );
    return selectedCitationElements as NodeEntry<Element>[];
  }
}
