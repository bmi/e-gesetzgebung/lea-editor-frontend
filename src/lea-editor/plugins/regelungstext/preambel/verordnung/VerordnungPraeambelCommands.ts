// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms } from 'slate';

import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLowestNodeAt,
  getLastLocationAt,
} from '../../../../plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';

export class VerordnungPraeambelCommands {
  public static mergeCitations(editor: Editor): void {
    const [, selectedCitationPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CITATION,
    }) as NodeEntry<Element>;

    const [, selectedPElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    // Add text to previous paragraph's text
    const [, previousPElementPath] = Editor.previous(editor, {
      at: selectedPElementPath,
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, previousPElementPath);

    if (previousTextNode) {
      Transforms.removeNodes(editor, { at: selectedCitationPath });

      Transforms.select(editor, getLastLocationAt(editor, previousPElementPath));
    }
  }

  public static insertCitations(editor: Editor): void {
    // Get preamble
    const [preamble] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.PREAMBLE,
    });

    if (preamble) {
      const [, preamblePath] = preamble as NodeEntry<Element>;

      // If preamble already contains citations -> return
      const [existingCitations] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.CITATIONS,
        at: preamblePath,
      });
      if (existingCitations) {
        return;
      }

      // Get formular path
      const [formula] = Editor.nodes(editor, {
        match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.FORMULA,
        at: preamblePath,
      });
      if (!formula) {
        return;
      }

      const [, formulaPath] = formula as NodeEntry<Element>;

      const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
      const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Präambel')]);
      const citation = createElementOfType(REGELUNGSTEXT_STRUCTURE.CITATION, [p]);
      const citations = createElementOfType(REGELUNGSTEXT_STRUCTURE.CITATIONS, [heading, citation]);
      formulaPath.pop();
      formulaPath.push(1);
      Transforms.insertNodes(editor, citations, { at: formulaPath });

      Transforms.select(editor, getLastLocationAt(editor, formulaPath));
    }
  }

  public static insertCitation(editor: Editor): void {
    const [selectedCitation] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CITATION,
    });
    const [, path] = selectedCitation as NodeEntry<Ancestor>;

    // Create next sibling's path
    const newPath = path.map((x, y) => (y === path.length - 1 ? ++x : x));

    // Create and insert new paragraph
    const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('')]);
    const newCitation = createElementOfType(REGELUNGSTEXT_STRUCTURE.CITATION, [p]);
    Transforms.insertNodes(editor, newCitation, { at: newPath });

    Transforms.select(editor, getLastLocationAt(editor, newPath));
  }

  public static deleteCitations(editor: Editor): void {
    // Remember intro text
    const [citations] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === REGELUNGSTEXT_STRUCTURE.CITATIONS,
    });
    const [, citationsPath] = citations as NodeEntry<Ancestor>;

    // Delete citations
    Transforms.removeNodes(editor, { at: citationsPath });

    // Determine formula and select it
    const [formula] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === REGELUNGSTEXT_STRUCTURE.FORMULA,
    });

    if (formula) {
      const [, formulaPath] = formula as NodeEntry<Element>;

      Transforms.select(editor, getLastLocationAt(editor, formulaPath));
    }
  }
}
