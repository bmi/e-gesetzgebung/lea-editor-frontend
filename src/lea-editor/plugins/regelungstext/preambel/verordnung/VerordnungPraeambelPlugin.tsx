// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../../plugin-core';
import { REGELUNGSTEXT_STRUCTURE } from '../../RegelungstextStructure';
import { VerordnungPraeambelCommands as Commands } from './VerordnungPraeambelCommands';
import { VerordnungPraeambelQueries as Queries } from './VerordnungPraeambelQueries';

import {
  Unit,
  isFirstPosition,
  isLastPosition,
  isEnter,
  isFirstSiblingOfType,
  isLeftOrRightNavigation,
  isTabOrTabBack,
} from '../../../../plugin-core/utils';
import { Element } from '../../../../../interfaces';

export class VerordnungPraeambelPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (Queries.isFormula(editor)) {
        return;
      }
      if (!Queries.isCitation(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.CITATION)) {
        Commands.mergeCitations(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (Queries.isFormula(editor)) {
        return;
      }
      if (!Queries.isCitation(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isCitation(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (isTabOrTabBack(event)) {
      return true;
    }

    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (Queries.isFormula(editor) && !isEnter(event) && !isLeftOrRightNavigation(event)) {
      return false;
    }

    if (isEnter(event) && !isLastPosition(editor)) {
      return false;
    }

    if (Queries.isFormula(editor) && isEnter(event) && isLastPosition(editor)) {
      Commands.insertCitations(editor);
      event.preventDefault();
      return false;
    }

    if (Queries.isCitation(editor) && isEnter(event)) {
      if (!Queries.hasEmptyCitations(editor) || (!isLastPosition(editor) && !isFirstPosition(editor))) {
        Commands.insertCitation(editor);
        event.preventDefault();
      }
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
