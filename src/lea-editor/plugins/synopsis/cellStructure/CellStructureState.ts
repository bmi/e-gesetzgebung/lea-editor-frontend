// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DiffTable, ElementAndChildrenGuid } from '../../../plugin-core/utils';

type CellStructureState = {
  cellStructure: DiffTable | null;
  guidsArray: ElementAndChildrenGuid[][] | null;
};

const InitialCellStructureState: CellStructureState = {
  cellStructure: null,
  guidsArray: null,
};

export { InitialCellStructureState, CellStructureState };
