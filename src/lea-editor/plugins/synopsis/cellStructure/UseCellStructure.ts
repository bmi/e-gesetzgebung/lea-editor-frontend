// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  getEditorsGuidsAboveArticle,
  getCellStructureAboveArticle,
  getCompleteCellStructure,
  getUpdatedGuidsAboveArticle,
} from '../../../plugin-core/utils';
import { updateCells, updateGuids } from './CellStructureSlice';
import { useAppDispatch, useAppSelector } from '../../../../components/Store';
import { Editor } from 'slate';

export const useCellStructureState = () => {
  return useAppSelector((state) => state.editorStatic.cells.cellStructure);
};

const useGuidsTableState = () => {
  return useAppSelector((state) => state.editorStatic.cells.guidsArray);
};

export const useCellStructureDispatch = () => {
  const dispatch = useAppDispatch();
  const previousElementGuids = useGuidsTableState();
  const calculateCellStructure = (...editors: Editor[]) => {
    if (editors.length < 2) return;
    const guidArrays = getEditorsGuidsAboveArticle(...editors);
    const cellStructureAboveArticle = getCellStructureAboveArticle(...guidArrays);
    const completeCellStructure = getCompleteCellStructure(cellStructureAboveArticle);
    dispatch(updateGuids(guidArrays));
    dispatch(updateCells(completeCellStructure));
  };

  const updateCellStructure = (editor: Editor, index: number) => {
    if (previousElementGuids === null || previousElementGuids.length < 2) return;
    const guidArrays = getUpdatedGuidsAboveArticle(previousElementGuids, editor, index);
    if (guidArrays === null) return;
    const cellStructureAboveArticle = getCellStructureAboveArticle(...guidArrays);
    const completeCellStructure = getCompleteCellStructure(cellStructureAboveArticle);
    dispatch(updateGuids(guidArrays));
    dispatch(updateCells(completeCellStructure));
  };
  return {
    calculateCellStructure,
    useGuidsTableState,
    updateCellStructure,
  };
};
