// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect } from 'react';
import { BaseEditor, BaseInsertNodeOperation, Editor, NodeEntry } from 'slate';
import { ReactEditor } from 'slate-react';
import { Element } from '../../../../interfaces';
import { CombinedPluginRenderProps, Plugin, PluginProps, useSynopsisInfo } from '../../../plugin-core';
import {
  checkIfCellsShouldRender,
  checkIfCellsVisible,
  getParentElement,
  isNodeOperation,
  setAllDividersHeight,
  setDividersHeight,
  setNextDividersHeight,
} from '../../../plugin-core/utils';
import { DocumentDTO, useFirstDocumentState, useSynopsisDocumentsState } from '../../../../api';
import { Divider } from 'antd';
import { useCellStructureDispatch, useCellStructureState } from './UseCellStructure';
import { HistoryEditor } from 'slate-history';

export class CellStructurePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  private isSynopsisVisible = false;

  private firstDocument: DocumentDTO | null = null;

  private synopsisDocuments: DocumentDTO[] | undefined = undefined;

  public create<T extends Editor>(e: T): T & ReactEditor & HistoryEditor {
    const editor = e as T & ReactEditor & HistoryEditor;
    const { apply } = editor;
    e.apply = (op) => {
      apply(op);
      if (!checkIfCellsVisible(this.isSynopsisVisible, this.firstDocument, this.synopsisDocuments)) return;
      // Check if operation only manipulates text or sets the attributes of a node
      if (isNodeOperation(op)) {
        const node = (op as unknown as BaseInsertNodeOperation).node;
        if (node && (node as Element).GUID) {
          this.updateCellStructure(editor, this.editorDocument.index);
        }
      }
      if (op.type !== 'set_selection') {
        let element: NodeEntry<Element> | null = null;
        try {
          const [...elements] = Editor.nodes<Element>(editor, {
            at: op.path,
            mode: 'lowest',
            match: (node) => !!(node as Element).GUID || !!(node as Element).refGUID,
          });
          element = elements[0];
        } catch (e) {}
        setTimeout(() => {
          if (element?.[0].GUID) {
            setNextDividersHeight(this.editorDocument.index, element[0].GUID);
          } else {
            setAllDividersHeight(this.editorDocument.index);
          }
        });
      }
    };
    return editor;
  }

  public renderCells = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementType = e.type || '';
    const cellTable = useCellStructureState();
    const guid = e.GUID ?? '';
    const refGUID = e.refGUID ?? '';
    const elementPath = ReactEditor.findPath(props.editor, e);
    const parentElement = getParentElement(props.editor, elementPath);
    const renderCells =
      props.cellsVisible && !!parentElement && checkIfCellsShouldRender(elementType, parentElement.type);
    if (!(renderCells && cellTable)) return null;
    const combinedGUID = refGUID ? refGUID + ':ref' : guid;
    renderCells &&
      props.firstRender &&
      setTimeout(() => {
        setDividersHeight(refGUID ? refGUID : guid, this.editorDocument.index, !!refGUID);
      });

    const guidArray = cellTable[0];
    const guidExistsArray = cellTable[this.editorDocument.index + 1];
    const guidIndex = guidArray.findIndex((guid, index) => combinedGUID === guid && guidExistsArray[index]);
    const emptyCellDividerGuids: string[] = [];
    for (let i = guidIndex; i < guidExistsArray.length; i++) {
      if (guidExistsArray[i + 1] === false) {
        emptyCellDividerGuids.push(guidArray[i + 1]);
      } else {
        break;
      }
    }
    return (
      <>
        <Divider
          type="horizontal"
          divider-guid={`${this.editorDocument.index}:${guidIndex}-${combinedGUID}`}
          className={`synopsis-divider`}
        ></Divider>
        {emptyCellDividerGuids.map((_guid, index) => (
          <Divider
            key={`${_guid}`}
            type="horizontal"
            divider-guid={`${this.editorDocument.index}:${guidIndex + 1 + index}-${_guid}`}
            className={`synopsis-divider`}
          ></Divider>
        ))}
      </>
    );
  };

  private updateCellStructure: (editor: BaseEditor, index: number) => void = (_editor, _index) => {};

  public RegisterDispatchFunctions = (): ReactElement => {
    const { isSynopsisVisible } = useSynopsisInfo();
    const { document } = useFirstDocumentState();
    const { documents } = useSynopsisDocumentsState();
    const { updateCellStructure } = useCellStructureDispatch();
    const cellStructureState = useCellStructureState();

    useEffect(() => {
      this.isSynopsisVisible = isSynopsisVisible;
      this.firstDocument = document;
      this.synopsisDocuments = documents;
      this.updateCellStructure = updateCellStructure;
    }, [isSynopsisVisible, cellStructureState]);
    return <></>;
  };
}
