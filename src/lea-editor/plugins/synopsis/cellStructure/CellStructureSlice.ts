// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { DiffTable, ElementAndChildrenGuid } from '../../../plugin-core/utils/CellUtils';
import { InitialCellStructureState } from './CellStructureState';

const cellStructureSlice = createSlice({
  name: 'cellStructure',
  initialState: InitialCellStructureState,
  reducers: {
    updateCells(state, action: PayloadAction<DiffTable | null>) {
      state.cellStructure = action.payload;
    },
    updateGuids(state, action: PayloadAction<ElementAndChildrenGuid[][] | null>) {
      state.guidsArray = action.payload;
    },
  },
});
export const { updateCells, updateGuids } = cellStructureSlice.actions;
export const CellStructureReducer = cellStructureSlice.reducer;
