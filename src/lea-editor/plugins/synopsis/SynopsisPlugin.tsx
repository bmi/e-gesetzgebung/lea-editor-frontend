// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { BaseEditor } from 'slate';
import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../plugin-core';
import { Element } from '../../../interfaces';

import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';

import './SynopsisPlugin.less';

export class SynopsisPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends BaseEditor>(editor: T): T {
    return editor;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;

    const elementType = e.type || '';
    const guid = e.GUID;
    switch (elementType) {
      case STRUCTURE_ELEMENTS.TABLE_CELL: {
        const style = e.style;
        const refererIndex = e.refersTo?.indexOf('|');
        const refersTo = refererIndex ? e.refersTo?.substring(refererIndex + 1) : '';
        return (
          <td
            id={guid && `${this.editorDocument.index}:${guid}`}
            className={`${elementType} ${refersTo} ${style}`}
            {...props.attributes}
          >
            {props.children}
          </td>
        );
      }
      case STRUCTURE_ELEMENTS.SPAN: {
        const status = e.status;
        return (
          <span className={`${elementType} ${status}`} {...props.attributes}>
            {props.children}
          </span>
        );
      }
      case STRUCTURE_ELEMENTS.TEXTWRAPPER: {
        const text = e.children[0].text;
        let paraClass = '';
        if (text) {
          paraClass = text.startsWith('§' || 'Artikel') ? 'einzelvorschrift' : '';
        }
        return (
          <span className={`${elementType} ${paraClass}`} {...props.attributes}>
            {props.children}
          </span>
        );
      }
      default:
        return null;
    }
  };
}
