// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse, Form, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useState } from 'react';
import { ArrowRightBlackIcon, ArrowDownBlackIcon } from '@lea/ui';
import './ColumnResizer.less';
import { ColumnSize, useColumnSizeChange, useEditorDocumentsState } from '../../../plugin-core';
import FormItem from 'antd/lib/form/FormItem';
import { SelectWrapper } from '@plateg/theme';
const { Panel } = Collapse;

export const ColumnResizer = (): ReactElement => {
  const { Option } = Select;
  const { t } = useTranslation();
  const { editorDocuments } = useEditorDocumentsState();

  const { setColumnSize } = useColumnSizeChange();

  const [collapse, _setCollapseHasHover] = useState(false);

  const handleChange = (size: ColumnSize, index: number) => {
    setColumnSize(index, size);
  };

  const setExpandIcon = (isActive?: boolean) => {
    return (
      <div className="lea-icon">
        {isActive ? <ArrowDownBlackIcon hasHover={collapse} /> : <ArrowRightBlackIcon hasHover={collapse} />}
      </div>
    );
  };

  return (
    <div className="column-resizer-option-wrapper">
      <Collapse
        className="options-collapse"
        expandIcon={({ isActive }) => setExpandIcon(isActive)}
        defaultActiveKey={[0]}
        ghost
      >
        <Panel
          id={'column-resizer-panel'}
          header={<div className="panel-header">{t('lea.synopsisOptions.columnResizer.title')}</div>}
          key={0}
        >
          <Form layout="vertical" className="column-resizer-form" tabIndex={0}>
            {editorDocuments?.map((editorDocument) => (
              <FormItem
                key={editorDocument.index}
                labelAlign="left"
                label={`Spalte ${editorDocument.index + 1}`}
                className="formItem"
              >
                <SelectWrapper
                  className="sizeOptions"
                  size="small"
                  defaultValue={editorDocument.size}
                  value={editorDocument.size}
                  aria-label={`Spaltenbreite einstellen`}
                  onChange={(size) => handleChange(size, editorDocument.index)}
                >
                  {Object.keys(ColumnSize).map((size) => (
                    <Option key={`${editorDocument.index}-${size}`} value={size} aria-label={size}>
                      {t(`lea.synopsisOptions.columnResizer.sizes.${size}`)}
                    </Option>
                  ))}
                </SelectWrapper>
              </FormItem>
            ))}
          </Form>
        </Panel>
      </Collapse>
    </div>
  );
};
