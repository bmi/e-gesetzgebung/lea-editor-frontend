// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse } from 'antd';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { ArrowRightBlackIcon, ArrowDownBlackIcon, ChoicesCheckBox, ChoicesItemInterface } from '@lea/ui';
import './VisibleColumns.less';
import {
  useEditorDocumentVisibleChange,
  useEditorDocumentsState,
  useEditorDocumentsVisibleState,
} from '../../../plugin-core';
const { Panel } = Collapse;

export const VisibleColumns = (): ReactElement => {
  const { t } = useTranslation();
  const { editorDocuments } = useEditorDocumentsState();
  const documentsVisibleList = useEditorDocumentsVisibleState();
  const { setDocumentsVisible } = useEditorDocumentVisibleChange();

  const [collapse, _setCollapseHasHover] = useState(false);
  const columnInitValues = useRef<ChoicesItemInterface[]>(
    editorDocuments
      ? editorDocuments.map((editorDocument) => ({
          label: `Spalte ${editorDocument.index + 1}`,
          value: editorDocument.index,
        }))
      : [],
  );

  useEffect(() => {
    editorDocuments?.forEach((editorDocument) => {
      if (!columnInitValues.current.find((value) => value.value === editorDocument.index)) {
        columnInitValues.current.push({
          label: `Spalte ${editorDocument.index + 1}`,
          value: editorDocument.index,
        });
      }
    });
  }, [editorDocuments?.length]);

  const setExpandIcon = (isActive?: boolean) => {
    return (
      <div className="lea-icon">
        {isActive ? <ArrowDownBlackIcon hasHover={collapse} /> : <ArrowRightBlackIcon hasHover={collapse} />}
      </div>
    );
  };

  return (
    <div className="visible-columns-option-wrapper">
      <Collapse
        className="options-collapse"
        expandIcon={({ isActive }) => setExpandIcon(isActive)}
        defaultActiveKey={[0]}
        ghost
      >
        <Panel
          id={'visible-columns-panel'}
          header={<div className="panel-header">{t('lea.synopsisOptions.visibleColumns.title')}</div>}
          key={0}
        >
          <div tabIndex={0} className="visibleColumnsSelect">
            {t('lea.synopsisOptions.visibleColumns.select.title')}
            <ChoicesCheckBox
              checkedList={documentsVisibleList}
              setCheckedList={(checkedList) => setDocumentsVisible(checkedList as number[])}
              choicesList={columnInitValues.current}
              isVisible={true}
            />
          </div>
        </Panel>
      </Collapse>
    </div>
  );
};
