// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse, Switch } from 'antd';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useEffect } from 'react';
import './SynchronousScrolling.less';
import {
  EditorWizards,
  useSynchronousScrollingDispatch,
  useSynchronousScrollingState,
  useSynopsisInfo,
  useWizardVisibleChange,
} from '../../../plugin-core';
const { Panel } = Collapse;

export const SynchronousScrolling = (): ReactElement => {
  const { t } = useTranslation();
  const { isSynopsisVisible } = useSynopsisInfo();
  const synchronousScrolling = useSynchronousScrollingState();

  const { openWizard } = useWizardVisibleChange(EditorWizards.SYNCHRONOUSSCROLLING);
  const { setSynchronousScrollingOff } = useSynchronousScrollingDispatch();

  const handleSynchronousSwitchChange = (open: boolean) => {
    if (open) {
      openWizard();
    } else {
      setSynchronousScrollingOff();
    }
  };

  useEffect(() => {
    if (!isSynopsisVisible) {
      setSynchronousScrollingOff();
    }
  }, [isSynopsisVisible]);

  return (
    <div className="synchronous-scrolling-option-wrapper">
      <Collapse className="options-collapse" defaultActiveKey={[0]} ghost collapsible="icon">
        <Panel
          id={'synchronous-scrolling-panel'}
          header={<div className="panel-header">{t('lea.synopsisOptions.synchronousScrolling.title')}</div>}
          key={0}
        >
          <div className="synchronousScrollingSwitch">
            <Switch
              size="small"
              id="synchronous-scrolling-switch"
              disabled={!isSynopsisVisible}
              title={t('lea.synopsisOptions.synchronousScrolling.switch.title')}
              onChange={handleSynchronousSwitchChange}
              checked={synchronousScrolling}
              className="switch"
            />
            <div>{t('lea.synopsisOptions.synchronousScrolling.switch.title')}</div>
          </div>
        </Panel>
      </Collapse>
    </div>
  );
};
