// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { LeaDrawer, AriaController as AriaControllerLea } from '@lea/ui';
import React, { ReactElement, useEffect, useRef } from 'react';
import { Editor } from 'slate';
import { Plugin, PluginProps } from '../../../plugin-core';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { Divider } from 'antd';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
  useEditorOptionsVisibleState,
} from '../../../plugin-core/editor-store/editor-options';
import { SynchronousScrolling } from './SynchronousScrolling';
import { ColumnResizer } from './ColumnResizer';
import { useTranslation } from 'react-i18next';
import { VisibleColumns } from './VisibleColumns';

export class SynopsisOptionsPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T {
    return e;
  }

  public OptionDialog = (): ReactElement => {
    const { t } = useTranslation();
    const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.SYNOPSIS);
    const { closeOptions } = useEditorOptionsVisibleChange(EditorOptions.SYNOPSIS);
    const commentOptionRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
      AriaControllerPlateg.setAriaLabelsByQuery(
        '.synsopsis-options .ant-drawer-close',
        'Parallelansichtsoptionen schließen',
      );
      setTimeout(
        () =>
          commentOptionRef.current &&
          AriaControllerLea.setElementIdByClassNameInElement(
            'ant-drawer-close',
            'lea-drawer-comment-option-close-button',
            commentOptionRef.current,
          ),
      );
    }, [optionsVisible]);

    return (
      <LeaDrawer
        className="synsopsis-options"
        placement={'right'}
        open={optionsVisible}
        onClose={closeOptions}
        title={'Parallelansicht'}
        expandable={true}
        refProp={commentOptionRef}
        ariaLabel={t('lea.synopsisOptions.ariaLabel')}
      >
        <SynchronousScrolling />
        <VisibleColumns />
        <Divider className="option-divider" />
        <ColumnResizer />
      </LeaDrawer>
    );
  };
}
