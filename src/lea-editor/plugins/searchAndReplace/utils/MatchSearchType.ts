// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { LeaNode } from '../../../../interfaces';
import { SearchForTypes } from '../searchReplaceOptions/SearchReplaceOptions';
import { ReactEditor } from 'slate-react';
import { SearchReplaceEditor } from '../SearchReplaceEditor';
import { STRUCTURE_ELEMENTS } from '../../../plugin-core/utils/StructureElements';
import { getNodeAt } from '../../../plugin-core/utils';

export const MatchSearchType = (editor: SearchReplaceEditor, node: LeaNode, leafs: number[]): boolean => {
  const isEditable = () => {
    try {
      const element = ReactEditor.toDOMNode(editor, node);
      const editableElement = element?.closest('.contentEditable');
      return editableElement !== null;
    } catch (e) {
      return false;
    }
  };

  const isAuthorialNote = () => {
    let path = [...leafs];
    path = path.splice(0, path.length - 3);
    const parent = getNodeAt(editor, path);
    if (parent) {
      return parent[0].type === STRUCTURE_ELEMENTS.AUTHORIALNOTE;
    } else {
      return false;
    }
  };

  switch (editor.searchType) {
    case SearchForTypes.Text:
      return isEditable() && !isAuthorialNote();
    case SearchForTypes.TextReadonly:
      return !isEditable() && !isAuthorialNote();
    default:
      return false;
  }
};
