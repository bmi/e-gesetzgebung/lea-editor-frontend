// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Node, Text, Path } from 'slate';
import { PluginRange } from '../../plugin-core';

export const SearchDecorator = (node: Node, path: Path, searchText?: string): PluginRange[] => {
  const ranges: PluginRange[] = [];

  if (searchText && Text.isText(node)) {
    const { text } = node;
    const parts = text.split(searchText);
    let offset = 0;

    parts.forEach((part, i) => {
      if (i !== 0) {
        ranges.push({
          anchor: { path, offset: offset - searchText.length },
          focus: { path, offset },
          highlight: true,
        });
      }

      offset = offset + part.length + searchText.length;
    });
  }

  return ranges;
};
