// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Point, Range, Transforms } from 'slate';
import { REPLACE_ONE, SEARCH_NEXT, SEARCH_PREV } from '../../../constants';
import { SearchPoint } from '../../../interfaces';

import { SearchReplaceEditor } from './SearchReplaceEditor';
import { SearchForTypes } from './searchReplaceOptions/SearchReplaceOptions';

export class SearchReplaceCommands {
  public static selectSearchReplaceDialog(): void {
    const searchButton = document.getElementById('search-replace-options-button');
    const searchBox = document.getElementById('search-text-input');

    if (searchButton && !searchBox) {
      searchButton.click();
    }

    this.setSearchboxFocus();
  }

  public static setSearchboxFocus(): void {
    setTimeout(() => {
      const searchBox = document.getElementById('search-text-input');
      if (searchBox) {
        searchBox.focus();
      }
    }, 20);
  }

  public static search(editor: SearchReplaceEditor, text?: string): void {
    editor.search(text);
  }

  public static replace(editor: SearchReplaceEditor, format: string, text?: string, selectNext = true): void {
    if (format === REPLACE_ONE) {
      SearchReplaceCommands.replaceNext(editor, text, selectNext);
    } else {
      SearchReplaceCommands.replaceAll(editor, text);
    }
  }

  public static selectNextSearchResult(editor: SearchReplaceEditor, format: string, replaceAll = false): void {
    if (!editor.searchText || !editor.searchResults || editor.searchResults.length === 0) {
      return;
    }

    const selection = editor.selection ?? editor.lastSearchSelection;
    const { searchResults, searchText } = editor;
    const [selectedIndex, point] =
      format === SEARCH_PREV
        ? SearchReplaceCommands.nextSelectionPoint(searchResults, selection, true)
        : SearchReplaceCommands.nextSelectionPoint(searchResults, selection);
    SearchReplaceCommands.select(editor, point, searchText.length, replaceAll);
    if (editor.searchType === SearchForTypes.AuthorialNotes) {
      editor.searchPosition = SearchReplaceCommands.setAuthorialNoteSearchPosition(editor, format);
    } else {
      editor.searchPosition = (format === SEARCH_PREV ? editor.searchCount - 1 - selectedIndex : selectedIndex) + 1;
    }
  }

  private static replaceNext(editor: SearchReplaceEditor, text?: string, selectNext = true, replaceAll = false): void {
    if (!text) {
      return;
    }
    const selection = editor.selection ?? editor.lastSearchSelection;
    if (selection && Editor.string(editor, selection) === editor.searchText) {
      editor.lastSearchSelection = { anchor: selection.anchor, focus: selection.anchor };
      editor.replace(text);
      editor.search(editor.searchText);
      if (!editor.searchResults || editor.searchResults.length === 0) {
        Transforms.deselect(editor);
        return;
      }
    }

    if (selectNext) {
      SearchReplaceCommands.selectNextSearchResult(editor, SEARCH_NEXT, replaceAll);
    }
  }

  private static replaceAll(editor: SearchReplaceEditor, text?: string): void {
    editor.searchResults.forEach((sr) => {
      sr.offsets.forEach(() => {
        SearchReplaceCommands.replaceNext(editor, text, true, true);
      });
    });
    SearchReplaceCommands.replaceNext(editor, text, true, true); // if first replaceNext call did only a search, this call removes the last search result
    editor.search(editor.searchText); // call search to reset search results
    Transforms.deselect(editor);
  }

  private static nextSelectionPoint(
    searchPoints: SearchPoint[],
    selection: Range | null,
    reverse = false,
  ): [number, Point] {
    const points = reverse ? searchPoints.slice().reverse() : searchPoints;

    if (!selection) {
      return [0, { path: points[0].path, offset: points[0].offsets[0] } as Point];
    }

    let selectedIndex = -1;
    const selectionPath = reverse ? selection.anchor.path : selection.focus.path;
    const selectionOffset = reverse ? selection.anchor.offset : selection.focus.offset;
    for (let i = 0; i < points.length; ++i) {
      const p =
        selectionPath.length < points[i].path.length ? points[i].path.slice(0, selectionPath.length) : points[i].path;
      const sp =
        selectionPath.length > points[i].path.length ? selectionPath.slice(0, points[i].path.length) : selectionPath;

      if (p.every((v, i) => v === sp[i])) {
        const offsets = reverse ? points[i].offsets.slice().reverse() : points[i].offsets;
        const index = offsets.findIndex((o) => {
          return reverse ? selectionOffset > o : selectionOffset <= o;
        });
        const offsetIndex = index === -1 ? 0 : index;
        const pointIndex = this.getNextPointIndex(i, points, index !== -1);
        const normalizedOffsetIndex = reverse ? points[pointIndex].offsets.length - 1 - offsetIndex : offsetIndex;
        selectedIndex = points
          .slice(0, pointIndex + 1)
          .reduce((x, cp, index) => x + (index === pointIndex ? offsetIndex : cp.offsets.length), 0);
        return [
          selectedIndex,
          { path: points[pointIndex].path, offset: points[pointIndex].offsets[normalizedOffsetIndex] } as Point,
        ];
      }

      for (let j = 0; j < p.length; ++j) {
        if (reverse) {
          if (p[j] > sp[j]) break;
          if (p[j] < sp[j]) {
            selectedIndex = points
              .slice(0, i + 1)
              .reduce((x, cp, index) => x + (index === i ? 0 : cp.offsets.length), 0);
            return [selectedIndex, { path: points[i].path, offset: points[i].offsets[0] } as Point];
          }
        } else {
          if (p[j] < sp[j]) break;
          if (p[j] > sp[j]) {
            selectedIndex = points
              .slice(0, i + 1)
              .reduce((x, cp, index) => x + (index === i ? 0 : cp.offsets.length), 0);
            return [selectedIndex, { path: points[i].path, offset: points[i].offsets[0] } as Point];
          }
        }
      }
    }
    return [0, { path: points[0].path, offset: points[0].offsets[0] } as Point];
  }

  private static select(editor: SearchReplaceEditor, point: Point, searchLength: number, replaceAll = false): void {
    if (editor.searchType === SearchForTypes.AuthorialNotes) {
      const fnElements = document.querySelectorAll<HTMLElement>('.fn-mark');
      if (fnElements) {
        fnElements.forEach((elem) => elem.classList.remove('selected'));
        fnElements[editor.searchPosition]?.classList.add('selected');
        editor.selection = {
          anchor: point,
          focus: { path: point.path, offset: point.offset + searchLength },
        };
      }
    } else {
      if (!replaceAll) {
        Transforms.select(editor, { path: point.path, offset: point.offset });
        setTimeout(() =>
          Transforms.select(editor, {
            anchor: point,
            focus: { path: point.path, offset: point.offset + searchLength },
          }),
        );
      } else {
        Transforms.select(editor, { anchor: point, focus: { path: point.path, offset: point.offset + searchLength } });
      }
    }
  }

  private static setAuthorialNoteSearchPosition(editor: SearchReplaceEditor, format: string): number {
    if (format === SEARCH_PREV) {
      return editor.searchPosition === 0 ? editor.searchCount - 1 : editor.searchPosition - 1;
    }

    return editor.searchPosition === editor.searchCount ? 0 : editor.searchPosition + 1;
  }

  private static getNextPointIndex(i: number, points: SearchPoint[], matchingIndexExists: boolean) {
    if (matchingIndexExists) {
      return i;
    } else {
      return i + 1 === points.length ? 0 : i + 1;
    }
  }
}
