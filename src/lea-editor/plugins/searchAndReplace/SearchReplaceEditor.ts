// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Range } from 'slate';
import { ReactEditor } from 'slate-react';

import { SearchPoint } from '../../../interfaces';
import { SearchForTypes } from './searchReplaceOptions/SearchReplaceOptions';

export interface SearchReplaceEditor extends ReactEditor {
  searchText: string;
  searchPosition: number;
  searchCount: number;
  searchResults: SearchPoint[];
  lastSearchSelection: Range | null;
  searchType: SearchForTypes;
  search: (text?: string) => void;
  replace: (text?: string) => void;
  saveSelection: () => void;
  resetSearch: () => void;
}
