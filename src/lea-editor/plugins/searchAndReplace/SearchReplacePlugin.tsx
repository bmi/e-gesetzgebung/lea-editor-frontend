// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useRef } from 'react';
import { Editor, NodeEntry, Operation, Text, Transforms } from 'slate';

import { SearchPoint, LeaNode, Element } from '../../../interfaces';

import { Plugin, PluginRenderLeafProps, IPluginOptionDialogProps, PluginRange } from '../../plugin-core';
import { SearchReplaceEditor } from './SearchReplaceEditor';
import { SearchReplaceCommands } from './SearchReplaceCommands';
import { SearchDecorator } from './SearchDecorator';
import TimeoutHelper from '../../../helpers/TimeoutHelper';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { LeaDrawer, AriaController as AriaControllerLea } from '@lea/ui';
import { useTranslation } from 'react-i18next';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
  useEditorOptionsVisibleState,
} from '../../plugin-core/editor-store/editor-options';
import { EditorEvent } from '../../../interfaces/EditorEvent';
import { SearchReplaceOptions } from './searchReplaceOptions/SearchReplaceOptions';
import {
  useSearchReplaceDispatch,
  useSearchReplaceState,
} from '../../plugin-core/editor-store/workspace/search-replace';
import { MatchSearchType } from './utils';

const timer = new TimeoutHelper();

export class SearchReplacePlugin extends Plugin {
  // private static readonly Hotkeys = SEARCH_REPLACE_HOTKEYS;

  private readonly refs = Array(1)
    .fill(0)
    .map(() => React.createRef<HTMLElement>());

  public get toolRefs(): React.RefObject<HTMLElement>[] {
    return this.refs;
  }

  public create<T extends Editor>(editor: T): T & SearchReplaceEditor {
    const e = editor as T & SearchReplaceEditor;
    const { apply, saveSelection } = e;
    e.searchText = '';
    e.searchPosition = 0;
    e.searchCount = 0;
    e.searchResults = [];
    e.lastSearchSelection = null;

    e.apply = (op: Operation) => {
      e.searchPosition = 0;
      apply(op);
    };

    e.search = (text?: string) => {
      e.searchCount = 0;
      e.searchResults = [];

      if (!text) {
        e.searchText = '';
        return;
      }

      e.searchText = text;
      const searchResult = SearchReplacePlugin.search(e, { children: e.children } as Element, text, []);
      if (searchResult && searchResult.count > 0 && searchResult.points && searchResult.points.length > 0) {
        e.searchCount = searchResult.count;
        e.searchResults = searchResult.points;
        if (e.searchPosition > e.searchCount) {
          e.searchPosition = 0;
        }
      }
    };

    e.replace = (text?: string) => {
      if (!text || !e.searchText) {
        return;
      }

      e.deleteFragment();
      Transforms.insertText(e, text, { voids: true });
    };

    e.saveSelection = () => {
      e.lastSearchSelection = e.selection;
      saveSelection();
    };

    e.resetSearch = () => {
      e.searchText = '';
      e.searchPosition = 0;
      e.searchCount = 0;
      e.searchResults = [];
      e.lastSearchSelection = null;
    };

    return e;
  }

  public OptionDialog = ({ editor }: IPluginOptionDialogProps): ReactElement => {
    const { t } = useTranslation();
    const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.SEARCH_REPLACE);
    const { closeOptions } = useEditorOptionsVisibleChange(EditorOptions.SEARCH_REPLACE);
    const { setSearchCount, setSearchText, setSelectedSearchPosition } = useSearchReplaceDispatch();
    const { searchText } = useSearchReplaceState();
    const searchReplaceOptionRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
      searchReplaceOptionRef.current?.addEventListener('focus', SearchReplacePlugin.refreshSearch);
    }, []);

    useEffect(() => {
      AriaControllerPlateg.setAriaLabelsByQuery(
        '.search-replace-options .ant-drawer-close',
        'Suchen / Ersetzen Optionen schließen',
      );
      setTimeout(
        () =>
          searchReplaceOptionRef.current &&
          AriaControllerLea.setElementIdByClassNameInElement(
            'ant-drawer-close',
            'lea-drawer-search-replace-option-close-button',
            searchReplaceOptionRef.current,
          ),
      );
    }, [optionsVisible]);

    const handleFocus = () => {
      search(editor, searchText);
    };

    const handleClose = () => {
      setSearchCount(0);
      setSearchText('');
      setSelectedSearchPosition(0);
      editor.resetSearch();
      closeOptions();
    };

    const search = (editor: SearchReplaceEditor, searchText: string) => {
      if (searchText !== '' && editor) {
        SearchReplaceCommands.search(editor, searchText);
        setSearchCount(editor.searchCount);
      }
    };

    return (
      <LeaDrawer
        className="search-replace-options"
        placement={'right'}
        open={optionsVisible}
        onClose={handleClose}
        title={t('lea.searchReplaceOptions.ariaLabel')}
        expandable={true}
        refProp={searchReplaceOptionRef}
        ariaLabel={t('lea.searchReplaceOptions.ariaLabel')}
        onFocus={handleFocus}
      >
        {editor && <SearchReplaceOptions editor={editor} />}
      </LeaDrawer>
    );
  };

  public renderLeaf({ attributes, children, leaf }: PluginRenderLeafProps): React.ReactElement | null {
    let style: React.CSSProperties = {};
    if (leaf.highlight) {
      style = {
        backgroundColor: '#fcff3e',
        border: '1px solid black',
      };
      return (
        <span {...attributes} style={style}>
          {children}
        </span>
      );
    }
    return null;
  }

  public decorate = (entry: NodeEntry<Element>, editor: SearchReplaceEditor, searchString?: string): PluginRange[] => {
    const [node, path] = entry;
    if (searchString === '' || !Text.isText(node) || !MatchSearchType(editor, node, path)) return [];
    return SearchDecorator(node, path, searchString);
  };

  private static search = (editor: SearchReplaceEditor, node: LeaNode, text: string, leafs: number[]) => {
    if (Text.isText(node) && MatchSearchType(editor, node, leafs)) {
      const partPoints: number[] = [];
      const parts = node.text.split(text);
      let offset = 0;
      parts.forEach((part, i) => {
        if (i !== 0) {
          partPoints.push(offset - text.length);
        }
        offset = offset + part.length + text.length;
      });
      return partPoints.length === 0
        ? { count: 0, points: [] }
        : { count: partPoints.length, points: [{ path: leafs, offsets: partPoints } as SearchPoint] };
    }
    if ('children' in node) {
      let points: SearchPoint[] = [];
      let l = 0;
      let count = 0;
      for (const n of node.children) {
        const searchResult = SearchReplacePlugin.search(editor, n, text, [...leafs, l]);
        if (searchResult !== undefined) {
          const { count: c, points: point } = searchResult;
          count += c;
          points = [...points, ...point];
        }
        ++l;
      }
      return { count, points };
    }
    return undefined;
  };

  private static refreshSearch = (e: CustomEventInit<EditorEvent>): void => {
    if (!e.detail) {
      return;
    }

    const { editor } = e.detail;
    if ((editor as SearchReplaceEditor).searchText) {
      const sre = editor as SearchReplaceEditor;
      timer.resetTimer();
      timer.setTimer(() => sre.search(sre.searchText), 1000);
    }
  };
}
