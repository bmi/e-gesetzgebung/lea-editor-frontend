// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect } from 'react';

import { Collapse } from 'antd';
import { useTranslation } from 'react-i18next';
import { SelectWrapper, HiddenInfoComponent } from '@plateg/theme';
import { SearchForTypes } from './SearchReplaceOptions';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';
import {
  useSearchReplaceDispatch,
  useSearchReplaceState,
} from '../../../plugin-core/editor-store/workspace/search-replace';
import {
  useAuthorialNotePageChange,
  useAuthorialNotePageState,
  useEditorDocumentState,
  useEditorOnFocusState,
} from '../../../plugin-core';
import { SearchReplaceCommands } from '../SearchReplaceCommands';
import { SearchReplaceEditor } from '../SearchReplaceEditor';

const { Panel } = Collapse;

interface Props {
  editor: SearchReplaceEditor;
}

export const SearchType = ({ editor }: Props): React.JSX.Element => {
  const { t } = useTranslation();
  const { setSearchType, setSearchCount } = useSearchReplaceDispatch();
  const { searchText, searchType } = useSearchReplaceState();
  const { visible } = useAuthorialNotePageState();
  const { openAuthorialNotePage } = useAuthorialNotePageChange();
  const editorOnFocus = useEditorOnFocusState();
  const { editorDocument } = useEditorDocumentState(editorOnFocus.index);

  useEffect(() => {
    if (editorDocument?.isReadonly) {
      setSearchType(SearchForTypes.TextReadonly);
    }
  }, [editorDocument]);

  const searchForOptions = [
    {
      id: SearchForTypes.Text,
      title: t('lea.searchReplaceOptions.searchFor.options.fliesstext'),
      disabled: editorDocument?.isReadonly,
    },
    {
      id: SearchForTypes.TextReadonly,
      title: t('lea.searchReplaceOptions.searchFor.options.fliesstextReadonly'),
      disabled: false,
    },
  ];

  useEffect(() => {
    if (searchText !== '' && editor) {
      editor.searchType = searchType;
      SearchReplaceCommands.search(editor, searchText);
      setSearchCount(editor.searchCount);
    }
  }, [searchType]);

  const onChangeHandler = (value: SearchForTypes) => {
    setSearchType(value);
    if (value === SearchForTypes.AuthorialNotes && !visible) {
      openAuthorialNotePage();
    }
  };

  return (
    <div className="search-replace options-collapse search-box">
      <div className="search-for-panel-header">
        <h4>{t('lea.searchReplaceOptions.searchFor.title')}</h4>
        <span className="infoSearchFor">
          <HiddenInfoComponent
            title={t(`lea.searchReplaceOptions.searchFor.infoComponent.title`)}
            text={t(`lea.searchReplaceOptions.searchFor.infoComponent.text`)}
            customSelector=".options-collapse"
          />
        </span>
      </div>
      <SelectWrapper
        key="search-for-options"
        className="search-for-options"
        defaultValue={!editorDocument?.isReadonly ? SearchForTypes.Text : SearchForTypes.TextReadonly}
        suffixIcon={<SelectDown />}
        onChange={(value: SearchForTypes) => onChangeHandler(value)}
        options={searchForOptions.map((option) => ({
          label: (
            <span key={option.id} aria-label={option.title} className="search-for-select-item">
              {option.title}
            </span>
          ),
          value: option.id,
          title: option.title,
          disabled: option.disabled,
        }))}
      />
    </div>
  );
};
