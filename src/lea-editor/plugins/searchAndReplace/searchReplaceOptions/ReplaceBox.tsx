// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, useRef, useEffect } from 'react';

import { Input, Collapse } from 'antd';
import { REPLACE_ONE, REPLACE_ALL } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { ReplaceButton } from './SearchButton';
import { ISearchReplaceProps, SearchForTypes } from './SearchReplaceOptions';
import { ArrowRightBlackIcon, ArrowDownBlackIcon } from '@lea/ui';
import { useSearchReplaceState } from '../../../plugin-core/editor-store/workspace/search-replace';
import { useEditorDocumentState, useEditorOnFocusState } from '../../../plugin-core';

const { Panel } = Collapse;

export const ReplaceBox = ({ editor }: ISearchReplaceProps): React.JSX.Element => {
  const { t } = useTranslation();
  const [replace, setReplace] = useState<string | undefined>();
  const { searchType } = useSearchReplaceState();
  const [showReplace, setShowReplace] = useState(true);
  const replaceNextRef = useRef<HTMLElement>(null);
  const editorOnFocus = useEditorOnFocusState();
  const { editorDocument } = useEditorDocumentState(editorOnFocus.index);

  useEffect(() => {
    if (searchType === SearchForTypes.TextReadonly || editorDocument?.isReadonly) {
      setShowReplace(false);
    } else {
      setShowReplace(true);
    }
  }, [searchType, editorDocument]);

  const setExpandIcon = (isActive?: boolean) => {
    return (
      <div className="lea-icon">
        {isActive ? <ArrowDownBlackIcon hasHover={false} /> : <ArrowRightBlackIcon hasHover={false} />}
      </div>
    );
  };

  return showReplace ? (
    <Collapse
      className="search-replace options-collapse"
      bordered={false}
      ghost={true}
      size="small"
      expandIcon={({ isActive }) => setExpandIcon(isActive)}
    >
      <Panel className="search-replace-item" header={<h4>{t('lea.searchReplaceOptions.replace.title')}</h4>} key="1">
        <Input
          id="replace-text-input"
          type="text"
          placeholder="Ersetzen durch..."
          aria-label="Ersetzen durch"
          onKeyDown={(e) => {
            if (e.key === 'Enter' && replaceNextRef.current) {
              replaceNextRef.current.focus();
            }
          }}
          onChange={(e) => setReplace(e.target.value)}
        />
        <div className="action-buttons">
          <ReplaceButton
            ref={replaceNextRef}
            label="Nächstes Vorkommen ersetzen"
            id="replace-one-button"
            format={REPLACE_ONE}
            text={replace}
            editor={editor}
          >
            {t('lea.searchReplaceOptions.replace.buttons.replace')}
          </ReplaceButton>
          <ReplaceButton
            id="replace-all-button"
            label="Alle Vorkommen ersetzen"
            format={REPLACE_ALL}
            text={replace}
            editor={editor}
          >
            {t('lea.searchReplaceOptions.replace.buttons.replaceAll')}
          </ReplaceButton>
        </div>
      </Panel>
    </Collapse>
  ) : (
    <></>
  );
};
