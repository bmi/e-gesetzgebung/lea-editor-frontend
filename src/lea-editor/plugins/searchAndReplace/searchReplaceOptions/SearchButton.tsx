// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Button } from 'antd';

import { ButtonProps } from '../../../../interfaces/ButtonProps';

import { SearchReplaceEditor } from '../SearchReplaceEditor';
import { SearchReplaceCommands } from '../SearchReplaceCommands';

import {
  useSearchReplaceDispatch,
  useSearchReplaceState,
} from '../../../plugin-core/editor-store/workspace/search-replace';

interface SearchReplaceButtonProps extends ButtonProps {
  editor: SearchReplaceEditor;
}

export const SearchButton = React.forwardRef<HTMLElement, SearchReplaceButtonProps>(
  ({ id, format, label, children, editor, ...rest }: SearchReplaceButtonProps, ref) => {
    const { setSelectedSearchPosition } = useSearchReplaceDispatch();
    const { searchCount } = useSearchReplaceState();
    return (
      <Button
        {...rest}
        ref={ref}
        id={id}
        type="primary"
        aria-label={label}
        disabled={searchCount === 0}
        onClick={(event: React.MouseEvent<HTMLElement, MouseEvent>) => {
          event.preventDefault();
          SearchReplaceCommands.selectNextSearchResult(editor, format);
          setSelectedSearchPosition(editor.searchPosition);
          // Give the UI thread time to render and to focus the selected text in the editor, then focus the current button
          setButtonFocus(ref as React.RefObject<HTMLElement>);
        }}
      >
        {children}
      </Button>
    );
  },
);

export const ReplaceButton = React.forwardRef<HTMLElement, SearchReplaceButtonProps>(
  ({ id, format, label, text, children, editor, ...rest }: SearchReplaceButtonProps, ref) => {
    const { setSelectedSearchPosition, setSearchCount } = useSearchReplaceDispatch();
    return (
      <Button
        {...rest}
        ref={ref}
        id={id}
        type="primary"
        aria-label={label}
        onClick={(event: React.MouseEvent<HTMLElement, MouseEvent>) => {
          event.preventDefault();
          SearchReplaceCommands.replace(editor, format, text);
          setSelectedSearchPosition(editor.searchPosition);
          setSearchCount(editor.searchCount);
          // Give the UI thread time to render and to focus the selected text in the editor, then focus the current button
          setButtonFocus(ref as React.RefObject<HTMLElement>);
        }}
      >
        {children}
      </Button>
    );
  },
);

function setButtonFocus(ref: React.RefObject<HTMLElement>) {
  if (ref?.current) {
    setTimeout(() => ref.current?.focus(), 250);
  }
}
