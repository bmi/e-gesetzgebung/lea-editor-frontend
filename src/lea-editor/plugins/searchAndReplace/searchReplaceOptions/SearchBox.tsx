// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useRef } from 'react';
import { Input } from 'antd';

import { useTranslation } from 'react-i18next';
import { SEARCH_NEXT, SEARCH_PREV } from '../../../../constants';
import {
  useSearchReplaceDispatch,
  useSearchReplaceState,
} from '../../../plugin-core/editor-store/workspace/search-replace';
import { SearchButton } from './SearchButton';
import { ISearchReplaceProps } from './SearchReplaceOptions';

export const SearchBox = ({ editor }: ISearchReplaceProps): React.JSX.Element => {
  const { t } = useTranslation();
  const { setSearchText } = useSearchReplaceDispatch();
  const { searchText, searchCount, selectedSearchPosition } = useSearchReplaceState();
  const searchNextRef = useRef<HTMLElement>(null);
  const replacePrevRef = useRef<HTMLElement>(null);

  const getResultText = () => {
    if (searchCount === 0) {
      return t('lea.searchReplaceOptions.search.results.no');
    } else if (selectedSearchPosition === 0) {
      return searchCount > 1
        ? t('lea.searchReplaceOptions.search.results.multiple', { count: searchCount })
        : t('lea.searchReplaceOptions.search.results.single');
    } else {
      return t('lea.searchReplaceOptions.search.results.from', {
        searchPosition: selectedSearchPosition,
        count: searchCount,
      });
    }
  };

  return (
    <div className="search-box">
      <div className="header">
        <h4 id="search-for-label">{t('lea.searchReplaceOptions.search.title')}</h4>
        <span id="results">{getResultText()}</span>
      </div>
      <Input
        id="search-text-input"
        type="text"
        placeholder="Suchen nach..."
        aria-labelledby="search-for-label"
        aria-describedby="results"
        value={searchText}
        onKeyDown={(e) => {
          if (e.key === 'Enter' && searchNextRef.current) {
            searchNextRef.current.focus();
          }
        }}
        onChange={(e) => setSearchText(e.target.value)}
      />
      <div className="action-buttons">
        <SearchButton
          ref={replacePrevRef}
          label="Zurück zum vorherigen Vorkommen"
          id="search-previous-button"
          aria-describedby="results"
          format={SEARCH_PREV}
          editor={editor}
        >
          {t('lea.searchReplaceOptions.search.buttons.prev')}
        </SearchButton>
        {
          <SearchButton
            ref={searchNextRef}
            label="Weiter zum nächsten Vorkommen"
            id="search-next-button"
            aria-describedby="results"
            format={SEARCH_NEXT}
            editor={editor}
          >
            {t('lea.searchReplaceOptions.search.buttons.next')}
          </SearchButton>
        }
      </div>
    </div>
  );
};
