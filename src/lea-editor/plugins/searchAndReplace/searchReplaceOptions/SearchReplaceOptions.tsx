// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Divider } from 'antd';
import React, { useEffect, useState } from 'react';
import { SearchReplaceCommands } from '../SearchReplaceCommands';
import { SearchReplaceEditor } from '../SearchReplaceEditor';
import { useDebounce } from '../useDebounce';
import {
  useSearchReplaceDispatch,
  useSearchReplaceState,
} from '../../../plugin-core/editor-store/workspace/search-replace';
import TimeoutHelper from '../../../../helpers/TimeoutHelper';
import { EditorOptions, useEditorOptionsVisibleState } from '../../../plugin-core/editor-store/editor-options';

import './SearchReplaceOptions.less';
import { SearchType } from './SearchType';
import { SearchBox } from './SearchBox';
import { ReplaceBox } from './ReplaceBox';

export interface ISearchReplaceProps {
  editor: SearchReplaceEditor;
}

export enum SearchForTypes {
  Text,
  TextReadonly,
  Comments,
  AuthorialNotes,
}

export const SearchReplaceOptions = ({ editor }: ISearchReplaceProps): React.JSX.Element => {
  const { setSelectedSearchPosition, setSearchCount } = useSearchReplaceDispatch();
  const { searchText, selectedSearchPosition, searchCount, searchType } = useSearchReplaceState();
  const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.SEARCH_REPLACE);
  const [timer] = useState(new TimeoutHelper());
  const debouncedSearchText = useDebounce(searchText || '', 500);

  useEffect(() => {
    SearchReplaceCommands.search(editor, debouncedSearchText);
    setSelectedSearchPosition(editor.searchPosition);
    setSearchCount(editor.searchCount);
  }, [debouncedSearchText]);

  useEffect(() => {
    editor.searchType = searchType;
  }, [searchType]);

  useEffect(() => {
    if (editor.searchPosition === 0) {
      timer.resetTimer();
      timer.setTimer(() => {
        SearchReplaceCommands.search(editor, editor.searchText);
        setSearchCount(editor.searchCount);
      }, 1000);
    }
  }, [selectedSearchPosition]);

  useEffect(() => {
    setSelectedSearchPosition(editor.searchPosition);
    setSearchCount(editor.searchCount);
  }, [selectedSearchPosition, searchCount]);

  return optionsVisible ? (
    <>
      <SearchType editor={editor} />
      <Divider className="option-divider" />
      <SearchBox editor={editor} />
      <ReplaceBox editor={editor} />
      <Divider className="option-divider" />
    </>
  ) : (
    <></>
  );
};
