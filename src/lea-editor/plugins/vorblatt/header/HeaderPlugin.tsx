// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../plugin-core';
import { HeaderQueries as Queries } from './HeaderQueries';

import { Unit, isFirstTextPosition, isLastPosition } from '../../../plugin-core/utils';
import { Element } from '../../../../interfaces';

export class HeaderPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstTextPosition(editor)) {
        deleteBackward(unit);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public keyDown(editor: Editor): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const fromEgfa = e['lea:fromEgfa'] ? 'fromEgfa' : '';
    const guid = e.GUID;

    if (Queries.isHeaderEditable(props.editor, elementType, e['lea:editable']) && !this.editorDocument.isReadonly) {
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark} contentEditable ${fromEgfa}`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role="textbox"
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else {
      return null;
    }
  };
}
