// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import { isOfType, isWithinEditableNode } from '../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../../plugins/begruendung/BegruendungStructure';

export class HeaderQueries {
  public static isHeaderEditable(editor: Editor, type?: string, editable?: boolean): boolean {
    if (!editable) return false;
    const isHeading = type ? type === BEGRUENDUNG_STRUCTURE.HEADING : isOfType(editor, BEGRUENDUNG_STRUCTURE.HEADING);
    return isHeading && editable;
  }

  public static isEditable(editor: Editor): boolean {
    const isHeading = isOfType(editor, BEGRUENDUNG_STRUCTURE.HEADING);
    const isEditable = isWithinEditableNode(editor);
    return isHeading && isEditable;
  }
}
