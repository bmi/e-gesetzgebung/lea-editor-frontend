// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Node } from 'slate';
import { DataNode } from 'antd/lib/tree';

import { Element, TextWrapper } from '../../../../interfaces';
import { VORBLATT_STRUCTURE } from '../VorblattStructure';

export class TableOfContentsCommands {
  public static getContentTableData(e: Editor): DataNode[] {
    const treeData: DataNode[] = [];
    if (e.children && e.children.length > 0) {
      (e.children as Element[]).forEach((n: Element, index: number) => {
        TableOfContentsCommands.createContentTable(n, treeData, [index]);
      });
    }
    return treeData;
  }

  private static createContentTable(node: Element, array: DataNode[] | undefined, path: number[]) {
    if (!node.children || !array) {
      return;
    }

    const key = path.join('-');
    if (node.type === VORBLATT_STRUCTURE.HCONTAINER || node.type === VORBLATT_STRUCTURE.TBLOCK) {
      if (
        node.children.length > 1 &&
        (node.children[0] as Element).type === VORBLATT_STRUCTURE.NUM &&
        (node.children[1] as Element).type === VORBLATT_STRUCTURE.HEADING
      ) {
        const numText = Node.string(node.children[0]);
        const headingText = Node.string(node.children[1]);

        array.push({
          title: `${numText} ${headingText}`,
          key,
          children: [],
        });
      }
    }

    const i = array.length - 1;
    node.children.forEach((n: Element | TextWrapper, index: number) => {
      TableOfContentsCommands.createContentTable(
        n as Element,
        array[i]?.children && key.startsWith(array[i].key.toString()) ? array[i].children : array,
        [...path, index],
      );
    });
  }
}
