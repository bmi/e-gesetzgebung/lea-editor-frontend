// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { Plugin, PluginProps, PElementCommands, CombinedPluginRenderProps } from '../../../plugin-core';
import { isEnter, isFirstPosition, isFirstSiblingOfType, isLastPosition, Unit } from '../../../plugin-core/utils';
import { VORBLATT_STRUCTURE } from '../VorblattStructure';
import { VorblattAbsatzQueries as Queries } from '.';
import { Element } from '../../../../interfaces';

export class VorblattAbsatzPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, VORBLATT_STRUCTURE.PARAGRAPH)) {
        PElementCommands.mergeAbsaetze(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isEnter(event) && isLastPosition(editor)) {
      PElementCommands.insertAbsatz(editor);
      event.preventDefault();
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const fromEgfa = e['lea:fromEgfa'] ? 'fromEgfa' : '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable ${fromEgfa}`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          aria-label="Juristischer Absatz"
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          aria-label="Juristischer Absatz"
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
