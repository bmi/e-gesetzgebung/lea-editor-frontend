// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';

import { isOfType, isWithinTypeHierarchy, isWithinNodeOfType } from '../../../plugin-core/utils';
import { VORBLATT_STRUCTURE } from '../VorblattStructure';
import { TABELLE_STRUCTURE } from '../../../plugin-core/generic-plugins/tabelle';

export class VorblattAbsatzQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === VORBLATT_STRUCTURE.PARAGRAPH : isOfType(editor, VORBLATT_STRUCTURE.PARAGRAPH);
    return (
      isPElement &&
      (isWithinTypeHierarchy(editor, [VORBLATT_STRUCTURE.CONTENT, VORBLATT_STRUCTURE.HCONTAINER], path) ||
        isWithinTypeHierarchy(editor, [VORBLATT_STRUCTURE.TBLOCK, VORBLATT_STRUCTURE.HCONTAINER], path)) &&
      !isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE, path)
    );
  }
}
