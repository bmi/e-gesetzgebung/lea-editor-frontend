// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EntityState } from '@reduxjs/toolkit';
import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../components/Store';
import { Element } from '../../../interfaces';
import { EditorPageState, PluginState } from '../../plugin-core';
import { authorialNoteAdapter, addAuthorialNote, removeAuthorialNote, AuthorialNoteSlice } from './AuthorialNoteSlice';
import { EqualityFn, shallowEqual } from 'react-redux';

type UseAuthorialNoteChange = {
  addAuthorialNote: (authorialNoteRef: Element) => void;
  removeAuthorialNote: (id: string) => void;
};

interface AuthorialNotes {
  [name: string]: Element[];
}

const equal: EqualityFn<AuthorialNotes> = (a, b) => {
  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);
  if (aKeys.length !== bKeys.length || !aKeys.every((aKey) => bKeys.includes(aKey))) return false;
  return aKeys.every((aKey) => shallowEqual(a[aKey], b[aKey]));
};

export const useAuthorialNoteState = (): AuthorialNotes => {
  const authorialNotes = useAppSelector<AuthorialNotes>((state) => {
    const indizes = (state.editorPage as EditorPageState)?.pluginState
      ? Object.keys((state.editorPage as EditorPageState)?.pluginState)
      : [];
    const authorialNotes: AuthorialNotes = {};
    indizes.forEach((index) => {
      authorialNotes[index] = authorialNoteAdapter
        .getSelectors()
        .selectAll(
          ((state.editorPage as EditorPageState)?.pluginState?.[index] as PluginState)?.[
            AuthorialNoteSlice(Number(index)).name
          ] as EntityState<Element>,
        );
    });
    return authorialNotes;
  }, equal);
  return authorialNotes;
};

export const useAuthorialNoteAtState = (editorDocumentIndex: number, id: string): Element | undefined => {
  const authorialNote = useAppSelector((state) =>
    authorialNoteAdapter
      .getSelectors()
      .selectById(
        (((state.editorPage as EditorPageState)?.pluginState?.[editorDocumentIndex] as PluginState)?.[
          AuthorialNoteSlice(editorDocumentIndex).name
        ] as EntityState<Element>) || authorialNoteAdapter.getInitialState(),
        id,
      ),
  );

  return authorialNote;
};

export const useAuthorialNoteDispatch = (editorDocumentIndex: number): UseAuthorialNoteChange => {
  const dispatch = useAppDispatch();

  const addAuthorialNoteCallback = useCallback(
    (authorialNoteElement: Element) => {
      const action = addAuthorialNote(editorDocumentIndex);
      dispatch(action(authorialNoteElement));
    },
    [editorDocumentIndex],
  );
  const removeAuthorialNoteCallback = useCallback(
    (id: string) => {
      const action = removeAuthorialNote(editorDocumentIndex);
      dispatch(action(id));
    },
    [editorDocumentIndex],
  );

  return {
    addAuthorialNote: addAuthorialNoteCallback,
    removeAuthorialNote: removeAuthorialNoteCallback,
  };
};
