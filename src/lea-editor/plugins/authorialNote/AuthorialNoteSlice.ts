// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createAction, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { Element } from '../../../interfaces';

const enum AuthorialNotesActions {
  ADD = 'AuthorialNotes/Add',
  REMOVE = '"AuthorialNotes/Remove"',
}

export const authorialNoteAdapter = createEntityAdapter<Element>({
  selectId: (authorialNoteWithRef) => authorialNoteWithRef.GUID || '',
});

export const addAuthorialNote = (editorDocumentIndex: number) =>
  createAction<Element>(`${editorDocumentIndex}/${AuthorialNotesActions.ADD}`);
export const removeAuthorialNote = (editorDocumentIndex: number) =>
  createAction<string>(`${editorDocumentIndex}/${AuthorialNotesActions.REMOVE}`);

export const AuthorialNoteSlice = (editorDocumentIndex: number) =>
  createSlice({
    name: 'authorialNotes',
    initialState: authorialNoteAdapter.getInitialState(),
    reducers: {},
    extraReducers: (builder) =>
      builder
        .addCase(addAuthorialNote(editorDocumentIndex), (state, action) => {
          authorialNoteAdapter.addOne(state, action.payload);
        })
        .addCase(removeAuthorialNote(editorDocumentIndex), (state, action) => {
          authorialNoteAdapter.removeOne(state, action.payload);
        }),
  });
