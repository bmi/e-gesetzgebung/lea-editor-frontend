// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useCallback, useEffect, useMemo, useRef, useState } from 'react';

import { useTranslation } from 'react-i18next';

import {
  IPluginContextMenuProps,
  IPluginToolbarProps,
  Plugin,
  useEditableState,
  EditorWizards,
  PluginProps,
  useEditorOnFocusState,
  CombinedPluginRenderProps,
} from '../../plugin-core';
import { AuthorialNoteCommands as Commands } from './AuthorialNoteCommands';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CreateAuthorialNoteIcon, IconDropdown } from '@lea/ui';
import { Element } from '../../../interfaces';
import { AUTHORIALNOTE_STRUCTURE } from './AuthorialNoteStructure';
import {
  getLowestNodeAt,
  isEnter,
  isEscape,
  isFirstTextPosition,
  isLastTextPosition,
  isLeftNavigation,
  isLeftOrRightNavigation,
  isNextNeighborOfType,
  isPreviousNeighborOfType,
  isRightNavigation,
  isTab,
  Unit,
} from '../../plugin-core/utils';
import { useWizardVisibleChange } from '../../plugin-core/editor-store/editor-wizards/useEditorWizardVisible';
import { useAuthorialNotePageChange, useAuthorialNotePageState } from '../../plugin-core/editor-store/workspace';
import { useAuthorialNoteAtState, useAuthorialNoteDispatch } from './useAuthorialNote';
import { AuthorialNoteSlice } from './AuthorialNoteSlice';
import { ReducersMapObject } from '@reduxjs/toolkit';
import { ItemType } from 'antd/lib/menu/interface';
import { Checkbox } from 'antd';

export class AuthorialNotePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;
    editor.isVoid = (element) => {
      if ((element as Element).type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE) return true;
      return false;
    };

    editor.deleteBackward = (unit: Unit) => {
      if (isFirstTextPosition(editor) && isPreviousNeighborOfType(editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE)) {
        Commands.deletePreviousAuthorialNote(editor, this.removeAuthorialNoteRef);
      } else {
        deleteBackward(unit);
      }
    };

    editor.deleteForward = (unit: Unit) => {
      const node = getLowestNodeAt(editor);
      if (
        isLastTextPosition(editor, node ? node.text : '') &&
        isNextNeighborOfType(editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE)
      ) {
        Commands.deleteNextAuthorialNote(editor, this.removeAuthorialNoteRef);
      } else {
        deleteForward(unit);
      }
    };
    return editor;
  }

  private removeAuthorialNoteRef: (id: string) => void = (_id: string) => {};

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (!isLeftOrRightNavigation(event)) {
      return false;
    }
    const textNode = getLowestNodeAt(editor);
    if (isLeftNavigation(event) && isFirstTextPosition(editor)) {
      event.preventDefault();
      Commands.focusPreviousElement(editor, this.editorDocument.index);
      ReactEditor.focus(editor as ReactEditor);
      return false;
    } else if (isRightNavigation(event) && isLastTextPosition(editor, textNode?.text || '')) {
      event.preventDefault();
      Commands.focusNextElement(editor, this.editorDocument.index);
      ReactEditor.focus(editor as ReactEditor);
      return false;
    }
    return true;
  }

  public ContextMenu = ({ editor, keyName }: IPluginContextMenuProps): ReactElement => {
    const { t } = useTranslation();
    const editorOnFocus = useEditorOnFocusState();
    const { openWizard } = useWizardVisibleChange(EditorWizards.AUTHORIALNOTE);
    const { openAuthorialNotePage } = useAuthorialNotePageChange();
    const { isEditable } = useEditableState(this.editorDocument.index);

    const handleNumberedAuthorialNote = () => {
      Commands.addNumberedAuthorialNote(editor);
      openAuthorialNotePage();
    };

    useMemo(() => {
      if (editorOnFocus.index === this.editorDocument.index && !this.editorDocument.isReadonly && isEditable) {
        this._contextMenuItemTypes = [
          {
            key: `${keyName}-create-numbered-authorialNote-contextMenu`,
            title: t('lea.contextMenu.authorialNote.createNumbered'),
            onClick: handleNumberedAuthorialNote,
            label: t('lea.contextMenu.authorialNote.createNumbered'),
          },
          {
            key: `${keyName}-create-custom-authorialNote-contextMenu`,
            title: t('lea.contextMenu.authorialNote.createCustom'),
            label: t('lea.contextMenu.authorialNote.createCustom'),
            onClick: openWizard,
          },
        ];
      } else {
        this._contextMenuItemTypes = null;
      }
    }, [editorOnFocus, isEditable]);

    return <></>;
  };

  public ToolbarTools = ({ editor }: IPluginToolbarProps): ReactElement | null => {
    const { t } = useTranslation();
    const editorOnFocus = useEditorOnFocusState();
    const { isEditable } = useEditableState(this.editorDocument.index);
    const { openWizard } = useWizardVisibleChange(EditorWizards.AUTHORIALNOTE);
    const { openAuthorialNotePage } = useAuthorialNotePageChange();
    const [hasHover, setHasHover] = useState(false);
    const [isActive, onActive] = useState(false);

    const buttonRef = useRef<HTMLButtonElement>(null);

    useEffect(() => {
      buttonRef.current?.addEventListener('focus', () => setHasHover(true));
      buttonRef.current?.addEventListener('blur', () => setHasHover(false));
    }, []);

    const handleAddNumberedAuthorialNote = useCallback(() => {
      openAuthorialNotePage();
      Commands.addNumberedAuthorialNote(editor);
      ReactEditor.focus(editor);
    }, [editor]);

    const handleSetFormatAuthorialNoteVisible = useCallback(() => {
      openWizard();
    }, []);

    const handleClick = useCallback(() => {
      onActive(false);
    }, []);

    useEffect(() => {
      if (isEditable) {
        setHasHover(false);
        onActive(false);
      }
    }, [isEditable]);

    const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
      const event = e.nativeEvent;
      if (isTab(event)) {
        event.preventDefault();
        event.stopPropagation();
      }
      if (isEscape(event)) {
        setTimeout(() => buttonRef.current?.focus());
      }
    };

    const getItemTypes = (): ItemType[] => {
      return [
        {
          key: 'lea-add-numbered-authorialNote',
          title: t('lea.toolbar.labels.authorialNote.numbered'),
          label: t('lea.toolbar.labels.authorialNote.numbered'),
          onClick: () => handleAddNumberedAuthorialNote(),
        },
        {
          key: 'lea-add-custom-authorialNote',
          title: t('lea.toolbar.labels.authorialNote.custom'),
          label: t('lea.toolbar.labels.authorialNote.custom'),
          onClick: () => handleSetFormatAuthorialNoteVisible(),
        },
      ];
    };

    return editorOnFocus.index === this.editorDocument.index && !this.editorDocument.isReadonly ? (
      <IconDropdown
        id="authorial-dropdown"
        hasHover={isEditable && hasHover}
        items={getItemTypes()}
        onHover={setHasHover}
        isActive={isActive}
        onActive={onActive}
        onClick={handleClick}
        keyDownHandler={keyDownHandler}
        selectable={false}
        icon={<CreateAuthorialNoteIcon hasHover={isEditable && hasHover} active={isActive} />}
        disabled={!isEditable}
        buttonRef={buttonRef}
        ariaLabel={t('lea.toolbar.labels.authorialNote.ariaLabel')}
      />
    ) : null;
  };

  public ViewTools = (): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const { visible } = useAuthorialNotePageState();
    const { toggelAuthorialNotePage } = useAuthorialNotePageChange();

    if (this.editorDocument.index === editorOnFocus.index) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox onChange={toggelAuthorialNotePage} checked={visible}>
              Fußnoten
            </Checkbox>
          ),
          key: 'viewTools-authorialNotesPage',
          title: 'Fußnoten',
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { removeAuthorialNote: removeAuthorialNoteRef } = useAuthorialNoteDispatch(this.editorDocument.index);

    useEffect(() => {
      this.removeAuthorialNoteRef = removeAuthorialNoteRef;
    }, []);
    return <></>;
  };

  public reducer(): ReducersMapObject {
    const slice = AuthorialNoteSlice(this.editorDocument.index);
    return {
      [slice.name]: slice.reducer,
    };
  }

  public renderElement = ({
    element: elementProp,
    attributes,
    children,
    editor,
  }: CombinedPluginRenderProps): ReactElement | null => {
    const element = elementProp as Element;
    const guid = element.GUID;
    const firstFocus = useRef(false);
    const onSelect = (e: React.MouseEvent) => {
      const area = document.getElementById(`${this.editorDocument.index}:authorialNote:${guid ?? ''}`);
      area?.focus();
      e.stopPropagation();
      e.preventDefault();
    };
    const keyDown = (e: React.KeyboardEvent) => {
      const event = e as unknown as KeyboardEvent;
      if (isEnter(event)) {
        const area = document.getElementById(`${this.editorDocument.index}:authorialNote:${guid ?? ''}`);
        area?.focus();
      } else if (isLeftNavigation(event)) {
        event.preventDefault();
        Commands.focusBeforeAuthorialNote(editor, guid);
        ReactEditor.focus(editor);
      } else if (isRightNavigation(event)) {
        event.preventDefault();
        Commands.focusAfterAuthorialNote(editor, guid);
        ReactEditor.focus(editor);
      }
    };
    if (element.type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE && guid) {
      const { addAuthorialNote } = useAuthorialNoteDispatch(this.editorDocument.index);
      const authorialNote = useAuthorialNoteAtState(this.editorDocument.index, guid);
      !authorialNote && setTimeout(() => addAuthorialNote(element));
      return (
        <a
          id={`${this.editorDocument.index}:${guid}`}
          {...attributes}
          className={element.type}
          contentEditable={false}
          title="Fußnote"
          tabIndex={0}
          aria-label={`Fußnote ${element.marker?.toString() ?? ''}`}
          onKeyDown={keyDown}
          onClick={(e: React.MouseEvent) => onSelect(e)}
          onFocus={() => {
            firstFocus.current = !firstFocus.current;
            if (firstFocus.current) {
              (document.activeElement as HTMLElement).blur();
              setTimeout(() => document.getElementById(`${this.editorDocument.index}:${guid}`)?.focus(), 10);
            }
          }}
        >
          {element.marker}
          {children}
        </a>
      );
    }
    return null;
  };
}
