// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';

export const AUTHORIALNOTE_STRUCTURE = {
  AUTHORIALNOTE: STRUCTURE_ELEMENTS.AUTHORIALNOTE,
  P: STRUCTURE_ELEMENTS.P,
  TEXTWRAPPER: STRUCTURE_ELEMENTS.TEXTWRAPPER,
};
