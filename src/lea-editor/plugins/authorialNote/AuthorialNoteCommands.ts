// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BasePoint, Editor, NodeEntry, Path, Range, Transforms } from 'slate';
import { v4 as uuid } from 'uuid';
import { Element } from '../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getFirstLocationAt,
  getLastLocationAt,
  getLowestNodeAt,
  getLowestNodeEntryAt,
} from '../../plugin-core/utils';
import { AUTHORIALNOTE_STRUCTURE } from './AuthorialNoteStructure';
export class AuthorialNoteCommands {
  private static split(str: string, index: number) {
    return [str.slice(0, index), str.slice(index)];
  }
  public static addNumberedAuthorialNote(editor: Editor) {
    if (!editor.selection) return;
    const point: BasePoint = Range.isForward(editor.selection) ? editor.selection.focus : editor.selection.anchor;
    const selectedNode = getLowestNodeAt(editor, point.path);
    if (!selectedNode) return;
    const [textBeforeSplit, textAfterSplit] = AuthorialNoteCommands.split(selectedNode.text, point.offset);

    const selectedParentPath = point.path.slice(0, point.path.length - 1);
    Transforms.delete(editor, { at: selectedParentPath });
    Transforms.insertNodes(editor, createTextWrapper(textAfterSplit), { at: selectedParentPath });
    Transforms.insertNodes(editor, AuthorialNoteCommands.createAuthorialNoteElement('1'), { at: selectedParentPath });
    Transforms.insertNodes(editor, createTextWrapper(textBeforeSplit), { at: selectedParentPath });

    AuthorialNoteCommands.renumberAuthorialNotes(editor);
    Transforms.select(editor, getLastLocationAt(editor, point.path));
  }

  public static addCustomAuthorialNote(editor: Editor, marker: string) {
    if (!editor.selection) return;
    const point: BasePoint = Range.isForward(editor.selection) ? editor.selection.focus : editor.selection.anchor;
    const selectedNode = getLowestNodeAt(editor, point.path);
    if (!selectedNode) return;
    const [textBeforeSplit, textAfterSplit] = AuthorialNoteCommands.split(selectedNode.text, point.offset);

    const selectedParentPath = point.path.slice(0, point.path.length - 1);
    Transforms.delete(editor, { at: selectedParentPath });
    Transforms.insertNodes(editor, createTextWrapper(textAfterSplit), { at: selectedParentPath });
    Transforms.insertNodes(editor, AuthorialNoteCommands.createAuthorialNoteElement(marker), {
      at: selectedParentPath,
    });
    Transforms.insertNodes(editor, createTextWrapper(textBeforeSplit), { at: selectedParentPath });
    Transforms.select(editor, getLastLocationAt(editor, point.path));
  }

  private static deleteAuthorialNote(editor: Editor, at: Path) {
    const [, previousTextWrapperPath] = Editor.previous(editor, {
      at,
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.TEXTWRAPPER,
    }) as NodeEntry<Element>;
    const [, nextTextWrapperPath] = Editor.next(editor, {
      at,
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.TEXTWRAPPER,
    }) as NodeEntry<Element>;
    const previousText = getLowestNodeAt(editor, previousTextWrapperPath)?.text || '';
    const nextText = getLowestNodeAt(editor, nextTextWrapperPath)?.text || '';

    Transforms.delete(editor, { at: nextTextWrapperPath });
    Transforms.delete(editor, { at });
    Transforms.insertText(editor, `${previousText}${nextText}`, { at: previousTextWrapperPath });

    Transforms.select(editor, { path: [...previousTextWrapperPath, 0], offset: previousText.length });
  }

  public static deleteNextAuthorialNote(editor: Editor, removeAuthorialNoteRef: (id: string) => void) {
    const [authorialNote, path] = Editor.next(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE,
    }) as NodeEntry<Element>;

    AuthorialNoteCommands.deleteAuthorialNote(editor, path);
    AuthorialNoteCommands.renumberAuthorialNotes(editor);
    removeAuthorialNoteRef(authorialNote.GUID || '');
  }

  public static deletePreviousAuthorialNote(editor: Editor, removeAuthorialNoteRef: (id: string) => void) {
    const [authorialNote, path] = Editor.previous(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE,
    }) as NodeEntry<Element>;

    AuthorialNoteCommands.deleteAuthorialNote(editor, path);
    AuthorialNoteCommands.renumberAuthorialNotes(editor);
    removeAuthorialNoteRef(authorialNote.GUID || '');
  }

  public static renumberAuthorialNotes(editor: Editor): void {
    const [...allAuthorialNotes] = Editor.nodes<Element>(editor, {
      at: [0],
      match: (n) =>
        (n as Element).type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE && !isNaN(Number((n as Element).marker || '')),
    });
    allAuthorialNotes.forEach((elementNode, index) => {
      const [element, elementPath] = elementNode;
      const newElement = Object.assign({}, element);
      newElement.marker = (++index).toString();
      Transforms.delete(editor, { at: elementPath });
      Transforms.insertNodes(editor, newElement, { at: elementPath });
    });
  }

  public static focusNextElement(editor: Editor, documentIndex: number): void {
    const [nextElement] = Editor.next(editor) as NodeEntry<Element>;
    if (nextElement.type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE) {
      document.getElementById(`${documentIndex}:${nextElement.GUID || ''}`)?.focus();
      editor.selection = null;
    }
  }

  public static focusPreviousElement(editor: Editor, documentIndex: number): void {
    const [previousElement] = Editor.previous(editor) as NodeEntry<Element>;
    if (previousElement.type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE) {
      document.getElementById(`${documentIndex}:${previousElement.GUID || ''}`)?.focus();
      editor.selection = null;
    }
  }

  public static focusAfterAuthorialNote(editor: Editor, guid: string | undefined): void {
    const [...elements] = Editor.nodes(editor, { at: [0], match: (element) => (element as Element).GUID === guid });
    if (elements.length < 1) return;
    const [, authorialNotePath] = elements[0];
    const [, nextElementPath] = Editor.next(editor, { at: authorialNotePath }) as NodeEntry<Element>;
    const textNodeEntry = getLowestNodeEntryAt(editor, nextElementPath);
    if (!textNodeEntry) return;

    Transforms.select(editor, getFirstLocationAt(editor, nextElementPath));
  }

  public static focusBeforeAuthorialNote(editor: Editor, guid: string | undefined): void {
    const [...elements] = Editor.nodes(editor, { at: [0], match: (element) => (element as Element).GUID === guid });
    if (elements.length < 1) return;
    const [, authorialNotePath] = elements[0];
    const [, previousElementPath] = Editor.previous(editor, {
      at: authorialNotePath,
    }) as NodeEntry<Element>;
    const textNodeEntry = getLowestNodeEntryAt(editor, previousElementPath);
    if (!textNodeEntry) return;

    Transforms.select(editor, getLastLocationAt(editor, previousElementPath));
  }

  private static createAuthorialNoteElement(marker: string): Element {
    const p = createElementOfType(AUTHORIALNOTE_STRUCTURE.P, [createTextWrapper('')]);
    const authorialNote = createElementOfType(AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE, [p]);
    authorialNote.marker = marker;
    authorialNote.placement = 'bottom';
    authorialNote.placementBase = '';
    authorialNote['xml:id'] = `lea${uuid()}`;
    return authorialNote;
  }
}
