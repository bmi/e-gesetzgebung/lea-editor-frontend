// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import {
  CombinedPluginRenderProps,
  IPluginContextMenuProps,
  IPluginToolbarProps,
  Plugin,
  PluginProps,
  PluginRange,
  PluginRenderLeafProps,
  useSelectionState,
  useSynopsisInfo,
} from '../../plugin-core';
import { BaseSelection, Editor, NodeEntry, Range, Text, TextDirection } from 'slate';
import { IconButton, CreateCommentIcon } from '@lea/ui';
import { CommentResponseDTO, LeaPoint } from '../../../api/Comment';
import { NewCommentWithRef } from '../../../interfaces/Comment';
import { useTranslation } from 'react-i18next';

import { useCommentPageChange, useCommentPageState } from '../../plugin-core/editor-store/workspace/comment-page';
import { useCommentsChange, useCommentsState } from './useComments';
import { CommentMarker, CommentMarkerWrapper, Element } from '../../../interfaces';
import { ItemType } from 'antd/lib/menu/interface';
import { Checkbox } from 'antd';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { getNodeAt, getNodesOfTypeInRange, Unit } from '../../plugin-core/utils';
import { v4 as uuid } from 'uuid';
import { DocumentStateType, useFirstDocumentState } from '../../../api';
import { ReactEditor } from 'slate-react';
import { CommentCommands } from './CommentCommands';
import { t } from 'i18next';

export class CommentPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward, deleteFragment } = editor;

    editor.deleteBackward = (unit: Unit) => {
      if (!editor.selection) return;
      const beforePoint = editor.before(editor.selection.anchor);
      beforePoint?.path.splice(beforePoint.path.length - 2);
      const prevWrapper = getNodeAt(editor, beforePoint?.path);
      if (!prevWrapper) {
        deleteBackward(unit);
        return;
      }
      const [prevWrapperElement] = prevWrapper;
      if (
        (prevWrapperElement.type === STRUCTURE_ELEMENTS.COMMENTMARKER ||
          prevWrapperElement.type === STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER) &&
        editor.selection.anchor.offset === 0
      ) {
        const deletedCommentsCount = CommentCommands.markCommentAndMarkerAsDeleted(
          this.comments,
          this.updateComment,
          prevWrapperElement,
        );
        editor.move({ distance: deletedCommentsCount + 1, reverse: true });
        return;
      }
      deleteBackward(unit);
    };
    editor.deleteForward = (unit: Unit) => {
      if (!editor.selection) return;
      let text = '';
      const [currentNode, currentPath] = editor.node(editor.selection.anchor) as NodeEntry<Element>;
      // eslint-disable-next-line prefer-const
      let commentWrapper = editor.parent(currentPath) as NodeEntry<Element> | undefined;
      if (commentWrapper?.[0].type !== STRUCTURE_ELEMENTS.COMMENTMARKER) {
        text = Text.isText(currentNode) ? currentNode.text : '';
        commentWrapper = editor.next<Element>({
          at: editor.selection.focus,
          match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER,
        });
      } else if (commentWrapper) {
        commentWrapper = editor.parent(commentWrapper[1]) as NodeEntry<Element>;
      }
      if (commentWrapper && editor.selection.focus.offset === text.length) {
        const [commentWrapperElement] = commentWrapper;
        const deletedCommentsCount = CommentCommands.markCommentAndMarkerAsDeleted(
          this.comments,
          this.updateComment,
          commentWrapperElement,
        );
        editor.move({ distance: deletedCommentsCount });
        return;
      }
      deleteForward(unit);
    };
    editor.deleteFragment = (options?: { direction?: TextDirection }) => {
      if (!editor.selection) return;
      const startSelection = editor.start(editor.selection);
      const endSelection = editor.end(editor.selection);
      const commentMarkersInRange = getNodesOfTypeInRange(
        editor,
        [STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER],
        [startSelection.path, endSelection.path],
      );
      commentMarkersInRange.forEach(([commentMarker]) => {
        CommentCommands.markCommentAndMarkerAsDeleted(this.comments, this.updateComment, commentMarker);
      });
      deleteFragment(options);
    };
    return editor;
  }

  public get isFinal(): boolean {
    return this.editorDocument.documentStateType === DocumentStateType.FINAL;
  }

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public set contextMenuItemTypes(itemTypes: ItemType[] | null) {
    this._contextMenuItemTypes = itemTypes;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  private updateComment: (comment: Partial<CommentResponseDTO>) => void = () => {};
  private comments: CommentResponseDTO[] = [];

  private setNewComment: (newComment: NewCommentWithRef) => void = () => {};

  public ToolbarTools = ({ editor }: IPluginToolbarProps): ReactElement | null => {
    const { newComment } = useCommentsState();
    const { isSynopsisVisible } = useSynopsisInfo();
    const { selection } = useSelectionState();
    const [hasHover, setHasHover] = useState(false);
    const disabled = !selection || !selection?.focus || Range.isCollapsed(selection);
    return !isSynopsisVisible ? (
      <IconButton
        icon={<CreateCommentIcon hasHover={!disabled && hasHover} />}
        id="create-comment"
        disabled={disabled}
        onClick={() => {
          !newComment && this.addCommentMarkers(editor, selection);
        }}
        onHover={setHasHover}
      />
    ) : null;
  };

  public ViewTools = (): ReactElement | null => {
    const { visible } = useCommentPageState();
    const { isSynopsisVisible } = useSynopsisInfo();
    const { toggelCommentPage } = useCommentPageChange();

    if (!isSynopsisVisible) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox onChange={toggelCommentPage} checked={visible}>
              Kommentare
            </Checkbox>
          ),
          key: 'viewTools-commentsPage',
          title: 'Komentare',
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public ContextMenu = ({ keyName, editor }: IPluginContextMenuProps): ReactElement => {
    const { t } = useTranslation();
    const { newComment } = useCommentsState();
    const { selection } = useSelectionState();

    useMemo(() => {
      const disabled = !selection || !selection?.focus || Range.isCollapsed(selection);
      if (!disabled) {
        this.contextMenuItemTypes = [
          {
            key: `${keyName}-create-comment`,
            title: t('lea.contextMenu.comments.addComment'),
            label: t('lea.contextMenu.comments.addComment'),
            onClick: () => {
              //add start and endwrapper to anchor and focus
              !newComment && this.addCommentMarkers(editor, selection);
            },
          },
        ];
      } else {
        this._contextMenuItemTypes = null;
      }
    }, [selection]);

    return <></>;
  };

  private addCommentMarkers = (editor: Editor, selection: BaseSelection) => {
    // set anchor, focus and elementGuid for start- and endmarker

    let anchor: LeaPoint = {
      elementGuid: '',
      offset: 0,
    };
    let focus: LeaPoint = {
      elementGuid: '',
      offset: 0,
    };
    if (selection) {
      if (selection.anchor.path.toString() === selection.focus.path.toString()) {
        // Get offset and parent of selection in same element
        const [parent, parentPath] = editor.parent(selection.anchor, { depth: -1 });
        const firstWrapperPath = ReactEditor.findPath(editor as ReactEditor, parent.children[0]);
        const anchorNodesInRange = getNodesOfTypeInRange(
          editor,
          [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
          [firstWrapperPath, selection.anchor.path],
        );
        const focusNodesInRange = getNodesOfTypeInRange(
          editor,
          [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
          [firstWrapperPath, selection.focus.path],
        );
        anchor = CommentCommands.setCommentInformation(
          selection.anchor,
          (parent as Element).GUID ?? (editor.parent(parentPath)[0] as Element).GUID ?? '',
          anchorNodesInRange,
          anchor,
        );
        focus = CommentCommands.setCommentInformation(
          selection.focus,
          (parent as Element).GUID ?? '',
          focusNodesInRange,
          focus,
        );
      } else {
        // Get offset and parent of selection in different elements
        const [anchorParent, anchorParentPath] = editor.parent(selection.anchor, { depth: -1 });
        const [focusParent, focusParentPath] = editor.parent(selection.focus, { depth: -1 });
        const firstAnchorWrapperPath = ReactEditor.findPath(editor as ReactEditor, anchorParent.children[0]);
        const firstFocusWrapperPath = ReactEditor.findPath(editor as ReactEditor, focusParent.children[0]);
        const anchorNodesInRange = getNodesOfTypeInRange(
          editor,
          [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
          [firstAnchorWrapperPath, selection.anchor.path],
        );
        const focusNodesInRange = getNodesOfTypeInRange(
          editor,
          [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
          [firstFocusWrapperPath, selection.focus.path],
        );
        anchor = CommentCommands.setCommentInformation(
          selection.anchor,
          (anchorParent as Element).GUID ?? (editor.parent(anchorParentPath)[0] as Element).GUID ?? '',
          anchorNodesInRange,
          anchor,
        );
        focus = CommentCommands.setCommentInformation(
          selection.focus,
          (focusParent as Element).GUID ?? (editor.parent(focusParentPath)[0] as Element).GUID ?? '',
          focusNodesInRange,
          focus,
        );
      }
      // Add new comment placeholder in commentPage
      this.setNewComment({
        content: '',
        commentId: uuid(),
        documentContent: JSON.stringify(editor.children),
        anchor: anchor,
        focus: focus,
      });
    }
  };

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementType = e.type;
    const guid = e.GUID;

    if (elementType === STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER) {
      const commentMarker = e as unknown as CommentMarkerWrapper;
      const comments = commentMarker.children;
      return (
        <span
          contentEditable={false}
          hidden={comments.filter((comment) => comment.hidden).length === comments.length}
          style={{ borderColor: comments[0].color }}
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          data-commentids={JSON.stringify(comments.map((comment) => comment['lea:commentId']))}
          className={`${elementType} comment-marker`}
          {...props.attributes}
        >
          {props.children}
        </span>
      );
    } else if (elementType === STRUCTURE_ELEMENTS.COMMENTMARKER) {
      const comment = e as unknown as CommentMarker;
      const commentId = comment['lea:commentId'];
      const commentPosition = comment['lea:commentPosition'];
      return (
        <span
          contentEditable={false}
          aria-description={
            commentPosition === 'start' ? t('lea.comments.marker.startMarker') : t('lea.comments.marker.endMarker')
          }
          key={`${commentPosition}-${commentId}`}
          data-commentid={commentId}
          data-hidden={comment.hidden}
          data-commentposition={commentPosition}
          className={`comment ${commentPosition}-${commentId}`}
        >
          {props.children}
        </span>
      );
    }

    return null;
  };

  public renderLeaf = ({ attributes, children, leaf }: PluginRenderLeafProps): React.ReactElement | null => {
    let style: React.CSSProperties = {};
    const { document } = useFirstDocumentState();
    const bgColor = document?.documentPermissions.hasWrite ? '#FFF' : '#F2F4FD';
    if (leaf.comment) {
      if (!leaf.hidden) {
        style = {
          backgroundColor: leaf.commentColor ? leaf.commentColor : bgColor,
          color: leaf.commentColor ? '#FFF' : 'inherit',
        };
      }
      return (
        <span className={leaf.commentColor ? 'isActive' : ''} {...attributes} style={style}>
          {children}
        </span>
      );
    }
    return null;
  };

  public decorate = (entry: NodeEntry<Element>, editor: Editor): PluginRange[] => {
    const [node, path] = entry;
    const ranges: PluginRange[] = [];

    if (!Text.isText(node) || node.type === STRUCTURE_ELEMENTS.COMMENTMARKER) {
      return [];
    }
    const [element, elementPath] = editor.parent(path) as NodeEntry<Element>;
    if (element) {
      const [parentWrapper] = editor.parent(elementPath) as NodeEntry<Element>;
      if (parentWrapper && parentWrapper.children.length > 0) {
        const firstElementPath = ReactEditor.findPath(editor as ReactEditor, parentWrapper.children[0]);
        if (firstElementPath) {
          const markersBeforeMe = getNodesOfTypeInRange(
            editor,
            [STRUCTURE_ELEMENTS.COMMENTMARKER],
            [firstElementPath, path],
          );
          if (markersBeforeMe.length > 0) {
            const startMarkers: CommentMarker[] = [];
            const endMarkers = [];
            markersBeforeMe.forEach(([element]) => {
              if (element && element.type === STRUCTURE_ELEMENTS.COMMENTMARKER) {
                if ((element as unknown as CommentMarker)['lea:commentPosition'] === 'start') {
                  startMarkers.push(element as unknown as CommentMarker);
                } else {
                  endMarkers.push(element);
                }
              }
            });

            // alle kommentare sind schon vor meinem wrapper geschlossen und ich muss nicht dekorieren
            if (startMarkers.length === endMarkers.length) {
              return [];
            } else {
              ranges.push({
                anchor: { path: path, offset: 0 },
                focus: { path: path, offset: node.text?.length ?? 0 },
                comment: true,
                hidden: element['lea:hidden'],
                commentColor: element['lea:isActive'] ? element['lea:commentColor'] : undefined,
              });
              return ranges;
            }
          }
        }
      }
    }
    return [];
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { setNewComment, updateComment } = useCommentsChange();
    const { comments } = useCommentsState();

    useEffect(() => {
      this.setNewComment = setNewComment;
      this.updateComment = updateComment;
    }, []);

    useEffect(() => {
      this.comments = comments;
    }, [comments]);
    return <></>;
  };
}
