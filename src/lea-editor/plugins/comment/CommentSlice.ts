// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { InitialCommentState } from './CommentState';
import { CommentResponseDTO, CreateCommentDTO } from '../../../api';

const commentSlice = createSlice({
  name: 'comment',
  initialState: InitialCommentState,
  reducers: {
    setComments(state, action: PayloadAction<CommentResponseDTO[]>) {
      state.comments = action.payload;
    },
    setFilteredComments(state, action: PayloadAction<CommentResponseDTO[]>) {
      state.filteredComments = action.payload;
    },
    setNewComment(state, action: PayloadAction<CreateCommentDTO | null>) {
      state.newComment = action.payload;
    },
    updateComment(state, action: PayloadAction<Partial<CommentResponseDTO>>) {
      state.comments = state.comments.map((comment) => {
        if (comment.commentId === action.payload.commentId) {
          comment = { ...comment, ...action.payload };
        }
        return comment;
      });
      state.filteredComments = state.filteredComments.map((comment) => {
        if (comment.commentId === action.payload.commentId) {
          comment = { ...comment, ...action.payload };
        }
        return comment;
      });
    },
  },
});

export const { setComments, setFilteredComments, setNewComment, updateComment } = commentSlice.actions;
export const CommentSlice = commentSlice;
