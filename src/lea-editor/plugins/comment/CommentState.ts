// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CommentResponseDTO, CreateCommentDTO } from '../../../api/Comment';

export interface CommentState {
  comments: CommentResponseDTO[];
  filteredComments: CommentResponseDTO[];
  newComment: CreateCommentDTO | null;
  commentUpdate: Partial<CommentResponseDTO>;
}

export const InitialCommentState: CommentState = {
  comments: [],
  filteredComments: [],
  newComment: null,
  commentUpdate: {},
};
