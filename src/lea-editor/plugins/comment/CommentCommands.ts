// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Point, NodeEntry } from 'slate';
import { CommentResponseDTO, LeaPoint } from '../../../api/Comment';
import { CommentMarker, Element } from '../../../interfaces';

export class CommentCommands {
  public static setCommentInformation(
    selectionPoint: Point,
    elementGuid: string,
    rangeNodes: NodeEntry<Element>[],
    point: LeaPoint,
  ) {
    let offset = 0;
    rangeNodes.forEach(([node, nodePath]) => {
      if ([...nodePath, 0].toString() === selectionPoint.path.toString()) {
        offset += selectionPoint.offset;
      } else {
        offset += node.children?.[0].text?.length ? node.children[0].text.length : 0;
      }
    });
    point.elementGuid = elementGuid;
    point.offset = offset;
    return point;
  }

  public static markCommentAndMarkerAsDeleted(
    comments: CommentResponseDTO[],
    updateComment: (comment: Partial<CommentResponseDTO>) => void,
    wrapper: Element,
  ) {
    const commentIds = wrapper.children.map(
      (commentMarker) => (commentMarker as unknown as CommentMarker)['lea:commentId'],
    );
    const commentsToDelete = comments.filter((comment) => commentIds.includes(comment.commentId));
    commentsToDelete.forEach((comment) => updateComment({ commentId: comment.commentId, deleted: true }));
    return commentsToDelete.length;
  }
}
