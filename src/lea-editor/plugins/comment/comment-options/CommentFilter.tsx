// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse } from 'antd';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { ChoicesCheckBox, ChoicesItemInterface, ArrowRightBlackIcon, ArrowDownBlackIcon } from '@lea/ui';
import { CheckboxValueType } from '../../../../interfaces';
import { useCommentsChange, useCommentsState } from '../useComments';
import { CommentResponseDTO } from '../../../../api';
const { Panel } = Collapse;

export const CommentFilter = (): ReactElement => {
  const { t } = useTranslation();
  const { setFilteredComments } = useCommentsChange();
  const { comments } = useCommentsState();
  const [open, setOpen] = useState(comments.filter((comment) => comment.state === 'OPEN'));
  const [closed, setClosed] = useState(comments.filter((comment) => comment.state === 'CLOSED'));
  const [categoriesHasHover, _setCategoriesHasHover] = useState(false);
  const [originHasHover, _setOriginHasHover] = useState(false);

  const originInitValues = useRef<ChoicesItemInterface[]>([]);

  const categoriesInitValues: ChoicesItemInterface[] = [
    { label: `Offen (${open.length})`, value: 'OPEN' },
    { label: `Erledigt (${closed.length})`, value: 'CLOSED' },
  ];

  const [categoriesValueList, setCategoriesValueList] = useState<CheckboxValueType[]>([
    ...categoriesInitValues.map((categoriesValue) => categoriesValue.value as CheckboxValueType),
  ]);
  const [originValueList, setOriginValueList] = useState<CheckboxValueType[]>([]);

  useEffect(() => {
    const commentStates: CommentResponseDTO[] = [];
    if (categoriesValueList.find((category) => category === 'OPEN')) {
      commentStates.push(...open);
    }
    if (categoriesValueList.find((category) => category === 'CLOSED')) {
      commentStates.push(...closed);
    }
    setFilteredComments(commentStates);
  }, [categoriesValueList, open, closed]);

  useEffect(() => {
    const usersList: ChoicesItemInterface[] = [];
    comments.forEach((comment) => {
      if (!usersList.find((value) => value.value === comment.createdBy.gid)) {
        usersList.push({ label: comment.createdBy.name, value: comment.createdBy.gid });
      }
    });
    originInitValues.current = usersList;
    setOriginValueList([
      ...originInitValues.current.map((originValue) => originValue.value as CheckboxValueType),
    ] as CheckboxValueType[]);
  }, [comments]);

  useEffect(() => {
    setCommentInitData();
  }, [originValueList, comments]);

  const setCommentInitData = () => {
    const filterResult = comments.filter((initialComment) =>
      originValueList.find((value) => value === initialComment.createdBy.gid),
    );
    setOpen(filterResult.filter((comment) => comment.state === 'OPEN'));
    setClosed(filterResult.filter((comment) => comment.state === 'CLOSED'));
  };

  const setCategoryExpandIcon = (isActive?: boolean) => {
    return (
      <div className="lea-icon">
        {isActive ? (
          <ArrowDownBlackIcon hasHover={categoriesHasHover} />
        ) : (
          <ArrowRightBlackIcon hasHover={categoriesHasHover} />
        )}
      </div>
    );
  };

  const setOriginExpandIcon = (isActive?: boolean) => {
    return (
      <div className="lea-icon">
        {isActive ? (
          <ArrowDownBlackIcon hasHover={originHasHover} />
        ) : (
          <ArrowRightBlackIcon hasHover={originHasHover} />
        )}
      </div>
    );
  };

  return (
    <div className="option-wrapper">
      <h4 className="option-title">{t('lea.commentOptions.filter.title')}</h4>
      <Collapse
        className="options-collapse"
        expandIcon={({ isActive }) => setCategoryExpandIcon(isActive)}
        defaultActiveKey={[0]}
        ghost
      >
        <Panel
          id={'category_panel'}
          header={
            <div className="panel-header">
              {t('lea.commentOptions.filter.category')}
              <strong className="items-counter">
                {t('lea.commentOptions.filter.selectedItemsLabel', {
                  selectedItems: categoriesValueList.length,
                  allItems: categoriesInitValues.length,
                })}
              </strong>
            </div>
          }
          key={0}
        >
          <div id="commentFilter" tabIndex={0}>
            <ChoicesCheckBox
              checkedList={categoriesValueList}
              setCheckedList={setCategoriesValueList}
              choicesList={categoriesInitValues}
              isVisible={true}
            />
          </div>
        </Panel>
      </Collapse>
      <Collapse
        className="options-collapse"
        expandIcon={({ isActive }) => setOriginExpandIcon(isActive)}
        defaultActiveKey={[0]}
        ghost
      >
        <Panel
          id={'origin_panel'}
          header={
            <div className="panel-header">
              {t('lea.commentOptions.filter.origin')}
              <strong className="items-counter">
                {t('lea.commentOptions.filter.selectedItemsLabel', {
                  selectedItems: originValueList.length,
                  allItems: originInitValues.current.length,
                })}
              </strong>
            </div>
          }
          key={0}
        >
          <div id="commentFilter" tabIndex={0}>
            <ChoicesCheckBox
              checkedList={originValueList}
              setCheckedList={setOriginValueList}
              choicesList={originInitValues.current}
              isVisible={true}
              allCheckable
            />
          </div>
        </Panel>
      </Collapse>
    </div>
  );
};
