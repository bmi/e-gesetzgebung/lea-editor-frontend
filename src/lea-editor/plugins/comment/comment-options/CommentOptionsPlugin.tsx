// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { LeaDrawer, AriaController as AriaControllerLea } from '@lea/ui';
import React, { ReactElement, useEffect, useRef } from 'react';
import { Editor } from 'slate';
import { IPluginOptionDialogProps, Plugin, PluginProps } from '../../../plugin-core';
import { CommentNavigator } from './CommentNavigator';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { CommentFilter } from './CommentFilter';
import { Divider } from 'antd';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
  useEditorOptionsVisibleState,
} from '../../../plugin-core/editor-store/editor-options';
import { useTranslation } from 'react-i18next';

export class CommentOptionsPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T {
    return e;
  }

  public OptionDialog = ({ editor }: IPluginOptionDialogProps): ReactElement => {
    const { t } = useTranslation();
    const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.COMMENT);
    const { closeOptions } = useEditorOptionsVisibleChange(EditorOptions.COMMENT);
    const commentOptionRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
      AriaControllerPlateg.setAriaLabelsByQuery('.comment-options .ant-drawer-close', 'Kommentaroptionen schließen');
      setTimeout(
        () =>
          commentOptionRef.current &&
          AriaControllerLea.setElementIdByClassNameInElement(
            'ant-drawer-close',
            'lea-drawer-comment-option-close-button',
            commentOptionRef.current,
          ),
      );
    }, [optionsVisible]);

    return (
      <LeaDrawer
        className="comment-options"
        placement={'right'}
        open={optionsVisible}
        onClose={closeOptions}
        title={'Kommentaroptionen'}
        expandable={true}
        refProp={commentOptionRef}
        ariaLabel={t('lea.commentOptions.ariaLabel')}
      >
        <CommentNavigator editor={editor} isOpen={optionsVisible} />
        <Divider className="option-divider" />
        <CommentFilter />
      </LeaDrawer>
    );
  };
}
