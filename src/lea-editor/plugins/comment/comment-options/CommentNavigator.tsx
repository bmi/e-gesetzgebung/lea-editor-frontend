// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useEffect, useState } from 'react';

import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { useCommentsState } from '..';
import { focusCommentInEditor, sortComments } from '../../../Editor/Comment/Utils';

interface ICommentNavigatorProps {
  editor: Editor;
  isOpen: boolean;
}
const setCommentCheckboxEvent = new Event('setCommentCheckbox');

export const CommentNavigator = ({ editor, isOpen }: ICommentNavigatorProps): ReactElement => {
  const { t } = useTranslation();
  const { filteredComments } = useCommentsState();
  const [canNavigate, setCanNavigate] = useState(false);
  const [currentCommentIndex, setCurrentCommentIndex] = useState(-1);
  const [commentCountText, setCommentCountText] = useState('');

  useEffect(() => {
    if (isOpen) {
      const nav = document.getElementById('commentNavigation');
      nav?.focus();
    }
  }, [isOpen]);

  useEffect(() => {
    if (filteredComments.length > 0) {
      setCanNavigate(true);
    }
    const overallComments = filteredComments.length > 0 ? filteredComments.length.toString() : 'keine';
    setCommentCountText('Kommentare ' + overallComments);
    setCurrentCommentIndex(-1);
  }, [filteredComments]);

  useEffect(() => {
    const sortedComments = sortComments(filteredComments, editor as ReactEditor);
    const commentToFocus = sortedComments[currentCommentIndex];
    if (commentToFocus) {
      const commentPage = document.getElementById('commentPage');
      if (!commentPage) {
        document.dispatchEvent(setCommentCheckboxEvent);
      }
      setCommentCountText(
        'Kommentar ' + (currentCommentIndex + 1).toString() + ' von ' + filteredComments.length.toString(),
      );
      focusCommentInEditor(editor as ReactEditor, commentToFocus);
    }
  }, [currentCommentIndex]);

  return (
    <div id="commentNavigation" tabIndex={0} className="option-wrapper">
      <h4 className="option-title">{t('lea.commentOptions.jumpBetweenComments')}</h4>
      <div className="option-content">
        <p>{commentCountText}</p>
        <div className="comment-navigation-buttons">
          <Button
            id="prev-comment"
            aria-label="Zurück zum vorherigen Kommentar"
            onClick={() => {
              currentCommentIndex - 1 < 0
                ? setCurrentCommentIndex(filteredComments.length - 1)
                : setCurrentCommentIndex(currentCommentIndex - 1);
            }}
            tabIndex={0}
            disabled={!canNavigate}
          >
            {t('lea.commentOptions.prevComment')}
          </Button>
          <Button
            id="next-comment"
            aria-label="Weiter zum nächsten Kommentar"
            onClick={() => {
              currentCommentIndex === filteredComments.length - 1
                ? setCurrentCommentIndex(0)
                : setCurrentCommentIndex(currentCommentIndex + 1);
            }}
            tabIndex={0}
            disabled={!canNavigate}
          >
            {t('lea.commentOptions.nextComment')}
          </Button>
        </div>
      </div>
    </div>
  );
};
