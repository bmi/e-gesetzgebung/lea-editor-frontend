// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { displayMessage } from '@plateg/theme';
import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CommentResponseDTO, CommentService, CreateCommentDTO } from '../../../api/Comment';
import { useCommentPageChange } from '../../plugin-core/editor-store/workspace/comment-page';
import { useCommentCountStateContext } from './useCommentsContext';
import { CommentSlice, setComments, setFilteredComments, setNewComment, updateComment } from './CommentSlice';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { EditorPageState } from '../../plugin-core';
import { InitialCommentState } from './CommentState';

type UseCommentsState = {
  comments: CommentResponseDTO[];
  filteredComments: CommentResponseDTO[];
  newComment: CreateCommentDTO | null;
};

type UseCommentsChange = {
  fetchComments: (id: string) => void;
  setComments: (comments: CommentResponseDTO[]) => void;
  setFilteredComments: (comments: CommentResponseDTO[]) => void;
  setNewComment: (newComment: CreateCommentDTO) => void;
  updateComment: (comment: Partial<CommentResponseDTO>) => void;
  removeNewComment: () => void;
  // toggleActiveComment: (id: string) => void;
};

type FetchComment = {
  fetchComments: (id: string) => void;
};

const useFetchComments = (dispatch: (comment: CommentResponseDTO[]) => void): FetchComment => {
  const { t } = useTranslation();
  const [documentId, setDocumentId] = useState<string>('');
  const [fetchCommentsResponse, fetchComments] = CommentService.getAll(documentId);

  const fetchCommentsCallback = useCallback((id: string) => {
    setDocumentId(id);
  }, []);

  useEffect(() => {
    if (documentId != '') {
      fetchComments();
      setDocumentId('');
    }
  }, [documentId]);

  useEffect(() => {
    if (fetchCommentsResponse.hasError) {
      displayMessage(t('lea.messages.comments.loadError'), 'error');
    } else if (fetchCommentsResponse.data && !fetchCommentsResponse.isLoading) {
      dispatch(fetchCommentsResponse.data);
    }
  }, [fetchCommentsResponse]);

  return { fetchComments: fetchCommentsCallback };
};

export const useCommentsState = (): UseCommentsState => {
  const { comments, newComment, filteredComments } = useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.workspace[CommentSlice.name] || InitialCommentState,
  );

  return {
    comments: comments,
    newComment: newComment,
    filteredComments: filteredComments,
  };
};

export const useCommentCountState = (): boolean => {
  const { state } = useCommentCountStateContext();

  return state;
};

export const useCommentsChange = (): UseCommentsChange => {
  const dispatch = useAppDispatch();
  const { openCommentPage } = useCommentPageChange();

  const setCommentsCallback = useCallback((comments: CommentResponseDTO[]) => {
    comments.length > 0 && openCommentPage();
    dispatch(setComments(comments));
  }, []);

  const setFilteredCommentsCallback = useCallback((filteredComments: CommentResponseDTO[]) => {
    dispatch(setFilteredComments(filteredComments));
  }, []);

  const setNewCommentCallback = useCallback((newComment: CreateCommentDTO) => {
    dispatch(setNewComment(newComment));
  }, []);

  const updateCommentCallback = useCallback((comment: Partial<CommentResponseDTO>) => {
    dispatch(updateComment(comment));
  }, []);

  const removeNewCommentCallback = useCallback(() => {
    dispatch(setNewComment(null));
  }, []);

  const { fetchComments } = useFetchComments(setCommentsCallback);

  return {
    fetchComments,
    setComments: setCommentsCallback,
    setFilteredComments: setFilteredCommentsCallback,
    setNewComment: setNewCommentCallback,
    removeNewComment: removeNewCommentCallback,
    updateComment: updateCommentCallback,
    // toggleActiveComment: toggleActiveCommentCallback,
  };
};
