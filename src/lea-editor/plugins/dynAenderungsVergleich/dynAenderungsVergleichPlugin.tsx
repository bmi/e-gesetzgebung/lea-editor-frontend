// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useRef } from 'react';
import { Editor, Transforms, Range, Path, RangeRef, NodeEntry, Node, BasePoint } from 'slate';
import { ReactEditor } from 'slate-react';
import { Element } from '../../../interfaces';

import {
  Plugin,
  CombinedPluginRenderProps,
  useDynAenderungsvergleichDispatch,
  useDynAenderungsvergleichState,
  IPluginOptionDialogProps,
} from '../../plugin-core';

import {
  Unit,
  createInlineFragment,
  createTextWrapper,
  getLastLocationAt,
  getLowestNodeAt,
  getNextElementOfType,
  getNodesOfTypeAt,
  getNodesOfTypeInRange,
  getPreviousNodeEntryElement,
  isFirstPosition,
  isFirstSiblingOfType,
  isFirstTextPosition,
  isLastTextPosition,
  isLeftNavigation,
  isPreviousNeighborOfType,
  isRightNavigation,
  getPreviousElement,
  getNextElement,
  isWithinNodeOfType,
} from '../../plugin-core/utils';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { ItemType } from 'antd/lib/menu/interface';
import { Switch, Collapse, Divider, Menu } from 'antd';
import { useTranslation } from 'react-i18next';
import { TextDirection } from 'slate/dist/types';
import { EditableQueries } from '../../plugin-core/utils/EditableQueries';
import { AUTHORIALNOTE_STRUCTURE } from '../authorialNote/AuthorialNoteStructure';
import { REGELUNGSTEXT_STRUCTURE } from '../regelungstext';
import { JuristischerAbsatzCommands, ListeMitUntergliederungCommands } from '../regelungstext/stammform';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { AriaController, LeaDrawer } from '@lea/ui';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
  useEditorOptionsVisibleState,
} from '../../plugin-core/editor-store/editor-options';
const { Panel } = Collapse;
import './dynAenderungsvergleich.less';
import { IconDaenvAcceptChanges, IconDaenvDeclineChanges } from '@lea/ui/src/Icon/components';
import { SelectInfo } from 'rc-menu/lib/interface';

export class DynAenderungsVergleichPlugin extends Plugin {
  private readonly refs = Array(1)
    .fill(0)
    .map(() => React.createRef<HTMLElement>());

  public get toolRefs(): React.RefObject<HTMLElement>[] {
    return this.refs;
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward, deleteFragment, insertText } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    //insert text is also on copy paste
    e.insertText = (text: string) => {
      if (!e.selection) return;
      if (Range.isRange(e.selection)) {
        e.deleteFragment({ direction: Range.isForward(e.selection) ? 'forward' : 'backward' });
      }
      const changeWrapper = getNodesOfTypeAt(e, [STRUCTURE_ELEMENTS.CHANGEWRAPPER]);
      if (this.isDynAenderungsvergleich) {
        if (changeWrapper.length > 0) {
          if (changeWrapper[0][0]['lea:changeType'] === 'ct_inserted') {
            insertText(text);
          } else if (changeWrapper[0][0]['lea:changeType'] === 'ct_deleted') {
            if (e.selection.anchor.offset === 0) {
              Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
            }
            const element = createInlineFragment('ct_inserted', text);
            e.insertNode(element);
          }
        } else {
          const element = createInlineFragment('ct_inserted', text);
          e.insertNode(element);
        }
      } else if (changeWrapper.length > 0) {
        e.insertNode(createTextWrapper(text));
      } else {
        insertText(text);
      }
    };

    editor.deleteBackward = (unit: Unit) => {
      const selectionStart = editor.selection ? Range.start(editor.selection) : null;
      const wrappers = getNodesOfTypeAt(e, [STRUCTURE_ELEMENTS.CHANGEWRAPPER, STRUCTURE_ELEMENTS.TEXTWRAPPER]);
      let previousElement = undefined;
      if (wrappers?.length > 0) {
        previousElement = getPreviousElement(editor, wrappers[0][1], [
          STRUCTURE_ELEMENTS.CHANGEWRAPPER,
          STRUCTURE_ELEMENTS.TEXTWRAPPER,
        ]);
      }

      if (this.isDynAenderungsvergleich) {
        if (
          isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT) &&
          isFirstPosition(editor) &&
          !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT)
        ) {
          ListeMitUntergliederungCommands.mergePoints(editor);
        } else if (
          !isWithinNodeOfType(editor, REGELUNGSTEXT_STRUCTURE.POINT) &&
          isFirstPosition(editor) &&
          !isFirstSiblingOfType(editor, REGELUNGSTEXT_STRUCTURE.PARAGRAPH)
        ) {
          JuristischerAbsatzCommands.mergeParagraphs(editor, this.isDynAenderungsvergleich);
        }

        if (wrappers && selectionStart) {
          if (selectionStart.offset > 0) {
            // console.log('inside wrapper offset wrapper > 0');

            //cursor is inside a wrapper
            if (
              wrappers[0][0].type == STRUCTURE_ELEMENTS.TEXTWRAPPER &&
              wrappers[0][0].children[0].text &&
              wrappers[0][0].children[0].text?.length > 0
            ) {
              const deletedText = wrappers[0][0].children[0].text[selectionStart?.offset - 1];
              deleteBackward(unit);
              const element = createInlineFragment('ct_deleted', deletedText);
              e.insertNode(element);
              Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
            } else if (wrappers[0][0].type == STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (wrappers[0][0]['lea:changeType'] === 'ct_inserted') {
                if (wrappers[0][0].children[0].text && wrappers[0][0].children[0].text?.length === 1) {
                  //remove node when no text is in inserted changemarker
                  Transforms.removeNodes(editor, { at: editor.selection?.focus });
                } else {
                  deleteBackward(unit);
                }
              } else if (wrappers[0][0]['lea:changeType'] === 'ct_deleted') {
                Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
              }
            }
          } else if (selectionStart.offset === 0) {
            // console.log('offset wrapper 0', selectionStart);
            // console.log('which wrapper', wrappers[0][0].type);
            // console.log('previous wrapper', previousElement);

            if (previousElement?.[0].type === STRUCTURE_ELEMENTS.AUTHORIALNOTE) {
              deleteBackward(unit);
              return;
            } else if (previousElement?.[0].type === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              // console.log('previous wrapper is change');

              if (previousElement?.[0]['lea:changeType'] === 'ct_deleted') {
                // console.log('previous wrapper is deleted');
                Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
                return;
              } else if (previousElement?.[0]['lea:changeType'] === 'ct_inserted') {
                // console.log('previous wrapper is inserted');
                Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
                deleteBackward(unit);
                return;
              }
            }
            //cursor is at start of a wrapper
            if (wrappers[0][0].type == STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (wrappers[0][0]['lea:changeType'] === 'ct_deleted') {
                if (!previousElement) return;
                // console.log('in deleted ');
                const previousElementText = previousElement?.[0].children[0].text;
                if (previousElementText?.length === 0) {
                  Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
                  return;
                }
                if (previousElementText) {
                  const char = previousElementText.slice(previousElementText.length - 1);
                  if (wrappers[0][0].children[0].text) {
                    insertText(char);
                    Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
                    if (previousElementText.length === 1) {
                      // console.log('previous text has only 1 char and im in deleted');
                      Transforms.removeNodes(editor, { at: previousElement[1] });
                    } else {
                      deleteBackward(unit);
                      Transforms.move(editor, { distance: 1, unit: 'character' });
                    }
                  }
                }
              } else if (wrappers[0][0]['lea:changeType'] === 'ct_inserted') {
                const [, parentElementPath] = Editor.above(editor, {
                  match: (n: Node) =>
                    n && [STRUCTURE_ELEMENTS.P, STRUCTURE_ELEMENTS.HEADING].includes((n as Element).type),
                }) as NodeEntry<Element>;
                const previousElement = getPreviousNodeEntryElement(
                  e,
                  [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
                  wrappers[0],
                  parentElementPath,
                );
                if (previousElement?.[0].type === STRUCTURE_ELEMENTS.TEXTWRAPPER) {
                  const text = previousElement?.[0].children[0].text;
                  if (text?.length === 0) {
                    Transforms.removeNodes(editor, { at: previousElement[1] });
                    return;
                  }
                  if (text) {
                    const char = text.slice(text.length - 1);
                    Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
                    const element = createInlineFragment('ct_deleted', char);
                    e.insertNode(element);
                    Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
                    deleteBackward(unit);
                    Transforms.move(editor, { distance: 1, unit: 'character' });
                  }
                }
              }
            } else if (wrappers[0][0].type == STRUCTURE_ELEMENTS.TEXTWRAPPER) {
              // console.log('o position in Textwrapper');
              if (previousElement?.[0].type === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
                // console.log('o position in Text and previous changewrapper');
                Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
                deleteBackward(unit);
              }
            }
          }
        }
      } else {
        // console.log('offset', selectionStart?.offset, 'prev', previousElement);
        if (selectionStart?.offset === 0) {
          // console.log('offset 0', previousElement);
          if (previousElement) {
            const previousElementParent = Editor.above(editor, { at: previousElement[1] }) as NodeEntry<Element>;
            //first position in p element before num block
            if (previousElementParent[0].type === STRUCTURE_ELEMENTS.NUM) {
              deleteBackward(unit);
              return;
            }
            if (previousElement[0].type === STRUCTURE_ELEMENTS.AUTHORIALNOTE) {
              deleteBackward(unit);
              return;
            }
            const text = previousElement[0].children[0].text;
            if (previousElement[0]['lea:changeType'] === 'ct_deleted') {
              text?.length && Transforms.move(editor, { distance: text.length + 2, reverse: true, unit: 'character' });
              deleteBackward(unit);
            } else if (text?.length && text.length === 1) {
              Transforms.removeNodes(editor, { at: previousElement[1] });
            } else {
              Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
              deleteBackward(unit);
            }
          } else {
            deleteBackward(unit);
          }
        } else {
          const text = wrappers[0][0].children[0].text;
          if (
            text?.length &&
            text.length <= 1 &&
            previousElement &&
            !isPreviousNeighborOfType(editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE)
          ) {
            const nextElement = getNextElement(editor, wrappers[0][1]);
            if (nextElement && nextElement[0]['lea:changeType'] === 'ct_deleted') {
              //if next wrapper is delete after currentwrapper will be removed it will move to the current selection
              //-> move to characters to avoid
              Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
            }
            Transforms.removeNodes(editor, { at: wrappers[0][1] });
          } else {
            deleteBackward(unit);
          }
        }
      }
    };
    editor.deleteForward = (unit: Unit) => {
      const selectionStart = editor.selection ? Range.start(editor.selection) : null;
      const [wrapper, wrapperPath] = getNodesOfTypeAt(e, [
        STRUCTURE_ELEMENTS.CHANGEWRAPPER,
        STRUCTURE_ELEMENTS.TEXTWRAPPER,
      ])[0];

      const nextElement = getNextElementOfType(
        editor,
        [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
        wrapper,
        wrapperPath.slice(0, -1),
      );
      if (this.isDynAenderungsvergleich) {
        if (wrapper && selectionStart) {
          if (
            selectionStart.offset >= 0 &&
            wrapper.children[0].text &&
            selectionStart.offset < wrapper.children[0].text?.length
          ) {
            //cursor is inside a wrapper
            if (
              wrapper.type == STRUCTURE_ELEMENTS.TEXTWRAPPER &&
              wrapper.children[0].text &&
              wrapper.children[0].text?.length > 0
            ) {
              const deletedText = wrapper.children[0].text[selectionStart?.offset];
              deleteForward(unit);
              const element = createInlineFragment('ct_deleted', deletedText);
              e.insertNode(element);
            } else if (wrapper.type == STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (wrapper['lea:changeType'] === 'ct_inserted') {
                if (wrapper.children[0].text && wrapper.children[0].text?.length === 1) {
                  //remove node when no text is in inserted changemarker
                  Transforms.removeNodes(editor, { at: editor.selection?.focus });
                } else {
                  deleteForward(unit);
                }
              } else if (wrapper['lea:changeType'] === 'ct_deleted') {
                Transforms.move(editor, { distance: 1, unit: 'character' });
              }
            }
          } else {
            //cursor is at start of a wrapper
            if (wrapper.type == STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (!nextElement) return;
              if (nextElement[0].type === STRUCTURE_ELEMENTS.TEXTWRAPPER) {
                const text = nextElement[0].children[0].text;
                const deleteText = text?.slice(0, 1);
                if (deleteText && wrapper['lea:changeType'] === 'ct_deleted') {
                  insertText(deleteText);
                  if (text?.length === 1) {
                    Transforms.removeNodes(editor, { at: nextElement[1] });
                  } else {
                    Transforms.move(editor, { distance: 1, unit: 'character' });
                    deleteForward(unit);
                    Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
                  }
                } else if (deleteText && wrapper['lea:changeType'] === 'ct_inserted') {
                  Transforms.move(editor, { distance: 1, unit: 'character' });
                  text?.length === 1 ? Transforms.removeNodes(editor, { at: nextElement[1] }) : deleteForward(unit);
                  const element = createInlineFragment('ct_deleted', deleteText);
                  e.insertNode(element);
                }
              } else if (nextElement[0].type === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
                if (nextElement[0]['lea:changeType'] === 'ct_inserted') {
                  Transforms.move(editor, { distance: 1, unit: 'character' });
                  deleteForward(unit);
                } else if (nextElement[0]['lea:changeType'] === 'ct_deleted') {
                  Transforms.move(editor, { distance: 2, unit: 'character' });
                }
              }
            } else if (
              wrapper.type == STRUCTURE_ELEMENTS.TEXTWRAPPER &&
              wrapper.children[0].text &&
              wrapper.children[0].text?.length > 0
            ) {
              const deletedText = wrapper.children[0].text[selectionStart?.offset - 1];
              if (wrapper['lea:changeType'] === 'ct_deleted') {
                insertText(deletedText);
                Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
              }
            }
          }
        }
      } else {
        const wrapperText = wrapper.children[0].text;
        if (wrapperText && selectionStart?.offset === wrapperText.length) {
          if (nextElement) {
            const text = nextElement[0].children[0].text;
            if (nextElement[0]['lea:changeType'] === 'ct_deleted') {
              text?.length && Transforms.move(editor, { distance: text.length + 2, unit: 'character' });
              deleteForward(unit);
            } else if (text?.length && text.length === 1) {
              Transforms.removeNodes(editor, { at: nextElement[1] });
            } else {
              Transforms.move(editor, { distance: 1, unit: 'character' });
              deleteForward(unit);
            }
          }
        } else {
          const text = wrapper.children[0].text;
          if (text?.length && text.length <= 1 && nextElement) {
            // const previousElement = getPreviousElement(editor, wrapperPath);
            if (nextElement && nextElement[0]['lea:changeType'] === 'ct_deleted') {
              //if next wrapper is delete after currentwrapper will be removed it will move to the current selection
              //-> move to characters to avoid
              Transforms.move(editor, { distance: 1, reverse: true, unit: 'character' });
            }
            Transforms.removeNodes(editor, { at: wrapperPath });
          } else {
            deleteForward(unit);
          }
        }
      }
    };

    editor.deleteFragment = (options?: { direction?: TextDirection }) => {
      const selectionStart = editor.selection ? Range.start(editor.selection) : null;
      const selectionEnd = editor.selection ? Range.end(editor.selection) : null;

      if (selectionStart && selectionEnd && this.isDynAenderungsvergleich) {
        //Get text and changewrappers inside selection
        const nodesInRange = getNodesOfTypeInRange(
          e,
          [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER],
          [selectionStart.path, selectionEnd.path],
        );
        const [, parentElementPath] = Editor.above(editor, {
          match: (n: Node) =>
            n &&
            [STRUCTURE_ELEMENTS.P, STRUCTURE_ELEMENTS.HEADING, STRUCTURE_ELEMENTS.TABLE_CAPTION].includes(
              (n as Element).type,
            ),
        }) as NodeEntry<Element>;
        //differentiate between starting and ending element because of the offset
        if (nodesInRange?.length >= 2) {
          const [startElement, startElementPath] = nodesInRange[0];
          const [endElement, endElementPath] = nodesInRange[nodesInRange?.length - 1];
          const TextwrapperTextToBeDeleted: Array<string> = [];

          //end element
          if (selectionEnd.offset === endElement?.children[0]?.text?.length) {
            //whole last node is in range
            if (endElement.type === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (endElement['lea:changeType'] === 'ct_deleted') {
                TextwrapperTextToBeDeleted.unshift(endElement.children[0].text || '');
              }
              Transforms.removeNodes(editor, { at: endElementPath, voids: true });
            } else if (endElement.type === STRUCTURE_ELEMENTS.TEXTWRAPPER) {
              TextwrapperTextToBeDeleted.unshift(endElement.children[0].text || '');
              Transforms.removeNodes(editor, { at: endElementPath, voids: true });
            }
          } else {
            if (
              endElement.type === STRUCTURE_ELEMENTS.TEXTWRAPPER &&
              endElement?.children[0]?.text &&
              endElement?.children[0]?.text.length > 0
            ) {
              TextwrapperTextToBeDeleted.unshift(endElement?.children[0]?.text?.substring(0, selectionEnd.offset));
              const textLocation = getLastLocationAt(editor, endElementPath);
              Transforms.delete(editor, {
                at: {
                  anchor: { path: (textLocation as BasePoint).path, offset: 0 },
                  focus: { path: (textLocation as BasePoint).path, offset: selectionEnd.offset },
                },
              });
            }
          }
          //middle elements
          for (let index = nodesInRange?.length - 2; index >= 1; index--) {
            if (
              nodesInRange[index][0].type === STRUCTURE_ELEMENTS.TEXTWRAPPER ||
              (nodesInRange[index][0].type === STRUCTURE_ELEMENTS.CHANGEWRAPPER &&
                nodesInRange[index][0]['lea:changeType'] === 'ct_deleted')
            ) {
              TextwrapperTextToBeDeleted.unshift(nodesInRange[index][0].children[0].text || '');
            }
            Transforms.removeNodes(editor, { at: nodesInRange[index][1], voids: false });
          }

          //Startelement
          if (selectionStart?.offset === 0) {
            //whole first node is in range
            if (startElement.type === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
              if (startElement['lea:changeType'] === 'ct_deleted') {
                TextwrapperTextToBeDeleted.unshift(startElement.children[0].text || '');
              }
              Transforms.removeNodes(editor, { at: startElementPath, voids: true });
            } else if (startElement.type === STRUCTURE_ELEMENTS.TEXTWRAPPER) {
              TextwrapperTextToBeDeleted.unshift(startElement.children[0].text || '');
              //if start element is last textwrapper in parentelement just delete fragment
              const textWrappers = getNodesOfTypeAt(e, [STRUCTURE_ELEMENTS.TEXTWRAPPER], parentElementPath);
              if (textWrappers.length === 1) {
                deleteFragment(options);
              } else {
                Transforms.removeNodes(editor, { at: startElementPath, voids: true });
              }
            }
          } else {
            if (
              startElement.type === STRUCTURE_ELEMENTS.TEXTWRAPPER &&
              startElement?.children[0]?.text &&
              startElement?.children[0]?.text.length > 0
            ) {
              TextwrapperTextToBeDeleted.unshift(startElement?.children[0]?.text?.substring(selectionStart.offset));
            }
            deleteFragment(options);
          }

          //weitere Verarbeitung
          let concatenatedText: string = '';
          TextwrapperTextToBeDeleted.forEach((text: string) => {
            if (text?.trim().length > 0) {
              concatenatedText += ` ${text}`;
            }
          });
          const element = createInlineFragment('ct_deleted', concatenatedText.trim() || '');
          e.insertNode(element);
          if (options?.direction === 'backward') {
            Transforms.move(editor, { distance: concatenatedText.trim().length, reverse: true, unit: 'character' });
          }
        } else {
          //range is inside one text or changewrapper
          //which wrapper is it
          const textFromNodeInRange = nodesInRange[0][0].children[0].text;
          if (nodesInRange[0][0].type == STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
            if (nodesInRange[0][0]['lea:changeType'] === 'ct_deleted') {
              if (options?.direction === 'backward') {
                Transforms.setSelection(editor, {
                  anchor: { path: selectionStart?.path, offset: selectionStart?.offset },
                });
              } else if (options?.direction === 'forward') {
                Transforms.setSelection(editor, {
                  focus: { path: selectionEnd?.path, offset: selectionEnd?.offset },
                });
              }
            } else if (nodesInRange[0][0]['lea:changeType'] === 'ct_inserted') {
              if (selectionStart.offset === 0 && selectionEnd.offset === textFromNodeInRange?.length) {
                Transforms.removeNodes(editor, { at: editor.selection?.focus });
              } else {
                deleteFragment();
              }
            }
          } else if (nodesInRange[0][0].type == STRUCTURE_ELEMENTS.TEXTWRAPPER) {
            if (selectionStart?.offset === 0 && selectionEnd?.offset === nodesInRange[0][0].children[0].text?.length) {
              //whole text from text wrapper is selected
              //if start element is last textwrapper in parentelement just delete fragment
              const textWrappers = getNodesOfTypeAt(e, [STRUCTURE_ELEMENTS.TEXTWRAPPER], parentElementPath);
              if (textWrappers.length === 1) {
                deleteFragment(options);
              } else {
                Transforms.removeNodes(editor, { at: editor.selection?.focus });
              }

              const element = createInlineFragment('ct_deleted', textFromNodeInRange || '');
              e.insertNode(element);
              if (options?.direction === 'backward') {
                Transforms.move(editor, { distance: textFromNodeInRange?.length, reverse: true, unit: 'character' });
              }
            } else {
              //inside single textwrapper - only a portion is selected
              const text = nodesInRange[0][0].children[0].text;
              if (text) {
                const deleteText = text.substring(selectionStart.offset, selectionEnd.offset);
                deleteFragment(options);
                const element = createInlineFragment('ct_deleted', deleteText || '');
                e.insertNode(element);
                if (options?.direction === 'backward') {
                  Transforms.move(editor, { distance: deleteText.length, reverse: true, unit: 'character' });
                }
              }
            }
          }
        }
      } else {
        deleteFragment(options);
      }
    };

    return editor;
  }

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  public keyDown(editor: Editor, event: KeyboardEvent): any {
    const node = getLowestNodeAt(editor);
    const text = node ? node.text : '';
    if (
      !EditableQueries.isEditable(editor, this.editorDocument.documentType, this.isAuthorialNotePlugin) ||
      this.editorDocument.isReadonly
    ) {
      return false;
    }

    if (isLastTextPosition(editor, text) && isRightNavigation(event)) {
      if (!this.isDynAenderungsvergleich) {
        const [wrapper, wrapperPath] = getNodesOfTypeAt(editor, [
          STRUCTURE_ELEMENTS.CHANGEWRAPPER,
          STRUCTURE_ELEMENTS.TEXTWRAPPER,
        ])[0];

        if (wrapper) {
          const nextElementWrapper = getNextElement(editor, wrapperPath);

          if (nextElementWrapper && nextElementWrapper[0]['lea:changeType'] === 'ct_deleted') {
            const nextText = nextElementWrapper[0].children[0].text;
            nextText && Transforms.move(editor, { distance: nextText.length + 3, unit: 'character' });
          } else {
            Transforms.move(editor, { distance: 2, unit: 'character' });
          }
        }
      } else {
        Transforms.move(editor, { distance: 2, unit: 'character' });
        return false;
      }
    }
    if (isFirstTextPosition(editor) && isLeftNavigation(event)) {
      if (!this.isDynAenderungsvergleich) {
        const [wrapper, wrapperPath] = getNodesOfTypeAt(editor, [
          STRUCTURE_ELEMENTS.CHANGEWRAPPER,
          STRUCTURE_ELEMENTS.TEXTWRAPPER,
        ])[0];

        if (wrapper) {
          const previousElementWrapper = getPreviousElement(editor, wrapperPath);

          if (previousElementWrapper && previousElementWrapper[0]['lea:changeType'] === 'ct_deleted') {
            const prevText = previousElementWrapper[0].children[0].text;
            prevText && Transforms.move(editor, { distance: prevText.length + 3, reverse: true, unit: 'character' });
          } else {
            Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
          }
        }
      } else {
        Transforms.move(editor, { distance: 2, reverse: true, unit: 'character' });
        return false;
      }
    }
    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementType = e.type;
    const guid = e.GUID;

    if (elementType === STRUCTURE_ELEMENTS.CHANGEWRAPPER) {
      const changeType = e['lea:changeType'];

      if (!this.isDynAenderungsvergleich) {
        return changeType === 'ct_deleted' ? (
          <></>
        ) : (
          <div
            id={guid && `${this.editorDocument.index}:${guid || ''}`}
            className={`${elementType} ${changeType}`}
            {...props.attributes}
          >
            {props.children}
          </div>
        );
      }
      return changeType === 'ct_deleted' ? (
        <del
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeType}`}
          {...props.attributes}
        >
          {props.children}
        </del>
      ) : (
        <ins
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeType}`}
          {...props.attributes}
        >
          {props.children}
        </ins>
      );
    }
    return null;
  };

  public OptionDialog = ({ editor }: IPluginOptionDialogProps): ReactElement => {
    const { t } = useTranslation();
    const { optionsVisible } = useEditorOptionsVisibleState(EditorOptions.AENDERUNGS_NACHVERFOLGUNG);
    const { closeOptions } = useEditorOptionsVisibleChange(EditorOptions.AENDERUNGS_NACHVERFOLGUNG);
    const daenvOptionRef = useRef<HTMLDivElement>(null);

    const { toggleTriedToDisableActive, toggleDynAenderungsvergleich } = useDynAenderungsvergleichDispatch();
    const { isActive, triedToDisableActive } = useDynAenderungsvergleichState();

    const dynAenvAcceptItems = [
      { key: 'accept', label: t('lea.daenvOptions.accept.accept'), icon: <IconDaenvAcceptChanges /> },
      { key: 'decline', label: t('lea.daenvOptions.accept.decline'), icon: <IconDaenvDeclineChanges /> },
    ];

    useEffect(() => {
      if (daenvOptionRef.current && optionsVisible) {
        AriaControllerPlateg.setAriaLabelsByQuery(
          '.daenv-options .ant-drawer-close',
          'Dynamischen Änderungsvergleich schließen',
        );
        AriaController.setElementIdByClassNameInElement(
          'ant-drawer-close',
          'lea-drawer-daenv-options-close-button',
          daenvOptionRef.current,
        );
      }
    }, [optionsVisible, daenvOptionRef]);

    const toggleDynAV = () => {
      if (isActive) {
        toggleTriedToDisableActive();
      } else {
        toggleDynAenderungsvergleich();
      }
    };

    const selectionHandler = (clickEvent: SelectInfo) => {
      if (!this.isDynAenderungsvergleich) return;
      if (clickEvent.key === 'accept') {
        acceptChanges();
      } else if (clickEvent.key === 'decline') {
        declineChanges();
      }
      // consolidateTextWrappers();
    };

    const acceptChanges = () => {
      removeDeletedWrappers();
      convertInsertedWrappers();
    };

    const declineChanges = () => {
      revertDeletedWrappers();
      removeInsertedWrappers();
    };

    const removeDeletedWrappers = () => {
      const [...deletedNodes] = editor.nodes({
        at: [0],
        match: (n) => (n as Element)['lea:changeType'] === 'ct_deleted',
      });
      if (!deletedNodes) return;
      for (let index = deletedNodes?.length - 1; index >= 0; index--) {
        Transforms.removeNodes(editor, { at: deletedNodes[index][1], voids: false });
      }
    };

    const revertDeletedWrappers = () => {
      const [...deletedNodes] = editor.nodes({
        at: [0],
        match: (n) => (n as Element)['lea:changeType'] === 'ct_deleted',
      });
      if (!deletedNodes) return;
      for (let index = deletedNodes?.length - 1; index >= 0; index--) {
        const pathToRestoreTextWrapper = deletedNodes[index][1];
        const textToRestore = (deletedNodes[index][0] as Element).children[0].text;
        Transforms.removeNodes(editor, { at: pathToRestoreTextWrapper, voids: false });
        if (textToRestore) {
          editor.insertNode(createTextWrapper(textToRestore), { at: pathToRestoreTextWrapper });
        }
      }
    };

    const removeInsertedWrappers = () => {
      const [...insertedNodes] = editor.nodes({
        at: [0],
        match: (n) => (n as Element)['lea:changeType'] === 'ct_inserted',
      });
      if (!insertedNodes) return;
      for (let index = insertedNodes?.length - 1; index >= 0; index--) {
        Transforms.removeNodes(editor, { at: insertedNodes[index][1], voids: false });
      }
    };

    const convertInsertedWrappers = () => {
      const [...insertedNodes] = editor.nodes({
        at: [0],
        match: (n) => (n as Element)['lea:changeType'] === 'ct_inserted',
      });
      if (!insertedNodes) return;
      for (let index = insertedNodes?.length - 1; index >= 0; index--) {
        const pathToConvertToTextWrapper = insertedNodes[index][1];
        const textToConvert = (insertedNodes[index][0] as Element).children[0].text;
        Transforms.removeNodes(editor, { at: pathToConvertToTextWrapper, voids: false });
        if (textToConvert) {
          editor.insertNode(createTextWrapper(textToConvert), { at: pathToConvertToTextWrapper });
        }
      }
    };

    // const consolidateTextWrappers = () => {
    //   //
    //   const [...textNodes] = editor.nodes({
    //     at: [0],
    //     match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.TEXTWRAPPER,
    //   });
    //   if (!textNodes) return;
    //   for (let index = textNodes?.length - 1; index >= 0; index--) {

    //   }
    // };

    return (
      <LeaDrawer
        className="daenv-options"
        placement={'right'}
        open={optionsVisible}
        onClose={closeOptions}
        title={t('lea.daenvOptions.title')}
        expandable={true}
        refProp={daenvOptionRef}
        ariaLabel={t('lea.daenvOptions.ariaLabel')}
      >
        <div className="daenv-options-wrapper">
          <Collapse className="options-collapse" defaultActiveKey={[0]} ghost collapsible="icon">
            <Panel
              id={'daenv-options-panel'}
              header={<div className="panel-header">{t('lea.daenvOptions.switch.title')}</div>}
              key={0}
            >
              <div className="daenvActiveSwitch">
                <Switch
                  disabled={triedToDisableActive}
                  id="daenv-active-switch"
                  title={t('lea.daenvOptions.switch.title')}
                  onChange={toggleDynAV}
                  checked={isActive}
                  className="switch"
                />
                <div>{t('lea.daenvOptions.switch.description')}</div>
              </div>
            </Panel>
          </Collapse>
          <Divider className="option-divider" />
          <Collapse className="options-collapse" defaultActiveKey={[0]} ghost collapsible="icon">
            <Panel
              id="daenv-accept-panel"
              header={<div className="panel-header">{t('lea.daenvOptions.accept.title')}</div>}
              key={0}
            >
              <Menu
                disabled={!this.isDynAenderungsvergleich}
                id="contextMenu"
                mode="vertical"
                items={dynAenvAcceptItems}
                onSelect={selectionHandler}
                selectedKeys={[]}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
              />
              <div className="daenvAccept"></div>
            </Panel>
          </Collapse>
        </div>
      </LeaDrawer>
    );
  };
}
