// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { Element } from '../../../interfaces';
import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../plugin-core';
import { isChromiumGreaterThan129, isFirefox } from '../../plugin-core/utils';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';

export class ContentEditablePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    return editor;
  }

  public useEffect(_editor: ReactEditor): void {
    const editable = global.document.getElementById(
      `${this.isAuthorialNotePlugin ? 'authorialNote' : 'editable'}-area-${this.editorDocument.index}`,
    );
    if (!this.editorDocument.isReadonly && editable && !(isFirefox || isChromiumGreaterThan129)) {
      editable.contentEditable = 'false';
    }
    if (editable) {
      editable.setAttribute('role', 'none');
    }
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const guid = e.GUID;
    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';

    if (
      !this.editorDocument.isReadonly &&
      elementType === STRUCTURE_ELEMENTS.AKOMANTOSO &&
      (isFirefox || isChromiumGreaterThan129)
    ) {
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          contentEditable={false}
          suppressContentEditableWarning={true}
          onFocus={(e) => e.stopPropagation()}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
