// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';

import { isOfType, isWithinTypeHierarchy } from '../../../plugin-core/utils';
import { ANLAGE_STRUCTURE } from '../AnlageStructure';

export class AnlageAbsatzQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === ANLAGE_STRUCTURE.P : isOfType(editor, ANLAGE_STRUCTURE.P);
    if (isPElement && isWithinTypeHierarchy(editor, [ANLAGE_STRUCTURE.MAIN_BODY], path)) {
      return true;
    }
    return false;
  }
}
