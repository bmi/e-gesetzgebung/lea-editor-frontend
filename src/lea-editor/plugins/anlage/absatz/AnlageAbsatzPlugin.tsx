// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../plugin-core';
import { isEnter, isFirstPosition, isFirstSiblingOfType, isLastPosition, Unit } from '../../../plugin-core/utils';
import { ANLAGE_STRUCTURE } from '../AnlageStructure';
import { AnlageAbsatzQueries as Queries } from '.';

import { Element } from '../../../../interfaces';
import { PElementCommands } from '../../../plugin-core/element-commands/PElementCommands';

export class AnlageAbsatzPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, ANLAGE_STRUCTURE.P)) {
        PElementCommands.mergeAbsaetze(editor, [ANLAGE_STRUCTURE.P]);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isEnter(event) && isLastPosition(editor) && Queries.isEditable(editor)) {
      PElementCommands.insertAbsatz(editor);
      event.preventDefault();
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;

    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          contentEditable={true}
          tabIndex={0}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else if (elementType === ANLAGE_STRUCTURE.LONGTITLE) {
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          role="heading"
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
