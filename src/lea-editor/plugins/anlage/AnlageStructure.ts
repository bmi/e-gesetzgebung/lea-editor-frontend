// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';

export const ANLAGE_STRUCTURE = {
  LONGTITLE: STRUCTURE_ELEMENTS.LONG_TITLE,
  AUTHORIALNOTE: STRUCTURE_ELEMENTS.AUTHORIALNOTE,
  MAIN_BODY: STRUCTURE_ELEMENTS.MAIN_BODY,
  P: STRUCTURE_ELEMENTS.P,
  DOCTITLE: STRUCTURE_ELEMENTS.DOCTITLE,
  PREFACE: STRUCTURE_ELEMENTS.PREFACE,
};
