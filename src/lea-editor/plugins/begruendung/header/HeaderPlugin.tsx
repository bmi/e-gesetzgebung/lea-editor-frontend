// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';

import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../plugin-core';
import { HeaderQueries as Queries } from './HeaderQueries';

import { Unit, isFirstTextPosition, isLastPosition, getFirstChildIndexAt } from '../../../plugin-core/utils';
import { Element } from '../../../../interfaces';
import { BEGRUENDUNG_STRUCTURE } from '../BegruendungStructure';
import { ReactI18NextChild } from 'react-i18next';

export class HeaderPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstTextPosition(editor)) {
        deleteBackward(unit);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public keyDown(editor: Editor): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);

    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] ?? '';
    const guid = e.GUID;
    const fromEgfa = e['lea:fromEgfa'] ? 'fromEgfa' : '';

    if (elementType === BEGRUENDUNG_STRUCTURE.HCONTAINER || elementType === BEGRUENDUNG_STRUCTURE.TBLOCK) {
      const headingPosition = getFirstChildIndexAt(props.editor, BEGRUENDUNG_STRUCTURE.HEADING, elementPath);
      const heading = (props.children as Element[]).splice(0, headingPosition + 1);
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark} ${fromEgfa}`}
          {...props.attributes}
        >
          <div className="headingContent" role="heading">
            {heading as ReactI18NextChild}
          </div>
          {props.children}
        </div>
      );
    } else if (Queries.isEditableRender(props.editor, elementPath, elementType, e['lea:editable'])) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark} contentEditable ${fromEgfa}`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role="textbox"
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else if (elementType === BEGRUENDUNG_STRUCTURE.LONGTITLE) {
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark}`}
          role="heading"
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else {
      return null;
    }
  };
}
