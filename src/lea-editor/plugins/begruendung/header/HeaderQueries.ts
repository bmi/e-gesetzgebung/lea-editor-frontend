// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';
import { isOfType, isWithinEditableNode, isWithinTypeHierarchy } from '../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../../plugins/begruendung/BegruendungStructure';

export class HeaderQueries {
  public static isEditableRender(editor: Editor, path?: Path, type?: string, editable?: boolean): boolean {
    const isHeading = type
      ? type === BEGRUENDUNG_STRUCTURE.HEADING
      : isOfType(editor, BEGRUENDUNG_STRUCTURE.HEADING) &&
        type !== BEGRUENDUNG_STRUCTURE.HCONTAINER &&
        type !== BEGRUENDUNG_STRUCTURE.TBLOCK;
    const isRef = type ? type === BEGRUENDUNG_STRUCTURE.REF : isOfType(editor, BEGRUENDUNG_STRUCTURE.REF);
    const isEditable = editable ?? false;
    return (
      (isRef &&
        isWithinTypeHierarchy(editor, [BEGRUENDUNG_STRUCTURE.HEADING, BEGRUENDUNG_STRUCTURE.HCONTAINER], path)) ||
      (isHeading && isEditable)
    );
  }

  public static isEditable(editor: Editor): boolean {
    const isHeading = isOfType(editor, BEGRUENDUNG_STRUCTURE.HEADING);
    const isRef = isOfType(editor, BEGRUENDUNG_STRUCTURE.REF);
    const isEditable = isWithinEditableNode(editor);
    return (
      (isRef && isWithinTypeHierarchy(editor, [BEGRUENDUNG_STRUCTURE.HEADING, BEGRUENDUNG_STRUCTURE.HCONTAINER])) ||
      (isHeading && isEditable)
    );
  }
}
