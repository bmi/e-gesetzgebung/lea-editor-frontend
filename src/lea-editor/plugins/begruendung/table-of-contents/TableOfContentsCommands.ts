// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Node, NodeEntry, Path, Transforms } from 'slate';
import { DataNode } from 'antd/lib/tree';

import { Element, TextWrapper } from '../../../../interfaces';
import { BEGRUENDUNG_GLIEDERUNGSEBENEN, BEGRUENDUNG_STRUCTURE } from '../BegruendungStructure';
import {
  createElementOfType,
  createTextWrapper,
  getHeadingTextFromNode,
  isLastSiblingOfType,
} from '../../../plugin-core/utils';
import { DocumentType } from '../../../../api';

export class TableOfContentsCommands {
  public static getContentTableData(e: Editor): DataNode[] {
    const treeData: DataNode[] = [];
    if (e.children && e.children.length > 0) {
      (e.children as Element[]).forEach((n: Element, index: number) => {
        TableOfContentsCommands.createContentTable(n, treeData, [index]);
      });
    }
    return treeData;
  }

  private static createContentTable(node: Element, array: DataNode[] | undefined, path: number[]) {
    if (!node.children || !array) {
      return;
    }

    const key = path.join('-');
    if (BEGRUENDUNG_GLIEDERUNGSEBENEN.some((x) => x === node.type)) {
      if (
        node.children.length > 1 &&
        (node.children[0] as Element).type === BEGRUENDUNG_STRUCTURE.NUM &&
        (node.children[1] as Element).type === BEGRUENDUNG_STRUCTURE.HEADING
      ) {
        const numText = Node.string(node.children[0]);
        const headingText = Node.string(node.children[1]);

        array.push({
          title: `${numText} ${headingText}`,
          key,
          children: [],
        });
      } else if (
        node.children.length > 1 &&
        (node.children[0] as Element).type === BEGRUENDUNG_STRUCTURE.HEADING &&
        (node.children[1] as Element).type === BEGRUENDUNG_STRUCTURE.CONTENT
      ) {
        const headingText = getHeadingTextFromNode(node.children[0] as Element) || 'Begründungsabschnitt';
        array.push({
          title: `${headingText}`,
          key,
          children: [],
        });
      }
    }

    const i = array.length - 1;
    node.children.forEach((n: Element | TextWrapper, index: number) => {
      TableOfContentsCommands.createContentTable(
        n as Element,
        array[i]?.children && key.startsWith(array[i].key.toString()) ? array[i].children : array,
        [...path, index],
      );
    });
  }

  public static addBelow(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    _type: string,
    documentType?: DocumentType,
  ): NodeEntry<Element> | null {
    const [, path] = nodeEntry;

    const newElement: Element = TableOfContentsCommands.getElementForType(documentType);

    const newPath: Path = path.map((x, y) => (y === path.length - 1 ? ++x : x));
    Transforms.insertNodes(editor, newElement, { at: newPath });
    return Editor.node(editor, newPath) as NodeEntry<Element>;
  }

  public static addElement(
    editor: Editor,
    nodeEntry: NodeEntry<Element>,
    type: string,
    documentType?: DocumentType,
  ): NodeEntry<Element> | null {
    const [selectedElement, path] = nodeEntry;

    const newElement: Element = TableOfContentsCommands.getElementForType(documentType);

    const [...typeElements] = Editor.nodes(editor, {
      at: path,
      match: (element) => (element as Element).type === type,
    });
    const newPath: Path = typeElements.length > 0 ? typeElements[1][1] : path.concat(selectedElement.children.length);
    Transforms.insertNodes(editor, newElement, { at: newPath });
    return Editor.node(editor, newPath) as NodeEntry<Element>;
  }

  public static deleteElement(editor: Editor, nodeEntry: NodeEntry<Element>): NodeEntry<Element> | null {
    const [, path] = nodeEntry;

    const isLastSibling = isLastSiblingOfType(editor, BEGRUENDUNG_STRUCTURE.HCONTAINER, path);
    Transforms.delete(editor, { at: path });
    const newPath = isLastSibling ? path.map((x, y) => (y === path.length - 1 ? --x : x)) : path;
    return Editor.node(editor, newPath) as NodeEntry<Element>;
  }

  private static getElementForType(documentType?: DocumentType): Element {
    return TableOfContentsCommands.createBegruendungsAbschnittRegelungstext(documentType);
  }

  private static createBegruendungsAbschnittRegelungstext(_documentType?: DocumentType): Element {
    const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
    const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
    const refPlaceholderText = '';
    const ref = createElementOfType(BEGRUENDUNG_STRUCTURE.REF, [createTextWrapper(refPlaceholderText)], false);
    ref.href = '';
    const heading = createElementOfType(BEGRUENDUNG_STRUCTURE.HEADING, [createTextWrapper('Zu '), ref]);
    const begruendungsAbschnittRegelungstext = createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [
      heading,
      content,
    ]);
    begruendungsAbschnittRegelungstext.refersTo = 'begruendungsabschnitt-regelungstext';
    begruendungsAbschnittRegelungstext.name = 'attributsemantik-noch-undefiniert';
    return begruendungsAbschnittRegelungstext;
  }
}
