// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseEditor, Editor, NodeEntry } from 'slate';
import { ContextMenuOption, Element } from '../../../../interfaces';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { EditorDocument } from '../../../plugin-core';
import { BEGRUENDUNG_STRUCTURE } from '../BegruendungStructure';
import { getSiblings } from '../../../plugin-core/utils';

export class TableOfContentsQueries {
  public static getOptions(
    editor: BaseEditor,
    nodeEntry: NodeEntry<Element>,
    setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void,
    editorDocument?: EditorDocument,
  ): ContextMenuOption[] {
    const [element] = nodeEntry;
    const options: ContextMenuOption[] = [];
    if (element.refersTo === 'begruendungsabschnitt-regelungstext') {
      options.push({
        type: BEGRUENDUNG_STRUCTURE.HCONTAINER,
        onClick: () => {
          const newNodeEntry = Commands.addBelow(
            editor,
            nodeEntry,
            BEGRUENDUNG_STRUCTURE.HCONTAINER,
            editorDocument?.documentType,
          );
          setTargetNodeEntry(newNodeEntry);
        },
      });
    } else if (element.refersTo === 'begruendung-besonderer-teil') {
      options.push({
        type: BEGRUENDUNG_STRUCTURE.HCONTAINER,
        onClick: () => {
          const newNodeEntry = Commands.addElement(
            editor,
            nodeEntry,
            BEGRUENDUNG_STRUCTURE.HCONTAINER,
            editorDocument?.documentType,
          );
          setTargetNodeEntry(newNodeEntry);
        },
      });
    }
    return options;
  }

  public static getOptionForDeleteElement(editor: Editor, nodeEntry: NodeEntry<Element>): boolean {
    const [element, path] = nodeEntry;

    const siblings = getSiblings(editor, BEGRUENDUNG_STRUCTURE.HCONTAINER, path);

    if (siblings.length > 1 && element.refersTo === 'begruendungsabschnitt-regelungstext') {
      return true;
    }
    return false;
  }
}
