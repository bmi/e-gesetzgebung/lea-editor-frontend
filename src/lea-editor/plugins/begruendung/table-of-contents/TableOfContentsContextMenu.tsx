// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useTranslation } from 'react-i18next';

import { IPluginContextMenuProps, useEditorDocumentState, useEditorOnFocusState } from '../../../plugin-core';
import React, { ReactElement, useMemo } from 'react';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsQueries as Queries } from './TableOfContentsQueries';
import {
  useContextMenuChange,
  useContextMenuNodeEntryState,
} from '../../../plugin-core/editor-store/workspace/contextMenu';
import { ItemType } from 'antd/lib/menu/interface';
import { TableOfContentsPlugin } from './TableOfContentsPlugin';
import { NodeEntry } from 'slate';
import { Element } from '../../../../interfaces';

interface ITableOfContentsContextMenuProps extends IPluginContextMenuProps {
  plugin: TableOfContentsPlugin;
}

export const TableOfContentsContextMenu = ({
  keyName,
  plugin,
  editor,
}: ITableOfContentsContextMenuProps): ReactElement => {
  const { t } = useTranslation();
  const editorOnFocus = useEditorOnFocusState();
  const { setTargetNodeEntry } = useContextMenuChange();
  const { nodeEntry } = useContextMenuNodeEntryState();
  const { editorDocument } = useEditorDocumentState(plugin.editorDocument.index);

  const renderType = (type: string): string => {
    const typeSplit = type.split(':');
    type = typeSplit[typeSplit.length - 1];
    return t(`lea.contextMenu.tableOfContents.options.types.${type}`);
  };

  const getItemTypes = (nodeEntry: NodeEntry<Element>): ItemType[] => {
    const addElement = Queries.getOptions(editor, nodeEntry, setTargetNodeEntry, editorDocument);
    const deleteElement = Queries.getOptionForDeleteElement(editor, nodeEntry);
    const newItemTypes: ItemType[] = [];
    if (addElement.length > 0) {
      const children: ItemType[] = [];
      addElement.forEach(({ type, onClick }) => {
        children.push({
          key: `${keyName}-tableOfContents-begruendung-addElement-${type.replace(':', '-')}`,
          title: renderType(type),
          label: renderType(type),
          onClick,
        });
      });
      newItemTypes.push({
        key: `${keyName}-tableOfContents-begruendung-addElement-group`,
        label: t('lea.contextMenu.tableOfContents.addElement'),
        popupClassName: 'submenu-container',
        children,
      });
    }
    if (deleteElement) {
      if (addElement.length > 0) {
        newItemTypes.push({ type: 'divider' });
      }
      newItemTypes.push({
        key: `${keyName}-tableOfContents-begruendung-deleteElement`,
        title: t('lea.contextMenu.tableOfContents.deleteElement.title'),
        label: t('lea.contextMenu.tableOfContents.deleteElement.title'),
        onClick: () => {
          const newNodeEntry = Commands.deleteElement(editor, nodeEntry);
          setTargetNodeEntry(newNodeEntry);
        },
      });
    }
    return newItemTypes;
  };

  useMemo(() => {
    if (nodeEntry && !plugin.editorDocument.isReadonly && plugin.editorDocument.index === editorOnFocus.index) {
      plugin.contextMenuItemTypes = getItemTypes(nodeEntry);
    } else {
      plugin.contextMenuItemTypes = null;
    }
  }, [nodeEntry]);

  return <></>;
};
