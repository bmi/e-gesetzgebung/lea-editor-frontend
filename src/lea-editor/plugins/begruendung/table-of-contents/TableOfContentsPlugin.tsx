// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect } from 'react';
import { Editor, Operation } from 'slate';
import { ReactEditor } from 'slate-react';
import {
  Plugin,
  IPluginNavigationDialogProps,
  IPluginContextMenuProps,
  PluginProps,
  useEditorOnFocusState,
} from '../../../plugin-core';
import { TableOfContentsCommands as Commands } from './TableOfContentsCommands';
import { TableOfContentsContextMenu } from './TableOfContentsContextMenu';
import { useTableOfContentsDispatch, useTableOfContentsState } from '../../../plugin-core/editor-store/workspace';
import {
  TableOfContentsDataSlice,
  TableOfContentsManager,
  useTableOfContentsDataDispatch,
  useTableOfContentsDataState,
} from '../../../Editor';
import { DataNode } from 'antd/lib/tree';
import { ReducersMapObject } from '@reduxjs/toolkit';
import { ItemType } from 'antd/lib/menu/interface';
import { Checkbox } from 'antd';

export class TableOfContentsPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { apply } = editor;

    // Check if structure has changed
    editor.apply = (op: Operation) => {
      apply(op);
      if (
        ['remove_node', 'insert_node', 'split_node', 'move_node', 'remove_text', 'insert_text'].some(
          (x) => x === op.type,
        )
      ) {
        this.setTableOfContentsData(Commands.getContentTableData(editor));
      }
    };

    return editor;
  }

  private setTableOfContentsData: (dataNodes: DataNode[]) => void = (_dataNodes: DataNode[]) => {};

  private _viewToolItemTypes: ItemType[] | null = [];

  public set viewToolItemTypes(itemTypes: ItemType[] | null) {
    this._viewToolItemTypes = itemTypes;
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return this._viewToolItemTypes;
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public set contextMenuItemTypes(itemTypes: ItemType[] | null) {
    this._contextMenuItemTypes = itemTypes;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  public useEffect(editor: ReactEditor): void {
    this.setTableOfContentsData(Commands.getContentTableData(editor));
  }

  public NavigationDialog = ({ editor }: IPluginNavigationDialogProps): ReactElement => {
    const editorOnFocus = useEditorOnFocusState();
    const { tableOfContentsData } = useTableOfContentsDataState(this.editorDocument.index);
    return (
      <TableOfContentsManager
        editor={editor}
        index={this.editorDocument.index}
        isReadonly={this.editorDocument.isReadonly}
        tableOfContentsData={tableOfContentsData}
        isAvailable={editorOnFocus.index === this.editorDocument.index}
        documentTitle={this.editorDocument.documentTitle}
      />
    );
  };

  public ViewTools = (): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const { visible } = useTableOfContentsState();
    const { toggelTableOfContents } = useTableOfContentsDispatch();
    if (editorOnFocus.index === this.editorDocument.index) {
      this.viewToolItemTypes = [
        {
          label: (
            <Checkbox onChange={toggelTableOfContents} checked={visible}>
              Strukturanzeige
            </Checkbox>
          ),
          key: 'viewTools-begruendung-tableOfContents',
          title: 'Strukturanzeige',
        },
      ];
    } else {
      this.viewToolItemTypes = null;
    }
    return <></>;
  };

  public RegisterDispatchFunctions = (): ReactElement => {
    const { setTableOfContentsData } = useTableOfContentsDataDispatch(this.editorDocument.index);

    useEffect(() => {
      this.setTableOfContentsData = setTableOfContentsData;
    }, []);
    return <></>;
  };

  public reducer(): ReducersMapObject {
    const slice = TableOfContentsDataSlice(this.editorDocument.index);
    return { [slice.name]: slice.reducer };
  }

  public ContextMenu = ({ keyName, editor }: IPluginContextMenuProps) =>
    TableOfContentsContextMenu({
      editor,
      keyName,
      plugin: this,
    });
}
