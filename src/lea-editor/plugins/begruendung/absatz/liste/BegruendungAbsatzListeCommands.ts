// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms } from 'slate';
import { ReactEditor } from 'slate-react';

import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLastLocationAt,
  getLowestNodeAt,
  getSelectedSiblingIndex,
  isLastSiblingOfType,
  isPreviousNeighborOfType,
} from '../../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../BegruendungStructure';

export class BegruendungAbsatzListeCommands {
  public static mergeListItems(editor: Editor): void {
    const [selectedListItem, selectedListItemPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.LIST_ITEM,
    }) as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if list item to be deleted is empty.
    // Since merge of list item containing text will be a future demand it's handled within here
    if (selectedListItem.children.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous list item's text
    const [previousListItem, previousListItemPath] = Editor.previous(editor, {
      at: selectedListItemPath,
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.LIST_ITEM,
    }) as NodeEntry<Element>;

    const previousTextNode = getLowestNodeAt(editor, [...previousListItemPath, previousListItem.children.length - 1]);

    if (previousTextNode && selectedTextNode) {
      //Merge TextWrapper and remove parts from lists
      Transforms.removeNodes(editor, { at: selectedListItemPath });

      Transforms.select(editor, getLastLocationAt(editor, previousListItemPath));
      // ReactEditor.focus fails unit test because there is no DOM
      try {
        ReactEditor.focus(editor as ReactEditor);
      } catch (error) {}
    }
  }

  public static insertListe(editor: Editor): void {
    // Get current paragraph
    const [currentParagraph] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === BEGRUENDUNG_STRUCTURE.P,
    });

    if (!currentParagraph) {
      return;
    }
    const [, currentParagraphPath] = currentParagraph as NodeEntry<Element>;
    // Remove current paragraph
    Transforms.removeNodes(editor, { at: currentParagraphPath });

    const PElement = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
    const listItem = createElementOfType(BEGRUENDUNG_STRUCTURE.LIST_ITEM, [PElement], true, { value: '•' });
    const list = createElementOfType(BEGRUENDUNG_STRUCTURE.UNORDERED_LIST, [listItem]);

    Transforms.insertNodes(editor, list, { at: currentParagraphPath });

    // Determine list item content path and select it
    const [item] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === BEGRUENDUNG_STRUCTURE.LIST_ITEM,
      at: currentParagraphPath,
    });
    const [, itemPath] = item as NodeEntry<Element>;
    Transforms.select(editor, getLastLocationAt(editor, itemPath));
    // ReactEditor.focus fails unit test because there is no DOM
    try {
      ReactEditor.focus(editor as ReactEditor);
    } catch (error) {}
  }

  public static insertListItem(editor: Editor): void {
    const selectedTextNode = getLowestNodeAt(editor);
    const text = selectedTextNode ? selectedTextNode.text : '';

    // Currently insertion is not allowed if current list item is empty.
    // Since this could be a future demand it's handled within here.
    if (text.length === 0 && !isPreviousNeighborOfType(editor, BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE)) {
      return;
    }

    const [selectedListItem] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.LIST_ITEM,
    });
    const [, path] = selectedListItem as NodeEntry<Ancestor>;

    // Create next sibling's path
    const newPath = path.map((x, y) => (y === path.length - 1 ? x + 1 : x));

    // Insert new list item
    const PElement = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
    const newItem = createElementOfType(BEGRUENDUNG_STRUCTURE.LIST_ITEM, [PElement], true, { value: '•' });
    Transforms.insertNodes(editor, newItem, { at: newPath });

    // Select newly created node
    Transforms.select(editor, getLastLocationAt(editor, newPath));
    // ReactEditor.focus fails unit test because there is no DOM
    try {
      ReactEditor.focus(editor as ReactEditor);
    } catch (error) {}
  }

  public static deleteList(editor: Editor): void {
    // Get selected list
    const [selectedList] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.UNORDERED_LIST,
    });
    const [, listPath] = selectedList as NodeEntry<Ancestor>;

    const selectedTextNode = getLowestNodeAt(editor);

    if (selectedTextNode && selectedTextNode.text.length > 0) {
      return;
    }

    // Delete selected list
    Transforms.removeNodes(editor, { at: listPath });

    // Insert paragraph instead and select it
    const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
    Transforms.insertNodes(editor, p, { at: listPath });

    Transforms.select(editor, getLastLocationAt(editor, listPath));
    // ReactEditor.focus fails unit test because there is no DOM
    try {
      ReactEditor.focus(editor as ReactEditor);
    } catch (error) {}
  }

  public static endList(editor: Editor): void {
    const listItemNumber = getSelectedSiblingIndex(editor, BEGRUENDUNG_STRUCTURE.LIST_ITEM);

    // Only end list if it has two or more items
    if (listItemNumber < 2 || !isLastSiblingOfType(editor, BEGRUENDUNG_STRUCTURE.LIST_ITEM)) {
      return;
    }

    const [selectedList] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.LIST_ITEM,
    });

    const [, path] = selectedList as NodeEntry<Ancestor>;

    // Path of new paragreaph
    const newPath = [...path.slice(0, path.length - 2), path[path.length - 2] + 1];

    // Remove selected list item
    Transforms.removeNodes(editor, { at: path });

    // Create new paragraph
    const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
    Transforms.insertNodes(editor, p, { at: newPath });

    Transforms.select(editor, getLastLocationAt(editor, newPath));
    // ReactEditor.focus fails unit test because there is no DOM
    try {
      ReactEditor.focus(editor as ReactEditor);
    } catch (error) {}
  }
}
