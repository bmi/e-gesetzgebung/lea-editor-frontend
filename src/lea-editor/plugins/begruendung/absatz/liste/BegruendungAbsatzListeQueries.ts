// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path, Range } from 'slate';
import { Element } from '../../../../../interfaces';

import {
  isFirstSiblingOfType,
  isOfType,
  isWithinTypeHierarchy,
  isPreviousNeighborOfType,
} from '../../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../BegruendungStructure';
import { TABELLE_STRUCTURE } from '../../../../plugin-core/generic-plugins/tabelle';

export class BegruendungAbsatzListeQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === BEGRUENDUNG_STRUCTURE.P : isOfType(editor, BEGRUENDUNG_STRUCTURE.P);
    return (
      isPElement &&
      isWithinTypeHierarchy(
        editor,
        [
          BEGRUENDUNG_STRUCTURE.LIST_ITEM,
          BEGRUENDUNG_STRUCTURE.UNORDERED_LIST,
          BEGRUENDUNG_STRUCTURE.CONTENT,
          BEGRUENDUNG_STRUCTURE.HCONTAINER,
        ],
        path,
      ) &&
      !isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.TABLE], path)
    );
  }

  public static canCreateList(editor: Editor): boolean {
    if (!editor.selection || Range.isExpanded(editor.selection)) return false;
    const isPElement = isOfType(editor, BEGRUENDUNG_STRUCTURE.P);
    const isInTable = isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.TABLE]);
    if (!isPElement || isInTable) return false;
    const isFirstPElement = isFirstSiblingOfType(editor, BEGRUENDUNG_STRUCTURE.P);
    const [selectedPNode] = Editor.nodes<Element>(editor, {
      match: (n) => (n as Element).type === BEGRUENDUNG_STRUCTURE.P,
    });
    const isListBefore = isPreviousNeighborOfType(editor, BEGRUENDUNG_STRUCTURE.UNORDERED_LIST, selectedPNode[1]);
    return !isListBefore && !isFirstPElement;
  }
}
