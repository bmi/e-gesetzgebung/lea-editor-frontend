// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import {
  CombinedPluginRenderProps,
  IPluginToolbarProps,
  Plugin,
  PluginProps,
  useEditableState,
  useEditorOnFocusState,
  useSelectionState,
} from '../../../../plugin-core';
import { IconButton, ListIcon } from '@lea/ui';

import {
  getLowestNodeAt,
  getSiblings,
  isCutPaste,
  isEnter,
  isFirstPosition,
  isFirstSiblingOfType,
  isLastPosition,
  isOfType,
  isPreviousNeighborOfType,
  Unit,
} from '../../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../BegruendungStructure';
import { BegruendungAbsatzListeQueries as Queries, BegruendungAbsatzListeCommands as Commands } from '.';
import { Element } from '../../../../../interfaces';

const onClickEvent = new Event('toggleBegruendungList');

export class BegruendungAbsatzListePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  private readonly _toolRefs = Array(1)
    .fill(0)
    .map(() => React.createRef<HTMLElement>());

  public get toolRefs(): React.RefObject<HTMLElement>[] {
    return this._toolRefs;
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, BEGRUENDUNG_STRUCTURE.LIST_ITEM)) {
        Commands.mergeListItems(editor);
      } else if (isFirstPosition(editor) && getSiblings(editor, BEGRUENDUNG_STRUCTURE.LIST_ITEM).length === 1) {
        Commands.deleteList(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isCutPaste(event) || (isEnter(event) && !isLastPosition(editor))) {
      event.preventDefault();
      return false;
    }

    if (isEnter(event) && isLastPosition(editor)) {
      const node = getLowestNodeAt(editor);
      const text = node ? node.text : '';
      if (text.length === 0 && !isPreviousNeighborOfType(editor, BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE)) {
        Commands.endList(editor);
      } else {
        Commands.insertListItem(editor);
      }
      event.preventDefault();
      return false;
    }

    return true;
  }

  public ToolbarTools = ({ editor }: IPluginToolbarProps): ReactElement | null => {
    const editorOnFocus = useEditorOnFocusState();
    const { isEditable } = useEditableState(this.editorDocument.index);
    const { selection } = useSelectionState();
    const [isActive, setIsActive] = useState(false);
    const [hasHover, setHasHover] = useState(false);
    const [isDisabled, setIsDisabled] = useState(false);

    useEffect(() => {
      const disabled = this.editorDocument.isReadonly || !isEditable || !Queries.canCreateList(editor);
      setIsDisabled(disabled);
      if (isDisabled && !disabled) {
        setHasHover(false);
        setIsActive(false);
      }
    }, [isEditable, selection]);

    const handleOnClick = useCallback(() => {
      setIsActive(!isActive);
      if (
        !this.editorDocument.isReadonly &&
        isOfType(editor, BEGRUENDUNG_STRUCTURE.P) &&
        !isFirstSiblingOfType(editor, BEGRUENDUNG_STRUCTURE.P)
      ) {
        Commands.insertListe(editor);
      }
    }, [isActive]);

    React.useEffect(() => {
      document.addEventListener('toggleBegruendungList', handleOnClick);
      return () => document.removeEventListener('toggleBegruendungList', handleOnClick);
    });

    return !this.editorDocument.isReadonly && editorOnFocus.index === this.editorDocument.index ? (
      <IconButton
        id="unordered-list-button"
        onClick={() => document.dispatchEvent(onClickEvent)}
        aria-label="Liste einfügen"
        disabled={isDisabled}
        onHover={setHasHover}
        icon={<ListIcon hasHover={!isDisabled && hasHover} />}
      />
    ) : null;
  };

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);
    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;
    const value = e.value;
    if (Queries.isEditable(props.editor, elementPath, elementType)) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark} contentEditable`}
          role="textbox"
          contentEditable={true}
          suppressContentEditableWarning={true}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else if (elementType === BEGRUENDUNG_STRUCTURE.LIST_ITEM) {
      return (
        <li
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          value={value}
          {...props.attributes}
        >
          {props.children}
        </li>
      );
    } else if (elementType === BEGRUENDUNG_STRUCTURE.UNORDERED_LIST) {
      return (
        <ul
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </ul>
      );
    }
    return null;
  };
}
