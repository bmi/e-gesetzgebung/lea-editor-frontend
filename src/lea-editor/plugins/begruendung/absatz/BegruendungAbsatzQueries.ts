// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';

import { isOfType, isWithinTypeHierarchy } from '../../../plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../BegruendungStructure';
import { TABELLE_STRUCTURE } from '../../../plugin-core/generic-plugins/tabelle';

export class BegruendungAbsatzQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === BEGRUENDUNG_STRUCTURE.P : isOfType(editor, BEGRUENDUNG_STRUCTURE.P);
    return (
      isPElement &&
      (isWithinTypeHierarchy(editor, [BEGRUENDUNG_STRUCTURE.CONTENT, BEGRUENDUNG_STRUCTURE.HCONTAINER], path) ||
        isWithinTypeHierarchy(
          editor,
          [BEGRUENDUNG_STRUCTURE.TBLOCK, BEGRUENDUNG_STRUCTURE.CONTENT, BEGRUENDUNG_STRUCTURE.HCONTAINER],
          path,
        )) &&
      !isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.TABLE], path) &&
      !isWithinTypeHierarchy(editor, [BEGRUENDUNG_STRUCTURE.UNORDERED_LIST], path)
    );
  }
}
