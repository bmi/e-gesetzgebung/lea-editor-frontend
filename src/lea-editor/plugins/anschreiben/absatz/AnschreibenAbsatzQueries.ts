// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';

import {
  isOfType,
  isWithinTypeHierarchy,
  isWithinExactTypeHierarchy,
  getChildrenOfTypeAt,
  getNodesOfTypeAt,
  getNodeAt,
} from '../../../plugin-core/utils';
import { ANSCHREIBEN_STRUCTURE } from '../AnschreibenStructure';

export class AnschreibenAbsatzQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    return (
      this.isEditableMainBodyElement(editor, path, type) ||
      this.isEditablePrefaceElement(editor, path, type) ||
      this.isEditableRefElement(editor, path, type)
    );
  }

  public static isEditableMainBodyElement(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === ANSCHREIBEN_STRUCTURE.P : isOfType(editor, ANSCHREIBEN_STRUCTURE.P);
    if (isPElement && isWithinTypeHierarchy(editor, [ANSCHREIBEN_STRUCTURE.MAIN_BODY], path)) {
      const node = getNodeAt(editor, path);
      if (node?.[0].type !== ANSCHREIBEN_STRUCTURE.P) {
        const [, pElementPath] = getNodesOfTypeAt(editor, [ANSCHREIBEN_STRUCTURE.P], path)[0];
        path = pElementPath;
      }
      return (
        getChildrenOfTypeAt(
          editor,
          [ANSCHREIBEN_STRUCTURE.ORGANIZATION, ANSCHREIBEN_STRUCTURE.REF, ANSCHREIBEN_STRUCTURE.DOCTITLE],
          path,
        ).length === 0
      );
    }
    return false;
  }

  public static isEditablePrefaceElement(editor: Editor, path?: Path, type?: string) {
    if (path) {
      return (
        isWithinExactTypeHierarchy(
          editor,
          [ANSCHREIBEN_STRUCTURE.P, ANSCHREIBEN_STRUCTURE.TBLOCK, ANSCHREIBEN_STRUCTURE.PREFACE],
          path,
        ) ||
        type === ANSCHREIBEN_STRUCTURE.DOC_AUTHORITY ||
        type === ANSCHREIBEN_STRUCTURE.DOC_DATE
      );
    } else {
      return (
        isWithinTypeHierarchy(
          editor,
          [ANSCHREIBEN_STRUCTURE.P, ANSCHREIBEN_STRUCTURE.TBLOCK, ANSCHREIBEN_STRUCTURE.PREFACE],
          path,
        ) ||
        isWithinTypeHierarchy(editor, [ANSCHREIBEN_STRUCTURE.DOC_AUTHORITY], path) ||
        isWithinTypeHierarchy(editor, [ANSCHREIBEN_STRUCTURE.DOC_DATE], path)
      );
    }
  }

  public static isEditableRefElement(editor: Editor, path?: Path, type?: string) {
    const isRefElement = type ? type === ANSCHREIBEN_STRUCTURE.REF : isOfType(editor, ANSCHREIBEN_STRUCTURE.REF);
    return (
      isRefElement && isWithinTypeHierarchy(editor, [ANSCHREIBEN_STRUCTURE.P, ANSCHREIBEN_STRUCTURE.MAIN_BODY], path)
    );
  }
}
