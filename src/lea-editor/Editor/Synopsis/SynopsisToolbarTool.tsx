// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, useRef, useCallback, useEffect } from 'react';
import './SynopsisToolbarTool.less';
import { ToolbarDropdown } from '@lea/ui';
import { useTranslation } from 'react-i18next';
import { checkIfCellsVisible, isEscape, isTab } from '../../plugin-core/utils';
import { useWizardVisibleDispatch } from '../../../components/Wizard/useWizardVisible';
import { useEditorInfoDispatch, useSynopsisInfo } from '../../plugin-core/editor-store/editor-info/useEditorInfo';
import {
  AenderungsvergleichCreateDTO,
  DocumentService,
  useFirstDocumentDispatch,
  useSynopsisDocumentsDispatch,
  // Document States für statischen Änderungsvergleich
  useFirstDocumentState,
  useSynopsisDocumentsState,
  CompoundDocumentService,
} from '../../../api';
import { Wizards } from '../../../components/Wizard/WizardVisibleState';
import { SaveConfirmation } from '../../../components/Toolbars/SaveConfirmation/SaveConfirmation';
import { ItemType } from 'antd/lib/menu/interface';
import { SynopsisMode, SynopsisType } from '../../../constants';
import { displayMessage } from '@plateg/theme';
import { useParams } from 'react-router-dom';
import { DocumentPageParams } from '../../../components/Pages/DocumentPage/DocumentPage';
import { useAppInfoDispatch, useAppInfoState } from '../../../components/App';

interface ISynopsisToolbarToolProps {
  onClose: () => void;
  onConfigure: () => void;
  canSave: boolean;
}

export const SyopsisToolbarTool = ({ onClose, onConfigure, canSave }: ISynopsisToolbarToolProps) => {
  const { removeDocument } = useSynopsisDocumentsDispatch();
  const { openWizard } = useWizardVisibleDispatch(Wizards.SYNOPSIS);
  const { isSynopsisVisible } = useSynopsisInfo();
  const { synopsisType } = useAppInfoState();
  const [isActive, onActive] = useState(false);
  // Dokumente für statischen Änderungsvergleich und für Änderungsbefehle
  const { document } = useFirstDocumentState();
  const { documents } = useSynopsisDocumentsState();
  const firstDocument = useFirstDocumentDispatch();
  const synopsisDocuments = useSynopsisDocumentsDispatch();
  const [aenderungsvergleichData, setAenderungsvergleichData] = useState<AenderungsvergleichCreateDTO>({
    base: '',
    versions: [],
  });
  const [aenderungsbefehlData, setAenderungsbefehlData] = useState<AenderungsvergleichCreateDTO>({
    base: '',
    versions: [],
  });
  const [openKeys, setOpenKeys] = useState<string[]>([]);
  const [aenderungsvergleichResponse, fetchAenderungsvergleich] =
    DocumentService.createAenderungsvergleich(aenderungsvergleichData);
  const [aenderungsbefehlResponse, fetchAenderungsbefehl] = DocumentService.createAenderungsbefehle(
    document?.proposition?.id ?? '',
    aenderungsbefehlData,
  );
  const [closeConfirmationOpened, setCloseConfirmationOpened] = useState(false);
  const [configureConfirmationOpened, setConfigureConfirmationOpened] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const isPopoverVisibleRef = useRef<boolean>(false);
  const closeSynopsisMainBtnRef = useRef<HTMLAnchorElement>(null);
  const { setSynopsisMode } = useEditorInfoDispatch();
  const { setSynopsisType } = useAppInfoDispatch();
  const { documentId } = useParams<DocumentPageParams>();
  const { fetchDocument } = useFirstDocumentDispatch();
  const [bestandsrechtListResponse, fetchBestandsrechtList] = CompoundDocumentService.getBestandsrecht(
    document?.compoundDocumentId ?? '',
  );
  const [isFirstDocumentBestandsrecht, setIsFirstDocumentBestandsrecht] = useState(false);

  const isCloseSynopsisDisabled = !isSynopsisVisible;

  const { t } = useTranslation();

  const handleCloseClick = () => {
    if (isCloseSynopsisDisabled || isPopoverVisibleRef.current) {
      return;
    }
    if (canSave && !closeConfirmationOpened) {
      isPopoverVisibleRef.current = true;
      setCloseConfirmationOpened(true);
    } else if (!canSave) {
      removeDocument();
      onActive(false);
    }
    setSynopsisType(SynopsisType.None);
  };

  const handleOpenWizard = useCallback(() => {
    openWizard();
    setTimeout(() => onActive(false), 10);
  }, [isSynopsisVisible]);

  const handleConfigureClick = (synopsisType: SynopsisType) => {
    if (isPopoverVisibleRef.current) return;
    setSynopsisType(synopsisType);
    if (isSynopsisVisible && canSave) {
      isPopoverVisibleRef.current = true;
      setConfigureConfirmationOpened(true);
    } else {
      handleOpenWizard();
    }
  };

  const setActive = useCallback(
    (active: boolean) => {
      if (isActive && isPopoverVisibleRef.current) return;
      !active && setOpenKeys([]);
      setTimeout(() => onActive(active), 10);
    },
    [isActive, closeConfirmationOpened, configureConfirmationOpened],
  );

  const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
    const event = e.nativeEvent;
    if (isTab(event) && !isPopoverVisibleRef.current) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (isEscape(event)) {
      setTimeout(() => buttonRef.current?.focus());
    }
  };

  // Funktion für Erstellen eines statischen Änderungsvergleichs

  const handleCreateAenderungsvergleich = () => {
    if (document && documents) {
      setSynopsisType(SynopsisType.Aenderungsvergleich);
      const aenderungsvergleichData: AenderungsvergleichCreateDTO = {
        base: document?.id ?? '',
        versions: documents.map((document) => document.id),
      };
      setAenderungsvergleichData(aenderungsvergleichData);
    }
  };

  const handleCreateAenderungsbefehl = () => {
    if (document && documents) {
      setSynopsisType(SynopsisType.Aenderungsbefehl);
      const aenderungsvergleichData: AenderungsvergleichCreateDTO = {
        base: document?.id ?? '',
        versions: documents.map((document) => document.id),
      };
      setAenderungsbefehlData(aenderungsvergleichData);
    }
  };

  useEffect(() => {
    if (document?.compoundDocumentId) {
      fetchBestandsrechtList();
    }
  }, []);

  useEffect(() => {
    if (!bestandsrechtListResponse.isLoading && bestandsrechtListResponse.data) {
      const isFirstDocumentBestandrecht = bestandsrechtListResponse.data.some(
        (bestandsrecht) => bestandsrecht.id === document?.id,
      );
      setIsFirstDocumentBestandsrecht(isFirstDocumentBestandrecht);
    }
  }, [bestandsrechtListResponse]);

  useEffect(() => {
    if (aenderungsvergleichData.base !== '') fetchAenderungsvergleich();
  }, [aenderungsvergleichData]);

  useEffect(() => {
    if (aenderungsbefehlData.base !== '') fetchAenderungsbefehl();
  }, [aenderungsbefehlData]);

  useEffect(() => {
    if (!aenderungsvergleichResponse.isLoading) {
      if (aenderungsvergleichResponse.hasError) {
        displayMessage(t('lea.messages.createAenderungsvergleich.error'), 'error');
      } else if (aenderungsvergleichResponse.data) {
        const data = aenderungsvergleichResponse.data;
        // extract the first document to overwrite it with new document
        const document = data.document;
        firstDocument.setDocument(document);
        removeDocument();
      }
    }
  }, [aenderungsvergleichResponse]);

  useEffect(() => {
    if (!aenderungsbefehlResponse.isLoading) {
      if (aenderungsbefehlResponse.hasError) {
        displayMessage(t('lea.messages.createAenderungsbefehl.error'), 'error');
      } else if (aenderungsbefehlResponse.data) {
        const document = aenderungsbefehlResponse.data;
        // remove all documents except first two
        documents.slice(2).forEach((document) => synopsisDocuments.removeDocument(document.id));
        // add aenderungsbefehle as third document
        synopsisDocuments.addDocument(document);
      }
    }
  }, [aenderungsbefehlResponse]);

  useEffect(() => {
    if (closeConfirmationOpened || configureConfirmationOpened) {
      isPopoverVisibleRef.current = true;
    } else {
      setTimeout(() => {
        isPopoverVisibleRef.current = false;
      }, 200);
    }
  }, [closeConfirmationOpened, configureConfirmationOpened]);

  const closeAenderungsvergleich = () => {
    if (isSynopsisVisible) return;
    setSynopsisType(SynopsisType.None);
    fetchDocument(documentId);
  };

  const getAenderungsvergleichTitle = () => {
    if (synopsisType !== SynopsisType.Aenderungsvergleich) {
      return t('lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichConfigManually');
    } else {
      return t('lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.closeAenderungsvergleich');
    }
  };

  const getItemTypes = (): ItemType[] => {
    return [
      {
        key: 'lea-parallel-view-submenu',
        label: t('lea.toolbar.labels.synopsis.parallelView'),
        title: t('lea.toolbar.labels.synopsis.parallelView'),
        disabled: synopsisType === SynopsisType.Aenderungsvergleich,
        popupClassName: 'lea-ui-toolbar-dropdown-submenu-children',
        children: [
          {
            label: (
              <SaveConfirmation
                confirmationOpened={closeConfirmationOpened}
                setConfirmationOpened={setCloseConfirmationOpened}
                onSave={onClose}
                type="closeSynopsis"
                mainBtnRef={closeSynopsisMainBtnRef}
              >
                <a
                  className="close-synopsis-button"
                  title={t('lea.toolbar.labels.synopsis.parallelViewSubmenu.closeSynopsis')}
                  onClick={handleCloseClick}
                  ref={closeSynopsisMainBtnRef}
                >
                  {t('lea.toolbar.labels.synopsis.parallelViewSubmenu.closeSynopsis')}
                </a>
              </SaveConfirmation>
            ),
            key: 'lea-close-synopsis-parallel-view',
            disabled: isCloseSynopsisDisabled,
          },
          {
            label: (
              <SaveConfirmation
                confirmationOpened={configureConfirmationOpened}
                setConfirmationOpened={setConfigureConfirmationOpened}
                onSave={() => onConfigure()}
                type="configureSynopsis"
              >
                {t('lea.toolbar.labels.synopsis.parallelViewSubmenu.configSynopsis')}
              </SaveConfirmation>
            ),
            title: t('lea.toolbar.labels.synopsis.parallelViewSubmenu.configSynopsis'),
            key: 'lea-config-synopsis-parallel-view',
            onClick: () => handleConfigureClick(SynopsisType.Synopsis),
          },
          { type: 'divider' },
          {
            key: 'lea-toolbar-labels-synopsis-generateAenderungsbefehl',
            label: t('lea.toolbar.labels.synopsis.parallelViewSubmenu.newSynopsis'),
            title: t('lea.toolbar.labels.synopsis.parallelViewSubmenu.newSynopsis'),
            onClick: () => handleConfigureClick(SynopsisType.NewSynopsis),
          },
        ],
      },
      { type: 'divider' },
      {
        key: 'lea-anderungsvergleich-submenu',
        label: t('lea.toolbar.labels.synopsis.aenderungsvergleich'),
        title: t('lea.toolbar.labels.synopsis.aenderungsvergleich'),
        // Submenü für Erstellung eines statischen Änderungsvergleichs

        popupClassName: 'lea-ui-toolbar-dropdown-submenu-children',
        children: [
          {
            key: 'lea-anderungsvergleich-mode-submenu',
            label: t('lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting.title'),
            title: t('llea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting'),
            popupClassName: 'lea-ui-toolbar-dropdown-submenu-children',
            disabled:
              !checkIfCellsVisible(isSynopsisVisible, document, documents) &&
              synopsisType !== SynopsisType.Aenderungsvergleich,
            children: [
              {
                key: 'lea-toolbar-labels-synopsis-hdrMode',
                label: t(
                  'lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting.hdrMode',
                ),
                title: t(
                  'lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting.hdrMode',
                ),
                onClick: () => {
                  synopsisType !== SynopsisType.Aenderungsvergleich && handleCreateAenderungsvergleich();
                  setSynopsisMode(SynopsisMode.Hdr);
                },
              },
              {
                label: t(
                  'lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting.showChanges',
                ),
                title: t(
                  'lea.toolbar.labels.synopsis.aenderungsvergleichSubmenu.aenderungsvergleichFromExisting.showChanges',
                ),
                key: 'lea-toolbar-labels-synopsis-changeMode',
                onClick: () => {
                  synopsisType !== SynopsisType.Aenderungsvergleich && handleCreateAenderungsvergleich();
                  setSynopsisMode(SynopsisMode.Changes);
                },
              },
            ],
          },
          {
            label: (
              <SaveConfirmation
                confirmationOpened={configureConfirmationOpened}
                setConfirmationOpened={setConfigureConfirmationOpened}
                onSave={() => onConfigure()}
                type="configureSynopsis"
              >
                {getAenderungsvergleichTitle()}
              </SaveConfirmation>
            ),
            title: getAenderungsvergleichTitle(),
            key: 'lea-config-aenderungsvergleich-manually',
            onClick: () => {
              if (synopsisType === SynopsisType.Aenderungsvergleich) {
                closeAenderungsvergleich();
              } else {
                handleConfigureClick(SynopsisType.Aenderungsvergleich);
              }
            },
          },
        ],
      },
      { type: 'divider' },
      {
        key: 'lea-new-synopsis-parallel-view',
        label: t('lea.toolbar.labels.synopsis.generateAenderungsbefehl'),
        title: t('lea.toolbar.labels.synopsis.generateAenderungsbefehl'),
        disabled: !checkIfCellsVisible(isSynopsisVisible, document, documents) || !isFirstDocumentBestandsrecht,
        onClick: () => {
          handleCreateAenderungsbefehl();
        },
      },
    ];
  };

  return (
    <>
      <ToolbarDropdown
        id="synopsisToolbar-dropdown"
        buttonRef={buttonRef}
        items={getItemTypes()}
        isActive={isActive}
        onActive={setActive as React.Dispatch<React.SetStateAction<boolean>>}
        header="Synopse"
        keyDownHandler={keyDownHandler}
        arrowIconVisible
        openKeys={openKeys}
        setOpenKeys={(keys) => setOpenKeys(keys)}
      ></ToolbarDropdown>
    </>
  );
};
