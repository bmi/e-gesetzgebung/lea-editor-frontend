// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CommentMarker, Element } from '../../../../interfaces';
import { ReactEditor } from 'slate-react';
import { CommentResponseDTO } from '../../../../api';
import { STRUCTURE_ELEMENTS } from '../../../plugin-core/utils/StructureElements';
import { getNodesOfTypeInRange } from '../../../plugin-core/utils';

export const focusCommentInEditor = (baseEditor: ReactEditor, comment: CommentResponseDTO) => {
  const [focusNode] = baseEditor.nodes({
    at: [0],
    match: (n) =>
      (n as CommentMarker)['lea:commentId'] === comment.commentId &&
      (n as CommentMarker)['lea:commentPosition'] === 'start',
  });
  if (focusNode) {
    const [, focusNodePath] = focusNode;
    baseEditor.select(focusNodePath);
  }
};

export const sortComments = (comments: CommentResponseDTO[], baseEditor: ReactEditor) => {
  if (comments.length > 0) {
    const sortedComments = [...comments];
    const [...commentMarker] = baseEditor.nodes({
      at: [0],
      match: (n) => (n as CommentMarker)['lea:commentPosition'] === 'start',
    });
    const sortedCommentIds = commentMarker.map((cm) => {
      const [marker] = cm;
      return (marker as CommentMarker)['lea:commentId'];
    });

    sortedComments.sort((a, b) => sortedCommentIds.indexOf(a.commentId) - sortedCommentIds.indexOf(b.commentId));
    return sortedComments;
  }
  return comments;
};

export const handleSetActiveAndColorForNodesInCommentRange = (
  commentId: string,
  isActive: boolean,
  isDeleted: boolean,
  baseEditor: ReactEditor,
  filteredComments: CommentResponseDTO[],
) => {
  if (!baseEditor) return;
  const [...commentMarkers] = baseEditor.nodes({
    at: [0],
    match: (n) =>
      (n as CommentMarker).type === STRUCTURE_ELEMENTS.COMMENTMARKER &&
      (n as CommentMarker)['lea:commentId'] === commentId,
  });
  if (commentMarkers.length === 0) {
    return;
  }

  const nodesInRange = getNodesOfTypeInRange(
    baseEditor,
    [STRUCTURE_ELEMENTS.TEXTWRAPPER, STRUCTURE_ELEMENTS.CHANGEWRAPPER, STRUCTURE_ELEMENTS.NUM],
    [commentMarkers[0]?.[1], commentMarkers[1]?.[1]],
  );
  const startMarker = commentMarkers[0][0] as CommentMarker;
  for (const [nodeInRange, nodeInRangePath] of nodesInRange) {
    let active = isActive;
    let hidden = startMarker.hidden;
    active = checkRangeElementsActive(active, nodeInRange, filteredComments);
    hidden = checkRangeElementsHidden(filteredComments, nodeInRange, baseEditor, hidden);
    baseEditor.setNodes(
      {
        'lea:isActive': active,
        'lea:hidden': hidden,
        'lea:commentColor': startMarker.color,
        'lea:commentsOnElement': setCommentIdsOnElement(commentId, nodeInRange['lea:commentsOnElement']),
      } as Partial<Element>,
      { at: nodeInRangePath },
    );
  }
  if (isDeleted) {
    commentMarkers.reverse();
    commentMarkers.forEach((marker) => {
      const [parent, parentPath] = baseEditor.parent(marker[1]);
      if (parent.children.length === 1) {
        baseEditor.removeNodes({ at: parentPath });
        baseEditor.mergeNodes({ at: parentPath, mode: 'lowest' });
      } else {
        baseEditor.removeNodes({ at: marker[1] });
      }
    });
  }
};

const setCommentIdsOnElement = (commentId: string, commentsOnElement?: string[]): string[] => {
  let commentIds = commentsOnElement ?? [];
  if (commentIds.length > 0) {
    if (!commentIds.includes(commentId)) {
      commentIds = [...commentIds, commentId];
    }
  } else {
    commentIds = [commentId];
  }
  return commentIds;
};

const checkRangeElementsActive = (active: boolean, nodeInRange: Element, filteredComments: CommentResponseDTO[]) => {
  if (
    !active &&
    filteredComments &&
    nodeInRange['lea:commentsOnElement'] &&
    nodeInRange['lea:commentsOnElement'].length > 0
  ) {
    const activeComments = filteredComments.filter((comment) => comment.isActive);
    const match = activeComments.filter((comment) => nodeInRange['lea:commentsOnElement']?.includes(comment.commentId));
    return match ? match.length > 0 : false;
  }
  return active;
};

const checkRangeElementsHidden = (
  filteredComments: CommentResponseDTO[],
  nodeInRange: Element,
  baseEditor: ReactEditor,
  hidden?: boolean,
) => {
  if (
    hidden &&
    filteredComments &&
    nodeInRange['lea:commentsOnElement'] &&
    nodeInRange['lea:commentsOnElement'].length > 0
  ) {
    const commentsInRange = filteredComments.filter((comment) =>
      nodeInRange['lea:commentsOnElement']?.includes(comment.commentId),
    );
    for (const commentInRange of commentsInRange) {
      const [marker] = baseEditor.nodes({
        at: [0],
        match: (n) =>
          (n as CommentMarker).type === STRUCTURE_ELEMENTS.COMMENTMARKER &&
          (n as CommentMarker)['lea:commentId'] === commentInRange.commentId &&
          (n as CommentMarker)['lea:commentPosition'] === 'start',
      });
      if (marker) {
        const [node] = marker;
        if (!(node as CommentMarker).hidden) {
          hidden = false;
        }
      }
    }
  }
  return hidden;
};
