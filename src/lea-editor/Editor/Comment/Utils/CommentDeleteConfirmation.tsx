// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { displayMessage, ExclamationCircleFilled } from '@plateg/theme';
import { Button, Popover } from 'antd';
import FocusTrap from 'focus-trap-react';
import React, { FunctionComponent, useEffect, useState } from 'react';
import Text from 'antd/lib/typography/Text';
import { CommentWithRef } from '../../../../interfaces';
import { useTranslation } from 'react-i18next';
import { CommentService } from '../../../../api/Comment';
import { ApiReturnType, useFirstDocumentState } from '../../../../api';
import { useCommentsChange } from '../../../plugins/comment';

interface CommentDeleteConfirmationProps {
  confirmationOpened: boolean;
  isAnswer: boolean;
  setDeleteCommentAction: React.Dispatch<React.SetStateAction<CommentWithRef | null>>;
  setIsAnswer: React.Dispatch<React.SetStateAction<boolean>>;
  setConfirmationOpened: React.Dispatch<React.SetStateAction<boolean>>;
  comment: CommentWithRef;
  replyId: string;
}

interface ICommentReply {
  documentId: string;
  commentId: string;
  replyId: string;
}

export const CommentDeleteConfirmation: FunctionComponent<CommentDeleteConfirmationProps> = ({
  confirmationOpened,
  isAnswer,
  setDeleteCommentAction,
  setIsAnswer,
  setConfirmationOpened,
  comment,
  replyId,
}) => {
  const { t } = useTranslation();
  const { document } = useFirstDocumentState();
  const { fetchComments } = useCommentsChange();
  const [trapped, setTrapped] = useState(false);
  const [deleteCommentReplyAction, setDeleteCommentReplyAction] = useState<ICommentReply | null>(null);
  const [deleteCommentReplyStateResponse, deleteCommentReplyStateRequest]: ApiReturnType<unknown> =
    CommentService.deleteReply(
      deleteCommentReplyAction?.documentId || '',
      deleteCommentReplyAction?.commentId || '',
      deleteCommentReplyAction?.replyId || '',
    );

  useEffect(() => {
    if (!confirmationOpened) {
      setTrapped(confirmationOpened);
    }
  }, [confirmationOpened]);

  useEffect(() => {
    if (!deleteCommentReplyAction) return;
    deleteCommentReplyStateRequest();
  }, [deleteCommentReplyAction]);
  useEffect(() => {
    if (deleteCommentReplyStateResponse.isLoading) return;
    if (deleteCommentReplyStateResponse.hasError) {
      switch (deleteCommentReplyStateResponse.status?.statusCode) {
        case 403:
          displayMessage(t(`lea.messages.comments.delete.deleteReply.rightsError`), 'error');
          break;
        default:
          displayMessage(t(`lea.messages.comments.delete.deleteReply.defaultError`), 'error');
          break;
      }
    } else {
      displayMessage(t(`lea.messages.comments.delete.deleteReply.success`), 'success');
      fetchComments(document?.id || '');
    }
    setDeleteCommentReplyAction(null);
  }, [deleteCommentReplyStateResponse]);

  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <Text className="ant-popover-message-title">
        {t(`lea.messages.comments.delete.deleteConfirmation.${!isAnswer ? 'deleteComment' : 'deleteAnswer'}`)}
      </Text>
    </>
  );

  const handleDelete = () => {
    if (isAnswer) {
      setDeleteCommentReplyAction({ documentId: comment.documentId, commentId: comment.commentId, replyId });
    } else {
      setDeleteCommentAction(comment);
    }
    setConfirmationOpened(false);
    setIsAnswer(false);
  };

  const handleCancle = () => {
    setConfirmationOpened(false);
    setIsAnswer(false);
  };

  const popoverContent = (
    <FocusTrap active={!confirmationOpened ? confirmationOpened : trapped} autoFocus>
      <div style={{ maxWidth: '300px' }}>
        <p style={{ lineHeight: '20px', fontSize: '16px' }}>
          {t(
            `lea.messages.comments.delete.deleteConfirmation.${!isAnswer ? 'warningCommentText' : 'warningAnswerText'}`,
          )}
        </p>
        <div className="ant-popover-buttons">
          <Button id="comment-handleDelete-btn" onClick={handleDelete} type="primary">
            {t('lea.comments.actions.deleteComment')}
          </Button>
          <Button id="comment-abortDelete-btn" onClick={handleCancle} type="default">
            {t('lea.comments.actions.cancelDelete')}
          </Button>
        </div>
      </div>
    </FocusTrap>
  );
  return (
    <Popover
      title={popoverTitle}
      open={confirmationOpened ? confirmationOpened : trapped}
      placement="topLeft"
      content={popoverContent}
      afterOpenChange={(visible) => {
        visible && setTrapped(visible);
      }}
    />
  );
};
