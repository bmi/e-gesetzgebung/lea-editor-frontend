// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { BaseElement, Text } from 'slate';

export const Serialize = (nodes: BaseElement[], id: string) => {
  return (
    <>
      {nodes.map((node, i) => {
        if (Text.isText(node)) {
          return node.text;
        } else {
          const children = node.children as BaseElement[];
          const serializedChildren = Serialize(children, id);
          return <div key={`serial-${id}-${i}`}>{serializedChildren}</div>;
        }
      })}
    </>
  );
};
