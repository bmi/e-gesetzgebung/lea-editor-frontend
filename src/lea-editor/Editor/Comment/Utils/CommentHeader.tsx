// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import React from 'react';
import { format } from 'date-fns';
interface CommentHeaderProps {
  name: string;
  updated: string;
  items: DropdownMenuItem[];
  id: string;
  showDate: boolean;
}
export const CommentHeader = ({ name, updated, items, id, showDate }: CommentHeaderProps) => {
  return (
    <>
      <div className="commentHeader">
        <div role="heading" className="commentName">
          {name}
        </div>
        <div className="comment-item-dropdown" onClick={(e) => e.stopPropagation()}>
          <DropdownMenu items={items} elementId={id} overlayClass="dropdown-menu" />
        </div>
      </div>
      {showDate && (
        <div className="comment-updated">{updated ? format(new Date(updated), 'dd.MM.yyyy HH:mm') : ''}</div>
      )}
    </>
  );
};
