// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useCallback, useEffect, useMemo, useReducer, useRef, useState } from 'react';
import './CommentPage.scss';
import { CommentItem } from '.';
import { BaseElement, BaseSelection, createEditor, Descendant, Transforms } from 'slate';
import { ApiRequest, ApiResponse, useFirstDocumentState, updateFirstDocument } from '../../../api';
import { CommentResponseDTO, CommentService, ReplyDTO } from '../../../api/Comment';
import { displayMessage } from '@plateg/theme';
import { useTranslation } from 'react-i18next';
import { CommentWithRef, IEditComment, ReplyWithMetadata } from '../../../interfaces/Comment';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import * as _ from 'lodash';
import { ColorCodes } from '../../../constants';
import { Serialize, handleSetActiveAndColorForNodesInCommentRange, sortComments } from './Utils';
import { EditComment } from './EditComment';
import { ReactEditor, withReact } from 'slate-react';
import { useCommentPageChange, useCommentPageState } from '../../plugin-core/editor-store/workspace/comment-page';
import { useCommentsChange, useCommentsState } from '../../plugins/comment';
import { CommentMarker, Element } from '../../../interfaces';
import { STRUCTURE_ELEMENTS } from '../../plugin-core/utils/StructureElements';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { useSelectionState } from '../../plugin-core';

interface ICommentPageProps {
  baseEditor: ReactEditor;
  saveSelection: React.MutableRefObject<BaseSelection>;
}

export const CommentPage: FunctionComponent<ICommentPageProps> = ({ baseEditor, saveSelection }) => {
  const { document } = useFirstDocumentState();
  const { visible } = useCommentPageState();
  const { openCommentPage } = useCommentPageChange();
  const dispatch = useAppDispatch();
  const user = useAppSelector((selector) => selector.user.user);
  const { fetchComments, removeNewComment, setFilteredComments, updateComment } = useCommentsChange();
  const { newComment, comments, filteredComments } = useCommentsState();
  const editorRef = useRef<ReactEditor>();
  if (!editorRef.current) editorRef.current = withReact(createEditor() as ReactEditor);
  const editor = editorRef.current;
  const initValue = useRef<Descendant[]>([{ children: [{ text: '' }] }]);
  const [value, setValue] = useState<Descendant[] | null>(null);
  const [isEditing, setIsEditing] = useState<IEditComment | null>(null);
  const [isAnswering, setIsAnswering] = useState<string>('');

  const commentPageRef = useRef<HTMLDivElement>(null);
  const [, forceUpdate] = useReducer((x: number) => x + 1, 0);

  const [changeCommentStateAction, setChangeCommentStateAction] = useState<CommentWithRef | null>(null);
  const [deleteCommentAction, setDeleteCommentAction] = useState<CommentWithRef | null>(null);
  const defaultColor = ColorCodes[0];
  const { selection } = useSelectionState();

  const [addCommentResponse, addComment] = CommentService.create(
    document?.id || '',
    newComment && {
      ...newComment,
      content: JSON.stringify(value),
    },
  );

  const [updateCommentStateResponse, setCommentStateApiResponse] = useState<ApiResponse<CommentResponseDTO>>({
    isLoading: true,
    hasError: false,
  });
  const updateCommentStateRequest: ApiRequest = changeCommentStateAction
    ? CommentService.changeState(
        changeCommentStateAction.documentId,
        changeCommentStateAction.commentId,
        {
          status: changeCommentStateAction.state === 'OPEN' ? 'CLOSED' : 'OPEN',
        },
        setCommentStateApiResponse,
      )
    : CommentService.changeState(
        '',
        '',
        {
          status: 'OPEN',
        },
        setCommentStateApiResponse,
      );

  const [deleteCommentStateResponse, deleteCommentStateRequest] = CommentService.deleteComment(
    deleteCommentAction?.documentId || '',
    deleteCommentAction ? [deleteCommentAction.commentId] : [''],
    { content: JSON.stringify(baseEditor.children) },
  );

  const { t } = useTranslation();

  useEffect(() => {
    if (newComment) {
      setTimeout(() => {
        forceUpdate();
      }, 100);
    }
  }, [newComment]);

  useEffect(() => {
    setCommentColor();
    setFilteredComments(comments.filter((comment) => !comment.deleted));
    if (saveSelection.current) {
      baseEditor.selection = saveSelection.current;
      ReactEditor.focus(baseEditor);
    }
  }, [comments]);

  const getRepliesInOrder = (replyParent: ReplyDTO, comment: CommentWithRef): ReplyDTO[] => {
    const child = comment.replies.find((reply) => reply.parentId === replyParent.replyId);
    if (child) {
      return [replyParent, ...getRepliesInOrder(child, comment)];
    }
    return [replyParent];
  };

  const checkForMarker = () => {
    if (comments.length > 0) {
      const [...commentMarker] = baseEditor.nodes<CommentMarker>({
        at: [0],
        match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.COMMENTMARKER,
      });
      for (const [marker, markerPath] of commentMarker) {
        const isNotFiltered = filteredComments.find((comment) => comment.commentId === marker['lea:commentId']);
        if (isNotFiltered) {
          baseEditor.setNodes({ hidden: false } as Partial<CommentMarker>, { at: markerPath });
        } else {
          baseEditor.setNodes({ hidden: true } as Partial<CommentMarker>, { at: markerPath });
        }
      }
    }
  };

  const serializedComments = useMemo(() => {
    // Wird gesetzt um ein gleichzeitiges Rendern verschiedener Komponenten zu unterbinden
    setTimeout(() => {
      checkForMarker();
      comments.forEach((comment) =>
        handleSetActiveAndColorForNodesInCommentRange(
          comment.commentId,
          comment.isActive ?? false,
          comment.deleted,
          baseEditor,
          filteredComments,
        ),
      );
    });
    return sortComments(filteredComments, baseEditor).map<CommentResponseDTO>((comment) => {
      const extendedComment: CommentResponseDTO = { ...comment };
      const repliesWithSerializedContent: ReplyWithMetadata[] = [];
      const rootReply = extendedComment.replies?.find((reply) => reply.parentId === comment.commentId);
      if (rootReply) {
        const orderdReplies = getRepliesInOrder(rootReply, comment);
        orderdReplies.forEach((reply) => {
          repliesWithSerializedContent.push({
            ...reply,
            serializedContent: Serialize(JSON.parse(reply.content) as BaseElement[], reply.replyId),
          });
        });
      }
      extendedComment.serializedContent = Serialize(JSON.parse(comment.content) as BaseElement[], comment.commentId);
      extendedComment.organizedReplies = repliesWithSerializedContent;
      return extendedComment;
    });
  }, [filteredComments, comments]);

  useEffect(() => {
    if (isAnswering === '' && !isEditing && !newComment) {
      editor.selection = null;
      editor.children = [];
      setValue(null);
    }
  }, [isAnswering, isEditing, newComment]);

  useEffect(() => {
    if (isAnswering !== '') {
      isEditing && setIsEditing(null);
      removeNewComment();
      editor.children = initValue.current;
      setValue(initValue.current);
    }
  }, [isAnswering]);

  useEffect(() => {
    if (isEditing) {
      isAnswering !== '' && setIsAnswering('');
      removeNewComment();
      editor.children = JSON.parse(isEditing.content) as Descendant[];
      setValue(JSON.parse(isEditing.content) as Descendant[]);
    }
  }, [isEditing]);

  useEffect(() => {
    if (newComment) {
      isAnswering !== '' && setIsAnswering('');
      isEditing && setIsEditing(null);
      editor.children = initValue.current;
      setValue(initValue.current);
      openCommentPage();
    }
  }, [newComment]);

  useEffect(() => {
    if (addCommentResponse.isLoading) return;
    if (addCommentResponse.hasError) {
      switch (updateCommentStateResponse.status?.statusCode) {
        case 403:
          displayMessage(t('lea.messages.comments.save.rightsError'), 'error');
          break;
        default:
          displayMessage(t('lea.messages.comments.save.defaultError'), 'error');
          break;
      }
    } else if (addCommentResponse.data) {
      removeNewComment();
      const document = addCommentResponse.data;
      fetchComments(document?.id || '');
      dispatch(updateFirstDocument(document));
      setValue(initValue.current);
      displayMessage(t('lea.messages.comments.save.success'), 'success');
    }
  }, [addCommentResponse]);

  const setCommentColor = () => {
    const groupedCommentsByAuthor = _.groupBy(comments, 'createdBy.name');
    Object.keys(groupedCommentsByAuthor).forEach((key, index) => {
      const comments = groupedCommentsByAuthor[key];
      for (const comment of comments) {
        !comment.colorCode && updateComment({ commentId: comment.commentId, colorCode: ColorCodes[index] });
        if (baseEditor) {
          const [...nodes] = baseEditor.nodes({
            at: [0],
            match: (n) => (n as CommentMarker)['lea:commentId'] === comment.commentId,
          });
          for (const [, nodePath] of nodes) {
            Transforms.setNodes(baseEditor, { color: ColorCodes[index] } as Partial<CommentMarker>, {
              at: nodePath,
            });
          }
        }
      }
    });
  };

  const onChangeHandler = useCallback((newValue: Descendant[]) => {
    setValue(newValue);
  }, []);

  const handleAddComment = () => {
    if (!selection) return;
    // eslint-disable-next-line prefer-const
    let { path, offset } = baseEditor.start(selection);
    const newPath = [...path];
    if (JSON.stringify(path) !== JSON.stringify(selection.anchor.path)) {
      const wrapperIndex = path.length - 2;
      const newSelectionIndexValue = path[wrapperIndex] + 2;
      newPath[wrapperIndex] = newSelectionIndexValue;
      offset = 0;
    } else if (selection.anchor.offset !== offset) {
      const wrapperIndex = path.length - 2;
      const newSelectionIndexValue = path[wrapperIndex] + 4;
      newPath[wrapperIndex] = newSelectionIndexValue;
      offset = 0;
    } else if (offset === 0) {
      const wrapperIndex = path.length - 2;
      const newSelectionIndexValue = path[wrapperIndex] + 2;
      newPath[wrapperIndex] = newSelectionIndexValue;
    }
    saveSelection.current = {
      anchor: { path: newPath, offset },
      focus: { path: newPath, offset },
    };

    addComment();
  };

  const handleCancleAddComment = () => {
    removeNewComment();
    setValue(initValue.current);
  };

  useEffect(() => {
    if (!changeCommentStateAction) return;
    updateCommentStateRequest();
  }, [changeCommentStateAction]);

  useEffect(() => {
    if (!deleteCommentAction) return;
    deleteCommentStateRequest();
  }, [deleteCommentAction]);

  useEffect(() => {
    if (updateCommentStateResponse.isLoading) return;
    if (updateCommentStateResponse.hasError) {
      switch (updateCommentStateResponse.status?.statusCode) {
        case 403:
          displayMessage(t('lea.messages.comments.save.rightsError'), 'error');
          break;
        default:
          displayMessage(t('lea.messages.comments.save.defaultError'), 'error');
          break;
      }
    } else {
      displayMessage(
        t('lea.messages.comments.state.success', {
          state: changeCommentStateAction?.state === 'OPEN' ? 'erledigt' : 'offen',
        }),
        'success',
      );
      fetchComments(document?.id || '');
    }
    setCommentStateApiResponse({
      isLoading: true,
      hasError: false,
    });
    setChangeCommentStateAction(null);
  }, [updateCommentStateResponse]);

  useEffect(() => {
    if (deleteCommentStateResponse.isLoading) return;
    if (deleteCommentStateResponse.hasError) {
      switch (deleteCommentStateResponse.status?.statusCode) {
        case 403:
          displayMessage(
            t(
              `lea.messages.comments.delete.${
                deleteCommentAction?.replies && deleteCommentAction.replies.length > 0
                  ? 'deleteThread'
                  : 'deleteComment'
              }.rightsError`,
            ),
            'error',
          );
          break;
        default:
          displayMessage(
            t(
              `lea.messages.comments.delete.${
                deleteCommentAction?.replies && deleteCommentAction.replies.length > 0
                  ? 'deleteThread'
                  : 'deleteComment'
              }.defaultError`,
            ),
            'error',
          );
          break;
      }
    } else {
      displayMessage(
        t(
          `lea.messages.comments.delete.${
            deleteCommentAction?.replies && deleteCommentAction.replies.length > 0 ? 'deleteThread' : 'deleteComment'
          }.success`,
        ),
        'success',
      );
      if (deleteCommentStateResponse.data) {
        const document = deleteCommentStateResponse.data;
        dispatch(updateFirstDocument(document));
      }
      fetchComments(document?.id || '');
    }
    setDeleteCommentAction(null);
  }, [deleteCommentStateResponse]);

  const getActionItems = (comment: CommentResponseDTO): DropdownMenuItem[] => {
    const items: DropdownMenuItem[] = [];
    const stateActionType = comment.state === 'OPEN' ? 'Closed' : 'Opened';

    items.push(
      ...[
        {
          element: t(
            `lea.comments.actions.${
              (comment.replies && comment.replies.length > 0 ? 'setThread' : 'set') + stateActionType
            }`,
          ),
          onClick: () => {
            setChangeCommentStateAction(comment);
          },
        },
      ],
    );
    return items;
  };

  return visible ? (
    <div className="comment-page" id="commentPage" ref={commentPageRef}>
      {serializedComments.map((comment) => {
        return (
          <CommentItem
            key={comment.commentId}
            items={getActionItems(comment)}
            color={comment.colorCode || defaultColor}
            comment={comment}
            setIsEditing={setIsEditing}
            isEditing={isEditing}
            isAnswering={isAnswering}
            setIsAnswering={setIsAnswering}
            setValue={setValue}
            editor={editor}
            baseEditor={baseEditor}
            value={value}
            setDeleteCommentAction={setDeleteCommentAction}
          />
        );
      })}
      {newComment && value && (
        <div className="create-comment-wrapper">
          <EditComment
            editor={editor}
            value={value}
            header={t('lea.comments.newComment.addCommentHeader')}
            userName={user?.dto.name ?? ''}
            onChangeHandler={onChangeHandler}
            handleCancleAddComment={handleCancleAddComment}
            handleAddComment={handleAddComment}
          />
        </div>
      )}
    </div>
  ) : (
    <></>
  );
};
