// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { Descendant } from 'slate';
import { Editable, ReactEditor, Slate } from 'slate-react';
import { useTranslation } from 'react-i18next';
import './EditComment.less';
import { isChromiumGreaterThan129 } from '../../plugin-core/utils';

interface EditCommentProps {
  editor: ReactEditor;
  value: Descendant[];
  header?: string;
  userName?: string;
  onChangeHandler: (newValue: Descendant[]) => void;
  handleCancleAddComment: () => void;
  handleAddComment: () => void;
}

export const EditComment = ({
  editor,
  value,
  header,
  userName,
  onChangeHandler,
  handleCancleAddComment,
  handleAddComment,
}: EditCommentProps) => {
  const { t } = useTranslation();

  const handleKeyDown = (e: any) => {
    // Diese Funktion wird ausgebaut sobald weitere Features (z.B. Listen) hinzukommen
  };

  return (
    <div className="edit-comment-wrapper">
      <h1>{userName}</h1>
      <h1>{header}</h1>
      <Slate editor={editor} initialValue={value} onChange={onChangeHandler}>
        <Editable
          maxLength={1000}
          id="create-comment-textbox"
          className="editable"
          onKeyDown={(e) => {
            handleKeyDown(e);
          }}
          onKeyPress={(e) => {
            handleKeyDown(e);
          }}
          onBlur={(e) => {
            if (isChromiumGreaterThan129) {
              e.stopPropagation();
            }
          }}
        />
      </Slate>
      <div className="comment-button-wrapper">
        <Button id="lea-cancle-create-comment" size="small" onClick={handleCancleAddComment}>
          {t('lea.comments.newComment.cancleAddCommentButton')}
        </Button>
        <Button id="lea-create-comment" size="small" type="primary" onClick={handleAddComment}>
          {t('lea.comments.newComment.addCommentButton')}
        </Button>
      </div>
    </div>
  );
};
