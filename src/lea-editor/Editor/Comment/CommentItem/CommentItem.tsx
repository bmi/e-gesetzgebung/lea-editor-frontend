// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react';
import { Button, Collapse, Divider, Typography } from 'antd';
import { displayMessage } from '@plateg/theme';
const { Panel } = Collapse;
const { Paragraph } = Typography;

import './CommentItem.less';

import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { CommentService } from '../../../../api/Comment';
import { EditComment } from '../EditComment';
import { useTranslation } from 'react-i18next';
import { BaseElement, BaseText, Descendant } from 'slate';
import { useFirstDocumentState } from '../../../../api';
import { CommentWithRef, IEditComment } from '../../../../interfaces';
import { ReactEditor } from 'slate-react';
import { CommentHeader, focusCommentInEditor } from '../Utils';
import { CommentDeleteConfirmation } from '../Utils/CommentDeleteConfirmation';
import { useCommentsChange } from '../../../plugins/comment';
import { useAppSelector } from '@plateg/theme/src/components/store';

interface CommentItemProps {
  editor: ReactEditor;
  baseEditor: ReactEditor;
  value: Descendant[] | null;
  comment: CommentWithRef;
  items: DropdownMenuItem[];
  color: string;
  isEditing: IEditComment | null;
  setIsAnswering: React.Dispatch<React.SetStateAction<string>>;
  isAnswering: string;
  setIsEditing: React.Dispatch<React.SetStateAction<IEditComment | null>>;
  setValue: React.Dispatch<React.SetStateAction<Descendant[] | null>>;
  children?: React.ReactNode;
  setDeleteCommentAction: React.Dispatch<React.SetStateAction<CommentWithRef | null>>;
}

export const CommentItem: FunctionComponent<CommentItemProps> = ({
  isEditing,
  setIsEditing,
  isAnswering,
  setIsAnswering,
  value,
  editor,
  baseEditor,
  setValue,
  comment,
  items,
  color,
  setDeleteCommentAction,
}) => {
  const { t } = useTranslation();
  const { document } = useFirstDocumentState();
  const { fetchComments, updateComment } = useCommentsChange();
  const user = useAppSelector((selector) => selector.user.user);
  const [commentActive, setCommentActive] = useState(comment.isActive);
  const [commentContent, setCommentContent] = useState(comment.content);
  const [panelShown, setPanelShown] = useState(comment.isActive);
  const [isLoading, setIsLoading] = useState(false);
  const [expanded, setExpanded] = useState(false);
  const [confirmationOpened, setConfirmationOpened] = useState(false);
  const [isAnswer, setIsAnswer] = useState(false);
  const [replyId, setReplyId] = useState('');
  const defaultRows: number = 4;

  const [addReplyResponse, addReply] = CommentService.createReply(document?.id || '', comment.commentId, {
    parentId:
      comment.organizedReplies && comment.organizedReplies.length > 0
        ? comment.organizedReplies[comment.organizedReplies.length - 1].replyId
        : comment.commentId,
    content: JSON.stringify(value),
  });

  const [saveEditResponse, saveEdit] = isEditing?.replyId
    ? CommentService.updateReply(document?.id || '', comment.commentId, isEditing?.replyId, {
        parentId: comment.replies.find((reply) => reply.replyId === isEditing.replyId)?.parentId || '',
        content: JSON.stringify(value),
      })
    : CommentService.update(document?.id || '', comment.commentId, { content: JSON.stringify(value) });

  const handleCollapseChange = (key: string | string[], commentId: string) => {
    const isActive = key[0] === '';
    updateComment({ commentId: commentId, isActive: isActive });
    setCommentActive(isActive);
    if (isActive) {
      setTimeout(() => {
        setPanelShown(isActive);
      });
    } else {
      setPanelShown(isActive);
    }
  };

  useEffect(() => {
    setCommentActive(comment.isActive);
    setPanelShown(comment.isActive);
  }, [comment.isActive]);

  const handleCancleAnswere = () => {
    setIsAnswering('');
  };

  const handleCancleEdit = () => {
    setIsEditing(null);
  };
  const handleSaveEdit = () => {
    if (!isLoading) {
      saveEdit();
      setIsLoading(true);
    }
  };

  const onChangeHandler = useCallback((newValue: Descendant[]) => {
    setValue(newValue);
  }, []);

  const handleAddReply = () => {
    if (!isLoading) {
      addReply();
      setIsLoading(true);
    }
  };

  useEffect(() => {
    setCommentContent(
      (JSON.parse(comment.content) as BaseElement[])
        .map((children) => {
          return (children.children[0] as BaseText).text;
        })
        .join(),
    );
  }, [comment.serializedContent]);

  useEffect(() => {
    if (addReplyResponse.isLoading) return;
    setIsLoading(false);
    if (addReplyResponse.hasError) {
      displayMessage(t('lea.messages.comments.reply.saveError'), 'error');
    } else {
      handleCancleAnswere();
      displayMessage(t('lea.messages.comments.reply.saveSuccess'), 'success');
      fetchComments(document?.id || '');
    }
  }, [addReplyResponse]);

  useEffect(() => {
    if (saveEditResponse.isLoading) return;
    setIsLoading(false);
    if (saveEditResponse.hasError) {
      switch (saveEditResponse.status?.statusCode) {
        case 403:
          displayMessage(t('lea.messages.comments.save.rightsError'), 'error');
          break;
        default:
          displayMessage(t('lea.messages.comments.save.defaultError'), 'error');
          break;
      }
    } else {
      handleCancleEdit();
      displayMessage(t('lea.messages.comments.save.success'), 'success');
      fetchComments(document?.id || '');
    }
  }, [saveEditResponse]);

  const getReplyActionItems = (id: string, content: string, userMail: string): DropdownMenuItem[] => {
    const replyItems: DropdownMenuItem[] = [];

    replyItems.unshift(
      {
        element: t('lea.comments.actions.edit'),
        disabled: () => userMail !== user?.dto.email,
        onClick: () => {
          setIsAnswering('');
          setIsEditing({ replyId: id, content });
          if (!commentActive) {
            setCommentStateActive();
          }
        },
      },
      {
        element: t('lea.comments.actions.deleteAnswer'),
        disabled: () => userMail !== user?.dto.email,
        onClick: () => {
          setIsAnswer(true);
          setReplyId(id);
          setConfirmationOpened(true);
        },
      },
    );
    return replyItems;
  };

  const setCommentStateActive = () => {
    setCommentActive(true);
    setPanelShown(true);
    updateComment({ commentId: comment.commentId, isActive: true });
  };

  const getCommentActionItem = useMemo(() => {
    const commentItems: DropdownMenuItem[] = [...items];

    commentItems.unshift(
      {
        element: t('lea.comments.actions.open'),
        onClick: () => {
          if (!commentActive) {
            setCommentStateActive();
          }
        },
      },
      {
        element: t('lea.comments.actions.edit'),
        disabled: () => comment.createdBy.email !== user?.dto.email,
        onClick: () => {
          setIsAnswering('');
          if (!commentActive) {
            setCommentStateActive();
          }
          setIsEditing({ id: comment.commentId, content: comment.content });
        },
      },
      {
        element: t(
          `lea.comments.actions.${comment.replies && comment.replies.length > 0 ? 'deleteThread' : 'deleteComment'}`,
        ),
        disabled: () => comment.createdBy.email !== user?.dto.email,
        onClick: () => {
          setConfirmationOpened(true);
        },
      },
    );
    return commentItems;
  }, [items]);

  const setExpandIcon = (_isActive?: boolean) => {
    return <></>;
  };

  const getExpandTextButton = (expanded: boolean) => {
    return expanded ? t('lea.comments.buttons.showLess') : t('lea.comments.buttons.showMore');
  };

  return (
    <div
      className={`comment-wrapper${comment.state === 'CLOSED' ? ' closedComment' : ''}`}
      key={`${comment.commentId}_wrapper`}
      id={comment.commentId}
      onClick={(_e) => focusCommentInEditor(baseEditor, comment)}
      style={{
        borderLeft: commentActive ? `6px solid ${color}` : `1px solid ${color}`,
        borderBottom: `1px solid ${color}`,
        borderTop: `1px solid ${color}`,
        borderRight: `1px solid ${color}`,
        padding: commentActive ? '5px' : '7px',
      }}
    >
      <div className="collapseWrapper">
        <CommentDeleteConfirmation
          confirmationOpened={confirmationOpened}
          isAnswer={isAnswer}
          setDeleteCommentAction={setDeleteCommentAction}
          setIsAnswer={setIsAnswer}
          setConfirmationOpened={setConfirmationOpened}
          comment={comment}
          replyId={replyId}
        />
        <Collapse
          activeKey={commentActive ? `${comment.commentId}_panel` : ''}
          className="comment-collapse"
          expandIcon={({ isActive }) => setExpandIcon(isActive)}
          onChange={(key) => handleCollapseChange(key, comment.commentId)}
          ghost
        >
          <Panel
            id={comment.commentId + '_panel'}
            header={
              <CommentHeader
                name={comment.createdBy.name}
                updated={comment.updatedAt}
                items={getCommentActionItem}
                id={comment.commentId}
                showDate={true}
              />
            }
            key={comment.commentId + '_panel'}
          >
            <div className={`panelWrapper${panelShown ? ' visible' : ''}`}>
              {isEditing?.id === comment.commentId && value ? (
                <EditComment
                  editor={editor}
                  value={value}
                  onChangeHandler={onChangeHandler}
                  handleCancleAddComment={handleCancleEdit}
                  handleAddComment={handleSaveEdit}
                  header={t('lea.comments.editComment.editCommentHeader')}
                />
              ) : (
                commentActive && (
                  <>
                    <Paragraph
                      className="comment-content"
                      key={`comment-full-text-${comment.commentId}`}
                      style={{ width: '100%', height: 'auto' }}
                      ellipsis={{
                        rows: defaultRows + 1,
                        expandable: 'collapsible',
                        expanded,
                        symbol: (expanded) => getExpandTextButton(expanded),
                        onExpand: (_, info) => setExpanded(info.expanded),
                      }}
                    >
                      {commentContent}
                    </Paragraph>
                    <Divider
                      className="reply-divider-open-comment"
                      type="horizontal"
                      style={{ borderColor: comment.colorCode }}
                    />
                  </>
                )
              )}
              {comment.organizedReplies && comment.organizedReplies.length > 0 ? (
                <div className="replies">
                  <div className="comment-reply-header">{t('lea.comments.replies.header')}</div>
                  <Divider
                    className="reply-content-divider"
                    type="horizontal"
                    style={{ borderColor: comment.colorCode }}
                  />
                  {comment.organizedReplies.map((replyWithContent) => {
                    return (
                      <React.Fragment key={replyWithContent.replyId}>
                        {isEditing?.replyId === replyWithContent.replyId && value ? (
                          <EditComment
                            editor={editor}
                            value={value}
                            onChangeHandler={onChangeHandler}
                            handleCancleAddComment={handleCancleEdit}
                            handleAddComment={handleSaveEdit}
                            header={t('lea.comments.editComment.editCommentHeader')}
                            userName={replyWithContent.createdBy.name}
                          />
                        ) : (
                          <>
                            <CommentHeader
                              name={replyWithContent.createdBy.name}
                              updated={replyWithContent.updatedAt}
                              items={getReplyActionItems(
                                replyWithContent.replyId,
                                replyWithContent.content,
                                replyWithContent.createdBy.email,
                              )}
                              id={replyWithContent.replyId}
                              showDate={true}
                            />
                            <div className="comment-content">{replyWithContent.serializedContent}</div>
                          </>
                        )}
                      </React.Fragment>
                    );
                  })}
                </div>
              ) : (
                <></>
              )}
              {isAnswering === comment.commentId && value ? (
                <EditComment
                  editor={editor}
                  value={value}
                  onChangeHandler={onChangeHandler}
                  handleCancleAddComment={handleCancleAnswere}
                  handleAddComment={handleAddReply}
                  header={t('lea.comments.newComment.addReplyHeader')}
                  userName={user?.dto.name ?? ''}
                />
              ) : (
                <Button
                  type="text"
                  className="createAnswere"
                  onClick={() => {
                    setIsEditing(null);
                    setIsAnswering(comment.commentId);
                  }}
                >
                  {t('lea.comments.replies.replyButton')}
                </Button>
              )}
            </div>
          </Panel>
        </Collapse>
      </div>

      {!commentActive ? (
        <Paragraph
          className="comment-header-content"
          key={`comment-preview-text-${comment.commentId}`}
          ellipsis={{
            rows: defaultRows,
            expandable: false,
            expanded: false,
          }}
        >
          {commentContent}
        </Paragraph>
      ) : (
        <></>
      )}
      {!commentActive && comment.replies && comment.replies.length > 0 && (
        // <div
        //   role="heading"
        //   aria-level={3}
        //   className={`repliesCount${comment.state === 'CLOSED' ? ' closedComment' : ''}`}
        // >
        //   {comment.replies.length > 1
        //     ? t('lea.comments.replies.replyCount', { count: comment.replies.length })
        //     : comment.replies[0].createdBy.name}
        // </div>
        <>
          <Divider className="reply-divider" type="horizontal" style={{ borderColor: comment.colorCode }} />
          <div className="comment-reply-header">{t('lea.comments.replies.header')}</div>
          <Divider className="reply-content-divider" type="horizontal" style={{ borderColor: comment.colorCode }} />
          {comment.organizedReplies?.map((reply) => {
            return (
              <div className="reply-preview-container" key={`comment-reply-${reply.replyId}`}>
                <CommentHeader
                  name={reply.createdBy.name}
                  updated={reply.updatedAt}
                  items={getReplyActionItems(reply.replyId, reply.content, reply.createdBy.email)}
                  id={reply.replyId}
                  showDate={false}
                />
                <Paragraph
                  className={`comment-reply-content`}
                  key={reply.replyId}
                  ellipsis={{ rows: 1, expandable: false, expanded: false }}
                >
                  {reply.serializedContent}
                </Paragraph>
              </div>
            );
          })}
        </>
      )}
    </div>
  );
};
