// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useMemo, useRef, useState } from 'react';
import { BaseSelection, Descendant } from 'slate';

import { getEditorReducer, InitialEditorDocument, PluginRegistry } from '../plugin-core';
import { PluginFactory } from '../plugins';

import { PluginEditor } from './PluginEditor';
import { useFirstDocumentState, useSynopsisDocumentsState } from '../../api';

import './Editor.less';
import { AuthorialNotePluginRegistry } from './AuthorialNote/plugins';
import { WithReducer } from '../../components/Store';
import { combineReducers, ReducersMapObject } from 'redux';
import { getReadonlyState } from '../plugin-core/utils/DocumentUtils';
import { tryParseJsonContent } from '../../helpers/JsonParseHelper';
import { useAppInfoState } from '../../components/App';
import { RouterPrompt } from '../../components/Toolbars/SaveConfirmation/RouterPrompt';

export const Editor: FunctionComponent = () => {
  const { basePath } = useAppInfoState();
  const firstDocument = useFirstDocumentState();
  const { documents } = useSynopsisDocumentsState();
  const valueRef = useRef<Descendant[]>([]);
  const valueAuthRef = useRef<Descendant[]>([]);
  const valueSynopsisAuthRefs = useRef<Descendant[][]>([]);
  const valueSynopsisRefs = useRef<Descendant[][]>([]);
  const saveSelection = useRef<BaseSelection>(null);
  const [showPrompt, setShowPrompt] = useState(true);

  useEffect(() => {
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      event.preventDefault();
    };

    window.addEventListener('beforeunload', handleBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);

  useMemo(() => {
    if (documents?.length > 0) {
      valueSynopsisRefs.current = [];
      valueSynopsisAuthRefs.current = [];
      documents.forEach((document, index) => {
        valueSynopsisRefs.current[index] = tryParseJsonContent(
          document.content,
          basePath,
          document.title,
        ) as Descendant[];
        valueSynopsisAuthRefs.current[index] = tryParseJsonContent(
          document.content,
          basePath,
          document.title,
          true,
        ) as Descendant[];
      });
    }
  }, [documents]);

  useMemo(() => {
    if (firstDocument.document) {
      valueRef.current = tryParseJsonContent(
        firstDocument.document.content,
        basePath,
        firstDocument.document.title,
        false,
      ) as Descendant[];
      valueAuthRef.current = tryParseJsonContent(
        firstDocument.document.content,
        basePath,
        firstDocument.document.title,
        true,
      ) as Descendant[];
    }
  }, [firstDocument.document?.content]);

  const Registry = useMemo(() => {
    return (
      firstDocument.document &&
      new PluginRegistry(
        PluginFactory.getPlugins(firstDocument.document, {
          editorDocument: {
            ...InitialEditorDocument,
            documentStateType: firstDocument.document.state,
            documentType: firstDocument.document.type,
          },
          isDynAenderungsvergleich: false,
        }),
      )
    );
  }, [firstDocument.document]);

  const mainEditor = useMemo(() => Registry?.register(), [firstDocument.document]);

  const AuthorialNoteRegistry = useMemo(() => {
    return (
      firstDocument.document &&
      new AuthorialNotePluginRegistry(
        PluginFactory.getAuthorialNotePagePlugins(firstDocument.document, {
          editorDocument: {
            ...InitialEditorDocument,
            documentStateType: firstDocument.document.state,
            documentType: firstDocument.document.type,
            isReadonly: getReadonlyState(firstDocument.document),
          },
          isDynAenderungsvergleich: false,
        }),
      )
    );
  }, [firstDocument.document]);
  const authorialNoteEditor = useMemo(() => AuthorialNoteRegistry?.register(), [firstDocument.document]);

  const synopsisRegistries = useMemo(() => {
    return documents?.map((synopsisDocument, index) => {
      return new PluginRegistry(
        PluginFactory.getSynopsisPagePlugins(synopsisDocument, {
          editorDocument: {
            ...InitialEditorDocument,
            documentStateType: synopsisDocument.state,
            documentType: synopsisDocument.type,
            index: index + 1,
          },
          isDynAenderungsvergleich: false,
        }),
      );
    });
  }, [documents]);

  const synopsisEditors = useMemo(() => {
    return documents?.map((_, index) => {
      return synopsisRegistries[index].register();
    });
  }, [documents]);

  const synopsisAuthorialNoteRegistries = useMemo(() => {
    return documents?.map((synopsisDocument, index) => {
      return new AuthorialNotePluginRegistry(
        PluginFactory.getAuthorialNotePagePlugins(synopsisDocument, {
          editorDocument: {
            ...InitialEditorDocument,
            documentStateType: synopsisDocument.state,
            documentType: synopsisDocument.type,
            index: index + 1,
            isReadonly: getReadonlyState(synopsisDocument),
          },
          isDynAenderungsvergleich: false,
        }),
      );
    });
  }, [documents]);

  const synopsisAuthorialNoteEditors = useMemo(() => {
    return documents?.map((_, index) => {
      return synopsisAuthorialNoteRegistries[index].register();
    });
  }, [documents]);

  const mainEditorReducer: ReducersMapObject | null = useMemo(() => {
    const reducers = Registry?.stateReducer() ?? null;
    return reducers && { 0: reducers };
  }, [firstDocument.document]);

  const synopsisEditorReducers: ReducersMapObject | null = useMemo(() => {
    const reducer: ReducersMapObject = {};
    synopsisRegistries.forEach((synopsisRegistry, index) => {
      const stateReducer = synopsisRegistry.stateReducer();
      if (stateReducer) {
        reducer[index + 1] = stateReducer;
      }
    });
    return reducer;
  }, [documents]);

  const editorReducer = useMemo(() => {
    const reducers = {
      ...mainEditorReducer,
      ...synopsisEditorReducers,
    };
    return getEditorReducer(Object.keys(reducers).length > 0 ? combineReducers(reducers) : null);
  }, [firstDocument.document, documents]);

  return !firstDocument.document || !mainEditor || !authorialNoteEditor || !Registry || !AuthorialNoteRegistry ? (
    <></>
  ) : (
    <main className="lea-editor">
      <RouterPrompt when={showPrompt} />
      <WithReducer keyProp="editorPage" reducer={editorReducer}>
        <Registry.PluginContextProvider>
          <PluginEditor
            valueRef={valueRef}
            valueAuthRef={valueAuthRef}
            valueSynopsisAuthRefs={valueSynopsisAuthRefs}
            valueSynopsisRefs={valueSynopsisRefs}
            mainEditor={mainEditor}
            authorialNoteEditor={authorialNoteEditor}
            synopsisAuthorialNoteEditors={synopsisAuthorialNoteEditors}
            synopsisEditors={synopsisEditors}
            registry={Registry}
            authorialNoteRegistry={AuthorialNoteRegistry}
            synopsisAuthorialNoteRegistries={synopsisAuthorialNoteRegistries}
            synopsisRegistries={synopsisRegistries}
            saveSelection={saveSelection}
            setShowPrompt={setShowPrompt}
          />
        </Registry.PluginContextProvider>
      </WithReducer>
    </main>
  );
};
