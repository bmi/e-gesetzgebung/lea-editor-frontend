// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useCallback, useEffect, useRef } from 'react';
import { AriaController, LeaDrawer } from '@lea/ui';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';

import './AuthorialNotePage.scss';
import { Editable, Slate } from 'slate-react';
import { Descendant } from 'slate';
import { AuthorialNotePluginRegistry } from './plugins';
import { useAuthorialNotePageChange, useAuthorialNotePageState } from '../../plugin-core/editor-store/workspace';
import { useAuthorialNoteState } from '../../plugins/authorialNote';
import { Editor } from '../../../interfaces';
import { useFirstDocumentState, useSynopsisDocumentsState } from '../../../api';
import { useEditorDocumentsLengthState, useEditorDocumentsVisibleState } from '../../plugin-core';
import { useTranslation } from 'react-i18next';

interface AuthorialNotePageProps {
  mainEditor: Editor;
  synopsisEditors: Editor[];
  editorIndex: number;
  onChange: (value: Descendant[]) => void;
  onSynopsisChange: (index: number) => (value: Descendant[]) => void;
  value: React.MutableRefObject<Descendant[]>;
  valueSynopsis: React.MutableRefObject<Descendant[][]>;
  registry: AuthorialNotePluginRegistry;
  synopsisRegistries: AuthorialNotePluginRegistry[];
  handleClick: (event: React.MouseEvent<HTMLDivElement>) => void;
}

export const AuthorialNotePage = ({
  onChange,
  onSynopsisChange,
  value,
  valueSynopsis,
  registry: Registry,
  synopsisRegistries,
  mainEditor,
  synopsisEditors,
  handleClick,
}: AuthorialNotePageProps): ReactElement => {
  const { t } = useTranslation();
  const { document } = useFirstDocumentState();
  const { documents } = useSynopsisDocumentsState();
  const { visible } = useAuthorialNotePageState();
  const { editorDocumentsLength } = useEditorDocumentsLengthState();
  const { closeAuthorialNotePage } = useAuthorialNotePageChange();
  const visibleDocuments = useEditorDocumentsVisibleState();
  const authorialNotes = useAuthorialNoteState();
  const keyDownCallback = useCallback(Registry.keyDownCallback(mainEditor.editor), []);
  const blurCallback = useCallback(Registry.blurCallback(mainEditor.editor), []);

  const authorialNotePageRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (authorialNotePageRef.current) {
      AriaControllerPlateg.setAriaLabelsByQuery(
        '.authorialNote-page-wrapper .ant-drawer-close',
        'Fußnotenbereich schließen',
      );
      AriaController.setElementIdByClassNameInElement(
        'ant-drawer-close',
        'lea-drawer-authorialNote-close-button',
        authorialNotePageRef.current,
      );
    }
    if (visible) {
      Registry.effectCallback(mainEditor.editor);
      synopsisRegistries.forEach((synopsisRegistry, index) => {
        synopsisRegistry.effectCallback(synopsisEditors[index].editor);
      });
    }
  }, [visible, editorDocumentsLength]);

  const handleDragDrop = (event: React.MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleOnContextMenu = (event: React.MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const isFirstColumnVisible = visibleDocuments.includes(0);
  return (
    <div
      className={`authorialNote-page-wrapper${visible ? ' visible' : ''}${
        synopsisEditors.length === 0 ? ' no-synopsis' : ''
      }`}
    >
      <LeaDrawer
        placement="bottom"
        title="Fußnoten"
        open={visible}
        refProp={authorialNotePageRef}
        onClose={closeAuthorialNotePage}
        ariaLabel={t('lea.Document.authorialNoteArea.mainAriaLabel')}
      >
        <div className="authorialNote-content-wrapper">
          <section
            className="authorialNote-section"
            aria-label={t('lea.Document.authorialNoteArea.ariaLabel', {
              index: 1,
              documentTitle: document?.title || '',
            })}
            tabIndex={0}
          >
            <ol className="authorial-note-list">
              <Slate editor={mainEditor.editor} initialValue={value.current} onChange={onChange}>
                <Editable
                  className={`authorialNote-editor${authorialNotes['0']?.length === 0 ? ' empty' : ''}${
                    isFirstColumnVisible ? '' : ' column-hidden'
                  }`}
                  id={`authorialNote-area-0`}
                  renderElement={Registry.renderElementCallback({
                    firstRender: false,
                    cellsVisible: false,
                    editor: mainEditor.editor,
                  })}
                  renderLeaf={Registry.renderLeafCallback(mainEditor.editor)}
                  onKeyDown={keyDownCallback}
                  onContextMenu={handleOnContextMenu}
                  onDragStart={handleDragDrop}
                  onDrop={handleDragDrop}
                  onPaste={Registry.pasteCallback(mainEditor.editor, true)}
                  onCut={Registry.cutCallback(mainEditor.editor, true)}
                  onClick={handleClick}
                  onBlur={blurCallback}
                />
              </Slate>
            </ol>
          </section>
          {synopsisEditors.length > 0 &&
            synopsisEditors.length === valueSynopsis.current.length &&
            synopsisEditors.map((synopsisEditor, index) => {
              const isColumnVisible = visibleDocuments.includes(index + 1);
              if (synopsisEditor) {
                return (
                  <section
                    key={synopsisEditor.id}
                    className="authorialNote-section"
                    aria-label={t('lea.Document.authorialNoteArea.ariaLabel', {
                      index,
                      documentTitle: documents[index]?.title || '',
                    })}
                    tabIndex={0}
                  >
                    <ol className="authorial-note-list">
                      <Slate
                        key={`${documents[index].id}-authorialNotePage`}
                        editor={synopsisEditor.editor}
                        initialValue={valueSynopsis.current[index]}
                        onChange={onSynopsisChange(index)}
                      >
                        <Editable
                          className={`authorialNote-editor synopsis${
                            authorialNotes[`${index + 1}`]?.length === 0 ? ' empty' : ''
                          }${isColumnVisible ? '' : ' column-hidden'}`}
                          id={`authorialNote-area-${index + 1}`}
                          renderElement={synopsisRegistries[index].renderElementCallback({
                            firstRender: false,
                            cellsVisible: false,
                            editor: synopsisEditor.editor,
                          })}
                          renderLeaf={synopsisRegistries[index].renderLeafCallback(synopsisEditor.editor)}
                          onKeyDown={synopsisRegistries[index].keyDownCallback(synopsisEditor.editor)}
                          onContextMenu={handleOnContextMenu}
                          onDragStart={handleDragDrop}
                          onDrop={handleDragDrop}
                          onPaste={synopsisRegistries[index].pasteCallback(synopsisEditor.editor, true)}
                          onCut={synopsisRegistries[index].cutCallback(synopsisEditor.editor, true)}
                          onClick={handleClick}
                          onBlur={synopsisRegistries[index].blurCallback(synopsisEditor.editor)}
                        />
                      </Slate>
                    </ol>
                  </section>
                );
              } else {
                return <></>;
              }
            })}
        </div>
      </LeaDrawer>
    </div>
  );
};
