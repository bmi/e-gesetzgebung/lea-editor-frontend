// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useCallback, useMemo } from 'react';
import { NodeEntry, createEditor, Editor } from 'slate';
import { ReactEditor, RenderElementProps, RenderLeafProps, withReact } from 'slate-react';
import { Divider } from 'antd';
import { v4 as uuid } from 'uuid';

import {
  CombinedPluginRenderProps,
  Plugin,
  PluginRenderLeafProps,
  PluginRenderProps,
  useDynAenderungsvergleichState,
  useEditorDocumentState,
} from '../../../plugin-core';
import { Editor as EditorInterface, Element } from '../../../../interfaces';
import { ItemType } from 'antd/lib/menu/interface';
import { HistoryEditor } from 'slate-history';

interface INavigationDialogProps {
  handleContextMenu: (e: React.MouseEvent<HTMLDivElement>, nodeEntry?: NodeEntry<Element>) => void;
}

interface IContextMenuProps {
  editorIndex: number;
}

interface IRegisterContextProps {
  editorIndex: number;
}

export class AuthorialNotePluginRegistry {
  public constructor(private readonly plugins: Plugin[]) {}

  public register(): EditorInterface {
    const editor = withReact(createEditor() as ReactEditor & HistoryEditor);
    const id = uuid();
    const isRemote = false;
    return { editor: this.plugins.reduce((e, p) => p.create(e), editor), id, isRemote };
  }

  public effectCallback(editor: ReactEditor): void {
    this.plugins.forEach((plugin) => plugin.useEffect(editor));
  }

  public keyDownCallback = (editor: Editor): ((props: any) => any) => {
    return useCallback((event: KeyboardEvent) => this.keyDown(editor, event), []);
  };

  private keyDown(editor: Editor, event: KeyboardEvent): void {
    const keyDownHandled = this.plugins.map((x) => x.keyDown(editor, event));
    if (!keyDownHandled.some((x) => x)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  public pasteCallback(editor: Editor, isEditable: boolean): (event: any) => void {
    return (event: ClipboardEvent) => {
      if (!isEditable) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.plugins.some((x) => (x.paste ? x.paste(editor, event) : false));
    };
  }

  public cutCallback(editor: Editor, isEditable: boolean): (event: any) => void {
    return (event: ClipboardEvent) => {
      if (!isEditable) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.plugins.some((x) => (x.cut ? x.cut(editor, event) : false));
    };
  }

  public blurCallback(editor: Editor): (props: any) => any {
    return (event: FocusEvent) => this.blur(editor, event);
  }

  public blur(editor: Editor, event: FocusEvent): void {
    this.plugins.forEach((p) => p.blur(editor, event));
  }

  public getToolbarRefs(): React.RefObject<HTMLElement>[] {
    return this.plugins.flatMap((p) => p.toolRefs);
  }

  public getContextMenuItemTypes(): ItemType[] {
    let isFirst = true;
    return this.plugins.reduce((itemTypes, plugin) => {
      const newItemTypes = plugin.contextMenuItemTypes;
      if (newItemTypes) {
        if (isFirst) {
          isFirst = false;
          return [...itemTypes, ...newItemTypes];
        }
        return [...itemTypes, { type: 'divider' }, ...newItemTypes];
      }
      return itemTypes;
    }, [] as ItemType[]);
  }

  public hasToolbarTools(): boolean {
    return this.plugins.some((plugin) => plugin.ToolbarTools);
  }

  public isEditable(editor: Editor): boolean {
    return this.plugins.some((plugin) => plugin.isEditable(editor));
  }

  public Toolbar = (): ReactElement => {
    let isFirst = true;
    return (
      <>
        {this.plugins.map((Plugin) => {
          if (isFirst && Plugin.ToolbarTools) {
            isFirst = false;
            return <Plugin.ToolbarTools key={`ToolbarTools_${Plugin.pluginId}`} />;
          }
          return Plugin.ToolbarTools ? (
            <React.Fragment key={`ToolFragment${Plugin.pluginId}`}>
              <Divider className="lea-toolbar-divider" key={`ToolDivider${Plugin.pluginId}`} type="vertical" />
              <Plugin.ToolbarTools key={`ToolbarTools_${Plugin.pluginId}`} />
            </React.Fragment>
          ) : null;
        })}
      </>
    );
  };

  public ViewTools = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ViewTools ? <Plugin.ViewTools key={`ViewTools_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public NavigationDialogs = ({ handleContextMenu }: INavigationDialogProps): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.NavigationDialog ? (
            <Plugin.NavigationDialog
              key={`NavigationDialog_${Plugin.pluginId}`}
              handleContextMenu={handleContextMenu}
            />
          ) : null,
        )}
      </>
    );
  };

  public OptionDialogs = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.OptionDialog ? <Plugin.OptionDialog key={`OptionDialog_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public ActionDialogs = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ActionDialog ? <Plugin.ActionDialog key={`ActionDialog_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public ModalDialogs = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ModalDialog ? <Plugin.ModalDialog key={`ModalDialog_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public ContextMenu = (props: IContextMenuProps): ReactElement => {
    const keyName = `authorialNote-${props.editorIndex}`;
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ContextMenu ? (
            <Plugin.ContextMenu key={`AuthorialNoteContextMenuContent${Plugin.pluginId}`} keyName={keyName} />
          ) : null,
        )}
      </>
    );
  };
  public renderElementCallback(pluginRenderProps: PluginRenderProps): (props: any) => any {
    return (props: RenderElementProps) => this.renderElement({ ...props, ...pluginRenderProps });
  }

  private renderElement(props: CombinedPluginRenderProps): ReactElement {
    for (const p of this.plugins) {
      const element = p.renderElement(props);
      if (element) {
        return element;
      }
    }
    return props.editor.isVoid(props.element) ? <></> : <>{props.children}</>;
  }

  public renderLeafCallback = (editor: ReactEditor): ((props: any) => any) => {
    return useCallback((props: RenderLeafProps) => this.renderLeaf(props, editor), []);
  };

  private renderLeaf(props: RenderLeafProps, editor: ReactEditor): ReactElement {
    const { children } = props as { children: ReactElement };
    for (const p of this.plugins) {
      const element = p.renderLeaf({ ...(props as PluginRenderLeafProps), editor });
      if (element) {
        return element;
      }
    }

    return <span {...props.attributes}>{children}</span>;
  }

  public RegisterContext = ({ editorIndex }: IRegisterContextProps): ReactElement => {
    const { editorDocument } = useEditorDocumentState(editorIndex);
    const { isActive } = useDynAenderungsvergleichState();
    useMemo(() => {
      if (editorDocument) {
        this.plugins.forEach((p) => p.update(editorDocument, isActive));
      }
    }, [editorDocument, isActive]);

    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.RegisterDispatchFunctions ? (
            <Plugin.RegisterDispatchFunctions key={`RegisterDispatchFunctions_${Plugin.pluginId}`} />
          ) : null,
        )}
      </>
    );
  };
}
