// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseText, Editor, NodeEntry, Transforms } from 'slate';
import { Element } from '../../../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLowestNodeAt,
  getLowestNodeEntryAt,
} from '../../../../plugin-core/utils';
import { AUTHORIALNOTE_STRUCTURE } from './AuthorialNotePageStructure';

export class AuthorialNotePageCommands {
  public static mergeAbsaetze(editor: Editor): void {
    const [selectedParagraph, selectedParagraphPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if paragraph to be deleted is empty.
    // Since merge of paragraphs containing text will be a future demand it's handled within here
    if (selectedParagraph.children.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous paragraph's text
    const [previousNode, previousNodePath] = Editor.previous(editor, {
      at: selectedParagraphPath,
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const previousTextNodeEntry = getLowestNodeEntryAt(editor, [...previousNodePath, previousNode.children.length - 1]);
    const [previousTextNode, previousTextNodePath] = previousTextNodeEntry as NodeEntry<BaseText>;
    Transforms.removeNodes(editor, { at: selectedParagraphPath });

    Transforms.select(editor, {
      path: previousTextNodePath,
      offset: previousTextNode?.text.length || 0,
    });
  }

  public static insertAbsatz(editor: Editor): void {
    const [selectedParagraph, selectedParagraphPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    if (selectedParagraph.children.length === 1 && selectedTextNode && selectedTextNode.text.length === 0) {
      return;
    }

    // Create next sibling's path
    const newPath = selectedParagraphPath.map((x, y) => (y === selectedParagraphPath.length - 1 ? ++x : x));

    // Insert new paragraph
    const newParagraph = createElementOfType(AUTHORIALNOTE_STRUCTURE.P, [createTextWrapper('')]);
    Transforms.insertNodes(editor, newParagraph, { at: newPath });

    // Select newly created node
    Transforms.select(editor, { path: [...newPath, 0, 0], offset: 0 });
  }
}
