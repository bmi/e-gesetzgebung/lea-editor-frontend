// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../../../plugin-core';
import { AUTHORIALNOTE_STRUCTURE } from './AuthorialNotePageStructure';
import {
  isEnter,
  isFirstPosition,
  isLastPosition,
  isLeftNavigation,
  isRightNavigation,
  isUpOrDownNavigation,
  isWithinNodeOfType,
  Unit,
} from '../../../../plugin-core/utils';
import { Element } from '../../../../../interfaces';

import { AuthorialNotePageQueries as Queries } from './AuthorialNoteQueries';
import { AuthorialNotePageCommands as Commands } from './AuthorialNotePageCommands';
import { HistoryEditor } from 'slate-history';

export class AuthorialPagePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T & ReactEditor & HistoryEditor {
    const editor = e as T & ReactEditor & HistoryEditor;
    editor.isVoid = (element) => Queries.isVoidElement(editor, element);
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }
    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !Queries.isFirstElement(editor)) {
        Commands.mergeAbsaetze(editor);
        ReactEditor.focus(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor) || !Queries.isLastElement(editor)) {
        deleteForward(unit);
      }
    };
    return editor;
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }
    if ((isEnter(event) && !isLastPosition(editor)) || isUpOrDownNavigation(event)) {
      event.preventDefault();
      return false;
    }
    if (isEnter(event) && isLastPosition(editor)) {
      event.preventDefault();
      Commands.insertAbsatz(editor);
      ReactEditor.focus(editor as ReactEditor);
      return false;
    }

    if (
      (isLeftNavigation(event) && isFirstPosition(editor) && Queries.isFirstElement(editor)) ||
      (isRightNavigation(event) && isLastPosition(editor) && Queries.isLastElement(editor))
    ) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const element = props.element as Element;
    const guid = element.GUID || '';
    const elementType = element.type;
    const changeMark = element['lea:changeMark'] || '';
    const path = ReactEditor.findPath(props.editor, element);
    if (elementType === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE) {
      const handleSelect = () => {
        const authorialNote = global.document.getElementById(`${this.editorDocument.index}:${guid}`);
        authorialNote?.focus();
        authorialNote?.scrollIntoView();
      };

      const handleFocus = () => {
        const authorialNote = global.document.getElementById(`${this.editorDocument.index}:${guid}`);

        authorialNote?.scrollIntoView();
      };

      const handleKeyDown = (e: React.KeyboardEvent) => {
        if (isEnter(e as unknown as KeyboardEvent)) {
          handleSelect();
        }
      };

      const authorialNoteNumber = element.marker?.toString() || '';
      return (
        <li className={`${elementType} ${changeMark}`} {...props.attributes}>
          <a
            id={guid && `${this.editorDocument.index}:authorialNote:${guid}`}
            aria-label={`Fußnote ${authorialNoteNumber}`}
            tabIndex={0}
            onClick={handleSelect}
            onKeyDown={handleKeyDown}
            contentEditable={false}
          >
            {element.marker}
          </a>
          <div
            className={`authorialNoteContent ${this.editorDocument.isReadonly ? '' : 'contentEditable'}`}
            aria-label={`Fußnote ${authorialNoteNumber} Inhalt`}
            contentEditable={!this.editorDocument.isReadonly}
            onFocus={handleFocus}
            suppressContentEditableWarning={this.editorDocument.isReadonly ? undefined : true}
            role={this.editorDocument.isReadonly ? undefined : 'textbox'}
          >
            {props.children}
          </div>
        </li>
      );
    }
    if (path && isWithinNodeOfType(props.editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE, path)) {
      return (
        <div className={`${elementType} ${changeMark}`} {...props.attributes}>
          {props.children}
        </div>
      );
    }

    return null;
  };
}
