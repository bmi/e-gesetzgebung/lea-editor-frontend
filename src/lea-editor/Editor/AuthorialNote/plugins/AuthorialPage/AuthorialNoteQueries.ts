// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseElement, Editor, NodeEntry } from 'slate';
import { Element } from '../../../../../interfaces';
import { hasNodeOfType, isWithinNodeOfType, isWithinTypeHierarchy } from '../../../../plugin-core/utils';
import { AUTHORIALNOTE_STRUCTURE } from './AuthorialNotePageStructure';
import { ReactEditor } from 'slate-react';

export class AuthorialNotePageQueries {
  public static isEditable(editor: Editor): boolean {
    return isWithinTypeHierarchy(editor, [AUTHORIALNOTE_STRUCTURE.P, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE]);
  }

  public static isLastElement(editor: Editor): boolean {
    const [, pElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const [authorialNoteElement] = Editor.above(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE,
    }) as NodeEntry<Element>;
    return authorialNoteElement.children.length - 1 === pElementPath[pElementPath.length - 1];
  }

  public static isFirstElement(editor: Editor): boolean {
    const [, pElementPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === AUTHORIALNOTE_STRUCTURE.P,
    }) as NodeEntry<Element>;

    return pElementPath[pElementPath.length - 1] === 0;
  }
  public static isVoidElement = (editor: ReactEditor, element: BaseElement) => {
    try {
      const path = ReactEditor.findPath(editor, element);
      if (path && hasNodeOfType(editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE, path)) return false;
      if (path && isWithinNodeOfType(editor, AUTHORIALNOTE_STRUCTURE.AUTHORIALNOTE, path)) return false;
    } catch (e) {
      return true;
    }
    return true;
  };
}
