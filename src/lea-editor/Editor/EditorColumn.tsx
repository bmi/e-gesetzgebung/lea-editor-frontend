// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { MouseEventHandler, useEffect, useRef, useState } from 'react';
import { Descendant, NodeEntry } from 'slate';
import { Editable, ReactEditor, Slate } from 'slate-react';
import { DocumentDTO, DocumentType } from '../../api';
import { Element } from '../../interfaces';
import { ArrowDownCurrentColorIcon, ArrowUpCurrentColorIcon, EyeIconCurrentColor, IconButton } from '@lea/ui';
import {
  ColumnSize,
  PluginRegistry,
  useAuthorialNotePageState,
  useColumnSizeChange,
  useColumnSizeState,
  useEditorDocumentsVisibleState,
  useEditorDocumentVisibleChange,
  useEditorOnFocusState,
  useSynopsisInfo,
  useEditorOnFocusDispatch,
  useDynAenderungsvergleichState,
  useEditorInfoDispatch,
} from '../plugin-core';
import { isChromiumGreaterThan129, isFirefox, setAllDividersHeight } from '../plugin-core/utils';
import './PluginEditor.less';
import './Editor.less';
import { useTranslation } from 'react-i18next';
import { Filters } from '@plateg/theme/src/shares/filters';
import { useSearchReplaceState } from '../plugin-core/editor-store/workspace/search-replace';
import { SearchReplaceEditor } from '../plugins/searchAndReplace/SearchReplaceEditor';
import { Collapse, CollapseProps } from 'antd';
import { SynopsisMode } from '../../constants';
import { useCellStructureState } from '../plugins/synopsis/cellStructure/UseCellStructure';

interface ISynopsisPageProps {
  value: Descendant[];
  editor: ReactEditor;
  registry: PluginRegistry;
  index: number;
  cellsVisible: boolean;
  document: DocumentDTO;
  isResizing: number | null;
  setIsResizing: (index: number | null) => void;
  keyDownHandler: React.KeyboardEventHandler;
  onChange: (value: Descendant[]) => void;
  handleDragDrop: (event: React.MouseEvent<HTMLDivElement>) => void;
  handleClick: (event: React.MouseEvent<HTMLDivElement>) => void;
  handleScroll: (event: React.UIEvent<HTMLDivElement, UIEvent>) => void;
  onContextMenu: (e: React.MouseEvent<HTMLDivElement>, newNodeEntry?: NodeEntry<Element>) => void;
  onPaste: (event: any) => void;
  onCut: (event: any) => void;
  onBlur: React.FocusEventHandler;
}

export const EditorColumn = ({
  value,
  editor,
  registry: Registry,
  index,
  cellsVisible,
  document,
  isResizing,
  setIsResizing,
  keyDownHandler,
  onChange,
  handleDragDrop,
  handleClick,
  handleScroll,
  onContextMenu,
  onPaste,
  onCut,
  onBlur,
}: ISynopsisPageProps) => {
  const { t } = useTranslation();
  const cells = useCellStructureState();
  const smallSize = 456;
  const mediumSize = 558.5;
  const largeSize = 660;
  const { isSynopsisVisible } = useSynopsisInfo();
  const authorialnotePage = useAuthorialNotePageState();
  const { setColumnSize } = useColumnSizeChange();
  const { setEditorOnFocus } = useEditorOnFocusDispatch();
  const columnSize = useColumnSizeState(index);
  const editorOnFocus = useEditorOnFocusState();
  const visibleDocuments = useEditorDocumentsVisibleState();
  const [isColumnVisible, setIsColumnVisible] = useState(true);
  const { setDocumentsVisible } = useEditorDocumentVisibleChange();
  const { isActive: isDynAenderungsvergleich } = useDynAenderungsvergleichState();
  const firstRender = useRef(true);
  const firstResize = useRef(true);
  const startXOffset = useRef(0);
  const { searchText } = useSearchReplaceState();
  const [searchString, setSearchString] = useState('');
  const [eyeIconHasHover, setEyeIconHasHover] = useState(false);
  const [style, setStyle] = useState<React.CSSProperties | undefined>(
    isSynopsisVisible
      ? {
          maxWidth: `${largeSize}px`,
          minWidth: `${smallSize}px`,
        }
      : { width: '21cm' },
  );
  const { synopsisMode } = useSynopsisInfo();
  const { updateSelection } = useEditorInfoDispatch();

  useEffect(() => {
    if (visibleDocuments.length > 0) {
      setIsColumnVisible(visibleDocuments.includes(index));
    }
  }, [visibleDocuments.length]);

  useEffect(() => {
    firstRender.current = false;
    const editable = global.document.getElementById(`editable-area-${index}`);
    if (editable && !(isFirefox || isChromiumGreaterThan129)) {
      editable.contentEditable = 'false';
    }
  }, []);

  useEffect(() => {
    setSearchString(searchText);
  }, [searchText]);

  const documentTypeClassName = document.type.toLowerCase().replace('_', '-');

  const compoundDocumentTitle = document?.cdName ?? '';
  const documentTitle = document?.title ?? '';
  const documentVersion = document?.version ?? '';
  const documentLastUpdateUTC = `${document.updatedAt || ''}${document.updatedAt.endsWith('Z') ? '' : 'Z'}`;
  const documentLastUpdateTime = Filters.timeFromString(documentLastUpdateUTC);
  const documentLastUpdateDate = Filters.dateFromString(documentLastUpdateUTC);
  const todayDate = new Date().toISOString().slice(0, 10);
  const lastUpdate = documentLastUpdateUTC.slice(0, 10);

  const getWidthFromColumnSize = (size: ColumnSize) => {
    let newWidth = 0;
    switch (size) {
      case ColumnSize.MEDIUM:
        newWidth = mediumSize;
        break;
      case ColumnSize.LARGE:
        newWidth = largeSize;
        break;
      default:
        newWidth = smallSize;
    }
    return newWidth;
  };

  const onMouseDown: MouseEventHandler<HTMLDivElement> = (e) => {
    setIsResizing(index);
    const element = global.document.getElementById(`editor-column-${index}`) as HTMLElement;
    startXOffset.current = e.clientX - element.getBoundingClientRect().right;
  };

  //when resizing through divider on mouseup
  const onMouseUp = () => {
    if (isResizing !== null) {
      setAllDividersHeight(index);
    }
  };

  //when style is changed after changing size in resizer component and resizing through divider is null
  useEffect(() => {
    if (isResizing === null) {
      setAllDividersHeight(index);
    }
  }, [style]);

  useEffect(() => {
    if (index === 0) {
      setAllDividersHeight(index);
    }
  }, [cells]);

  const updateSize = (newWidth: number) => {
    if (newWidth >= smallSize && newWidth <= largeSize) {
      if (smallSize < newWidth) {
        if (newWidth < largeSize) {
          columnSize !== ColumnSize.MEDIUM && setColumnSize(index, ColumnSize.MEDIUM);
        } else {
          columnSize !== ColumnSize.LARGE && setColumnSize(index, ColumnSize.LARGE);
        }
      } else {
        columnSize !== ColumnSize.SMALL && setColumnSize(index, ColumnSize.SMALL);
      }
    }
  };

  const handleResize = (newX?: number) => {
    const element = global.document.getElementById(`editor-column-${index}`) as HTMLElement;
    let newWidth = newX
      ? newX - element?.getBoundingClientRect().left - startXOffset.current
      : element?.getBoundingClientRect().width - 5;
    if (newWidth - smallSize < 10) {
      newWidth = smallSize;
    } else if (largeSize - newWidth < 10) {
      newWidth = largeSize;
    }
    if (isSynopsisVisible) {
      setStyle({ maxWidth: `${newWidth}px`, minWidth: `${newWidth}px` });
      updateSize(newWidth);
    }
  };

  const onMouseMove = (e: MouseEvent) => {
    if (isResizing === index) {
      window.getSelection()?.empty();
      handleResize(e.clientX);
    }
  };

  const getClosestColumnIndex = () => {
    const closestColumnIndex = [...visibleDocuments].sort((a, b) => Math.abs(index - a + 0.1) - Math.abs(index - b))[0];
    return closestColumnIndex;
  };

  const handleEyeButtonClick = () => {
    setTimeout(() => setEyeIconHasHover(false));
    if (!isColumnVisible) {
      setDocumentsVisible([...visibleDocuments, index]);
    } else {
      setDocumentsVisible(visibleDocuments.filter((i) => i !== index));
    }
  };

  useEffect(() => {
    if (!isColumnVisible && editorOnFocus.index === index) {
      setEditorOnFocus({ index: getClosestColumnIndex(), isAuthorialNoteEditor: false });
    }
  }, [isColumnVisible]);

  useEffect(() => {
    global.document.addEventListener('mousemove', onMouseMove);
    if (isSynopsisVisible) global.document.addEventListener('mouseup', onMouseUp);
    return () => {
      global.document.removeEventListener('mousemove', onMouseMove);
      if (isSynopsisVisible) global.document.removeEventListener('mouseup', onMouseUp);
    };
  });

  useEffect(() => {
    if (isColumnVisible) {
      const authorialNoteElement = global.document.getElementById(`authorialNote-area-${index}`) as HTMLElement;
      if (authorialNoteElement) {
        setAuthorialNoteWidth(authorialNoteElement);
      }
    }
  }, [style, authorialnotePage.visible, isColumnVisible]);

  const setAuthorialNoteWidth = (authorialNoteElement: HTMLElement) => {
    if (isSynopsisVisible && style && style.maxWidth && style.minWidth) {
      authorialNoteElement.style.maxWidth = `${index > 0 ? Number(style.maxWidth) + 2 : style.maxWidth}`;
      authorialNoteElement.style.minWidth = `${index > 0 ? Number(style.minWidth) + 2 : style.minWidth}`;
    } else {
      authorialNoteElement.style.width = '100%';
    }
  };

  useEffect(() => {
    if (isSynopsisVisible && !firstResize.current) {
      const newWidth = getWidthFromColumnSize(columnSize);
      if (!firstResize.current) {
        setStyle({
          maxWidth: `${newWidth}px`,
          minWidth: `${newWidth}px`,
        });
      }
    } else {
      firstResize.current = false;
    }
  }, [columnSize]);

  useEffect(() => {
    if (isSynopsisVisible) {
      setStyle({
        maxWidth: `${largeSize}px`,
        minWidth: `${smallSize}px`,
      });
      setTimeout(() => handleResize());
    } else {
      setTimeout(() =>
        setStyle({
          width: '21cm',
        }),
      );
    }
  }, [isSynopsisVisible]);

  const editorColumnClassName = () => {
    return `editorColumn${isSynopsisVisible && editorOnFocus.index === index ? ' selected' : ''}${
      isResizing !== null ? ' resizing' : ''
    }${isColumnVisible ? '' : ' column-hidden'}`;
  };

  const eyeIconAriaLabel = () => {
    return `${isColumnVisible ? 'Spalte ausblenden' : 'Spalte einblenden'}`;
  };

  const setExpandIcon = (isActive?: boolean) => {
    return <div className="lea-icon">{isActive ? <ArrowUpCurrentColorIcon /> : <ArrowDownCurrentColorIcon />}</div>;
  };

  const collapseItem: CollapseProps['items'] = [
    {
      key: index,
      label: (
        <p className="synopsis-header-title">
          {t('lea.Document.editorArea.collapseItems.title')}: <b>{documentTitle}</b>
        </p>
      ),
      children: (
        <div className="synopsis-header-text">
          <p>
            {t('lea.Document.editorArea.collapseItems.compoundDocument')}: <b>{compoundDocumentTitle}</b>
          </p>
          <p className="synopsis-header-version-text">
            {t('lea.Document.editorArea.collapseItems.version')}: <b>{documentVersion}</b>
          </p>
          <p className="synopsis-header-date-text">
            {t('lea.Document.editorArea.collapseItems.lastSaved')}{' '}
            {lastUpdate === todayDate ? (
              <b>{`${t('lea.Document.editorArea.collapseItems.today')} · ${documentLastUpdateTime}`}</b>
            ) : (
              <b>
                {documentLastUpdateDate && documentLastUpdateTime
                  ? `${documentLastUpdateDate} · ${documentLastUpdateTime}`
                  : ''}
              </b>
            )}
          </p>
        </div>
      ),
    },
  ];

  return (
    <>
      <section
        className={editorColumnClassName()}
        style={style}
        id={`editor-column-${index}`}
        tabIndex={0}
        aria-label={t('lea.Document.editorArea.ariaLabel', {
          index: index + 1,
          documentTitle,
        })}
      >
        <Registry.RegisterContext editorIndex={index} editor={editor} />
        {isSynopsisVisible && (
          <div
            className={`synopsis-document-synopsis-header synopsis-header${
              isColumnVisible ? ' column-visible' : ' column-hidden'
            }${isSynopsisVisible && editorOnFocus.index === index ? ' selected' : ''}`}
          >
            <Collapse
              key={`collapse-${index}`}
              defaultActiveKey={index}
              items={collapseItem}
              bordered={false}
              expandIcon={({ isActive }) => setExpandIcon(isActive)}
              expandIconPosition="end"
              size="small"
              className="synopsis-header-collapse"
            />

            <IconButton
              id={`column-visible-button-${index}`}
              onClick={handleEyeButtonClick}
              aria-label={eyeIconAriaLabel()}
              disabled={false}
              onHover={setEyeIconHasHover}
              icon={<EyeIconCurrentColor active={!isColumnVisible} hasHover={eyeIconHasHover} />}
            />
          </div>
        )}
        <Slate editor={editor} initialValue={value} onChange={onChange}>
          <Editable
            className={`plugin-editor synopsis-visible synopsisDocument ${documentTypeClassName || ''}${
              isColumnVisible ? '' : ' column-hidden'
            } ${document.type === DocumentType.SYNOPSE ? 'synopsis-view' : ''}${synopsisMode === SynopsisMode.Changes ? ' synopsis-changes-mode' : ' synopsis-hdr-mode'} ${isDynAenderungsvergleich ? 'dyn-aenderungsvergleich-active' : 'no-dyn-aenderungsvergleich'}`}
            id={`editable-area-${index}`}
            spellCheck={true}
            renderElement={Registry.renderElementCallback(
              { firstRender: firstRender.current, cellsVisible, editor },
              index,
            )}
            renderLeaf={Registry.renderLeafCallback(editor)}
            onKeyDown={keyDownHandler}
            onBlur={onBlur}
            decorate={Registry.decorateCallback(editor as SearchReplaceEditor, searchString)}
            onContextMenu={onContextMenu}
            onDragStart={handleDragDrop}
            onSelect={() => {
              if ((isFirefox || isChromiumGreaterThan129) && editor.children) {
                const selection = window.getSelection();
                if (selection?.anchorNode) {
                  const selectionRange = ReactEditor.toSlateRange(editor, selection, {
                    exactMatch: true,
                    suppressThrow: true,
                  });
                  if (selectionRange) {
                    editor.setSelection(selectionRange);
                    updateSelection(index, false, true, selectionRange);
                  }
                }
              }
            }}
            onDrop={handleDragDrop}
            onClick={handleClick}
            onScroll={handleScroll}
            onPaste={onPaste}
            onCut={onCut}
          />
        </Slate>
      </section>
      {isSynopsisVisible && (
        <div
          className={`changeSize${isResizing === index && isColumnVisible ? ' active' : ''}${
            isColumnVisible ? ' column-visible' : ''
          }`}
          onMouseDown={onMouseDown}
        >
          {isColumnVisible && (
            <div className="resizer">
              <div className="resizerStroke"></div>
              <div className="resizerStroke"></div>
            </div>
          )}
        </div>
      )}
    </>
  );
};
