// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useCallback, useEffect, useMemo, useReducer, useRef, useState } from 'react';
import { BaseSelection, Descendant } from 'slate';
import { ReactEditor } from 'slate-react';
import isHotkey from 'is-hotkey';

import { ContextMenu } from './ContextMenu';
import {
  PluginRegistry,
  useEditorDocumentReadonlyState,
  useEditorDocumentsLengthState,
  useEditorInfoDispatch,
  useEditorDocumentsInfoChange,
  useSynopsisInfo,
  useEditorOnFocusState,
  useEditorOnFocusDispatch,
  useSynchronousScrollingState,
  useEditorDocumentsVisibleState,
  useDynAenderungsvergleichState,
  useDynAenderungsvergleichDispatch,
  useSynchronousScrollingDispatch,
  EditorFocus,
} from '../plugin-core';
import { HistoryEditor } from 'slate-history';

import pkg from '../../../package.json';

import './PluginEditor.less';
import { Divider } from 'antd';
import { CommentPage } from './Comment';
import {
  DocumentStateType,
  useExportDocument,
  useFirstDocumentState,
  useSynopsisDocumentsDispatch,
  useSynopsisDocumentsState,
  useAllDocumentsDispatch,
  OnSuccessFunction,
  EntityType,
  DocumentType,
  CommentService,
  updateFirstDocument,
  DocumentInfo,
  CommentResponseDTO,
} from '../../api';
import { AuthorialNotePage } from './AuthorialNote/AuthorialNotePage';
import { FormatAuthorialNoteWizard } from '../../components/Wizard/FormatAuthorialNote';
import { AuthorialNotePluginRegistry } from './AuthorialNote/plugins';
import { Toolbar } from '@lea/ui';
import { ToolbarFileTool, ToolbarInsertTool, ToolbarViewTool } from './Toolbar';
import RightToolbar from '../../components/Toolbars/RightToolbar';
import { SyopsisToolbarTool } from './Synopsis';
import { SynopsisWizard } from '../../components/Wizard/SynopsisWizard';
import { useCommentCountState, useCommentsChange, useCommentsState } from '../plugins/comment';
import { Editor } from '../../interfaces';
import { EditorRegistry } from './EditorRegistry';
import TopToolbar from '../../components/Toolbars/TopToolbar';
import { NewVersionWizard } from '../../components/Wizard/NewVersionWizard';
import { useContextMenuChange, useContextMenuNodeEntryState } from '../plugin-core/editor-store/workspace/contextMenu';
import {
  checkIfCellsVisible,
  isFirefox,
  isNextLandmarkCommand,
  isPreviousLandmarkCommand,
  isSearch,
} from '../plugin-core/utils';
import { useAppInfoState } from '../../components/App';
import { EgfaWizard, Wizards, useWizardVisibleDispatch } from '../../components/Wizard';
import { TableOfContentsWrapper } from './TableOfContents';
import SynchronousScrollingWizard from '../../components/Wizard/SynchronousScrollingWizard/SynchronousScrollingWizard';

import { EditorColumn } from './EditorColumn';
import { focusLandMark } from '../plugin-core/utils/LandmarkUtils';
import { useTranslation } from 'react-i18next';
import { SearchReplaceCommands } from '../plugins/searchAndReplace/SearchReplaceCommands';
import { useSearchReplaceState } from '../plugin-core/editor-store/workspace/search-replace';
import { SearchForTypes } from '../plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';
import { SearchReplaceEditor } from '../plugins/searchAndReplace/SearchReplaceEditor';
import { useCellStructureDispatch } from '../plugins/synopsis/cellStructure/UseCellStructure';
import { PinnedCompoundDocumentsSettings } from '../../api/User/UserSettings';
import { useDocumentsTableActionState } from '../../components/Pages/useDocumentsTableAction';
import { useAppDispatch } from '../../components/Store';
import { displayMessage } from '@plateg/theme';

interface PluginEditorProps {
  valueRef: React.MutableRefObject<Descendant[]>;
  valueAuthRef: React.MutableRefObject<Descendant[]>;
  valueSynopsisRefs: React.MutableRefObject<Descendant[][]>;
  valueSynopsisAuthRefs: React.MutableRefObject<Descendant[][]>;
  mainEditor: Editor;
  authorialNoteEditor: Editor;
  synopsisEditors: Editor[];
  synopsisAuthorialNoteEditors: Editor[];
  registry: PluginRegistry;
  authorialNoteRegistry: AuthorialNotePluginRegistry;
  synopsisRegistries: PluginRegistry[];
  synopsisAuthorialNoteRegistries: AuthorialNotePluginRegistry[];
  saveSelection: React.MutableRefObject<BaseSelection>;
  setShowPrompt: React.Dispatch<React.SetStateAction<boolean>>;
}

export const PluginEditor: FunctionComponent<PluginEditorProps> = ({
  valueRef,
  valueAuthRef,
  valueSynopsisRefs,
  valueSynopsisAuthRefs,
  mainEditor,
  authorialNoteEditor,
  synopsisEditors,
  synopsisAuthorialNoteEditors,
  registry: Registry,
  authorialNoteRegistry,
  synopsisRegistries,
  synopsisAuthorialNoteRegistries,
  saveSelection,
  setShowPrompt,
}) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const { openContextMenu } = useContextMenuChange();
  const { openWizard: openSynopsisWizard } = useWizardVisibleDispatch(Wizards.SYNOPSIS);
  const { calculateCellStructure } = useCellStructureDispatch();

  const { exportDocument } = useExportDocument();
  const { fetchComments } = useCommentsChange();
  const { comments } = useCommentsState();
  const [activeElement, setActiveElement] = useState<HTMLElement | null>(null);
  const { updateSelection, setEditorDocumentsCount } = useEditorInfoDispatch();
  const { searchType } = useSearchReplaceState();
  const pluginDocuments = useEditorDocumentsInfoChange();
  const synopsisDocument = useSynopsisDocumentsDispatch();
  const allDocuments = useAllDocumentsDispatch();
  const firstDocumentState = useFirstDocumentState();
  const { documents } = useSynopsisDocumentsState();
  const synchronousScrolling = useSynchronousScrollingState();
  const { setSynchronousScrollingOn } = useSynchronousScrollingDispatch();
  const visibleDocuments = useEditorDocumentsVisibleState();
  const userSettingsRef = useRef<PinnedCompoundDocumentsSettings | null>(null);

  const { basePath } = useAppInfoState();
  const { nodeEntry } = useContextMenuNodeEntryState();
  const { isSynopsisVisible, editorDocumentsCount } = useSynopsisInfo();
  const { editorDocumentsLength } = useEditorDocumentsLengthState();

  const { compoundDocumentInfo } = useDocumentsTableActionState();
  const [optionsDialogEditor, setOptionsDialogEditor] = useState<SearchReplaceEditor | null>(null);

  const commentSetOnScroll =
    useRef<React.Dispatch<React.SetStateAction<React.UIEvent<HTMLDivElement, UIEvent> | undefined>>>();

  const editorOnFocus = useEditorOnFocusState();
  const { setEditorOnFocus } = useEditorOnFocusDispatch();
  const { editorDocumentReadonly } = useEditorDocumentReadonlyState(editorOnFocus.index);

  const { triedToDisableActive } = useDynAenderungsvergleichState();
  const { toggleDynAenderungsvergleich, toggleTriedToDisableActive } = useDynAenderungsvergleichDispatch();

  const [isElementContentEditable, setIsElementContentEditable] = useState(true);
  const [isResizing, setIsResizing] = useState<number | null>(null);
  const [deleteCommentIds, setDeleteCommentIds] = useState<string[]>([]);

  const firstRender = useRef(true);

  const cellsVisible = checkIfCellsVisible(isSynopsisVisible, firstDocumentState.document, documents);

  //To render the comments
  const [, forceUpdate] = useReducer((x: number) => x + 1, 0);
  const commentCount = useCommentCountState();
  useEffect(() => {
    setTimeout(() => forceUpdate());
  }, [commentCount]);

  useEffect(() => {
    firstRender.current = false;
  }, []);

  const getEditor = ({ index, isAuthorialNoteEditor }: EditorFocus): SearchReplaceEditor | null => {
    let selectedEditor;
    if (index > 0) {
      selectedEditor = synopsisEditors[index - 1]?.editor;
    } else if (isAuthorialNoteEditor) {
      if (index > 0) {
        selectedEditor = synopsisAuthorialNoteEditors[index - 1]?.editor;
      } else {
        selectedEditor = authorialNoteEditor?.editor;
      }
    } else {
      selectedEditor = mainEditor?.editor;
    }
    return (selectedEditor as SearchReplaceEditor & HistoryEditor) || null;
  };

  const [deleteCommentsResponse, deleteCommentsRequest] = CommentService.deleteComment(
    firstDocumentState.document?.id ?? '',
    deleteCommentIds,
    { content: JSON.stringify(getEditor({ index: 0, isAuthorialNoteEditor: false })?.children ?? []) },
  );

  const getRegistry = ({ index, isAuthorialNoteEditor }: EditorFocus): PluginRegistry | null => {
    if (isAuthorialNoteEditor) return null;
    let selectedRegistry;
    if (index > 0) {
      selectedRegistry = synopsisRegistries[index - 1];
    } else {
      selectedRegistry = Registry;
    }
    return selectedRegistry || null;
  };

  const isEditable = (editorOnFocus: EditorFocus): boolean => {
    const registry = getRegistry(editorOnFocus);
    const editor = getEditor(editorOnFocus);
    return !!editor && !!registry && registry.isEditable(editor);
  };

  const onSelectionChange = (index: number, isAuthorialNoteEditor?: boolean) => {
    const editor = getEditor(editorOnFocus);
    if (editor) {
      if (index === editorOnFocus.index && !!isAuthorialNoteEditor === editorOnFocus.isAuthorialNoteEditor) {
        updateSelection(index, !!isAuthorialNoteEditor, isEditable(editorOnFocus), editor.selection);
      } else {
        setEditorOnFocus({ index, isAuthorialNoteEditor: !!isAuthorialNoteEditor });
      }
    }
  };

  const keyDownHandler = useCallback(
    (event: React.KeyboardEvent<HTMLDivElement>) => {
      const e = event.nativeEvent;

      // Just for Dev reasons
      if (isHotkey('mod+l', e)) {
        console.log(valueRef.current);
        console.log(mainEditor.editor.selection);
      }
    },
    [Registry],
  );
  const handleDragDrop = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
  }, []);

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const isEditable = isContentEditable(event.target as HTMLElement);
    if (!isEditable) {
      (global.document.activeElement as HTMLElement)?.blur();
      const editor = getEditor(editorOnFocus);
      if (editor) {
        editor.selection = null;
      }
      onSelectionChange(editorOnFocus.index);
    }
    setIsElementContentEditable(isEditable);
  };

  //Workaround Firefox selection bug
  const isContentEditable = useCallback(
    (element: HTMLElement): boolean => {
      if (
        !isFirefox ||
        !editorDocumentReadonly ||
        editorDocumentReadonly.documentStateType !== DocumentStateType.DRAFT ||
        editorDocumentReadonly.isReadonly
      )
        return true;
      else return isContentEditableFirefox(element);
    },
    [editorDocumentReadonly],
  );

  const isContentEditableFirefox = useCallback(
    (element: HTMLElement): boolean => {
      const editableAttribute = element.getAttribute('contentEditable');
      if (editableAttribute === 'true') {
        return true;
      } else if (editableAttribute === 'false' || !element.parentElement) {
        return false;
      } else {
        return isContentEditableFirefox(element.parentElement);
      }
    },
    [editorDocumentReadonly, editorOnFocus],
  );

  const changeScrollTopTo = (scrollTop: number, editorIndex?: number) => {
    for (let i = 0; i <= synopsisEditors.length || 0; i++) {
      if (!editorIndex || i !== editorIndex) {
        const scrollElement = document.getElementById(`editable-area-${i}`);
        if (scrollElement) {
          scrollElement.scrollTop = scrollTop;
        }
      }
    }
  };

  const handleScroll = (editorIndex: number) => (event: React.UIEvent<HTMLDivElement, UIEvent>) => {
    commentSetOnScroll.current?.(event);
    if (synchronousScrolling && visibleDocuments.includes(editorIndex)) {
      changeScrollTopTo((event.target as HTMLElement).scrollTop, editorIndex);
    }
  };

  const onContextMenu = (e: React.MouseEvent<HTMLDivElement>) => {
    const contentEditable = isContentEditable(e.target as HTMLElement);
    if (!contentEditable) {
      (global.document.activeElement as HTMLElement)?.blur();
      const editor = getEditor(editorOnFocus);
      if (editor) {
        editor.selection = null;
      }
      onSelectionChange(editorOnFocus.index);
    }
    openContextMenu({ e });
  };

  const onMouseUp = () => {
    if (isResizing !== null) {
      setIsResizing(null);
      window.getSelection()?.empty();
    }
  };

  const onKeyDown = (e: KeyboardEvent) => {
    if (isNextLandmarkCommand(e)) {
      e.preventDefault();
      e.stopPropagation();
      focusLandMark(true);
    } else if (isPreviousLandmarkCommand(e)) {
      e.preventDefault();
      e.stopPropagation();
      focusLandMark(false);
    } else if (isSearch(e)) {
      e.preventDefault();
      e.stopPropagation();
      SearchReplaceCommands.selectSearchReplaceDialog();
    }
  };

  useEffect(() => {
    global.document.addEventListener('mouseup', onMouseUp);
    global.document.addEventListener('keydown', onKeyDown);
    return () => {
      global.document.removeEventListener('mouseup', onMouseUp);
      global.document.removeEventListener('keydown', onKeyDown);
    };
  });

  //perform updateSelection after editorOnFocus change
  useEffect(() => {
    const editor = getEditor(
      saveSelection.current ? { index: editorOnFocus.index, isAuthorialNoteEditor: false } : editorOnFocus,
    );
    if (editor) {
      if (saveSelection.current) {
        editor.selection = saveSelection.current;
        saveSelection.current = null;
        ReactEditor.focus(editor);
      }
      updateSelection(
        editorOnFocus.index,
        editorOnFocus.isAuthorialNoteEditor,
        isEditable(editorOnFocus),
        editor.selection,
      );
    }
  }, [editorOnFocus]);

  useEffect(() => {
    if (nodeEntry) {
      const editor = getEditor(editorOnFocus);
      if (editor) {
        editor.selection = null;
      }
      onSelectionChange(editorOnFocus.index);
    }
  }, [nodeEntry]);

  useEffect(() => {
    if (editorDocumentsLength !== editorDocumentsCount) {
      setEditorDocumentsCount(editorDocumentsLength);
    }
  }, [editorDocumentsLength]);

  useEffect(() => {
    const editor = getEditor(editorOnFocus);
    const handleCopy = Registry.copyCallback(editor);
    const handlePaste = (event: ClipboardEvent) => {
      if (!isElementContentEditable) {
        event.preventDefault();
        event.stopPropagation();
      }
    };
    global.document.addEventListener('copy', handleCopy);
    global.document.addEventListener('paste', handlePaste);
    return () => {
      global.document.removeEventListener('copy', handleCopy);
      global.document.removeEventListener('paste', handlePaste);
    };
  }, [isElementContentEditable]);

  useEffect(() => {
    if (firstDocumentState.document) {
      pluginDocuments.setDocuments([firstDocumentState.document]);
    }
  }, [firstDocumentState.document]);

  useEffect(() => {
    const allDocuments = firstDocumentState.document ? [firstDocumentState.document] : [];
    allDocuments.push(...documents);
    pluginDocuments.setDocuments(allDocuments);
  }, [documents]);

  useEffect(() => {
    setTimeout(() => updateAllCells());
  }, [mainEditor, synopsisEditors]);

  useEffect(() => {
    if (firstDocumentState.document?.type === DocumentType.SYNOPSE) return;
    fetchComments(firstDocumentState.document?.id ?? '');
  }, [firstDocumentState.document]);

  useEffect(() => {
    if (synchronousScrolling) {
      changeScrollTopTo(0);
    }
  }, [synchronousScrolling]);

  useEffect(() => {
    const editor = getEditor({
      index: editorOnFocus.index,
      isAuthorialNoteEditor: searchType === SearchForTypes.AuthorialNotes,
    });
    setOptionsDialogEditor(editor);
  }, [searchType, editorOnFocus]);

  useEffect(() => {
    // check for synchronious scrolling
    if (isSynopsisVisible && !synchronousScrolling && cellsVisible) {
      setSynchronousScrollingOn();
    }
  }, [isSynopsisVisible]);

  const editorRegistry = useMemo(() => {
    return mainEditor && authorialNoteEditor && new EditorRegistry([mainEditor, authorialNoteEditor]);
  }, [mainEditor, authorialNoteEditor]);

  const synopsisEditorRegistries = useMemo(() => {
    let editorRegistries: EditorRegistry[] = [];
    if (synopsisEditors.length === synopsisAuthorialNoteEditors.length && synopsisEditors.length > 0) {
      editorRegistries = synopsisEditors.map(
        (synopsisEditor, index) => new EditorRegistry([synopsisEditor, synopsisAuthorialNoteEditors[index]]),
      );
    }
    return editorRegistries;
  }, [synopsisEditors, synopsisAuthorialNoteEditors]);

  const updateAllCells = () => {
    const editors = synopsisEditors.map((editor) => editor.editor);
    calculateCellStructure(mainEditor.editor, ...editors);
  };

  const onChangeHandler = useCallback(
    (newValue: Descendant[]) => {
      setActiveElement(document.activeElement as HTMLElement);
      valueRef.current = newValue;
      if (!mainEditor || !authorialNoteEditor || !editorRegistry || !Registry) return;
      if (mainEditor.isRemote) {
        mainEditor.isRemote = false;
        return;
      }
      if (isFirefox) {
        const activeElement = document.activeElement;
        ReactEditor.focus(mainEditor.editor);
        (activeElement as HTMLElement).focus();
      } else {
        ReactEditor.focus(mainEditor.editor);
      }
      if (mainEditor.editor.operations.some((op) => op.type === 'set_selection')) {
        onSelectionChange(0);
      }
      if (authorialNoteEditor.editor.children.length === 0) {
        valueAuthRef.current = JSON.parse(JSON.stringify(newValue)) as Descendant[];
      }
      editorRegistry.apply(mainEditor);
    },
    [editorOnFocus, mainEditor, authorialNoteEditor, editorRegistry, Registry],
  );

  const onSynopsisChangeHandler = useCallback(
    (index: number) => (newValue: Descendant[]) => {
      valueSynopsisRefs.current[index] = newValue;
      if (!synopsisEditors || !synopsisRegistries) return;
      if (synopsisEditors[index].isRemote) {
        synopsisEditors[index].isRemote = false;
        return;
      }
      if (synopsisEditors[index].editor.operations.some((op) => op.type === 'set_selection')) {
        onSelectionChange(index + 1);
      }
      if (synopsisAuthorialNoteEditors[index].editor.children.length === 0) {
        valueSynopsisAuthRefs.current[index] = JSON.parse(JSON.stringify(newValue)) as Descendant[];
      }
      synopsisEditorRegistries[index].apply(synopsisEditors[index]);
      if (isFirefox) {
        const activeElement = document.activeElement;
        ReactEditor.focus(synopsisEditors[index].editor);
        (activeElement as HTMLElement).focus();
      } else {
        ReactEditor.focus(synopsisEditors[index].editor);
      }
    },
    [synopsisEditors, editorOnFocus, synopsisRegistries, synopsisEditorRegistries],
  );

  const onAuthorialNoteChangeHandler = useCallback(
    (newValue: Descendant[]) => {
      valueAuthRef.current = newValue;
      if (!authorialNoteEditor || !editorRegistry || !authorialNoteRegistry) return;
      if (!authorialNoteEditor.isRemote) {
        if (isFirefox) {
          const activeElement = document.activeElement;
          ReactEditor.focus(authorialNoteEditor.editor);
          (activeElement as HTMLElement).focus();
        } else {
          ReactEditor.focus(authorialNoteEditor.editor);
        }
        if (authorialNoteEditor.editor.operations.some((op) => op.type === 'set_selection')) {
          onSelectionChange(0, true);
        }
        editorRegistry.apply(authorialNoteEditor);
      } else {
        authorialNoteEditor.isRemote = false;
      }
    },
    [authorialNoteEditor, editorOnFocus, mainEditor, authorialNoteRegistry],
  );

  const onSynopsisAuthorialNoteChangeHandler = useCallback(
    (index: number) => (newValue: Descendant[]) => {
      valueSynopsisAuthRefs.current[index] = newValue;
      if (!synopsisAuthorialNoteEditors || !synopsisEditorRegistries || !synopsisAuthorialNoteRegistries) return;
      if (!synopsisAuthorialNoteEditors[index].isRemote) {
        if (isFirefox) {
          const activeElement = document.activeElement;
          ReactEditor.focus(synopsisAuthorialNoteEditors[index].editor);
          (activeElement as HTMLElement).focus();
        } else {
          ReactEditor.focus(synopsisAuthorialNoteEditors[index].editor);
        }
        if (synopsisAuthorialNoteEditors[index].editor.operations.some((op) => op.type === 'set_selection')) {
          onSelectionChange(index + 1, true);
        }
        synopsisEditorRegistries[index].apply(synopsisAuthorialNoteEditors[index]);
      } else {
        synopsisAuthorialNoteEditors[index].isRemote = false;
      }
    },
    [synopsisAuthorialNoteEditors, editorOnFocus, synopsisAuthorialNoteRegistries, synopsisEditorRegistries],
  );

  const canSave: boolean = useMemo(() => {
    return (
      ((firstDocumentState.document?.state === DocumentStateType.DRAFT ||
        firstDocumentState.document?.state === DocumentStateType.BEREIT_FUER_BUNDESTAG) &&
        firstDocumentState.document.documentPermissions.hasWrite) ||
      documents.some(
        (document) =>
          (document.state === DocumentStateType.DRAFT ||
            firstDocumentState.document?.state === DocumentStateType.BEREIT_FUER_BUNDESTAG) &&
          document.documentPermissions.hasWrite,
      )
    );
  }, [firstDocumentState.document?.state, documents[0]?.state, isSynopsisVisible]);

  useEffect(() => {
    if (triedToDisableActive) {
      saveAllDocuments(comments, resetDynAenderungsvergleich);
    }
  }, [triedToDisableActive, comments]);

  useEffect(() => {
    if (deleteCommentIds.length > 0) {
      deleteCommentsRequest();
    }
  }, [deleteCommentIds]);

  useEffect(() => {
    if (deleteCommentsResponse.isLoading) return;
    if (deleteCommentsResponse.hasError) {
      displayMessage(t(`lea.messages.save.someError`), 'error');
      return;
    }
    if (deleteCommentsResponse.data) {
      const document = deleteCommentsResponse.data;
      dispatch(updateFirstDocument(document));
      fetchComments(firstDocumentState.document?.id || '');
      displayMessage(t('lea.messages.save.success'), 'success');
      activeElement &&
        setTimeout(() => {
          const el = window.document.querySelector(`[id="${activeElement.id}"]`);
          if (el) {
            el.scrollIntoView();
          }
        }, 200);
    }
  }, [deleteCommentsResponse]);

  const resetDynAenderungsvergleich = () => {
    toggleDynAenderungsvergleich();
    toggleTriedToDisableActive();
  };

  const saveAllDocuments = (allComments: CommentResponseDTO[], onSuccessFunction?: OnSuccessFunction) => {
    const deletedComments = allComments.filter((comment) => comment.deleted);
    if (deletedComments.length > 0) {
      saveSelection.current = null;
      setDeleteCommentIds(deletedComments.map((comment) => comment.commentId));
      return;
    }
    const firstDocumentInfo = firstDocumentState.document?.documentPermissions.hasWrite
      ? [
          {
            id: firstDocumentState.document?.id || '',
            content: JSON.stringify(valueRef.current),
            history: mainEditor.editor.history,
            isFirstDocument: true,
          } as DocumentInfo,
        ]
      : [];
    const synopsisDocumentsInfo = documents
      .map((document, index) => {
        if (document.documentPermissions.hasWrite) {
          return {
            id: document.id || '',
            content: JSON.stringify(valueSynopsisRefs.current[index]),
            history: synopsisEditors[index].editor.history,
            isFirstDocument: false,
          } as DocumentInfo;
        } else {
          return undefined;
        }
      })
      .filter((synopsisDocumentInfo) => synopsisDocumentInfo !== undefined);
    // .filter((_document, index) => documents[index].permission.hasWrite);
    const allDocumentsInfo = [...firstDocumentInfo, ...synopsisDocumentsInfo];
    allDocuments.updateDocuments(allDocumentsInfo, onSuccessFunction);
  };

  const onSaveHandler = useCallback(() => {
    const editor = getEditor({ index: editorOnFocus.index, isAuthorialNoteEditor: false });
    if (editor) {
      saveSelection.current = editor.selection;
    }
    saveAllDocuments(comments);
  }, [documents, firstDocumentState.document, editorOnFocus.index, comments]);

  const onSaveAndCloseSuccessHandler = () => {
    if (isSynopsisVisible) {
      setEditorOnFocus({ index: 0, isAuthorialNoteEditor: false });
      synopsisDocument.removeDocument();
    } else {
      window.location.hash = `#${basePath}`;
    }
  };

  const onSaveAndCloseHandler = useCallback(() => {
    saveAllDocuments(comments, onSaveAndCloseSuccessHandler);
  }, [documents, firstDocumentState.document, isSynopsisVisible, comments]);

  const onConfigureSynopsisHandler = useCallback(() => {
    saveAllDocuments(comments, openSynopsisWizard);
  }, [documents, firstDocumentState.document, comments]);

  const onExportHandler = useCallback(() => {
    exportDocument(valueRef.current, firstDocumentState.document);
  }, []);

  const onClose = () => {
    window.location.hash = `#${basePath}`;
  };

  return (
    <>
      <TopToolbar
        canSave={canSave}
        onExport={onExportHandler}
        onSave={onSaveHandler}
        onClose={onClose}
        setShowPrompt={setShowPrompt}
      />
      <div className="Slate" onKeyDown={keyDownHandler}>
        <Toolbar>
          <Toolbar.Left />
          <Toolbar.Middle ariaLabel={t('lea.toolbar.ariaLabel')}>
            <ToolbarFileTool />
            {synopsisRegistries.map((synopsisRegistry, index) => (
              <synopsisRegistry.Toolbar
                key={`${documents[index].id}-toolbar`}
                editor={synopsisEditors[index]?.editor}
              />
            ))}
            <Registry.Toolbar editor={mainEditor.editor} />
            <Divider className="lea-toolbar-divider" key={'ToolDividerViewTools'} type="vertical" />
            <ToolbarViewTool
              registry={editorOnFocus.index === 0 ? Registry : synopsisRegistries[editorOnFocus.index - 1]}
            />
            {synopsisRegistries.map((synopsisRegistry, index) => (
              <synopsisRegistry.ViewTools key={`${documents[index].id}-viewTools`} />
            ))}
            <Registry.ViewTools />

            <Divider className="lea-toolbar-divider" key={`ToolDividerInsert`} type="vertical" />
            <ToolbarInsertTool
              registry={editorOnFocus.index === 0 ? Registry : synopsisRegistries[editorOnFocus.index - 1]}
            />
            {synopsisRegistries.map((synopsisRegistry, index) => (
              <synopsisRegistry.InsertTools
                editor={synopsisEditors[index]?.editor}
                key={`${documents[index].id}-insertTools`}
              />
            ))}
            <Registry.InsertTools editor={mainEditor.editor} />
            <Divider className="lea-toolbar-divider" key={`ToolDividerLast`} type="vertical" />
            <SyopsisToolbarTool
              onClose={onSaveAndCloseHandler}
              onConfigure={onConfigureSynopsisHandler}
              canSave={canSave}
            />
          </Toolbar.Middle>
          <Toolbar.Right>{pkg.version}</Toolbar.Right>
        </Toolbar>
        <div className="EditableContainer">
          <div className="SlateSidebar Left">
            <TableOfContentsWrapper
              documentName={
                editorOnFocus.index === 0
                  ? (firstDocumentState.document?.title ?? '')
                  : (documents[editorOnFocus.index - 1]?.title ?? '')
              }
            >
              {synopsisRegistries.map((synopsisRegistry, index) => (
                <synopsisRegistry.NavigationDialogs
                  key={`${documents[index].id}-navigationDialog`}
                  editor={synopsisEditors[index]?.editor}
                />
              ))}
              <Registry.NavigationDialogs editor={mainEditor.editor} />
            </TableOfContentsWrapper>
          </div>
          <div className="emptyBox"></div>
          <div className={`EditableArea${isSynopsisVisible ? ' synopsis-visible' : ' no-synopsis'}`}>
            <div className={`DocumentEditable${isSynopsisVisible ? ' synopsis-visible' : ' no-synopsis'}`}>
              {firstDocumentState.document && (
                <EditorColumn
                  editor={mainEditor.editor}
                  value={valueRef.current}
                  registry={Registry}
                  index={0}
                  isResizing={isResizing}
                  setIsResizing={setIsResizing}
                  cellsVisible={cellsVisible}
                  document={firstDocumentState.document}
                  keyDownHandler={Registry.keyDownCallback(mainEditor.editor, isElementContentEditable)}
                  onContextMenu={onContextMenu}
                  onChange={onChangeHandler}
                  handleDragDrop={handleDragDrop}
                  handleClick={handleClick}
                  handleScroll={handleScroll(0)}
                  onPaste={Registry.pasteCallback(mainEditor.editor, isElementContentEditable)}
                  onCut={Registry.cutCallback(mainEditor.editor, isElementContentEditable)}
                  onBlur={Registry.blurCallback(mainEditor.editor)}
                />
              )}
              {isSynopsisVisible &&
                synopsisEditors &&
                synopsisRegistries &&
                synopsisEditors.length === valueSynopsisRefs.current.length &&
                synopsisEditors.map((synopsisEditor, index) => {
                  if (synopsisEditor) {
                    return (
                      <EditorColumn
                        key={synopsisEditor.id}
                        editor={synopsisEditor.editor}
                        value={valueSynopsisRefs.current[index]}
                        registry={synopsisRegistries[index]}
                        index={index + 1}
                        isResizing={isResizing}
                        setIsResizing={setIsResizing}
                        cellsVisible={cellsVisible}
                        document={documents[index]}
                        keyDownHandler={synopsisRegistries[index].keyDownCallback(
                          synopsisEditor.editor,
                          isElementContentEditable,
                        )}
                        onContextMenu={onContextMenu}
                        onChange={onSynopsisChangeHandler(index)}
                        handleDragDrop={handleDragDrop}
                        handleClick={handleClick}
                        handleScroll={handleScroll(index + 1)}
                        onPaste={synopsisRegistries[index].pasteCallback(
                          synopsisEditor.editor,
                          isElementContentEditable,
                        )}
                        onCut={synopsisRegistries[index].cutCallback(synopsisEditor.editor, isElementContentEditable)}
                        onBlur={Registry.blurCallback(synopsisEditor.editor)}
                      />
                    );
                  } else {
                    return <></>;
                  }
                })}
              {!isSynopsisVisible && <CommentPage baseEditor={mainEditor.editor} saveSelection={saveSelection} />}
            </div>

            <AuthorialNotePage
              mainEditor={authorialNoteEditor}
              editorIndex={0}
              synopsisEditors={synopsisAuthorialNoteEditors}
              value={valueAuthRef}
              valueSynopsis={valueSynopsisAuthRefs}
              registry={authorialNoteRegistry}
              synopsisRegistries={synopsisAuthorialNoteRegistries}
              onChange={onAuthorialNoteChangeHandler}
              onSynopsisChange={onSynopsisAuthorialNoteChangeHandler}
              handleClick={handleClick}
            />
            <ContextMenu
              editor={editorOnFocus.index === 0 ? mainEditor.editor : synopsisEditors[editorOnFocus.index - 1]?.editor}
              registry={editorOnFocus.index === 0 ? Registry : synopsisRegistries[editorOnFocus.index - 1]}
            />
            {synopsisRegistries.map((synopsisRegistry, index) => (
              <synopsisRegistry.ContextMenu
                key={`${documents[index].id}-contextMenu`}
                editorIndex={index + 1}
                editor={synopsisEditors[index]?.editor}
              />
            ))}
            <Registry.ContextMenu editor={mainEditor.editor} editorIndex={0} />
            <FormatAuthorialNoteWizard
              editor={editorOnFocus.index === 0 ? mainEditor.editor : synopsisEditors[editorOnFocus.index - 1]?.editor}
            />
            <SynchronousScrollingWizard />
            <NewVersionWizard userSettingsRef={userSettingsRef} />
            <SynopsisWizard
              documentInfo={{
                entity: EntityType.SINGLE_DOCUMENT,
                id: firstDocumentState.document?.id ?? '',
                title: firstDocumentState.document?.title ?? '',
                regulatoryProposalId: firstDocumentState.document?.proposition?.id,
              }}
            />
            <EgfaWizard compoundDocumentInfo={compoundDocumentInfo} />
            {/* <Registry.ActionDialogs /> */}
          </div>
          <div className="emptyBox"></div>
          <div className="SlateSidebar Right">
            <div className="optionsWrapper">
              <Registry.OptionDialogs editor={optionsDialogEditor} />
            </div>

            <RightToolbar />
          </div>
        </div>
      </div>
      <Registry.ModalDialogs />
    </>
  );
};
