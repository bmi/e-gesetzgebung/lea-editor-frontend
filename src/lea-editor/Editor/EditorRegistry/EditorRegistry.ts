// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseOperation } from 'slate';
import { Editor } from '../../../interfaces/Editor';

export class EditorRegistry {
  public constructor(private readonly editors: Editor[]) {}

  public apply(initialEditor: Editor) {
    const operations = initialEditor.editor.operations.filter((operation) => operation.type !== 'set_selection');
    if (operations.length > 0) {
      setTimeout(() => this.emitter(operations, initialEditor.id));
    }
  }

  private emitter(operations: BaseOperation[], id: string) {
    const externEditors = this.editors.filter((editor) => editor.id !== id);
    externEditors.forEach((externEditor) => {
      operations.forEach((operation) => {
        if (externEditor.editor.children.length > 0) {
          externEditor.isRemote = true;
          try {
            externEditor.editor.apply(JSON.parse(JSON.stringify(operation)) as BaseOperation);
          } catch (e) {}
        }
      });
    });
  }
}
