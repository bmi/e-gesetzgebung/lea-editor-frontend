// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createAction, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DataNode } from 'antd/lib/tree';

enum TableOfContentsDataActions {
  SetData = 'TableOfContents/Data/Set',
}

export const setTableOfContentsData = (editorDocumentIndex: number) =>
  createAction<DataNode[]>(`${editorDocumentIndex}/${TableOfContentsDataActions.SetData}`);

export const tableOfContentsDataEntityAdapter = createEntityAdapter<DataNode>({
  selectId: (treeData) => treeData.key.toString(),
});

export const TableOfContentsDataSlice = (editorDocumentIndex: number) =>
  createSlice({
    name: 'tableOfContentsData',
    initialState: tableOfContentsDataEntityAdapter.getInitialState(),
    reducers: {},
    extraReducers: (builder) =>
      builder.addCase(setTableOfContentsData(editorDocumentIndex), (state, action) => {
        if (tableOfContentsDataEntityAdapter.getSelectors().selectTotal(state) === action.payload.length) {
          tableOfContentsDataEntityAdapter.upsertMany(state, action.payload);
          if (tableOfContentsDataEntityAdapter.getSelectors().selectTotal(state) !== action.payload.length) {
            const deleteNodes = tableOfContentsDataEntityAdapter
              .getSelectors()
              .selectAll(state)
              .filter((node) => !action.payload.some((findNode) => findNode.key.toString() === node.key.toString()));
            tableOfContentsDataEntityAdapter.removeMany(
              state,
              deleteNodes.map((deleteNode) => deleteNode.key.toString()),
            );
          }
        } else {
          tableOfContentsDataEntityAdapter.setAll(state, action.payload);
        }
      }),
  });
