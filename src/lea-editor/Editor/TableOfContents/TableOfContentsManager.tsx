// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DataNode, EventDataNode } from 'antd/lib/tree';
import React, { FocusEvent, Key, useEffect, useRef, useState } from 'react';
import { useTableOfContentsState, useSelectionState } from '../../plugin-core';
import {
  useContextMenuChange,
  useContextMenuNodeEntryState,
  useContextMenuState,
} from '../../plugin-core/editor-store/workspace/contextMenu';

import { TocTree, AllowDropProps } from '@lea/ui';
import { Editor, Location, NodeEntry, Text, Transforms } from 'slate';
import { isContextMenuCommand, isEnter, getLastLocationAt, getElementGUIDFromSelection } from '../../plugin-core/utils';
import { ReactEditor } from 'slate-react';
import { NodeDragEventParams } from 'rc-tree/lib/contextTypes';
import { Element } from '../../../interfaces';
import { useTranslation } from 'react-i18next';

import './TableOfContentsManager.less';

interface ITableOfContentsManager {
  editor: ReactEditor;
  index: number;
  nodeDraggable?: (node: DataNode) => boolean;
  onDrop?: (
    info: NodeDragEventParams<DataNode, HTMLDivElement> & {
      dragNode: EventDataNode<DataNode>;
      dragNodesKeys: Key[];
      dropPosition: number;
      dropToGap: boolean;
    },
  ) => void;
  isReadonly?: boolean;
  tableOfContentsData: DataNode[];
  canAddChildren?: (dataNode: DataNode) => boolean;
  onDragging?: React.Dispatch<React.SetStateAction<boolean>>;
  isAvailable: boolean;
  documentTitle: string;
  allowDrop?: (a: AllowDropProps) => boolean;
}

interface ITreeRightClickEventInfo {
  event: React.MouseEvent<HTMLDivElement>;
  node: any;
}

export const jumpToPath = (editor: ReactEditor, key: string, index: number) => {
  const path = key.split('-').map((x) => parseInt(x, 10));
  const [node] = Editor.node(editor, path) as NodeEntry<Element>;
  const element = document.getElementById(node.GUID ? `${index}:${node.GUID}` : '');
  const firstEditableElement = element ? element.getElementsByClassName('contentEditable')[0] : null;
  if (firstEditableElement) {
    (firstEditableElement as HTMLElement).focus();
    const elementGUID = firstEditableElement.id.slice(-36); // GUID has always 36 characters
    const [firstEditableNode] = Editor.nodes(editor, {
      match: (node) => {
        return (node as Element).GUID === elementGUID;
      },
      at: [],
    });
    if (firstEditableNode) {
      const [, firstEditablePath] = firstEditableNode;
      Transforms.select(editor, getLastLocationAt(editor, firstEditablePath));
    }
  } else {
    // else statement comes to effect in read-only documents
    const [...leafs] = Editor.nodes(editor, { at: path, match: (n: any) => Text.isText(n) });

    if (leafs && leafs.length > 0) {
      const numPath = [...path, 0];
      Transforms.select(editor, getLastLocationAt(editor, numPath));
    }
  }
};

export const TableOfContentsManager = ({
  editor,
  index,
  onDrop,
  nodeDraggable,
  isReadonly,
  tableOfContentsData,
  onDragging,
  canAddChildren,
  isAvailable,
  documentTitle,
  allowDrop,
}: ITableOfContentsManager) => {
  const { t } = useTranslation();
  const { openContextMenu, setTargetNodeEntry } = useContextMenuChange();
  const { targetNodeEntry } = useContextMenuNodeEntryState();
  const contextMenuState = useContextMenuState();
  const { visible } = useTableOfContentsState();
  const { selection } = useSelectionState();
  const activeKeyRef = useRef<Key | null>(null);
  const [activeKey, setActiveKey] = useState<Key | null>(null);

  useEffect(() => {
    document.getElementById('theme-generic-lea-drawer-tree-input-0')?.addEventListener('focus', onFocus);
    return () => {
      document.getElementById('theme-generic-lea-drawer-tree-input-0')?.removeEventListener('focus', onFocus);
    };
  });

  useEffect(() => {
    if (!contextMenuState.isOpen) {
      setTimeout(() => document.activeElement?.id !== 'theme-generic-lea-drawer-tree-input-0' && setActiveKey(null));
    }
  }, [contextMenuState.isOpen]);

  const onBlur = (e?: FocusEvent) => {
    if ('contextMenu' === e?.relatedTarget?.id) {
      setActiveKey(activeKeyRef.current);
    }
  };

  const onFocus = () => {
    !activeKey && setActiveKey(tableOfContentsData[0].key);
  };

  const onActiveChange = (key: Key | null) => {
    if (!contextMenuState.isOpen) {
      if (key) {
        activeKeyRef.current = key;
      }
      setActiveKey(key);
    }
  };

  const renderTitle = (node: DataNode): React.ReactNode => {
    return <span id={`tree-node-${node.key}`}>{node.title as string}</span>;
  };

  const onSelect = (node: DataNode) => {
    jumpToPath(editor, node.key.toString(), index);
  };

  const onRightClick = (info: ITreeRightClickEventInfo) => {
    info.event.preventDefault();
    info.event.stopPropagation();
    if (isReadonly) return;
    const activeKey = (info.node as DataNode).key.toString();
    const path: Location = activeKey.split('-').map((x) => parseInt(x, 10));
    const nodeEntry = Editor.node(editor, path) as NodeEntry<Element>;
    onActiveChange(activeKey);
    openContextMenu({
      e: info.event,
      positionTarget: document.getElementById(`tree-node-${activeKey}`) as HTMLElement,
      focusTarget: document.getElementById('theme-generic-lea-drawer-tree-input-0') as HTMLElement,
      nodeEntry,
    });
  };

  const keyDownHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
    const event = e.nativeEvent;
    if (activeKey && !contextMenuState.isOpen && isEnter(event)) {
      jumpToPath(editor, activeKey.toString(), index);
    }
    if (isContextMenuCommand(event)) {
      e.preventDefault();
      e.stopPropagation();
      if (isReadonly) return;
      if (activeKey) {
        const path: Location = (activeKey as string).split('-').map((x) => parseInt(x, 10));
        const nodeEntry = Editor.node(editor, path) as NodeEntry<Element>;
        openContextMenu({
          e: event,
          positionTarget: document.getElementById(`tree-node-${activeKey}`) as HTMLElement,
          focusTarget: document.getElementById('theme-generic-lea-drawer-tree-input-0') as HTMLElement,
          nodeEntry,
        });
      }
    }
  };

  useEffect(() => {
    if (!visible && isAvailable) {
      onActiveChange(null);
      const elementGUID = getElementGUIDFromSelection(editor, selection);
      const element = document.getElementById(`${index}:${elementGUID}`);
      if (element && element.contentEditable === 'true') {
        element.focus();
      } else {
        const editorElement = document.getElementById(`editor-column-${index}`);
        editorElement && editorElement.focus();
      }
    }
  }, [visible]);

  useEffect(() => {
    if (targetNodeEntry) {
      const [, path] = targetNodeEntry;
      setTimeout(() => onActiveChange(path.join('-')));
      setTargetNodeEntry(null);
    }
  }, [tableOfContentsData]);

  const findNodeTitleById = (
    nodes: DataNode[],
    id: string,
  ): { node: DataNode; order: number; length: number } | null => {
    if (!nodes.length || !id) {
      return null;
    }
    const foundNode = nodes.find((node) => node.key === id);
    if (foundNode) {
      return { node: foundNode, order: nodes.indexOf(foundNode) + 1, length: nodes.length };
    } else {
      let foundSubNode = { node: {} as DataNode, order: 0, length: 0 };
      for (const element of nodes) {
        if (element.children) {
          const childrenNode = findNodeTitleById((element.children as DataNode[]) || [], id);
          if (childrenNode) {
            foundSubNode = childrenNode;
          }
        }
      }
      if (foundSubNode.length) {
        return foundSubNode;
      } else {
        return null;
      }
    }
  };

  const onKeyUp = () => {
    const accessibilityElement = document.querySelector('.lea-ui-toc-tree')?.querySelector('[aria-live="assertive"]');
    if (accessibilityElement) {
      const elementKeys = accessibilityElement?.textContent?.split('>');
      if (elementKeys) {
        const elementKey = elementKeys[elementKeys.length - 1].trim();
        const node = findNodeTitleById(tableOfContentsData, elementKey || '');
        let titleExtension = '';
        if (node?.node.children?.length || -1 > 0) {
          const domNode = document
            .querySelector('.lea-ui-toc-tree')
            ?.querySelector(`.ant-tree-node-content-wrapper-open:has(#tree-node-${elementKey})`);
          if (domNode) {
            titleExtension = ' erweitert';
          } else {
            titleExtension = ' erweiterbar';
          }
        }
        accessibilityElement.textContent = `${node?.node?.title as string}${titleExtension}`;
      }
    }
  };

  useEffect(() => {
    window.addEventListener('keyup', onKeyUp);
    return () => {
      window.removeEventListener('keyup', onKeyUp);
    };
  });

  return isAvailable ? (
    <TocTree
      ariaLabel={t('lea.tableOfContents.ariaLabel', { documentTitle })}
      treeData={tableOfContentsData ?? []}
      onSelect={onSelect}
      onRightClick={onRightClick}
      onFocus={onFocus}
      onBlur={onBlur}
      open={visible}
      onDrop={onDrop}
      nodeDraggable={nodeDraggable}
      allowDrop={allowDrop}
      onActiveChange={onActiveChange}
      activeKey={activeKey}
      titleRender={renderTitle}
      onKeyDown={keyDownHandler}
      onDragging={onDragging}
      canAddChildren={canAddChildren}
    />
  ) : (
    <></>
  );
};
