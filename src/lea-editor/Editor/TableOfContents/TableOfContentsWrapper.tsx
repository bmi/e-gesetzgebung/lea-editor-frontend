// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useRef } from 'react';
import { useTableOfContentsDispatch, useTableOfContentsState } from '../../plugin-core';
import './TableOfContentsWrapper.less';

import { AriaController, LeaDrawer } from '@lea/ui';
import { AriaController as AriaControllerPlateg } from '@plateg/theme';
import { useTranslation } from 'react-i18next';

interface ITableOfContentsWrapper {
  documentName: string;
  children: React.ReactNode;
}

export const TableOfContentsWrapper = ({ children, documentName }: ITableOfContentsWrapper) => {
  const { t } = useTranslation();
  const { closeTableOfContents } = useTableOfContentsDispatch();
  const { visible } = useTableOfContentsState();
  const tableOfContentRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (tableOfContentRef.current && visible) {
      AriaControllerPlateg.setAriaLabelsByQuery(
        '.table-of-contents-drawer .ant-drawer-close',
        'Strukturanzeige schließen',
      );
      AriaController.setElementIdByClassNameInElement(
        'ant-drawer-close',
        'lea-drawer-tableOfContent-close-button',
        tableOfContentRef.current,
      );
    }
  }, [visible, tableOfContentRef]);

  return (
    <LeaDrawer
      className="table-of-contents-drawer"
      placement={'left'}
      open={visible}
      onClose={closeTableOfContents}
      title={t('lea.tableOfContents.header', { title: documentName })}
      expandable={true}
      refProp={tableOfContentRef}
      ariaLabel={t('lea.tableOfContents.ariaLabel', { documentTitle: documentName })}
    >
      {children}
    </LeaDrawer>
  );
};
