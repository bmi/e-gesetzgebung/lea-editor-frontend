// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EntityState } from '@reduxjs/toolkit';
import { DataNode } from 'antd/lib/tree';
import { useCallback } from 'react';
import { EqualityFn } from 'react-redux';
import { useAppDispatch, useAppSelector } from '../../../components/Store';
import { EditorPageState, PluginState } from '../../plugin-core';
import {
  setTableOfContentsData,
  tableOfContentsDataEntityAdapter,
  TableOfContentsDataSlice,
} from './TableOfContentsDataSlice';

type UseTableOfContentsDataState = {
  tableOfContentsData: DataNode[];
};

type UseTableOfContentsDataDispatch = {
  setTableOfContentsData: (dataNodes: DataNode[]) => void;
};

const equalFunc: EqualityFn<DataNode[]> = (a, b) => {
  if (a.length !== b.length) {
    return false;
  } else if (a.length === 0 && b.length === 0) {
    return true;
  }
  if (
    a.some((aChild, index) => {
      const bChild: DataNode = b[index];
      const childEqual =
        aChild.children && bChild.children
          ? equalFunc(aChild.children, bChild.children)
          : !aChild.children && !bChild.children;
      return (
        aChild.key != bChild.key || aChild.title != bChild.title || aChild.className != bChild.className || !childEqual
      );
    })
  ) {
    return false;
  }
  return true;
};

export const useTableOfContentsDataState = (editorDocumentIndex: number): UseTableOfContentsDataState => {
  const tableOfContentsData = useAppSelector<DataNode[]>(
    (state) =>
      tableOfContentsDataEntityAdapter
        .getSelectors()
        .selectAll(
          (((state.editorPage as EditorPageState)?.pluginState?.[editorDocumentIndex] as PluginState)?.[
            TableOfContentsDataSlice(editorDocumentIndex).name
          ] as EntityState<DataNode>) || tableOfContentsDataEntityAdapter.getInitialState(),
        ),
    equalFunc,
  );

  return {
    tableOfContentsData,
  };
};

export const useTableOfContentsDataDispatch = (editorDocumentIndex: number): UseTableOfContentsDataDispatch => {
  const dispatch = useAppDispatch();

  const setTableOfContentsDataCallback = useCallback((dataNodes: DataNode[]) => {
    const action = setTableOfContentsData(editorDocumentIndex);
    dispatch(action(dataNodes));
  }, []);

  return {
    setTableOfContentsData: setTableOfContentsDataCallback,
  };
};
