// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useState } from 'react';
import { Menu } from 'antd';

import './ContextMenu.scss';
import {
  useContextMenuChange,
  useContextMenuNodeEntryState,
  useContextMenuState,
} from '../../plugin-core/editor-store/workspace/contextMenu';
import {
  isContextMenuCommand,
  isEnter,
  isEscape,
  isLeftNavigation,
  isRightNavigation,
  isTabOrTabBack,
} from '../../plugin-core/utils';
import { ReactEditor } from 'slate-react';
import { IconDirectionRightOutlinedBlack } from '@lea/ui/src/Icon/components';
import { PluginRegistry } from '../../plugin-core';
import { ItemType } from 'antd/lib/menu/interface';

interface IContextMenuStyle {
  top: number | string;
  left: number | string;
  opacity?: number;
}

interface IProps {
  editor: ReactEditor;
  registry: PluginRegistry;
}

const InitContextMenuStyle: IContextMenuStyle = {
  top: '0px',
  left: '0px',
  opacity: 0,
};

export const ContextMenu = ({ registry }: IProps): ReactElement => {
  const { isOpen, position, targetId } = useContextMenuState();
  const { closeContextMenu, openContextMenu } = useContextMenuChange();
  const { nodeEntry } = useContextMenuNodeEntryState();
  const [menuVisible, setMenuVisible] = useState(false);
  const [menu, setMenu] = useState<HTMLDivElement | null>(null);
  const [openKeys, setOpenKeys] = useState<string[]>([]);
  const [subMenuVisible, setSubMenuVisible] = useState(false);
  const [items, setItems] = useState<ItemType[]>([]);
  const [style, setStyle] = useState<IContextMenuStyle>(
    position
      ? {
          top: position.yPos,
          left: position.xPos,
          opacity: 0,
        }
      : InitContextMenuStyle,
  );

  useEffect(() => {
    if (position && menuVisible && items.length > 0) {
      const { xPos, yPos } = position;
      setTimeout(() => {
        setStyle(menu ? getMenuPosition(menu, parseInt(xPos), parseInt(yPos)) : InitContextMenuStyle);
      });
    } else {
      setStyle(InitContextMenuStyle);
    }
  }, [items, position]);

  useEffect(() => {
    if (menuVisible && menu && style.opacity === 1) {
      document.getElementById('contextMenu')?.focus();
    }
  }, [style]);

  useEffect(() => {
    document.addEventListener('mouseup', closeContextMenu);
    document.addEventListener('keydown', contextMenuKeyDownHandler);
    return () => {
      document.removeEventListener('mouseup', closeContextMenu);
      document.removeEventListener('keydown', contextMenuKeyDownHandler);
    };
  });

  const contextMenuKeyDownHandler = (event: KeyboardEvent) => {
    if (isContextMenuCommand(event)) {
      if (isOpen) {
        closeContextMenu();
        const element = document.getElementById(targetId);
        setTimeout(() => element?.focus());
      } else {
        event.preventDefault();
        event.stopPropagation();
        openContextMenu({ e: event });
      }
    }
  };

  const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
    const event = e.nativeEvent;
    if (isTabOrTabBack(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (isEscape(event) && !subMenuVisible) {
      closeContextMenu();
      const element = document.getElementById(targetId);
      setTimeout(() => element?.focus());
    } else if (isLeftNavigation(event) || isRightNavigation(event) || isEnter(event) || isEscape(event)) {
      changeSubMenuOpen();
    }
  };

  const selectionHandler = () => {
    closeContextMenu();
    const element = document.getElementById(targetId);
    setTimeout(() => element?.focus());
  };

  const changeSubMenuOpen = () => {
    if (menu) {
      if (menu.getElementsByClassName('ant-menu-submenu-open').length > 0) {
        setSubMenuVisible(true);
      } else {
        setSubMenuVisible(false);
      }
    }
  };

  const initMenu = () => {
    setMenuVisible(true);
    setStyle(InitContextMenuStyle);
    setItems([]);
    setOpenKeys([]);
    setTimeout(() => registry && setItems(registry.getContextMenuItemTypes()));
  };

  useEffect(() => {
    if (!isOpen) {
      setMenuVisible(false);
    } else {
      initMenu();
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen) {
      setMenuVisible(false);
      setTimeout(() => {
        initMenu();
      });
    }
  }, [nodeEntry]);

  return menuVisible ? (
    <div
      role="toolbar"
      style={style}
      className="menu-container"
      ref={(r) => setMenu(r)}
      onContextMenu={(event: React.MouseEvent<HTMLDivElement>) => event.preventDefault()}
      aria-hidden={!menuVisible}
    >
      <Menu
        id="contextMenu"
        mode="vertical"
        expandIcon={<IconDirectionRightOutlinedBlack />}
        onSelect={selectionHandler}
        onKeyDown={keyDownHandler}
        items={items}
        selectedKeys={[]}
        onOpenChange={(openKeys) => setOpenKeys(openKeys)}
        onMouseUp={(e) => {
          e.preventDefault();
          e.stopPropagation();
        }}
        openKeys={openKeys}
      />
    </div>
  ) : (
    <></>
  );
};

function getMenuPosition(menu: HTMLDivElement, x = 0, y = 0) {
  const menuStyles = {
    top: y,
    left: x,
    opacity: 1,
  };

  const { innerWidth, innerHeight } = window;
  const innerHeightWithoutFooter = innerHeight - 80;
  const rect = menu.getBoundingClientRect();

  if (y + rect.height > innerHeightWithoutFooter) {
    menuStyles.top -= rect.height + 5;
  } else {
    menuStyles.top += 5;
  }

  if (x + rect.width > innerWidth) {
    menuStyles.left -= rect.width + 5;
  } else {
    menuStyles.left += 5;
  }

  if (menuStyles.top < 0) {
    menuStyles.top = rect.height < innerHeightWithoutFooter ? (innerHeightWithoutFooter - rect.height) / 2 : 0;
  }

  if (menuStyles.left < 0) {
    menuStyles.left = rect.width < innerWidth ? (innerWidth - rect.width) / 2 : 0;
  }

  if (document.location.hostname.includes('plattform')) {
    menuStyles.left = menuStyles.left - 64;
    menuStyles.top = menuStyles.top - 64;
  }

  return menuStyles;
}
