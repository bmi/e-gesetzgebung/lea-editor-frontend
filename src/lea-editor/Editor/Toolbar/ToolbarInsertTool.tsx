// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ToolbarDropdown } from '@lea/ui';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import {
  PluginRegistry,
  useEditorDocumentState,
  useEditorOnFocusState,
  useDocumentHasPreambelState,
  useSynopsisInfo,
} from '../../plugin-core';
import { ItemType } from 'antd/lib/menu/interface';
import { isEscape, isTab } from '../../plugin-core/utils';
interface IProps {
  registry: PluginRegistry;
}
export const ToolbarInsertTool = ({ registry }: IProps): ReactElement => {
  const [isActive, onActive] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const [items, setItems] = useState<ItemType[]>([]);
  const { isSynopsisVisible } = useSynopsisInfo();
  const { index } = useEditorOnFocusState();
  const documentHasPreambel = useDocumentHasPreambelState(index);
  const { editorDocument } = useEditorDocumentState(index);

  const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
    const event = e.nativeEvent;
    if (isTab(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (isEscape(event)) {
      setTimeout(() => buttonRef.current?.focus());
    }
  };

  useEffect(() => {
    registry && setItems(registry.getInsertToolItemTypes());
  }, [documentHasPreambel, isSynopsisVisible, editorDocument]);
  return (
    <ToolbarDropdown
      id="insertToolToolbar-dropdown"
      items={items}
      keyDownHandler={keyDownHandler}
      buttonRef={buttonRef}
      isActive={isActive}
      onActive={onActive}
      onClick={() => onActive(false)}
      selectable={false}
      disabled={items.length === 0}
      header="Einfügen"
    />
  );
};
