// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ToolbarDropdown } from '@lea/ui';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import {
  PluginRegistry,
  useAuthorialNotePageState,
  useDynAenderungsvergleichState,
  useEditorDocumentState,
  useEditorOnFocusState,
  useSatzZaehlungState,
  useSynopsisInfo,
  useTableOfContentsState,
} from '../../plugin-core';
import { ItemType } from 'antd/lib/menu/interface';
import { useCommentPageState } from '../../plugin-core/editor-store/workspace/comment-page';
import { isEscape, isTab } from '../../plugin-core/utils';
interface IProps {
  registry: PluginRegistry;
}
export const ToolbarViewTool = ({ registry }: IProps): ReactElement => {
  const [isActive, onActive] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const [items, setItems] = useState<ItemType[]>([]);
  const { visible: tableofContent } = useTableOfContentsState();
  const { visible: authorialNote } = useAuthorialNotePageState();
  const { visible: comments } = useCommentPageState();
  const { isActive: dynAenderungsvergleich } = useDynAenderungsvergleichState();
  const { isSynopsisVisible } = useSynopsisInfo();
  const { index } = useEditorOnFocusState();
  const { editorDocument } = useEditorDocumentState(index);
  const satzZaehlungVisible = useSatzZaehlungState();

  const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
    const event = e.nativeEvent;
    if (isTab(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (isEscape(event)) {
      setTimeout(() => buttonRef.current?.focus());
    }
  };

  useEffect(() => {
    registry && setItems(registry.getViewToolItemTypes());
  }, [
    tableofContent,
    authorialNote,
    comments,
    isSynopsisVisible,
    editorDocument,
    dynAenderungsvergleich,
    satzZaehlungVisible,
  ]);
  return (
    <ToolbarDropdown
      id="viewToolToolbar-dropdown"
      items={items}
      keyDownHandler={keyDownHandler}
      buttonRef={buttonRef}
      isActive={isActive}
      onActive={onActive}
      selectable={false}
      header="Ansicht"
    />
  );
};
