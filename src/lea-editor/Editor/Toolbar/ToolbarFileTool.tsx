// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, useRef, FunctionComponent, useEffect, useCallback } from 'react';

import { ToolbarDropdown } from '@lea/ui';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentService, DocumentStateType, EgfaModulStatus, useFirstDocumentState } from '../../../api';
import { ItemType } from 'antd/lib/menu/interface';
import { isEscape, isTab } from '../../plugin-core/utils';
import { useDocumentsTableActionDispatch } from '../../../components/Pages/useDocumentsTableAction';

export const ToolbarFileTool: FunctionComponent = () => {
  const { document } = useFirstDocumentState();
  const [isActive, onActive] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const isCreateCopyDisabled = document?.state !== DocumentStateType.FREEZE;
  const isGfaDisabled = !document?.documentPermissions.hasWrite || document?.state !== DocumentStateType.DRAFT;
  const [egfaInfoResponse, fetchEgfaInfo] = CompoundDocumentService.getEgfaData(document?.compoundDocumentId ?? '');
  const [documentHasEgfaData, setDocumentHasEgfaData] = useState(false);
  const [openKeys, setOpenKeys] = useState<string[]>([]);
  const { onNewVersion, onEgfa } = useDocumentsTableActionDispatch();

  const { t } = useTranslation();

  const handleCreateCopy = () => {
    if (isCreateCopyDisabled) return;
    onActive(false);
    onNewVersion({
      id: document.compoundDocumentId ?? '',
      rvId: document.proposition?.id,
      compoundDocumentState: document.state,
    });
  };
  const handleImportEgfaData = () => {
    onEgfa({ id: document?.compoundDocumentId ?? '', compoundDocumentState: document?.state });
  };

  useEffect(() => {
    if (document?.compoundDocumentId) {
      fetchEgfaInfo();
    }
  }, [document?.compoundDocumentId]);

  useEffect(() => {
    if (!egfaInfoResponse.isLoading && !egfaInfoResponse.hasError && egfaInfoResponse.data) {
      const hasEgfaData = egfaInfoResponse.data.some(
        (modulInfo) =>
          modulInfo.egfaDataStatus === EgfaModulStatus.MODUL_DATA_NEW_UPDATE ||
          modulInfo.egfaDataStatus === EgfaModulStatus.MODUL_NO_NEWER_DATA,
      );
      setDocumentHasEgfaData(hasEgfaData);
    }
  }, [egfaInfoResponse]);

  const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
    const event = e.nativeEvent;
    if (isTab(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (isEscape(event)) {
      setTimeout(() => buttonRef.current?.focus());
    }
  };

  const setActive = useCallback((active: boolean) => {
    !active && setOpenKeys([]);
    setTimeout(() => onActive(active), 10);
  }, []);

  const getItemTypes = (): ItemType[] => {
    return [
      {
        key: 'lea-create-new-version-of-dokumentenmappe',
        title: t('lea.toolbar.labels.file.createNewVersion'),
        label: t('lea.toolbar.labels.file.createNewVersion'),
        onClick: () => handleCreateCopy(),
        disabled: isCreateCopyDisabled,
      },
      {
        key: 'lea-egfa-submenu',
        label: t('lea.toolbar.labels.file.egfa.title'),
        title: t('lea.toolbar.labels.file.egfa.title'),
        popupClassName: 'lea-ui-toolbar-dropdown-submenu-children',
        disabled: isGfaDisabled,
        children: [
          {
            key: 'lea-import-egfa-data',
            title: t('lea.toolbar.labels.file.egfa.import'),
            label: t('lea.toolbar.labels.file.egfa.import'),
            onClick: () => handleImportEgfaData(),
            disabled: documentHasEgfaData,
          },
          {
            key: 'lea-update-egfa-data',
            title: t('lea.toolbar.labels.file.egfa.update'),
            label: t('lea.toolbar.labels.file.egfa.update'),
            onClick: () => handleImportEgfaData(),
            disabled: !documentHasEgfaData,
          },
        ],
      },
    ];
  };

  return (
    <ToolbarDropdown
      id="fileToolbar-dropdown"
      keyDownHandler={keyDownHandler}
      items={getItemTypes()}
      isActive={isActive}
      onActive={setActive}
      buttonRef={buttonRef}
      header="Datei"
      openKeys={openKeys}
      setOpenKeys={(keys) => setOpenKeys(keys)}
      selectable={false}
      arrowIconVisible
    />
  );
};
