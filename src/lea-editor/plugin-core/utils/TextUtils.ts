// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import {
  getLowestNodeAt,
  hasNextNeighborsOtherThanMarker,
  hasOnlyNextNeighborsOfTypeDeletedWrapper,
  hasPreviousNeighbor,
} from './NodeUtils';

export const isFirstPosition = (editor: Editor): boolean => {
  if (hasPreviousNeighbor(editor)) return false;
  return isFirstTextPosition(editor);
};

export const isLastPosition = (editor: Editor): boolean => {
  const node = getLowestNodeAt(editor);
  const text = node?.text ?? '';
  if (hasNextNeighborsOtherThanMarker(editor)) {
    if (hasOnlyNextNeighborsOfTypeDeletedWrapper(editor)) {
      return isLastTextPosition(editor, text);
    }
    return false;
  }
  return isLastTextPosition(editor, text);
};

export const isFirstTextPosition = (editor: Editor): boolean => {
  return !!editor.selection && editor.selection.anchor.offset === 0;
};

export const isLastTextPosition = (editor: Editor, text: string): boolean => {
  return !!editor.selection && editor.selection.anchor.offset === text.length;
};
