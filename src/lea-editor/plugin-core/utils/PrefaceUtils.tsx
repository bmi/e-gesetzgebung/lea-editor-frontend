// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Location, NodeEntry, Transforms } from 'slate';
import { ReactEditor } from 'slate-react';
import { DocumentType } from '../../../api';
import { DocumentPrefaceProps, Element } from '../../../interfaces';
import { STRUCTURE_ELEMENTS } from './StructureElements';

export const changePreface = (
  editor: ReactEditor,
  { proponent, abbreviation, title, shortTitle, type }: DocumentPrefaceProps,
) => {
  if (type === DocumentType.ANLAGE) {
    // Currently documents of type Anlage have no preface.
    // However, this might change in the future
    return;
  }
  const selection = editor.selection;
  replaceDocumentTitle(editor, title);
  replaceDocumentProponent(editor, proponent, type);
  replaceDocumentShortTitle(editor, shortTitle, type);
  replaceDocumentAbbreviation(editor, abbreviation);
  if (type === DocumentType.ANSCHREIBEN_STAMMGESETZ || type === DocumentType.ANSCHREIBEN_MANTELGESETZ) {
    replaceDocumentDocStage(editor, 'Gesetzentwurf');
  }
  if (proponent.toLowerCase() === 'bundesregierung') {
    replaceDocumentArticle(editor);
  }
  selection && Transforms.select(editor, selection as Location);
};

/**
 * The purpose of the function is to replace the abbreviation
 * within the preface of the document with the passed value.
 */
const replaceDocumentAbbreviation = (editor: ReactEditor, value: string): void => {
  /** 1. Search the node with the type: akn:preface */
  const [prefaceNodes] = Editor.nodes(editor, {
    at: [],
    match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE,
  });

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes?.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [, prefaceNodePath] = prefaceNodes;

  /** 4. Search for the node that contains the abbreviation.. */
  const [abbreviationNodes] = Editor.nodes(editor, {
    at: prefaceNodePath,
    match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.INLINE,
  });

  /** 5. exit the function if no results are available  */
  if (!abbreviationNodes || !abbreviationNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [, abbreviationNodePath] = abbreviationNodes as NodeEntry<Element>;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  Transforms.insertText(editor, value, { at: abbreviationNodePath });
};

/**
 * The purpose of the function is to replace the proponent
 * within the preface of the document with the passed value.
 */
const replaceDocumentProponent = (editor: ReactEditor, value: string, documentType?: DocumentType): void => {
  /** 1. Search the node with the type: akn:preface */
  const prefaceNodes = Array.from(
    Editor.nodes(editor, { at: [], match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE }),
  );

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, prefaceNodePath]] = prefaceNodes;

  /** 4. Search for the node that contains the proponent.. */
  const proponentNodes = Array.from(
    Editor.nodes(editor, {
      at: prefaceNodePath,
      match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.DOC_PROPONENT,
    }),
  );

  /** 5. exit the function if no results are available  */
  if (!proponentNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, proponentNodePath]] = proponentNodes;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  if (documentType?.includes('ANSCHREIBEN')) {
    Transforms.insertText(editor, value.slice(0, 1) + value.slice(1).toLowerCase(), { at: proponentNodePath });
  } else {
    Transforms.insertText(editor, value, { at: proponentNodePath });
  }
};

/**
 * The purpose of the function is to replace the title
 * within the preface of the document with the passed value.
 */
const replaceDocumentShortTitle = (editor: ReactEditor, value: string, documentType?: DocumentType): void => {
  /** 1. Search the node with the type: akn:preface */
  const prefaceNodes = Array.from(
    Editor.nodes(editor, { at: [], match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE }),
  );

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, prefaceNodePath]] = prefaceNodes;

  /** 4. Search for the node that contains the Title.. */
  const titleNodes = Array.from(
    Editor.nodes(editor, { at: prefaceNodePath, match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.SHORT_TITLE }),
  );

  /** 5. exit the function if no results are available  */
  if (!titleNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, titleNodePath]] = titleNodes;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  if (documentType === DocumentType.ANSCHREIBEN_STAMMGESETZ || documentType === DocumentType.ANSCHREIBEN_MANTELGESETZ) {
    Transforms.insertText(editor, value, { at: [...titleNodePath] });
  } else {
    Transforms.insertText(editor, value, { at: [...titleNodePath, 0] });
  }
};

/**
 * The purpose of the function is to replace the title
 * within the preface of the document with the passed value.
 */
const replaceDocumentTitle = (editor: ReactEditor, value: string): void => {
  /** 1. Search the node with the type: akn:preface */
  const prefaceNodes = Array.from(
    Editor.nodes(editor, { at: [], match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE }),
  );

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, prefaceNodePath]] = prefaceNodes;

  /** 4. Search for the node that contains the Title.. */
  const titleNodes = Array.from(
    Editor.nodes(editor, { at: prefaceNodePath, match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.DOCTITLE }),
  );

  /** 5. exit the function if no results are available  */
  if (!titleNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, titleNodePath]] = titleNodes;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  Transforms.insertText(editor, value, { at: titleNodePath });
};

const replaceDocumentDocStage = (editor: ReactEditor, value: string): void => {
  /** 1. Search the node with the type: akn:preface */
  const prefaceNodes = Array.from(
    Editor.nodes(editor, { at: [], match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE }),
  );

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, prefaceNodePath]] = prefaceNodes;

  /** 4. Search for the node that contains the Title.. */
  const titleNodes = Array.from(
    Editor.nodes(editor, { at: prefaceNodePath, match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.DOC_STAGE }),
  );

  /** 5. exit the function if no results are available  */
  if (!titleNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, titleNodePath]] = titleNodes;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  Transforms.insertText(editor, value, { at: titleNodePath });
};

const replaceDocumentArticle = (editor: ReactEditor): void => {
  /** 1. Search the node with the type: akn:preface */
  const prefaceNodes = Array.from(
    Editor.nodes(editor, { at: [], match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.PREFACE }),
  );

  /** 2. exit the function if no results are available  */
  if (!prefaceNodes.length) return;

  /**
   * 3. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, prefaceNodePath]] = prefaceNodes;

  /** 4. Search for the node that contains the Title.. */
  const artikelNodes = Array.from(
    Editor.nodes(editor, { at: prefaceNodePath, match: (n) => (n as Element).refersTo === 'artikel' }),
  );

  /** 5. exit the function if no results are available  */
  if (!artikelNodes.length) return;

  /**
   * 6. Get the path to the first node.
   * In our documents there will normally be only one node.
   */
  const [[, artikelNodePath]] = artikelNodes;

  /**
   * 7. Finally, replace the text with the passed value.
   */
  Transforms.insertText(editor, ' der ', { at: artikelNodePath });
};
