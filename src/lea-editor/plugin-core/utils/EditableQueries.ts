// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import {
  BegruendungAbsatzQueries,
  BegruendungAbsatzListeQueries,
  HeaderQueries as BegruendungHeaderQueries,
} from '../../plugins/begruendung';
import { TabelleQueries } from '../generic-plugins/tabelle';
import {
  JuristischerAbsatzQueries,
  ListeMitUntergliederungQueries,
  HeaderQueries as StammformHeaderQueries,
} from '../../plugins/regelungstext/stammform';
import * as PreambelPlugin from '../../plugins/regelungstext/preambel';
import {
  AenderungsbefehleQueries,
  HeaderQueries as MantelformHeaderQueries,
} from '../../plugins/regelungstext/mantelform';
import { VorblattAbsatzQueries, HeaderQueries as VorblattHeaderQueries } from '../../plugins/vorblatt';
import { AnschreibenAbsatzQueries } from '../../plugins/anschreiben';
import { AnlageAbsatzQueries } from '../../plugins/anlage';
import { DocumentType } from '../../../api';
import { AuthorialNotePageQueries } from '../../Editor/AuthorialNote/plugins';

export class EditableQueries {
  public static isEditable(editor: Editor, documentType: DocumentType, isAuthorialNoteEditor: boolean): boolean {
    if (!isAuthorialNoteEditor) {
      switch (documentType) {
        case DocumentType.BEGRUENDUNG_STAMMGESETZ:
        case DocumentType.BEGRUENDUNG_MANTELGESETZ:
        case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
        case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
          return (
            BegruendungAbsatzQueries.isEditable(editor) ||
            BegruendungAbsatzListeQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            BegruendungHeaderQueries.isEditable(editor)
          );
        case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
          return (
            JuristischerAbsatzQueries.isEditable(editor) ||
            ListeMitUntergliederungQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            StammformHeaderQueries.isEditable(editor) ||
            (PreambelPlugin.GesetzPraeambelQueries.isEditable(editor) &&
              !PreambelPlugin.GesetzPraeambelQueries.isFormula(editor))
          );
        case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
          return (
            AenderungsbefehleQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            MantelformHeaderQueries.isEditable(editor) ||
            (PreambelPlugin.GesetzPraeambelQueries.isEditable(editor) &&
              !PreambelPlugin.GesetzPraeambelQueries.isFormula(editor))
          );
        case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
          return (
            JuristischerAbsatzQueries.isEditable(editor) ||
            ListeMitUntergliederungQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            StammformHeaderQueries.isEditable(editor) ||
            (PreambelPlugin.VerordnungPraeambelQueries.isEditable(editor) &&
              !PreambelPlugin.VerordnungPraeambelQueries.isFormula(editor))
          );
        case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
          return (
            AenderungsbefehleQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            MantelformHeaderQueries.isEditable(editor) ||
            (PreambelPlugin.VerordnungPraeambelQueries.isEditable(editor) &&
              !PreambelPlugin.VerordnungPraeambelQueries.isFormula(editor))
          );
        case DocumentType.VORBLATT_STAMMGESETZ:
        case DocumentType.VORBLATT_MANTELGESETZ:
        case DocumentType.VORBLATT_STAMMVERORDNUNG:
        case DocumentType.VORBLATT_MANTELVERORDNUNG:
          return (
            VorblattAbsatzQueries.isEditable(editor) ||
            TabelleQueries.isEditable(editor) ||
            VorblattHeaderQueries.isEditable(editor)
          );
        case DocumentType.ANSCHREIBEN_STAMMGESETZ:
        case DocumentType.ANSCHREIBEN_MANTELGESETZ:
        case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
        case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
          return AnschreibenAbsatzQueries.isEditable(editor);
        case DocumentType.ANLAGE:
          return AnlageAbsatzQueries.isEditable(editor);
        default:
          return false;
      }
    } else {
      return AuthorialNotePageQueries.isEditable(editor) && this.allSelectedElementsAreContentEditable();
    }
  }
  private static allSelectedElementsAreContentEditable = () => {
    const selection = window.getSelection();
    if (selection?.rangeCount) {
      const range = selection.getRangeAt(0);
      const clonedSelection = range.cloneContents();
      const div = document.createElement('div');
      div.appendChild(clonedSelection);
      if (div.querySelector('[contenteditable="false"]') === null) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };
}
