// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseText, Editor, NodeEntry, Path, Transforms } from 'slate';
import { getLowestNodeEntryAt, getParentElement } from './NodeUtils';
import { Element } from '../../../interfaces';

export const trimSpaces = (editor: Editor, target: EventTarget) => {
  if (preventTrim(target)) return;
  const parent = getParentElement(editor);

  const [selectedElement, selectedElementPath] = Editor.above(editor, {
    match: (n) => !!parent && (n as Element).type === parent.type,
  }) as NodeEntry<Element>;

  for (let i = 0; i < selectedElement.children.length; i++) {
    const node = getLowestNodeEntryAt(editor, [...selectedElementPath, i]);
    if (node) {
      const [nodeEntry, path] = node;
      const text = nodeEntry?.text;
      trimText(editor, text, nodeEntry, path);
    }
  }
};

const trimText = (editor: Editor, text: string, nodeEntry: BaseText, path: Path) => {
  if (text && nodeEntry) {
    const shouldTrim = text.startsWith(' ') || text.endsWith(' ');
    const findings = [...text.matchAll(/\s{2,}/g)];
    for (let i = findings.length - 1; i >= 0; i--) {
      text = replaceFinding(text, findings[i]);
    }
    if (findings.length > 0 || shouldTrim) {
      text = text.trim();
      Transforms.insertText(editor, text, { at: path, voids: true });
    }
  }
};

const replaceFinding = (text: string, finding: RegExpMatchArray) => {
  if (finding.index) {
    const firstText = text.substring(0, finding.index);
    const lastText = text.substring(finding.index + finding[0].length);
    if (finding[0].includes('\xa0')) {
      return firstText.concat('\xa0', lastText);
    } else {
      return firstText.concat(' ', lastText);
    }
  }
  return text;
};

const preventTrim = (target: EventTarget) => {
  const targetId = (target as HTMLElement)?.id;
  switch (targetId) {
    case 'contextMenu':
    case 'authorial-dropdown-button':
    case 'undo-button':
    case 'redo-button':
      return true;
    default:
      return false;
  }
};
