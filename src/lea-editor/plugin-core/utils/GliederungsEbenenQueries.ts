// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export type GliederungsEbene = {
  type: string;
  title: string;
  dependingOn?: string;
  isEinzelvorschrift?: boolean;
};
export class GliederungsEbenenQueries {
  private _gliederungsEbenen: GliederungsEbene[];

  public constructor(gliederungsEbenenProps: GliederungsEbene[]) {
    this._gliederungsEbenen = gliederungsEbenenProps;
  }

  public get gliederungsEbenen(): GliederungsEbene[] {
    return this._gliederungsEbenen;
  }

  public getGliederungsEbeneFromType(type: string): GliederungsEbene | undefined {
    return this.gliederungsEbenen.find((gliederungsEbene) => gliederungsEbene.type === type);
  }

  public getEinzelvorschrift(): GliederungsEbene | undefined {
    return this.gliederungsEbenen.find((gliederungsEbene) => gliederungsEbene.isEinzelvorschrift);
  }

  public getOptionalGliederungsEbenenBetween(startType: string, endType: string): GliederungsEbene[] {
    const gliederungsEbenen = [...this.gliederungsEbenen];
    const gliederungsEbeneIndex = gliederungsEbenen.findIndex(
      (gliederungsEbene) => gliederungsEbene.type === startType,
    );
    const nextGliederungsEbeneIndex = gliederungsEbenen.findIndex(
      (gliederungsEbene) => endType === gliederungsEbene.type,
    );

    const optionalGliederungsEbenen = gliederungsEbenen
      .splice(gliederungsEbeneIndex + 1, nextGliederungsEbeneIndex - gliederungsEbeneIndex - 1)
      .filter((gliederungsEbene, index) => {
        if (index > 0 && gliederungsEbene.dependingOn) {
          return false;
        }
        return true;
      });

    return optionalGliederungsEbenen;
  }

  public getOptionalGliederungsEbenenAbove(type: string): GliederungsEbene[] {
    const gliederungsEbenen = [...this.gliederungsEbenen];
    const gliederungsEbeneIndex = gliederungsEbenen.findIndex((gliederungsEbene) => gliederungsEbene.type === type);

    const optionalGliederungsEbenen = gliederungsEbenen
      .slice(0, gliederungsEbeneIndex)
      .filter((gliederungsEbene, index) => {
        if (index > 0 && gliederungsEbene.dependingOn) {
          return false;
        }
        return true;
      });

    return optionalGliederungsEbenen;
  }

  public getOptionalGliederungsEbenenBelow(type: string): GliederungsEbene[] {
    const gliederungsEbenen = [...this.gliederungsEbenen];
    const gliederungsEbeneIndex = gliederungsEbenen.findIndex((gliederungsEbene) => gliederungsEbene.type === type);

    const optionalGliederungsEbenen = gliederungsEbenen
      .slice(gliederungsEbeneIndex + 1, gliederungsEbenen.length)
      .filter((gliederungsEbene, index) => {
        if (index > 0 && gliederungsEbene.dependingOn) {
          return false;
        }
        return true;
      });

    return optionalGliederungsEbenen;
  }
}
