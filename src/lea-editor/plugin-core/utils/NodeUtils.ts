// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, BaseOperation, BaseSelection, BaseText, Editor, Location, Node, NodeEntry, Path, Span } from 'slate';

import { ChangeWrapper, CommentMarkerWrapper, Element, TextWrapper } from '../../../interfaces';
import { v4 as uuid } from 'uuid';
import { STRUCTURE_ELEMENTS } from './StructureElements';

export const isOfType = (editor: Editor, type: string, location?: Path): boolean => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  let result = false;
  const [selectedNode] = Editor.nodes(editor, {
    at: location,
    match: (n) => (n as Element).type === type,
  });
  if (selectedNode) {
    const [node] = selectedNode;
    result = (node as Element).type === type;
  }
  return result;
};

export const isPreviousNeighborOfType = (editor: Editor, type: string, location?: Path): boolean => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }

  if (location[location.length - 1] > 0) {
    const previousPath = location.map((x, y, location) => (y === location.length - 1 ? x - 1 : x));
    const [node] = Editor.node(editor, previousPath) as NodeEntry<Element>;
    return node?.type === type;
  }
  return false;
};

export const hasPreviousNeighbor = (editor: Editor, location?: Path): boolean => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }
  return location[location.length - 1] > 0;
};

export const isNextNeighborOfType = (editor: Editor, type: string, location?: Path): boolean => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }

  const [parent] = Editor.above(editor, { at: location }) as NodeEntry<Element>;
  if (location && parent && location[location.length - 1] < parent.children.length - 1) {
    const nextPath = location.map((x, y, location) => (y === location.length - 1 ? x + 1 : x));
    const [node] = Editor.node(editor, nextPath) as NodeEntry<Element>;
    return node?.type === type;
  }
  return false;
};

export const hasNextNeighborsOtherThanMarker = (editor: Editor, location?: Path): boolean => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }

  const [parent] = Editor.above(editor, { at: location }) as NodeEntry<Element>;
  for (let index = location[location.length - 1] + 1; index < parent.children.length; index++) {
    if (parent.children[index].type !== STRUCTURE_ELEMENTS.MARKER) {
      return true;
    }
  }
  return false;
};

export const hasOnlyNextNeighborsOfTypeDeletedWrapper = (editor: Editor, location?: Path): boolean => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }

  const [parent] = Editor.above(editor, { at: location }) as NodeEntry<Element>;

  for (let index = location[location.length - 1] + 1; index < parent.children.length; index++) {
    if (
      parent.children[index].type !== STRUCTURE_ELEMENTS.CHANGEWRAPPER ||
      (parent.children[index].type === STRUCTURE_ELEMENTS.CHANGEWRAPPER &&
        (parent.children[index] as ChangeWrapper)['lea:changeType'] !== 'ct_deleted')
    ) {
      return false;
    }
  }
  return true;
};

export const isWithinNodeOfType = (editor: Editor, type: string, location?: Path): boolean => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  const node = Editor.above(editor, {
    match: (n) => (n as Element).type === type,
    at: location,
  });
  return !!node;
};

// Checks whether the currently selected node is
// within a node with a editable attribute
export const isWithinEditableNode = (editor: Editor, location?: Path): boolean => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  const node = Editor.above(editor, {
    match: (n) => (n as Element)['lea:editable'] === true,
    at: location,
  });

  return !!node;
};

export const hasNodeOfType = (editor: Editor, type: string, path?: Path): boolean => {
  if (!path) {
    path = editor.selection?.anchor.path;
  }
  const [...nodes] = Editor.nodes(editor, {
    match: (n) => {
      return (n as Element).type === type;
    },
    at: path,
    voids: true,
  });
  return nodes.length > 0;
};

// Checks whether the currently selected node is within the specified hierarchy of types of nodes
export const isWithinTypeHierarchy = (editor: Editor, types: string[], location?: Path): boolean => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  return types.every((type) => {
    const node = Editor.above(editor, {
      match: (n) => (n as Element).type === type,
      at: location,
    });

    if (!node) {
      return false;
    }
    const [, nodePath] = node;
    location = nodePath;
    return true;
  });
};

// Checks whether the currently selected node is exactly within the specified hierarchy of types of nodes
export const isWithinExactTypeHierarchy = (editor: Editor, types: string[], location?: Path): boolean => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  return types.every((type) => {
    const node = Editor.above(editor, {
      match: (n) => (n as Element).type === type,
      at: location,
    });

    if (!node) {
      return false;
    }
    const [, nodePath] = node;

    if (location && nodePath.length !== location?.length - 1) {
      return false;
    }
    location = nodePath;
    return true;
  });
};

export const getNodeAt = (editor: Editor, location?: Path): NodeEntry<Element> | null => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  if (location) {
    const node = Editor.node(editor, location) as NodeEntry<Element>;
    return node;
  }
  return null;
};

export const getNodesOfTypeAt = (editor: Editor, type: string[], location?: Path): NodeEntry<Element>[] => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  const [...nodes] = Editor.nodes<Element>(editor, {
    at: location,
    match: (n: Node) => n && type.includes((n as Element).type),
  });
  if (!nodes) {
    return [];
  }
  return nodes;
};

export const getNodesOfTypeInRange = (editor: Editor, type: string[], range: Span): NodeEntry<Element>[] => {
  const [...nodes] = Editor.nodes<Element>(editor, {
    at: range,
    match: (n: Node) => n && type.includes((n as Element).type),
    mode: 'all',
  });
  if (!nodes) {
    return [];
  }
  return nodes;
};

export const getChildrenOfTypeAt = (editor: Editor, type: string[], location?: Path): NodeEntry<Element>[] => {
  const nodes: NodeEntry<Element>[] = [];
  // If location is not set -> use current editor selection instead
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  if (location) {
    const [parent, parentPath] = Editor.node(editor, location) as NodeEntry<Element>;
    if (parent?.children) {
      parent.children.forEach((child, index) => {
        if (type.includes(child.type)) {
          nodes.push([child as Element, [...parentPath, index]]);
        }
      });
    }
  }
  return nodes;
};

// returns the index of the first child that has the given type
export const getFirstChildIndexAt = (editor: Editor, type: string, location?: Path): number => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  if (location) {
    const [parent] = Editor.node(editor, location) as NodeEntry<Element>;
    return parent.children.findIndex((child) => child.type === type);
  }
  return -1;
};

export const getLowestNodeAt = (editor: Editor, location?: Path): BaseText | undefined => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  const [nodes] = Editor.nodes(editor, { at: location, mode: 'lowest' });
  if (!nodes) {
    return;
  }
  const [node] = nodes as NodeEntry<BaseText>;
  return node;
};

export const getLowestNodeEntryAt = (editor: Editor, location?: Path): NodeEntry<BaseText> | undefined => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }
  const [nodes] = Editor.nodes(editor, { at: location, mode: 'lowest' });
  if (!nodes) {
    return;
  }
  return nodes as NodeEntry<BaseText>;
};

export const getLastLocationAt = (editor: Editor, location?: Path): Location => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }

  const [nodes] = Editor.nodes(editor, { at: location, mode: 'lowest', reverse: true });
  const [node, path] = nodes as NodeEntry<BaseText>;
  const textLength = node.text?.length;

  return { path: path, offset: textLength };
};

export const getFirstLocationAt = (editor: Editor, location?: Path): Location => {
  if (!location) {
    location = editor.selection?.anchor.path;
  }

  const [nodes] = Editor.nodes(editor, { at: location, mode: 'lowest', reverse: false });
  const [, path] = nodes as NodeEntry<BaseText>;

  return { path: path, offset: 0 };
};

export const getSelectedSiblingIndex = (
  editor: Editor,
  type: string,
  selectedNodePath?: BaseSelection | Path,
): number => {
  // If location is not set -> use current editor selection instead
  if (!selectedNodePath) {
    const selectedNode = Editor.above(editor, { match: (n) => (n as Element).type === type });
    if (!selectedNode) return -1;
    const [, path] = selectedNode;
    selectedNodePath = path;
  }
  if (selectedNodePath) {
    const siblings = getSiblings(editor, type, selectedNodePath);
    return siblings.findIndex(function (siblingEntry) {
      const [, path] = siblingEntry as NodeEntry<Ancestor>;
      return path.toString() === selectedNodePath?.toString();
    });
  }
  return -1;
};

export const getSiblings = (
  editor: Editor,
  type: string,
  selectedNodePath?: BaseSelection | Path,
): NodeEntry<Element>[] => {
  // If location is not set -> use current editor selection instead
  if (!selectedNodePath) {
    const [nodeEntry] = Editor.nodes(editor, { match: (x) => (x as Element).type === type, mode: 'lowest' });
    if (!nodeEntry) {
      return [];
    }
    const [, path] = nodeEntry;
    selectedNodePath = path;
  }

  // If no selection can be determined -> return
  if (selectedNodePath) {
    const parent = Editor.parent(editor, selectedNodePath);
    const [, parentPath] = parent as NodeEntry<Element>;
    const path = selectedNodePath as number[];
    let [...siblings] = Editor.nodes<Element>(editor, { at: parentPath, match: (x) => (x as Element).type === type });
    siblings = siblings.filter((x) => x[1].length === path.length);
    return siblings;
  }
  return [];
};

export const isFirstSiblingOfType = (
  editor: Editor,
  type: string,
  selectedNodePath?: BaseSelection | Path,
): boolean => {
  const index = getSelectedSiblingIndex(editor, type, selectedNodePath);
  return index < 1;
};

export const isLastSiblingOfType = (editor: Editor, type: string, selectedNodePath?: BaseSelection | Path): boolean => {
  const siblings = getSiblings(editor, type, selectedNodePath);
  const index = getSelectedSiblingIndex(editor, type, selectedNodePath);
  return index === siblings.length - 1;
};

export const getPreviousSibling = (
  editor: Editor,
  type: string,
  selectedNodePath?: BaseSelection | Path,
): NodeEntry<Element> | undefined => {
  const siblings = getSiblings(editor, type, selectedNodePath);
  const index = getSelectedSiblingIndex(editor, type, selectedNodePath);

  if (index < 0) {
    return;
  }
  const previousIndex = index - 1;
  if (previousIndex < 0) {
    return;
  }
  return siblings[previousIndex];
};

export const getPreviousNodeEntryElement = (
  editor: Editor,
  types: string[],
  selectedElement: NodeEntry<Element>,
  selectedNodePath?: Path,
): NodeEntry<Element> | undefined => {
  const elements = getNodesOfTypeAt(editor, types, selectedNodePath);
  const index = elements.findIndex((element) => JSON.stringify(element) === JSON.stringify(selectedElement));

  if (index < 0) {
    return;
  }
  const previousIndex = index - 1;
  if (previousIndex < 0) {
    return;
  }
  return elements[previousIndex];
};

export const getPreviousElement = (
  editor: Editor,
  elementPath: Path,
  types?: string[],
): NodeEntry<Element> | undefined => {
  if (elementPath[elementPath.length - 1] === 0) return;
  const beforeContent = Editor.before(editor, elementPath);
  const previousElement = Editor.above(editor, { at: beforeContent?.path }) as NodeEntry<Element>;

  if (types && types.length > 0) {
    let i = 0;
    while (i < types.length) {
      if (previousElement[0].type === types[i]) {
        return previousElement;
      }
      i++;
    }
    return;
  }

  return previousElement;
};

export const getNextElement = (editor: Editor, elementPath: Path): NodeEntry<Element> | undefined => {
  const afterContent = Editor.after(editor, elementPath);
  const nextElement = Editor.above(editor, { at: afterContent?.path }) as NodeEntry<Element>;

  return nextElement;
};

export const getNextElementOfType = (
  editor: Editor,
  types: string[],
  selectedElement: Element,
  selectedNodePath?: Path,
): NodeEntry<Element> | undefined => {
  const elements = getNodesOfTypeAt(editor, types, selectedNodePath);
  const index = elements.findIndex((element) => JSON.stringify(element[0]) === JSON.stringify(selectedElement));

  if (index < 0) {
    return;
  }
  const nextIndex = index + 1;

  return elements[nextIndex];
};

export const getAllOfType = (editor: Editor, type: string): NodeEntry<Element>[] => {
  const [...body] = Editor.nodes(editor, {
    at: [0],
    match: (n) =>
      (n as Element).type === STRUCTURE_ELEMENTS.AKOMANTOSO || (n as Element).type === STRUCTURE_ELEMENTS.BODY,
  });
  if (!body.length || !body[0]) {
    return [];
  }
  const [, bodyPath] = body[0];

  const [...nodes] = Editor.nodes<Element>(editor, {
    at: bodyPath,
    match: (n) => (n as Element).type === type,
  });
  return nodes;
};

export const isFirstOfType = (editor: Editor, type: string, selectedNodePath?: BaseSelection | Path): boolean => {
  const [, currentNodePath] = Editor.node(editor, (selectedNodePath ? selectedNodePath : editor.selection) as Location);
  const nodes = getAllOfType(editor, type);
  return nodes[0][1].toString() === currentNodePath.toString();
};

export const isLastOfType = (editor: Editor, type: string, selectedNodePath?: BaseSelection | Path): boolean => {
  const [, currentNodePath] = Editor.node(editor, (selectedNodePath ? selectedNodePath : editor.selection) as Location);
  const nodes = getAllOfType(editor, type);
  return nodes[nodes.length - 1][1].toString() === currentNodePath.toString();
};

export const getParentElement = (editor: Editor, location?: Location): Element | undefined => {
  if (!location) {
    const [, parentPath] = Editor.above(editor) as NodeEntry<Element>;
    location = parentPath;
  }

  const parentNode = Editor.above(editor, { at: location }) as NodeEntry<Element>;
  if (!parentNode) {
    return undefined;
  }
  const parent = parentNode[0];
  return parent;
};

export const createElementOfType = (
  type: string,
  children?: (Element | TextWrapper)[],
  hasGuid = true,
  attributes: Partial<Element> = {},
): Element => {
  if (hasGuid) {
    return {
      children: children ?? [],
      GUID: uuid(),
      type: type,
      ...attributes,
    };
  } else
    return {
      children: children ?? [],
      type: type,
      ...attributes,
    };
};

export const createTextWrapper = (text: string): TextWrapper => {
  return {
    type: STRUCTURE_ELEMENTS.TEXTWRAPPER,
    children: [
      {
        text,
      },
    ],
  };
};

export const createInlineFragment = (changeType: string, text: string): ChangeWrapper => {
  return {
    type: STRUCTURE_ELEMENTS.CHANGEWRAPPER,
    'lea:changeType': changeType,
    children: [
      {
        text,
      },
    ],
  };
};

export const createCommentMarkerWrapper = (
  commentId: string,
  commentPosition: string,
  text = '',
): CommentMarkerWrapper => {
  return {
    type: STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER,
    children: [
      {
        type: STRUCTURE_ELEMENTS.COMMENTMARKER,
        children: [{ text }],
        'lea:commentId': commentId || '',
        'lea:commentPosition': commentPosition,
      },
    ],
  };
};

export const getElementGUIDFromSelection = (editor: Editor, selection: BaseSelection): string => {
  const [nodeEntry] = Editor.nodes(editor, {
    match: (x) => !!(x as Element).GUID,
    mode: 'lowest',
    at: selection?.anchor,
  });
  if (nodeEntry) {
    const [element] = nodeEntry as NodeEntry<Element>;
    return element.GUID ?? '';
  } else {
    return '';
  }
};

// Generator of all Textnodes inside element.
export const getHeadingTextFromNode = (element: Element): string => {
  const headingTexts = Node.texts(element);
  let currentHeadingText = headingTexts.next();
  const currentHeadingTextsArray = [];
  while (!currentHeadingText.done) {
    // This query prevents texts from authorial notes to be shown in the table of contents
    // Authorialnotes are in Begründung on level 5 and in Regelungstext on level 4
    if (currentHeadingText.value[1].length < 4) {
      currentHeadingTextsArray.push(currentHeadingText.value[0].text);
    }
    currentHeadingText = headingTexts.next();
  }
  const headingText = currentHeadingTextsArray.join('');
  return headingText;
};

export const isNodeOperation = (op: BaseOperation) => {
  return ['insert_node', 'merge_node', 'move_node', 'remove_node', 'split_node'].some((type) => op.type === type);
};
