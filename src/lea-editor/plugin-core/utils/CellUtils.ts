// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, NodeEntry } from 'slate';
import { DocumentDTO } from '../../../api';
import { LeafElementCellStructure, WrapperElementCellStructure } from '../../../constants';
import { Element } from '../../../interfaces';
import { diff_match_patch } from 'diff-match-patch';
import { getParentElement } from './NodeUtils';
import { STRUCTURE_ELEMENTS } from './StructureElements';

// The row with index 0 of this type containts the element guids, whereas
// the other rows have boolean values that indicate whether a guid occurs
// in a particular document. The row with index 1 represents the document with
// index 0 and so on
export type DiffTable = [string[], ...boolean[][]];

export type ElementAndChildrenGuid = {
  elementGuid: string;
  children?: string[];
};
type ElementAndChildrenGuids = {
  elementGuid: string;
  children?: string[][];
};

type ParentDiffTable = [ElementAndChildrenGuids[], ...boolean[][]];

export const setDividersHeight = (guid: string | undefined, originIndex: number, isRef?: boolean) => {
  if (!guid || guid === '') return;
  const editableAreas = document.getElementsByClassName('plugin-editor');
  const dividerNodes: Array<NodeListOf<HTMLElement>> = [];

  //check if refGUID is available
  let refGUIDAvailable = false;
  for (let i = 0; i < editableAreas.length; i++) {
    refGUIDAvailable =
      document.querySelectorAll<HTMLElement>(`[divider-guid="${i}:${guid}:ref"]`).length > 0 ? true : refGUIDAvailable;
  }

  //get divider for comparrison
  for (let i = 0; i < editableAreas.length; i++) {
    const dividersGUID = document.querySelectorAll<HTMLElement>(`[divider-guid="${i}:${guid}"]`);
    const dividersRefGUID = document.querySelectorAll<HTMLElement>(`[divider-guid="${i}:${guid}:ref"]`);
    if ((originIndex === i && isRef) || (originIndex !== i && !isRef && refGUIDAvailable)) {
      dividerNodes.push(dividersRefGUID);
    } else {
      dividerNodes.push(dividersGUID);
    }
  }
  setDividerOffset(dividerNodes);
};

const setDividerOffset = (dividerNodes: Array<NodeListOf<HTMLElement>>) => {
  let dividerOffsetTop = 0;
  dividerNodes.forEach((dividers, editorIndex) => {
    if (dividers?.length < 1) return;
    let divider = dividers[0];

    divider.setAttribute('style', `margin-top: 10px`);
    const editableAreaScrollTop = (document.getElementById(`editable-area-${editorIndex}`) as HTMLElement).scrollTop;
    const currentDividerOffsetTop = Math.round(divider.getBoundingClientRect().top + editableAreaScrollTop);
    if (dividerOffsetTop > 0 && dividerOffsetTop > currentDividerOffsetTop) {
      divider.setAttribute('style', `margin-top: ${dividerOffsetTop - currentDividerOffsetTop + 10}px`);
    } else if (dividerOffsetTop > 0 && dividerOffsetTop < currentDividerOffsetTop) {
      for (let i = editorIndex - 1; i >= 0; i--) {
        if (dividerNodes[i]?.length < 1) continue;
        divider = dividerNodes[i][0];
        const dividerMarginTop = Math.round(parseInt(divider.style.marginTop) || 10);
        divider.setAttribute('style', `margin-top: ${currentDividerOffsetTop - dividerOffsetTop + dividerMarginTop}px`);
      }
      dividerOffsetTop = currentDividerOffsetTop;
    } else {
      dividerOffsetTop = currentDividerOffsetTop;
    }
  });
};

export const setNextDividersHeight = (editorIndex: number, guid: string) => {
  const dividers = (document.getElementById(`editable-area-${editorIndex}`) as HTMLElement)?.getElementsByClassName(
    'synopsis-divider',
  ) as HTMLCollectionOf<HTMLElement>;
  let nextDivider = null;
  let nearestOffsetTop = null;

  const currentElement = document.getElementById(`${editorIndex}:${guid}`) as HTMLElement;
  if (!currentElement) return;
  for (const divider of dividers) {
    const dividerOffsetTop = divider.getBoundingClientRect().top;
    if (
      dividerOffsetTop > currentElement.getBoundingClientRect().top &&
      (!nearestOffsetTop || dividerOffsetTop < nearestOffsetTop)
    ) {
      nearestOffsetTop = dividerOffsetTop;
      nextDivider = divider;
    }
  }
  const dividerGUID = nextDivider?.getAttribute('divider-guid');
  if (dividerGUID) {
    const guid = dividerGUID.split(':')[1];
    setDividersHeight(guid, editorIndex, dividerGUID.split(':').length > 2);
  }
};

export const setAllDividersHeight = (editorIndex: number) => {
  const editableArea = document.getElementById(`editable-area-${editorIndex}`) as HTMLElement;
  const dividers = editableArea?.getElementsByClassName('synopsis-divider');
  for (const divider of dividers) {
    const dividerGUID = divider.getAttribute('divider-guid');
    if (dividerGUID) {
      const guid = dividerGUID.split(':')[1];
      setDividersHeight(guid, editorIndex, dividerGUID.split(':').length > 2);
    }
  }
};

export const checkIfCellsVisible = (
  isSynopsisVisible: boolean,
  firstDocument: DocumentDTO | null,
  documents?: DocumentDTO[],
) => {
  return (
    (isSynopsisVisible && documents?.every((editorDocument) => editorDocument.type === firstDocument?.type)) ?? false
  );
};

const getEditorGuidsAboveArticle = (editor: Editor): ElementAndChildrenGuid[] => {
  const dividerNodes = Editor.nodes(editor, {
    match: (node, path) => {
      const parentElement = getParentElement(editor, path);
      const isWrapper = WrapperElementCellStructure.includes((node as Element).type);
      const isWithinWrapper = WrapperElementCellStructure.some(
        (element) => !!Editor.above(editor, { match: (n) => (n as Element).type === element, at: path }),
      );
      return (checkIfCellsShouldRender((node as Element).type, parentElement?.type) && !isWithinWrapper) || isWrapper;
    },
    at: [],
  });
  let nextDividerNode = dividerNodes.next();
  const guidArray: ElementAndChildrenGuid[] = [];
  while (!nextDividerNode.done) {
    const [node, path] = nextDividerNode.value as NodeEntry<Element>;
    const isWrapperElement = WrapperElementCellStructure.includes(node.type);
    if (isWrapperElement) {
      const [...childNodes] = Editor.nodes(editor, {
        match: (node, p) => {
          const parentElement = getParentElement(editor, p);
          return !!parentElement && checkIfCellsShouldRender((node as Element).type, parentElement.type);
        },
        at: path,
      });
      const childrenGuids = childNodes
        .map((nodeEntry) => {
          const [node] = nodeEntry;
          return getCompleteGuid(node as Element);
        })
        .filter((guid) => !!guid) as string[];
      const id = getCompleteGuid(node);
      id
        ? guidArray.push({ elementGuid: id, children: childrenGuids })
        : guidArray.push({ elementGuid: node.type, children: childrenGuids });
    } else {
      const id = getCompleteGuid(node);
      id && guidArray.push({ elementGuid: id });
    }
    nextDividerNode = dividerNodes.next();
  }
  return guidArray;
};

export const getEditorsGuidsAboveArticle = (...editors: Editor[]) => {
  const guidArrays = editors.map((editor) => getEditorGuidsAboveArticle(editor));
  return guidArrays;
};

export const getUpdatedGuidsAboveArticle = (
  previousElementGuids: ElementAndChildrenGuid[][],
  updatedEditor: Editor,
  index: number,
) => {
  if (previousElementGuids === null) return null;
  const updatedGuids = getEditorGuidsAboveArticle(updatedEditor);
  const elementsGuids = previousElementGuids.map((guids, i) => {
    if (i === index) {
      return updatedGuids;
    } else {
      return guids;
    }
  });
  return elementsGuids;
};

export const getCellStructureAboveArticle = (...guidArrays: ElementAndChildrenGuid[][]): ParentDiffTable => {
  const guidBuffer: ElementAndChildrenGuids[] = [];
  const unicodeArrays = guidArrays.map((guidArr, editorIndex) => {
    return guidArr.map((cell) => {
      const guidIndex = guidBuffer.findIndex((c) => c.elementGuid === cell.elementGuid);
      if (guidIndex === -1) {
        const children = cell.children;
        guidBuffer.push({
          elementGuid: cell.elementGuid,
          children: children
            ? guidArrays.map((_, i) => {
                return i === editorIndex ? children : [];
              })
            : undefined,
        });
        return String.fromCharCode(guidBuffer.length - 1);
      } else {
        const bufferEntry = guidBuffer[guidIndex];
        if (!!cell.children && bufferEntry.children) {
          bufferEntry.children[editorIndex] = cell.children;
        }
        return String.fromCharCode(guidIndex);
      }
    });
  });
  const diffTableUnicode = createUnicodeDiffTable(unicodeArrays);
  const firstGuidDiffTableColumn = diffTableUnicode[0].map((unicodeChar) => {
    const unicodeIndex = unicodeChar.charCodeAt(0);
    const guid = guidBuffer[unicodeIndex];
    return guid;
  });
  const guidDiffTable: ParentDiffTable = [firstGuidDiffTableColumn, ...(diffTableUnicode.slice(1) as boolean[][])];
  return guidDiffTable;
};

export const getCompleteCellStructure = (cellStructureAboveArticle: ParentDiffTable) => {
  const guidRow = JSON.parse(JSON.stringify(cellStructureAboveArticle[0])) as ElementAndChildrenGuids[];
  const completeDiffTable = cellStructureAboveArticle.map(() => []) as unknown as DiffTable;
  guidRow.forEach((guidElement, elementIndex) => {
    const childrenGuids = guidElement.children;
    if (!childrenGuids) {
      completeDiffTable.forEach((col, i) => {
        if (i === 0) {
          (col as string[]).push(cellStructureAboveArticle[0][elementIndex].elementGuid);
        } else {
          (col as boolean[]).push(cellStructureAboveArticle[i][elementIndex] as boolean);
        }
      });
      return;
    }
    const guidBuffer: string[] = [];
    const unicodeArrays = childrenGuids.map((guidArr, editorIndex) => {
      if (!cellStructureAboveArticle[editorIndex + 1][elementIndex]) return [];
      return guidArr.map((guid) => {
        const guidIndex = guidBuffer.indexOf(guid);
        if (guidIndex === -1) {
          guidBuffer.push(guid);
          return String.fromCharCode(guidBuffer.length - 1);
        } else {
          return String.fromCharCode(guidIndex);
        }
      });
    });
    const unicodeDiffTable = createUnicodeDiffTable(unicodeArrays);
    const guidDiffTable = [
      unicodeDiffTable[0].map((unicodeChar) => {
        const unicodeIndex = unicodeChar.charCodeAt(0);
        const guid = guidBuffer[unicodeIndex];
        return guid;
      }),
      ...unicodeDiffTable.slice(1),
    ] as DiffTable;
    completeDiffTable.forEach((col, i) => {
      if (i === 0) {
        (col as string[]).push(...guidDiffTable[0]);
      } else {
        (col as boolean[]).push(...(guidDiffTable[i] as boolean[]));
      }
    });
  });
  return completeDiffTable;
};

const createUnicodeDiffTable = (unicodeArrays: string[][], unicodeDiffTable: DiffTable = [[]]): DiffTable => {
  if (unicodeDiffTable.length < 2) {
    const a: DiffTable = [unicodeArrays[0], unicodeArrays[0].map(() => true)];
    unicodeArrays = unicodeArrays.slice(1);
    unicodeDiffTable = a;
  }
  if (unicodeArrays.length === 0) return unicodeDiffTable;
  const dmp = new diff_match_patch();
  const firstUniCodeString = unicodeDiffTable[0].join('');
  const secondUniCodeString = unicodeArrays[0].join('');
  unicodeDiffTable.push([]);
  const numberOfColumns = unicodeDiffTable.length;

  const diffs = dmp.diff_main(firstUniCodeString, secondUniCodeString);
  diffs.forEach((diff) => {
    const [operation, unicodeString] = diff;
    const unicodeArray = unicodeString.split('');
    const currentRowIndex = unicodeDiffTable[numberOfColumns - 1].length;
    unicodeDiffTable.forEach((_, index) => {
      if (index === 0) {
        operation === diff_match_patch.DIFF_INSERT && unicodeDiffTable[0].splice(currentRowIndex, 0, ...unicodeArray);
      } else if (index < numberOfColumns - 1) {
        operation === diff_match_patch.DIFF_INSERT &&
          unicodeDiffTable[index].splice(currentRowIndex, 0, ...unicodeArray.map(() => false));
      } else {
        unicodeDiffTable[index].splice(
          currentRowIndex,
          0,
          ...unicodeArray.map(
            () => operation === diff_match_patch.DIFF_INSERT || operation === diff_match_patch.DIFF_EQUAL,
          ),
        );
      }
    });
  });
  return createUnicodeDiffTable(unicodeArrays.slice(1), unicodeDiffTable);
};

export const checkIfCellsShouldRender = (elementType: string, parentElementType?: string): boolean => {
  return (
    LeafElementCellStructure.some((x) => x === elementType) &&
    // Don't render cells if akn:p is in table cell
    parentElementType !== STRUCTURE_ELEMENTS.TABLE_CELL &&
    parentElementType !== STRUCTURE_ELEMENTS.TABLE_HEADERCELL
  );
};

const getCompleteGuid = (node: Element) => {
  return node.refGUID ? `${node.refGUID}:ref` : node.GUID;
};
