// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

interface NavigatorUABrandVersion {
  readonly brand: string;
  readonly version: string;
}
interface UALowEntropyJSON {
  readonly brands?: NavigatorUABrandVersion[];
}

interface NavigatorUA extends Navigator {
  userAgentData?: UALowEntropyJSON;
}

export const isFirefox = navigator.userAgent.indexOf('Firefox') != -1;

export const isChromiumGreaterThan129 =
  Number((navigator as NavigatorUA).userAgentData?.brands?.find((brand) => brand.brand === 'Chromium')?.version) > 129;
