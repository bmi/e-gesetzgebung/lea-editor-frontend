// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const focusLandMark = (forewardDirection: boolean, nextIndex?: number) => {
  const mainLea = global.document.getElementsByClassName('lea-editor') as HTMLCollectionOf<HTMLElement>;
  const landmarks = mainLea[0].querySelectorAll<HTMLElement>('nav, section, aside');
  if (landmarks.length > 0) {
    let index = nextIndex || -1;
    if (!nextIndex) {
      for (let i = 0; i < landmarks.length; i++) {
        if (landmarks.item(i).contains(global.document.activeElement)) {
          index = i;
        }
      }
    }
    nextIndex = getNextIndex(forewardDirection, index, landmarks);
    const nextlandmark = landmarks.item(nextIndex);
    if (nextlandmark.ariaHidden !== 'true') {
      nextlandmark?.focus();
    } else {
      focusLandMark(forewardDirection, nextIndex);
    }
  }
};

const getNextIndex = (forewardDirection: boolean, index: number, landmarks: NodeListOf<HTMLElement>) => {
  if (forewardDirection) {
    return (index + 1) % landmarks.length;
  } else {
    return (index - 1 + landmarks.length) % landmarks.length;
  }
};
