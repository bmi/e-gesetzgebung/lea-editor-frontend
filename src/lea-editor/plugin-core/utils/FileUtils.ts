// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export function getFileExtension(fileName: string | undefined | null): string {
  const empty = '';
  return fileName ? /\.([^.]*?)(?=\?|#|$)/.exec(fileName.trim())?.[0] || empty : empty;
}

export function incFileNameVersionNumber(
  fileName: string,
  increaseNumber: boolean,
  fileNameSuggestion?: string,
  version?: number,
): string {
  if (fileNameSuggestion === undefined) {
    // If currently no suggestion for a file name is available do not rename file yet
    return fileName;
  }
  const fileFormat = fileName.substring(fileName.lastIndexOf('.'));
  const currentDate = new Date().toISOString().split('T')[0].replace(/-/g, '');
  let newFileName = '';
  if (increaseNumber && version) {
    newFileName = version.toString();
  }
  newFileName = `${currentDate}_${fileNameSuggestion}_${newFileName}${fileFormat}`;
  return newFileName;
}
