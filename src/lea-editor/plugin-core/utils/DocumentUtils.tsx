// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { CommentResponseDTO, DocumentDTO, DocumentStateType } from '../../../api';
import { CommentMarkerWrapper, Element } from '../../../interfaces';
import { STRUCTURE_ELEMENTS } from './StructureElements';
import { CreateCommentIcon, MultipleCommentsIcon } from '@lea/ui';
import { useCommentsChange } from '../../plugins/comment';
import { Editor, Path } from 'slate';

export const getReadonlyState = (document: DocumentDTO) => {
  if (document.documentPermissions.hasRead && !document.documentPermissions.hasWrite) {
    return true;
  }
  return document.state !== DocumentStateType.DRAFT && document.state !== DocumentStateType.BEREIT_FUER_BUNDESTAG;
};

export const RenderCommentIcon = (element: Element): ReactElement => {
  const { updateComment } = useCommentsChange();
  const commentMarker = element.children.filter((e) => e.type === STRUCTURE_ELEMENTS.COMMENTMARKERWRAPPER);
  if (commentMarker.length === 0) return <></>;
  const commentIds = commentMarker.flatMap((e) =>
    (e as unknown as CommentMarkerWrapper).children
      .filter((comment) => comment['lea:commentPosition'] === 'start' && !comment.hidden)
      .flatMap((comment) => comment['lea:commentId']),
  );
  if (commentMarker.length / 2 === 1) {
    return (
      <div
        className="single-comment-wrapper"
        data-commentids={JSON.stringify(commentIds)}
        onClick={(e) => ActivateComments(e, updateComment)}
      >
        <CreateCommentIcon />
      </div>
    );
  } else if (commentMarker.length / 2 > 1) {
    return (
      <div
        className="multiple-comment-wrapper"
        data-commentids={JSON.stringify(commentIds)}
        onClick={(e) => ActivateComments(e, updateComment)}
      >
        <MultipleCommentsIcon />
      </div>
    );
  } else {
    return <></>;
  }
};

const ActivateComments = (
  e: React.MouseEvent<HTMLDivElement, MouseEvent>,
  updateComment: (comment: Partial<CommentResponseDTO>) => void,
) => {
  const commentIdsString = (e.target as SVGSVGElement).parentElement?.getAttribute('data-commentids');
  const commentIds = commentIdsString ? (JSON.parse(commentIdsString) as string[]) : null;
  if (commentIds) {
    commentIds.forEach((commentId) => {
      updateComment({ commentId: commentId, isActive: true });
    });
  }
};

export const isGZR = (editor: Editor, path: Path): boolean => {
  let isGZR = false;
  if (path) {
    const parentArticle = Editor.above<Element>(editor, {
      at: path,
      match: (n) => (n as Element).type === STRUCTURE_ELEMENTS.ARTICLE,
    });
    if (parentArticle) {
      const [parentArticleElement] = parentArticle;
      isGZR = parentArticleElement.refersTo === 'geltungszeitregel';
    }
  }
  return isGZR;
};
