// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import isHotkey from 'is-hotkey';

export type Unit = 'character' | 'word' | 'line' | 'block';

export const isCutPaste = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+x', event) || isHotkey('mod+v', event);
};

export const isClipboardEvent = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+x', event) || isHotkey('mod+c', event) || isHotkey('mod+v', event);
};

export const isUndoEvent = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+z', event);
};

export const isRedoEvent = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+y', event);
};

export const isProtectedWhiteSpace = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+shift+ ', event);
};

export const isEnter = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'enter';
};

export const isEscape = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'escape';
};

export const isTab = (event: KeyboardEvent): boolean => {
  return isHotkey('tab', event);
};

const isTabBack = (event: KeyboardEvent): boolean => {
  return isHotkey('shift+tab', event);
};

export const isTabOrTabBack = (event: KeyboardEvent): boolean => {
  return isTab(event) || isTabBack(event);
};

export const isListCommand = (event: KeyboardEvent): boolean => {
  return isHotkey('alt+l', event);
};

export const isContextMenuCommand = (event: KeyboardEvent): boolean => {
  return isHotkey('shift+f10', event);
};

export const isLeftNavigation = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'arrowleft';
};

export const isRightNavigation = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'arrowright';
};

const isUpNavigation = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'arrowup';
};

const isDownNavigation = (event: KeyboardEvent): boolean => {
  return event.key.toLowerCase() === 'arrowdown';
};

export const isLeftOrRightNavigation = (event: KeyboardEvent): boolean => {
  return isLeftNavigation(event) || isRightNavigation(event);
};

export const isUpOrDownNavigation = (event: KeyboardEvent): boolean => {
  return isUpNavigation(event) || isDownNavigation(event);
};

export const isNextLandmarkCommand = (event: KeyboardEvent): boolean => {
  return isHotkey('alt+t', event);
};

export const isPreviousLandmarkCommand = (event: KeyboardEvent): boolean => {
  return isHotkey('alt+shift+t', event);
};

export const isSearch = (event: KeyboardEvent): boolean => {
  return isHotkey('mod+f', event);
};
