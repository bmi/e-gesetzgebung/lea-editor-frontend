// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EditorInfoStateReducer } from './editor-info/EditorInfoStateSlice';
import { WorkspaceState } from './EditorPageState';
import { EditorWizardVisibleSlice } from './editor-wizards';
import { AuthorialNotePageSlice, DynAenderungsvergleichSlice, TableOfContentsSlice } from './workspace';
import { combineReducers, Reducer } from '@reduxjs/toolkit';
import { CommentPageSlice } from './workspace/comment-page';
import { ContextMenuSlice } from './workspace/contextMenu';
import { EditorOptionsVisibleSlice } from './editor-options';
import { SearchReplaceSlice } from './workspace/search-replace';
import { CommentSlice } from '../../plugins/comment/CommentSlice';

export const getEditorReducer = (pluginReducer: Reducer | null) => {
  const workspace = combineReducers<WorkspaceState>({
    [EditorOptionsVisibleSlice.name]: EditorOptionsVisibleSlice.reducer,
    [TableOfContentsSlice.name]: TableOfContentsSlice.reducer,
    [AuthorialNotePageSlice.name]: AuthorialNotePageSlice.reducer,
    [CommentPageSlice.name]: CommentPageSlice.reducer,
    [CommentSlice.name]: CommentSlice.reducer,
    [ContextMenuSlice.name]: ContextMenuSlice.reducer,
    [SearchReplaceSlice.name]: SearchReplaceSlice.reducer,
    [DynAenderungsvergleichSlice.name]: DynAenderungsvergleichSlice.reducer,
  });
  return combineReducers({
    [EditorWizardVisibleSlice.name]: EditorWizardVisibleSlice.reducer,
    editorInfoState: EditorInfoStateReducer,
    workspace: workspace,
    ...(pluginReducer ? { pluginState: pluginReducer } : {}),
  });
};
