// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EntityState } from '@reduxjs/toolkit';
import { BaseSelection } from 'slate';
import { DocumentStateType, DocumentType } from '../../../../api/Document/Document';
import { SynopsisMode } from '../../../../constants';

export enum ColumnSize {
  SMALL = 'SMALL',
  MEDIUM = 'MEDIUM',
  LARGE = 'LARGE',
}

export interface EditorDocument {
  id: string;
  documentTitle: string;
  documentStateType: DocumentStateType;
  documentType: DocumentType;
  isReadonly: boolean;
  isEditable: boolean;
  isAuthorialNoteEditable: boolean;
  index: number;
  visible: boolean;
  size: ColumnSize;
  documentHasPreambel: boolean;
}

export interface EditorFocus {
  index: number;
  isAuthorialNoteEditor: boolean;
}

interface EditorInfo {
  isSynopsisVisible: boolean;
  editorDocumentsCount: number;
  editorOnFocus: EditorFocus;
  selection: BaseSelection;
  synchronousScrolling: boolean;
  synopsisMode: SynopsisMode;
  removeSynopsisDocuments: boolean;
  hdrCheckStartet: boolean;
  satzZaehlungVisible: boolean;
}

export interface EditorInfoState {
  editorDocuments: EntityState<EditorDocument>;
  editorInfo: EditorInfo;
}

const InitialEditorOnFocus: EditorFocus = { index: 0, isAuthorialNoteEditor: false };

export const InitialEditorInfo: EditorInfo = {
  isSynopsisVisible: false,
  editorDocumentsCount: 0,
  selection: null,
  editorOnFocus: InitialEditorOnFocus,
  synchronousScrolling: false,
  synopsisMode: SynopsisMode.Changes,
  removeSynopsisDocuments: false,
  hdrCheckStartet: false,
  satzZaehlungVisible: true,
};

export const InitialEditorDocument: EditorDocument = {
  id: '',
  documentStateType: DocumentStateType.DRAFT,
  documentType: DocumentType.BEGRUENDUNG_STAMMGESETZ,
  documentTitle: '',
  isReadonly: false,
  isEditable: false,
  isAuthorialNoteEditable: false,
  index: 0,
  visible: true,
  size: ColumnSize.MEDIUM,
  documentHasPreambel: false,
};

export const InitialEditorInfoState: EditorInfoState = {
  editorDocuments: { ids: [], entities: {} },
  editorInfo: InitialEditorInfo,
};
