// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { BaseSelection } from 'slate';
import { useAppDispatch, useAppSelector } from '../../../../components/Store';
import { EditorPageState } from '../EditorPageState';
import { changeSelection, editorDocumentsAdapter, EditorDocumentsSlice } from './EditorDocumentsSlice';
import { EditorFocus, InitialEditorInfo, InitialEditorInfoState } from './EditorInfoState';

import {
  EditorInfoSlice,
  setEditorDocumentsCount,
  setEditorOnFocus,
  setHdrCheckStartet,
  setRemoveSynopsisDocuments,
  setSynchronousScrollingOff,
  setSynchronousScrollingOn,
  setSynopsisMode,
  setSatzZaehlungVisible,
  toggleSatzZaehlungVisible,
} from './EditorInfoStateSlice';
import { SynopsisMode } from '../../../../constants';

type UseSynopsisInfo = {
  isSynopsisVisible: boolean;
  editorDocumentsCount: number;
  synopsisMode: SynopsisMode;
  removeSynopsisDocuments: boolean;
};

type UseEditorDocumentsLengthState = {
  editorDocumentsLength: number;
};

type UseHdrCheckState = {
  hdrCheckStartet: boolean;
};

type UseSelectionState = { selection: BaseSelection };

type UseEditorInfoChange = {
  setEditorDocumentsCount: (count: number) => void;
  updateSelection: (index: number, isAuthorialNote: boolean, isEditable: boolean, selection: BaseSelection) => void;
  setSynopsisMode: (synpsisMode: SynopsisMode) => void;
  setRemoveSynopsisDocuments: (removeDocuments: boolean) => void;
};

type UseHdrCheckDispatch = {
  setHdrCheckStartet: (hdrCheckStartet: boolean) => void;
};

type UseEditorOnFocusDispatch = {
  setEditorOnFocus: (editorOnFocus: EditorFocus) => void;
};

type UseSynchronousScrollingDispatch = {
  setSynchronousScrollingOn: () => void;
  setSynchronousScrollingOff: () => void;
  setSynchronousScrolling: (synchronousScrolling: boolean) => void;
};

type UseSatzZahlungDispatch = {
  setSatzZaehlungVisible: (satzZahlungVisible: boolean) => void;
  toggleSatzZaehlungVisible: () => void;
};

export const useSynopsisInfo = (): UseSynopsisInfo => {
  const editorDocumentsCount =
    useAppSelector(
      (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].editorDocumentsCount,
    ) || InitialEditorInfo.editorDocumentsCount;

  const synopsisMode = useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].synopsisMode,
  );

  const removeSynopsisDocuments = useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].removeSynopsisDocuments,
  );

  return {
    isSynopsisVisible: editorDocumentsCount > 1,
    editorDocumentsCount,
    synopsisMode,
    removeSynopsisDocuments,
  };
};

export const useEditorDocumentsLengthState = (): UseEditorDocumentsLengthState => {
  const editorDocumentsLength = useAppSelector((state) =>
    editorDocumentsAdapter
      .getSelectors()
      .selectTotal(
        (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
          InitialEditorInfoState[EditorDocumentsSlice.name],
      ),
  );

  return { editorDocumentsLength };
};

export const useHdrCheckStartetState = (): UseHdrCheckState => {
  const hdrCheckStartet = useAppSelector(
    (state) => (state.editorPage as EditorPageState).editorInfoState[EditorInfoSlice.name].hdrCheckStartet,
  );

  return { hdrCheckStartet };
};

export const useSelectionState = (): UseSelectionState => {
  const selection = useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].selection,
  );

  return {
    selection,
  };
};

export const useSynchronousScrollingState = (): boolean =>
  useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].synchronousScrolling,
  );

export const useEditorOnFocusState = () =>
  useAppSelector((state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].editorOnFocus);

export const useSatzZaehlungState = () =>
  useAppSelector(
    (state) => (state.editorPage as EditorPageState)?.editorInfoState[EditorInfoSlice.name].satzZaehlungVisible,
  );

export const useEditorInfoDispatch = (): UseEditorInfoChange => {
  const dispatch = useAppDispatch();

  const setEditorDocumentsCountCallback = useCallback((count: number) => {
    dispatch(setEditorDocumentsCount(count));
  }, []);

  const updateSelectionCallback = useCallback(
    (index: number, isAuthorialNote: boolean, isEditable: boolean, selection: BaseSelection) => {
      dispatch(changeSelection({ index, isEditable, isAuthorialNoteEditable: isAuthorialNote, selection }));
    },
    [],
  );

  const setSynopsisModeCallback = useCallback((synopsisMode: SynopsisMode) => {
    dispatch(setSynopsisMode(synopsisMode));
  }, []);

  const setRemoveSynopsisDocumentsCallback = useCallback((removeDocuments: boolean) => {
    dispatch(setRemoveSynopsisDocuments(removeDocuments));
  }, []);

  return {
    setEditorDocumentsCount: setEditorDocumentsCountCallback,
    updateSelection: updateSelectionCallback,
    setSynopsisMode: setSynopsisModeCallback,
    setRemoveSynopsisDocuments: setRemoveSynopsisDocumentsCallback,
  };
};

export const useEditorOnFocusDispatch = (): UseEditorOnFocusDispatch => {
  const dispatch = useAppDispatch();

  const setEditorOnFocusCallback = useCallback((editorOnFocus: EditorFocus) => {
    dispatch(setEditorOnFocus(editorOnFocus));
  }, []);

  return {
    setEditorOnFocus: setEditorOnFocusCallback,
  };
};

export const useSynchronousScrollingDispatch = (): UseSynchronousScrollingDispatch => {
  const dispatch = useAppDispatch();

  const setSynchronousScrollingOnCallback = useCallback(() => {
    dispatch(setSynchronousScrollingOn());
  }, []);

  const setSynchronousScrollingOffCallback = useCallback(() => {
    dispatch(setSynchronousScrollingOff());
  }, []);

  const setSynchronousScrollingCallback = useCallback((synchronousScrolling: boolean) => {
    if (synchronousScrolling) {
      setSynchronousScrollingOnCallback();
    } else {
      setSynchronousScrollingOffCallback();
    }
  }, []);

  return {
    setSynchronousScrollingOn: setSynchronousScrollingOnCallback,
    setSynchronousScrollingOff: setSynchronousScrollingOffCallback,
    setSynchronousScrolling: setSynchronousScrollingCallback,
  };
};

export const useHdrCheckStartetDispatch = (): UseHdrCheckDispatch => {
  const dispatch = useAppDispatch();

  const setHdrCheckStartetCallback = useCallback((hdrCheckStartet: boolean) => {
    dispatch(setHdrCheckStartet(hdrCheckStartet));
  }, []);

  return {
    setHdrCheckStartet: setHdrCheckStartetCallback,
  };
};

export const useSatzzahlungDispatch = (): UseSatzZahlungDispatch => {
  const dispatch = useAppDispatch();
  const setSatzZaehlungVisibleCallback = useCallback(
    (satzZaehlungVisible: boolean) => dispatch(setSatzZaehlungVisible(satzZaehlungVisible)),
    [],
  );
  const toggleSatzZaehlungVisibleCallback = useCallback(() => dispatch(toggleSatzZaehlungVisible()), []);
  return {
    setSatzZaehlungVisible: setSatzZaehlungVisibleCallback,
    toggleSatzZaehlungVisible: toggleSatzZaehlungVisibleCallback,
  };
};
