// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { combineReducers, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { EditorDocumentsSlice, changeSelection } from './EditorDocumentsSlice';

import { EditorFocus, EditorInfoState, InitialEditorInfo } from './EditorInfoState';
import { SynopsisMode } from '../../../../constants';

export const EditorInfoSlice = createSlice({
  name: 'editorInfo',
  initialState: InitialEditorInfo,
  reducers: {
    setEditorDocumentsCount(state, action: PayloadAction<number>) {
      state.editorDocumentsCount = action.payload;
      state.isSynopsisVisible = action.payload > 1;
    },
    setEditorOnFocus(state, action: PayloadAction<EditorFocus>) {
      state.editorOnFocus.index = action.payload.index;
      state.editorOnFocus.isAuthorialNoteEditor = action.payload.isAuthorialNoteEditor;
    },
    setSynchronousScrollingOn(state) {
      state.synchronousScrolling = true;
    },
    setSynchronousScrollingOff(state) {
      state.synchronousScrolling = false;
    },
    setSynopsisMode(state, action: PayloadAction<SynopsisMode>) {
      state.synopsisMode = action.payload;
    },
    setRemoveSynopsisDocuments(state, action: PayloadAction<boolean>) {
      state.removeSynopsisDocuments = action.payload;
    },
    setHdrCheckStartet(state, action: PayloadAction<boolean>) {
      state.hdrCheckStartet = action.payload;
    },
    setSatzZaehlungVisible(state, action: PayloadAction<boolean>) {
      state.satzZaehlungVisible = action.payload;
    },
    toggleSatzZaehlungVisible(state) {
      state.satzZaehlungVisible = !state.satzZaehlungVisible;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(changeSelection, (state, action) => {
      state.selection = action.payload.selection;
    });
  },
});

export const {
  setEditorDocumentsCount,
  setEditorOnFocus,
  setSynchronousScrollingOff,
  setSynchronousScrollingOn,
  setSynopsisMode,
  setRemoveSynopsisDocuments,
  setHdrCheckStartet,
  setSatzZaehlungVisible,
  toggleSatzZaehlungVisible,
} = EditorInfoSlice.actions;

const createReducer = () => {
  return combineReducers<EditorInfoState>({
    [EditorDocumentsSlice.name]: EditorDocumentsSlice.reducer,
    [EditorInfoSlice.name]: EditorInfoSlice.reducer,
  });
};

export const EditorInfoStateReducer = createReducer();
