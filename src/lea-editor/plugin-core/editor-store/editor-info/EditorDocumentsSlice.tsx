// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createEntityAdapter, createSlice, PayloadAction, Update } from '@reduxjs/toolkit';
import { BaseSelection } from 'slate';
import { DocumentDTO } from '../../../../api';
import { ColumnSize, EditorDocument } from './EditorInfoState';
import { getReadonlyState } from '../../utils/DocumentUtils';

type ChangeSelectionPayload = {
  index: number;
  isEditable: boolean;
  isAuthorialNoteEditable: boolean;
  selection: BaseSelection;
};

type ChangeSizePayload = {
  index: number;
  size: ColumnSize;
};

type ChangeDocumentHasPreambelPayload = {
  index: number;
  documentHasPreambel: boolean;
};

export const editorDocumentsAdapter = createEntityAdapter<EditorDocument>({
  selectId: (editorDocument) => editorDocument.index,
  sortComparer: (a, b) => a.index - b.index,
});

export const EditorDocumentsSlice = createSlice({
  name: 'editorDocuments',
  initialState: editorDocumentsAdapter.getInitialState(),
  reducers: {
    setEditorDocuments(state, action: PayloadAction<DocumentDTO[]>) {
      const editorDocuments = action.payload.map((document, index) => {
        return {
          id: document.id,
          documentStateType: document.state,
          documentTitle: document.title,
          documentType: document.type,
          index: index,
          isReadonly: getReadonlyState(document),
          isEditable: false,
          isAuthorialNoteEditable: false,
          visible: true,
          size: ColumnSize.MEDIUM,
          documentHasPreambel: false,
        } as EditorDocument;
      });
      editorDocumentsAdapter.setAll(state, editorDocuments);
    },
    removeDocuments(state) {
      editorDocumentsAdapter.removeAll(state);
    },
    changeSelection(state, action: PayloadAction<ChangeSelectionPayload>) {
      const updateDocuments: Update<EditorDocument>[] = editorDocumentsAdapter
        .getSelectors()
        .selectAll(state)
        .map((editorDocument) => {
          if (editorDocument.index === action.payload.index) {
            return {
              id: editorDocument.index,
              changes: {
                isEditable: action.payload.isEditable,
                isAuthorialNoteEditable: action.payload.isAuthorialNoteEditable,
              },
            };
          }
          return {
            id: editorDocument.index,
            changes: { isEditable: false, isAuthorialNoteEditable: false },
          };
        });
      editorDocumentsAdapter.updateMany(state, updateDocuments);
    },
    setDocumentVisible(state, action: PayloadAction<Array<number>>) {
      if (action.payload.length === 0) {
        return;
      }
      const editorDocuments = editorDocumentsAdapter.getSelectors().selectAll(state);
      editorDocumentsAdapter.updateMany(state, [
        ...editorDocuments.map((editorDocument) => ({
          id: editorDocument.index,
          changes: { visible: action.payload.includes(editorDocument.index) },
        })),
      ]);
    },
    setColumnSize(state, action: PayloadAction<ChangeSizePayload>) {
      editorDocumentsAdapter.updateOne(state, { id: action.payload.index, changes: { size: action.payload.size } });
    },
    setDocumentHasPreambel(state, action: PayloadAction<ChangeDocumentHasPreambelPayload>) {
      editorDocumentsAdapter.updateOne(state, {
        id: action.payload.index,
        changes: { documentHasPreambel: action.payload.documentHasPreambel },
      });
    },
  },
});

export const {
  setEditorDocuments,
  removeDocuments,
  changeSelection,
  setDocumentVisible,
  setColumnSize,
  setDocumentHasPreambel,
} = EditorDocumentsSlice.actions;
