// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { DocumentDTO, DocumentStateType } from '../../../../api';
import { useAppDispatch, useAppSelector } from '../../../../components/Store';
import { EditorPageState } from '../EditorPageState';
import {
  editorDocumentsAdapter,
  EditorDocumentsSlice,
  setEditorDocuments,
  removeDocuments,
  setDocumentVisible,
  setColumnSize,
  setDocumentHasPreambel,
} from './EditorDocumentsSlice';
import { ColumnSize, EditorDocument, InitialEditorDocument, InitialEditorInfoState } from './EditorInfoState';
import { shallowEqual } from 'react-redux';

type EditorReadonlyInfo = {
  documentStateType: DocumentStateType;
  isReadonly: boolean;
};

type UseEditableState = {
  isEditable: boolean;
};
type UseEditorDocumentState = {
  editorDocument: EditorDocument | undefined;
};
type UseEditorDocumentsState = {
  editorDocuments: EditorDocument[] | undefined;
};
type UseEditorDocumentReadonlyState = {
  editorDocumentReadonly: EditorReadonlyInfo | null;
};
type UseEditorDocumentsInfoChange = {
  setDocuments: (documents: DocumentDTO[]) => void;
  removeDocuments: () => void;
};

type UseEditorDocumentVisibleChange = {
  setDocumentsVisible: (visibleList: number[]) => void;
};

type UseDocumentHasPreambelChange = {
  setDocumentHasPreambel: (index: number, documentHasPreambel: boolean) => void;
};

type UseSetColumnSizeChange = {
  setColumnSize: (index: number, size: ColumnSize) => void;
};

export const useEditorDocumentState = (index: number): UseEditorDocumentState => {
  const editorDocument = useAppSelector((state) =>
    editorDocumentsAdapter
      .getSelectors()
      .selectById(
        (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
          InitialEditorInfoState[EditorDocumentsSlice.name],
        index,
      ),
  );

  return {
    editorDocument,
  };
};

export const useEditorDocumentsState = (): UseEditorDocumentsState => {
  const editorDocuments = useAppSelector((state) =>
    editorDocumentsAdapter
      .getSelectors()
      .selectAll(
        (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
          InitialEditorInfoState[EditorDocumentsSlice.name],
      ),
  );

  return {
    editorDocuments,
  };
};

export const useEditorDocumentsVisibleState = (): number[] =>
  useAppSelector(
    (state) =>
      editorDocumentsAdapter
        .getSelectors()
        .selectAll(
          (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
            InitialEditorInfoState[EditorDocumentsSlice.name],
        )
        .filter((editorDocument) => editorDocument.visible)
        .map((editorDocument) => editorDocument.index),
    shallowEqual,
  );

export const useEditorDocumentReadonlyState = (index: number): UseEditorDocumentReadonlyState => {
  const documentStateType =
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.documentStateType,
    ) || InitialEditorDocument.documentStateType;

  const isReadonly =
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.isReadonly,
    ) || InitialEditorDocument.isReadonly;

  const editorDocumentReadonly = { documentStateType, isReadonly } as EditorReadonlyInfo;

  return {
    editorDocumentReadonly,
  };
};

export const useEditableState = (index: number): UseEditableState => {
  const isEditable =
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.isEditable,
    ) || InitialEditorDocument.isEditable;

  const isAuthorialNoteEditable =
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.isAuthorialNoteEditable,
    ) || InitialEditorDocument.isAuthorialNoteEditable;

  return {
    isEditable: !!isEditable || !!isAuthorialNoteEditable,
  };
};

export const useColumnSizeState = (index: number): ColumnSize => {
  return (
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.size,
    ) || InitialEditorDocument.size
  );
};

export const useDocumentHasPreambelState = (index: number): boolean => {
  return (
    useAppSelector(
      (state) =>
        editorDocumentsAdapter
          .getSelectors()
          .selectById(
            (state.editorPage as EditorPageState)?.editorInfoState[EditorDocumentsSlice.name] ||
              InitialEditorInfoState[EditorDocumentsSlice.name],
            index,
          )?.documentHasPreambel,
    ) || InitialEditorDocument.documentHasPreambel
  );
};

export const useEditorDocumentsInfoChange = (): UseEditorDocumentsInfoChange => {
  const dispatch = useAppDispatch();

  const setDocumentsCallback = useCallback((documents: DocumentDTO[]) => {
    dispatch(removeDocuments());
    setTimeout(() => dispatch(setEditorDocuments(documents)));
  }, []);

  const removeDocumentsCallback = useCallback(() => {
    dispatch(removeDocuments());
  }, []);

  return {
    setDocuments: setDocumentsCallback,
    removeDocuments: removeDocumentsCallback,
  };
};

export const useEditorDocumentVisibleChange = (): UseEditorDocumentVisibleChange => {
  const dispatch = useAppDispatch();

  const setDocumentVisibleCallback = useCallback((visibleList: number[]) => {
    dispatch(setDocumentVisible(visibleList));
  }, []);
  return {
    setDocumentsVisible: setDocumentVisibleCallback,
  };
};

export const useColumnSizeChange = (): UseSetColumnSizeChange => {
  const dispatch = useAppDispatch();

  const setColumnSizeCallback = useCallback((index: number, size: ColumnSize) => {
    dispatch(setColumnSize({ index, size }));
  }, []);
  return {
    setColumnSize: setColumnSizeCallback,
  };
};

export const useDocumentHasPreambelChange = (): UseDocumentHasPreambelChange => {
  const dispatch = useAppDispatch();

  const setDocumentHasPreambelCallback = useCallback((index: number, documentHasPreambel: boolean) => {
    dispatch(setDocumentHasPreambel({ index, documentHasPreambel }));
  }, []);
  return {
    setDocumentHasPreambel: setDocumentHasPreambelCallback,
  };
};
