// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { InitialEditorInfoState, EditorInfoState } from './editor-info';
import { EditorWizards, EditorWizardVisibleState } from './editor-wizards';
import { EditorOptionsVisibleState, InitialEditorOptionsVisibleState } from './editor-options';
import {
  InitialTableOfContentsState,
  TableOfContentsState,
  AuthorialNotePageState,
  InitialAuthorialNotePageState,
} from './workspace';
import { CommentPageState, InitialCommentPageState } from './workspace/comment-page';
import { ContextMenuState, InitialContextMenuState } from './workspace/contextMenu';
import { InitialSearchReplaceState, SearchReplaceState } from './workspace/search-replace';
import {
  DynAenderungsvergleichState,
  InitialDynAenderungsvergleichState,
} from './workspace/dynamischer-aenderungsvergleich';
import { CommentState, InitialCommentState } from '../../plugins/comment';

export interface WorkspaceState {
  tableOfContents: TableOfContentsState;
  authorialNotePage: AuthorialNotePageState;
  commentPage: CommentPageState;
  comment: CommentState;
  contextMenu: ContextMenuState;
  editorOptionsVisible: EditorOptionsVisibleState;
  searchReplace: SearchReplaceState;
  dynAenderungsvergleich: DynAenderungsvergleichState;
}

export interface PluginState {
  [name: string]: unknown;
}

export interface EditorPageState {
  editorInfoState: EditorInfoState;
  editorWizardVisible: EditorWizardVisibleState;
  workspace: WorkspaceState;
  pluginState: PluginState;
}

export const InitialEditorPageState: EditorPageState = {
  editorInfoState: InitialEditorInfoState,
  editorWizardVisible: {
    [EditorWizards.AUTHORIALNOTE]: false,
    [EditorWizards.SYNOPSIS]: false,
    [EditorWizards.SYNCHRONOUSSCROLLING]: false,
  },
  workspace: {
    editorOptionsVisible: InitialEditorOptionsVisibleState,
    tableOfContents: InitialTableOfContentsState,
    authorialNotePage: InitialAuthorialNotePageState,
    commentPage: InitialCommentPageState,
    comment: InitialCommentState,
    contextMenu: InitialContextMenuState,
    searchReplace: InitialSearchReplaceState,
    dynAenderungsvergleich: InitialDynAenderungsvergleichState,
  },
  pluginState: {},
};
