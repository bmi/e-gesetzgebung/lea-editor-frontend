// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export enum EditorWizards {
  AUTHORIALNOTE = 'NewCompoundDocumentWizard',
  SYNOPSIS = 'NewDocumentWizard',
  SYNCHRONOUSSCROLLING = ' SynchronousScrolling',
}

export interface EditorWizardVisibleState {
  [EditorWizards.AUTHORIALNOTE]: boolean;
  [EditorWizards.SYNOPSIS]: boolean;
  [EditorWizards.SYNCHRONOUSSCROLLING]: boolean;
}

export const InitialEditorWizardVisibleState: EditorWizardVisibleState = {
  [EditorWizards.AUTHORIALNOTE]: false,
  [EditorWizards.SYNOPSIS]: false,
  [EditorWizards.SYNCHRONOUSSCROLLING]: false,
};
