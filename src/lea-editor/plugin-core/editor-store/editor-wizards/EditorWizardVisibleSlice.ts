// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { InitialEditorWizardVisibleState, EditorWizards } from './EditorWizardVisibleState';

const editorWizardVisibleSlice = createSlice({
  name: 'editorWizardVisible',
  initialState: InitialEditorWizardVisibleState,
  reducers: {
    openWizard(state, action: PayloadAction<EditorWizards>) {
      state[action.payload] = true;
    },
    closeWizard(state, action: PayloadAction<EditorWizards>) {
      state[action.payload] = false;
    },
  },
});

export const { openWizard, closeWizard } = editorWizardVisibleSlice.actions;
export const EditorWizardVisibleSlice = editorWizardVisibleSlice;
