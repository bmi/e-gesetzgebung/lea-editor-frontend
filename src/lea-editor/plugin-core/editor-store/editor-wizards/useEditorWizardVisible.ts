// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../components/Store';
import { EditorPageState } from '../EditorPageState';
import { closeWizard, EditorWizardVisibleSlice, openWizard } from './EditorWizardVisibleSlice';
import { EditorWizards, EditorWizardVisibleState } from './EditorWizardVisibleState';

type UseWizardVisibleState = {
  wizardVisible: boolean;
};

type UseWizardVisibleChange = {
  openWizard: () => void;
  closeWizard: () => void;
};

export const useWizardVisibleState = (wizard: EditorWizards): UseWizardVisibleState => {
  const state = useAppSelector<EditorWizardVisibleState | null>(
    (state) => (state.editorPage as EditorPageState)?.[EditorWizardVisibleSlice.name],
  );

  return { wizardVisible: !!state && state[wizard] };
};

export const useWizardVisibleChange = (wizard: EditorWizards): UseWizardVisibleChange => {
  const dispatch = useAppDispatch();

  const openWizardCallback = useCallback(() => {
    dispatch(openWizard(wizard));
  }, []);

  const closeWizardCallback = useCallback(() => {
    dispatch(closeWizard(wizard));
  }, []);

  return { openWizard: openWizardCallback, closeWizard: closeWizardCallback };
};
