// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice } from '@reduxjs/toolkit';
import { InitialTableOfContentsState } from './TableOfContentsState';

const tableOfContentsSlice = createSlice({
  name: 'tableOfContents',
  initialState: InitialTableOfContentsState,
  reducers: {
    openTableOfContents(state) {
      state.visible = true;
    },
    closeTableOfContents(state) {
      state.visible = false;
    },
    toggleTableOfContents(state) {
      state.visible = !state.visible;
    },
  },
});

export const { openTableOfContents, closeTableOfContents, toggleTableOfContents } = tableOfContentsSlice.actions;
export const TableOfContentsSlice = tableOfContentsSlice;
