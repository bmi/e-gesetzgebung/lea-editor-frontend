// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import { EditorPageState } from '../../EditorPageState';
import {
  closeTableOfContents,
  openTableOfContents,
  TableOfContentsSlice,
  toggleTableOfContents,
} from './TableOfContentsSlice';
import { InitialTableOfContentsState } from './TableOfContentsState';

type UseTableOfContentsState = {
  visible: boolean;
};

type UseTableOfContentsDispatch = {
  toggelTableOfContents: () => void;
  openTableOfContents: () => void;
  closeTableOfContents: () => void;
};

export const useTableOfContentsState = (): UseTableOfContentsState => {
  const visible =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[TableOfContentsSlice.name].visible) ||
    InitialTableOfContentsState.visible;

  return { visible };
};

export const useTableOfContentsDispatch = (): UseTableOfContentsDispatch => {
  const dispatch = useAppDispatch();

  const toggelTableOfContentsCallback = useCallback(() => {
    dispatch(toggleTableOfContents());
  }, []);

  const openTableOfContentsCallback = useCallback(() => {
    dispatch(openTableOfContents());
  }, []);

  const closeTableOfContentsCallback = useCallback(() => {
    dispatch(closeTableOfContents());
  }, []);

  return {
    toggelTableOfContents: toggelTableOfContentsCallback,
    openTableOfContents: openTableOfContentsCallback,
    closeTableOfContents: closeTableOfContentsCallback,
  };
};
