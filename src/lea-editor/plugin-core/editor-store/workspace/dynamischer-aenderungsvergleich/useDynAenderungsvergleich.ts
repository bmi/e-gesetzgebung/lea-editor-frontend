// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import { EditorPageState } from '../../EditorPageState';
import {
  DynAenderungsvergleichSlice,
  disableDynAenderungsvergleich,
  toggleDynAenderungsvergleich,
  toggleTriedToDisableActive,
} from './DynAenderungsvergleichSlice';
import { InitialDynAenderungsvergleichState } from './DynAenderungsvergleichState';

type UseDynAenderungsvergleichState = {
  isActive: boolean;
  triedToDisableActive: boolean;
};

type UseDynAenderungsvergleichDispatch = {
  disableDynAenderungsvergleich: () => void;
  toggleDynAenderungsvergleich: () => void;
  toggleTriedToDisableActive: () => void;
};

export const useDynAenderungsvergleichState = (): UseDynAenderungsvergleichState => {
  const isActive =
    useAppSelector(
      (state) => (state.editorPage as EditorPageState)?.workspace[DynAenderungsvergleichSlice.name].isActive,
    ) || InitialDynAenderungsvergleichState.isActive;

  const triedToDisableActive =
    useAppSelector(
      (state) =>
        (state.editorPage as EditorPageState)?.workspace[DynAenderungsvergleichSlice.name].triedToDisableActive,
    ) || InitialDynAenderungsvergleichState.triedToDisableActive;

  return { isActive, triedToDisableActive };
};

export const useDynAenderungsvergleichDispatch = (): UseDynAenderungsvergleichDispatch => {
  const dispatch = useAppDispatch();

  const disableDynAenderungsvergleichCallback = useCallback(() => {
    dispatch(disableDynAenderungsvergleich());
  }, []);

  const toggelDynAenderungsvergleichCallback = useCallback(() => {
    dispatch(toggleDynAenderungsvergleich());
  }, []);

  const toggelTriedToDisableActiveCallback = useCallback(() => {
    dispatch(toggleTriedToDisableActive());
  }, []);

  return {
    disableDynAenderungsvergleich: disableDynAenderungsvergleichCallback,
    toggleDynAenderungsvergleich: toggelDynAenderungsvergleichCallback,
    toggleTriedToDisableActive: toggelTriedToDisableActiveCallback,
  };
};
