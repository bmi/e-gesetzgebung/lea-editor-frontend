// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice } from '@reduxjs/toolkit';
import { InitialDynAenderungsvergleichState } from './DynAenderungsvergleichState';

const dynAenderungsvergleichSlice = createSlice({
  name: 'dynAenderungsvergleich',
  initialState: InitialDynAenderungsvergleichState,
  reducers: {
    disableDynAenderungsvergleich(state) {
      state.isActive = false;
    },

    toggleDynAenderungsvergleich(state) {
      state.isActive = !state.isActive;
    },

    toggleTriedToDisableActive(state) {
      state.triedToDisableActive = !state.triedToDisableActive;
    },
  },
});

export const { disableDynAenderungsvergleich, toggleDynAenderungsvergleich, toggleTriedToDisableActive } =
  dynAenderungsvergleichSlice.actions;
export const DynAenderungsvergleichSlice = dynAenderungsvergleichSlice;
