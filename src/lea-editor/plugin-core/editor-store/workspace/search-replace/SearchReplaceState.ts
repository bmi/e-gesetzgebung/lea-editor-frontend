// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { SearchForTypes } from '../../../../plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';

export interface SearchReplaceState {
  searchText: string;
  selectedSearchPosition: number;
  searchCount: number;
  searchType: SearchForTypes;
}

export const InitialSearchReplaceState: SearchReplaceState = {
  searchText: '',
  selectedSearchPosition: 0,
  searchCount: 0,
  searchType: SearchForTypes.Text,
};
