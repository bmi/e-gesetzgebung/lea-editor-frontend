// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { InitialSearchReplaceState } from './SearchReplaceState';
import { SearchForTypes } from '../../../../plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';

const searchReplaceSlice = createSlice({
  name: 'searchReplace',
  initialState: InitialSearchReplaceState,
  reducers: {
    setSearchText(state, action: PayloadAction<string>) {
      state.searchText = action.payload;
    },
    setSelectedSearchPosition(state, action: PayloadAction<number>) {
      state.selectedSearchPosition = action.payload;
    },
    setSearchCount(state, action: PayloadAction<number>) {
      state.searchCount = action.payload;
    },
    setSearchType(state, action: PayloadAction<SearchForTypes>) {
      state.searchType = action.payload;
    },
  },
});

export const { setSearchText, setSelectedSearchPosition, setSearchCount, setSearchType } = searchReplaceSlice.actions;
export const SearchReplaceSlice = searchReplaceSlice;
