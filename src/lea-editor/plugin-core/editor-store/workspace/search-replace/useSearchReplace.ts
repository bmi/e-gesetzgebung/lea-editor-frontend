// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import {
  SearchReplaceSlice,
  setSearchText,
  setSelectedSearchPosition,
  setSearchCount,
  setSearchType,
  InitialSearchReplaceState,
} from './';
import { EditorPageState } from '../../EditorPageState';
import { useCallback } from 'react';
import { SearchForTypes } from '../../../../plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';

type UseSearchReplaceState = {
  searchText: string;
  selectedSearchPosition: number;
  searchCount: number;
  searchType: SearchForTypes;
};

type UseSearchReplaceDispatch = {
  setSearchText: (searchText: string) => void;
  setSelectedSearchPosition: (selectedSearchPosition: number) => void;
  setSearchCount: (searchCount: number) => void;
  setSearchType: (searchType: SearchForTypes) => void;
};

export const useSearchReplaceState = (): UseSearchReplaceState => {
  const searchText =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[SearchReplaceSlice.name].searchText) ||
    InitialSearchReplaceState.searchText;

  const selectedSearchPosition =
    useAppSelector(
      (state) => (state.editorPage as EditorPageState)?.workspace[SearchReplaceSlice.name].selectedSearchPosition,
    ) || InitialSearchReplaceState.selectedSearchPosition;

  const searchCount =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[SearchReplaceSlice.name].searchCount) ||
    InitialSearchReplaceState.searchCount;

  const searchType =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[SearchReplaceSlice.name].searchType) ||
    InitialSearchReplaceState.searchType;

  return { searchText, selectedSearchPosition, searchCount, searchType };
};

export const useSearchReplaceDispatch = (): UseSearchReplaceDispatch => {
  const dispatch = useAppDispatch();

  const setSearchTextCallback = useCallback((searchText: string) => {
    dispatch(setSearchText(searchText));
  }, []);

  const setSelectedSearchPositionCallback = useCallback((selectedSearchPosition: number) => {
    dispatch(setSelectedSearchPosition(selectedSearchPosition));
  }, []);

  const setSearchCountCallback = useCallback((searchCount: number) => {
    dispatch(setSearchCount(searchCount));
  }, []);

  const setSearchTypeCallback = useCallback((searchType: SearchForTypes) => {
    dispatch(setSearchType(searchType));
  }, []);

  return {
    setSearchText: setSearchTextCallback,
    setSelectedSearchPosition: setSelectedSearchPositionCallback,
    setSearchCount: setSearchCountCallback,
    setSearchType: setSearchTypeCallback,
  };
};
