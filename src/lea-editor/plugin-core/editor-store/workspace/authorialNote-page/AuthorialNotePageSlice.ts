// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { InitialAuthorialNotePageState } from './AuthorialNotePageState';

import { createSlice } from '@reduxjs/toolkit';

const authorialNotePageSlice = createSlice({
  name: 'authorialNotePage',
  initialState: InitialAuthorialNotePageState,
  reducers: {
    openAuthorialNotePage(state) {
      state.visible = true;
    },
    closeAuthorialNotePage(state) {
      state.visible = false;
    },
    toggleAuthorialNotePage(state) {
      state.visible = !state.visible;
    },
  },
});

export const { openAuthorialNotePage, closeAuthorialNotePage, toggleAuthorialNotePage } =
  authorialNotePageSlice.actions;
export const AuthorialNotePageSlice = authorialNotePageSlice;
