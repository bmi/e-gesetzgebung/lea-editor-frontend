// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import { EditorPageState } from '../../EditorPageState';
import {
  toggleAuthorialNotePage,
  openAuthorialNotePage,
  closeAuthorialNotePage,
  AuthorialNotePageSlice,
} from './AuthorialNotePageSlice';
import { InitialAuthorialNotePageState } from './AuthorialNotePageState';

type UseAuthorialNotePageState = {
  visible: boolean;
};

type UseAuthorialNotePageChange = {
  toggelAuthorialNotePage: () => void;
  openAuthorialNotePage: () => void;
  closeAuthorialNotePage: () => void;
};

export const useAuthorialNotePageState = (): UseAuthorialNotePageState => {
  const visible =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[AuthorialNotePageSlice.name].visible) ||
    InitialAuthorialNotePageState.visible;

  return { visible: visible };
};

export const useAuthorialNotePageChange = (): UseAuthorialNotePageChange => {
  const dispatch = useAppDispatch();

  const toggelAuthorialNotePageCallback = useCallback(() => {
    dispatch(toggleAuthorialNotePage());
  }, []);

  const openAuthorialNotePageCallback = useCallback(() => {
    dispatch(openAuthorialNotePage());
  }, []);

  const closeAuthorialNotePageCallback = useCallback(() => {
    dispatch(closeAuthorialNotePage());
  }, []);

  return {
    toggelAuthorialNotePage: toggelAuthorialNotePageCallback,
    openAuthorialNotePage: openAuthorialNotePageCallback,
    closeAuthorialNotePage: closeAuthorialNotePageCallback,
  };
};
