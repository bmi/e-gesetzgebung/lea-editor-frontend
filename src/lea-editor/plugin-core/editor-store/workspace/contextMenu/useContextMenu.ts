// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { NodeEntry } from 'slate';
import { Element } from '../../../../../interfaces';
import { EditorPageState } from '../../EditorPageState';
import { closeMenu, ContextMenuSlice, openMenu, setTargetNodeEntry } from './ContextMenuSlice';
import { InitialContextMenuState, MenuPosition } from './ContextMenuState';
import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import { EqualityFn } from 'react-redux';

interface OpenContextMenuProps {
  e?: React.MouseEvent<HTMLDivElement> | KeyboardEvent;
  positionTarget?: HTMLElement;
  focusTarget?: HTMLElement;
  nodeEntry?: NodeEntry<Element>;
}

type UseContextMenuChange = {
  openContextMenu: (props: OpenContextMenuProps) => void;
  closeContextMenu: () => void;
  setTargetNodeEntry: (nodeEntry: NodeEntry<Element> | null) => void;
};

type UseContextMenuState = {
  position: MenuPosition | null;
  isOpen: boolean;
  targetId: string;
};

type UseContextMenuNodeEntryState = {
  nodeEntry: NodeEntry<Element> | null;
  targetNodeEntry: NodeEntry<Element> | null;
};

export const useContextMenuChange = (): UseContextMenuChange => {
  const dispatch = useAppDispatch();

  const getTargetIdFromElement = useCallback((element: HTMLElement): string => {
    const id = element.id;
    if (id) return id;
    if (!element.parentElement) return '';
    return getTargetIdFromElement(element.parentElement);
  }, []);

  const openContextMenu = useCallback(({ e, positionTarget, focusTarget, nodeEntry }: OpenContextMenuProps) => {
    const selection = window.getSelection();
    const selectTarget = positionTarget ?? selection?.getRangeAt(0) ?? e?.target;
    let position = {
      xPos: `${(selectTarget as HTMLElement | undefined)?.getBoundingClientRect().left ?? 0}px`,
      yPos: `${(selectTarget as HTMLElement | undefined)?.getBoundingClientRect().top ?? 0}px`,
    };
    if ((e as React.MouseEvent<HTMLDivElement>).pageX && (e as React.MouseEvent<HTMLDivElement>).pageY) {
      position = {
        xPos: `${(e as React.MouseEvent<HTMLDivElement>).pageX}px`,
        yPos: `${(e as React.MouseEvent<HTMLDivElement>).pageY}px`,
      };
    }
    dispatch(
      openMenu({
        position,
        targetId: getTargetIdFromElement(focusTarget ?? (e?.target as HTMLElement)),
        nodeEntry,
      }),
    );
    e?.preventDefault();
    e?.stopPropagation();
  }, []);

  const closeContextMenu = useCallback(() => {
    dispatch(closeMenu());
  }, []);

  const setTargetNodeEntryCallback = useCallback((nodeEntry: NodeEntry<Element> | null) => {
    dispatch(setTargetNodeEntry(nodeEntry));
  }, []);

  return { openContextMenu, closeContextMenu, setTargetNodeEntry: setTargetNodeEntryCallback };
};

export const useContextMenuState = (): UseContextMenuState => {
  const isOpen =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[ContextMenuSlice.name].isOpen) ||
    InitialContextMenuState.isOpen;
  const position =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[ContextMenuSlice.name].position) ??
    InitialContextMenuState.position;
  const targetId =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[ContextMenuSlice.name].targetId) ||
    InitialContextMenuState.targetId;

  return { position, isOpen, targetId };
};

const nodeEntryEquality: EqualityFn<NodeEntry<Element> | null> = (a, b) => {
  return a?.toString() === b?.toString();
};

export const useContextMenuNodeEntryState = (): UseContextMenuNodeEntryState => {
  const nodeEntry =
    useAppSelector<NodeEntry<Element> | null>(
      (state) => (state.editorPage as EditorPageState)?.workspace[ContextMenuSlice.name].nodeEntry,
      nodeEntryEquality,
    ) ?? InitialContextMenuState.nodeEntry;

  const targetNodeEntry =
    useAppSelector(
      (state) => (state.editorPage as EditorPageState)?.workspace[ContextMenuSlice.name].targetNodeEntry,
    ) ?? InitialContextMenuState.targetNodeEntry;

  return {
    nodeEntry,
    targetNodeEntry,
  };
};
