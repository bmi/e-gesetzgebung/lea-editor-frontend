// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { NodeEntry } from 'slate';
import { Element } from '../../../../../interfaces';
import { InitialContextMenuState, MenuPosition } from './ContextMenuState';

type OpenContextMenuPayload = {
  position: MenuPosition;
  targetId?: string;
  nodeEntry?: NodeEntry<Element>;
};

const contextMenuSlice = createSlice({
  name: 'contextMenu',
  initialState: InitialContextMenuState,
  reducers: {
    openMenu(state, action: PayloadAction<OpenContextMenuPayload>) {
      state.isOpen = true;
      state.position = action.payload.position;
      if (action.payload.targetId) {
        state.targetId = action.payload.targetId;
      }
      state.nodeEntry = action.payload.nodeEntry || null;
      state.targetNodeEntry = null;
    },
    closeMenu(state) {
      state.isOpen = InitialContextMenuState.isOpen;
      state.nodeEntry = InitialContextMenuState.nodeEntry;
      state.position = InitialContextMenuState.position;
      state.targetId = InitialContextMenuState.targetId;
    },
    setTargetNodeEntry(state, action: PayloadAction<NodeEntry<Element> | null>) {
      state.targetNodeEntry = action.payload;
    },
  },
});

export const { openMenu, closeMenu, setTargetNodeEntry } = contextMenuSlice.actions;
export const ContextMenuSlice = contextMenuSlice;
