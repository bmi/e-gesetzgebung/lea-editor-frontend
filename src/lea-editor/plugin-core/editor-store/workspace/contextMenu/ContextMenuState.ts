// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { NodeEntry } from 'slate';
import { Element } from '../../../../../interfaces';

export interface MenuPosition {
  xPos: string;
  yPos: string;
}

export interface ContextMenuState {
  nodeEntry: NodeEntry<Element> | null;
  targetId: string;
  isOpen: boolean;
  position: MenuPosition | null;
  targetNodeEntry: NodeEntry<Element> | null;
}

export const InitialContextMenuState: ContextMenuState = {
  nodeEntry: null,
  targetId: '',
  isOpen: false,
  position: null,
  targetNodeEntry: null,
};
