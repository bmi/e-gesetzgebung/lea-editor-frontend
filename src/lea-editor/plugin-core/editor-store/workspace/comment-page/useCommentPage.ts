// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../../components/Store';
import { EditorPageState } from '../../EditorPageState';
import { closeCommentPage, CommentPageSlice, openCommentPage, toggleCommentPage } from './CommentPageSlice';
import { InitialCommentPageState } from './CommentPageState';

type UseCommentPageState = {
  visible: boolean;
};
type UseCommentPageChange = {
  toggelCommentPage: () => void;
  openCommentPage: () => void;
  closeCommentPage: () => void;
};

export const useCommentPageState = (): UseCommentPageState => {
  const visible =
    useAppSelector((state) => (state.editorPage as EditorPageState)?.workspace[CommentPageSlice.name].visible) ||
    InitialCommentPageState.visible;

  return { visible };
};

export const useCommentPageChange = (): UseCommentPageChange => {
  const dispatch = useAppDispatch();

  const toggelCommentPageCallback = useCallback(() => {
    dispatch(toggleCommentPage());
  }, []);

  const openCommentPageCallback = useCallback(() => {
    dispatch(openCommentPage());
  }, []);

  const closeCommentPageCallback = useCallback(() => {
    dispatch(closeCommentPage());
  }, []);

  return {
    toggelCommentPage: toggelCommentPageCallback,
    openCommentPage: openCommentPageCallback,
    closeCommentPage: closeCommentPageCallback,
  };
};
