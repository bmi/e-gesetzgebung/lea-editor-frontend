// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { InitialCommentPageState } from './CommentPageState';
import { createSlice } from '@reduxjs/toolkit';

const commentPageSlice = createSlice({
  name: 'commentPage',
  initialState: InitialCommentPageState,
  reducers: {
    openCommentPage(state) {
      state.visible = true;
    },
    closeCommentPage(state) {
      state.visible = false;
    },
    toggleCommentPage(state) {
      state.visible = !state.visible;
    },
  },
});

export const { openCommentPage, closeCommentPage, toggleCommentPage } = commentPageSlice.actions;
export const CommentPageSlice = commentPageSlice;
