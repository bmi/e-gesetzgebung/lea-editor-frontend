// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export enum EditorOptions {
  COMMENT = 'COMMENT',
  AENDERUNGS_NACHVERFOLGUNG = 'AENDERUNGS_NACHVERFOLGUNG',
  SYNOPSIS = 'SYNOPSIS',
  SEARCH_REPLACE = 'SEARCH_REPLACE',
  HDR_ASSISTENT = 'HDR_ASSISTENT',
}

export interface EditorOptionsVisibleState {
  [EditorOptions.COMMENT]: boolean;
  [EditorOptions.AENDERUNGS_NACHVERFOLGUNG]: boolean;
  [EditorOptions.SYNOPSIS]: boolean;
  [EditorOptions.SEARCH_REPLACE]: boolean;
  [EditorOptions.HDR_ASSISTENT]: boolean;
}

export const InitialEditorOptionsVisibleState: EditorOptionsVisibleState = {
  [EditorOptions.COMMENT]: false,
  [EditorOptions.AENDERUNGS_NACHVERFOLGUNG]: false,
  [EditorOptions.SYNOPSIS]: false,
  [EditorOptions.SEARCH_REPLACE]: false,
  [EditorOptions.HDR_ASSISTENT]: false,
};
