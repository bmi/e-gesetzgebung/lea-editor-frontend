// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../components/Store';
import { EditorPageState } from '../EditorPageState';
import { closeOptions, toggleOptions, EditorOptionsVisibleSlice, openOptions } from './EditorOptionsVisibleSlice';
import { EditorOptions, EditorOptionsVisibleState } from './EditorOptionsVisibleState';

type UseOptionsVisibleState = {
  optionsVisible: boolean;
};

type UseOptionsVisibleChange = {
  openOptions: () => void;
  closeOptions: () => void;
  toggleOptions: () => void;
};

export const useEditorOptionsVisibleState = (options: EditorOptions): UseOptionsVisibleState => {
  const state = useAppSelector<EditorOptionsVisibleState | null>(
    (state) => (state.editorPage as EditorPageState)?.workspace?.[EditorOptionsVisibleSlice.name],
  );

  return { optionsVisible: !!state && state[options] };
};

export const useEditorOptionsVisibleChange = (options: EditorOptions): UseOptionsVisibleChange => {
  const dispatch = useAppDispatch();

  const openOptionsCallback = useCallback(() => {
    dispatch(openOptions(options));
  }, []);

  const closeOptionsCallback = useCallback(() => {
    dispatch(closeOptions(options));
  }, []);

  const toggleOptionsCallback = useCallback(() => {
    dispatch(toggleOptions(options));
  }, []);

  return { openOptions: openOptionsCallback, closeOptions: closeOptionsCallback, toggleOptions: toggleOptionsCallback };
};
