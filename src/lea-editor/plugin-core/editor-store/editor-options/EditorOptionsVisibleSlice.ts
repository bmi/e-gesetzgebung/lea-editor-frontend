// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { InitialEditorOptionsVisibleState, EditorOptions } from './EditorOptionsVisibleState';

const editorOptionsVisibleSlice = createSlice({
  name: 'editorOptionsVisible',
  initialState: InitialEditorOptionsVisibleState,
  reducers: {
    toggleOptions(state, action: PayloadAction<EditorOptions>) {
      const currentState = state[action.payload];
      Object.values(EditorOptions).forEach((option) => {
        state[option] = false;
      });
      state[action.payload] = !currentState;
    },
    openOptions(state, action: PayloadAction<EditorOptions>) {
      Object.values(EditorOptions).forEach((option) => {
        state[option] = false;
      });
      state[action.payload] = true;
    },
    closeOptions(state, action: PayloadAction<EditorOptions>) {
      state[action.payload] = false;
    },
  },
});

export const { openOptions, closeOptions, toggleOptions } = editorOptionsVisibleSlice.actions;
export const EditorOptionsVisibleSlice = editorOptionsVisibleSlice;
