// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseText, Editor, NodeEntry, Transforms } from 'slate';
import { Element } from '../../../interfaces';
import {
  createElementOfType,
  createTextWrapper,
  getLowestNodeAt,
  getLowestNodeEntryAt,
  getLastLocationAt,
} from '../utils';
import { ELEMENT_STRUCTURE } from './ElementStructure';

export class PElementCommands {
  public static mergeAbsaetze(editor: Editor, mergeTypes?: string[]): void {
    const [selectedParagraph, selectedParagraphPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === ELEMENT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    // Currently merge is only allowed if paragraph to be deleted is empty.
    if (selectedParagraph.children.length > 1 || (selectedTextNode && selectedTextNode.text.length > 0)) {
      return;
    }

    // Add text to previous paragraph's text
    const [previousNode, previousNodePath] = Editor.previous(editor, {
      at: selectedParagraphPath,
      match: (n) => (n as Element).type === ELEMENT_STRUCTURE.P || mergeTypes?.includes((n as Element).type) || false,
    }) as NodeEntry<Element>;

    const previousTextNodeEntry = getLowestNodeEntryAt(editor, [...previousNodePath, previousNode.children.length - 1]);
    const [previousTextNode, previousTextNodePath] = previousTextNodeEntry as NodeEntry<BaseText>;

    if (previousTextNode) {
      Transforms.select(editor, getLastLocationAt(editor, previousTextNodePath));
      //Merge TextWrapper and remove parts from nodes
      Transforms.removeNodes(editor, { at: selectedParagraphPath });
    }
  }

  public static insertAbsatz(editor: Editor): void {
    const [selectedParagraph, selectedParagraphPath] = Editor.above(editor, {
      match: (n) => (n as Element).type === ELEMENT_STRUCTURE.P,
    }) as NodeEntry<Element>;

    const selectedTextNode = getLowestNodeAt(editor);

    if (selectedParagraph.children.length === 1 && selectedTextNode && selectedTextNode.text.length === 0) {
      return;
    }

    // Create next sibling's path
    const newPath = selectedParagraphPath.map((x, y) => (y === selectedParagraphPath.length - 1 ? ++x : x));

    // Insert new paragraph
    const newParagraph = createElementOfType(ELEMENT_STRUCTURE.P, [createTextWrapper('')]);
    Transforms.insertNodes(editor, newParagraph, { at: newPath });

    // Select newly created node
    Transforms.select(editor, getLastLocationAt(editor, newPath));
  }
}
