// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useMemo } from 'react';
import { createEditor, Editor, Node, NodeEntry } from 'slate';
import { ReactEditor, RenderElementProps, RenderLeafProps, withReact } from 'slate-react';
import { Divider } from 'antd';
import { v4 as uuid } from 'uuid';

import {
  Plugin,
  IPluginContextProviderProps,
  useEditorDocumentState,
  useSelectionState,
  PluginRenderProps,
  CombinedPluginRenderProps,
  PluginRenderLeafProps,
  PluginRange,
  useDynAenderungsvergleichState,
} from '.';
import {
  isEnter,
  isClipboardEvent,
  isContextMenuCommand,
  isLeftNavigation,
  isRightNavigation,
  isFirstPosition,
  isLastPosition,
} from './utils';
import { Element, Editor as EditorInterface } from '../../interfaces';
import { combineReducers, Reducer } from '@reduxjs/toolkit';
import { ItemType } from 'antd/lib/menu/interface';
import { SearchReplaceEditor } from '../plugins/searchAndReplace/SearchReplaceEditor';
import { withHistory } from 'slate-history';
import { RenderCommentIcon } from './utils/DocumentUtils';

interface IContextMenuProps {
  editor: ReactEditor;
  editorIndex: number;
}

interface INavigationDialogProps {
  editor: ReactEditor;
}

interface IToolBarProps {
  editor: ReactEditor;
}

interface IInsertDropdownProps {
  editor: ReactEditor;
}

interface IOptionDialogProps {
  editor: SearchReplaceEditor | null;
}

interface IRegisterContextProps {
  editor: ReactEditor;
  editorIndex: number;
}

export class PluginRegistry {
  public constructor(private readonly plugins: Plugin[]) {}

  public register(): EditorInterface {
    const editor = withHistory(withReact(createEditor() as SearchReplaceEditor));
    const id = uuid();
    const isRemote = false;
    return { editor: this.plugins.reduce((e, p) => p.create(e), editor), id, isRemote };
  }

  public effectCallback(editor: ReactEditor): void {
    this.plugins.forEach((plugin) => plugin.useEffect(editor));
  }

  public keyDownCallback(editor: Editor, isEditable: boolean): (props: any) => any {
    return (event: KeyboardEvent) => this.keyDown(editor, isEditable, event);
  }

  private keyDown(editor: Editor, isEditable: boolean, event: KeyboardEvent): void {
    if (isClipboardEvent(event)) return;
    if (isContextMenuCommand(event)) return;
    if ((isLeftNavigation(event) && isFirstPosition(editor)) || (isRightNavigation(event) && isLastPosition(editor))) {
      event.preventDefault();
      event.stopPropagation();
      return;
    }
    const keyDownHandled = this.plugins.map((x) => x.keyDown(editor, event));
    if (!keyDownHandled.some((x) => x) || !isEditable || isEnter(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  public copyCallback(editor: SearchReplaceEditor | null): (event: any) => void {
    return (event: ClipboardEvent) => {
      event.preventDefault();
      this.plugins.some((x) => (x.copy ? x.copy(editor) : false));
    };
  }

  public pasteCallback(editor: Editor, isEditable: boolean): (event: any) => void {
    return (event: ClipboardEvent) => {
      if (!isEditable) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.plugins.some((x) => (x.paste ? x.paste(editor, event) : false));
    };
  }

  public cutCallback(editor: Editor, isEditable: boolean): (event: any) => void {
    return (event: ClipboardEvent) => {
      if (!isEditable) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.plugins.some((x) => (x.cut ? x.cut(editor, event) : false));
    };
  }

  public blurCallback(editor: Editor): (props: any) => any {
    return (event: FocusEvent) => this.blur(editor, event);
  }

  public blur(editor: Editor, event: FocusEvent): void {
    this.plugins.forEach((p) => p.blur(editor, event));
  }

  public getToolbarRefs(): React.RefObject<HTMLElement>[] {
    return this.plugins.flatMap((p) => p.toolRefs);
  }

  public getViewToolItemTypes(): ItemType[] {
    return this.plugins.reduce((itemTypes, plugin) => {
      const newItemTypes = plugin.viewToolItemTypes;
      if (newItemTypes) {
        return [...itemTypes, ...newItemTypes];
      }
      return itemTypes;
    }, [] as ItemType[]);
  }

  public getInsertToolItemTypes(): ItemType[] {
    return this.plugins.reduce((itemTypes, plugin) => {
      const newItemTypes = plugin.insertToolItemTypes;
      if (newItemTypes) {
        return [...itemTypes, ...newItemTypes];
      }
      return itemTypes;
    }, [] as ItemType[]);
  }

  public getContextMenuItemTypes(): ItemType[] {
    let isFirst = true;
    return this.plugins.reduce((itemTypes, plugin) => {
      const newItemTypes = plugin.contextMenuItemTypes;
      if (newItemTypes) {
        if (isFirst) {
          isFirst = false;
          return [...itemTypes, ...newItemTypes];
        }
        return [...itemTypes, { type: 'divider' }, ...newItemTypes];
      }
      return itemTypes;
    }, [] as ItemType[]);
  }

  public isEditable(editor: Editor): boolean {
    return this.plugins.some((plugin) => plugin.isEditable(editor));
  }

  public Toolbar = (props: IToolBarProps): ReactElement => {
    //Update Toolbar when selection changes
    useSelectionState();

    return (
      <>
        {this.plugins.map((Plugin) => {
          return Plugin.ToolbarTools ? (
            <React.Fragment key={`ToolFragment${Plugin.pluginId}`}>
              {Plugin.ToolbarTools(props) && (
                <Divider className="lea-toolbar-divider" key={`ToolDivider${Plugin.pluginId}`} type="vertical" />
              )}
              <Plugin.ToolbarTools key={`ToolbarTools_${Plugin.pluginId}`} editor={props.editor} />
            </React.Fragment>
          ) : null;
        })}
      </>
    );
  };

  public ViewTools = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ViewTools ? <Plugin.ViewTools key={`ViewTools_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public InsertTools = ({ editor }: IInsertDropdownProps): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.InsertTools ? (
            <Plugin.InsertTools editor={editor as Editor} key={`InsertTools_${Plugin.pluginId}`} />
          ) : null,
        )}
      </>
    );
  };

  public NavigationDialogs = ({ editor }: INavigationDialogProps): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.NavigationDialog ? (
            <Plugin.NavigationDialog key={`NavigationDialog_${Plugin.pluginId}`} editor={editor} />
          ) : null,
        )}
      </>
    );
  };

  public OptionDialogs = ({ editor }: IOptionDialogProps): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.OptionDialog ? <Plugin.OptionDialog key={`OptionDialog_${Plugin.pluginId}`} editor={editor} /> : null,
        )}
      </>
    );
  };

  public ActionDialogs = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ActionDialog ? <Plugin.ActionDialog key={`ActionDialog_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public ModalDialogs = (): ReactElement => {
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ModalDialog ? <Plugin.ModalDialog key={`ModalDialog_${Plugin.pluginId}`} /> : null,
        )}
      </>
    );
  };

  public ContextMenu = (props: IContextMenuProps): ReactElement => {
    const keyName = `${props.editorIndex}`;
    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.ContextMenu ? (
            <Plugin.ContextMenu key={`ContextMenuContent${Plugin.pluginId}`} keyName={keyName} editor={props.editor} />
          ) : null,
        )}
      </>
    );
  };

  public renderElementCallback(pluginRenderProps: PluginRenderProps, editorIndex: number): (props: any) => any {
    return (props: RenderElementProps) => this.renderElement({ ...props, ...pluginRenderProps }, editorIndex);
  }

  private renderElement = (props: CombinedPluginRenderProps, editorIndex: number): ReactElement => {
    const e = props.element as Element;
    const guid = e.GUID;
    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'];
    const fromEgfa = e['lea:fromEgfa'] ? 'lea:fromEgfa' : '';

    const divider = this.plugins.map((p) => p.renderCells(props));
    for (const p of this.plugins) {
      const element = p.renderElement(props);

      if (element) {
        return (
          <>
            {RenderCommentIcon(e)}
            {element}
            {divider}
          </>
        );
      }
    }

    return (
      <>
        {RenderCommentIcon(e)}
        <div
          id={guid && `${editorIndex}:${guid || ''}`}
          className={`${elementType} ${changeMark || ''} ${fromEgfa}`}
          {...props.attributes}
        >
          {props.children}
        </div>
        {divider}
      </>
    );
  };

  public renderLeafCallback = (editor: ReactEditor) => (props: RenderLeafProps) => this.renderLeaf(props, editor);

  private renderLeaf(props: RenderLeafProps, editor: ReactEditor): ReactElement {
    const { children } = props as { children: ReactElement };
    for (const p of this.plugins) {
      const element = p.renderLeaf({ ...(props as PluginRenderLeafProps), editor });
      if (element) {
        return element;
      }
    }

    return <span {...props.attributes}>{children}</span>;
  }

  public decorateCallback = (editor: SearchReplaceEditor, searchString?: string) => (entry: NodeEntry<Node>) =>
    this.decorate(entry as NodeEntry<Element>, editor, searchString);

  private decorate(entry: NodeEntry<Element>, editor: SearchReplaceEditor, searchString?: string): PluginRange[] {
    let ranges: PluginRange[] = [];
    for (const p of this.plugins) {
      const currentRanges = p.decorate(entry, editor, searchString);
      if (currentRanges) {
        ranges = [...ranges, ...currentRanges];
      }
    }
    return ranges;
  }

  // Arrow Function Property instead of method to make sure "this" is bound to the PluginRegistry instance
  // https://www.typescriptlang.org/docs/handbook/2/classes.html#arrow-functions
  public stateReducer = (): Reducer | null => {
    const reducers = this.plugins.map((p) => p.reducer());
    const reducerObject = reducers.reduce((obj, item) => {
      return { ...obj, ...item };
    }, {});
    return Object.keys(reducerObject).length > 0 ? combineReducers({ ...reducerObject }) : null;
  };

  public PluginContextProvider = (props: IPluginContextProviderProps): ReactElement => {
    return this.plugins.reduce(
      (children, Plugin) =>
        Plugin.PluginContextProvider ? Plugin.PluginContextProvider({ children }) || <>{children}</> : <>{children}</>,
      <>{props.children}</>,
    );
  };

  public RegisterContext = ({ editorIndex, editor }: IRegisterContextProps): ReactElement => {
    const { editorDocument } = useEditorDocumentState(editorIndex);
    const { isActive } = useDynAenderungsvergleichState();
    useMemo(() => {
      if (editorDocument) {
        this.plugins.forEach((p) => p.update(editorDocument, isActive));
      }
    }, [editorDocument, isActive]);

    useEffect(() => {
      this.effectCallback(editor);
    }, []);

    return (
      <>
        {this.plugins.map((Plugin) =>
          Plugin.RegisterDispatchFunctions ? (
            <Plugin.RegisterDispatchFunctions key={`RegisterDispatchFunctions_${Plugin.pluginId}`} />
          ) : null,
        )}
      </>
    );
  };
}
