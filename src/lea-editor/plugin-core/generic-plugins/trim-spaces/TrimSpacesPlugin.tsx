// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor } from 'slate';
import { PluginProps, Plugin } from '../../Plugin';
import { EditableQueries as Queries } from '../../utils/EditableQueries';
import { trimSpaces } from '../../utils';

export class TrimSpacesPlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }
  public create<T extends Editor>(e: T): T {
    return e;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor, this.editorDocument.documentType, this.isAuthorialNotePlugin);
  }

  public blur(editor: Editor, event: FocusEvent): void {
    if (this.isEditable(editor) && event.relatedTarget) {
      trimSpaces(editor, event.relatedTarget);
    }
  }
}
