// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { ReactI18NextChild, useTranslation } from 'react-i18next';
import { TableIcon, IconDropdown } from '@lea/ui';

import {
  CombinedPluginRenderProps,
  IPluginContextMenuProps,
  IPluginToolbarProps,
  Plugin,
  PluginProps,
  useEditableState,
  useEditorOnFocusState,
} from '../..';
import { Element } from '../../../../interfaces';
import {
  isCutPaste,
  isEnter,
  isEscape,
  isFirstPosition,
  isFirstSiblingOfType,
  isLastPosition,
  isOfType,
  isTab,
  isWithinNodeOfType,
  isWithinTypeHierarchy,
  Unit,
} from '../../utils';
import { TabelleQueries as Queries, TabelleCommands as Commands, GridSizeSelector, TABELLE_STRUCTURE } from '.';
import { TabelleContextMenu } from './TabelleContextMenu';
import { PElementCommands } from '../../element-commands/PElementCommands';
import { ItemType } from 'antd/lib/menu/interface';
import { STRUCTURE_ELEMENTS } from '../../utils/StructureElements';

export class TabellePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  private _contextMenuItemTypes: ItemType[] | null = [];

  public set contextMenuItemTypes(itemTypes: ItemType[] | null) {
    this._contextMenuItemTypes = itemTypes;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return this._contextMenuItemTypes;
  }

  public create<T extends Editor>(e: T): T & ReactEditor {
    const editor = e as T & ReactEditor;
    const { deleteBackward, deleteForward } = editor;

    if (this.editorDocument.isReadonly) {
      return editor;
    }

    editor.deleteBackward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteBackward(unit);
        return;
      }
      if (!isFirstPosition(editor)) {
        deleteBackward(unit);
      } else if (isFirstPosition(editor) && !isFirstSiblingOfType(editor, TABELLE_STRUCTURE.P)) {
        PElementCommands.mergeAbsaetze(editor);
      }
    };
    editor.deleteForward = (unit: Unit) => {
      if (!Queries.isEditable(editor)) {
        deleteForward(unit);
        return;
      }
      if (!isLastPosition(editor)) {
        deleteForward(unit);
      }
    };

    return editor;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (this.editorDocument.isReadonly || !Queries.isEditable(editor)) {
      return false;
    }

    if (isEnter(event) && isLastPosition(editor) && !Queries.isCaption(editor)) {
      PElementCommands.insertAbsatz(editor);
      event.preventDefault();
      return false;
    }

    if (isCutPaste(event) || isEnter(event)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  public ToolbarTools = ({ editor }: IPluginToolbarProps): ReactElement | null => {
    const { t } = useTranslation();
    const editorOnFocus = useEditorOnFocusState();
    const { isEditable } = useEditableState(this.editorDocument.index);
    const [isDisabled, setIsDisabled] = useState(false);
    const [hasHover, setHasHover] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const buttonRef = useRef<HTMLButtonElement>(null);

    const insertTable = (rows: number, cols: number) => {
      if (isOfType(editor, TABELLE_STRUCTURE.P) && !isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE)) {
        Commands.insertTable(editor, this.editorDocument.documentType, rows, cols);
        ReactEditor.focus(editor);
        setIsActive(false);
      }
    };

    useEffect(() => {
      buttonRef.current?.addEventListener('focus', () => setHasHover(true));
      buttonRef.current?.addEventListener('blur', () => {
        setHasHover(false);
      });
    }, []);

    useEffect(() => {
      const isWithinPElement = isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.P]);
      const isWithinTable = isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE);
      const disabled = this.editorDocument.isReadonly || !isWithinPElement || isWithinTable || !isEditable;
      setIsDisabled(disabled);
      if (isDisabled && !disabled) {
        setHasHover(false);
        setIsActive(false);
      }
    }, [isEditable, editor.selection]);

    const keyDownHandler = (e: React.KeyboardEvent<HTMLElement>) => {
      const event = e.nativeEvent;
      if (isTab(event)) {
        event.preventDefault();
        event.stopPropagation();
      }
      if (isEscape(event)) {
        setTimeout(() => buttonRef.current?.focus());
      }
    };

    const getItemTypes = (): ItemType[] => {
      return [
        {
          key: 'lea-insert-table-button',
          title: t('lea.toolbar.labels.tables.insertTable'),
          label: t('lea.toolbar.labels.tables.insertTable'),
          onClick: () => insertTable(2, 2),
        },
        {
          key: 'lea-select-table-size-button',
          label: <GridSizeSelector rows={12} cols={12} onCellClick={insertTable} />,
        },
      ];
    };

    return editorOnFocus.index === this.editorDocument.index && !this.editorDocument.isReadonly ? (
      <IconDropdown
        id="table-dropdown"
        buttonRef={buttonRef}
        items={getItemTypes()}
        icon={<TableIcon hasHover={!isDisabled && hasHover} active={isActive} />}
        ariaLabel={t('lea.toolbar.labels.tables.ariaLabel')}
        disabled={isDisabled}
        keyDownHandler={keyDownHandler}
        onHover={setHasHover}
        onActive={setIsActive}
        isActive={isActive}
        hasHover={!isDisabled && hasHover}
      />
    ) : null;
  };

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementPath = ReactEditor.findPath(props.editor, e);
    const elementType = e.type ?? '';
    const changeMark = e['lea:changeMark'] ?? '';
    const guid = e.GUID;
    const fromEgfa = e['lea:fromEgfa'] ? 'lea:fromEgfa' : '';
    const colspan = e.colspan;

    if (
      Queries.isEditable(props.editor, elementPath, elementType) ||
      (e['lea:editable'] && elementType !== STRUCTURE_ELEMENTS.TBLOCK)
    ) {
      return !this.editorDocument.isReadonly ? (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark} contentEditable ${fromEgfa}`}
          contentEditable={true}
          suppressContentEditableWarning={true}
          role={'textbox'}
          {...props.attributes}
        >
          {props.children}
        </div>
      ) : (
        <div
          id={guid && `${this.editorDocument.index}:${guid}`}
          className={`${elementType} ${changeMark}`}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    } else {
      switch (elementType) {
        case TABELLE_STRUCTURE.TABLE: {
          const children = e.children as Element[];
          const captionShift = Number(children[0].type === TABELLE_STRUCTURE.TABLE_CAPTION);
          const hasHeader = children?.some((x) =>
            x.children?.some((y) => (y as Element).type === TABELLE_STRUCTURE.TABLE_HEADERCELL),
          );
          return (
            <table
              id={guid && `${this.editorDocument.index}:${guid}`}
              className={`${elementType} ${changeMark}`}
              {...props.attributes}
            >
              {captionShift === 1 && <caption>{(props.children as ReactI18NextChild[])[0]} </caption>}
              {children && hasHeader && <thead>{(props.children as ReactI18NextChild[])[captionShift]}</thead>}
              {children && children.length > 1 && hasHeader && (
                <tbody>{(props.children as ReactI18NextChild[]).slice(1 + captionShift)}</tbody>
              )}
              {children && children.length > 1 && !hasHeader && (
                <tbody>{(props.children as ReactI18NextChild[]).slice(captionShift)}</tbody>
              )}
            </table>
          );
        }
        case TABELLE_STRUCTURE.TABLE_ROW:
          return (
            <tr
              id={guid && `${this.editorDocument.index}:${guid}`}
              className={`${elementType} ${changeMark}`}
              {...props.attributes}
            >
              {props.children}
            </tr>
          );
        case TABELLE_STRUCTURE.TABLE_HEADERCELL:
          return (
            <th
              id={guid && `${this.editorDocument.index}:${guid}`}
              className={`${elementType} ${changeMark}`}
              colSpan={colspan}
              data-colspan={colspan}
              {...props.attributes}
            >
              {props.children}
            </th>
          );
        case TABELLE_STRUCTURE.TABLE_CELL:
          return (
            <td
              id={guid && `${this.editorDocument.index}:${guid}`}
              className={`${elementType} ${changeMark}`}
              colSpan={colspan}
              data-colspan={colspan}
              {...props.attributes}
            >
              {props.children}
            </td>
          );

        default:
          return null;
      }
    }
  };

  public ContextMenu = ({ keyName, editor }: IPluginContextMenuProps) =>
    TabelleContextMenu({
      editor,
      keyName,
      plugin: this,
    });
}
