// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Ancestor, Editor, NodeEntry, Transforms, Path, Node } from 'slate';

import { Element } from '../../../../interfaces';
import { createElementOfType, createTextWrapper, getLastLocationAt, getNodesOfTypeAt, isOfType } from '../../utils';
import { AuthorialNoteCommands } from '../../../plugins/authorialNote/AuthorialNoteCommands';
import { TABELLE_STRUCTURE } from './TabelleStructure';
import { DocumentType } from '../../../../api/Document/Document';

export class TabelleCommands {
  public static insertTable(editor: Editor, documentType: DocumentType, rows: number, cols: number): void {
    // Get current paragraph
    const [currentParagraph] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === TABELLE_STRUCTURE.P,
    });

    if (!currentParagraph) {
      return;
    }
    const [, currentParagraphPath] = currentParagraph as NodeEntry<Element>;
    let newPath = [...currentParagraphPath];

    // Check for text content
    const currentTextNodeString = Node.string(currentParagraph[0]);

    if (currentTextNodeString.length > 0 || documentType.includes('REGELUNGSTEXT')) {
      // If current paragraph contains text -> create next one
      newPath = currentParagraphPath.map((x, y) => (y === currentParagraphPath.length - 1 ? x + 1 : x));
    } else {
      // If current paragraph is empty -> remove it
      Transforms.removeNodes(editor, { at: currentParagraphPath });
    }

    // Create and insert new table
    const table = TabelleCommands.createTable(rows, cols);
    Transforms.insertNodes(editor, table, { at: newPath });

    TabelleCommands.setSelectionAtFirstCell(editor, newPath);
  }

  public static insertRow(editor: Editor, rowIndexOffset: number) {
    // Get currently selected row
    const [selectedRow] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE_ROW,
      mode: 'lowest',
    });
    const [, rowPath] = selectedRow as NodeEntry<Ancestor>;

    //Get all columns of current row to get row length
    const cells = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL], rowPath);
    const isHeaderRow = isOfType(editor, TABELLE_STRUCTURE.TABLE_HEADERCELL);

    const countCells = cells.filter((cell) => cell[0].colspan == 1).length;

    const count = countCells / 2;

    cells.splice(1, count);

    //create new Row with cells
    const newCells: Element[] = [];

    cells.forEach(() => newCells.push(TabelleCommands.createTableCell('', { colspan: 2 })));

    const newRow: Element = createElementOfType(TABELLE_STRUCTURE.TABLE_ROW, newCells);

    // Create offset path
    const newPath = rowPath.map((x, y) => (y === rowPath.length - 1 ? x + rowIndexOffset : x));

    //Insert new row
    Transforms.insertNodes(editor, newRow, { at: newPath });

    //Format Header if changed
    if (isHeaderRow) {
      TabelleCommands.formatHeader(editor);
    }

    //set focus
    TabelleCommands.setSelectionAtFirstCell(editor, newPath);
  }

  public static insertColumn(editor: Editor, columnIndexOffset: number) {
    // Get currently selected cell
    const [selectedCell] = Editor.nodes(editor, {
      match: (n) =>
        (n as Element).type === TABELLE_STRUCTURE.TABLE_CELL ||
        (n as Element).type === TABELLE_STRUCTURE.TABLE_HEADERCELL,
      mode: 'lowest',
    });

    const [selectedRow] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE_ROW,
      mode: 'lowest',
    });

    // Get new column index
    const [, cellPath] = selectedCell as NodeEntry<Element>;
    const [, selectedRowPath] = selectedRow as NodeEntry<Ancestor>;
    const cells = getNodesOfTypeAt(
      editor,
      [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL],
      selectedRowPath,
    );
    let selectedCellIndex = cellPath[cellPath.length - 1];
    const cellsLeft = [...cells];
    cellsLeft.splice(selectedCellIndex, Infinity);

    const countCells = cellsLeft.filter((cell) => cell[0].colspan == 1).length;

    let countSplit = countCells / 2;
    let newColumnIndex = 0;
    let columnIndex = 0;

    // check if right cell
    if (countCells % 2 !== 0) {
      selectedCellIndex = selectedCellIndex - 1;
      countSplit = countSplit - 0.5;
    }

    // Get currently selected table
    const [selectedTable] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE,
      mode: 'lowest',
    });

    //Get rows
    const [, tablePath] = selectedTable as NodeEntry<Ancestor>;
    const rows = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_ROW], tablePath);
    const [, firstRowPath] = rows[0] as NodeEntry<Ancestor>;
    let firstCellNewColumn = 0;

    for (let i = 0; i < rows.length; i++) {
      const [, rowPath] = rows[i] as NodeEntry<Ancestor>;
      if (JSON.stringify(selectedRowPath) === JSON.stringify(rowPath)) {
        if (columnIndexOffset === 1) {
          if (cells[selectedCellIndex][0].colspan === 1) {
            newColumnIndex = selectedCellIndex + 2;
          } else {
            newColumnIndex = selectedCellIndex + 1;
          }
        } else {
          newColumnIndex = selectedCellIndex;
        }

        const newCellPath = rowPath.concat(newColumnIndex);
        Transforms.insertNodes(
          editor,
          i == 0
            ? TabelleCommands.createHeaderCell('', { colspan: 2 })
            : TabelleCommands.createTableCell('', { colspan: 2 }),
          { at: newCellPath },
        );
      } else {
        const otherRowCells = getNodesOfTypeAt(
          editor,
          [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL],
          rowPath,
        );
        const otherRowCellsLeft = [...otherRowCells];
        otherRowCellsLeft.splice(selectedCellIndex, Infinity);
        const otherRowCountCells = otherRowCellsLeft.filter((cell) => cell[0].colspan == 1).length;
        let otherRowCountSplit = otherRowCountCells / 2;
        if (otherRowCountCells % 2 !== 0) {
          otherRowCountSplit = otherRowCountSplit - 0.5;
          if (countSplit - otherRowCountSplit == 0) {
            columnIndex = selectedCellIndex + 1;
          } else {
            columnIndex = selectedCellIndex - (countSplit - otherRowCountSplit);
          }
        } else {
          if (countSplit == 0 && otherRowCountSplit > 0 && otherRowCells[selectedCellIndex][0].colspan === 1) {
            columnIndex = selectedCellIndex + 1 - (countSplit - otherRowCountSplit);
          } else {
            columnIndex = selectedCellIndex - (countSplit - otherRowCountSplit);
          }
        }

        const rowCells = [...otherRowCells];
        const rowCellsLeft = [...otherRowCells].splice(columnIndex, Infinity);
        const rowCountCells = rowCellsLeft.filter((cell) => cell[0].colspan == 1).length;

        if (columnIndexOffset === 0) {
          if (rowCells[columnIndex][0].colspan === 1) {
            if (rowCountCells % 2 == 0) {
              newColumnIndex = columnIndex;
            } else {
              newColumnIndex = columnIndex - 1;
            }
          } else {
            newColumnIndex = columnIndex;
          }
        } else {
          if (rowCells[columnIndex][0].colspan === 1) {
            if (rowCountCells % 2 == 0) {
              newColumnIndex = columnIndex + 2;
            } else {
              newColumnIndex = columnIndex + 1;
            }
          } else {
            newColumnIndex = columnIndex + 1;
          }
        }

        const newCellPath = rowPath.concat(newColumnIndex);
        Transforms.insertNodes(
          editor,
          i == 0
            ? TabelleCommands.createHeaderCell('', { colspan: 2 })
            : TabelleCommands.createTableCell('', { colspan: 2 }),
          { at: newCellPath },
        );
      }

      if (JSON.stringify(firstRowPath) === JSON.stringify(rowPath)) {
        firstCellNewColumn = newColumnIndex;
      }
    }

    // Get path of first column cell
    const newPath = firstRowPath.concat(firstCellNewColumn);
    // Set focus
    setTimeout(() => {
      TabelleCommands.setSelectionAtFirstCell(editor, newPath);
    }, 10);
  }

  public static splitCell(editor: Editor) {
    // Get currently selected cell row
    const [selectedRow] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE_ROW,
      mode: 'lowest',
    });

    const [selectedCell] = Editor.nodes(editor, {
      match: (n) =>
        (n as Element).type === TABELLE_STRUCTURE.TABLE_CELL ||
        (n as Element).type === TABELLE_STRUCTURE.TABLE_HEADERCELL,
      mode: 'lowest',
    });

    const [selectedP] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.P,
      mode: 'lowest',
    });

    const [, rowPath] = selectedRow as NodeEntry<Element>;
    const [cellNode, cellPath] = selectedCell as NodeEntry<Element>;
    const [pNode] = selectedP as NodeEntry<Element>;
    const oldCellPath = [...cellPath];
    const oldCellText = pNode.children[0].children[0].text ? pNode.children[0].children[0].text : '';
    if (cellNode.colspan === 2) {
      const newCellPath = rowPath.concat(cellPath[cellPath.length - 1] + 1);
      Transforms.removeNodes(editor, { at: cellPath });
      if (cellNode.type === 'akn:th') {
        Transforms.insertNodes(editor, TabelleCommands.createHeaderCell(oldCellText, { colspan: 1 }), {
          at: oldCellPath,
        });
        Transforms.insertNodes(editor, TabelleCommands.createHeaderCell('', { colspan: 1 }), { at: newCellPath });
      } else {
        Transforms.insertNodes(editor, TabelleCommands.createTableCell(oldCellText, { colspan: 1 }), {
          at: oldCellPath,
        });
        Transforms.insertNodes(editor, TabelleCommands.createTableCell('', { colspan: 1 }), { at: newCellPath });
      }
    } else {
      return;
    }
    TabelleCommands.setSelectionAtFirstCell(editor, oldCellPath);
  }

  public static createTable(rows: number, cols: number) {
    const headercells: Element[] = [];
    for (let i = 0; i < cols; i++) {
      headercells.push(TabelleCommands.createHeaderCell('', { colspan: 2 }));
    }
    const tablerows: Element[] = [];
    const headerrow = createElementOfType(TABELLE_STRUCTURE.TABLE_ROW, headercells);
    tablerows.push(headerrow);
    let cells: Element[] = [];
    for (let i = 0; i < rows - 1; i++) {
      cells = [];
      for (let j = 0; j < cols; j++) {
        cells.push(TabelleCommands.createTableCell('', { colspan: 2 }));
      }
      tablerows.push(createElementOfType(TABELLE_STRUCTURE.TABLE_ROW, cells));
    }

    return createElementOfType(TABELLE_STRUCTURE.TABLE, tablerows);
  }

  public static deleteTable(editor: Editor, documentType: DocumentType) {
    const [table] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === TABELLE_STRUCTURE.TABLE,
    });

    if (table) {
      const [, path] = table as NodeEntry<Element>;
      Transforms.removeNodes(editor, { at: path });
      AuthorialNoteCommands.renumberAuthorialNotes(editor);
      // Insert paragraph instead and select it
      if (!documentType.includes('REGELUNGSTEXT')) {
        const p = createElementOfType(TABELLE_STRUCTURE.P, [createTextWrapper('')]);
        Transforms.insertNodes(editor, p, { at: path });

        Transforms.select(editor, getLastLocationAt(editor, path));
      } else {
        const newPath = path.map((x, y) => (y === path.length - 1 ? x - 1 : x));
        Transforms.select(editor, getLastLocationAt(editor, newPath));
      }
    }
  }

  public static deleteColumn(editor: Editor, documentType: DocumentType) {
    // Get currently selected table
    const [selectedTable] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE,
      mode: 'lowest',
    });

    //Get all rows
    const [, tablePath] = selectedTable as NodeEntry<Ancestor>;
    const rows = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_ROW], tablePath);

    // delete whole table when last column
    const [firstRow, firstRowPath] = rows[0];
    if (firstRow.children.length <= 1) {
      TabelleCommands.deleteTable(editor, documentType);
      return;
    }

    // Get currently selected cell
    const [selectedCell] = Editor.nodes(editor, {
      match: (n) =>
        (n as Element).type === TABELLE_STRUCTURE.TABLE_CELL ||
        (n as Element).type === TABELLE_STRUCTURE.TABLE_HEADERCELL,
      mode: 'lowest',
    });

    const [selectedRow] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE_ROW,
      mode: 'lowest',
    });

    // Get column index
    const [cellNode, cellPath] = selectedCell as NodeEntry<Element>;
    const [, selectedRowPath] = selectedRow as NodeEntry<Ancestor>;
    let columnIndex = 0;

    const cells = getNodesOfTypeAt(
      editor,
      [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL],
      selectedRowPath,
    );
    let selectedCellIndex = cellPath[cellPath.length - 1];
    const cellsLeft = [...cells];
    cellsLeft.splice(selectedCellIndex, Infinity);

    const countCells = cellsLeft.filter((cell) => cell[0].colspan == 1).length;

    let countSplit = countCells / 2;

    // check if right cell
    if (countCells % 2 !== 0) {
      selectedCellIndex = selectedCellIndex - 1;
      countSplit = countSplit - 0.5;
    }

    let firstRowColumn = 0;

    //Delete all Cells of Column
    rows.forEach((row) => {
      const [, rowPath] = row as NodeEntry<Ancestor>;
      if (JSON.stringify(selectedRowPath) === JSON.stringify(rowPath)) {
        columnIndex = selectedCellIndex;
        if (cellNode.colspan === 1) {
          for (let i = 0; i < 2; i++) {
            const currentSplitCellPath = rowPath.concat(selectedCellIndex);
            Transforms.removeNodes(editor, { at: currentSplitCellPath, hanging: false });
          }
        } else {
          const currentCellPath = rowPath.concat(selectedCellIndex);
          Transforms.removeNodes(editor, { at: currentCellPath, hanging: false });
        }
      } else {
        const otherRowCells = getNodesOfTypeAt(
          editor,
          [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL],
          rowPath,
        );
        const otherRowCellsLeft = [...otherRowCells];
        otherRowCellsLeft.splice(selectedCellIndex, Infinity);
        const otherRowCountCells = otherRowCellsLeft.filter((cell) => cell[0].colspan == 1).length;
        let otherRowCountSplit = otherRowCountCells / 2;
        if (otherRowCountCells % 2 !== 0) {
          otherRowCountSplit = otherRowCountSplit - 0.5;
          if (countSplit - otherRowCountSplit == 0) {
            columnIndex = selectedCellIndex + 1;
          } else {
            columnIndex = selectedCellIndex - (countSplit - otherRowCountSplit);
          }
        } else {
          if (countSplit == 0 && otherRowCountSplit > 0 && otherRowCells[selectedCellIndex][0].colspan === 1) {
            columnIndex = selectedCellIndex + 1 - (countSplit - otherRowCountSplit);
          } else {
            columnIndex = selectedCellIndex - (countSplit - otherRowCountSplit);
          }
        }

        const rowCellsLeft = [...otherRowCells].splice(columnIndex, Infinity);
        const rowCountCells = rowCellsLeft.filter((cell) => cell[0].colspan == 1).length;

        if (otherRowCells[columnIndex][0].colspan === 1) {
          if (rowCountCells % 2 == 0) {
            for (let i = 0; i < 2; i++) {
              const otherRowCellPath = rowPath.concat(columnIndex);
              Transforms.removeNodes(editor, { at: otherRowCellPath, hanging: false });
            }
          } else {
            for (let i = 0; i < 2; i++) {
              const otherRowCellPath = rowPath.concat(columnIndex - 1);
              Transforms.removeNodes(editor, { at: otherRowCellPath, hanging: false });
            }
          }
        } else {
          const otherRowCellPath = rowPath.concat(columnIndex);
          Transforms.removeNodes(editor, { at: otherRowCellPath, hanging: false });
        }
      }
      if (JSON.stringify(firstRowPath) === JSON.stringify(rowPath)) {
        firstRowColumn = columnIndex;
      }
    });

    // Get new column path
    let newPath = [];

    if (firstRowColumn === 0) {
      newPath = firstRowPath.concat(firstRowColumn);
    } else {
      newPath = firstRowPath.concat(firstRowColumn - 1);
    }

    // Reorder authorial notes
    AuthorialNoteCommands.renumberAuthorialNotes(editor);

    // Set Focus
    TabelleCommands.setSelectionAtFirstCell(editor, newPath);
  }

  public static deleteRow(editor: Editor, documentType: DocumentType) {
    // Get currently selected table
    const [selectedTable] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE,
      mode: 'lowest',
    });

    //Get all rows
    const [, tablePath] = selectedTable as NodeEntry<Ancestor>;
    const rows = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_ROW], tablePath);

    //Delete whole Table if last row
    if (rows.length <= 1) {
      TabelleCommands.deleteTable(editor, documentType);
      return;
    }

    // Get currently selected row and delete
    const [selectedRow] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE_ROW,
      mode: 'lowest',
    });
    const [, rowPath] = selectedRow as NodeEntry<Ancestor>;
    const isHeaderRow = isOfType(editor, TABELLE_STRUCTURE.TABLE_HEADERCELL);
    Transforms.removeNodes(editor, { at: rowPath, hanging: false });

    // Format Header if changed
    if (isHeaderRow) {
      TabelleCommands.formatHeader(editor);
    }

    //Get new Row Path
    let newPath = rowPath;
    if (rows.length - 1 === rowPath[rowPath.length - 1]) {
      newPath = newPath.map((x, y) => (y === newPath.length - 1 ? --x : x));
    }
    // Reorder authorial notes
    AuthorialNoteCommands.renumberAuthorialNotes(editor);

    // Set Focus
    TabelleCommands.setSelectionAtFirstCell(editor, newPath);
  }

  public static formatHeader(editor: Editor) {
    // Get currently selected table
    const [selectedTable] = Editor.nodes(editor, {
      match: (n) => (n as Element).type === TABELLE_STRUCTURE.TABLE,
      mode: 'lowest',
    });

    //Get all rows
    const [, tablePath] = selectedTable as NodeEntry<Ancestor>;
    const rows = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_ROW], tablePath);

    //Get first two rows to rerender
    const renderRows = rows.slice(0, 2);
    for (let i = 0; i < renderRows.length; i++) {
      const [, rowPath] = renderRows[i] as NodeEntry<Ancestor>;
      //Get all cells of row
      const cells = getNodesOfTypeAt(
        editor,
        [TABELLE_STRUCTURE.TABLE_CELL, TABELLE_STRUCTURE.TABLE_HEADERCELL],
        rowPath,
      );
      //reformat type
      cells.forEach((cell) => {
        const [, cellPath] = cell as NodeEntry<Ancestor>;
        Transforms.setNodes(
          editor,
          { type: i === 0 ? TABELLE_STRUCTURE.TABLE_HEADERCELL : TABELLE_STRUCTURE.TABLE_CELL } as Element,
          { at: cellPath },
        );
      });
    }
  }

  public static setSelectionAtFirstCell(editor: Editor, newPath: Path) {
    // Determine first cell path
    const [p] = Editor.nodes(editor, {
      match: (x) => (x as Element).type === TABELLE_STRUCTURE.P,
      at: newPath,
    });

    if (!p) {
      return;
    }
    const [, selectionPath] = p as NodeEntry<Element>;

    // Select first cell
    Transforms.select(editor, selectionPath);
  }

  public static createHeaderCell(text: string, attributes?: Partial<Element>): Element {
    const p = createElementOfType(TABELLE_STRUCTURE.P, [createTextWrapper(text)]);
    return createElementOfType(TABELLE_STRUCTURE.TABLE_HEADERCELL, [p], undefined, attributes);
  }

  public static createTableCell(text: string, attributes?: Partial<Element>): Element {
    const p = createElementOfType(TABELLE_STRUCTURE.P, [createTextWrapper(text)]);
    return createElementOfType(TABELLE_STRUCTURE.TABLE_CELL, [p], undefined, attributes);
  }
}
