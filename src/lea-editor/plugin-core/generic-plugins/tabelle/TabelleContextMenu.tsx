// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ReactEditor } from 'slate-react';
import { TableRowNode } from '../../../../interfaces';
import { MAX_COLUMN_COUNT } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { getNodesOfTypeAt, isWithinNodeOfType } from '../../utils';
import { NodeEntry } from 'slate';
import './TabelleContextMenu.less';

import { TabellePlugin, TabelleCommands as Commands, TABELLE_STRUCTURE } from '.';
import { IPluginContextMenuProps, useEditorOnFocusState, useSelectionState } from '../..';

interface ITabelleContextMenuProps extends IPluginContextMenuProps {
  plugin: TabellePlugin;
}

export const TabelleContextMenu = ({ keyName, editor, plugin }: ITabelleContextMenuProps): ReactElement => {
  const { t } = useTranslation();
  const editorOnFocus = useEditorOnFocusState();
  const { selection } = useSelectionState();
  const [isTableSelected, setIsTableSelected] = useState(false);
  const [isCaptionSelected, setIsCaptionSelected] = useState(false);

  const [additionalColumnsAllowed, setAdditionalColumnsAllowed] = useState(false);

  useEffect(() => {
    const isTableSelected = isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE);
    const isCaptionSelected = isTableSelected && isWithinNodeOfType(editor, TABELLE_STRUCTURE.TABLE_CAPTION);
    const selectedRows = getNodesOfTypeAt(editor, [TABELLE_STRUCTURE.TABLE_ROW]);

    if (!isTableSelected || isCaptionSelected) {
      setIsTableSelected(isTableSelected);
      setIsCaptionSelected(isCaptionSelected);
      return;
    }

    let additionalAllowed = false;
    if (selectedRows.length > 0) {
      const [currentRow] = selectedRows[0] as NodeEntry<TableRowNode>;
      additionalAllowed = currentRow.children.length < MAX_COLUMN_COUNT;
      setAdditionalColumnsAllowed(additionalAllowed);
    }
    setIsTableSelected(isTableSelected);
    setIsCaptionSelected(isCaptionSelected);
  }, [editor, selection]);

  const handleTableDeletion = () => {
    Commands.deleteTable(editor, plugin.editorDocument.documentType);
    ReactEditor.focus(editor);
  };

  const handleColumnDeletion = () => {
    Commands.deleteColumn(editor, plugin.editorDocument.documentType);
    ReactEditor.focus(editor);
  };

  const handleRowDeletion = () => {
    Commands.deleteRow(editor, plugin.editorDocument.documentType);
    ReactEditor.focus(editor);
  };

  const insertRowAtIndex = (rowIndexOffset: number) => {
    Commands.insertRow(editor, rowIndexOffset);
    ReactEditor.focus(editor);
  };

  const insertColumnAtIndex = (columnIndexOffset: number) => {
    Commands.insertColumn(editor, columnIndexOffset);
    ReactEditor.focus(editor);
  };

  const splitCell = () => {
    Commands.splitCell(editor);
    ReactEditor.focus(editor);
  };

  useMemo(() => {
    if (
      isTableSelected &&
      !plugin.editorDocument.isReadonly &&
      plugin.editorDocument.index === editorOnFocus.index &&
      !isCaptionSelected
    ) {
      plugin.contextMenuItemTypes = [
        {
          key: `${keyName}-table-add`,
          title: t('lea.contextMenu.tables.add'),
          label: t('lea.contextMenu.tables.add'),
          popupClassName: 'sub-menu-table',
          children: [
            {
              key: `${keyName}-table-addRowAbove`,
              title: t('lea.contextMenu.tables.addRowAbove'),
              label: t('lea.contextMenu.tables.addRowAbove'),
              onClick: () => insertRowAtIndex(0),
            },
            {
              key: `${keyName}-table-addRowBelow`,
              title: t('lea.contextMenu.tables.addRowBelow'),
              label: t('lea.contextMenu.tables.addRowBelow'),
              onClick: () => insertRowAtIndex(1),
            },
            {
              key: `${keyName}-table-addColumnLeft`,
              title: t('lea.contextMenu.tables.addColumnLeft'),
              label: t('lea.contextMenu.tables.addColumnLeft'),
              onClick: () => insertColumnAtIndex(0),
              disabled: !additionalColumnsAllowed,
            },
            {
              key: `${keyName}-table-addColumnRight`,
              title: t('lea.contextMenu.tables.addColumnRight'),
              label: t('lea.contextMenu.tables.addColumnRight'),
              onClick: () => insertColumnAtIndex(1),
              disabled: !additionalColumnsAllowed,
            },
          ],
        },
        {
          key: `${keyName}-table-delete`,
          title: t('lea.contextMenu.tables.delete'),
          label: t('lea.contextMenu.tables.delete'),
          popupClassName: 'sub-menu-table',
          children: [
            {
              key: `${keyName}-table-deleteRow`,
              title: t('lea.contextMenu.tables.deleteRow'),
              label: t('lea.contextMenu.tables.deleteRow'),
              onClick: handleRowDeletion,
            },
            {
              key: `${keyName}-table-deleteColumn`,
              title: t('lea.contextMenu.tables.deleteColumn'),
              label: t('lea.contextMenu.tables.deleteColumn'),
              onClick: handleColumnDeletion,
            },
            {
              key: `${keyName}-table-deleteTable`,
              title: t('lea.contextMenu.tables.deleteTable'),
              label: t('lea.contextMenu.tables.deleteTable'),
              onClick: handleTableDeletion,
            },
          ],
        },
        {
          key: `${keyName}-table-divideCell`,
          title: t('lea.contextMenu.tables.divideCell'),
          label: t('lea.contextMenu.tables.divideCell'),
          onClick: splitCell,
        },
      ];
    } else if (isCaptionSelected) {
      plugin.contextMenuItemTypes = [
        {
          key: `${keyName}-table-deleteTable`,
          title: t('lea.contextMenu.tables.deleteTable'),
          label: t('lea.contextMenu.tables.deleteTable'),
          onClick: handleTableDeletion,
        },
      ];
    } else {
      plugin.contextMenuItemTypes = null;
    }
  }, [editorOnFocus, editorOnFocus, isTableSelected, isCaptionSelected]);

  return <></>;
};
