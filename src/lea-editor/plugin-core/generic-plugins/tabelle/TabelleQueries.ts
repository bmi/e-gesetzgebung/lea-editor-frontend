// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Path } from 'slate';

import { isOfType, isWithinTypeHierarchy } from '../../utils';
import { TABELLE_STRUCTURE } from '.';

export class TabelleQueries {
  public static isEditable(editor: Editor, path?: Path, type?: string): boolean {
    const isPElement = type ? type === TABELLE_STRUCTURE.P : isOfType(editor, TABELLE_STRUCTURE.P);
    const isCaption = this.isCaption(editor, path, type);
    return (
      (isPElement &&
        (isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.TABLE_CELL], path) ||
          isWithinTypeHierarchy(editor, [TABELLE_STRUCTURE.TABLE_HEADERCELL], path))) ||
      isCaption
    );
  }

  public static isCaption(editor: Editor, _path?: Path, type?: string): boolean {
    const isCaption = type
      ? type === TABELLE_STRUCTURE.TABLE_CAPTION
      : isOfType(editor, TABELLE_STRUCTURE.TABLE_CAPTION);
    return isCaption;
  }
}
