// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useState } from 'react';

import { useTranslation } from 'react-i18next';

import './GridSizeSelector.scss';

interface IProps {
  rows: number;
  cols: number;
  onCellClick: (rows: number, cols: number) => void;
}

export const GridSizeSelector = ({ rows, cols, onCellClick }: IProps): ReactElement => {
  const { t } = useTranslation();
  const [selectedHeight, setSelectedHeight] = useState<number>(0);
  const [selectedWidth, setSelectedWidth] = useState<number>(0);

  const onCellMouseOver = (e: React.MouseEvent<HTMLTableCellElement, MouseEvent>) => {
    const coords = (e.target as HTMLTableCellElement).id.split('-');

    setSelectedHeight(Number.parseInt(coords[0]));
    setSelectedWidth(Number.parseInt(coords[1]));
  };

  const handleCellClick = () => {
    onCellClick(selectedHeight, selectedWidth);
  };

  const renderTableData = () => {
    const tableRows = [];
    for (let i = 1; i <= rows; i++) {
      const tableCells = [];
      for (let j = 1; j <= cols; j++) {
        tableCells.push(
          <td
            key={`td-${i}-${j}`}
            id={`${i}-${j}`}
            onClick={handleCellClick}
            onMouseOver={onCellMouseOver}
            className={i <= selectedHeight && j <= selectedWidth ? 'selected' : ''}
          ></td>,
        );
      }
      tableRows.push(<tr key={`tr-${i}`}>{tableCells}</tr>);
    }
    return tableRows;
  };

  return (
    <>
      <div className={'grid-size-selector'}>
        <div className={'left'}>{t('lea.toolbar.labels.tables.pleaseSelect')}</div>
        <div className={'right'}>
          {selectedHeight} x {selectedWidth}
        </div>
      </div>
      <div style={{ clear: 'both', paddingBottom: '18px' }}>
        <table className={'grid-size-selector'}>
          <tbody>{renderTableData()}</tbody>
        </table>
      </div>
    </>
  );
};
