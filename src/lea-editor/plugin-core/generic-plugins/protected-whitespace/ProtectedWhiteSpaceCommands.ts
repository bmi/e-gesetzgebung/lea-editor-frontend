// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseRange, Editor, Location, NodeEntry, Range, Text, Transforms } from 'slate';

import { ReactEditor } from 'slate-react';

export class ProtectedWhiteSpaceCommands {
  public static replaceWhiteSpacesInsideSelection(editor: Editor, isAuthorialNotePlugin: boolean) {
    if (!editor.selection) return;
    const selectionStartPoint = Range.start(editor.selection);
    const selectionEndPoint = Range.end(editor.selection);
    const textNodes = Editor.nodes(editor as ReactEditor, {
      match: (node) => {
        return Text.isText(node);
      },
    });
    let nextTextWrapper = textNodes.next();
    while (!nextTextWrapper.done) {
      const [textNode, path] = nextTextWrapper.value as NodeEntry<Text>;
      const text = textNode.text;

      const isFirstTextNode = path.toString() === selectionStartPoint.path.toString();
      const isLastTextNode = path.toString() === selectionEndPoint.path.toString();

      const currentAnchorOffset = isFirstTextNode ? selectionStartPoint.offset : 0;
      const currentFocusOffset = isLastTextNode ? selectionEndPoint.offset : text.length;

      const textWithReplacedWhiteSpaces = text.slice(currentAnchorOffset, currentFocusOffset).replaceAll(' ', '\xa0');
      const insertAnchor: Location = { path: path, offset: currentAnchorOffset };
      const insertFocus: Location = { path: path, offset: currentFocusOffset };
      const insertSelection: BaseRange = { anchor: insertAnchor, focus: insertFocus };

      if (text.length > 0) {
        Transforms.delete(editor, { at: insertSelection.anchor, distance: currentFocusOffset - currentAnchorOffset });
        if (!isAuthorialNotePlugin) {
          Transforms.insertText(editor, textWithReplacedWhiteSpaces, { at: insertSelection.anchor });
        } else {
          setTimeout(() => {
            Transforms.insertText(editor, textWithReplacedWhiteSpaces, { at: insertSelection.anchor });
          }, 100);
        }
      }
      nextTextWrapper = textNodes.next();
    }
    Transforms.collapse(editor, { edge: 'end' });
  }
}
