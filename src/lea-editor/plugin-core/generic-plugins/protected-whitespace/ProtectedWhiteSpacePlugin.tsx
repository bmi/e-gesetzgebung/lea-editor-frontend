// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Editor, Range, NodeEntry } from 'slate';
import { Plugin, PluginProps } from '../..';
import { Element } from '../../../../interfaces';
import { PluginRange } from '../../Plugin';

import { isProtectedWhiteSpace } from '../../utils';
import { EditableQueries as Queries } from '../../utils/EditableQueries';
import { ProtectedWhiteSpaceCommands as Commands } from './ProtectedWhiteSpaceCommands';
import { ProtectedWhiteSpaceDecorator } from './Decorator';

export class ProtectedWhiteSpacePlugin extends Plugin {
  public constructor(props: PluginProps) {
    super(props);
  }

  public create<T extends Editor>(e: T): T {
    return e;
  }

  public isEditable(editor: Editor): boolean {
    return Queries.isEditable(editor, this.editorDocument.documentType, this.isAuthorialNotePlugin);
  }

  public keyDown(editor: Editor, event: KeyboardEvent): boolean {
    if (
      isProtectedWhiteSpace(event) &&
      editor.selection &&
      !this.editorDocument.isReadonly &&
      this.isEditable(editor)
    ) {
      if (Range.isCollapsed(editor.selection)) {
        editor.insertText('\xa0');
      } else {
        Commands.replaceWhiteSpacesInsideSelection(editor, this.isAuthorialNotePlugin);
      }
    }

    return false;
  }

  // For dev reasons only
  public decorate = (entry: NodeEntry<Element>): PluginRange[] => {
    const [node, path] = entry;
    return ProtectedWhiteSpaceDecorator(node, path);
  };
}
