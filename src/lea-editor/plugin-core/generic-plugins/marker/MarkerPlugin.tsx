// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { BaseEditor, Editor, Transforms } from 'slate';
import { ReactEditor } from 'slate-react';
import { Element } from '../../../../interfaces';
import { CombinedPluginRenderProps, Plugin, PluginProps } from '../../Plugin';
import { createElementOfType, createTextWrapper } from '../../utils';
import { MARKER_STRUCTURE } from './MarkerStructure';
import { HistoryEditor, History } from 'slate-history';

export class MarkerPlugin extends Plugin {
  public constructor(
    props: PluginProps,
    private history?: History,
  ) {
    super(props);
  }

  public create<T extends BaseEditor>(editor: T): T {
    return editor;
  }

  // Marker elements are only present in documents of version >= 1.4.
  // This method is used to insert these Elements into the documents with older
  // version, such that they have the same structure as the newer ones.
  public useEffect(editor: ReactEditor & HistoryEditor): void {
    if (this.history) return;
    const numNodes = Editor.nodes(editor, {
      match: (n) => {
        return (n as Element).type === MARKER_STRUCTURE.NUM;
      },
      at: [0],
    });
    try {
      let nextNumNode = numNodes.next();
      while (!nextNumNode.done) {
        const [node, path] = nextNumNode.value;

        const hasMarker = (node as Element).children.some((child) => {
          return child.type === MARKER_STRUCTURE.MARKER;
        });

        const hasTextwrapper = (node as Element).children.some((child) => {
          return child.type === MARKER_STRUCTURE.TEXTWRAPPER;
        });

        if (!hasMarker) {
          Transforms.insertNodes(
            editor,
            createElementOfType(MARKER_STRUCTURE.MARKER, [createTextWrapper('')], true, { name: '1' }),
            {
              at: [...path, 0],
            },
          );
        }

        if (!hasTextwrapper) {
          Transforms.insertNodes(editor, createTextWrapper(''), {
            at: [...path, 1],
          });
        }
        nextNumNode = numNodes.next();
      }
      editor.history.undos.pop();
    } catch (ex) {
      console.log(ex);
      return;
    }
  }

  public renderElement = (props: CombinedPluginRenderProps): ReactElement | null => {
    const e = props.element as Element;
    const elementType = e.type || '';
    const changeMark = e['lea:changeMark'] || '';
    const guid = e.GUID;
    const dataName = e.name;

    if (elementType === MARKER_STRUCTURE.MARKER) {
      return (
        <div
          id={guid && `${this.editorDocument.index}:${guid || ''}`}
          className={`${elementType} ${changeMark}`}
          data-name={dataName}
          {...props.attributes}
        >
          {props.children}
        </div>
      );
    }
    return null;
  };
}
