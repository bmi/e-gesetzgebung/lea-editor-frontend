// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { STRUCTURE_ELEMENTS } from '../../utils/StructureElements';

export const MARKER_STRUCTURE = {
  NUM: STRUCTURE_ELEMENTS.NUM,
  MARKER: STRUCTURE_ELEMENTS.MARKER,
  TEXTWRAPPER: STRUCTURE_ELEMENTS.TEXTWRAPPER,
};
