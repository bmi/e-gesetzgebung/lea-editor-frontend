// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Plugin } from './Plugin';

export abstract class CorePlugin extends Plugin {
  // DO NOT OVERRIDE IN YOUR PLUGIN!
  public get isCorePlugin(): boolean {
    return true;
  }
}
