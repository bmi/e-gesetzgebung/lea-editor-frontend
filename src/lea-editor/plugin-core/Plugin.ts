// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ReducersMapObject } from '@reduxjs/toolkit';
import React, { ReactElement } from 'react';
import { BasePoint, BaseRange, Editor, NodeEntry } from 'slate';
import { ReactEditor, RenderElementProps, RenderLeafProps } from 'slate-react';
import { Element } from '../../interfaces';

import { EditorDocument } from './editor-store/editor-info/EditorInfoState';
import { ItemType } from 'antd/lib/menu/interface';
import isHotkey from 'is-hotkey';
import { HotkeyActionCallback } from '../../types';
import { SearchReplaceEditor } from '../plugins/searchAndReplace/SearchReplaceEditor';
import { v4 as uuid } from 'uuid';
import { HistoryEditor } from 'slate-history';

type ReactFunction = () => ReactElement | null;

type ReactPropsFunction = (_props: any) => ReactElement | null;

export interface PluginProps {
  editorDocument: EditorDocument;
  isAuthorialNotePlugin?: boolean;
  idGenerator?: () => string;
  isDynAenderungsvergleich: boolean;
}

export interface PluginRenderProps {
  firstRender: boolean;
  cellsVisible: boolean;
  editor: ReactEditor;
}

export interface CombinedPluginRenderProps extends RenderElementProps, PluginRenderProps {}

export interface PluginRenderLeafProps extends RenderLeafProps {
  leaf: {
    [key: string]: any;
    text: string;
    comment: boolean;
    commentEndRef: React.MutableRefObject<HTMLElement>;
    commentStartRef: React.MutableRefObject<HTMLElement>;
    newComment: boolean;
    firstRendered?: boolean;
    commentName?: string;
    commentSelection?: {
      anchor: BasePoint;
      focus: BasePoint;
    };
    commentColor?: string;
    highlight?: boolean;
    hidden?: boolean;
  };
}

export interface PluginRange extends BaseRange {
  [key: string]: any;
  comment?: boolean;
  commentStartRef?: React.MutableRefObject<HTMLElement>;
  commentEndRef?: React.MutableRefObject<HTMLElement>;
  commentName?: string;
  commentSelection?: {
    anchor: BasePoint;
    focus: BasePoint;
  };
  newComment?: boolean;
  firstRendered?: boolean;
  commentColor?: string;
  highlight?: boolean;
  hidden?: boolean;
}

interface CombinedPluginRenderLeafProps extends PluginRenderLeafProps {
  editor: ReactEditor;
}

export interface IPluginNavigationDialogProps {
  editor: ReactEditor;
}

export interface IPluginContextMenuProps {
  keyName: string;
  editor: ReactEditor;
}

export interface IPluginToolbarProps {
  editor: ReactEditor | (ReactEditor & HistoryEditor);
}

export interface IPluginOptionDialogProps {
  editor: SearchReplaceEditor;
}

export interface IPluginContextProviderProps {
  children: React.ReactNode;
}

export abstract class Plugin {
  protected _editorDocument: EditorDocument;
  protected _isAuthorialNotePlugin: boolean;
  protected _pluginId: string;
  protected _isDynAenderungsvergleich: boolean;

  public constructor({ editorDocument, isAuthorialNotePlugin, idGenerator, isDynAenderungsvergleich }: PluginProps) {
    this._editorDocument = editorDocument;
    this._isAuthorialNotePlugin = !!isAuthorialNotePlugin;
    this._pluginId = idGenerator ? idGenerator() : uuid();
    this._isDynAenderungsvergleich = isDynAenderungsvergleich;
  }

  public get editorDocument(): EditorDocument {
    return this._editorDocument;
  }
  public get isAuthorialNotePlugin(): boolean {
    return this._isAuthorialNotePlugin;
  }

  public get isDynAenderungsvergleich(): boolean {
    return this._isDynAenderungsvergleich;
  }

  public get pluginId(): string {
    return this._pluginId;
  }

  // DO NOT OVERRIDE IN YOUR PLUGIN!
  public get isCorePlugin(): boolean {
    return false;
  }

  public get toolRefs(): React.RefObject<HTMLElement>[] {
    return [];
  }

  public get viewToolItemTypes(): ItemType[] | null {
    return null;
  }

  public get insertToolItemTypes(): ItemType[] | null {
    return null;
  }

  public get contextMenuItemTypes(): ItemType[] | null {
    return null;
  }

  public abstract create<T extends Editor>(editor: T): T;

  public update(
    editorDocument: EditorDocument,
    isDynAenderungsvergleich: boolean,
    isAuthorialNotePlugin?: boolean,
  ): void {
    this._editorDocument = editorDocument;
    this._isAuthorialNotePlugin = !!isAuthorialNotePlugin;
    this._isDynAenderungsvergleich = isDynAenderungsvergleich;
  }

  public useEffect(_editor: ReactEditor): void {
    // This is intentional
  }

  public keyDown(_editor: Editor, _event: KeyboardEvent): boolean {
    return false;
  }

  public copy?(_editor: SearchReplaceEditor | null): void {
    // This is intentional
  }

  public paste?(_editor: Editor, _event: ClipboardEvent): void {
    // This is intentional
  }

  public cut?(_editor: Editor, _event: ClipboardEvent): void {
    // This is intentional
  }

  public blur(_editor: Editor, _event: FocusEvent): void {
    // This is intentional
  }

  public ToolbarTools?: ReactPropsFunction = undefined;

  public ViewTools?: ReactFunction = undefined;

  public InsertTools?: ReactPropsFunction = undefined;

  public NavigationDialog?: ReactPropsFunction = undefined;

  public OptionDialog?: ReactPropsFunction = undefined;

  public ActionDialog?: ReactFunction = undefined;

  public ModalDialog?: ReactFunction = undefined;

  public ContextMenu?: ReactPropsFunction = undefined;

  public renderElement = (_props: CombinedPluginRenderProps): ReactElement | null => {
    return null;
  };

  public renderCells = (_props: CombinedPluginRenderProps): ReactElement | null => {
    return null;
  };

  public renderLeaf(_props: CombinedPluginRenderLeafProps): ReactElement | null {
    return null;
  }

  public decorate = (
    _entry: NodeEntry<Element>,
    _editor: SearchReplaceEditor,
    _searchString?: string,
  ): PluginRange[] => {
    return [];
  };

  public reducer(): ReducersMapObject {
    return {};
  }

  public isEditable(_editor: Editor): boolean {
    return false;
  }
  public PluginContextProvider?: ReactPropsFunction = undefined;

  public RegisterDispatchFunctions?: ReactFunction = undefined;

  protected handleHotkeys(
    hotkeys: Record<string, { type: string; code: string }>,
    hotkeyAction: HotkeyActionCallback,
    editor: Editor,
    event: KeyboardEvent,
  ): boolean {
    for (const hotkey in hotkeys) {
      if (isHotkey(hotkey, event)) {
        event.preventDefault();
        event.stopPropagation();
        const { code } = hotkeys[hotkey];
        hotkeyAction(editor, code);
        return true; // hotkey found!
      }
    }
    return false;
  }
}
