// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { DocumentType } from '../api';

export const DocumentTypeHelper = (type: DocumentType) => {
  switch (type) {
    case DocumentType.BEGRUENDUNG_STAMMGESETZ:
    case DocumentType.BEGRUENDUNG_MANTELGESETZ:
    case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
    case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
      return <>Begründung</>;
    case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
    case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
    case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
    case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
      return <>Regelungstext</>;
    case DocumentType.VORBLATT_STAMMGESETZ:
    case DocumentType.VORBLATT_MANTELGESETZ:
    case DocumentType.VORBLATT_STAMMVERORDNUNG:
    case DocumentType.VORBLATT_MANTELVERORDNUNG:
      return <>Vorblatt</>;
    case DocumentType.ANSCHREIBEN_STAMMGESETZ:
    case DocumentType.ANSCHREIBEN_MANTELGESETZ:
    case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
    case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
      return <>Anschreiben</>;
    case DocumentType.ANLAGE:
      return <>Anlage</>;
    default:
      return <></>;
  }
};
