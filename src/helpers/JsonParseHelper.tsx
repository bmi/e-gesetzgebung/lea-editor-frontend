// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { displayMessage } from '@plateg/theme';
import { t } from 'i18next';

export const tryParseJsonContent = (
  content: string,
  basePath: string,
  documentTitle?: string,
  isAuthorialNote?: boolean,
): any => {
  try {
    return JSON.parse(content);
  } catch (ex) {
    if (!isAuthorialNote) {
      displayMessage(t('lea.Document.editorArea.parseError', { title: documentTitle }), 'error');
      window.location.hash = `#${basePath}`;
      return [];
    }
  }
};
