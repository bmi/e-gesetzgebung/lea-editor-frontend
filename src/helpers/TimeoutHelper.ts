// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

class TimeoutHelper {
  private timer: NodeJS.Timeout | undefined = undefined;

  public resetTimer(): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  public setTimer(callback: () => void, delay: number): void {
    this.timer = setTimeout(callback, delay);
  }
}

export default TimeoutHelper;
