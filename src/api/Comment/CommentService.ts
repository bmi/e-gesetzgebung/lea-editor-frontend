// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api, ApiResponse } from '../Api';
import { DocumentDTO } from '../Document';
import {
  CommentResponseDTO,
  CreateCommentDTO,
  PatchCommentDTO,
  ChangeCommentStateDTO,
  ReplyDTO,
  CommentDeleteDTO,
} from './Comment';
import { Dispatch, SetStateAction } from 'react';

const getAll = (documentId: string) => {
  const path = 'documents/' + documentId + '/comments';
  return Api.getAll<CommentResponseDTO>(path, decodeCommentDTO);
};

const create = (documentId: string, comment: CreateCommentDTO | null) => {
  if (comment?.content) {
    comment.content = Api.encodeString(comment.content);
  }
  if (comment?.documentContent) {
    comment.documentContent = Api.encodeString(comment.documentContent);
  }
  const path = 'documents/' + documentId + '/commentsNEW';
  return Api.create<CreateCommentDTO | null, DocumentDTO>(comment, path);
};

const update = (documentId: string, commentId: string, comment: Partial<PatchCommentDTO>) => {
  if (comment?.content) {
    comment.content = Api.encodeString(comment.content);
  }
  const path = 'documents/' + documentId + '/comments/' + commentId;
  return Api.update<PatchCommentDTO, CommentResponseDTO>(comment, path);
};

const changeState = (
  documentId: string,
  commentId: string,
  comment: ChangeCommentStateDTO,
  setApiResponse: Dispatch<SetStateAction<ApiResponse<CommentResponseDTO>>>,
) => {
  const path = 'documents/' + documentId + '/comments/' + commentId + '/status';
  return Api.put<ChangeCommentStateDTO, CommentResponseDTO>(comment, path, setApiResponse);
};

const deleteComment = (documentId: string, commentIds: string[], document: Partial<DocumentDTO>) => {
  const path = 'documents/' + documentId + '/commentsNEW/';
  if (document?.content) {
    document.content = Api.encodeString(document.content);
  }
  const deleteCommentBody = {
    commentIds: commentIds,
    content: document.content ?? '',
  };
  return Api.delete<DocumentDTO, CommentDeleteDTO>(path, deleteCommentBody);
};

const updateReply = (documentId: string, commentId: string, replyId: string, reply: Partial<ReplyDTO>) => {
  if (reply?.content) {
    reply.content = Api.encodeString(reply.content);
  }
  const path = 'documents/' + documentId + '/comments/' + commentId + '/reply/' + replyId;
  return Api.update<CreateCommentDTO, CommentResponseDTO>(reply, path);
};

const createReply = (documentId: string, commentId: string, reply: Partial<ReplyDTO> | null) => {
  if (reply?.content) {
    reply.content = Api.encodeString(reply.content);
  }
  const path = 'documents/' + documentId + '/comments/' + commentId + '/reply';
  return Api.create<Partial<ReplyDTO> | null, ReplyDTO>(reply, path);
};

const deleteReply = (documentId: string, commentId: string, replyId: string) => {
  const path = 'documents/' + documentId + '/comments/' + commentId + '/reply/' + replyId;
  return Api.delete(path);
};

const decodeCommentDTO = (comment: CommentResponseDTO): CommentResponseDTO => {
  const decodedContent = Api.decodeString(comment.content);
  comment.content = decodedContent;
  comment.replies = comment.replies?.map((reply) => {
    reply.content = Api.decodeString(reply.content);
    return reply;
  });
  return comment;
};

export const CommentService = {
  getAll,
  create,
  deleteComment,
  update,
  changeState,
  createReply,
  updateReply,
  deleteReply,
};
