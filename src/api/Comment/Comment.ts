// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Point } from 'slate';
import { UserDTO } from '../User';
import { ReactElement } from 'react';
import { ReplyWithMetadata } from '../../interfaces';

type CommentState = 'OPEN' | 'CLOSED';

export interface CreateCommentDTO {
  commentId: string;
  content: string;
  documentContent: string;
  anchor: LeaPoint;
  focus: LeaPoint;
}

export interface LeaPoint {
  elementGuid: string;
  offset: number;
}

export interface CommentResponseDTO {
  documentId: string;
  commentId: string;
  content: string;
  anchor?: Point;
  focus?: Point;
  createdBy: UserDTO;
  updatedBy: UserDTO;
  createdAt: string;
  updatedAt: string;
  colorCode?: string;
  state: CommentState;
  replies: ReplyDTO[];
  isActive?: boolean;
  serializedContent?: ReactElement;
  organizedReplies?: ReplyWithMetadata[];
  deleted: boolean;
}

export interface PatchCommentDTO {
  content: string;
  anchor?: Point;
  focus?: Point;
}
export interface ChangeCommentStateDTO {
  status: CommentState;
}

export interface CommentDeleteDTO {
  commentIds: string[];
  content: string;
}

export interface ReplyDTO {
  replyId: string;
  parentId: string;
  content: string;
  createdBy: UserDTO;
  updatedBy: UserDTO;
  createdAt: string;
  updatedAt: string;
}
