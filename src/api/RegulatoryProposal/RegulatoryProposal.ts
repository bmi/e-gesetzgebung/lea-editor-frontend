// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  FarbeType,
  FilterPropertyType,
  PageableObject,
  RegelungsvorhabenTypType,
  SortObject,
  VorhabenStatusType,
} from '@plateg/rest-api';
import { CompoundDocumentType } from '../CompoundDocument';
import { DocumentStateType, DocumentType } from '../Document';
import { PermissionsDTO, UserDTO } from '../User';

export enum EntityType {
  REGULATORY_PROPOSAL = 'REGULATORY_PROPOSAL',
  COMPOUND_DOCUMENT = 'COMPOUND_DOCUMENT',
  SINGLE_DOCUMENT = 'SINGLE_DOCUMENT',
  BUTTON = 'BUTTON',
}

export enum ButtonType {
  SHOW_ALL = 'SHOW_ALL',
  SHOW_LESS = 'SHOW_LESS',
}

export interface EditorHomepageTableDTOs {
  dtos: PageEditorHomepageTableDTO;
  vorhabenartFilter: Array<RegelungsvorhabenTypType>;
  filterNames: Array<FilterPropertyType>;
  allContentEmpty: boolean;
}

interface PageEditorHomepageTableDTO {
  totalElements?: number;
  totalPages?: number;
  size?: number;
  content?: Array<HomepageRegulatoryProposalDTO>;
  number?: number;
  sort?: SortObject;
  first?: boolean;
  last?: boolean;
  numberOfElements?: number;
  pageable?: PageableObject;
  empty?: boolean;
}

export interface HomepageRegulatoryProposalDTO {
  entityType: EntityType;
  id: string;
  abbreviation: string;
  children: HomepageCompoundDocumentDTO[];
  farbe?: FarbeType;
}

export interface HomepageCompoundDocumentDTO {
  entityType: EntityType;
  id: string;
  regulatoryProposalId: string;
  title: string;
  type: CompoundDocumentType;
  state: DocumentStateType;
  regulatoryProposalState: VorhabenStatusType;
  version: string;
  children: (HomepageSingleDocumentDTO | ExpandCollapseButton)[];
  documentPermissions: PermissionsDTO;
  commentPermissions: PermissionsDTO;
  isNewest: boolean;
  isPinned: boolean;
}

export interface HomepageSingleDocumentDTO {
  entityType: EntityType;
  id: string;
  regulatoryProposalId: string;
  title: string;
  type: DocumentType;
  state: DocumentStateType;
  version: string;
  updatedAt: string;
  updatedBy: UserDTO;
  createdAt: string;
  createdBy: UserDTO;
  documentPermissions: PermissionsDTO;
  commentPermissions: PermissionsDTO;
}

export interface ExpandCollapseButton {
  entityType: EntityType;
  type: ButtonType;
  compoundDocument: HomepageCompoundDocumentDTO;
  id: string;
}

export interface PaginationDTO {
  pageNumber: number;
  pageSize: number;
  sortBy: string;
  sortDirection: string;
}
