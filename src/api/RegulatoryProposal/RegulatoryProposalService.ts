// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../Api';
import { EditorHomepageTableDTOs, HomepageRegulatoryProposalDTO, PaginationDTO } from './RegulatoryProposal';

const getHomepageMeineDokumente = (pagination?: PaginationDTO) => {
  return Api.create<any, EditorHomepageTableDTOs>(pagination, 'meineDokumente');
};

const getHomepageDokumenteAusAbstimmungen = () => {
  return Api.getAll<HomepageRegulatoryProposalDTO>('dokumenteAusAbstimmungen');
};

const getVersionHistory = (regulatoryProposalId: string, pagination?: PaginationDTO) => {
  const path = `regulatoryProposal/${regulatoryProposalId}`;
  return Api.create<PaginationDTO | undefined, EditorHomepageTableDTOs>(pagination, path);
};

export const RegulatoryProposalService = {
  getHomepageMeineDokumente,
  getHomepageDokumenteAusAbstimmungen,
  getVersionHistory,
};
