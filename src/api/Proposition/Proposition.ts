// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Proponent } from '../Proponent';

type PropositionState = 'ABGEBROCHEN' | 'ABGESCHLOSSEN' | 'IN_BEARBEITUNG' | 'ENTWURF';
type PropositionInitiant = 'BUNDESTAG' | 'BUNDESRAT' | 'BUNDESREGIERUNG';
type PropositionType = 'GESETZ' | 'RECHTSVERORDNUNG' | 'VERWALTUNGSVORSCHRIFT';

export interface PropositionDTO {
  id: string;
  title: string;
  shortTitle: string;
  abbreviation: string;
  state: PropositionState;
  proponent?: Proponent;
  initiant: PropositionInitiant;
  type: PropositionType;
}
