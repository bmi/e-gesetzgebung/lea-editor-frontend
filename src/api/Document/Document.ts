// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { History } from 'slate-history';
import { PropositionDTO } from '../Proposition';
import { PermissionsDTO, UserDTO } from '../User';

export enum DocumentStateType {
  DRAFT = 'DRAFT',
  FREEZE = 'FREEZE',
  FINAL = 'FINAL',
  BEREIT_FUER_KABINETTVERFAHREN = 'BEREIT_FUER_KABINETTVERFAHREN',
  BEREIT_FUER_BUNDESTAG = 'BEREIT_FUER_BUNDESTAG',
  ZUGELEITET_PKP = 'ZUGELEITET_PKP',
  BESTANDSRECHT = 'BESTANDSRECHT',
  AENDERUNG_GEWUENSCHT_PKP = 'AENDERUNG_GEWUENSCHT_PKP',
  AENDERUNG_GEWUENSCHT_FEDERFUEHRER = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER',
}

export interface DocumentDTO {
  id: string;
  compoundDocumentId?: string;
  cdName?: string;
  title: string;
  content: string;
  type: DocumentType;
  state: DocumentStateType;
  version: string;
  proposition?: PropositionDTO;
  createdBy: UserDTO;
  updatedBy: UserDTO;
  createdAt: string;
  updatedAt: string;
  documentPermissions: PermissionsDTO;
  commentPermissions: PermissionsDTO;
  history?: History;
}

export interface RenameDocumentDTO {
  title: string;
}

export interface CreateDocumentDTO {
  title: string;
  regelungsvorhabenId: string;
  type: DocumentType;
  initialNumberOfLevels?: number;
}

export interface DocumentSummaryDTO {
  id: string;
  title: string;
  type: DocumentType;
  state: DocumentStateType;
  version: string;
  createdAt: string;
  updatedA: string;
}
export interface AenderungsvergleichCreateDTO {
  base: string;
  versions: string[];
}

export interface AenderungsvergleichDTO {
  type: DocumentType;
  erstellt: string;
  ersteller: UserDTO;
  document: DocumentDTO;
}

export interface BestandsrechtDTO {
  regelungsvorhabenId: string;
  dokumentenmappenId: string;
  kurzTitel: string;
  langTitel: string;
}

export enum DocumentType {
  ANSCHREIBEN_MANTELGESETZ = 'ANSCHREIBEN_MANTELGESETZ',
  ANSCHREIBEN_STAMMGESETZ = 'ANSCHREIBEN_STAMMGESETZ',
  ANSCHREIBEN_STAMMVERORDNUNG = 'ANSCHREIBEN_STAMMVERORDNUNG',
  ANSCHREIBEN_MANTELVERORDNUNG = 'ANSCHREIBEN_MANTELVERORDNUNG',
  VORBLATT_MANTELGESETZ = 'VORBLATT_MANTELGESETZ',
  VORBLATT_STAMMGESETZ = 'VORBLATT_STAMMGESETZ',
  VORBLATT_STAMMVERORDNUNG = 'VORBLATT_STAMMVERORDNUNG',
  VORBLATT_MANTELVERORDNUNG = 'VORBLATT_MANTELVERORDNUNG',
  REGELUNGSTEXT_MANTELGESETZ = 'REGELUNGSTEXT_MANTELGESETZ',
  REGELUNGSTEXT_STAMMGESETZ = 'REGELUNGSTEXT_STAMMGESETZ',
  REGELUNGSTEXT_STAMMVERORDNUNG = 'REGELUNGSTEXT_STAMMVERORDNUNG',
  REGELUNGSTEXT_MANTELVERORDNUNG = 'REGELUNGSTEXT_MANTELVERORDNUNG',
  BEGRUENDUNG_MANTELGESETZ = 'BEGRUENDUNG_MANTELGESETZ',
  BEGRUENDUNG_STAMMGESETZ = 'BEGRUENDUNG_STAMMGESETZ',
  BEGRUENDUNG_STAMMVERORDNUNG = 'BEGRUENDUNG_STAMMVERORDNUNG',
  BEGRUENDUNG_MANTELVERORDNUNG = 'BEGRUENDUNG_MANTELVERORDNUNG',
  ANLAGE = 'ANLAGE',
  BT_DRUCKSACHE = 'BT_DRUCKSACHE',
  SYNOPSE = 'SYNOPSE',
}
