// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentDTO } from '.';
import { createEntityAdapter, createSlice, Update } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
export const synopsisDocumentsAdapter = createEntityAdapter<DocumentDTO>({
  selectId: (synopsisDocument) => synopsisDocument.id,
});

export const SynopsisDocumentSlice = createSlice({
  name: 'synopsisDocuments',
  initialState: synopsisDocumentsAdapter.getInitialState(), // InitialDocumentState,
  reducers: {
    addSynopsisDocument(state, action: PayloadAction<DocumentDTO>) {
      synopsisDocumentsAdapter.addOne(state, action.payload);
    },
    addSynopsisDocuments(state, action: PayloadAction<DocumentDTO[]>) {
      synopsisDocumentsAdapter.addMany(state, action.payload);
    },
    updateSynopsisDocument(state, action: PayloadAction<DocumentDTO>) {
      const updateDocuments: Update<DocumentDTO> = { id: action.payload.id, changes: action.payload };
      synopsisDocumentsAdapter.updateOne(state, updateDocuments);
    },
    removeSynopsisDocuments(state, action: PayloadAction<string | undefined>) {
      if (action.payload) {
        synopsisDocumentsAdapter.removeOne(state, action.payload);
      } else {
        synopsisDocumentsAdapter.removeAll(state);
      }
    },
  },
});

export const { addSynopsisDocument, updateSynopsisDocument, removeSynopsisDocuments, addSynopsisDocuments } =
  SynopsisDocumentSlice.actions;
export const SynopsisDocumentReducer = SynopsisDocumentSlice.reducer;
