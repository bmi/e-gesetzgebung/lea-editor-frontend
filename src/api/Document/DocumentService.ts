// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../Api';
import {
  AenderungsvergleichCreateDTO,
  AenderungsvergleichDTO,
  BestandsrechtDTO,
  DocumentDTO,
  DocumentSummaryDTO,
  RenameDocumentDTO,
} from '.';

const getById = (documentId: string) => {
  return Api.getById<DocumentDTO>(documentId, 'documents', decodeDocumentDTO);
};

const update = (documentId: string, document: Partial<DocumentDTO>) => {
  document = encodeDocumentDTO(document);
  const path = 'documents/' + documentId;
  return Api.update<DocumentDTO, DocumentDTO>(document, path, decodeDocumentDTO);
};

const renameDocument = (documentId: string, title: RenameDocumentDTO) => {
  const path = 'documents/' + documentId + '/title';
  return Api.update<RenameDocumentDTO, RenameDocumentDTO>(title, path);
};

const exportDocument = (documentId: string, document: Partial<DocumentDTO>) => {
  document = encodeDocumentDTO(document);
  return Api.createXml<any>(document, `documents/${documentId}/export`);
};

const createAenderungsvergleich = (documents: AenderungsvergleichCreateDTO) => {
  return Api.create<AenderungsvergleichCreateDTO, AenderungsvergleichDTO>(
    documents,
    'synopse',
    decodeAenderungsvergleichDTO,
  );
};

const createAenderungsbefehle = (regulatoryProposalId: string, documents: AenderungsvergleichCreateDTO) => {
  return Api.create<AenderungsvergleichCreateDTO, DocumentDTO>(
    documents,
    `regulatoryProposal/${regulatoryProposalId}/aenderungsbefehle`,
    decodeDocumentDTO,
  );
};

const createBestandsrecht = (documentId: string, document: BestandsrechtDTO) => {
  return Api.create<BestandsrechtDTO, DocumentSummaryDTO>(document, `documents/${documentId}/bestandsrecht`);
};

const decodeDocumentDTO = (document: DocumentDTO): DocumentDTO => {
  const decodedContent = Api.decodeString(document.content);
  document.content = decodedContent;
  return document;
};

const decodeAenderungsvergleichDTO = (synopse: AenderungsvergleichDTO): AenderungsvergleichDTO => {
  if (synopse.document) {
    const decodedContent = Api.decodeString(synopse.document.content);
    synopse.document.content = decodedContent;
  }
  return synopse;
};

const encodeDocumentDTO = (document: Partial<DocumentDTO>): Partial<DocumentDTO> => {
  if (document.content) {
    const encodedContent = Api.encodeString(document.content);
    document.content = encodedContent;
  }
  return document;
};

export const DocumentService = {
  getById,
  update,
  renameDocument,
  export: exportDocument,
  createAenderungsvergleich,
  createAenderungsbefehle,
  createBestandsrecht,
  encodeDocumentDTO,
};
