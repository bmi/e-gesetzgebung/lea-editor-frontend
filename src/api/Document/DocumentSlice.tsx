// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentDTO } from '.';
import { createEntityAdapter, createSlice, Update } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
export const documentsAdapter = createEntityAdapter<DocumentDTO>({
  selectId: (document) => document.id,
});

export const DocumentSlice = createSlice({
  name: 'documents',
  initialState: documentsAdapter.getInitialState(), // InitialDocumentState,
  reducers: {
    addFirstDocument(state, action: PayloadAction<DocumentDTO>) {
      documentsAdapter.addOne(state, action.payload);
    },
    updateFirstDocument(state, action: PayloadAction<DocumentDTO>) {
      const updateDocument: Update<DocumentDTO> = {
        id: action.payload.id,
        changes: action.payload,
      };
      documentsAdapter.updateOne(state, updateDocument);
    },
    removeFirstDocument(state) {
      documentsAdapter.removeAll(state);
    },
  },
});

export const { addFirstDocument, updateFirstDocument, removeFirstDocument } = DocumentSlice.actions;
export const DocumentReducer = DocumentSlice.reducer;
