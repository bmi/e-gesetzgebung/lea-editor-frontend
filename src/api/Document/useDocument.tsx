// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { displayMessage } from '@plateg/theme';
import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { shallowEqual } from 'react-redux';
import { useHistory } from 'react-router';
import { Descendant } from 'slate';
import { DocumentDTO, DocumentService, DocumentType } from '.';
import { useAppInfoState } from '../../components/App';
import { useAppDispatch, useAppSelector } from '../../components/Store';
import {
  addFirstDocument,
  documentsAdapter,
  DocumentSlice,
  removeFirstDocument,
  updateFirstDocument,
} from './DocumentSlice';
import {
  addSynopsisDocument,
  updateSynopsisDocument,
  removeSynopsisDocuments,
  synopsisDocumentsAdapter,
  SynopsisDocumentSlice,
  addSynopsisDocuments,
} from './SynopsisDocumentSlice';
import { format } from 'date-fns';
import { History } from 'slate-history';

type UseDocumentState = {
  document: DocumentDTO | null;
};

type UseSynopsisDocumentState = {
  documents: DocumentDTO[];
};

type UseFirstDocumentDispatch = {
  fetchDocument: (id: string) => void;
  removeDocument: () => void;
  updateDocument: (documentInfo: DocumentInfo, onSuccess?: OnSuccessFunction) => void;
  setDocument: (document: DocumentDTO) => void;
};

type UseSynopsisDocumentDispatch = {
  fetchDocument: (id: string) => void;
  fetchDocuments: (ids: string[], onSuccess?: OnSuccessFunction) => void;
  removeDocument: (id?: string) => void;
  updateDocument: (documentInfo: DocumentInfo, onSuccess?: OnSuccessFunction) => void;
  setDocuments: (documents: DocumentDTO[]) => void;
  addDocument: (document: DocumentDTO) => void;
};

type FetchDocument = {
  fetchDocument: (id: string) => void;
};

type FetchDocuments = {
  fetchDocuments: (ids: string[], onSuccess?: OnSuccessFunction) => void;
};

type UpdateDocument = {
  updateDocument: (documentInfo: DocumentInfo, onSuccess?: OnSuccessFunction, history?: History) => void;
};

type UpdateDocuments = {
  updateDocuments: (documentsInfo: DocumentInfo[], onSuccess?: OnSuccessFunction) => void;
};

export type DocumentInfo = {
  id: string;
  content: string;
  history: History;
  isFirstDocument: boolean;
};

type ExportDocument = {
  exportDocument: (value: Descendant[], document: DocumentDTO | null) => void;
};

export type OnSuccessFunction = () => void;

const initialUpdateDocumentData: DocumentInfo = {
  id: '',
  content: '',
  history: { undos: [], redos: [] },
  isFirstDocument: true,
};

const useFetchDocument = (dispatch: (document: DocumentDTO) => void): FetchDocument => {
  const history = useHistory();
  const { basePath } = useAppInfoState();
  const { t } = useTranslation();
  const [fetchDocumentId, setFetchDocumentId] = useState<string>('');
  const [fetchDocumentResponse, fetchDocument] = DocumentService.getById(fetchDocumentId);

  const fetchDocumentCallback = useCallback((id: string) => {
    setFetchDocumentId(id);
  }, []);

  useEffect(() => {
    if (fetchDocumentId != '') {
      fetchDocument();
      setFetchDocumentId('');
    }
  }, [fetchDocumentId]);

  useEffect(() => {
    if (fetchDocumentResponse.hasError) {
      displayMessage(t('lea.messages.stammgesetzRegelungstext.loadAccessError.text'), 'error');
      history.push(`${basePath}/`);
    } else if (!fetchDocumentResponse.isLoading && fetchDocumentResponse.data) {
      const documentTypes = Object.values(DocumentType);
      if (documentTypes.includes(fetchDocumentResponse.data.type)) {
        dispatch(fetchDocumentResponse.data);
      } else {
        displayMessage(t('lea.messages.stammgesetzRegelungstext.typeError.text'), 'error');
      }
    }
  }, [fetchDocumentResponse]);

  return { fetchDocument: fetchDocumentCallback };
};

const useFetchDocuments = (dispatch: (documents: DocumentDTO[]) => void): FetchDocuments => {
  const history = useHistory();
  const { basePath } = useAppInfoState();
  const { t } = useTranslation();
  const onSuccessRef = useRef<OnSuccessFunction>();

  const documents = useRef<DocumentDTO[]>([]);
  const fetchDocumentIds = useRef<string[]>([]);
  const [fetchDocumentId, setFetchDocumentId] = useState<string>('');
  const [fetchDocumentResponse, fetchDocument] = DocumentService.getById(fetchDocumentId);

  const fetchDocumentsCallback = useCallback((ids: string[], onSuccess?: () => void) => {
    onSuccessRef.current = onSuccess;
    fetchDocumentIds.current = ids;
    setFetchDocumentId(fetchDocumentIds.current[0]);
  }, []);

  const combineDocuments = (document: DocumentDTO) => {
    documents.current.push(document);
    if (documents.current.length < fetchDocumentIds.current.length) {
      setFetchDocumentId(fetchDocumentIds.current[documents.current.length]);
    } else {
      fetchDocumentIds.current = [];
      dispatch(documents.current);
    }
  };

  useEffect(() => {
    if (fetchDocumentId !== '') {
      fetchDocument();
      setFetchDocumentId('');
    }
  }, [fetchDocumentId]);

  useEffect(() => {
    if (fetchDocumentResponse.hasError) {
      displayMessage(t('lea.messages.stammgesetzRegelungstext.loadAccessError.text'), 'error');
      history.push(`${basePath}/`);
    } else if (!fetchDocumentResponse.isLoading && fetchDocumentResponse.data) {
      const documentTypes = Object.values(DocumentType);
      if (documentTypes.includes(fetchDocumentResponse.data?.type)) {
        combineDocuments(fetchDocumentResponse.data);
        onSuccessRef.current?.();
      } else {
        displayMessage(t('lea.messages.stammgesetzRegelungstext.typeError.text'), 'error');
      }
    }
  }, [fetchDocumentResponse]);

  return { fetchDocuments: fetchDocumentsCallback };
};

const useUpdateDocument = (dispatch: (document: DocumentDTO) => void): UpdateDocument => {
  const { t } = useTranslation();
  const onSuccessRef = useRef<OnSuccessFunction>();
  const [updateDocumentData, setUpdateDocumentData] = useState<DocumentInfo>(initialUpdateDocumentData);

  const [updateDocumentResponse, updateDocument] = DocumentService.update(updateDocumentData.id, {
    content: updateDocumentData.content,
  });
  const updateDocumentCallback = useCallback((documentInfo: DocumentInfo, onSuccess?: () => void) => {
    onSuccessRef.current = onSuccess;
    setUpdateDocumentData(documentInfo);
  }, []);

  useEffect(() => {
    if (updateDocumentData.id != '') {
      updateDocument();
      setUpdateDocumentData(initialUpdateDocumentData);
    }
  }, [updateDocumentData]);

  useEffect(() => {
    if (updateDocumentResponse.isLoading) return;
    if (updateDocumentResponse.data) {
      displayMessage(t('lea.messages.save.success'), 'success');
      onSuccessRef.current?.();
      dispatch(updateDocumentResponse.data);
    } else if (updateDocumentResponse.hasError) {
      switch (updateDocumentResponse.status?.statusCode) {
        case 400: {
          displayMessage(t(`lea.messages.save.badRequestError`), 'error');
          break;
        }
        case 401: {
          displayMessage(t(`lea.messages.save.unauthorizedError`), 'error');
          break;
        }
        case 403: {
          displayMessage(t(`lea.messages.save.invalidRightsError`), 'error');
          break;
        }
        case 404: {
          displayMessage(t(`lea.messages.save.notFoundError`), 'error');
          break;
        }
        case 500: {
          displayMessage(t(`lea.messages.save.error`), 'error');
          break;
        }
        default:
          displayMessage(t(`lea.messages.save.error`), 'error');
          break;
      }
    }
  }, [updateDocumentResponse]);

  return { updateDocument: updateDocumentCallback };
};

const useUpdateAllDocuments = (
  FirstDocumentdispatch: (document: DocumentDTO) => void,
  SynopsisDocumentDispatch: (document: DocumentDTO) => void,
): UpdateDocuments => {
  const { t } = useTranslation();
  const onSuccessRef = useRef<OnSuccessFunction>();

  const [allDocumentsData, setAllDocumentsData] = useState<DocumentInfo[]>([]);
  const [updatedDocumentIndex, setUpdatedDocumentIndex] = useState(0);
  const [updateDocumentData, setUpdateDocumentData] = useState<DocumentInfo>(initialUpdateDocumentData);
  const updateDocumentsResponseRef = useRef<(DocumentDTO | undefined)[]>([]);

  const [updateDocumentResponse, updateDocument] = DocumentService.update(updateDocumentData.id, {
    content: updateDocumentData.content,
  });
  const updateDocumentsCallback = useCallback((DocumentsInfo: DocumentInfo[], onSuccess?: () => void) => {
    if (allDocumentsData.length > 0) return;
    onSuccessRef.current = onSuccess;
    setAllDocumentsData(DocumentsInfo);
    setUpdateDocumentData(DocumentsInfo[0]);
  }, []);

  useEffect(() => {
    if (updateDocumentData.id != '') {
      updateDocument();
      setUpdatedDocumentIndex((index) => index + 1);
    }
  }, [updateDocumentData]);

  useEffect(() => {
    if (updateDocumentResponse.isLoading) return;
    if (updateDocumentResponse.hasError) {
      displayMessage(t(`lea.messages.save.someError`), 'error');
      return;
    }
    updateDocumentsResponseRef.current = [...updateDocumentsResponseRef.current, updateDocumentResponse.data];
    if (updatedDocumentIndex < allDocumentsData.length) {
      setUpdateDocumentData(allDocumentsData[updatedDocumentIndex]);
      return;
    }
    updateDocumentsResponseRef.current.forEach((document, index) => {
      if (!document) return;
      if (allDocumentsData[index].isFirstDocument) {
        FirstDocumentdispatch({
          ...document,
          history: JSON.parse(JSON.stringify(allDocumentsData[index].history)) as History,
        });
      } else {
        SynopsisDocumentDispatch({
          ...document,
          history: JSON.parse(JSON.stringify(allDocumentsData[index].history)) as History,
        });
      }
    });
    displayMessage(
      t(allDocumentsData.length > 1 ? 'lea.messages.save.allSuccess' : 'lea.messages.save.success'),
      'success',
    );
    onSuccessRef.current?.();
    setUpdateDocumentData(initialUpdateDocumentData);
    setAllDocumentsData([]);
    setUpdatedDocumentIndex(0);
    updateDocumentsResponseRef.current = [];
  }, [updateDocumentResponse]);

  return { updateDocuments: updateDocumentsCallback };
};

export const useFirstDocumentState = (): UseDocumentState => {
  const document = useAppSelector<DocumentDTO[]>(
    (state) => documentsAdapter.getSelectors().selectAll(state.editorStatic[DocumentSlice.name]),
    shallowEqual,
  );

  return {
    document: document[0] || null,
  };
};

export const useFirstDocumentDispatch = (): UseFirstDocumentDispatch => {
  const dispatch = useAppDispatch();

  const addDocument = (document: DocumentDTO) => {
    removeDocumentCallback();
    dispatch(addFirstDocument(document));
  };

  const updateDocumentState = (document: DocumentDTO) => {
    dispatch(updateFirstDocument(document));
  };

  const { fetchDocument } = useFetchDocument(addDocument);

  const { updateDocument } = useUpdateDocument(updateDocumentState);

  const setDocumentCallback = useCallback((document: DocumentDTO) => {
    removeDocumentCallback();
    dispatch(addFirstDocument(document));
  }, []);

  const removeDocumentCallback = useCallback(() => {
    dispatch(removeFirstDocument());
  }, []);

  return {
    fetchDocument,
    removeDocument: removeDocumentCallback,
    updateDocument,
    setDocument: setDocumentCallback,
  };
};

export const useSynopsisDocumentsState = (): UseSynopsisDocumentState => {
  const documents = useAppSelector<DocumentDTO[]>(
    (state) => synopsisDocumentsAdapter.getSelectors().selectAll(state.editorStatic[SynopsisDocumentSlice.name]),
    shallowEqual,
  );

  return {
    documents: documents,
  };
};

export const useAllDocumentsDispatch = (): UpdateDocuments => {
  const dispatch = useAppDispatch();

  const updateFirstDocumentState = (document: DocumentDTO) => {
    dispatch(updateFirstDocument(document));
  };

  const updateSynopsisDocumentState = (document: DocumentDTO) => {
    dispatch(updateSynopsisDocument(document));
  };

  const { updateDocuments } = useUpdateAllDocuments(updateFirstDocumentState, updateSynopsisDocumentState);

  return {
    updateDocuments,
  };
};

export const useSynopsisDocumentsDispatch = (): UseSynopsisDocumentDispatch => {
  const dispatch = useAppDispatch();

  const addDocument = (document: DocumentDTO) => {
    removeDocumentCallback(document.id);
    dispatch(addSynopsisDocument(document));
  };

  const addDocuments = (documents: DocumentDTO[]) => {
    removeDocumentCallback();
    dispatch(addSynopsisDocuments(documents));
  };

  const updateDocumentState = (document: DocumentDTO) => {
    dispatch(updateSynopsisDocument(document));
  };

  const { fetchDocument } = useFetchDocument(addDocument);

  const { fetchDocuments } = useFetchDocuments(addDocuments);

  const { updateDocument } = useUpdateDocument(updateDocumentState);

  const setDocumentsCallback = useCallback((documents: DocumentDTO[]) => {
    removeDocumentCallback();
    dispatch(addSynopsisDocuments(documents));
  }, []);

  const removeDocumentCallback = useCallback((id?: string) => {
    dispatch(removeSynopsisDocuments(id));
  }, []);

  return {
    fetchDocument,
    fetchDocuments,
    removeDocument: removeDocumentCallback,
    updateDocument,
    setDocuments: setDocumentsCallback,
    addDocument,
  };
};

export const useExportDocument = (): ExportDocument => {
  const { t } = useTranslation();
  const [exportValue, setExportValue] = useState<Descendant[]>([]);
  const [document, setDocument] = useState<DocumentDTO | null>();
  const [exportDocumentResponse, exportDocument] = DocumentService.export(document?.id ?? '', {
    content: JSON.stringify(exportValue),
    title: document?.title,
  });

  const onExportCallback = useCallback((value: Descendant[], document: DocumentDTO | null) => {
    setDocument(document);
    setExportValue([...value]);
  }, []);

  useEffect(() => {
    exportValue.length > 0 && exportDocument();
  }, [exportValue]);

  useEffect(() => {
    if (exportDocumentResponse.hasError) {
      displayMessage(t(`lea.messages.export.error`), 'error');
    }

    if (exportDocumentResponse.data && !exportDocumentResponse.hasError) {
      const fileName = `${document ? document.title?.split(' ').join('-') : 'document'}`;
      const documentType = document
        ? document.type
            .toLowerCase()
            .split('_')
            .map((value) => value.charAt(0).toUpperCase() + value.substring(1))
            .join('-')
        : '';
      const xmlData = new Blob([exportDocumentResponse.data], { type: 'text/plain' });
      const link = window.document.createElement('a');
      const dateTime = format(new Date(), 'yyyyMMdd_HHmm');
      const documentTitle = `${dateTime}_Export_${documentType}_${fileName}.xml`;

      link.setAttribute('href', window.URL.createObjectURL(xmlData));
      link.setAttribute('download', documentTitle);
      link.click();
      link.remove();

      displayMessage(t(`lea.messages.export.success`), 'success');
      setExportValue([]);
    }
  }, [exportDocumentResponse]);

  return { exportDocument: onExportCallback };
};
