// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../Api';
import { ExportPdfDTO } from './ExportPdf';

const exportPdf = (exportInformation?: ExportPdfDTO) => {
  const path = 'documents/multiple/export/pdf';
  const mimeSubtype = exportInformation?.mergeDocuments ? 'pdf' : 'zip';
  return Api.exportDocument<ExportPdfDTO | undefined>(path, exportInformation, mimeSubtype);
};

export const ExportPdfService = { exportPdf };
