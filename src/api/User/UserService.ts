// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BasePath, BaseUrl } from '../../constants';
import { Api } from '../Api';
import { CompoundDocumentDTO } from '../CompoundDocument';
import { UserSettingsDTO } from './UserSettings';

type UserResponse = {
  status: number;
};

export class UserService {
  public static async getUser(): Promise<UserResponse> {
    const response = await fetch(`${BaseUrl}${BasePath}/user`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + Api.getJWToken(),
      },
      credentials: 'include',
    });
    const status = response.status;
    return { status };
  }
}

const getUserSettings = () => {
  const path = 'user/settings';
  return Api.getObject<UserSettingsDTO>(path);
};

const updateUserSettings = (userSettings: UserSettingsDTO | null) => {
  const path = 'user/settings';
  return Api.update<UserSettingsDTO | null, CompoundDocumentDTO>(userSettings, path, undefined, 'PUT');
};

export const UserSettingsService = {
  getUserSettings,
  updateUserSettings,
};
