// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../Api';
import { CreateCompoundDocumentDTO, CompoundDocumentDTO, ImportCompoundDocumentDTO } from '.';
import { CreateDocumentDTO, DocumentDTO } from '../Document';
import {
  BestandsrechtListDTO,
  CompoundDocumentPermissionDTO,
  CompoundDocumentPermissionUpdateDTO,
  CompoundDocumentSummaryDTO,
  CompoundDocumentTitleDTO,
  CompundDocumentStateChangeDTO,
  EgfaModulInfo,
  EgfaUpdatedModulesDTO,
  RenameCompundDocumentDTO,
} from './CompoundDocument';

const getAllTitles = () => {
  return Api.getAll<CompoundDocumentTitleDTO>('compounddocumenttitles');
};

const getById = (compoundDocumentId: string) => {
  return Api.getById<CompoundDocumentDTO>(compoundDocumentId, 'compounddocuments', decodeCompoundDocumentDTO);
};

const getSummaryById = (compoundDocumentId: string) => {
  return Api.getById<CompoundDocumentSummaryDTO>(compoundDocumentId, 'compounddocuments', undefined, '/list');
};

const getPermissionsById = (compoundDocumentId: string, permission: string) => {
  return Api.getById<CompoundDocumentPermissionDTO[]>(
    compoundDocumentId,
    'compounddocuments',
    undefined,
    `/allowedusers/${permission}`,
  );
};

const create = (compoundDocument: CreateCompoundDocumentDTO) => {
  return Api.create<CreateCompoundDocumentDTO, CompoundDocumentDTO>(compoundDocument, 'compounddocuments');
};

const importDocument = (importDocument: ImportCompoundDocumentDTO, compoundDocumentId: string) => {
  importDocument = encodeImportDocumentDTO(importDocument);
  return Api.create<ImportCompoundDocumentDTO, CompoundDocumentDTO>(
    importDocument,
    'compounddocuments/' + compoundDocumentId + '/documents/import',
  );
};

const createDocument = (document: CreateDocumentDTO, compoundDocumentId: string) => {
  return Api.create<CreateDocumentDTO, DocumentDTO>(document, 'compounddocuments/' + compoundDocumentId + '/documents');
};

const update = (compundDocumentId: string, compoundDocument: Partial<CompoundDocumentDTO>) => {
  compoundDocument = encodeCompoundDocumentDTO(compoundDocument);
  const path = 'compounddocuments/' + compundDocumentId;
  return Api.update<CompoundDocumentDTO, CompoundDocumentDTO>(compoundDocument, path, decodeCompoundDocumentDTO);
};

const renameCompundDocument = (compoundDocumentId: string, title: RenameCompundDocumentDTO) => {
  const path = 'compounddocuments/' + compoundDocumentId + '/title';
  return Api.update<RenameCompundDocumentDTO, RenameCompundDocumentDTO>(title, path);
};

const changeStatus = (compoundDocumentId: string, status: CompundDocumentStateChangeDTO) => {
  const path = 'compounddocuments/' + compoundDocumentId;
  return Api.update<CompundDocumentStateChangeDTO, CompundDocumentStateChangeDTO>(status, path);
};

const exportCompoundDocument = (compoundDocumentId: string) => {
  const path = `compounddocuments/${compoundDocumentId}/export`;
  const mimeSubtype = 'zip';
  return Api.exportDocument(path, undefined, mimeSubtype);
};

const updateCompoundDocumentPermissions = (
  compoundDocumentId: string,
  permissions: CompoundDocumentPermissionUpdateDTO[],
) => {
  const path = 'compounddocuments/' + compoundDocumentId + '/allowedusers';
  return Api.create<CompoundDocumentPermissionUpdateDTO[], CompoundDocumentPermissionUpdateDTO[]>(permissions, path);
};

const updateCompoundDocumentWritePermissions = (
  compoundDocumentId: string,
  permissions: CompoundDocumentPermissionUpdateDTO | null,
) => {
  const path = 'compounddocuments/' + compoundDocumentId + '/changewritepermissions';
  return Api.create<CompoundDocumentPermissionUpdateDTO | null, CompoundDocumentPermissionUpdateDTO>(permissions, path);
};

const createNewVersion = (
  compoundDocumentId: string,
  documentIdList: string[],
  compoundDocument: Partial<CompoundDocumentDTO>,
) => {
  const path =
    'compounddocuments/' +
    compoundDocumentId +
    '/version?' +
    documentIdList.map((documentId) => 'copyId=' + documentId).join('&');
  return Api.create<Partial<CompoundDocumentDTO>, Partial<CompoundDocumentDTO>>(compoundDocument, path);
};

const getEgfaData = (compoundDocumentId: string) => {
  return Api.getById<EgfaModulInfo[]>(compoundDocumentId, 'compounddocuments', undefined, '/egfaStatus');
};

const getBestandsrecht = (compoundDocumentId: string) => {
  return Api.getById<BestandsrechtListDTO[]>(compoundDocumentId, 'compounddocuments', undefined, '/bestandsrecht');
};

const importEgfaData = (compoundDocumentId: string, updatedModules: string[], createCopy: boolean) => {
  const updatedModulesDTO: EgfaUpdatedModulesDTO = { moduleNames: updatedModules, createCopy };
  const path = 'compounddocuments/' + compoundDocumentId + '/egfadata';
  return Api.create<EgfaUpdatedModulesDTO, any>(updatedModulesDTO, path);
};

const getDrucksache = (compoundDocumentId: string) => {
  return Api.exportDocument(`druck/${compoundDocumentId}`, undefined, 'pdf', 'GET');
};

const createDrucksache = (compoundDocumentId: string) => {
  return Api.update<any, any>({}, 'compounddocuments/' + compoundDocumentId + '/druckSpeichern', undefined, 'PUT');
};

const decodeCompoundDocumentDTO = (compoundDocument: CompoundDocumentDTO): CompoundDocumentDTO => {
  const decodedDocuments = compoundDocument.documents.map((document) => {
    const decodedContent = Api.decodeString(document.content);
    document.content = decodedContent;
    return document;
  });
  compoundDocument.documents = decodedDocuments;
  return compoundDocument;
};

const encodeCompoundDocumentDTO = (compoundDocument: Partial<CompoundDocumentDTO>): Partial<CompoundDocumentDTO> => {
  if (compoundDocument.documents) {
    const encodedDocuments = compoundDocument.documents.map((document) => {
      const encodedContent = Api.encodeString(document.content);
      document.content = encodedContent;
      return document;
    });
    compoundDocument.documents = encodedDocuments;
  }
  return compoundDocument;
};

const encodeImportDocumentDTO = (document: ImportCompoundDocumentDTO): ImportCompoundDocumentDTO => {
  document.xmlContent = Api.encodeString(document.xmlContent);
  return document;
};

const decodeImportDocumentDTO = (document: ImportCompoundDocumentDTO): ImportCompoundDocumentDTO => {
  document.xmlContent = Api.decodeString(document.xmlContent);
  return document;
};

export const CompoundDocumentService = {
  getAllTitles,
  getById,
  getSummaryById,
  create,
  createDocument,
  update,
  renameCompundDocument,
  changeStatus,
  exportCompoundDocument,
  createNewVersion,
  getEgfaData,
  importEgfaData,
  getDrucksache,
  createDrucksache,
  importDocument,
  encodeCompoundDocumentDTO,
  decodeCompoundDocumentDTO,
  decodeImportDocumentDTO,
  getPermissionsById,
  updateCompoundDocumentPermissions,
  updateCompoundDocumentWritePermissions,
  getBestandsrecht,
};
