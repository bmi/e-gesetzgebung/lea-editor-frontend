// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FreigabeType } from '@plateg/rest-api/models';
import { PropositionDTO } from '..';
import { DocumentDTO, DocumentStateType, DocumentSummaryDTO } from '../Document';

export enum CompoundDocumentType {
  STAMMGESETZ = 'STAMMGESETZ',
  MANTELGESETZ = 'MANTELGESETZ',
  STAMMVERORDNUNG = 'STAMMVERORDNUNG',
  AENDERUNGSVERORDNUNG = 'AENDERUNGSVERORDNUNG',
}

export enum EgfaModulType {
  ERFUELLUNGSAUFWAND = 'ERFUELLUNGSAUFWAND',
  EA_OEHH = 'EA_OEHH',
  PREISE = 'PREISE',
  KMU = 'KMU',
  SONSTIGE_KOSTEN = 'SONSTIGE_KOSTEN',
  ENAP = 'ENAP',
  GLEICHWERTIGKEIT = 'GLEICHWERTIGKEIT',
  DEMOGRAFIE = 'DEMOGRAFIE',
  GLEICHSTELLUNG = 'GLEICHSTELLUNG',
  DISABILITY = 'DISABILITY',
  VERBRAUCHER = 'VERBRAUCHER',
  WEITERE = 'WEITERE',
  EVALUIERUNG = 'EVALUIERUNG',
}

export enum EgfaModulStatus {
  MODUL_NO_DATA = 'MODUL_NO_DATA',
  MODUL_NO_NEWER_DATA = 'MODUL_NO_NEWER_DATA',
  MODUL_FINISHED_NOT_USED = 'MODUL_FINISHED_NOT_USED',
  MODUL_DATA_NEW_UPDATE = 'MODUL_DATA_NEW_UPDATE',
}

export interface CompoundDocumentDTO {
  createdAt: string;
  updatedAt: string;
  id: string;
  title: string;
  proposition: PropositionDTO;
  type: CompoundDocumentType;
  documents: DocumentDTO[];
  version: string;
}

export interface CompoundDocumentTitleDTO {
  id: string;
  title: string;
  type: CompoundDocumentType;
  anzahlAnlagen: number;
}

export interface RenameCompundDocumentDTO {
  title: string;
}

export interface CompundDocumentStateChangeDTO {
  status: DocumentStateType;
}

export interface CreateCompoundDocumentDTO {
  regelungsVorhabenId: string;
  title: string;
  type: CompoundDocumentType;
  titleRegulatoryText?: string;
  initialNumberOfLevels?: number;
}

export interface ImportCompoundDocumentDTO {
  title: string;
  xmlContent: string;
}

export interface CompoundDocumentSummaryDTO {
  id: string;
  title: string;
  type: CompoundDocumentType;
  version: string;
  createdAt: string;
  updatedAt: string;
  documents: DocumentSummaryDTO[];
  children?: DocumentSummaryDTO[];
}

export interface CompoundDocumentPermissionDTO {
  gid: string;
  name: string;
  email: string;
}

export interface CompoundDocumentPermissionUpdateDTO {
  gid: string;
  email: string;
  name: string;
  freigabetyp: FreigabeType;
  befristung?: string;
  erstelleNeueVersionBeiDraft?: boolean;
  anmerkungen?: string;
}

export interface EgfaModulInfo {
  egfaDataStatus: EgfaModulStatus;
  egfaDmUpdateDate: string | null;
  egfaDmUpdateUser: string | null;
  egfaModuleId: EgfaModulType;
}

export interface EgfaUpdatedModulesDTO {
  moduleNames: string[];
  createCopy: boolean;
}

export interface BestandsrechtListDTO {
  id: string;
  eli: string;
  titelKurz: string;
  titelLang: string;
}
