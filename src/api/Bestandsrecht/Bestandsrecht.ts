// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export interface BestandsrechtResponseDTO {
  id: string;
  eli: string;
  titelKurz: string;
  titelLang: string;
  verknuepfteDokumentenMappeId: string | null;
}

export interface BestandsrechtUpdateDTO {
  id: string;
  verknuepfteDokumentenMappeId: string | null;
}
