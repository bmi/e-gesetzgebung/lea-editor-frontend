// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../Api';
import { BestandsrechtResponseDTO, BestandsrechtUpdateDTO } from './Bestandsrecht';

const getAll = (regelungsvorhabenId: string) => {
  const path = `compounddocuments/${regelungsvorhabenId}/bestandsrecht/dm`;
  return Api.getAll<BestandsrechtResponseDTO>(path);
};

const updateBestandrechtLinks = (regelungsvorhabenId: string, linkedBestandsrecht: BestandsrechtUpdateDTO[]) => {
  const path = `compounddocuments/${regelungsvorhabenId}/bestandsrechte`;
  return Api.update<BestandsrechtUpdateDTO[], BestandsrechtResponseDTO[]>(linkedBestandsrecht, path, undefined, 'PUT');
};

export const BestandsrechtService = {
  getAll,
  updateBestandrechtLinks,
};
