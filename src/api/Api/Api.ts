// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useRef, useState, Dispatch, SetStateAction } from 'react';
import { BaseUrl, BasePath } from '../../constants';
import { Buffer } from 'buffer';
import { RootState, store } from '@plateg/theme/src/components/store';

export interface ApiResponse<T> {
  isLoading: boolean;
  hasError: boolean;
  data?: T;
  status?: {
    statusCode: number;
    statusText: string;
  };
}

export type ApiRequest = () => void;

export type ApiReturnType<RT> = [ApiResponse<RT>, ApiRequest];

export class Api {
  public static getAll = <RT>(path: string, decode?: (resultDTO: RT) => RT): ApiReturnType<RT[]> => {
    const apiResponseRef = useRef<ApiResponse<RT[]>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT[]>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
      })
        .then((response) => {
          if (!response.ok) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
          } else if (response.status === 204) {
            return [];
          }
          return response.json();
        })
        .then((result) => {
          let resultDecoded: RT[] = [];
          if ((result as RT[]).length > 0) {
            resultDecoded = (result as RT[]).map((resultDTO) => (decode ? decode(resultDTO) : resultDTO));
          }
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static getObject = <RT>(path: string, decode?: (resultDTO: RT) => RT): ApiReturnType<RT> => {
    const apiResponseRef = useRef<ApiResponse<RT>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
      })
        .then((response) => {
          if (!response.ok) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
          } else if (response.status === 204) {
            return [];
          }
          return response.json();
        })
        .then((result) => {
          const resultDecoded = decode ? decode(result as RT) : (result as RT);
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef.current);
        })
        .catch(() => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static getById = <RT>(
    id: string,
    path: string,
    decode?: (resultDTO: RT) => RT,
    additionalPath = '',
  ): ApiReturnType<RT> => {
    const apiResponseRef = useRef<ApiResponse<RT>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}/${id}${additionalPath}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
      })
        .then((response) => {
          if (!response.ok) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
          } else if (response.status === 204) {
            return [];
          }
          return response.json();
        })
        .then((result) => {
          const resultDecoded = decode ? decode(result as RT) : (result as RT);
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef.current);
        })
        .catch(() => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static create = <CT, RT>(entity: CT, path: string, decode?: (resultDTO: RT) => RT): ApiReturnType<RT> => {
    const apiResponseRef = useRef<ApiResponse<RT>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: JSON.stringify(entity),
      })
        .then((response) => {
          if (response.status !== 200 && response.status !== 201 && response.status !== 204) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
            return [];
          }
          return response.json();
        })
        .then((result) => {
          const resultDecoded = decode ? decode(result as RT) : (result as RT);
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef.current);
        })
        .catch(() => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: '' as RT,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static delete = <RT, CT>(path: string, entity?: CT): ApiReturnType<RT> => {
    const apiResponseRef = useRef<ApiResponse<RT>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: entity ? JSON.stringify(entity) : '',
      })
        .then((response) => {
          if (response.status !== 200 && response.status !== 204) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
            return [];
          }
          return response.json();
        })
        .then((result) => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: result as RT,
          };
          setApiResponse(apiResponseRef.current);
        })
        .catch(() => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: '' as RT,
          };
          setApiResponse(apiResponseRef.current);
        });
    };
    return [apiResponse, apiRequest];
  };

  public static createXml = <CT>(entity: CT, path: string): ApiReturnType<string> => {
    const apiResponseRef = useRef<ApiResponse<string>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<string>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          Accept: 'application/xml',
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: JSON.stringify(entity),
      })
        .then((response) => {
          if (response.status !== 200) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
          }
          return response.text();
        })
        .then((result) => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: result,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static exportDocument = <CT>(
    path: string,
    entity?: CT,
    mimeSubtype?: string,
    method = 'POST',
  ): ApiReturnType<Blob | null> => {
    const apiResponseRef = useRef<ApiResponse<Blob | null>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<Blob | null>>(apiResponseRef.current);

    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: method,
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          Accept: `application/${mimeSubtype}`,
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: JSON.stringify(entity),
      })
        .then((response) => {
          if (!response.ok) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
              status: {
                statusCode: response.status,
                statusText: response.statusText,
              },
            };
            return null;
          } else return response.blob();
        })
        .then((result) => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: result,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static update = <CT, RT>(
    entity: Partial<CT>,
    path: string,
    decode?: (resultDTO: RT) => RT,
    method?: 'PATCH' | 'PUT',
  ): ApiReturnType<RT> => {
    const apiResponseRef = useRef<ApiResponse<RT>>({ isLoading: true, hasError: false });
    const [apiResponse, setApiResponse] = useState<ApiResponse<RT>>(apiResponseRef.current);
    const apiRequest = () => {
      apiResponseRef.current = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: method ?? 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: JSON.stringify(entity),
      })
        .then((response) => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            status: {
              statusCode: response.status,
              statusText: response.statusText,
            },
          };
          if (!response.ok) {
            apiResponseRef.current = {
              ...apiResponseRef.current,
              hasError: true,
            };
          } else if (response.status === 204) {
            return [];
          }
          return response.json();
        })
        .then((result) => {
          const resultDecoded = decode ? decode(result as RT) : (result as RT);
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef.current);
        })
        .catch(() => {
          apiResponseRef.current = {
            ...apiResponseRef.current,
            isLoading: false,
          };
          setApiResponse(apiResponseRef.current);
        });
    };

    return [apiResponse, apiRequest];
  };

  public static put<CT, RT>(
    entity: CT,
    path: string,
    setApiResponse: Dispatch<SetStateAction<ApiResponse<RT>>>,
    decode?: (resultDTO: RT) => RT,
  ): ApiRequest {
    let apiResponseRef: ApiResponse<RT> = { isLoading: true, hasError: false };

    return () => {
      apiResponseRef = { isLoading: true, hasError: false };
      void fetch(`${BaseUrl}${BasePath}/${path}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'X-XSRF-TOKEN-EDITOR': this.getCsrfToken(),
          Authorization: 'Bearer ' + this.getJWToken(),
          ...this.getStellvertreterHeader(),
        },
        credentials: 'include',
        body: JSON.stringify(entity),
      })
        .then((response) => {
          apiResponseRef = {
            ...apiResponseRef,
            status: {
              statusCode: response.status,
              statusText: response.statusText,
            },
          };
          if (!response.ok) {
            apiResponseRef = {
              ...apiResponseRef,
              hasError: true,
            };
          } else if (response.status === 204) {
            return [];
          }
          return response.json();
        })
        .then((result) => {
          const resultDecoded = decode ? decode(result as RT) : (result as RT);
          apiResponseRef = {
            ...apiResponseRef,
            isLoading: false,
            data: resultDecoded,
          };
          setApiResponse(apiResponseRef);
        })
        .catch(() => {
          apiResponseRef = {
            ...apiResponseRef,
            isLoading: false,
          };
          setApiResponse(apiResponseRef);
        });
    };
  }

  public static decodeString(string: string): string {
    return decodeURIComponent(
      Buffer.from(string, 'base64')
        .toString('binary')
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join(''),
    );
  }

  public static encodeString(string: string): string {
    return Buffer.from(
      encodeURIComponent(string).replace(/%([0-9A-F]{2})/g, function (_match, p1) {
        return String.fromCharCode(parseInt(p1 as string, 16));
      }),
      'binary',
    ).toString('base64');
  }

  public static getJWToken(): string {
    if (process.env.NODE_ENV === 'test') return '';
    return (store.getState() as RootState).kcToken.kcToken?.token || '';
  }

  private static getCsrfToken(): string {
    return ('; ' + document.cookie).split('; XSRF-TOKEN-EDITOR=').pop()?.split(';').shift() ?? '';
  }

  private static getStellvertreterHeader(): HeadersInit {
    const user = (store.getState() as RootState).user.user;
    let header = {};
    if (user?.dto.stellvertreter && user?.dto.gid) {
      header = { 'X-DE-BUND-EGG-Surrogate-For': user.dto.gid };
    }
    return header;
  }
}
