// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { CompoundDocumentInfo, DocumentInfo } from './HomePage/HomePage';
import { useWizardVisibleDispatch, Wizards } from '../Wizard';
import { useFirstDocumentDispatch } from '../../api';
import { setCompoundDocumentInfo, setDocumentInfo } from './DocumentsTableActionSlice';
import { useAppDispatch, useAppSelector } from '../Store';

type UseDocumentsTableActionState = {
  documentInfo: DocumentInfo;
  compoundDocumentInfo: CompoundDocumentInfo;
};

type UseDocumentsTableActionDispatch = {
  onCreateNewDocument: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onCreateNewCompoundDocument: () => void;
  onExportCompoundDocument: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onRenameDocument: (documentInfo: DocumentInfo) => void;
  onNewVersion: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onStartSynopsisView: (documentInfo: DocumentInfo) => void;
  onImportDocument: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onReadRights: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onEgfa: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onWriteRights: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onExportPdf: (compoundDocumentInfo: CompoundDocumentInfo) => void;
  onAssignBestandsrecht: (compoundDocumentInfo: CompoundDocumentInfo) => void;
};

export const useDocumentsTableActionState = (): UseDocumentsTableActionState => {
  const { compoundDocumentInfo, documentInfo } = useAppSelector((state) => state.editorStatic.documentsTableAction);

  return { compoundDocumentInfo, documentInfo };
};

export const useDocumentsTableActionDispatch = (): UseDocumentsTableActionDispatch => {
  const dispatch = useAppDispatch();
  const firstDocument = useFirstDocumentDispatch();
  //Sichtbarkeit des Dokumenten-Wizards
  const documentWizardDispatch = useWizardVisibleDispatch(Wizards.NEW_DOCUMENT);
  //Sichtbarkeit des Import-Wizards
  const importDocumentWizardDispatch = useWizardVisibleDispatch(Wizards.IMPORT_DOCUMENT);
  //Sichtbarkeit des Dokumenten-Mappen-Wizards
  const newCompoundDocumentWizardDispatch = useWizardVisibleDispatch(Wizards.NEW_COMPOUND_DOCUMENT);
  //Sichtbarkeit des Export-Wizards
  const exportCompoundDocumentWizardDispatch = useWizardVisibleDispatch(Wizards.EXPORT_COMPOUNDDOCUMENT);
  //Sichtbarkeit des Rename-Document-Wizards
  const renameDocumentWizardDispatch = useWizardVisibleDispatch(Wizards.RENAME_DOCUMENT);
  //Sichtbarkeit des Leserechtevergabe Wizard
  const setReadRightsWizardDispath = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_READ_RIGHTS);
  //Sichtbarkeit des Schreibrechtevergabe Wizard
  const setWriteRightsWizardDispath = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_WRITE_RIGHTS);
  //Sichtbarkeit des Versions-Wizards
  const newVersionWizardDispatch = useWizardVisibleDispatch(Wizards.NEW_VERSION);
  //Sichtbarkeit des Synopsen-Wizards
  const synopsisWizardDispatch = useWizardVisibleDispatch(Wizards.SYNOPSIS);
  //Sichtbarkeit des Egfa-Wizards
  const egfaWizardDispatch = useWizardVisibleDispatch(Wizards.EGFA);
  // Sichtbarkeit des Export-PDF-Wizards
  const exportPdfWizardDispatch = useWizardVisibleDispatch(Wizards.EXPORT_PDF);
  // Sichtbarkeit des Bestandsrecht-zuweisen-Wizards
  const assignBestandsrechtWizardDispatch = useWizardVisibleDispatch(Wizards.ASSIGN_BESTANDSRECHT);

  const onCreateNewDocumentHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    documentWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onCreateNewCompoundDocumentHandler = useCallback(() => newCompoundDocumentWizardDispatch.openWizard(), []);
  const onExportCompoundDocumentHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    exportCompoundDocumentWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onRenameDocumentHandler = useCallback((documentInfo: DocumentInfo) => {
    dispatch(setDocumentInfo(documentInfo));
    renameDocumentWizardDispatch.openWizard();
  }, []);
  const onNewVersionHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
    newVersionWizardDispatch.openWizard();
  }, []);
  const onStartSynopsisViewHandler = useCallback((documentInfo: DocumentInfo) => {
    synopsisWizardDispatch.openWizard();
    firstDocument.fetchDocument(documentInfo.id);
  }, []);
  const onImportDocumentHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    importDocumentWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onReadRightsHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    setReadRightsWizardDispath.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onEgfaHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    egfaWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onWriteRightsHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    setWriteRightsWizardDispath.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);
  const onExportPdfHandler = useCallback((compoundDocumentInfo: CompoundDocumentInfo) => {
    exportPdfWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compoundDocumentInfo));
  }, []);

  const onAssignBestandsrechtHandler = useCallback((compundDocumentInfo: CompoundDocumentInfo) => {
    assignBestandsrechtWizardDispatch.openWizard();
    dispatch(setCompoundDocumentInfo(compundDocumentInfo));
  }, []);

  return {
    onCreateNewDocument: onCreateNewDocumentHandler,
    onCreateNewCompoundDocument: onCreateNewCompoundDocumentHandler,
    onExportCompoundDocument: onExportCompoundDocumentHandler,
    onImportDocument: onImportDocumentHandler,
    onNewVersion: onNewVersionHandler,
    onReadRights: onReadRightsHandler,
    onWriteRights: onWriteRightsHandler,
    onRenameDocument: onRenameDocumentHandler,
    onStartSynopsisView: onStartSynopsisViewHandler,
    onEgfa: onEgfaHandler,
    onExportPdf: onExportPdfHandler,
    onAssignBestandsrecht: onAssignBestandsrechtHandler,
  };
};
