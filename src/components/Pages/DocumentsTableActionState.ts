// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CompoundDocumentType, DocumentStateType, EntityType } from '../../api';
import { CompoundDocumentInfo, DocumentInfo } from './HomePage/HomePage';

type DocumentsTableActionState = {
  compoundDocumentInfo: CompoundDocumentInfo;
  documentInfo: DocumentInfo;
};

const InitialDocumentsTableActionState: DocumentsTableActionState = {
  compoundDocumentInfo: {
    id: '',
    type: CompoundDocumentType.STAMMGESETZ,
    version: '',
    title: '',
    documents: [],
    compoundDocumentState: DocumentStateType.DRAFT,
  },
  documentInfo: {
    entity: EntityType.SINGLE_DOCUMENT,
    id: '',
    title: '',
  },
};

export { DocumentsTableActionState, InitialDocumentsTableActionState };
