// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  BreadcrumbComponent,
  GlobalDI,
  HeaderComponent,
  HeaderController,
  Loading,
  TableComponent,
  TableComponentProps,
  displayMessage,
} from '@plateg/theme';
import { Button, Layout, Typography } from 'antd';
import React, { FunctionComponent, useContext, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import {
  ButtonType,
  EntityType,
  HomepageCompoundDocumentDTO,
  PaginationDTO,
  RegulatoryProposalService,
} from '../../../api';
import { StandaloneContext } from '../../App/App';
import {
  DocumentWizards,
  HomePageDTO,
  HomepageTabs,
  Main,
  Readme,
  createDocumentsTableData,
  getFilterButton,
  getFilteredRows,
} from '../../Elements';
import { addExpandAndCollapseButtons } from '../../Elements/HomepageDocumentsTables/ShowAllOrLessButton/utils';
import { HomePageHelp } from '../HomePage/HomePageHelp';
import { useDocumentsTableActionState } from '../useDocumentsTableAction';
const { Text } = Typography;

import { EyeIconCurrentColor, OutlinedDirectionalLeftIcon } from '@lea/ui';
import { FarbeType, PaginierungDTOSortDirectionEnum } from '@plateg/rest-api';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  setPaginationFilterInfo,
  setPaginationResult,
  setPaginationInitState,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { UserSettingsService } from '../../../api/User/UserService';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../api/User/UserSettings';
import { useAppInfoState } from '../../App';
import { useHomePageTabState } from '../HomePage';
import './VersionHistoryPage.less';

interface VersionHistoryPageParams {
  regulatoryProposalId: string;
}

type TableCompPropsHomepageVersionHistoryTableWithoutId = Omit<TableComponentProps<HomePageDTO>, 'id'>;

const VersionHistoryPage: FunctionComponent = () => {
  const { t } = useTranslation();
  const isStandalone = useContext(StandaloneContext);
  const homePageTab = useHomePageTabState();
  const { basePath } = useAppInfoState();
  const { regulatoryProposalId } = useParams<VersionHistoryPageParams>();
  const [versionHistoryTableData, setVersionHistoryTableData] =
    useState<TableCompPropsHomepageVersionHistoryTableWithoutId>({
      columns: [],
      content: [],
      expandable: false,
      sorterOptions: [],
    });
  const { Header } = Layout;
  const [pagination, setPagination] = useState<PaginationDTO>();
  const [versionHistoryResponse, getVersionHistory] = RegulatoryProposalService.getVersionHistory(
    regulatoryProposalId,
    pagination,
  );
  const [isLoading, setIsLoading] = useState(true);
  const expandedCompoundDocumentIdsRef = useRef(new Set<string>());
  const headerController = GlobalDI.getOrRegister('editorHeaderController', () => new HeaderController());
  const [breadcrumbText, setBreadcrumbText] = useState('');
  const [updateTable, setUpdateTable] = useState(false);
  const [hasHover, setHasHover] = useState<boolean>(false);
  const [buttonType, setButtonType] = useState<ButtonType>(ButtonType.SHOW_ALL);
  const [userSettingsResponse, fetchUserSettings] = UserSettingsService.getUserSettings();
  const [userSettings, setUserSettings] = useState<UserSettingsDTO | null>(null);
  const userSettingsRef = useRef<PinnedCompoundDocumentsSettings | null>(null);
  const [userSettingsUpdateResponse, updateUserSettingsResponse] = UserSettingsService.updateUserSettings(userSettings);
  const isInitialUpdate = useRef(true);
  const dispatch = useAppDispatch();
  const pagDataRequest = useAppSelector((state) => state.tablePagination.tabs['versionHistory']).request;
  const { compoundDocumentInfo, documentInfo } = useDocumentsTableActionState();

  useEffect(() => {
    fetchUserSettings();
  }, []);

  useEffect(() => {
    if (userSettingsResponse.isLoading) return;
    if (userSettingsResponse.hasError) {
      displayMessage('', 'error');
    } else if (userSettingsResponse.data) {
      const initialUserSettings = userSettingsResponse.data;
      if (initialUserSettings.pinnedDokumentenmappen === undefined) {
        setUserSettings({ pinnedDokumentenmappen: [] });
        userSettingsRef.current = { pinnedDokumentenmappen: [], lastChange: undefined };
      } else {
        setUserSettings(initialUserSettings);
        userSettingsRef.current = {
          pinnedDokumentenmappen: initialUserSettings.pinnedDokumentenmappen,
          lastChange: undefined,
        };
      }
    }
  }, [userSettingsResponse]);

  useEffect(() => {
    if (userSettings) {
      updateUserSettingsResponse();
    }
  }, [userSettings]);

  useEffect(() => {
    if (userSettingsResponse.isLoading) return;
    if (isInitialUpdate.current) {
      isInitialUpdate.current = false;
      return;
    }
    if (userSettingsResponse.hasError) {
      displayMessage(t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.pinError'), 'error');
    } else {
      if (userSettingsRef.current?.lastChange === 'pinned') {
        displayMessage(t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.pinSuccess'), 'success');
      } else if (userSettingsRef.current?.lastChange === 'unpinned') {
        displayMessage(
          t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.releaseSuccess'),
          'success',
        );
      }
    }
  }, [userSettingsUpdateResponse]);

  const getPreviousPage = () => {
    switch (homePageTab as any) {
      case HomepageTabs.MEINE_DOKUMENTE:
        return 'Meine Dokumente';
      case HomepageTabs.ABSTIMMUNGEN:
        return 'Freigegebene Dokumente';
      default:
        return 'Meine Dokumente';
    }
  };

  const setBreadcrumb = () => {
    const mainText = [
      <Link key="editor-home" to={`${basePath}`} id="editor-home">
        {t('lea.startseite.breadcrumb.projectName')} - {getPreviousPage()}
      </Link>,
      <span key="version-history" style={{ fontWeight: 'bold' }}>
        {t('lea.startseite.breadcrumb.versionHistory', { title: breadcrumbText })}
      </span>,
    ];
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={mainText} />],
      headerCenter: [],
      headerRight: [],
      headerLast: [<HomePageHelp key="editor-header-help-link" />],
    });
  };

  useEffect(() => {
    setPagination({
      pageNumber: 0,
      pageSize: 5,
      sortBy: 'VERSION',
      sortDirection: PaginierungDTOSortDirectionEnum.Asc,
    });
    setVersionHistoryTableData(
      createDocumentsTableData({
        content: [],
        t,
        expandedCompoundDocumentIdsRef,
        setShowAllOrLessButtonType: setButtonType,
        homepageTab: HomepageTabs.VERSIONSHISTORIE,
        userSettingsRef: userSettingsRef,
        setUserSettings: setUserSettings,
        setShouldUpdateTable: setUpdateTable,
      }),
    );
    return () => {
      dispatch(setPaginationInitState({ tabKey: 'versionHistory' }));
    };
  }, []);

  useEffect(() => {
    if (
      pagination?.pageNumber !== pagDataRequest.currentPage ||
      pagination?.sortBy !== pagDataRequest?.columnKey ||
      pagination?.sortDirection !== pagDataRequest.sortOrder
    ) {
      setPagination({
        pageNumber: isInitialUpdate.current ? 0 : (pagDataRequest.currentPage ?? 0),
        pageSize: 5,
        sortBy: 'VERSION',
        sortDirection: PaginierungDTOSortDirectionEnum.Asc,
      });
    }
  }, [pagDataRequest.currentPage, pagDataRequest.columnKey, pagDataRequest.sortOrder, pagDataRequest.filters]);

  useEffect(() => {
    if (pagination) {
      getVersionHistory();
      setIsLoading(true);
    }
  }, [pagination]);

  useEffect(() => {
    if (versionHistoryResponse.isLoading) return;
    if (versionHistoryResponse.hasError) {
      setIsLoading(false);
      displayMessage(t('lea.messages.loadVersionshistorie.error'), 'error');
      return;
    }

    if (versionHistoryResponse.data) {
      setIsLoading(false);
      const { content, totalElements, number } = versionHistoryResponse.data.dtos;
      const { filterNames, vorhabenartFilter, allContentEmpty } = versionHistoryResponse.data;

      dispatch(
        setPaginationResult({
          tabKey: 'versionHistory',
          totalItems: totalElements,
          currentPage: number,
          allContentEmpty,
        }),
      );
      dispatch(setPaginationFilterInfo({ tabKey: 'versionHistory', vorhabenartFilter, filterNames }));
      setBreadcrumbText(content?.[0].abbreviation ?? '');
      setVersionHistoryTableData({
        ...versionHistoryTableData,
        content: addExpandAndCollapseButtons(
          JSON.parse(JSON.stringify(content?.[0].children)),
        ) as HomepageCompoundDocumentDTO[],
      });
    }
  }, [versionHistoryResponse]);

  useEffect(() => {
    if (breadcrumbText !== '') {
      setBreadcrumb();
    }
  }, [breadcrumbText]);

  const handleClick = () => {
    window.location.hash = `#${basePath}`;
  };

  const getVersionHistoryFilterButton = () => {
    if (versionHistoryResponse.data) {
      const regulatoryProposal = versionHistoryResponse.data.dtos.content?.[0];
      return getFilterButton(regulatoryProposal ? [regulatoryProposal] : [], {
        name: 'Regelungsvorhaben',
        columnIndex: 0,
      });
    } else {
      return getFilterButton(
        [{ abbreviation: '', children: [], id: '', entityType: EntityType.REGULATORY_PROPOSAL, farbe: FarbeType.Blau }],
        {
          name: 'Regelungsvorhaben',
          columnIndex: 0,
        },
      );
    }
  };

  const getVersionHistoryFilteredRows = () => {
    if (versionHistoryResponse.data) {
      const regulatoryProposal = versionHistoryResponse.data.dtos.content?.[0];
      return getFilteredRows(regulatoryProposal ? [regulatoryProposal] : [], regulatoryProposal?.abbreviation ?? '', {
        name: 'Regelungsvorhaben',
        columnIndex: 0,
      });
    } else {
      return getFilteredRows(
        [{ abbreviation: '', children: [], id: '', entityType: EntityType.REGULATORY_PROPOSAL, farbe: FarbeType.Blau }],
        '',
        {
          name: 'Regelungsvorhaben',
          columnIndex: 0,
        },
      );
    }
  };

  const handleMouseOver = () => {
    setHasHover(true);
  };

  const handleMouseLeave = () => {
    setHasHover(false);
  };

  const showAllOrLessDocuments = (action: ButtonType) => {
    versionHistoryResponse.data?.dtos.content?.[0].children.forEach((dms) => {
      dms.children.forEach((document) => {
        const tableRow = global.document.querySelector(`[data-row-key='${document.id}']`);
        const showAllButtons = global.document.getElementsByClassName('lea-table-expand-button-reduced');
        const showLessButtons = global.document.getElementsByClassName('lea-table-expand-button-expanded');
        if (tableRow && document.entityType === EntityType.SINGLE_DOCUMENT) {
          if (action === ButtonType.SHOW_ALL) {
            Array.from(showAllButtons).forEach((button) => {
              const buttonElement = button as HTMLButtonElement;
              buttonElement.click();
            });
          } else {
            Array.from(showLessButtons).forEach((button) => {
              const buttonElement = button as HTMLButtonElement;
              buttonElement.click();
            });
          }
        }
      });
    });
  };

  useEffect(() => {
    if (updateTable) {
      getVersionHistory();
      setUpdateTable(false);
    }
  }, [updateTable]);

  return (
    <>
      <div className={`lea-page-homepage ${isStandalone ? 'standalone' : 'embedded'}`}>
        <div className={`lea-page-homepage-main ${isStandalone ? 'standalone' : 'embedded'}`}>
          <Header className="header-component site-layout-background" id="lea-header">
            <HeaderComponent ctrl={headerController} />
          </Header>{' '}
          <Layout className="main-content-area has-drawer">
            <div className="ant-layout-content" style={{ padding: '0px' }}>
              <Main>
                <>
                  <div className="main-header">
                    <Text className="main-header-title">
                      {t('lea.startseite.versionHistory.title', { title: breadcrumbText })}
                    </Text>
                  </div>
                  <div className="main-content">
                    <div className="version-history-area">
                      {!isLoading ? (
                        <TableComponent
                          id="lea-user-version-history-table"
                          tabKey="versionHistory"
                          bePagination
                          expandable={versionHistoryTableData.expandable}
                          showAllorLessDocuments={{
                            method: showAllOrLessDocuments,
                            buttonType: buttonType,
                            setButtonType: setButtonType,
                            icon: <EyeIconCurrentColor />,
                          }}
                          columns={versionHistoryTableData.columns}
                          content={versionHistoryTableData.content}
                          filteredColumns={versionHistoryTableData.filteredColumns}
                          filterRowsMethod={getVersionHistoryFilteredRows}
                          prepareFilterButtonMethod={getVersionHistoryFilterButton}
                          //                sorterOptions={versionHistoryTableData.sorterOptions}
                          expandableCondition={versionHistoryTableData.expandableCondition}
                          customDefaultSortIndex={1}
                          itemsPerPage={5}
                          expandRows={true}
                        />
                      ) : (
                        <div className="loading-wrapper">
                          <Loading></Loading>
                        </div>
                      )}
                      <Button
                        className="back-to-homepage-button"
                        type="link"
                        onMouseOver={handleMouseOver}
                        onMouseLeave={handleMouseLeave}
                        icon={<OutlinedDirectionalLeftIcon hasHover={hasHover} />}
                        aria-label={t('lea.startseite.versionHistory.backToHomepage')}
                        onClick={handleClick}
                      >
                        {t('lea.startseite.versionHistory.backToHomepage')}
                      </Button>
                      <Readme />
                    </div>
                  </div>
                </>
              </Main>
            </div>
          </Layout>
          <DocumentWizards
            compoundDocumentInfo={compoundDocumentInfo}
            documentInfo={documentInfo}
            userSettingsRef={userSettingsRef}
            setUpdateTable={setUpdateTable}
          />
        </div>
      </div>
    </>
  );
};

export default VersionHistoryPage;
