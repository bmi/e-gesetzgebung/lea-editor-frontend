// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { InitialDocumentsTableActionState } from './DocumentsTableActionState';
import { CompoundDocumentInfo, DocumentInfo } from './HomePage/HomePage';

const documentsTableActionSlice = createSlice({
  name: 'DocumentsTableAction',
  initialState: InitialDocumentsTableActionState,
  reducers: {
    setCompoundDocumentInfo(state, action: PayloadAction<CompoundDocumentInfo>) {
      state.compoundDocumentInfo = action.payload;
    },
    setDocumentInfo(state, action: PayloadAction<DocumentInfo>) {
      state.documentInfo = action.payload;
    },
  },
});

export const { setCompoundDocumentInfo, setDocumentInfo } = documentsTableActionSlice.actions;
export const DocumentsTableActionReducer = documentsTableActionSlice.reducer;
