// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { CompoundDocumentService } from '../../../api';

interface DrucksachePageParams {
  compoundDocumentId: string;
}

const DrucksachePage: FunctionComponent = () => {
  const { compoundDocumentId } = useParams<DrucksachePageParams>();
  const [drucksacheResponse, fetchDrucksache] = CompoundDocumentService.getDrucksache(compoundDocumentId);

  useEffect(() => {
    fetchDrucksache();
  }, []);

  useEffect(() => {
    const pdfFile = drucksacheResponse.data;
    if (pdfFile) {
      const pdfUrl = window.URL.createObjectURL(pdfFile);
      window.open(pdfUrl, '_self');
    }
  }, [drucksacheResponse]);

  return <></>;
};

export default DrucksachePage;
