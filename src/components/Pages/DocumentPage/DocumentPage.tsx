// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { Editor } from '../../../lea-editor';
import { useFirstDocumentDispatch, useSynopsisDocumentsDispatch } from '../../../api';
import {
  useAuthorialNotePageChange,
  useDynAenderungsvergleichDispatch,
  useEditorDocumentsInfoChange,
  useTableOfContentsDispatch,
} from '../../../lea-editor/plugin-core';
import { useCommentPageChange } from '../../../lea-editor/plugin-core/editor-store/workspace/comment-page';
import { useContextMenuChange } from '../../../lea-editor/plugin-core/editor-store/workspace/contextMenu';
import { useHomePageTabCookie, useHomePageTabState } from '../HomePage';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
} from '../../../lea-editor/plugin-core/editor-store/editor-options';
import { useSearchReplaceDispatch } from '../../../lea-editor/plugin-core/editor-store/workspace/search-replace';
import { SearchForTypes } from '../../../lea-editor/plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';
import { SynopsisType } from '../../../constants';
import { useAppInfoDispatch } from '../../App';

export interface DocumentPageParams {
  documentId: string;
}

export type ParallelViewState = {
  fromParallelViewWizard: boolean;
};

/**
 * The DocumentPage asks the DocumentService for the document to be displayed using the URL parameters (documentId).
 * If successful, then the corresponding editor is loaded otherwise
 * the user is redirected back to the start page together with the corresponding error message.
 */
const DocumentPage: FunctionComponent = () => {
  /**
   * It is not necessary to check the documentId for "null",
   * otherwise the router would not forward to this page.
   */
  const location = useLocation();
  const { documentId } = useParams<DocumentPageParams>();
  const queryParams = new URLSearchParams(location.search);
  const synopsisIdsString: string | null = queryParams.get('synopsisDocuments');
  const parsedIds: string[] = synopsisIdsString ? (JSON.parse(decodeURIComponent(synopsisIdsString)) as string[]) : [];
  const firstDocument = useFirstDocumentDispatch();
  const synopsisDocument = useSynopsisDocumentsDispatch();
  const homePageTab = useHomePageTabState();
  const { closeCommentPage } = useCommentPageChange();
  const { closeTableOfContents, openTableOfContents } = useTableOfContentsDispatch();
  const { closeAuthorialNotePage } = useAuthorialNotePageChange();
  const { closeContextMenu } = useContextMenuChange();
  const closeCommentOptions = useEditorOptionsVisibleChange(EditorOptions.COMMENT);
  const closeSearchOptions = useEditorOptionsVisibleChange(EditorOptions.SEARCH_REPLACE);
  const { setHomePageTabFromCookie, setHomePageTabCookie } = useHomePageTabCookie();
  const { setSearchCount, setSearchText, setSelectedSearchPosition, setSearchType } = useSearchReplaceDispatch();
  const { removeDocuments } = useEditorDocumentsInfoChange();
  const { disableDynAenderungsvergleich } = useDynAenderungsvergleichDispatch();
  const { setSynopsisType } = useAppInfoDispatch();

  const reloadHandler = () => {
    setHomePageTabCookie(homePageTab, documentId);
  };

  useEffect(() => {
    window.addEventListener('beforeunload', reloadHandler);
    return () => {
      window.removeEventListener('beforeunload', reloadHandler);
    };
  }, [homePageTab]);
  /**
   * Since we are a SPA, the documentId must be included in the useEffect as a dependency.
   * If the user changes the documentId in the browser after the initial load,
   * intentionally or unintentionally, then it would have no effect on the application.
   * The disadvantage here would be, if this would happen unintentionally and he then shares the URL,
   * the others would always get an error although the actual document exists.
   */
  useEffect(() => {
    setHomePageTabFromCookie(documentId);
    if (!(location?.state as ParallelViewState)?.fromParallelViewWizard) {
      firstDocument.fetchDocument(documentId);
    }
    openTableOfContents();

    if (parsedIds.length > 0) {
      setSynopsisType(SynopsisType.Synopsis);
      synopsisDocument.fetchDocuments(parsedIds);
      return;
    }
    return () => {
      firstDocument.removeDocument();
      removeDocuments();
      closeCommentPage();
      closeAuthorialNotePage();
      closeTableOfContents();
      closeContextMenu();
      closeCommentOptions.closeOptions();
      setSearchCount(0);
      setSearchText('');
      setSearchType(SearchForTypes.Text);
      setSelectedSearchPosition(0);
      setSynopsisType(SynopsisType.None);
      closeSearchOptions.closeOptions();
      disableDynAenderungsvergleich();
    };
  }, [documentId]);

  return <Editor />;
};

export default DocumentPage;
