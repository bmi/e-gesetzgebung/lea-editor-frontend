// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { InfoComponent } from '@plateg/theme';
import { useTranslation } from 'react-i18next';

const HomePageHelpComponent = (): ReactElement => {
  const { t } = useTranslation();

  return (
    <div
      className="editor-homePage-help-content"
      dangerouslySetInnerHTML={{
        __html: t('lea.startseite.help.section1'),
      }}
    />
  );
};

export const HomePageHelp = (): ReactElement => {
  const { t } = useTranslation();
  return (
    <InfoComponent
      getContainer=".lea"
      isContactPerson={false}
      key="editor-homePage-help"
      title={t('lea.startseite.help.drawerTitle')}
      buttonText={t('lea.startseite.help.linkHelp')}
    >
      <HomePageHelpComponent />
    </InfoComponent>
  );
};
