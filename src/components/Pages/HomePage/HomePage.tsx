// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useContext, useEffect, useRef, useState } from 'react';
import { Button, Tabs, Typography, Layout, TabsProps } from 'antd';
import { useTranslation } from 'react-i18next';

import './HomePage.less';
import { DocumentWizards, Footer, Main, Readme } from '../../Elements';
import { AbstimmungenDocumentsTable, MyDocumentsTable } from '../../Elements/HomepageDocumentsTables/Tabs';
import {
  HeaderComponent,
  HeaderController,
  BreadcrumbComponent,
  TitleWrapperComponent,
  displayMessage,
} from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';
import {
  EntityType,
  DocumentType,
  useFirstDocumentDispatch,
  useSynopsisDocumentsDispatch,
  CompoundDocumentType,
  UserDTO,
  DocumentStateType,
} from '../../../api';
import { useHomePageTabDispatch, useHomePageTabState } from './useHomePage';
import { HomePageTabs } from './HomePageState';
import { HomePageHelp } from './HomePageHelp';
import { StandaloneContext } from '../../App/App';
import { useDocumentsTableActionDispatch, useDocumentsTableActionState } from '../useDocumentsTableAction';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../api/User/UserSettings';
import { UserSettingsService } from '../../../api/User/UserService';

const { Text } = Typography;

export interface DocumentInfo {
  entity: EntityType;
  id: string;
  title: string;
  regulatoryProposalId?: string;
}

export type DocumentList = {
  id: string;
  type: DocumentType;
  title: string;
  created: string;
}[];

export interface CompoundDocumentInfo {
  id: string;
  rvId?: string;
  version?: string;
  title?: string;
  type?: CompoundDocumentType;
  documents?: DocumentList;
  createdBy?: UserDTO;
  compoundDocumentState?: DocumentStateType;
}

const HomePage: FunctionComponent = () => {
  const { t } = useTranslation();
  const isStandalone = useContext(StandaloneContext);
  const { Header } = Layout;

  const firstDocument = useFirstDocumentDispatch();
  const synopsisDocument = useSynopsisDocumentsDispatch();
  const userSettingsRef = useRef<PinnedCompoundDocumentsSettings | null>(null);

  const homePageTab = useHomePageTabState();
  const { changeHomePageTab } = useHomePageTabDispatch();

  const { onCreateNewCompoundDocument } = useDocumentsTableActionDispatch();
  const { compoundDocumentInfo, documentInfo } = useDocumentsTableActionState();

  const [updateTable, setUpdateTable] = useState(false);
  const [userSettings, setUserSettings] = useState<UserSettingsDTO | null>(null);

  const headerController = GlobalDI.getOrRegister('editorHeaderController', () => new HeaderController());
  const [breadcrumbTabText, setBreadcrumbTabText] = useState('');
  const [userSettingsResponse, fetchUserSettings] = UserSettingsService.getUserSettings();
  const [userSettingsUpdateResponse, updateUserSettingsResponse] = UserSettingsService.updateUserSettings(userSettings);
  const isInitialUpdate = useRef(true);

  const setBreadcrumb = () => {
    const mainText = (
      <span key="editor-home" style={{ fontWeight: 'bold' }}>
        {t('lea.startseite.breadcrumb.projectName')} - {breadcrumbTabText}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[mainText]} />],
      headerCenter: [],
      headerRight: [],
      headerLast: [<HomePageHelp key="editor-header-help-link" />],
    });
  };

  const initBreadcrumbText = () => {
    switch (homePageTab) {
      case HomePageTabs.MEINE_DOKUMENTE:
        setBreadcrumbTabText(t('lea.startseite.homepageDocumentsTables.userDocumentsTable.title'));
        break;
      case HomePageTabs.ABSTIMMUNGEN:
        setBreadcrumbTabText(t('lea.startseite.homepageDocumentsTables.abstimmungenTable.title'));
        break;
      case HomePageTabs.ARCHIV:
        setBreadcrumbTabText(t('lea.startseite.homepageDocumentsTables.archivTable.title'));
        break;
      case HomePageTabs.ENTWUERFE:
        setBreadcrumbTabText(t('lea.startseite.homepageDocumentsTables.entwuerfeTable.title'));
        break;
      default:
        setBreadcrumbTabText(t('lea.startseite.homepageDocumentsTables.userDocumentsTable.title'));
    }
  };

  useEffect(() => {
    initBreadcrumbText();
    setBreadcrumb();
    changeHomePageTab(homePageTab ?? HomePageTabs.MEINE_DOKUMENTE);
    firstDocument.removeDocument();
    synopsisDocument.removeDocument();
    fetchUserSettings();
  }, []);

  useEffect(() => {
    if (userSettingsResponse.isLoading) return;
    if (userSettingsResponse.hasError) {
      displayMessage('', 'error');
    } else if (userSettingsResponse.data) {
      const initialUserSettings = userSettingsResponse.data;
      if (initialUserSettings.pinnedDokumentenmappen === undefined) {
        setUserSettings({ pinnedDokumentenmappen: [] });
        userSettingsRef.current = { pinnedDokumentenmappen: [], lastChange: undefined };
      } else {
        setUserSettings(initialUserSettings);
        userSettingsRef.current = {
          pinnedDokumentenmappen: initialUserSettings.pinnedDokumentenmappen,
          lastChange: undefined,
        };
      }
    }
  }, [userSettingsResponse]);

  useEffect(() => {
    if (userSettings) {
      updateUserSettingsResponse();
    }
  }, [userSettings]);

  useEffect(() => {
    if (userSettingsResponse.isLoading) return;
    if (isInitialUpdate.current) {
      isInitialUpdate.current = false;
      return;
    }
    if (userSettingsResponse.hasError) {
      displayMessage(t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.pinError'), 'error');
    } else {
      if (userSettingsRef.current?.lastChange === 'pinned') {
        displayMessage(t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.pinSuccess'), 'success');
      } else if (userSettingsRef.current?.lastChange === 'unpinned') {
        displayMessage(
          t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.releaseSuccess'),
          'success',
        );
      }
      setUpdateTable(true);
    }
  }, [userSettingsUpdateResponse]);

  const onTabChange = (
    activeKey: string,
    event: React.KeyboardEvent<Element> | React.MouseEvent<Element, MouseEvent>,
  ) => {
    switch (activeKey) {
      case '1': {
        changeHomePageTab(HomePageTabs.MEINE_DOKUMENTE);
        break;
      }
      case '2': {
        changeHomePageTab(HomePageTabs.ABSTIMMUNGEN);
        break;
      }
      case '3': {
        changeHomePageTab(HomePageTabs.ENTWUERFE);
        break;
      }
      case '4': {
        changeHomePageTab(HomePageTabs.ARCHIV);
        break;
      }
      default: {
        changeHomePageTab(HomePageTabs.MEINE_DOKUMENTE);
      }
    }
    setBreadcrumbTabText(event.currentTarget.textContent ?? '');
  };

  const getHomePageTabFromType = (homePageTab: HomePageTabs | null): string => {
    switch (homePageTab) {
      case HomePageTabs.MEINE_DOKUMENTE: {
        return '1';
      }
      case HomePageTabs.ABSTIMMUNGEN: {
        return '2';
      }
      case HomePageTabs.ENTWUERFE: {
        return '3';
      }
      case HomePageTabs.ARCHIV: {
        return '4';
      }
      default: {
        return '1';
      }
    }
  };

  const tabItems: TabsProps['items'] = [
    {
      className: 'user-documents-tab',
      label: t('lea.startseite.homepageDocumentsTables.userDocumentsTable.title'),
      key: '1',
      children: (
        <MyDocumentsTable
          userSettingsRef={userSettingsRef}
          shouldUpdateTable={updateTable}
          setShouldUpdateTable={setUpdateTable}
          setUserSettings={setUserSettings}
        />
      ),
    },
    {
      className: 'abstimmungen-documents-tab',
      label: t('lea.startseite.homepageDocumentsTables.abstimmungenTable.title'),
      key: '2',
      children: (
        <AbstimmungenDocumentsTable
          userSettingsRef={userSettingsRef}
          shouldUpdateTable={updateTable}
          setShouldUpdateTable={setUpdateTable}
          setUserSettings={setUserSettings}
        />
      ),
    },
    {
      label: t('lea.startseite.homepageDocumentsTables.entwuerfeTable.title'),
      key: '3',
      disabled: true,
    },
    {
      label: t('lea.startseite.homepageDocumentsTables.archivTable.title'),
      key: '4',
      disabled: true,
    },
  ];

  useEffect(() => {
    setBreadcrumb();
  }, [breadcrumbTabText]);

  return (
    <div className={`lea-page-homepage ${isStandalone ? 'standalone' : 'embedded'}`}>
      <div className={`lea-page-homepage-main ${isStandalone ? 'standalone' : 'embedded'}`}>
        <Header className="header-component site-layout-background" id="lea-header">
          <HeaderComponent ctrl={headerController} />
        </Header>
        <Layout className="main-content-area has-drawer">
          <div className="ant-layout-content" style={{ padding: '0px' }}>
            <Main>
              <>
                <TitleWrapperComponent>
                  <div className="heading-holder">
                    <div className="main-header">
                      <Text className="main-header-title">{t('lea.startseite.title')}</Text>
                      <div className="main-header-buttons">
                        <Button
                          id="lea-create-new-compoundDocument-button"
                          type="primary"
                          size="large"
                          onClick={onCreateNewCompoundDocument}
                        >
                          {t('lea.startseite.buttons.createNewCompoundDocumentBtnText')}
                        </Button>
                      </div>
                    </div>
                  </div>
                </TitleWrapperComponent>
                <div className="main-content">
                  <Tabs
                    items={tabItems}
                    defaultActiveKey={getHomePageTabFromType(homePageTab)}
                    className="my-votes-tabs standard-tabs"
                    onTabClick={(activeKey, e) => onTabChange(activeKey, e)}
                  />
                  <Readme />
                </div>
              </>
            </Main>
          </div>
        </Layout>
      </div>
      {isStandalone && <Footer />}
      <DocumentWizards
        compoundDocumentInfo={compoundDocumentInfo}
        documentInfo={documentInfo}
        userSettingsRef={userSettingsRef}
        setUpdateTable={setUpdateTable}
      />
    </div>
  );
};

export default HomePage;
