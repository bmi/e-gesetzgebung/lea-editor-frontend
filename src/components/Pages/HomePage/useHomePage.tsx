// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../Store';
import { changeHomePageTab } from './HomePageSlice';
import { HomePageTabs } from './HomePageState';

type UseWizardVisibleDispatch = {
  changeHomePageTab: (homePageTab: HomePageTabs | null) => void;
};

type UseHomePageTabCookie = {
  setHomePageTabCookie: (homePageTab: HomePageTabs | null, id: string) => void;
  setHomePageTabFromCookie: (id: string) => void;
};

export const useHomePageTabState = (): HomePageTabs | null =>
  useAppSelector((state) => state.editorStatic.homePage.homePageTab);

export const useHomePageTabDispatch = (): UseWizardVisibleDispatch => {
  const dispatch = useAppDispatch();

  const changeHomePageTabCallback = useCallback((homePageTab: HomePageTabs | null) => {
    dispatch(changeHomePageTab(homePageTab));
  }, []);

  return {
    changeHomePageTab: changeHomePageTabCallback,
  };
};

export const useHomePageTabCookie = (): UseHomePageTabCookie => {
  const { changeHomePageTab } = useHomePageTabDispatch();
  const HomePageTabCookie_Name = 'editorHomePageTab';
  const setHomePageTabCookieCallback = useCallback((homePageTab: HomePageTabs | null, id: string) => {
    if (!homePageTab) return;
    const now = new Date();
    const time = now.getTime();
    const expireTime = time + 1000 * 10;
    document.cookie = `${HomePageTabCookie_Name}=${homePageTab || ''}/${id};expires=${new Date(
      expireTime,
    ).toUTCString()}`;
  }, []);

  const setHomePageTabFromCookieCallback = useCallback((id: string) => {
    const cookies = document.cookie.split(';');
    cookies.forEach((cookie) => {
      if (cookie.startsWith(HomePageTabCookie_Name)) {
        const homePageTabCookies = cookie.replace(`${HomePageTabCookie_Name}=`, '').split('/');
        if (Object.values<string>(HomePageTabs).includes(homePageTabCookies[0]) && id === homePageTabCookies[1]) {
          changeHomePageTab(homePageTabCookies[0] as HomePageTabs);
          return;
        }
      }
    });
  }, []);

  return {
    setHomePageTabCookie: setHomePageTabCookieCallback,
    setHomePageTabFromCookie: setHomePageTabFromCookieCallback,
  };
};
