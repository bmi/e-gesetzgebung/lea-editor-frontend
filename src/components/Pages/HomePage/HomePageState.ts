// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

enum HomePageTabs {
  MEINE_DOKUMENTE = 'MEINE_DOKUMENTE',
  ABSTIMMUNGEN = 'ABSTIMMUNGEN',
  ENTWUERFE = 'ENTWUERFE',
  ARCHIV = 'ARCHIV',
  VERSIONSHISTORIE = 'VERSIONSHISTORIE',
}
type HomePageState = {
  homePageTab: HomePageTabs | null;
};

const InitialHomePageState: HomePageState = {
  homePageTab: null,
};

export { InitialHomePageState, HomePageState, HomePageTabs };
