// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { HomePageTabs, InitialHomePageState } from './HomePageState';

const homePageSlice = createSlice({
  name: 'homePage',
  initialState: InitialHomePageState,
  reducers: {
    changeHomePageTab(state, action: PayloadAction<HomePageTabs | null>) {
      state.homePageTab = action.payload;
    },
  },
});

export const { changeHomePageTab } = homePageSlice.actions;
export const HomePageReducer = homePageSlice.reducer;
