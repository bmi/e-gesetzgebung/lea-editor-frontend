// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export { HashRouter as Router, Switch, Route, Redirect, useRouteMatch } from 'react-router-dom';
//
export { default as ProtectedRoute } from './ProtectedRoute';
