// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect } from 'react';
import { RouteProps } from 'react-router-dom';
//
import { Route } from '.';
import { useAuth, useIsAuthenticated } from '../Auth';

const ProtectedRoute = (props: RouteProps): ReactElement => {
  const isAuthenticated = useIsAuthenticated();
  const { checkAuth } = useAuth();

  useEffect(() => {
    if (!isAuthenticated) {
      checkAuth();
    }
  }, []);

  return isAuthenticated ? <Route {...props} /> : <></>;
};

export default ProtectedRoute;
