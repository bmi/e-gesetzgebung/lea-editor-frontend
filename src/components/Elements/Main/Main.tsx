// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useContext } from 'react';
//
import './Main.less';
import { StandaloneContext } from '../../App/App';

type IProps = {
  children: React.ReactElement;
};
const Main: FunctionComponent<IProps> = (props: IProps) => {
  const isStandalone = useContext(StandaloneContext);
  return <main className={`lea-page-component-main ${isStandalone ? 'standalone' : ''}`} {...props} />;
};
export default Main;
