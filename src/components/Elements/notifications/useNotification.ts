// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useContext } from 'react';
import { INotificationContext, NotificationContext } from './NotificationProvider';

export function useNotification(): INotificationContext {
  return useContext(NotificationContext);
}
