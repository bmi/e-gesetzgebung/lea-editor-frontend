// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Modal } from 'antd';

import { useNotification } from './useNotification';

import './LeaNotification.scss';

function LeaNotification(): ReactElement {
  const { t } = useTranslation();
  const { notification, removeNotification } = useNotification();

  const handleConfirmClick = () => {
    removeNotification();
  };

  const handleCancelClick = () => {
    removeNotification();
  };

  const isVisible = useMemo(() => !!notification, [notification]);

  return (
    <Modal
      mask={false}
      title={notification ? notification.title : ''}
      open={isVisible}
      onCancel={handleCancelClick}
      footer={[
        <Button key="1" onClick={handleConfirmClick}>
          {t('lea.topToolbar.readOnlyInfo.dialog.button')}
        </Button>,
      ]}
    >
      {<p>{notification ? notification.message : ''}</p>}
    </Modal>
  );
}

export default LeaNotification;
