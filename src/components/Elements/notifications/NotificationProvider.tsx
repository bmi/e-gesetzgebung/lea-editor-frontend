// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, PropsWithChildren, ReactElement, useMemo } from 'react';

export interface INotificationContext {
  notification: { title: string; message: string } | null;
  addNotification: (title: string, message: string) => void;
  removeNotification: () => void;
}

export const NotificationContext = React.createContext<INotificationContext>({
  notification: null,
  addNotification: () => {},
  removeNotification: () => {},
});

function NotificationProvider({ children }: PropsWithChildren<any>): ReactElement {
  const [notification, setNotification] = useState<{ title: string; message: string } | null>(null);

  const removeNotification = () => setNotification(null);

  const addNotification = (title: string, message: string) =>
    setNotification({ title, message } as React.SetStateAction<{
      title: string;
      message: string;
    } | null>);

  const contextValue = useMemo(() => {
    return {
      notification,
      addNotification: (title: string, message: string) => addNotification(title, message),
      removeNotification: () => removeNotification(),
    };
  }, [notification]);

  return <NotificationContext.Provider value={contextValue}>{children}</NotificationContext.Provider>;
}

export default NotificationProvider;
