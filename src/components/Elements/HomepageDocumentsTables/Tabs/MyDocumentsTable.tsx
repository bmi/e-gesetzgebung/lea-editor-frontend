// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../TableStyles.less';

import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { displayMessage, Loading, TableComponent, TableComponentProps } from '@plateg/theme';

import { PaginationDTO, RegulatoryProposalService } from '../../../../api';
import { createDocumentsTableData, getFilterButton, getFilteredRows, HomePageDTO, HomepageTabs } from '..';
import { addExpandAndCollapseButtons } from '../ShowAllOrLessButton/utils';
import '../ShowAllOrLessButton/DocumentsTableStyle.scss';
import {
  setPaginationFilterInfo,
  setPaginationResult,
  setRequestCurrentPage,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { PaginierungDTOSortByEnum, PaginierungDTOSortDirectionEnum } from '@plateg/rest-api';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../../api/User/UserSettings';

interface IUserDocumentsTableProps {
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  shouldUpdateTable: boolean;
  setShouldUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
  setUserSettings: React.Dispatch<React.SetStateAction<UserSettingsDTO | null>>;
}

type TableCompPropsHomepageMeineDokuemnteTableWithoutId = Omit<TableComponentProps<HomePageDTO>, 'id'>;

const MyDocumentsTable: FunctionComponent<IUserDocumentsTableProps> = ({
  userSettingsRef,
  shouldUpdateTable,
  setShouldUpdateTable,
  setUserSettings,
}: IUserDocumentsTableProps) => {
  const { t } = useTranslation();
  const [myDocumentsTableData, setMyDocumentsTableData] = useState<TableCompPropsHomepageMeineDokuemnteTableWithoutId>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const homepageTabKey = 'meineDokumente';
  const [pagination, setPagination] = useState<PaginationDTO>();
  const [isLoading, setIsLoading] = useState(true);
  const [homepageData, fetchHomepageData] = RegulatoryProposalService.getHomepageMeineDokumente(pagination);
  const expandedCompoundDocumentIdsRef = useRef(new Set<string>());
  const dispatch = useAppDispatch();
  const pagDataRequest = useAppSelector((state) => state.tablePagination.tabs[homepageTabKey]).request;

  useEffect(() => {
    setMyDocumentsTableData(
      createDocumentsTableData({
        content: [],
        t,
        expandedCompoundDocumentIdsRef,
        homepageTab: HomepageTabs.MEINE_DOKUMENTE,
        userSettingsRef,
        setUserSettings,
        setShouldUpdateTable,
      }),
    );
  }, []);

  useEffect(() => {
    if (
      pagination?.pageNumber !== pagDataRequest.currentPage ||
      pagination?.sortBy !== pagDataRequest?.columnKey ||
      pagination?.sortDirection !== pagDataRequest.sortOrder
    ) {
      setPagination({
        pageNumber: pagDataRequest.currentPage ?? 0,
        pageSize: 1,
        sortBy: pagDataRequest.columnKey ?? PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortDirection: pagDataRequest.sortOrder ?? PaginierungDTOSortDirectionEnum.Asc,
      });
    }
  }, [pagDataRequest]);

  useEffect(() => {
    if (pagination !== undefined) {
      fetchHomepageData();
      setIsLoading(true);
    }
  }, [pagination]);

  useEffect(() => {
    if (homepageData.isLoading) return;
    if (homepageData.hasError) {
      displayMessage(t('lea.startseite.homepageDocumentsTables.userDocumentsTable.error'), 'error');
      setIsLoading(false);
      return;
    }
    if (!homepageData.data) return;
    setIsLoading(false);
    const { content, totalElements, number } = homepageData.data.dtos;
    const { filterNames, vorhabenartFilter, allContentEmpty } = homepageData.data;
    dispatch(
      setPaginationResult({
        tabKey: homepageTabKey,
        totalItems: totalElements,
        currentPage: number,
        allContentEmpty,
      }),
    );
    dispatch(setRequestCurrentPage({ tabKey: homepageTabKey, currentPage: number ? number + 1 : 1 }));
    dispatch(setPaginationFilterInfo({ tabKey: homepageTabKey, vorhabenartFilter, filterNames }));
    setMyDocumentsTableData({
      ...myDocumentsTableData,
      content: addExpandAndCollapseButtons(JSON.parse(JSON.stringify(content ?? []))),
    });
  }, [homepageData]);

  useEffect(() => {
    if (shouldUpdateTable) {
      setPagination({
        pageNumber: pagDataRequest.currentPage ?? 0,
        pageSize: 1,
        sortBy: pagDataRequest.columnKey ?? PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortDirection: pagDataRequest.sortOrder ?? PaginierungDTOSortDirectionEnum.Asc,
      });
      fetchHomepageData();
      setIsLoading(true);
      setShouldUpdateTable(false);
    }
  }, [shouldUpdateTable]);

  if (!homepageData.data) return <></>;
  return !isLoading ? (
    <TableComponent
      id="lea-user-documents-table"
      tabKey={homepageTabKey}
      bePagination
      expandable={myDocumentsTableData.expandable}
      columns={myDocumentsTableData.columns}
      content={myDocumentsTableData.content}
      filteredColumns={myDocumentsTableData.filteredColumns}
      filterRowsMethod={getFilteredRows}
      prepareFilterButtonMethod={getFilterButton}
      //     sorterOptions={myDocumentsTableData.sorterOptions}
      expandableCondition={myDocumentsTableData.expandableCondition}
      customDefaultSortIndex={1}
      itemsPerPage={1}
      expandRows={true}
    />
  ) : (
    <div className="loading-wrapper">
      <Loading></Loading>
    </div>
  );
};

export default MyDocumentsTable;
