// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../TableStyles.less';

import { displayMessage, Loading, TableComponent, TableComponentProps } from '@plateg/theme';
import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { HomepageRegulatoryProposalDTO, RegulatoryProposalService } from '../../../../api';
import { createDocumentsTableData, getFilterButton, getFilteredRows, HomePageDTO, HomepageTabs } from '..';
import { addExpandAndCollapseButtons } from '../ShowAllOrLessButton/utils';
import '../ShowAllOrLessButton/DocumentsTableStyle.scss';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../../api/User/UserSettings';

interface IUserDocumentsTableProps {
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  shouldUpdateTable: boolean;
  setShouldUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
  setUserSettings: React.Dispatch<React.SetStateAction<UserSettingsDTO | null>>;
}

const AbstimmungenDocumentsTable: FunctionComponent<IUserDocumentsTableProps> = ({
  userSettingsRef,
  shouldUpdateTable,
  setShouldUpdateTable,
  setUserSettings,
}: IUserDocumentsTableProps) => {
  const { t } = useTranslation();
  const [abstimmungenDocumentsTableData, setAbstimmungenDocumentsTableData] = useState<
    TableComponentProps<HomePageDTO>
  >({
    id: '1',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const [isLoading, setIsLoading] = useState(true);
  const [abstimmungenData, fetchAbstimmungenData] = RegulatoryProposalService.getHomepageDokumenteAusAbstimmungen();
  const expandedCompoundDocumentIdsRef = useRef(new Set<string>());

  useEffect(() => {
    fetchAbstimmungenData();
    setAbstimmungenDocumentsTableData(
      createDocumentsTableData({
        content: [],
        t,
        expandedCompoundDocumentIdsRef,
        homepageTab: HomepageTabs.ABSTIMMUNGEN,
        userSettingsRef,
        setUserSettings,
        setShouldUpdateTable,
      }),
    );
  }, []);

  useEffect(() => {
    if (abstimmungenData.isLoading) return;
    if (abstimmungenData.hasError) {
      displayMessage(t('lea.startseite.homepageDocumentsTables.abstimmungenTable.error'), 'error');
      setIsLoading(false);
      return;
    }
    if (!abstimmungenData.data) return;
    setIsLoading(false);
    setAbstimmungenDocumentsTableData({
      ...abstimmungenDocumentsTableData,
      content: addExpandAndCollapseButtons(
        JSON.parse(JSON.stringify(abstimmungenData.data)),
      ) as HomepageRegulatoryProposalDTO[],
    });
  }, [abstimmungenData]);

  useEffect(() => {
    if (shouldUpdateTable) {
      fetchAbstimmungenData();
      setIsLoading(true);
      setShouldUpdateTable(false);
    }
  }, [shouldUpdateTable]);

  return !isLoading ? (
    <>
      <TableComponent
        id="lea-user-abstimmungen-table"
        expandable={true}
        columns={abstimmungenDocumentsTableData.columns}
        content={abstimmungenDocumentsTableData.content}
        filteredColumns={abstimmungenDocumentsTableData.filteredColumns}
        filterRowsMethod={getFilteredRows}
        prepareFilterButtonMethod={getFilterButton}
        //     sorterOptions={abstimmungenDocumentsTableData.sorterOptions}
        customDefaultSortIndex={1}
        expandableCondition={abstimmungenDocumentsTableData.expandableCondition}
      />
    </>
  ) : (
    <div className="loading-wrapper">
      <Loading></Loading>
    </div>
  );
};

export default AbstimmungenDocumentsTable;
