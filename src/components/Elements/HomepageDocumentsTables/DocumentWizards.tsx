// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { CompoundDocumentInfo, DocumentInfo } from '../../Pages/HomePage/HomePage';
import {
  CreateNewDocumentWizard,
  ImportDocumentWizard,
  CreateNewCompoundDocumentWizard,
  ExportCompoundDocumentWizard,
  RenameDocumentWizard,
  NewVersionWizard,
  SetDocumentReadRightsWizard,
  SetDocumentWriteRightsWizard,
  SynopsisWizard,
  EgfaWizard,
  ExportPdfWizard,
} from '../../Wizard';
import { PinnedCompoundDocumentsSettings } from '../../../api/User/UserSettings';
import { AssignBestandsrechtWizard } from '../../Wizard/AssignBestandsrechtWizard';

interface DocumentWizardProps {
  compoundDocumentInfo: CompoundDocumentInfo;
  documentInfo: DocumentInfo;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  setUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

const DocumentWizards = (props: DocumentWizardProps): ReactElement => {
  return (
    <>
      <CreateNewDocumentWizard compoundDocumentInfo={props.compoundDocumentInfo} updateTable={props.setUpdateTable} />
      <ImportDocumentWizard />
      <CreateNewCompoundDocumentWizard />
      <ExportCompoundDocumentWizard compoundDocumentInfo={props.compoundDocumentInfo} />
      <RenameDocumentWizard documentInfo={props.documentInfo} updateTable={props.setUpdateTable} />
      <NewVersionWizard userSettingsRef={props.userSettingsRef} updateTable={props.setUpdateTable} />
      <SetDocumentReadRightsWizard compoundDocumentInfo={props.compoundDocumentInfo} />
      <SetDocumentWriteRightsWizard
        compoundDocumentInfo={props.compoundDocumentInfo}
        updateTable={props.setUpdateTable}
      />
      <SynopsisWizard documentInfo={props.documentInfo} />
      <EgfaWizard compoundDocumentInfo={props.compoundDocumentInfo} updateTable={props.setUpdateTable} />
      <ExportPdfWizard compoundDocumentInfo={props.compoundDocumentInfo} />
      <AssignBestandsrechtWizard compoundDocumentInfo={props.compoundDocumentInfo} />
    </>
  );
};

export default DocumentWizards;
