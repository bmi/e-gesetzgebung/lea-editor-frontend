// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement } from 'react';
import { TableComponentProps, InfoComponent } from '@plateg/theme';
import { ColumnsType } from 'antd/lib/table';
import { ColumnTitle } from 'antd/lib/table/interface';
import { TFunction, t } from 'i18next';
import { ReactI18NextChild } from 'react-i18next';
import {
  ButtonType,
  EntityType,
  ExpandCollapseButton,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../api';
import {
  TypeColumn,
  VersionColumn,
  PermissionColumn,
  StatusColumn,
  ActionColumn,
  TitleColumn,
  StateColumn,
} from './columns';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../api/User/UserSettings';
import PropertiesColumn from './columns/PropertiesColumn';

export enum HomepageTabs {
  MEINE_DOKUMENTE = 'MEINE_DOKUMENTE',
  ABSTIMMUNGEN = 'ABSTIMMUNGEN',
  ENTWUERFE = 'ENTWUERFE',
  ARCHIV = 'ARCHIV',
  VERSIONSHISTORIE = 'VERSIONSHISTORIE',
}

interface DocumentsTableDataProps {
  content: Array<HomepageRegulatoryProposalDTO>;
  t: TFunction;
  expandedCompoundDocumentIdsRef: React.MutableRefObject<Set<string>>;
  setShowAllOrLessButtonType?: (expand: ButtonType) => void;
  homepageTab: HomepageTabs;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  setUserSettings: (userSettings: UserSettingsDTO | null) => void;
  setShouldUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

export type HomePageDTO =
  | HomepageRegulatoryProposalDTO
  | HomepageCompoundDocumentDTO
  | HomepageSingleDocumentDTO
  | ExpandCollapseButton;

export function createDocumentsTableData({
  content,
  t,
  expandedCompoundDocumentIdsRef,
  setShowAllOrLessButtonType,
  homepageTab,
  userSettingsRef,
  setUserSettings,
  setShouldUpdateTable,
}: DocumentsTableDataProps): TableComponentProps<HomePageDTO> {
  const columns: ColumnsType<HomePageDTO> = [
    {
      key: 'title',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.title.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => (
        <TitleColumn
          props={props}
          expandedCompoundDocumentIdsRef={expandedCompoundDocumentIdsRef}
          setShowAllOrLessButtonType={setShowAllOrLessButtonType}
        />
      ),
    },
    {
      key: 'status',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.status.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => <StateColumn {...props} />,
    },
    {
      key: 'type',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.type.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => <TypeColumn {...props} />,
    },
    {
      key: 'bearbeitetam',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.updated.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO): ReactElement => <StatusColumn {...props} />,
      defaultSortOrder: StatusColumn.defaultSortOrder,
      //      sorter: StatusColumn.sortProps,
    },
    {
      key: 'state',
      title: () => (
        <span className="accessRights">
          {t('lea.startseite.homepageDocumentsTables.tableColumns.state.title') as ReactI18NextChild}
          <InfoComponent
            getContainer=".lea"
            isContactPerson={false}
            title={'Zugriffsrechte'}
            id={`zugriffsrechte-info-${homepageTab}`}
          >
            <p>
              <strong>
                {
                  t(
                    'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt1.title',
                  ) as ReactI18NextChild
                }
              </strong>
            </p>
            <p>
              {
                t(
                  'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt1.paragraph1',
                ) as ReactI18NextChild
              }
            </p>
            <p>
              {
                t(
                  'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt1.paragraph2',
                ) as ReactI18NextChild
              }
            </p>
            <p>
              {
                t(
                  'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt1.paragraph3',
                ) as ReactI18NextChild
              }
            </p>
            <p>
              <strong>
                {
                  t(
                    'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt2.title',
                  ) as ReactI18NextChild
                }
              </strong>
            </p>
            <p>
              {
                t(
                  'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt2.paragraph1',
                ) as ReactI18NextChild
              }
            </p>
            <p>
              {
                t(
                  'lea.startseite.homepageDocumentsTables.tableColumns.state.info.abschnitt2.paragraph2',
                ) as ReactI18NextChild
              }
            </p>
          </InfoComponent>
        </span>
      ),
      render: (props: HomePageDTO) => <PermissionColumn {...props} />,
    },
    {
      key: 'properties',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.properties.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => <PropertiesColumn props={props} userSettingsRef={userSettingsRef} />,
    },
    {
      key: 'action',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.action.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => (
        <ActionColumn
          props={props}
          homepageTab={homepageTab}
          userSettingsRef={userSettingsRef}
          setUserSettings={setUserSettings}
          setShouldUpdateTable={setShouldUpdateTable}
        />
      ),
    },
  ];
  if (homepageTab === HomepageTabs.MEINE_DOKUMENTE) {
    columns.splice(2, 0, {
      key: 'version',
      title: t('lea.startseite.homepageDocumentsTables.tableColumns.version.title') as ColumnTitle<HomePageDTO>,
      render: (props: HomePageDTO) => <VersionColumn {...props} />,
    });
  }
  return {
    id: '1',
    expandable: true,
    columns,
    content,
    expandableCondition: (document: HomePageDTO) => {
      switch (document.entityType) {
        case EntityType.REGULATORY_PROPOSAL:
          if ((document as HomepageRegulatoryProposalDTO).children.length > 0) {
            return true;
          }
          return false;
        case EntityType.COMPOUND_DOCUMENT:
          if ((document as HomepageCompoundDocumentDTO).children.length > 0) {
            return true;
          }
          return false;
        default:
          return false;
      }
    },
    filteredColumns: [{ name: 'title', columnIndex: 0 }],
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: t('lea.startseite.homepageDocumentsTables.sorterOptions.status.titleAsc'),
        titleDesc: t('lea.startseite.homepageDocumentsTables.sorterOptions.status.titleDesc'),
      },
    ],
  };
}

export function getFilterButton<T extends HomePageDTO>(
  initialContent: T[],
  column: { name: string; columnIndex: number },
): { displayName: string; labelContent: string; options: Set<string> } {
  const displayName = t(`lea.startseite.homepageDocumentsTables.abstimmungenTable.filter.${column.name}`);
  let options: Set<string> = new Set();
  const labelContent = t(`lea.startseite.homepageDocumentsTables.abstimmungenTable.filter.labelContent`);

  if (column.name === 'title') {
    options = new Set(
      initialContent.map((row) => {
        const rv = row as HomepageRegulatoryProposalDTO;
        return rv.abbreviation ? `${rv.abbreviation.charAt(0)}${rv.abbreviation.slice(1)}` : '';
      }),
    );
  }
  return { displayName, labelContent, options };
}

export function getFilteredRows<T extends HomePageDTO>(
  actualContent: T[],
  filteredBy: string,
  column: { name: string; columnIndex: number },
): T[] {
  let newRows: Set<T> = new Set();
  if (column.name === 'title') {
    newRows = new Set(
      actualContent.filter((row) => {
        const regelungsvorhaben = (row as HomepageRegulatoryProposalDTO).abbreviation ?? '';
        return regelungsvorhaben === filteredBy;
      }),
    );
  }
  return Array.from(newRows);
}
