// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  ButtonType,
  DocumentType,
  EntityType,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../../api';
import { HomePageDTO } from '../CreateDocumentsTableData';

export const addExpandAndCollapseButtons = (elements: HomePageDTO[]): HomePageDTO[] => {
  elements.forEach((element) => {
    if (element.entityType === EntityType.COMPOUND_DOCUMENT) {
      const compoundDocument = element as HomepageCompoundDocumentDTO;
      const documents = compoundDocument.children;

      // Workaround, der sicherstellen soll, dass die Id der Drucksache
      // sich von der Id der Dokumentenmappe unterscheidet.
      // Der hinzugefügte string "Drucksache:" wird in TitleColumn.tsx
      // wieder rausgefiltert
      documents.forEach((document) => {
        if ((document as HomepageSingleDocumentDTO).type === DocumentType.BT_DRUCKSACHE) {
          document.id = 'Drucksache:' + document.id;
        }
      });

      documents.push({
        entityType: EntityType.BUTTON,
        type: ButtonType.SHOW_ALL,
        id: 'button-' + compoundDocument.id,
        compoundDocument: compoundDocument,
      });
    } else if (element.entityType === EntityType.REGULATORY_PROPOSAL) {
      const regulatoryProposal = element as HomepageRegulatoryProposalDTO;
      addExpandAndCollapseButtons(regulatoryProposal.children);
    }
  });

  return elements;
};
