// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useRef, useState } from 'react';
import { Button } from 'antd';
import { useTranslation } from 'react-i18next';
import { ButtonType } from '../../../../api';

type Props = {
  showAllOrLessHandler: (expand: ButtonType) => void;
  setShowAllOrLessButtonType?: (expand: ButtonType) => void;
};

const ShowAllOrLessButton = ({ showAllOrLessHandler, setShowAllOrLessButtonType }: Props) => {
  const { t } = useTranslation();
  const [buttonType, setButtonType] = useState<ButtonType>(ButtonType.SHOW_ALL);
  const showAllOrLessBtnRef = useRef<HTMLButtonElement>(null);

  const clickHandler = () => {
    showAllOrLessHandler(buttonType);
    if (buttonType === ButtonType.SHOW_ALL) {
      setButtonType(ButtonType.SHOW_LESS);
      showAllOrLessBtnRef.current?.classList.add('lea-table-expand-button-expanded');
      showAllOrLessBtnRef.current?.classList.remove('lea-table-expand-button-reduced');
      const showAllButtons = global.document.getElementsByClassName('lea-table-expand-button-reduced');
      if (setShowAllOrLessButtonType && showAllButtons.length == 0) {
        setShowAllOrLessButtonType(ButtonType.SHOW_LESS);
      }
    } else {
      setButtonType(ButtonType.SHOW_ALL);
      showAllOrLessBtnRef.current?.classList.add('lea-table-expand-button-reduced');
      showAllOrLessBtnRef.current?.classList.remove('lea-table-expand-button-expanded');
      if (setShowAllOrLessButtonType) {
        setShowAllOrLessButtonType(ButtonType.SHOW_ALL);
      }
    }
  };
  return (
    <Button
      type="text"
      className="lea-table-expand-button lea-table-expand-button-reduced"
      onClick={clickHandler}
      ref={showAllOrLessBtnRef}
    >
      {t(`lea.startseite.homepageDocumentsTables.tableColumns.title.button.${buttonType}`)}
    </Button>
  );
};

export default ShowAllOrLessButton;
