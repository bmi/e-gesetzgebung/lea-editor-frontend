// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement } from 'react';
import { EntityType, HomepageSingleDocumentDTO } from '../../../../api';
import { HomePageDTO } from '../CreateDocumentsTableData';
import { DocumentTypeHelper } from '../../../../helpers/DocumentTypeHelper';

const TypeColumn: FunctionComponent<HomePageDTO> = (props): ReactElement => {
  if (props.entityType === EntityType.SINGLE_DOCUMENT) {
    return DocumentTypeHelper((props as HomepageSingleDocumentDTO).type);
  } else if (props.entityType === EntityType.COMPOUND_DOCUMENT) {
    return <>Dokumentenmappe</>;
  }
  return <></>;
};

export default TypeColumn;
