// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement } from 'react';
import { HomePageDTO } from '../CreateDocumentsTableData';
import {
  ButtonType,
  CompoundDocumentType,
  DocumentType,
  EntityType,
  ExpandCollapseButton,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../../api';
import { Link } from 'react-router-dom';
import { useAppInfoState } from '../../../App';
import { CompoundDocumentFolderIcon, RegulatoryProposalFolderIcon } from '@lea/ui';

import './TitleColumn.less';
import ShowAllOrLessButton from '../ShowAllOrLessButton/ShowAllOrLessButton';

interface IActionColumnProps {
  props: HomePageDTO;
  expandedCompoundDocumentIdsRef: React.MutableRefObject<Set<string>>;
  setShowAllOrLessButtonType?: (expand: ButtonType) => void;
}

const TitleColumn: FunctionComponent<IActionColumnProps> = ({
  props,
  expandedCompoundDocumentIdsRef,
  setShowAllOrLessButtonType,
}): ReactElement => {
  const { basePath } = useAppInfoState();

  switch (props.entityType) {
    case EntityType.REGULATORY_PROPOSAL:
      return (
        <div
          className={`lea-table-regulatory-proposal-column ${(props as HomepageRegulatoryProposalDTO).farbe?.toLowerCase()}`}
        >
          <RegulatoryProposalFolderIcon />
          <div className="regulatoryName">{(props as HomepageRegulatoryProposalDTO).abbreviation}</div>
        </div>
      );
    case EntityType.COMPOUND_DOCUMENT:
      return (
        <div className="lea-table-compound-document-column">
          <div className="lea-table-compound-document-title">
            <CompoundDocumentFolderIcon />
            <span className="title">{(props as HomepageCompoundDocumentDTO).title}</span>
          </div>
          <span className="lea-compound-document-title">{renderType((props as HomepageCompoundDocumentDTO).type)}</span>
        </div>
      );
    case EntityType.SINGLE_DOCUMENT:
      if ((props as HomepageSingleDocumentDTO).type === DocumentType.BT_DRUCKSACHE) {
        return (
          <Link
            to={`${basePath}/druck/${(props as HomepageSingleDocumentDTO).id.replace('Drucksache:', '')}`}
            target="_blank"
          >
            {(props as HomepageSingleDocumentDTO).title}
          </Link>
        );
      }
      return (
        <div className="lea-table-document-column">
          <Link to={`${basePath}/document/${(props as HomepageSingleDocumentDTO).id || ''}`}>
            {(props as HomepageSingleDocumentDTO).title}
          </Link>
        </div>
      );
    case EntityType.BUTTON: {
      const compoundDocument = (props as ExpandCollapseButton).compoundDocument;
      const handleClick = (action: ButtonType) => {
        const compoundDocument = (props as ExpandCollapseButton).compoundDocument;
        action === ButtonType.SHOW_ALL
          ? expandedCompoundDocumentIdsRef.current.add(compoundDocument.id)
          : expandedCompoundDocumentIdsRef.current.delete(compoundDocument.id);
        compoundDocument.children.forEach((document) => {
          const tableRow = global.document.querySelector(`[data-row-key='${document.id}']`);
          if (tableRow && document.entityType === EntityType.SINGLE_DOCUMENT) {
            action === ButtonType.SHOW_ALL
              ? tableRow.classList.add('visible-row')
              : tableRow.classList.remove('visible-row');
          }
        });
      };
      if (compoundDocument.children.length > 3) {
        return (
          <ShowAllOrLessButton
            showAllOrLessHandler={handleClick}
            setShowAllOrLessButtonType={setShowAllOrLessButtonType}
          />
        );
      }
      return <></>;
    }
    default:
      return <></>;
  }
};

const renderType = (type: string | undefined): string => {
  switch (type) {
    case CompoundDocumentType.STAMMGESETZ:
      return 'Stammgesetz';
    case CompoundDocumentType.MANTELGESETZ:
      return 'Mantelgesetz';
    case CompoundDocumentType.STAMMVERORDNUNG:
      return 'Stammverordnung';
    case CompoundDocumentType.AENDERUNGSVERORDNUNG:
      return 'Mantelverordnung';
    default:
      return '';
  }
};

export default TitleColumn;
