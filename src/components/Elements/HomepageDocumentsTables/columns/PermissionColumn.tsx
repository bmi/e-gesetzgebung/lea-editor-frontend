// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { DocumentStateType, EntityType, HomepageCompoundDocumentDTO, HomepageSingleDocumentDTO } from '../../../../api';
import { HomePageDTO } from '../CreateDocumentsTableData';

const DocumentPermissionsColumn: FunctionComponent<HomePageDTO> = (props): ReactElement => {
  const { t } = useTranslation();
  if (props.entityType === EntityType.COMPOUND_DOCUMENT || props.entityType === EntityType.SINGLE_DOCUMENT) {
    switch ((props as HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO).state) {
      case DocumentStateType.FREEZE:
        return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.freeze')}</>;
      case DocumentStateType.FINAL:
        return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.final')}</>;
      case DocumentStateType.DRAFT:
      case DocumentStateType.BEREIT_FUER_BUNDESTAG:
        if ((props as HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO).documentPermissions.hasWrite) {
          return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.hasWrite')}</>;
        }
        if ((props as HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO).documentPermissions.hasRead) {
          return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.hasRead')}</>;
        }
        break;
      case DocumentStateType.BEREIT_FUER_KABINETTVERFAHREN:
        return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.readyForKabinettverfahren')}</>;
      case DocumentStateType.ZUGELEITET_PKP:
        return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.zugestelltPKP')}</>;
      case DocumentStateType.BESTANDSRECHT:
        return <>{t('lea.startseite.homepageDocumentsTables.tableColumns.state.type.hasRead')}</>;
      default:
        return <></>;
    }
  }
  return <></>;
};

export default DocumentPermissionsColumn;
