// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { useTranslation } from 'react-i18next';
import { HomepageTabs } from '..';
import {
  DocumentStateType,
  HomepageCompoundDocumentDTO,
  EntityType,
  HomepageSingleDocumentDTO,
  CompoundDocumentType,
  ApiRequest,
  CompoundDocumentService,
} from '../../../../api';
import { useDocumentsTableActionDispatch } from '../../../Pages/useDocumentsTableAction';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import {
  PinnedCompoundDocumentsSettings,
  PinnedDokumentenmappe,
  UserSettingsDTO,
} from '../../../../api/User/UserSettings';
import { UserEntityWithStellvertreterResponseDTO, VorhabenStatusType } from '@plateg/rest-api';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { useEffect } from 'react';
import { displayMessage } from '@plateg/theme';

const setProperties = (
  documentProps: HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO,
): CompoundDocumentInfo => {
  return {
    id: documentProps.id,
    rvId: documentProps.regulatoryProposalId,
    type: documentProps.type as CompoundDocumentType,
    version: documentProps.version,
    title: documentProps.title,
    compoundDocumentState: documentProps.state,
    documents: (
      (documentProps as HomepageCompoundDocumentDTO).children.filter(
        (child) => child.entityType === EntityType.SINGLE_DOCUMENT,
      ) as HomepageSingleDocumentDTO[]
    ).map((child) => {
      return { id: child.id, type: child.type, title: child.title, created: child.createdAt };
    }),
  };
};

const pinClickHandler = (
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>,
  setUserSettings: (userSettings: UserSettingsDTO | null) => void,
  isCompoundDocumentPinned: false | PinnedDokumentenmappe | undefined,
  rvExistsInUserSettings: PinnedDokumentenmappe | undefined,
  documentProps: HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO,
) => {
  if (!userSettingsRef.current) return;
  if (isCompoundDocumentPinned) {
    const dmIndex = rvExistsInUserSettings?.dmIds.findIndex((id) => id === documentProps.id);

    dmIndex !== undefined && rvExistsInUserSettings?.dmIds.splice(dmIndex, 1);

    setUserSettings({ pinnedDokumentenmappen: userSettingsRef.current.pinnedDokumentenmappen });
    userSettingsRef.current = {
      pinnedDokumentenmappen: userSettingsRef.current.pinnedDokumentenmappen,
      lastChange: 'unpinned',
    };
  } else {
    if (rvExistsInUserSettings) {
      rvExistsInUserSettings.dmIds.push(documentProps.id);
      setUserSettings({ pinnedDokumentenmappen: userSettingsRef.current.pinnedDokumentenmappen });
      userSettingsRef.current = {
        pinnedDokumentenmappen: userSettingsRef.current.pinnedDokumentenmappen,
        lastChange: 'pinned',
      };
    } else {
      const newArray = [
        ...userSettingsRef.current.pinnedDokumentenmappen,
        {
          rvId: documentProps.regulatoryProposalId,
          dmIds: [documentProps.id],
        },
      ];
      setUserSettings({ pinnedDokumentenmappen: newArray });
      userSettingsRef.current = {
        pinnedDokumentenmappen: newArray,
        lastChange: 'pinned',
      };
    }
  }
};

interface CompoundDocumentActionItemProps {
  items: DropdownMenuItem[];
  homepageTab: HomepageTabs;
  documentProps: HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO;
  setReadyForKabinettverfahren: ApiRequest;
  setReadyForBundestag: ApiRequest | undefined;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  setUserSettings: (userSettings: UserSettingsDTO | null) => void;
  renameMenuItem: {
    element: string;
    disabled: () => boolean;
    onClick: () => void;
  };
  setShouldUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

const newVersionButtonDisabled = (
  documentProps: HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO,
  homepageTab: HomepageTabs,
  user: UserEntityWithStellvertreterResponseDTO | undefined,
) =>
  documentProps.entityType !== EntityType.COMPOUND_DOCUMENT ||
  (homepageTab === HomepageTabs.ABSTIMMUNGEN && !documentProps.documentPermissions.hasWrite) ||
  (homepageTab === HomepageTabs.VERSIONSHISTORIE &&
    ((documentProps as HomepageCompoundDocumentDTO).children[0] as HomepageSingleDocumentDTO).createdBy?.email !==
      user?.dto.email &&
    !documentProps.documentPermissions.hasWrite);

export const GetCompoundDocumentActionItems = ({
  items,
  homepageTab,
  documentProps,
  setReadyForKabinettverfahren,
  setReadyForBundestag,
  userSettingsRef,
  setUserSettings,
  renameMenuItem,
  setShouldUpdateTable,
}: CompoundDocumentActionItemProps) => {
  const { t } = useTranslation();
  const rvExistsInUserSettings = userSettingsRef?.current?.pinnedDokumentenmappen.find(
    (pinnedDokumentenmappe) => pinnedDokumentenmappe.rvId === documentProps.regulatoryProposalId,
  );
  const { user } = useAppSelector((state) => state.user);
  const isCompoundDocumentPinned =
    !!rvExistsInUserSettings &&
    userSettingsRef.current?.pinnedDokumentenmappen.find(
      (pinnedDokumentenmappe) =>
        pinnedDokumentenmappe.rvId === documentProps.regulatoryProposalId &&
        pinnedDokumentenmappe.dmIds.find((id) => id === documentProps.id),
    );

  const [createDrucksacheResponse, setCreateDrucksache] = CompoundDocumentService.createDrucksache(
    (documentProps as HomepageCompoundDocumentDTO).id,
  );
  const {
    onNewVersion,
    onCreateNewDocument,
    onEgfa,
    onExportCompoundDocument,
    onExportPdf,
    onImportDocument,
    onReadRights,
    onWriteRights,
    onAssignBestandsrecht,
  } = useDocumentsTableActionDispatch();

  useEffect(() => {
    if (createDrucksacheResponse.isLoading) return;
    if (createDrucksacheResponse.hasError) {
      displayMessage(t('lea.messages.createDrucksache.error', { title: documentProps.title }), 'error');
    } else {
      displayMessage(t('lea.messages.createDrucksache.success', { title: documentProps.title }), 'success');
      setShouldUpdateTable(true);
    }
  }, [createDrucksacheResponse]);

  if (homepageTab === HomepageTabs.MEINE_DOKUMENTE) {
    items.push(
      {
        element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.setStatusSubmenu.title'),
        children: [
          {
            element: t(
              'lea.startseite.homepageDocumentsTables.tableColumns.action.setStatusSubmenu.readyForKabinettverfahren',
            ),
            disabled: () => documentProps.state === DocumentStateType.BEREIT_FUER_KABINETTVERFAHREN,
            onClick: setReadyForKabinettverfahren,
          },
          {
            element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.setStatusSubmenu.readyForBundestag'),
            disabled: () =>
              documentProps.state === DocumentStateType.BEREIT_FUER_BUNDESTAG ||
              (documentProps as HomepageCompoundDocumentDTO).regulatoryProposalState !== VorhabenStatusType.Bundestag,
            onClick: setReadyForBundestag,
          },
        ],
      },
      { type: 'divider' },
    );
  }
  items.push(
    {
      disabled: () => (documentProps as HomepageCompoundDocumentDTO).isNewest,
      element: isCompoundDocumentPinned ? 'Von der Startseite lösen' : 'An die Startseite anheften',
      onClick: () =>
        pinClickHandler(
          userSettingsRef,
          setUserSettings,
          isCompoundDocumentPinned,
          rvExistsInUserSettings,
          documentProps,
        ),
    },
    { type: 'divider' },
  );

  items.push(
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.newVersion'),
      disabled: () => newVersionButtonDisabled(documentProps, homepageTab, user),
      onClick: () => onNewVersion(setProperties(documentProps)),
    },
    { type: 'divider' },
  );
  user?.dto.fachreferat === 'PD 1' &&
    items.push(
      {
        element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.createDrucksache'),
        disabled: () => !documentProps.documentPermissions.hasRead || documentProps.version === '1.0',
        onClick: () => setCreateDrucksache(),
      },
      { type: 'divider' },
    );
  items.push(
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.createNewDocument'),
      disabled: () => !documentProps.documentPermissions.hasWrite,
      onClick: () => onCreateNewDocument(setProperties(documentProps)),
    },
    { type: 'divider' },
    {
      element: documentProps.documentPermissions.hasWrite
        ? t('lea.startseite.homepageDocumentsTables.tableColumns.action.setWriteRights')
        : t('lea.startseite.homepageDocumentsTables.tableColumns.action.getWriteRights'),
      disabled: () => documentProps.state !== DocumentStateType.DRAFT,
      onClick: () => onWriteRights(setProperties(documentProps)),
    },
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.setReadRights'),
      onClick: () => onReadRights(setProperties(documentProps)),
    },
    { type: 'divider' },
    {
      disabled: () => documentProps.state !== DocumentStateType.DRAFT && !documentProps.documentPermissions.hasWrite,
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.assignBestandsrecht'),
      onClick: () => onAssignBestandsrecht(setProperties(documentProps)),
    },
    { type: 'divider' },
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.importDocument'),
      disabled: () => !documentProps.documentPermissions.hasWrite,
      onClick: () => onImportDocument(setProperties(documentProps)),
    },
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.export'),
      children: [
        {
          element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.exportCompoundDocument'),
          onClick: () => onExportCompoundDocument(setProperties(documentProps)),
        },
        {
          element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.exportPdf'),
          onClick: () => onExportPdf(setProperties(documentProps)),
        },
      ],
    },
    renameMenuItem,
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.archive'),
      disabled: () => true,
    },
    {
      element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.importEgfaData'),
      disabled: () => !documentProps.documentPermissions.hasWrite,
      onClick: () => onEgfa(setProperties(documentProps)),
    },
  );
};
