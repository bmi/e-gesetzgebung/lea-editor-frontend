// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement } from 'react';
import { EntityType, HomepageCompoundDocumentDTO, HomepageSingleDocumentDTO } from '../../../../api';
import { HomePageDTO } from '../CreateDocumentsTableData';
import { PinnedCompoundDocumentsSettings } from '../../../../api/User/UserSettings';
import { PinIcon, NewestVersionIcon } from '@lea/ui';
import { Tooltip } from 'antd';
import './PropertiesColumn.less';
import { useTranslation } from 'react-i18next';

interface IPropertiesColumnProps {
  props: HomePageDTO;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
}

const PropertiesColumn: FunctionComponent<IPropertiesColumnProps> = ({ props, userSettingsRef }): ReactElement => {
  const { t } = useTranslation();
  const documentProps = props as HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO;
  const rv = userSettingsRef?.current?.pinnedDokumentenmappen.find(
    (rv) => rv.rvId === documentProps.regulatoryProposalId,
  );
  if (props.entityType === EntityType.COMPOUND_DOCUMENT && rv?.dmIds.includes(documentProps.id)) {
    return (
      <Tooltip placement="top" title={t('lea.startseite.homepageDocumentsTables.tableColumns.properties.pin.isPinned')}>
        <div className="tooltip-pin-div">
          <PinIcon />
        </div>
      </Tooltip>
    );
  } else if (props.entityType === EntityType.COMPOUND_DOCUMENT && (props as HomepageCompoundDocumentDTO).isNewest) {
    return (
      <Tooltip
        placement="top"
        title={t('lea.startseite.homepageDocumentsTables.tableColumns.properties.newestVersion')}
      >
        <div className="tooltip-pin-div">
          <NewestVersionIcon />
        </div>
      </Tooltip>
    );
  }
  return <></>;
};

export default PropertiesColumn;
