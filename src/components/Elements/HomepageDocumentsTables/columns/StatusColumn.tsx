// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CompareFn, SortOrder } from 'antd/lib/table/interface';
import React, { FunctionComponent, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { TextAndContact } from '@plateg/theme';
import { UserEntityDTO, UserEntityDTOStatusEnum } from '@plateg/rest-api/models';
//
import './StatusColumn.less';
import {
  EntityType,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../../api';
import { UserDTO } from '../../../../api/User';
import { Filters } from '@plateg/theme/src/shares/filters';
import { HomePageDTO } from '../CreateDocumentsTableData';

type ColumnComponents = {
  defaultSortOrder: SortOrder;
  sortProps: CompareFn<HomePageDTO>;
};
type HomePageDataTypes = HomePageDTO;

const StatusColumn: FunctionComponent<HomePageDataTypes> & ColumnComponents = (props): ReactElement => {
  let updatedAt = '';
  let updatedBy: UserDTO | null = null;
  if (props.entityType === EntityType.SINGLE_DOCUMENT) {
    updatedAt = (props as HomepageSingleDocumentDTO).updatedAt;
    updatedBy = (props as HomepageSingleDocumentDTO).updatedBy;
  }
  if (props.entityType === EntityType.REGULATORY_PROPOSAL) {
    const firstCompundDocument = (props as HomepageRegulatoryProposalDTO).children.find(
      (compoundDocument) => compoundDocument.children.length > 0,
    );
    if (firstCompundDocument) {
      updatedAt = (firstCompundDocument.children[0] as HomepageSingleDocumentDTO).updatedAt;
      updatedBy = (firstCompundDocument.children[0] as HomepageSingleDocumentDTO).updatedBy;
    }
  } else if (
    props.entityType === EntityType.COMPOUND_DOCUMENT &&
    (props as HomepageCompoundDocumentDTO).children.length > 0
  ) {
    updatedAt = ((props as HomepageCompoundDocumentDTO).children[0] as HomepageSingleDocumentDTO).updatedAt;
    updatedBy = ((props as HomepageCompoundDocumentDTO).children[0] as HomepageSingleDocumentDTO).updatedBy;
  }

  const { t } = useTranslation();
  const date = updatedAt ? Filters.dateFromString(updatedAt) : null;
  const time = updatedAt ? Filters.timeFromString(updatedAt) : null;
  const user: UserEntityDTO = {
    email: updatedBy?.email ?? 'Keine Angabe',
    name: updatedBy?.name ?? '',
    status: UserEntityDTOStatusEnum.Ok,
  };

  return props.entityType === EntityType.BUTTON ? (
    <></>
  ) : (
    <>
      <TextAndContact
        drawerId={`erstellt-drawer-${props.id}-${String(props.entityType === EntityType.REGULATORY_PROPOSAL)}-${String(
          props.entityType === EntityType.COMPOUND_DOCUMENT,
        )}`}
        drawerTitle={t('lea.startseite.drawer.contact')}
        firstRow={date && time ? `${date} · ${time}` : ''}
        user={user}
      />
    </>
  );
};

StatusColumn.defaultSortOrder = 'descend';

StatusColumn.sortProps = (a: HomePageDTO, b: HomePageDTO, sortOrder?: SortOrder) => {
  let aModifiedAt = (a as HomepageSingleDocumentDTO)?.updatedAt || undefined;
  let bModifiedAt = (b as HomepageSingleDocumentDTO)?.updatedAt || undefined;

  if (a.entityType === EntityType.REGULATORY_PROPOSAL && b.entityType === EntityType.REGULATORY_PROPOSAL) {
    const aCompoundDocuments = (a as HomepageRegulatoryProposalDTO).children;
    const bCompoundDocuments = (b as HomepageRegulatoryProposalDTO).children;

    //sort compoundDocuments
    if (aCompoundDocuments.length === 1) {
      aCompoundDocuments[0].children.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));
    } else {
      aCompoundDocuments.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));
    }

    if (bCompoundDocuments.length === 1) {
      bCompoundDocuments[0].children.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));
    } else {
      bCompoundDocuments.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));
    }

    //get compound document to compare
    let aCompareCompoundDocument: HomepageCompoundDocumentDTO | undefined;
    let bCompareCompoundDocument: HomepageCompoundDocumentDTO | undefined;
    if (sortOrder === 'descend') {
      for (let i = aCompoundDocuments.length - 1; i >= 0; i--) {
        if (aCompoundDocuments[i].children.length > 0) {
          aCompareCompoundDocument = aCompoundDocuments[i];
          break;
        }
      }
      for (let i = bCompoundDocuments.length - 1; i >= 0; i--) {
        if (bCompoundDocuments[i].children.length > 0) {
          bCompareCompoundDocument = bCompoundDocuments[i];
          break;
        }
      }
    } else {
      aCompareCompoundDocument = aCompoundDocuments.find((compoundDocument) => compoundDocument.children.length > 0);
      bCompareCompoundDocument = bCompoundDocuments.find((compoundDocument) => compoundDocument.children.length > 0);
    }

    //get document to compare
    const aDocuments = aCompareCompoundDocument?.children as HomepageSingleDocumentDTO[];
    const bDocuments = bCompareCompoundDocument?.children as HomepageSingleDocumentDTO[];
    aModifiedAt = aDocuments?.[sortOrder === 'descend' ? aDocuments.length - 1 : 0]?.updatedAt;
    bModifiedAt = bDocuments?.[sortOrder === 'descend' ? bDocuments.length - 1 : 0]?.updatedAt;
  }

  if (a.entityType === EntityType.COMPOUND_DOCUMENT && b.entityType === EntityType.COMPOUND_DOCUMENT) {
    //sort documents
    const aChildren = (a as HomepageCompoundDocumentDTO).children as HomepageSingleDocumentDTO[];
    const bChildren = (b as HomepageCompoundDocumentDTO).children as HomepageSingleDocumentDTO[];
    aChildren.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));
    bChildren.sort((a, b) => StatusColumn.sortProps(a, b, sortOrder));

    //get document to compare
    aModifiedAt = aChildren[sortOrder === 'descend' ? aChildren.length - 1 : 0]?.updatedAt;
    bModifiedAt = bChildren[sortOrder === 'descend' ? bChildren.length - 1 : 0]?.updatedAt;
  }

  //only compare same types
  //only compare propositions or elements of the same type with the same propositon
  if (a.entityType === b.entityType) {
    if (
      a.entityType === EntityType.REGULATORY_PROPOSAL ||
      (a as HomepageSingleDocumentDTO | HomepageCompoundDocumentDTO).regulatoryProposalId ===
        (b as HomepageSingleDocumentDTO | HomepageCompoundDocumentDTO).regulatoryProposalId
    ) {
      //move empty element to top or botton, depending on sort order
      if (!aModifiedAt && bModifiedAt) {
        return sortOrder === 'descend' ? -1 : 1;
      }
      if (aModifiedAt && !bModifiedAt) {
        return sortOrder === 'descend' ? 1 : -1;
      }
      const dateA = new Date(aModifiedAt || '').getTime();
      const dateB = new Date(bModifiedAt || '').getTime();
      return dateB - dateA;
    }
  }
  return 0;
};

export default StatusColumn;
