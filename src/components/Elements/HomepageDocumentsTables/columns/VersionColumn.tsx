// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement } from 'react';
import { EntityType, HomepageCompoundDocumentDTO } from '../../../../api';
import { HomePageDTO } from '../CreateDocumentsTableData';

const VersionColumn: FunctionComponent<HomePageDTO> = (props): ReactElement => {
  if (props.entityType === EntityType.COMPOUND_DOCUMENT) {
    return <div className="lea-table-version">{(props as HomepageCompoundDocumentDTO).version}</div>;
  } else {
    return <></>;
  }
};

export default VersionColumn;
