// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { displayMessage, DropdownMenu } from '@plateg/theme';
import React, { FunctionComponent, ReactElement, useEffect, useState } from 'react';
import {
  CompoundDocumentService,
  DocumentService,
  DocumentStateType,
  DocumentType,
  EntityType,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../../api';
import { useTranslation } from 'react-i18next';
import { useAppInfoDispatch, useAppInfoState } from '../../../App';
import { HomePageDTO, HomepageTabs } from '..';
import { useHistory } from 'react-router-dom';
import { PinnedCompoundDocumentsSettings, UserSettingsDTO } from '../../../../api/User/UserSettings';
import { useDocumentsTableActionDispatch } from '../../../Pages/useDocumentsTableAction';
import { GetCompoundDocumentActionItems } from '.';
import { SynopsisType } from '../../../../constants';

interface IActionColumnProps {
  props: HomePageDTO;
  homepageTab: HomepageTabs;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  setUserSettings: (userSettings: UserSettingsDTO | null) => void;
  setShouldUpdateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

const ActionColumn: FunctionComponent<IActionColumnProps> = ({
  props,
  homepageTab,
  userSettingsRef,
  setUserSettings,
  setShouldUpdateTable,
}: IActionColumnProps): ReactElement => {
  const { t } = useTranslation();
  const { basePath } = useAppInfoState();
  const history = useHistory();
  const documentProps = props as HomepageCompoundDocumentDTO | HomepageSingleDocumentDTO;
  const regulatoryProposalProps = props as HomepageRegulatoryProposalDTO;
  const { onRenameDocument, onStartSynopsisView } = useDocumentsTableActionDispatch();
  const { setSynopsisType } = useAppInfoDispatch();
  const [compoundDocumentId, setCompoundDocumentId] = useState('');
  const [compoundDocumentTitle, setCompoundDocumentTitle] = useState('');
  const [fetchDocumentResponse, fetchDocument] = DocumentService.getById(
    (documentProps as HomepageSingleDocumentDTO).id,
  );
  const [fetchCompoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getById(compoundDocumentId);
  const [createBestandsrechtResponse, createBestandsrecht] = DocumentService.createBestandsrecht(
    (documentProps as HomepageSingleDocumentDTO).id,
    {
      regelungsvorhabenId: documentProps.regulatoryProposalId,
      dokumentenmappenId: compoundDocumentId,
      kurzTitel: (documentProps as HomepageSingleDocumentDTO).title,
      langTitel: compoundDocumentTitle,
    },
  );
  const [setReadyForKabinettverfahrenResponse, setReadyForKabinettverfahren] = CompoundDocumentService.changeStatus(
    documentProps.id,
    {
      status: DocumentStateType.BEREIT_FUER_KABINETTVERFAHREN,
    },
  );

  const [setReadyForBundestagResponse, setReadyForBundestag] = CompoundDocumentService.changeStatus(documentProps.id, {
    status: DocumentStateType.BEREIT_FUER_BUNDESTAG,
  });

  useEffect(() => {
    if (
      (documentProps as HomepageSingleDocumentDTO).type === DocumentType.REGELUNGSTEXT_STAMMGESETZ ||
      (documentProps as HomepageSingleDocumentDTO).type === DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG
    ) {
      fetchDocument();
    }
  }, []);

  useEffect(() => {
    if (fetchDocumentResponse.isLoading) return;
    if (fetchDocumentResponse && fetchDocumentResponse.data && fetchDocumentResponse.data.compoundDocumentId) {
      setCompoundDocumentId(fetchDocumentResponse.data.compoundDocumentId);
    }
  }, [fetchDocumentResponse]);

  useEffect(() => {
    if (compoundDocumentId) {
      fetchCompoundDocument();
    }
  }, [compoundDocumentId]);

  useEffect(() => {
    if (fetchCompoundDocumentResponse.isLoading) return;
    if (fetchCompoundDocumentResponse && fetchCompoundDocumentResponse.data) {
      setCompoundDocumentTitle(fetchCompoundDocumentResponse.data.title);
    }
  }, [fetchCompoundDocumentResponse]);

  useEffect(() => {
    if (setReadyForKabinettverfahrenResponse.isLoading) return;
    if (setReadyForKabinettverfahrenResponse.hasError) {
      displayMessage(
        t('lea.messages.setReadyForKabinettverfahren.error', { compoundDocument: documentProps.title }),
        'error',
      );
    } else {
      displayMessage(
        t('lea.messages.setReadyForKabinettverfahren.success', { compoundDocument: documentProps.title }),
        'success',
      );
      window.location.hash = `#${basePath}/compounddocument/${documentProps.id}`;
    }
  }, [setReadyForKabinettverfahrenResponse]);

  useEffect(() => {
    if (setReadyForKabinettverfahrenResponse.isLoading) return;
    if (setReadyForKabinettverfahrenResponse.hasError) {
      displayMessage(
        t('lea.messages.setReadyForKabinettverfahren.error', { compoundDocument: documentProps.title }),
        'error',
      );
    } else {
      displayMessage(
        t('lea.messages.setReadyForKabinettverfahren.success', { compoundDocument: documentProps.title }),
        'success',
      );
      window.location.hash = `#${basePath}/compounddocument/${documentProps.id}`;
    }
  }, [setReadyForKabinettverfahrenResponse]);

  useEffect(() => {
    if (setReadyForBundestagResponse.isLoading) return;
    if (setReadyForBundestagResponse.hasError) {
      displayMessage(t('lea.messages.setReadyForBundestag.error'), 'error');
    } else {
      displayMessage(t('lea.messages.setReadyForBundestag.success'), 'success');
      window.location.hash = `#${basePath}/compounddocument/${documentProps.id}`;
    }
  }, [setReadyForBundestagResponse]);

  useEffect(() => {
    if (createBestandsrechtResponse.isLoading) return;
    if (createBestandsrechtResponse.hasError) {
      displayMessage(t('lea.messages.save.error'), 'error');
    } else {
      displayMessage(t('lea.messages.save.success'), 'success');
      setShouldUpdateTable(true);
    }
  }, [createBestandsrechtResponse]);

  const renameMenuItem = {
    element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.rename'),
    disabled: () => !documentProps.documentPermissions.hasWrite,
    onClick: () => {
      onRenameDocument({
        entity: documentProps.entityType,
        id: documentProps.id,
        title: documentProps.title,
      });
    },
  };

  const setBestandsrechtMenuItem = {
    element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.setBestandsrecht'),
    onClick: () => {
      createBestandsrecht();
    },
  };

  const getActionItems = (): DropdownMenuItem[] => {
    const items: DropdownMenuItem[] = [];
    switch (props.entityType) {
      case EntityType.COMPOUND_DOCUMENT: {
        GetCompoundDocumentActionItems({
          items,
          homepageTab,
          documentProps,
          setReadyForKabinettverfahren,
          setReadyForBundestag,
          userSettingsRef,
          setUserSettings,
          renameMenuItem,
          setShouldUpdateTable,
        });
        break;
      }
      case EntityType.SINGLE_DOCUMENT: {
        if ((props as HomepageSingleDocumentDTO).type === DocumentType.BT_DRUCKSACHE) {
          break;
        }
        items.push(renameMenuItem, {
          element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.startSynopsisView'),
          disabled: () => false,
          onClick: () => {
            setSynopsisType(SynopsisType.Synopsis);
            onStartSynopsisView({
              entity: documentProps.entityType,
              id: documentProps.id,
              title: documentProps.title,
              regulatoryProposalId: documentProps.regulatoryProposalId,
            });
          },
        });

        if (
          (documentProps as HomepageSingleDocumentDTO).documentPermissions.hasWrite &&
          ((documentProps as HomepageSingleDocumentDTO).type === DocumentType.REGELUNGSTEXT_STAMMGESETZ ||
            (documentProps as HomepageSingleDocumentDTO).type === DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG)
        ) {
          items.push(setBestandsrechtMenuItem);
          break;
        }

        break;
      }
      case EntityType.REGULATORY_PROPOSAL: {
        items.push({
          element: t('lea.startseite.homepageDocumentsTables.tableColumns.action.openVersionHistory'),
          disabled: () => false,
          onClick: () => {
            history.push(`${basePath}/versionHistory/${regulatoryProposalProps.id}`);
          },
        });
        break;
      }
      default: {
        break;
      }
    }
    return items;
  };

  return props.entityType === EntityType.BUTTON ||
    (props.entityType === EntityType.SINGLE_DOCUMENT &&
      (props as HomepageSingleDocumentDTO).type === DocumentType.BT_DRUCKSACHE) ? (
    <></>
  ) : (
    <DropdownMenu items={getActionItems()} elementId={props.id} overlayClass="dropdown-menu" />
  );
};

export default ActionColumn;
