// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { EmptyContentComponent, ImageModule } from '@plateg/theme';
import Text from 'antd/lib/typography/Text';
//
import './Readme.less';

const Readme: FunctionComponent = () => {
  const { t } = useTranslation();
  return (
    <div className="lea-page-homepage-component-readme">
      <EmptyContentComponent
        noIndex
        images={[
          {
            label: t(`lea.startseite.readme.images.imgText1`),
            imgSrc: require(`../../../assets/images/Icon_Dokumentenmappe.svg`) as ImageModule,
            height: 100,
          },
          {
            label: t(`lea.startseite.readme.images.imgText2`),
            imgSrc: require(`../../../assets/images/Icon_Stammgesetz.svg`) as ImageModule,
            height: 100,
          },
          {
            label: t(`lea.startseite.readme.images.imgText3`),
            imgSrc: require(`../../../assets/images/Icon_DokumenteAbstimmen.svg`) as ImageModule,
            height: 100,
          },
        ]}
      >
        <div className="emptycontentcomponent-text-wrapper">
          <Text>{t(`lea.startseite.readme.textWrapper.text1`)}</Text>
        </div>
      </EmptyContentComponent>
    </div>
  );
};

export default Readme;
