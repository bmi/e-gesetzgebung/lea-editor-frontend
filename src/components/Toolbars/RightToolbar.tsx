// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  IconButton,
  CommentOptionsIcon,
  SynopsisOptionsIcon,
  SearchReplaceOptionsIcon,
  HdrAssistentIcon,
  DaenvOptionsIcon,
} from '@lea/ui';
import React, { ReactElement, useEffect, useState } from 'react';
import { Toolbar } from './Toolbar';

import './RightToolbar.less';
import {
  EditorOptions,
  useEditorOptionsVisibleChange,
  useEditorOptionsVisibleState,
} from '../../lea-editor/plugin-core/editor-store/editor-options';
import { useSynopsisInfo } from '../../lea-editor/plugin-core';
import { useTranslation } from 'react-i18next';
import { useFirstDocumentState } from '../../api';

const RightToolbar = (): ReactElement => {
  const { t } = useTranslation();
  const { document } = useFirstDocumentState();
  const commentOptionsState = useEditorOptionsVisibleState(EditorOptions.COMMENT);
  const commentOptionsDispatch = useEditorOptionsVisibleChange(EditorOptions.COMMENT);

  const aenderungsNachverfolgungOptionsState = useEditorOptionsVisibleState(EditorOptions.AENDERUNGS_NACHVERFOLGUNG);
  const aenderungsNachverfolgungOptionsDispatch = useEditorOptionsVisibleChange(
    EditorOptions.AENDERUNGS_NACHVERFOLGUNG,
  );

  const synopsisOptionsState = useEditorOptionsVisibleState(EditorOptions.SYNOPSIS);
  const synopsisOptionsDispatch = useEditorOptionsVisibleChange(EditorOptions.SYNOPSIS);

  const searchReplaceOptionsState = useEditorOptionsVisibleState(EditorOptions.SEARCH_REPLACE);
  const searchReplaceOptionsDispatch = useEditorOptionsVisibleChange(EditorOptions.SEARCH_REPLACE);

  const hdrAssistentState = useEditorOptionsVisibleState(EditorOptions.HDR_ASSISTENT);
  const hdrAssistentDispatch = useEditorOptionsVisibleChange(EditorOptions.HDR_ASSISTENT);

  const { isSynopsisVisible } = useSynopsisInfo();

  const [hasHover, setHasHover] = useState<string>('');

  const handleHover = (id: string) => (hasHover: boolean) => {
    setTimeout(() => (hasHover ? setHasHover(id) : setHasHover('')));
  };

  useEffect(() => {
    if (!isSynopsisVisible) {
      synopsisOptionsDispatch.closeOptions();
    } else {
      commentOptionsDispatch.closeOptions();
    }
  }, [isSynopsisVisible]);

  return (
    <Toolbar className="right-toolbar">
      <Toolbar.Side ariaLabel={t('lea.sideToolbar.ariaLabel')}>
        <IconButton
          id="search-replace-options-button"
          onClick={searchReplaceOptionsDispatch.toggleOptions}
          aria-label={t('lea.searchReplaceOptions.ariaLabel')}
          title={t('lea.searchReplaceOptions.ariaLabel')}
          aria-expanded={searchReplaceOptionsState.optionsVisible}
          onHover={handleHover('search-replace-options-button')}
          icon={
            <SearchReplaceOptionsIcon
              active={searchReplaceOptionsState.optionsVisible}
              hasHover={hasHover === 'search-replace-options-button'}
            />
          }
        />
        <IconButton
          id="aenderungs-nachverfolgung-options-button"
          onClick={aenderungsNachverfolgungOptionsDispatch.toggleOptions}
          aria-label={t('lea.aenderungsNachverfolgungOptions.ariaLabel')}
          title={t('lea.aenderungsNachverfolgungOptions.ariaLabel')}
          aria-expanded={aenderungsNachverfolgungOptionsState.optionsVisible}
          onHover={handleHover('aenderungs-nachverfolgung-options-button')}
          icon={
            <DaenvOptionsIcon
              active={aenderungsNachverfolgungOptionsState.optionsVisible}
              hasHover={hasHover === 'aenderungs-nachverfolgung-options-button'}
            />
          }
        />
        <IconButton
          id="comment-options-button"
          onClick={commentOptionsDispatch.toggleOptions}
          aria-label={t('lea.commentOptions.ariaLabel')}
          title={t('lea.commentOptions.ariaLabel')}
          aria-expanded={commentOptionsState.optionsVisible}
          disabled={isSynopsisVisible}
          onHover={handleHover('comment-options-button')}
          icon={
            <CommentOptionsIcon
              active={commentOptionsState.optionsVisible}
              hasHover={hasHover === 'comment-options-button'}
            />
          }
        />
        <IconButton
          id="synopsis-options-button"
          onClick={synopsisOptionsDispatch.toggleOptions}
          aria-label={t('lea.synopsisOptions.ariaLabel')}
          title={t('lea.synopsisOptions.ariaLabel')}
          aria-expanded={synopsisOptionsState.optionsVisible}
          disabled={!isSynopsisVisible}
          onHover={handleHover('synopsis-options-button')}
          icon={
            <SynopsisOptionsIcon
              active={synopsisOptionsState.optionsVisible}
              hasHover={hasHover === 'synopsis-options-button'}
            />
          }
        />
        <IconButton
          id="hdr-assistent-button"
          onClick={hdrAssistentDispatch.toggleOptions}
          aria-label={t('lea.hdrAssistent.ariaLabel')}
          title={t('lea.hdrAssistent.ariaLabel')}
          aria-expanded={hdrAssistentState.optionsVisible}
          disabled={!document?.type.includes('REGELUNGSTEXT')}
          onHover={handleHover('hdr-assistent-button')}
          icon={
            <HdrAssistentIcon
              active={hdrAssistentState.optionsVisible}
              hasHover={hasHover === 'hdr-assistent-button'}
            />
          }
        />
      </Toolbar.Side>
    </Toolbar>
  );
};

export default RightToolbar;
