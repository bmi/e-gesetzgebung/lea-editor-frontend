// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'antd';
import { ExclamationCircleFilled } from '@plateg/theme';
import Icon from '@ant-design/icons';
//
import { useAppInfoState } from '../App';
import { Toolbar } from './Toolbar';
import { useNotification } from '../Elements/notifications/useNotification';
import { CloseConfirmation } from './SaveConfirmation/CloseConfirmation';
//
import './TopToolbar.less';

import {
  DocumentStateType,
  useFirstDocumentState,
  useSynopsisDocumentsDispatch,
  useSynopsisDocumentsState,
} from '../../api';
import { useEditorInfoDispatch, useSynopsisInfo } from '../../lea-editor/plugin-core';
import { SynopsisType } from '../../constants';

interface IProps {
  onExport?: (event: React.MouseEvent) => void;
  canSave: boolean;
  onSave: (event: React.MouseEvent) => void;
  onClose: () => void;
  setShowPrompt: React.Dispatch<React.SetStateAction<boolean>>;
}

const TopToolbar = ({ onExport, canSave, onSave, onClose, setShowPrompt }: IProps): ReactElement => {
  const { t } = useTranslation();
  const { addNotification } = useNotification();
  const { removeDocument } = useSynopsisDocumentsDispatch();
  const firstDocument = useFirstDocumentState();
  const { removeSynopsisDocuments } = useSynopsisInfo();
  const { synopsisType } = useAppInfoState();
  const { setRemoveSynopsisDocuments } = useEditorInfoDispatch();
  const { documents } = useSynopsisDocumentsState();
  const { isSynopsisVisible } = useSynopsisInfo();
  const [closeConfirmationOpened, setCloseConfirmationOpened] = useState(false);

  useEffect(() => {
    if (removeSynopsisDocuments && firstDocument.document?.state) {
      removeDocument();
      setRemoveSynopsisDocuments(false);
    }
  }, [removeSynopsisDocuments, firstDocument.document]);

  const onPrint = () => {
    window.print();
  };

  const onCloseBtn = () => {
    setCloseConfirmationOpened(true);
    setShowPrompt(false);
  };

  const getInfoString = (documentState?: DocumentStateType) => {
    if (documentState === DocumentStateType.BEREIT_FUER_KABINETTVERFAHREN) {
      return t('lea.topToolbar.readOnlyInfo.readyForKabinettverfahren.label');
    } else if (synopsisType === SynopsisType.Aenderungsvergleich) {
      return t('lea.topToolbar.readOnlyInfo.aenderungsvergleich.label');
    } else if (documentState === DocumentStateType.BESTANDSRECHT) {
      return t('lea.topToolbar.readOnlyInfo.bestandsrecht.label');
    } else if (documentState) {
      return t(`lea.topToolbar.readOnlyInfo.label.${documentState}`);
    }
    return '';
  };

  const getStateText = () => {
    const documentState = isSynopsisVisible ? documents[0]?.state : firstDocument.document?.state;
    return getInfoString(documentState);
  };

  const onClickDocumentStateInfoHandler = useCallback(() => {
    if (firstDocument.document?.state === DocumentStateType.BEREIT_FUER_KABINETTVERFAHREN) {
      addNotification(
        t('lea.topToolbar.readOnlyInfo.readyForKabinettverfahren.dialog.title'),
        t('lea.topToolbar.readOnlyInfo.readyForKabinettverfahren.dialog.text'),
      );
    } else if (synopsisType === SynopsisType.Aenderungsvergleich) {
      addNotification(
        t('lea.topToolbar.readOnlyInfo.aenderungsvergleich.dialog.title'),
        t('lea.topToolbar.readOnlyInfo.aenderungsvergleich.dialog.text'),
      );
    } else if (firstDocument.document?.state === DocumentStateType.BESTANDSRECHT) {
      addNotification(
        t('lea.topToolbar.readOnlyInfo.bestandsrecht.dialog.title'),
        t('lea.topToolbar.readOnlyInfo.bestandsrecht.dialog.text'),
      );
    } else if (firstDocument.document?.state === DocumentStateType.BEREIT_FUER_BUNDESTAG) {
      addNotification(
        t('lea.topToolbar.readOnlyInfo.readyForBundestag.dialog.title'),
        t('lea.topToolbar.readOnlyInfo.readyForBundestag.dialog.text'),
      );
    } else {
      addNotification(t('lea.topToolbar.readOnlyInfo.dialog.title'), t('lea.topToolbar.readOnlyInfo.dialog.text'));
    }
  }, []);

  return (
    <Toolbar className="top-toolbar">
      <Toolbar.Left />
      <Toolbar.Middle>
        <span className="document-title">{firstDocument.document?.title ?? ''}</span>
      </Toolbar.Middle>
      <Toolbar.Right>
        {!canSave && (
          <Button id="lea-document-state-info-button" type="text" onClick={onClickDocumentStateInfoHandler}>
            <Icon component={ExclamationCircleFilled} /> {getStateText()}
          </Button>
        )}
        <Button id="print-button" onClick={onPrint} disabled={true}>
          {t('lea.topToolbar.print')}
        </Button>
        <Button id="export-button" onClick={onExport} disabled={isSynopsisVisible}>
          {t('lea.topToolbar.export')}
        </Button>
        <Button id="save-button" onClick={onSave} disabled={!canSave}>
          {t('lea.topToolbar.save')}
        </Button>
        <CloseConfirmation
          confirmationOpened={closeConfirmationOpened}
          setConfirmationOpened={setCloseConfirmationOpened}
          onClose={onClose}
          setShowPrompt={setShowPrompt}
        />
        <Button id="close-button" onClick={onCloseBtn}>
          {t('lea.topToolbar.close')}
        </Button>
      </Toolbar.Right>
    </Toolbar>
  );
};

export default TopToolbar;
