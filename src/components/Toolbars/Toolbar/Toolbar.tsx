// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
//
import './Toolbar.less';

type Props = {
  className?: string;
  ariaLabel?: string;
  children?: React.ReactNode;
};
const Toolbar: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={`toolbar ${className ?? ''}`} {...props} />
);
const ToolbarLeft: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={`toolbar-left ${className ?? ''}`} {...props} />
);
const ToolbarMiddle: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={`toolbar-middle ${className ?? ''}`} {...props} />
);
const ToolbarRight: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={`toolbar-right ${className ?? ''}`} {...props} />
);
const ToolbarTop: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={`toolbar-top ${className ?? ''}`} {...props} />
);

const ToolbarSide: FunctionComponent<Props> = ({ className, ariaLabel, ...props }) => (
  <nav tabIndex={0} className={`toolbar-side ${className ?? ''}`} aria-label={ariaLabel} {...props} />
);

export default Object.assign(Toolbar, {
  Left: ToolbarLeft,
  Middle: ToolbarMiddle,
  Right: ToolbarRight,
  Top: ToolbarTop,
  Side: ToolbarSide,
});
