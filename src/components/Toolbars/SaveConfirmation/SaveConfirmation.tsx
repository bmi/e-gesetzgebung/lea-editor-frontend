// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ExclamationCircleFilled } from '@plateg/theme';
import { Button, Popover } from 'antd';
import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import Text from 'antd/lib/typography/Text';
import { useTranslation } from 'react-i18next';

interface SaveConfirmationProps {
  confirmationOpened: boolean;
  setConfirmationOpened: React.Dispatch<React.SetStateAction<boolean>>;
  onSave: () => void;
  type: 'closeDocument' | 'closeSynopsis' | 'configureSynopsis';
  children?: React.ReactNode;
  mainBtnRef?: React.RefObject<HTMLAnchorElement>;
}

export const SaveConfirmation: FunctionComponent<SaveConfirmationProps> = ({
  confirmationOpened,
  setConfirmationOpened,
  onSave,
  type,
  children,
  mainBtnRef,
}) => {
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const closeSynopsisBtnRef = useRef<HTMLButtonElement>(null);

  const handleClose = () => {
    setConfirmationOpened(false);
    setOpen(false);
  };

  useEffect(() => {
    setOpen(confirmationOpened);
    if (confirmationOpened) {
      setTimeout(() => {
        closeSynopsisBtnRef.current?.focus();
      }, 100);
    }
  }, [confirmationOpened]);

  const popoverTitle = (
    <div>
      <span
        style={{ verticalAlign: 'top', transform: 'translate(0, 0.125em)' }}
        role="img"
        aria-label="Achtung-Icon"
        className="anticon anticon-exclamation-circle"
      >
        <ExclamationCircleFilled />
      </span>

      <Text style={{ maxWidth: '300px', display: 'inline-block' }} className="ant-popover-message-title">
        {t(`lea.topToolbar.saveConfirmation.${type}.warningText`)}
      </Text>
    </div>
  );

  const handleSave = () => {
    handleClose();
    onSave();
  };

  const popoverContent = (
    <div>
      <div className="ant-popover-buttons">
        <Button id="document-handleClose-btn" onClick={handleSave} type="primary" ref={closeSynopsisBtnRef}>
          {t(`lea.topToolbar.saveConfirmation.${type}.close`)}
        </Button>
        <Button
          id="comment-abortClose-btn"
          onClick={() => {
            handleClose();
            mainBtnRef?.current?.focus();
          }}
          type="default"
          ref={cancelBtnRef}
        >
          {t(`lea.topToolbar.saveConfirmation.${type}.abort`)}
        </Button>
      </div>
    </div>
  );
  return (
    <Popover zIndex={1051} title={popoverTitle} open={open} placement="bottomRight" content={popoverContent}>
      {children}
    </Popover>
  );
};
