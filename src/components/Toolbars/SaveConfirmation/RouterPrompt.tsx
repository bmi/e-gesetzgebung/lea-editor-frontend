// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router';

import { Button, Modal } from 'antd';
import { ExclamationCircleFilled } from '@plateg/theme';
import { useTranslation } from 'react-i18next';
import Text from 'antd/lib/typography/Text';
import './RouterPrompt.less';

interface RouterPromptProps {
  when: boolean;
}

export const RouterPrompt: FunctionComponent<RouterPromptProps> = ({ when }) => {
  const { t } = useTranslation();
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [currentPath, setCurrentPath] = useState('');

  const unblockRef = useRef<(() => void) | null>(null);

  useEffect(() => {
    if (when) {
      unblockRef.current = history.block((location) => {
        setCurrentPath(location.pathname);
        setOpen(true);
        return false;
      });
    } else {
      if (unblockRef.current) {
        unblockRef.current();
        unblockRef.current = null;
      }
    }

    return () => {
      if (unblockRef.current) {
        unblockRef.current();
      }
    };
  }, [when]);

  const handleOK = () => {
    if (unblockRef.current) {
      unblockRef.current();
      unblockRef.current = null;
    }
    history.push(currentPath);
    setOpen(false);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const popoverTitle = (
    <div>
      <span
        style={{ verticalAlign: 'top', transform: 'translate(0, 0.125em)', marginRight: '8px' }}
        role="img"
        aria-label="Achtung-Icon"
        className="anticon anticon-exclamation-circle"
      >
        <ExclamationCircleFilled />
      </span>

      <Text style={{ maxWidth: '300px', display: 'inline-block' }} className="ant-popover-message-title">
        {t(`lea.topToolbar.closeConfirmation.leavePage.warningTitle`)}
      </Text>
    </div>
  );

  const popoverContent = (
    <div>
      <Text style={{ maxWidth: '300px', display: 'inline-block', fontSize: '1.6rem' }}>
        {t(`lea.topToolbar.closeConfirmation.leavePage.warningText`)}
      </Text>
    </div>
  );

  const popoverFooter = (
    <div className="ant-popover-buttons">
      <Button id="prompt-handleClose-btn" className="prompt-close-btn" onClick={handleOK} type="primary">
        {t(`lea.topToolbar.closeConfirmation.leavePage.close`)}
      </Button>
      <Button id="prompt-abortClose-btn" onClick={handleCancel} type="default">
        {t(`lea.topToolbar.closeConfirmation.abort`)}
      </Button>
    </div>
  );
  return (
    <Modal
      zIndex={1051}
      title={popoverTitle}
      open={open}
      className="security-prompt"
      footer={popoverFooter}
      closable={false}
      width="370px"
    >
      {popoverContent}
    </Modal>
  );
};
