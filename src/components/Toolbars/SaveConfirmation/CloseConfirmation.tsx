// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ExclamationCircleFilled } from '@plateg/theme';
import { Button, Modal } from 'antd';
import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import Text from 'antd/lib/typography/Text';
import { useTranslation } from 'react-i18next';

interface CloseConfirmationProps {
  confirmationOpened: boolean;
  setConfirmationOpened: React.Dispatch<React.SetStateAction<boolean>>;
  onClose: () => void;
  setShowPrompt: React.Dispatch<React.SetStateAction<boolean>>;
}

export const CloseConfirmation: FunctionComponent<CloseConfirmationProps> = ({
  confirmationOpened,
  setConfirmationOpened,
  onClose,
  setShowPrompt,
}) => {
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);

  const closeSynopsisBtnRef = useRef<HTMLButtonElement>(null);

  const handleClose = () => {
    setConfirmationOpened(false);
    setOpen(false);
  };

  const handleCancel = () => {
    handleClose();
    setShowPrompt(true);
  };

  useEffect(() => {
    setOpen(confirmationOpened);
    if (confirmationOpened) {
      setTimeout(() => {
        closeSynopsisBtnRef.current?.focus();
      }, 100);
    }
  }, [confirmationOpened]);

  const handleCloseWindow = () => {
    handleClose();
    onClose();
  };
  const popoverTitle = (
    <div>
      <span
        style={{ verticalAlign: 'top', transform: 'translate(0, 0.125em)', marginRight: '8px' }}
        role="img"
        aria-label="Achtung-Icon"
        className="anticon anticon-exclamation-circle"
      >
        <ExclamationCircleFilled />
      </span>

      <Text style={{ display: 'inline-block' }} className="ant-popover-message-title">
        {t(`lea.topToolbar.closeConfirmation.closeDocument.warningTitle`)}
      </Text>
    </div>
  );

  const popoverContent = (
    <div>
      <Text style={{ maxWidth: '300px', display: 'inline-block', fontSize: '1.6rem' }}>
        {t(`lea.topToolbar.closeConfirmation.closeDocument.warningText`)}
      </Text>
    </div>
  );

  const popoverFooter = (
    <div className="ant-popover-buttons">
      <Button id="document-handleClose-btn" className="prompt-close-btn" onClick={handleCloseWindow} type="primary">
        {t(`lea.topToolbar.closeConfirmation.closeDocument.close`)}
      </Button>
      <Button id="document-abortClose-btn" onClick={handleCancel} type="default">
        {t(`lea.topToolbar.closeConfirmation.abort`)}
      </Button>
    </div>
  );
  return (
    <Modal
      zIndex={1051}
      title={popoverTitle}
      open={open}
      className="security-prompt"
      footer={popoverFooter}
      closable={false}
      width="420px"
    >
      {popoverContent}
    </Modal>
  );
};
