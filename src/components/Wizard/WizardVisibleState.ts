// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

enum Wizards {
  NEW_COMPOUND_DOCUMENT = 'NewCompoundDocumentWizard',
  NEW_DOCUMENT = 'NewDocumentWizard',
  EXPORT_COMPOUNDDOCUMENT = 'ExportCompoundDocumentWizard',
  IMPORT_DOCUMENT = 'ImportDocumentWizard',
  NEW_VERSION = 'NewVersionWizard',
  RENAME_DOCUMENT = 'RenameDocumentWizard',
  SYNOPSIS = 'SynopsisWizard',
  SET_DOCUMENT_READ_RIGHTS = 'DocumentReadRightsWizard',
  EGFA = 'EgfaWizard',
  SET_DOCUMENT_WRITE_RIGHTS = 'DocumentWriteRightsWizard',
  EXPORT_PDF = 'ExportPdfWizard',
  ASSIGN_BESTANDSRECHT = 'AssignBestandsrechtWizard',
}

type WizardVisibleState = {
  [Wizards.NEW_COMPOUND_DOCUMENT]: boolean;
  [Wizards.NEW_DOCUMENT]: boolean;
  [Wizards.EXPORT_COMPOUNDDOCUMENT]: boolean;
  [Wizards.IMPORT_DOCUMENT]: boolean;
  [Wizards.NEW_VERSION]: boolean;
  [Wizards.RENAME_DOCUMENT]: boolean;
  [Wizards.SYNOPSIS]: boolean;
  [Wizards.SET_DOCUMENT_READ_RIGHTS]: boolean;
  [Wizards.EGFA]: boolean;
  [Wizards.SET_DOCUMENT_WRITE_RIGHTS]: boolean;
  [Wizards.EXPORT_PDF]: boolean;
  [Wizards.ASSIGN_BESTANDSRECHT]: boolean;
};

const InitialWizardVisibleState: WizardVisibleState = {
  [Wizards.NEW_COMPOUND_DOCUMENT]: false,
  [Wizards.NEW_DOCUMENT]: false,
  [Wizards.EXPORT_COMPOUNDDOCUMENT]: false,
  [Wizards.IMPORT_DOCUMENT]: false,
  [Wizards.NEW_VERSION]: false,
  [Wizards.RENAME_DOCUMENT]: false,
  [Wizards.SYNOPSIS]: false,
  [Wizards.SET_DOCUMENT_READ_RIGHTS]: false,
  [Wizards.EGFA]: false,
  [Wizards.SET_DOCUMENT_WRITE_RIGHTS]: false,
  [Wizards.EXPORT_PDF]: false,
  [Wizards.ASSIGN_BESTANDSRECHT]: false,
};

export { InitialWizardVisibleState, WizardVisibleState, Wizards };
