// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

type Props = {
  fieldName: string;
};

export const AuthorialNoteMarkerFormItem: FunctionComponent<Props> = ({ fieldName }) => {
  const { t } = useTranslation();

  const keyDownHandler = (e: KeyboardEvent) => {
    if (e.key === ' ') {
      e.preventDefault();
    }
  };

  const label = (
    <span>
      {t(`lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteMarkerFormItem.label`)}
    </span>
  );
  return (
    <Form.Item
      name={fieldName}
      label={label}
      rules={[
        {
          required: true,
          message: t(
            `lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteMarkerFormItem.requiredMsg`,
          ),
        },
        () => ({
          validator(_, value: string) {
            if (value?.length > 0 && value?.trim() === '') {
              return Promise.reject(
                new Error(
                  t(
                    `lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteMarkerFormItem.requiredMsg`,
                  ),
                ),
              );
            }
            return Promise.resolve();
          },
        }),
      ]}
    >
      <Input onKeyDown={(e) => keyDownHandler(e as unknown as KeyboardEvent)} />
    </Form.Item>
  );
};
