// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';

import {
  ApiResponse,
  CompoundDocumentTitleDTO,
  CompoundDocumentType,
  DocumentType,
  HomepageCompoundDocumentDTO,
} from '../../../api';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

type Props = {
  fieldName: string;
  currentWizard:
    | 'CreateNewDocumentWizard'
    | 'ExportCompoundDocumentWizard'
    | 'ImportDocumentWizard'
    | 'SynopsisWizard'
    | 'ExportPdfWizard'
    | 'AssignBestandsrechtWizard';
  compoundDocumentResponse?: ApiResponse<CompoundDocumentTitleDTO[]>;
  compoundDocuments?: HomepageCompoundDocumentDTO[];
  documentType?: DocumentType;
  initialValue?: string;
};

export const CompoundDocumentFormItem: FunctionComponent<Props> = ({
  fieldName = 'regelungsvorhaben',
  currentWizard,
  compoundDocumentResponse,
  compoundDocuments,
  documentType,
  initialValue,
}) => {
  const { t } = useTranslation();
  const [options, setOptions] = useState<CompoundDocumentTitleDTO[] | HomepageCompoundDocumentDTO[]>([]);

  const label = (
    <span>
      {t('lea.Document.FormItems.CompoundDocumentFormItem.label')}
      <span className="infoRequired">
        <HiddenInfoComponent
          title={t('lea.Document.FormItems.CompoundDocumentFormItem.infoComponent.title')}
          text={t('lea.Document.FormItems.CompoundDocumentFormItem.infoComponent.text.' + currentWizard)}
        />
      </span>
    </span>
  );

  useEffect(() => {
    if (compoundDocumentResponse?.isLoading) return;
    if (!compoundDocumentResponse?.data || !Array.isArray(compoundDocumentResponse.data)) return;
    let options = compoundDocumentResponse.data;
    if (documentType?.includes('STAMMGESETZ')) {
      options = options.filter((compoundDocument) => compoundDocument.type === CompoundDocumentType.STAMMGESETZ);
    } else if (documentType?.includes('MANTELGESETZ')) {
      options = options.filter((compoundDocument) => compoundDocument.type === CompoundDocumentType.MANTELGESETZ);
    } else if (documentType?.includes('STAMMVERORDNUNG')) {
      options = options.filter((compoundDocument) => compoundDocument.type === CompoundDocumentType.STAMMVERORDNUNG);
    } else if (documentType?.includes('MANTELVERORDNUNG')) {
      options = options.filter(
        (compoundDocument) => compoundDocument.type === CompoundDocumentType.AENDERUNGSVERORDNUNG,
      );
    }
    setOptions(options);
  }, [compoundDocumentResponse]);

  useEffect(() => {
    if (compoundDocuments) {
      setOptions(compoundDocuments);
    }
  });

  return (
    <FormItemWithInfo
      label={label}
      rules={[{ required: true, message: t('lea.Document.FormItems.CompoundDocumentFormItem.requiredMsg') }]}
      name={fieldName}
    >
      <SelectWrapper
        disabled={compoundDocumentResponse?.isLoading}
        placeholder={t('lea.Document.FormItems.CompoundDocumentFormItem.placeholder')}
        suffixIcon={<SelectDown />}
        defaultValue={initialValue}
        options={options.map((item) => ({
          label: (
            <span key={item.id} aria-label={item.title}>
              {item.title}
            </span>
          ),
          value: item.id,
          title: item.title,
        }))}
      />
    </FormItemWithInfo>
  );
};
