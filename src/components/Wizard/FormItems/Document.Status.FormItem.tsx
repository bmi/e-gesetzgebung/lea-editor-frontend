// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio, Row } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { HiddenInfoComponent } from '@plateg/theme';
type Props = {
  fieldName: string;
};

export const DocumentStatusFormItem: FunctionComponent<Props> = ({ fieldName = 'document-status' }) => {
  const { t } = useTranslation();

  const radioValues = [{ key: 'referentenentwurf', title: 'Referentenentwurf' }];

  return (
    <Form.Item
      label={t('lea.Document.FormItems.DocumentStatusFormItem.label')}
      name={fieldName}
      rules={[
        {
          required: true,
          message: t('lea.Document.FormItems.DocumentStatusFormItem.requiredMsg'),
        },
      ]}
    >
      <Radio.Group value={radioValues[0].key}>
        <Row>
          {radioValues.map(({ key, title }) => {
            return (
              <Radio key={`DocStatus-Item-${title}`} id={`radio-button-${key}`} value={key}>
                {title}
                <HiddenInfoComponent
                  title={t(`lea.Document.FormItems.DocumentStatusFormItem.infoComponent.${key}.title`)}
                  text={t(`lea.Document.FormItems.DocumentStatusFormItem.infoComponent.${key}.text`)}
                />
              </Radio>
            );
          })}
        </Row>
      </Radio.Group>
    </Form.Item>
  );
};
