// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { HiddenInfoComponent } from '@plateg/theme';
import { Form, Radio } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

type Props = {
  fieldName: string;
  setCreateCopy: React.Dispatch<React.SetStateAction<boolean>>;
};

export const GfaNewVersionFormItem: FunctionComponent<Props> = ({ fieldName, setCreateCopy }) => {
  const { t } = useTranslation();
  const label = (
    <>
      <span>
        {<span>{t('lea.Wizard.EgfaWizard.newVersionRadio.label')}</span>}
        <span className="infoRequired">
          <HiddenInfoComponent
            title={t('lea.Wizard.EgfaWizard.newVersionRadio.infoComponent.title')}
            text={t('lea.Wizard.EgfaWizard.newVersionRadio.infoComponent.text')}
          />
        </span>
      </span>
    </>
  );
  return (
    <Form.Item name={fieldName} rules={[{ required: true }]} initialValue={false} label={label}>
      <Radio.Group
        name="radiogroup-gfa-new-version"
        className="horizontal-radios"
        onChange={(e) => setCreateCopy(e.target.value as boolean)}
      >
        <Radio id="wizard-gfa-new-version-ja" value={true}>
          {t('lea.Wizard.EgfaWizard.newVersionRadio.options.yes')}
        </Radio>
        <Radio id="wizard-gfa-new-version-nein" value={false}>
          {t('lea.Wizard.EgfaWizard.newVersionRadio.options.no')}
        </Radio>
      </Radio.Group>
    </Form.Item>
  );
};
