// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Radio } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';
import { CompoundDocumentType, DocumentType } from '../../../api';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';
type Props = {
  fieldName: string;
  compoundDocumentInfo: CompoundDocumentInfo;
};

export const DocumentTypeFormItem: FunctionComponent<Props> = ({
  fieldName = 'document-type',
  compoundDocumentInfo,
}) => {
  const { t } = useTranslation();
  const radioValues = [
    {
      key: DocumentType.ANSCHREIBEN_STAMMGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.ANSCHREIBEN_STAMMGESETZ'),
    },
    {
      key: DocumentType.VORBLATT_STAMMGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.VORBLATT_STAMMGESETZ'),
    },
    {
      key: DocumentType.REGELUNGSTEXT_STAMMGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.REGELUNGSTEXT_STAMMGESETZ'),
    },
    {
      key: DocumentType.BEGRUENDUNG_STAMMGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.BEGRUENDUNG_STAMMGESETZ'),
    },
    {
      key: DocumentType.ANSCHREIBEN_STAMMVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.ANSCHREIBEN_STAMMVERORDNUNG'),
    },
    {
      key: DocumentType.VORBLATT_STAMMVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.VORBLATT_STAMMVERORDNUNG'),
    },
    {
      key: DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.REGELUNGSTEXT_STAMMVERORDNUNG'),
    },
    {
      key: DocumentType.BEGRUENDUNG_STAMMVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.BEGRUENDUNG_STAMMVERORDNUNG'),
    },
    {
      key: DocumentType.ANSCHREIBEN_MANTELGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.ANSCHREIBEN_MANTELGESETZ'),
    },
    {
      key: DocumentType.VORBLATT_MANTELGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.VORBLATT_MANTELGESETZ'),
    },
    {
      key: DocumentType.REGELUNGSTEXT_MANTELGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.REGELUNGSTEXT_MANTELGESETZ'),
    },
    {
      key: DocumentType.BEGRUENDUNG_MANTELGESETZ,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.BEGRUENDUNG_MANTELGESETZ'),
    },
    {
      key: DocumentType.ANSCHREIBEN_MANTELVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.ANSCHREIBEN_MANTELVERORDNUNG'),
    },
    {
      key: DocumentType.VORBLATT_MANTELVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.VORBLATT_MANTELVERORDNUNG'),
    },
    {
      key: DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.REGELUNGSTEXT_MANTELVERORDNUNG'),
    },
    {
      key: DocumentType.BEGRUENDUNG_MANTELVERORDNUNG,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.BEGRUENDUNG_MANTELVERORDNUNG'),
    },
    {
      key: DocumentType.ANLAGE,
      title: t('lea.Document.FormItems.DocumentTypeFormItem.radioValues.ANLAGE'),
    },
  ];
  const existingDocumentTypes = (compoundDocumentInfo.documents ?? []).map((document) => document.type);
  const compoundDocumentType =
    compoundDocumentInfo.type === CompoundDocumentType.AENDERUNGSVERORDNUNG
      ? 'MANTELVERORDNUNG'
      : compoundDocumentInfo.type;
  const filteredRadioValues = radioValues.filter((radioValue) => {
    const documentType = radioValue.key;
    return (
      (documentType.includes(compoundDocumentType ?? CompoundDocumentType.STAMMGESETZ) &&
        !existingDocumentTypes.includes(documentType)) ||
      documentType === DocumentType.ANLAGE
    );
  });

  const getInfoComponentKeyFromType = (documentType: DocumentType) => {
    switch (documentType) {
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG: {
        return 'REGELUNGSTEXT';
      }
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG: {
        return 'BEGRUENDUNG';
      }
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG: {
        return 'ANSCHREIBEN';
      }
      case DocumentType.VORBLATT_MANTELGESETZ:
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
      case DocumentType.VORBLATT_STAMMGESETZ:
      case DocumentType.VORBLATT_STAMMVERORDNUNG: {
        return 'VORBLATT';
      }
      case DocumentType.ANLAGE: {
        return 'ANLAGE';
      }
      default: {
        return '';
      }
    }
  };

  const label = <span>{t('lea.Document.FormItems.DocumentTypeFormItem.label')}</span>;

  return (
    <FormItemWithInfo
      name={fieldName}
      label={label}
      rules={[
        {
          required: true,
          message: t('lea.Document.FormItems.DocumentTypeFormItem.requiredMsg'),
        },
      ]}
    >
      <Radio.Group className="horizontal-radios">
        {filteredRadioValues.map(({ key, title }, _index) => {
          return (
            <Radio id={`radio-button-${key}`} value={key} key={`radio-button-${key}`}>
              {title}
              <HiddenInfoComponent
                title={t(
                  `lea.Document.FormItems.DocumentTypeFormItem.infoComponent.${getInfoComponentKeyFromType(key)}.title`,
                )}
                text={t(
                  `lea.Document.FormItems.DocumentTypeFormItem.infoComponent.${getInfoComponentKeyFromType(key)}.text`,
                )}
              />
            </Radio>
          );
        })}
      </Radio.Group>
    </FormItemWithInfo>
  );
};
