// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined, FormItemWithInfo, SelectWrapper } from '@plateg/theme';

import {
  CompoundDocumentService,
  CompoundDocumentSummaryDTO,
  DocumentSummaryDTO,
  HomepageCompoundDocumentDTO,
} from '../../../api';
import { SynopsisDocument, FieldData } from '../SynopsisWizard/Steps';
import { NumberWords } from '../../../constants';
import FormItemLabel from 'antd/lib/form/FormItemLabel';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

type Props = {
  fieldNames: string[];
  synopsisDocument: SynopsisDocument;
  compoundDocuments: CompoundDocumentSummaryDTO[] | HomepageCompoundDocumentDTO[];
  index: number;
  currentIndex: number;
  setCurrentIndex: React.Dispatch<React.SetStateAction<number | undefined>>;
  handleDeleteDocumentClick: (index: number, id: string) => void;
  selectedDocumentIds: string[];
  setFields: React.Dispatch<React.SetStateAction<FieldData[]>>;
};

export const DocumentFormItem: FunctionComponent<Props> = ({
  fieldNames,
  synopsisDocument,
  compoundDocuments,
  index,
  currentIndex,
  setCurrentIndex,
  handleDeleteDocumentClick,
  selectedDocumentIds,
  setFields,
}) => {
  const { t } = useTranslation();
  const [compoundDocumentId, setCompoundDocumentId] = useState<string>(synopsisDocument.compoundDocumentId || '');
  const [compoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getSummaryById(
    compoundDocumentId ?? '',
  );
  const [previousCompoundDocumentId, setPreviousCompoundDocumentId] = useState('');
  const [allDocuments, setAllDocuments] = useState<DocumentSummaryDTO[]>([]);
  const [filteredDocuments, setFilteredDocuments] = useState<DocumentSummaryDTO[]>([]);

  useEffect(() => {
    compoundDocumentId !== '' && fetchCompoundDocument();
  }, []);

  useEffect(() => {
    if (!compoundDocumentResponse.data || compoundDocumentResponse.hasError) return;
    const allDocuments = compoundDocumentResponse.data.documents;
    setAllDocuments(allDocuments);
    const filteredDocuments = allDocuments.filter(
      (currentDocument) => !selectedDocumentIds.find((id) => currentDocument.id === id),
    );
    setFilteredDocuments(filteredDocuments);
  }, [compoundDocumentResponse]);

  useEffect(() => {
    if (compoundDocumentId === '') return;
    if (currentIndex === index) {
      fetchCompoundDocument();
      setPreviousCompoundDocumentId(compoundDocumentId);
    }
  }, [compoundDocumentId]);

  useEffect(() => {
    if (previousCompoundDocumentId !== compoundDocumentId) {
      setFields((fields) =>
        fields.map((field) => {
          if (field.name.includes(`documentId${index}`)) {
            return { ...field, value: '' };
          } else return field;
        }),
      );
    }
  }, [allDocuments]);

  useEffect(() => {
    const filteredDocuments = allDocuments.filter(
      (currentDocument) => !selectedDocumentIds.find((id) => currentDocument.id === id),
    );
    setFilteredDocuments(filteredDocuments);
  }, [selectedDocumentIds]);

  const documentLabel = <span>{t('lea.Document.FormItems.DocumentFormItem.documentLabel')}</span>;
  const compoundDocumentLabel = <span>{t('lea.Document.FormItems.DocumentFormItem.compoundDocumentLabel')}</span>;
  const documentNumber = NumberWords[index + 1];
  const configLabel = t('lea.Document.FormItems.DocumentFormItem.headerLabel', { index: documentNumber });

  const handleDeleteClick = () => {
    handleDeleteDocumentClick(index, synopsisDocument.documentId);
  };

  return (
    <>
      <FormItemLabel prefixCls="config" label={configLabel}></FormItemLabel>
      <FormItemWithInfo
        className="synopsis-form-item"
        label={compoundDocumentLabel}
        rules={[{ required: index < 1, message: t('lea.Document.FormItems.DocumentFormItem.requiredMsg') }]}
        name={`${fieldNames[0]}${index}`}
      >
        <SelectWrapper
          key={`${fieldNames[0]}${index}`}
          placeholder={t('lea.Document.FormItems.DocumentFormItem.placeholder')}
          suffixIcon={<SelectDown />}
          onChange={(value: string) => {
            setCurrentIndex(index);
            setCompoundDocumentId(value);
          }}
          options={compoundDocuments.map((item) => ({
            label: (
              <span key={item.id} aria-label={item.title}>
                {item.title}
              </span>
            ),
            value: item.id,
            title: item.title,
          }))}
        />
      </FormItemWithInfo>
      <FormItemWithInfo
        className="synopsis-form-item"
        label={documentLabel}
        rules={[{ required: index < 1, message: t('lea.Document.FormItems.DocumentFormItem.requiredMsg') }]}
        name={`${fieldNames[1]}${index}`}
      >
        <SelectWrapper
          key={`${fieldNames[1]}${index}`}
          onChange={() => {
            setCurrentIndex(index);
          }}
          placeholder={t('lea.Document.FormItems.DocumentFormItem.placeholder')}
          suffixIcon={<SelectDown />}
          disabled={!filteredDocuments.length}
          options={allDocuments.map((item) => ({
            label: (
              <span key={item.id} aria-label={item.title}>
                {item.title}
              </span>
            ),
            value: item.id,
            title: item.title,
            disabled: !filteredDocuments.find((filteredDocument) => filteredDocument.id === item.id),
          }))}
        />
      </FormItemWithInfo>
      <div className={`delete-button${index === 0 ? ' hidden' : ''}`}>
        <Button
          className="delete-document-button"
          type="link"
          icon={<DeleteOutlined />}
          aria-label="Dokument und Spalte entfernen"
          onClick={handleDeleteClick}
        >
          {t('lea.Wizard.SynopsisWizard.Step1SelectDocument.deleteColumn')}
        </Button>
      </div>
    </>
  );
};
