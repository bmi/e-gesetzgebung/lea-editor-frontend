// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Select } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo } from '@plateg/theme';
type Props = {
  fieldName: string;
};

export const AuthorialNoteTypeFormItem: FunctionComponent<Props> = ({ fieldName = 'authorialNoteType' }) => {
  const { t } = useTranslation();

  const label = (
    <span>
      {t('lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteTypeFormItem.label')}
    </span>
  );

  const options: string[] = [
    t('lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteTypeFormItem.numbered'),
    t('lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.FormItems.AuthorialNoteTypeFormItem.custom'),
  ];

  return (
    <FormItemWithInfo label={label} name={fieldName}>
      <Select>
        {options.map((authorialNoteType, index) => (
          <Select.Option key={authorialNoteType} value={index.toString()} aria-label={authorialNoteType}>
            {authorialNoteType}
          </Select.Option>
        ))}
      </Select>
    </FormItemWithInfo>
  );
};
