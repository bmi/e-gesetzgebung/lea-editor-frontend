// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import { FormItemWithInfo } from '@plateg/theme';
import { useTranslation } from 'react-i18next';
import { Radio } from 'antd';

type Props = {
  fieldName: string;
  isPreviousVersionPinned: boolean;
};

const PinPreviousVersionFormItem: FunctionComponent<Props> = ({ fieldName, isPreviousVersionPinned }) => {
  const { t } = useTranslation();
  const label = (
    <span>{t('lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.PinPreviousVersionFormItem.label')}</span>
  );
  const radioValues = [
    {
      key: false,
      title: t(
        'lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.PinPreviousVersionFormItem.options.moveToVersionHistory',
      ),
    },
    {
      key: true,
      title: isPreviousVersionPinned
        ? t(
            'lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.PinPreviousVersionFormItem.options.remainPinned',
          )
        : t(
            'lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.PinPreviousVersionFormItem.options.pinToHomepage',
          ),
    },
  ];
  return (
    <FormItemWithInfo label={label} name={fieldName}>
      <Radio.Group className="horizontal-radios" defaultValue={false}>
        {radioValues.map(({ key, title }) => (
          <Radio id={`radio-button-${key}`} value={key} key={`radio-button-${key}`}>
            {title}
          </Radio>
        ))}
      </Radio.Group>
    </FormItemWithInfo>
  );
};

export default PinPreviousVersionFormItem;
