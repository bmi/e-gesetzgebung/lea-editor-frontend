// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';

import { ApiResponse, PropositionDTO } from '../../../api';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

type Props = {
  fieldName: string;
  propositionsResponse: ApiResponse<PropositionDTO[]>;
};

export const RegelungsvorhabenFormItem: FunctionComponent<Props> = ({
  fieldName = 'regelungsvorhaben',
  propositionsResponse,
}) => {
  const { t } = useTranslation();
  const [options, setOptions] = useState<PropositionDTO[]>([]);

  const label = (
    <>
      <span>
        {t('lea.Document.FormItems.CompoundDocumentRegelungsvorhabenFormItem.label')}
        <span className="infoRequired">
          <HiddenInfoComponent
            title={t('lea.Document.FormItems.CompoundDocumentRegelungsvorhabenFormItem.infoComponent.title')}
            text={t('lea.Document.FormItems.CompoundDocumentRegelungsvorhabenFormItem.infoComponent.text')}
          />
        </span>
      </span>
    </>
  );

  useEffect(() => {
    if (propositionsResponse.isLoading) return;
    if (!propositionsResponse.data || !Array.isArray(propositionsResponse.data)) return;
    setOptions(propositionsResponse.data);
  }, [propositionsResponse]);

  return (
    <FormItemWithInfo
      label={label}
      rules={[
        { required: true, message: t('lea.Document.FormItems.CompoundDocumentRegelungsvorhabenFormItem.requiredMsg') },
      ]}
      name={fieldName}
    >
      <SelectWrapper
        disabled={propositionsResponse.isLoading}
        placeholder={t('lea.Document.FormItems.CompoundDocumentRegelungsvorhabenFormItem.placeholder')}
        suffixIcon={<SelectDown />}
        options={options.map((item) => ({
          label: <span key={item.id}>{`${item.abbreviation} - ${item.shortTitle}`}</span>,
          value: item.id,
          title: `${item.abbreviation} - ${item.shortTitle}`,
        }))}
      />
    </FormItemWithInfo>
  );
};
