// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, SelectWrapper } from '@plateg/theme';

import {
  CompoundDocumentService,
  CompoundDocumentSummaryDTO,
  DocumentSummaryDTO,
  HomepageCompoundDocumentDTO,
} from '../../../api';
import { NumberWords } from '../../../constants';
import FormItemLabel from 'antd/lib/form/FormItemLabel';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

type Props = {
  fieldNames: string[];
  compoundDocuments: CompoundDocumentSummaryDTO[] | HomepageCompoundDocumentDTO[];
  selectedDocumentIds: string[];
  documentId: string;
  setDocumentId: React.Dispatch<React.SetStateAction<string>>;
};

export const FirstDocumentFormItem: FunctionComponent<Props> = ({
  fieldNames,
  compoundDocuments,
  selectedDocumentIds,
  documentId,
  setDocumentId,
}) => {
  const { t } = useTranslation();
  const [compoundDocumentId, setCompoundDocumentId] = useState<string>('');
  const [compoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getSummaryById(
    compoundDocumentId ?? '',
  );
  const [allDocuments, setAllDocuments] = useState<DocumentSummaryDTO[]>([]);
  const [filteredDocuments, setFilteredDocuments] = useState<DocumentSummaryDTO[]>([]);

  useEffect(() => {
    compoundDocumentId !== '' && fetchCompoundDocument();
  }, [compoundDocumentId]);

  useEffect(() => {
    if (!compoundDocumentResponse.data || compoundDocumentResponse.hasError) return;
    const allDocuments = compoundDocumentResponse.data.documents;
    setAllDocuments(allDocuments);
    const filteredDocuments = allDocuments.filter(
      (currentDocument) => !selectedDocumentIds.find((id) => currentDocument.id === id),
    );
    setFilteredDocuments(filteredDocuments);
  }, [compoundDocumentResponse]);

  useEffect(() => {
    const filteredDocuments = allDocuments.filter(
      (currentDocument) => !selectedDocumentIds.find((id) => currentDocument.id === id),
    );
    setFilteredDocuments(filteredDocuments);
  }, [selectedDocumentIds]);

  const documentLabel = <span>{t('lea.Document.FormItems.DocumentFormItem.documentLabel')}</span>;
  const compoundDocumentLabel = <span>{t('lea.Document.FormItems.DocumentFormItem.compoundDocumentLabel')}</span>;
  const documentNumber = NumberWords[0];
  const configLabel = t('lea.Document.FormItems.DocumentFormItem.headerLabel', { index: documentNumber });

  return (
    <>
      <FormItemLabel prefixCls="config" label={configLabel}></FormItemLabel>
      <FormItemWithInfo
        className="synopsis-form-item"
        label={compoundDocumentLabel}
        rules={[
          { required: compoundDocumentId === '', message: t('lea.Document.FormItems.DocumentFormItem.requiredMsg') },
        ]}
        name={`${fieldNames[0]}-firstDocument`}
      >
        <SelectWrapper
          key={`${fieldNames[0]}-firstDocument`}
          suffixIcon={<SelectDown />}
          onChange={(value: string) => {
            setCompoundDocumentId(value);
          }}
          options={compoundDocuments.map((item) => ({
            label: (
              <span key={item.id} aria-label={item.title}>
                {item.title}
              </span>
            ),
            value: item.id,
            title: item.title,
          }))}
        />
      </FormItemWithInfo>
      <FormItemWithInfo
        className="synopsis-form-item"
        label={documentLabel}
        rules={[{ required: documentId === '', message: t('lea.Document.FormItems.DocumentFormItem.requiredMsg') }]}
        name={`${fieldNames[1]}-firstDocument`}
      >
        <SelectWrapper
          key={`${fieldNames[1]}-firstDocument`}
          suffixIcon={<SelectDown />}
          disabled={!filteredDocuments.length}
          onChange={(value: string) => {
            setDocumentId(value);
          }}
          options={allDocuments.map((item) => ({
            label: (
              <span key={item.id} aria-label={item.title}>
                {item.title}
              </span>
            ),
            value: item.id,
            title: item.title,
            disabled: !filteredDocuments.find((filteredDocument) => filteredDocument.id === item.id),
          }))}
        />
      </FormItemWithInfo>
    </>
  );
};
