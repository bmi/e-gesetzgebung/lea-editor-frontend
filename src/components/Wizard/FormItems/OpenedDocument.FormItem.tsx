// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo } from '@plateg/theme';
import FormItemLabel from 'antd/lib/form/FormItemLabel';
type Props = {
  compoundDocumentName: string;
  documentName?: string;
  configLabel?: string;
};

export const OpenedDocumentFormItem: FunctionComponent<Props> = ({
  compoundDocumentName,
  documentName,
  configLabel,
}) => {
  const { t } = useTranslation();

  const compundDocumentLabel = <span>{t(`lea.Document.FormItems.OpenedDocumentFormItem.compoundDocumentLabel`)}</span>;
  const documentLabel = <span>{t(`lea.Document.FormItems.OpenedDocumentFormItem.documentLabel`)}</span>;
  return (
    <>
      {configLabel ? <FormItemLabel prefixCls="config" label={configLabel}></FormItemLabel> : <></>}
      <FormItemWithInfo label={compundDocumentLabel} rules={[{ required: false }]}>
        {compoundDocumentName}
      </FormItemWithInfo>
      {documentName ? (
        <FormItemWithInfo label={documentLabel} rules={[{ required: false }]}>
          {documentName}
        </FormItemWithInfo>
      ) : (
        <></>
      )}
    </>
  );
};
