// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './FormItems.less';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';
import { CompoundDocumentType } from '../../../api';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

type Props = {
  fieldName: string;
};

export const CompoundDocumentTypeFormItem: FunctionComponent<Props> = ({ fieldName }) => {
  const { t } = useTranslation();
  const variants = [
    { key: CompoundDocumentType.STAMMGESETZ, value: CompoundDocumentType.STAMMGESETZ, label: 'Stammgesetz' },
    { key: CompoundDocumentType.MANTELGESETZ, value: CompoundDocumentType.MANTELGESETZ, label: 'Mantelgesetz' },
    { key: CompoundDocumentType.STAMMGESETZ, value: CompoundDocumentType.STAMMVERORDNUNG, label: 'Stammverordnung' },
    {
      key: CompoundDocumentType.STAMMGESETZ,
      value: CompoundDocumentType.AENDERUNGSVERORDNUNG,
      label: 'Mantelverordnung',
    },
  ];

  const label = (
    <span>
      Variante
      <span className="infoRequired">
        <HiddenInfoComponent
          title={t('lea.Document.FormItems.CompoundDocumentTypeFormItem.infoComponent.title')}
          text={t('lea.Document.FormItems.CompoundDocumentTypeFormItem.infoComponent.text')}
        />
      </span>
    </span>
  );

  return (
    <FormItemWithInfo
      label={label}
      rules={[{ required: true, message: t('lea.Document.FormItems.CompoundDocumentTypeFormItem.requiredMsg') }]}
      name={fieldName}
    >
      <SelectWrapper
        placeholder={t('lea.Document.FormItems.CompoundDocumentTypeFormItem.placeholder')}
        disabled={false}
        suffixIcon={<SelectDown />}
        className="input-height"
        options={variants.map((variant) => ({
          label: <span key={variant.key}>{variant.label}</span>,
          value: variant.value,
          title: variant.label,
        }))}
      />
    </FormItemWithInfo>
  );
};
