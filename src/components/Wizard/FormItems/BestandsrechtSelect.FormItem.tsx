// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import { BestandsrechtResponseDTO, BestandsrechtUpdateDTO } from '../../../api/Bestandsrecht';
import { TooltipComponent } from '@plateg/theme';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';

import './BestandsrechtSelect.FormItem.less';
import { Checkbox } from 'antd';

interface Props {
  initialValue: CompoundDocumentInfo;
  bestandsrecht: BestandsrechtResponseDTO;
  setBestandsrechtLinks: React.Dispatch<React.SetStateAction<BestandsrechtUpdateDTO[]>>;
  bestandsrechtLinks: BestandsrechtUpdateDTO[];
}

export const BestandsrechtSelectFormItem: FunctionComponent<Props> = ({
  initialValue,
  bestandsrecht,
  setBestandsrechtLinks,
  bestandsrechtLinks,
}) => {
  const { t } = useTranslation();

  console.log(bestandsrecht);

  const toggleLinkCompoundDocument = () => {
    setBestandsrechtLinks(
      bestandsrechtLinks.map((bestandsrechtLink) => {
        if (bestandsrechtLink.id === bestandsrecht.id) {
          if (bestandsrechtLink.verknuepfteDokumentenMappeId !== initialValue.id) {
            bestandsrechtLink.verknuepfteDokumentenMappeId = initialValue.id;
            bestandsrecht.verknuepfteDokumentenMappeId = initialValue.id;
          } else {
            bestandsrechtLink.verknuepfteDokumentenMappeId = null;
            bestandsrecht.verknuepfteDokumentenMappeId = null;
          }
        }
        return bestandsrechtLink;
      }),
    );
  };

  return (
    <>
      <span className="bestandsrecht-title">{bestandsrecht.titelKurz}</span>
      <div className="toggle-bestandsrecht-wrapper">
        <div className="toggle-bestandsrecht-link">
          <TooltipComponent
            title={t('lea.Wizard.AssignBestandsrechtWizard.BestandsrechtChooseForm.Toggle.tooltip', {
              active: bestandsrecht.verknuepfteDokumentenMappeId === initialValue.id ? 'entfernen' : 'hinzufügen',
            })}
          >
            <Checkbox
              id="toggle-bestandsrecht-link"
              checked={bestandsrecht.verknuepfteDokumentenMappeId === initialValue.id}
              onChange={toggleLinkCompoundDocument}
              aria-label={t('lea.Wizard.AssignBestandsrechtWizard.BestandsrechtChooseForm.Toggle.ariaLabel', {
                bestandsrecht: bestandsrecht.titelKurz,
                compoundDocument: initialValue.title,
                active: bestandsrecht.verknuepfteDokumentenMappeId === initialValue.id ? 'entfernen' : 'hinzufügen',
              })}
              title={initialValue.title}
            >
              {initialValue.title}
            </Checkbox>
          </TooltipComponent>
        </div>
      </div>
    </>
  );
};
