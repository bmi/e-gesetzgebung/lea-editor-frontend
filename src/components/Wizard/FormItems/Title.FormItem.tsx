// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './FormItems.less';
import { Input } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';
type Props = {
  fieldName: string;
  currentWizard: 'Document' | 'CompoundDocument' | 'NewVersion' | 'ExportPdf';
};

export const TitleFormItem: FunctionComponent<Props> = ({ fieldName, currentWizard }) => {
  const { t } = useTranslation();

  const label = (
    <span>
      {t(`lea.Document.FormItems.${currentWizard}TitleFormItem.label`)}
      {currentWizard !== 'NewVersion' ? (
        <span className="infoRequired">
          <HiddenInfoComponent
            title={t(`lea.Document.FormItems.${currentWizard}TitleFormItem.infoComponent.title`)}
            text={t(`lea.Document.FormItems.${currentWizard}TitleFormItem.infoComponent.text`)}
          />
        </span>
      ) : (
        <></>
      )}
    </span>
  );
  return (
    <FormItemWithInfo
      name={fieldName}
      label={label}
      rules={[
        { required: true, message: t(`lea.Document.FormItems.${currentWizard}TitleFormItem.requiredMsg`) },
        () => ({
          validator(_, value: string) {
            if (value?.length > 0 && value?.trim() === '') {
              return Promise.reject(new Error(t(`lea.Document.FormItems.${currentWizard}TitleFormItem.emtyTitleMsg`)));
            }
            return Promise.resolve();
          },
        }),
      ]}
    >
      <Input
        placeholder={t(`lea.Document.FormItems.${currentWizard}TitleFormItem.placeholder`)}
        className="input-height"
      />
    </FormItemWithInfo>
  );
};
