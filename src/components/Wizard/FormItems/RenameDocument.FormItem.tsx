// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { EntityType } from '../../../api';
import { DocumentInfo } from '../../Pages/HomePage/HomePage';

type Props = {
  fieldName: string;
  documentInfo: DocumentInfo;
};

export const RenameDocumentFormItem: FunctionComponent<Props> = ({ fieldName, documentInfo }) => {
  const { t } = useTranslation();

  const label = (
    <span>
      {t('lea.Wizard.RenameDocumentWizard.title', {
        documentType: documentInfo.entity === EntityType.COMPOUND_DOCUMENT ? 'Dokumentenmappe' : 'Dokument',
      })}
    </span>
  );
  return (
    <Form.Item
      name={fieldName}
      label={label}
      rules={[
        {
          required: true,
          message: t(
            `lea.Wizard.RenameDocumentWizard.Step1RenameDocument.FormItems.RenameDocumentFormItem.requiredMsg`,
          ),
        },
        () => ({
          validator(_, value: string) {
            if (value?.length > 0 && value?.trim() === '') {
              return Promise.reject(
                new Error(
                  t(`lea.Wizard.RenameDocumentWizard.Step1RenameDocument.FormItems.RenameDocumentFormItem.requiredMsg`),
                ),
              );
            }
            return Promise.resolve();
          },
        }),
      ]}
    >
      <Input />
    </Form.Item>
  );
};
