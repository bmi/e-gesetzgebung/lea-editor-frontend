// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ChoicesCheckBox, ChoicesItemInterface } from '@lea/ui';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import { DocumentList } from '../../Pages/HomePage/HomePage';
import { DocumentType } from '../../../api';
import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';

type Props = {
  fieldName: string;
  documents: DocumentList;
  documentIdList: CheckboxValueType[];
  setDocumentIdList: Dispatch<SetStateAction<CheckboxValueType[]>>;
};

export const SelectDocumentsFormItem: FunctionComponent<Props> = ({
  fieldName,
  documents,
  documentIdList,
  setDocumentIdList,
}) => {
  const { t } = useTranslation();
  const displayDocumentType = (documentType: DocumentType) => {
    switch (documentType) {
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
        return 'Regelungstext';
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
        return 'Begründung';
      case DocumentType.VORBLATT_STAMMGESETZ:
      case DocumentType.VORBLATT_MANTELGESETZ:
      case DocumentType.VORBLATT_STAMMVERORDNUNG:
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
        return 'Vorblatt';
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
        return 'Anschreiben';
      default:
        return '';
    }
  };

  const documentsInitValues: ChoicesItemInterface[] = documents.map((document) => {
    return {
      label: document.type === DocumentType.ANLAGE ? document.title : displayDocumentType(document.type),
      value: document.id,
    };
  });

  const label = (
    <span>
      {t('lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.SelectDocumentsFormItem.label')}
      <span className="infoRequired">
        <HiddenInfoComponent
          title={t(
            'lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.SelectDocumentsFormItem.infoComponent.title',
          )}
          text={t(
            'lea.Wizard.NewVersionWizard.Step1CreateNewVersion.FormItems.SelectDocumentsFormItem.infoComponent.text',
          )}
        />
      </span>
    </span>
  );

  return (
    <FormItemWithInfo
      name={fieldName}
      label={label}
      rules={[
        {
          required: false,
        },
      ]}
    >
      <ChoicesCheckBox
        checkedList={documentIdList}
        setCheckedList={setDocumentIdList}
        choicesList={documentsInitValues}
        isVisible={true}
      />
    </FormItemWithInfo>
  );
};
