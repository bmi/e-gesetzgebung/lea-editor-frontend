// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';
//
import { Step1SelectDocument, Step2DocumentSpecifications } from './Steps';
import { CompoundDocumentService, DocumentType } from '../../../api';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';

import { Wizards } from '../WizardVisibleState';
import { useSelectedDocumentTypeDispatch } from '../useWizard';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';

type Props = {
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
};

const CreateNewDocumentWizard: FunctionComponent<Props> = ({ compoundDocumentInfo, updateTable }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.NEW_DOCUMENT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_DOCUMENT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [documentType, setDocumentType] = useState<DocumentType | undefined>(undefined);
  const [documentTitle, setDocumentTitle] = useState<string>('');
  const [, setInitialNumberOfLevels] = useState<number>(0);
  const [compoundDocumentResponse, fetchCompoundDocuments] = CompoundDocumentService.getAllTitles();
  const { setSelectedDocumentType } = useSelectedDocumentTypeDispatch();

  useEffect(() => {
    if (!wizardVisible) {
      setActivePageIndex(0);
      setDocumentType(undefined);
      setDocumentTitle('');
      setSelectedDocumentType(undefined);
    } else {
      fetchCompoundDocuments();
    }
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.CreateNewDocumentWizard.title')}
      cancelBtnText={t('lea.Wizard.CreateNewDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.CreateNewDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.CreateNewDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1SelectDocument
              name="page1"
              activePageIndex={activePageIndex}
              setActivePageIndex={setActivePageIndex}
              documentType={documentType}
              setDocumentType={setDocumentType}
              setInitialNumberOfLevels={setInitialNumberOfLevels}
              compoundDocumentInfo={compoundDocumentInfo}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Step2DocumentSpecifications
              name="page2"
              compoundDocumentInfo={compoundDocumentInfo}
              documentTitle={documentTitle}
              setDocumentTitle={setDocumentTitle}
              documentType={documentType}
              compoundDocumentResponse={compoundDocumentResponse}
              updateTable={updateTable}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.CreateNewDocumentWizard.okBtnText'),
          },
        },
        // Schritt 3 (Auswahl der Gliederungsebenen) wurde auf Anfrage entfernt.
        // Code bleibt bestehen, falls er wieder eingebunden werden soll
      ]}
    />
  );
};

export default CreateNewDocumentWizard;
