// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ApiResponse, CompoundDocumentService, CompoundDocumentTitleDTO, DocumentType } from '../../../../api';
import { TitleFormItem, DocumentStatusFormItem, OpenedDocumentFormItem } from '../../FormItems';
import { useSelectedDocumentTypeDispatch, useSelectedDocumentTypeState } from '../../useWizard';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import { displayMessage } from '@plateg/theme';
import { useAppInfoState } from '../../../App';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
const { Title } = Typography;

type Props = {
  name: string;
  documentTitle: string;
  setDocumentTitle: React.Dispatch<React.SetStateAction<string>>;
  documentType?: DocumentType;
  compoundDocumentResponse: ApiResponse<CompoundDocumentTitleDTO[]>;
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
};

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const Step2DocumentSpecifications: FunctionComponent<Props> = ({
  name,
  documentTitle,
  setDocumentTitle,
  documentType,
  compoundDocumentResponse,
  compoundDocumentInfo,
  updateTable,
}) => {
  const { t } = useTranslation();
  const { basePath } = useAppInfoState();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_DOCUMENT);
  const [createdDocumentInCompoundDocumentResponse, createdDocumentInCompoundDocument] =
    CompoundDocumentService.createDocument(
      {
        title: documentTitle ?? '',
        regelungsvorhabenId: '',
        type: documentType ?? DocumentType.REGELUNGSTEXT_STAMMGESETZ,
      },
      compoundDocumentInfo.id ?? '',
    );
  const selectedDocumentType = useSelectedDocumentTypeState();
  const { setSelectedDocumentType } = useSelectedDocumentTypeDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const [fields, setFields] = useState<FieldData[]>([
    /** inital FormValues */
    { name: ['document-status'], value: 'referentenentwurf' },
    { name: ['document-title'], value: documentTitle },
  ]);

  useEffect(() => {
    if (selectedDocumentType !== documentType) {
      setSelectedDocumentType(documentType);
      if (documentType === DocumentType.ANLAGE) {
        if (!compoundDocumentInfo.id) return;
        const compoundDocument = compoundDocumentResponse?.data?.find(
          (compoundDocument) => compoundDocument.id === compoundDocumentInfo.id,
        );
        if (compoundDocumentInfo.id !== '' && compoundDocument) {
          prefillDocumentTitle(`Anlage ${(compoundDocument.anzahlAnlagen + 1).toString()}`);
        }
      } else if (documentType) {
        let type = documentType.split('_')[0].toLowerCase();
        type = type.charAt(0).toUpperCase() + type.substring(1).replace('ue', 'ü');
        prefillDocumentTitle(type);
      }
    }
  }, []);

  useEffect(() => {
    if (!fields.length) return;
    setDocumentTitle(fields.filter((field) => field.name.includes('document-title'))[0].value as string);
  }, [fields]);

  const onFinishHandler = useCallback(() => {
    if (!documentTitle) return alert('Cannot create Document -- no Title');
    if (!documentType) return alert('Cannot create Document -- no Type');
    if (!compoundDocumentInfo.id) return alert('Cannot create Document -- No compound document');
    if (!isLoading) {
      createdDocumentInCompoundDocument();
      setIsLoading(true);
    }
  }, [documentTitle, documentType, compoundDocumentInfo.id, isLoading]);

  const renderTypes = (type: DocumentType | undefined): string => {
    switch (type) {
      case DocumentType.BEGRUENDUNG_STAMMGESETZ:
        return 'Stammgesetz: Begründung';
      case DocumentType.REGELUNGSTEXT_STAMMGESETZ:
        return 'Stammgesetz: Regelungstext';
      case DocumentType.VORBLATT_STAMMGESETZ:
        return 'Stammgesetz: Vorblatt';
      case DocumentType.ANSCHREIBEN_STAMMGESETZ:
        return 'Stammgesetz: Anschreiben';
      case DocumentType.BEGRUENDUNG_MANTELGESETZ:
        return 'Mantelgesetz: Begründung';
      case DocumentType.REGELUNGSTEXT_MANTELGESETZ:
        return 'Mantelgesetz: Regelungstext';
      case DocumentType.VORBLATT_MANTELGESETZ:
        return 'Mantelgesetz: Vorblatt';
      case DocumentType.ANSCHREIBEN_MANTELGESETZ:
        return 'Mantelgesetz: Anschreiben';
      case DocumentType.BEGRUENDUNG_STAMMVERORDNUNG:
        return 'Stammverordnung: Begründung';
      case DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG:
        return 'Stammverordnung: Regelungstext';
      case DocumentType.VORBLATT_STAMMVERORDNUNG:
        return 'Stammverordnung: Vorblatt';
      case DocumentType.ANSCHREIBEN_STAMMVERORDNUNG:
        return 'Stammverordnung: Anschreiben';
      case DocumentType.BEGRUENDUNG_MANTELVERORDNUNG:
        return 'Mantelverordnung: Begründung';
      case DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG:
        return 'Mantelverordnung: Regelungstext';
      case DocumentType.VORBLATT_MANTELVERORDNUNG:
        return 'Mantelverordnung: Vorblatt';
      case DocumentType.ANSCHREIBEN_MANTELVERORDNUNG:
        return 'Mantelverordnung: Anschreiben';
      case DocumentType.ANLAGE:
        return 'Anlage';
      default:
        return '-';
    }
  };

  useEffect(() => {
    if (!createdDocumentInCompoundDocumentResponse.isLoading) {
      setIsLoading(false);
      if (createdDocumentInCompoundDocumentResponse.hasError) {
        const compoundDocumentTitle = compoundDocumentInfo.title;
        const documentTypeName = renderTypes(documentType);
        displayMessage(
          t('lea.messages.createDocument.error', {
            compoundDocument: compoundDocumentTitle,
            documentType: documentTypeName,
          }),
          'error',
        );
      } else if (createdDocumentInCompoundDocumentResponse.data) {
        closeWizard();
        displayMessage(t('lea.messages.createDocument.success'), 'success');
        window.location.hash = `#${basePath}/compounddocument/${createdDocumentInCompoundDocumentResponse.data.id}`;
        updateTable?.(true);
      }
    }
  }, [createdDocumentInCompoundDocumentResponse]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: React.SetStateAction<FieldData[]>) => {
    setFields(allFields);
  }, []);

  const prefillDocumentTitle = (title: string) => {
    setDocumentTitle(title);
    const newFields = [...fields];
    newFields[1] = { ...newFields[1], value: title };
    setFields(newFields);
  };

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
      >
        <Title level={2}>{t('lea.Wizard.CreateNewDocumentWizard.Step2DocumentSpecifications.title')}</Title>
        <p className="ant-typography p-no-style">
          {t('lea.Wizard.CreateNewDocumentWizard.Step2DocumentSpecifications.requiredMsg')}
        </p>

        {/** Form.Item - CompoundDocumentFormItem */}
        <OpenedDocumentFormItem compoundDocumentName={compoundDocumentInfo.title ?? ''} />

        {/** Form.Item – DocumentStatusFormItem */}
        <DocumentStatusFormItem fieldName="document-status" />

        {/** Form.Item – DocumentTitleFormItem */}
        <TitleFormItem fieldName="document-title" currentWizard="Document" />
      </Form>
    </>
  );
};

export default Step2DocumentSpecifications;
