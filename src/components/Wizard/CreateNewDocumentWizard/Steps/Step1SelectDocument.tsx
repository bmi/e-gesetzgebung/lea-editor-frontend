// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentTypeFormItem } from '../../FormItems';
import { DocumentType } from '../../../../api';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';

const { Title } = Typography;

type Props = {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  documentType: DocumentType | undefined;
  setDocumentType: (documentType: DocumentType) => void;
  setInitialNumberOfLevels: (initialNumberOfLevels: number) => void;
  compoundDocumentInfo: CompoundDocumentInfo;
};

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const Step1SelectDocument: FunctionComponent<Props> = ({
  name,
  activePageIndex,
  setActivePageIndex,
  documentType,
  setDocumentType,
  compoundDocumentInfo,
  setInitialNumberOfLevels,
}) => {
  const { t } = useTranslation();

  const [fields, setFields] = useState<FieldData[]>([{ name: ['document-type'], value: documentType }]);

  const onFinishHandler = useCallback(() => {
    if (!fields.length) return;

    const documentType = fields.filter((field) => field.name.includes('document-type'))[0]
      .value as string as unknown as DocumentType;
    setDocumentType(documentType);
    setActivePageIndex(activePageIndex + 1);
    if (documentType !== DocumentType.REGELUNGSTEXT_STAMMGESETZ) {
      setInitialNumberOfLevels(0);
    }
  }, [fields]);

  const onFieldsChangeHandler = useCallback(
    (_: any, allFields: React.SetStateAction<FieldData[]>) => setFields(allFields),
    [],
  );

  return (
    <Form
      name={name}
      layout="vertical"
      fields={fields}
      onFinish={onFinishHandler}
      onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
    >
      <Title level={2}>{t('lea.Wizard.CreateNewDocumentWizard.Step1SelectDocument.title')}</Title>
      <p className="ant-typography p-no-style">
        {t('lea.Wizard.CreateNewDocumentWizard.Step1SelectDocument.requiredMsg')}
      </p>

      <DocumentTypeFormItem fieldName="document-type" compoundDocumentInfo={compoundDocumentInfo} />
    </Form>
  );
};

export default Step1SelectDocument;
