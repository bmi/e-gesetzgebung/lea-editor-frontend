// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { MultiPageModalComponent } from '@plateg/theme';
import React, { useState, FunctionComponent, useEffect } from 'react';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';
import { Form } from 'antd/lib';
import { UserEntityDTO } from '@plateg/rest-api/models';
import {
  Step1SetDocumentWriteRightsUser,
  Step2SetDocumentWriteRights,
  Step3SetDocumentWriteRights,
  Step4SetDocumentWriteRightsSummary,
} from './Steps';
import { DocumentStateType } from '../../../api';

interface Props {
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

const SetDocumentWriteRightsWizard: FunctionComponent<Props> = ({ compoundDocumentInfo, updateTable }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.SET_DOCUMENT_WRITE_RIGHTS);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_WRITE_RIGHTS);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [newWriteRightsUser, setNewWriteRightsUser] = useState<UserEntityDTO | undefined>();
  const [timeLimit, setTimeLimit] = useState<string>('');
  const [createNewVersion, setCreateNewVersion] = useState(false);
  const [remarks, setRemarks] = useState<string>('');
  const [form] = Form.useForm();

  useEffect(() => {
    if (!wizardVisible) {
      setActivePageIndex(0);
    }
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`set-document-rights-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.SetDocumentWriteRightsWizard.title', { title: compoundDocumentInfo?.title ?? '' })}
      cancelBtnText={t('lea.Wizard.SetDocumentWriteRightsWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.SetDocumentWriteRightsWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.SetDocumentWriteRightsWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1SetDocumentWriteRightsUser
              setActivePageIndex={setActivePageIndex}
              newWriteRightsUser={newWriteRightsUser}
              setNewWriteRightsUser={setNewWriteRightsUser}
              form={form}
              compoundDocumentInfo={compoundDocumentInfo}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Step2SetDocumentWriteRights
              timelimit={timeLimit}
              remarks={remarks}
              setActivePageIndex={setActivePageIndex}
              setRemarks={setRemarks}
              setTimeLimit={setTimeLimit}
            />
          ),
        },
        {
          formName: 'page3',
          content: (
            <Step3SetDocumentWriteRights
              createNewVersion={createNewVersion}
              setActivePageIndex={setActivePageIndex}
              setCreateNewVersion={setCreateNewVersion}
              compoundDocumentState={compoundDocumentInfo.compoundDocumentState ?? DocumentStateType.DRAFT}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.SetDocumentWriteRightsWizard.Step3DocumentSpecifications.okBtnText'),
          },
        },
        {
          formName: 'page4',
          content: (
            <Step4SetDocumentWriteRightsSummary
              remarks={remarks}
              timeLimit={timeLimit}
              compoundDocumentInfo={compoundDocumentInfo}
              newWriteRightsUser={newWriteRightsUser}
              createNewVersion={createNewVersion}
              updateTable={updateTable}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.SetDocumentWriteRightsWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default SetDocumentWriteRightsWizard;
