// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography, Input } from 'antd';
import React, { useEffect, useState, useCallback } from 'react';
import { FormItemWithInfo, SelectWrapper } from '@plateg/theme';
import { useTranslation } from 'react-i18next';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

const { Title } = Typography;
const { TextArea } = Input;

interface SetDocumentReadRightsProps {
  timelimit: string;
  remarks: string;
  setTimeLimit: React.Dispatch<React.SetStateAction<string>>;
  setRemarks: React.Dispatch<React.SetStateAction<string>>;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

type Options = {
  value: string;
  key: string;
  label: string;
};

export const Step2SetDocumentWriteRights = ({
  timelimit,
  remarks,
  setRemarks,
  setTimeLimit,
  setActivePageIndex,
}: SetDocumentReadRightsProps) => {
  const { t } = useTranslation();
  const dateFormatter: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  };
  const [fields, setFields] = useState<FieldData[]>([
    /** inital FormValues */
    { name: ['timeLimit'], value: timelimit },
    { name: ['remarks'], value: remarks },
  ]);

  const onFieldsChangeHandler = useCallback(
    (_: FieldData[], allFields: FieldData[]) => setFields(allFields as React.SetStateAction<FieldData[]>),
    [],
  );

  const onFinishHandler = useCallback(() => {
    setActivePageIndex((index) => ++index);
  }, []);

  useEffect(() => {
    if (!fields.length) return;
    const remarks = fields.filter((field) => field.name.includes('remarks'))[0];
    const timeLimit = fields.filter((field) => field.name.includes('timeLimit'))[0];
    setRemarks((remarks.value as string) || '');
    setTimeLimit(timeLimit.value as string);
  }, [fields]);

  const addWeeks = (date: Date, weeks: number) => {
    date.setDate(date.getDate() + 7 * weeks);
    return date;
  };

  const getOptions = (): Options[] => {
    return [
      { value: 'Ohne Befristung', label: 'Ohne Befristung', key: '1' },
      {
        value: addWeeks(new Date(), 2).toLocaleDateString('de-de', dateFormatter),
        label: `2 Wochen (Fristablauf am ${addWeeks(new Date(), 2).toLocaleDateString('de-de', dateFormatter)})`,
        key: '2',
      },
      {
        value: addWeeks(new Date(), 4).toLocaleDateString('de-de', dateFormatter),
        label: `4 Wochen (Fristablauf am ${addWeeks(new Date(), 4).toLocaleDateString('de-de', dateFormatter)})`,
        key: '3',
      },
      {
        value: addWeeks(new Date(), 6).toLocaleDateString('de-de', dateFormatter),
        label: `6 Wochen (Fristablauf am ${addWeeks(new Date(), 6).toLocaleDateString('de-de', dateFormatter)})`,
        key: '4',
      },
    ];
  };

  return (
    <>
      <Form
        name="page2"
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
      >
        <Title level={2}>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step2DocumentSpecifications.title')}</Title>
        <div>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step2DocumentSpecifications.requiredMsg')}</div>

        <FormItemWithInfo
          label={t('lea.Wizard.SetDocumentWriteRightsWizard.Step2DocumentSpecifications.timeLimit.title')}
          name="timeLimit"
          rules={[
            {
              required: true,
              message: t(`lea.Wizard.SetDocumentWriteRightsWizard.Step2DocumentSpecifications.timeLimit.requiredMsg`),
            },
          ]}
        >
          <SelectWrapper
            suffixIcon={<SelectDown />}
            placeholder="Bitte auswählen"
            options={getOptions().map((item) => ({
              label: <span key={item.key}>{item.label}</span>,
              value: item.value,
              title: item.label,
            }))}
          />
        </FormItemWithInfo>
        <FormItemWithInfo
          label={t('lea.Wizard.SetDocumentWriteRightsWizard.Step2DocumentSpecifications.remarks.title')}
          name="remarks"
        >
          <TextArea rows={6}></TextArea>
        </FormItemWithInfo>
      </Form>
    </>
  );
};
