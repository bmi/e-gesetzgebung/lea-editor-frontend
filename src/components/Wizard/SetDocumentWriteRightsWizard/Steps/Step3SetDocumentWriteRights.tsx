// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography, Checkbox } from 'antd';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { DocumentStateType } from '../../../../api';
const { Title } = Typography;

interface SetDocumentWriteRightsSummaryProps {
  compoundDocumentState: DocumentStateType;
  createNewVersion: boolean;
  setCreateNewVersion: React.Dispatch<React.SetStateAction<boolean>>;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
}

export const Step3SetDocumentWriteRights = ({
  compoundDocumentState,
  createNewVersion,
  setActivePageIndex,
  setCreateNewVersion,
}: SetDocumentWriteRightsSummaryProps) => {
  const { t } = useTranslation();

  const onFinishHandler = useCallback(() => {
    setActivePageIndex((index) => ++index);
  }, []);

  return (
    <>
      <Form name="page3" layout="vertical" onFinish={onFinishHandler}>
        <Title level={2}>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step3DocumentSpecifications.title')}</Title>
        <Checkbox
          disabled={compoundDocumentState !== DocumentStateType.DRAFT}
          type="checkbox"
          id="createNewVersion"
          aria-label={t(
            'lea.Wizard.SetDocumentWriteRightsWizard.Step3DocumentSpecifications.createNewVersion.ariaLabel',
          )}
          onChange={() => setCreateNewVersion(!createNewVersion)}
          checked={createNewVersion || compoundDocumentState !== DocumentStateType.DRAFT}
        >
          {t('lea.Wizard.SetDocumentWriteRightsWizard.Step3DocumentSpecifications.createNewVersion.label')}
        </Checkbox>
      </Form>
    </>
  );
};
