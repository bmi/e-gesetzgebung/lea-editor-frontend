// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState, useReducer } from 'react';
import { Constants, EmailsSearchComponent, GeneralFormWrapper, HiddenInfoComponent } from '@plateg/theme';
import { FreigabeList } from '@plateg/theme/src/components/emails-search/component.react';
import { FreigabeType, UserEntityDTO } from '@plateg/rest-api/models';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentPermissionDTO, CompoundDocumentService } from '../../../../api';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import { FormInstance } from 'antd/lib';
import { Wizards } from '../../WizardVisibleState';
import { useWizardVisibleState } from '../../useWizardVisible';
const { Title } = Typography;

interface EmailSearchTexts {
  searchLabel: string;
  searchDrawer:
    | {
        searchDrawerTitle: string;
        searchDrawerText: string;
      }
    | React.JSX.Element;
  searchHint: string;
  addressLabel: string;
  addressDrawer:
    | {
        addressDrawerTitle: string;
        addressDrawerText: string;
      }
    | React.JSX.Element;
}

interface SetDocumentReadRightsProps {
  form: FormInstance<any>;
  compoundDocumentInfo: CompoundDocumentInfo;
  newWriteRightsUser?: UserEntityDTO;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
  setNewWriteRightsUser: React.Dispatch<React.SetStateAction<UserEntityDTO | undefined>>;
}

export const Step1SetDocumentWriteRightsUser = ({
  form,
  compoundDocumentInfo,
  newWriteRightsUser,
  setActivePageIndex,
  setNewWriteRightsUser,
}: SetDocumentReadRightsProps) => {
  const { t } = useTranslation();
  const visible = useWizardVisibleState(Wizards.SET_DOCUMENT_WRITE_RIGHTS);
  const [compoundDocumentPermissionResponse, fetchCompoundDocumentPermission] =
    CompoundDocumentService.getPermissionsById(compoundDocumentInfo.id, 'SCHREIBEN');
  const [freigabenListProps, setFreigabenListProps] = useState<FreigabeList>();
  const [initialValues, setInitialValues] = useState<CompoundDocumentPermissionDTO[]>([]);
  const [usersList, setUsersList] = useState<UserEntityDTO[]>([]);
  const [, forceUpdate] = useReducer((x: number) => x + 1, 0);

  useEffect(() => {
    fetchCompoundDocumentPermission();
  }, []);

  useEffect(() => {
    if (compoundDocumentPermissionResponse.isLoading) return;
    if (compoundDocumentPermissionResponse.data) {
      const freigaben = compoundDocumentPermissionResponse.data.filter((freigabe) => freigabe !== null);
      setInitialValues(freigaben);
      setFreigabenListProps({
        options: [FreigabeType.Schreibrechte],
        initialVals: {
          freigabenWithType: freigaben.map((freigabe) => {
            return { [freigabe.email]: FreigabeType.Schreibrechte };
          }),
        },
        undeletableEmails: [''],
      });
    }
  }, [compoundDocumentPermissionResponse]);

  useEffect(() => {
    if (freigabenListProps && initialValues && visible) {
      const emails = newWriteRightsUser
        ? [newWriteRightsUser.email]
        : initialValues.map((freigaben) => freigaben.email);
      form.setFieldsValue({
        ['freigaben']: emails,
        ['freigabenWithType']: freigabenListProps?.initialVals.freigabenWithType,
      });
      updateFormField('freigaben', emails ?? []);
      forceUpdate();
    }
  }, [freigabenListProps, initialValues, visible]);

  const updateFormField = (fieldName: string, value: string[]) => {
    form.setFieldsValue({
      [fieldName]: value,
      ['freigabenWithType']: value.map((email) => {
        return { [email]: FreigabeType.Schreibrechte };
      }),
    });
  };

  const onFinish = () => {
    setNewWriteRightsUser(usersList[0]);
    setActivePageIndex((index) => ++index);
  };

  const getMailSearchText = (): EmailSearchTexts => {
    return {
      searchLabel: t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.search.label'),
      searchDrawer: (
        <HiddenInfoComponent
          title={t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.search.drawer.title')}
          text={t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.search.drawer.text')}
        />
      ),
      searchHint: t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.search.hint'),
      addressLabel: t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.label'),
      addressDrawer: (
        <HiddenInfoComponent
          title={t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.drawer.title')}
          text={t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.drawer.text')}
        />
      ),
    };
  };

  return freigabenListProps ? (
    <GeneralFormWrapper name="page1" layout="vertical" form={form} onFinish={onFinish}>
      <Title className="title" level={2}>
        {t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.title.label')}
      </Title>
      <p className="ant-typography p-no-style requiredMessage">
        {t('lea.Wizard.SetDocumentWriteRightsWizard.emailAddress.requiredMsg')}
      </p>
      <EmailsSearchComponent
        form={form}
        isStaticFreigabenText={true}
        name={'freigaben'}
        texts={getMailSearchText()}
        freigabeListProps={freigabenListProps}
        currentUserEmail={''}
        updateFormField={updateFormField}
        isOnlyOneUser={true}
        rules={[
          {
            pattern: Constants.EMAIL_REGEXP_PATTERN,
            message: t('enorm.vote.emailAddress.errorShouldBeEmail'),
          },
        ]}
        setUsersDataOnParent={setUsersList}
      />
    </GeneralFormWrapper>
  ) : (
    <></>
  );
};
