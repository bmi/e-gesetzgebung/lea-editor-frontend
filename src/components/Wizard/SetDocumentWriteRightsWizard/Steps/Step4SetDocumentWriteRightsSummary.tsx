// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FreigabeType, UserEntityDTO, UserEntityShortDTO } from '@plateg/rest-api';
import { Form, Typography } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import { CompoundDocumentPermissionUpdateDTO, CompoundDocumentService } from '../../../../api';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { useTranslation } from 'react-i18next';
import { EmailSelectController } from '@plateg/theme/src/components/emailsSelect/controller';
import { displayMessage } from '@plateg/theme';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';

const { Title } = Typography;

interface SetDocumentWriteRightsSummaryProps {
  timeLimit: string;
  remarks: string;
  newWriteRightsUser?: UserEntityDTO;
  createNewVersion: boolean;
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable: React.Dispatch<React.SetStateAction<boolean>>;
}
export const Step4SetDocumentWriteRightsSummary = ({
  remarks,
  timeLimit,
  newWriteRightsUser,
  compoundDocumentInfo,
  createNewVersion,
  updateTable,
}: SetDocumentWriteRightsSummaryProps) => {
  const { t } = useTranslation();
  const ctrl = new EmailSelectController();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_WRITE_RIGHTS);
  const [updateObj, setUpdateObj] = useState<CompoundDocumentPermissionUpdateDTO | null>(null);
  const [updatePermissionsResponse, setCompoundDocumentPermissions] =
    CompoundDocumentService.updateCompoundDocumentWritePermissions(compoundDocumentInfo.id, updateObj);

  const onFinishHandler = useCallback(() => {
    const initUpdateObj = {
      gid: newWriteRightsUser?.gid || '',
      email: newWriteRightsUser?.email || '',
      name: newWriteRightsUser?.name || '',
      freigabetyp: FreigabeType.Schreibrechte,
      //date function works on de
      befristung:
        timeLimit === 'Ohne Befristung'
          ? ''
          : new Date(timeLimit.split('.').reverse().toString().replace(/,/g, '-')).toISOString(),
      erstelleNeueVersionBeiDraft: createNewVersion,
      anmerkungen: remarks,
    };
    setUpdateObj(initUpdateObj);
  }, []);

  useEffect(() => {
    if (!updateObj) return;
    setCompoundDocumentPermissions();
  }, [updateObj]);

  useEffect(() => {
    if (updatePermissionsResponse.isLoading) return;
    if (updatePermissionsResponse.hasError) {
      displayMessage(t('lea.messages.SetDocumentWriteRights.error', { title: compoundDocumentInfo.title }), 'error');
    } else {
      displayMessage(
        t('lea.messages.SetDocumentWriteRights.success', { title: compoundDocumentInfo.title }),
        'success',
      );
    }
    closeWizard();
    updateTable(true);
  }, [updatePermissionsResponse]);
  return (
    <Form name="page4" onFinish={onFinishHandler}>
      <Title level={2}>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step4DocumentSpecifications.title')}</Title>

      <dl className="set-write-rights-summary">
        <dt>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step4DocumentSpecifications.editor')}</dt>
        <dd>
          {newWriteRightsUser
            ? newWriteRightsUser.name + ' ' + ctrl.getRessortString(newWriteRightsUser as unknown as UserEntityShortDTO)
            : ''}
        </dd>
        <dt>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step4DocumentSpecifications.timeLimit')}</dt>
        <dd>{timeLimit}</dd>
        <dt>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step4DocumentSpecifications.newVersion')}</dt>
        <dd>{createNewVersion ? 'Ja' : 'Nein'}</dd>
        <dt>{t('lea.Wizard.SetDocumentWriteRightsWizard.Step4DocumentSpecifications.remarks')}</dt>
        <dd>{remarks}</dd>
      </dl>
    </Form>
  );
};
