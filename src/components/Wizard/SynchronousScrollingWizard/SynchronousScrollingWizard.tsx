// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';

import { Step1SynchronousScrolling } from './Steps/Step1SynchronousScrolling';
import {
  useWizardVisibleChange,
  useWizardVisibleState,
} from '../../../lea-editor/plugin-core/editor-store/editor-wizards/useEditorWizardVisible';
import { EditorWizards } from '../../../lea-editor/plugin-core/editor-store/editor-wizards';
import { useSynchronousScrollingDispatch } from '../../../lea-editor/plugin-core';

const SynchronousScrollingWizard: FunctionComponent = () => {
  const { t } = useTranslation();
  const { setSynchronousScrollingOn } = useSynchronousScrollingDispatch();
  const { wizardVisible } = useWizardVisibleState(EditorWizards.SYNCHRONOUSSCROLLING);
  const { closeWizard } = useWizardVisibleChange(EditorWizards.SYNCHRONOUSSCROLLING);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  const handleNextClick = () => {
    closeWizard();
    setSynchronousScrollingOn();
    const synchronousScrollingSwitch = document.getElementById('synchronous-scrolling-switch');
    synchronousScrollingSwitch?.focus();
  };

  const handleChangeVisibility = () => {
    closeWizard();
    const synchronousScrollingSwitch = document.getElementById('synchronous-scrolling-switch');
    synchronousScrollingSwitch?.focus();
  };

  return (
    <MultiPageModalComponent
      key={`format-authorialNote-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={handleChangeVisibility}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.SynchronousScrollingWizard.title')}
      cancelBtnText={t('lea.Wizard.SynchronousScrollingWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.SynchronousScrollingWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.SynchronousScrollingWizard.prevBtnText')}
      pages={[
        {
          content: <Step1SynchronousScrolling />,
          nextOnClick: handleNextClick,
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.SynchronousScrollingWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default SynchronousScrollingWizard;
