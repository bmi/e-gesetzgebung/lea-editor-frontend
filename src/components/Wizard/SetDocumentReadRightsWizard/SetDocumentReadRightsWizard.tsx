// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { MultiPageModalComponent } from '@plateg/theme';
import React, { useState, FunctionComponent } from 'react';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';
import { Form } from 'antd/lib';
import { Step1SetDocumentReadRights } from './Steps';

interface Props {
  compoundDocumentInfo: CompoundDocumentInfo;
}

const SetDocumentReadRightsWizard: FunctionComponent<Props> = ({ compoundDocumentInfo }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.SET_DOCUMENT_READ_RIGHTS);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_READ_RIGHTS);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [form] = Form.useForm();

  return (
    <MultiPageModalComponent
      key={`set-document-rights-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.SetDocumentReadRightsWizard.title', { title: compoundDocumentInfo?.title ?? '' })}
      cancelBtnText={t('lea.Wizard.SetDocumentReadRightsWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.SetDocumentReadRightsWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.SetDocumentReadRightsWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: <Step1SetDocumentReadRights form={form} compoundDocumentInfo={compoundDocumentInfo} />,
          primaryInsteadNextBtn: {
            buttonText: t(`lea.Wizard.SetDocumentReadRightsWizard.okBtnText`),
          },
          nextOnClick: () => {
            form.submit();
          },
        },
      ]}
    />
  );
};

export default SetDocumentReadRightsWizard;
