// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState, useReducer } from 'react';
import {
  Constants,
  displayMessage,
  EmailsSearchComponent,
  GeneralFormWrapper,
  HiddenInfoComponent,
} from '@plateg/theme';
import { FreigabeList } from '@plateg/theme/src/components/emails-search/component.react';
import { FreigabeType, UserEntityDTO } from '@plateg/rest-api/models';
import { useTranslation } from 'react-i18next';
import {
  CompoundDocumentPermissionDTO,
  CompoundDocumentPermissionUpdateDTO,
  CompoundDocumentService,
} from '../../../../api';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import { FormInstance } from 'antd/lib';
import { Wizards } from '../../WizardVisibleState';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../../useWizardVisible';
const { Title } = Typography;

interface EmailSearchTexts {
  searchLabel: string;
  searchDrawer:
    | {
        searchDrawerTitle: string;
        searchDrawerText: string;
      }
    | React.JSX.Element;
  searchHint: string;
  addressLabel: string;
  addressDrawer:
    | {
        addressDrawerTitle: string;
        addressDrawerText: string;
      }
    | React.JSX.Element;
}

interface SetDocumentReadRightsProps {
  form: FormInstance<any>;
  compoundDocumentInfo: CompoundDocumentInfo;
}

export const Step1SetDocumentReadRights = ({ form, compoundDocumentInfo }: SetDocumentReadRightsProps) => {
  const { t } = useTranslation();
  const visible = useWizardVisibleState(Wizards.SET_DOCUMENT_READ_RIGHTS);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SET_DOCUMENT_READ_RIGHTS);
  const [isLoading, setIsLoading] = useState(false);
  const [permissions, setPermissions] = useState<CompoundDocumentPermissionUpdateDTO[] | null>(null);
  const [compoundDocumentPermissionResponse, fetchCompoundDocumentPermission] =
    CompoundDocumentService.getPermissionsById(compoundDocumentInfo.id, 'LESEN');
  const [updatePermissionsResponse, setCompoundDocumentPermissions] =
    CompoundDocumentService.updateCompoundDocumentPermissions(compoundDocumentInfo.id, permissions || []);
  const [freigabenListProps, setFreigabenListProps] = useState<FreigabeList>();
  const [initialValues, setInitialValues] = useState<CompoundDocumentPermissionDTO[]>([]);
  const [usersList, setUsersList] = useState<UserEntityDTO[]>([]);
  const [, forceUpdate] = useReducer((x: number) => x + 1, 0);

  useEffect(() => {
    fetchCompoundDocumentPermission();
  }, []);

  useEffect(() => {
    if (compoundDocumentPermissionResponse.isLoading) return;
    if (compoundDocumentPermissionResponse.data) {
      const freigaben = compoundDocumentPermissionResponse.data.filter(
        (freigabe) => freigabe?.email !== compoundDocumentInfo.createdBy?.email && freigabe !== null,
      );
      setInitialValues(freigaben);
      setFreigabenListProps({
        options: [FreigabeType.Leserechte],
        initialVals: {
          freigabenWithType: freigaben.map((freigabe) => {
            return { [freigabe.email]: FreigabeType.Leserechte };
          }),
        },
        undeletableEmails: [''],
      });
    }
  }, [compoundDocumentPermissionResponse]);

  useEffect(() => {
    if (freigabenListProps && initialValues && visible) {
      const emails = initialValues.map((freigabe) => freigabe.email);
      form.setFieldsValue({
        ['freigaben']: emails,
        ['freigabenWithType']: freigabenListProps?.initialVals.freigabenWithType,
      });
      updateFormField('freigaben', emails ?? []);
      forceUpdate();
    }
  }, [freigabenListProps, initialValues, visible]);

  const updateFormField = (fieldName: string, value: string[]) => {
    form.setFieldsValue({
      [fieldName]: value,
      ['freigabenWithType']: value.map((email) => {
        return { [email]: FreigabeType.Leserechte };
      }),
    });
  };

  const onFinish = () => {
    if (!isLoading) {
      setPermissions(
        usersList.map((user) => {
          return {
            gid: user.gid ?? '',
            email: user.email ?? '',
            name: user.name ?? '',
            freigabetyp: FreigabeType.Leserechte,
          };
        }),
      );
      setIsLoading(true);
    }
  };

  useEffect(() => {
    if (permissions && visible) {
      setCompoundDocumentPermissions();
    }
  }, [permissions, visible]);

  useEffect(() => {
    if (updatePermissionsResponse.isLoading) return;
    if (updatePermissionsResponse.hasError) {
      displayMessage(t('lea.messages.setDocumentRights.error', { title: compoundDocumentInfo.title }), 'error');
    } else {
      displayMessage(t('lea.messages.setDocumentRights.success', { title: compoundDocumentInfo.title }), 'success');
    }
    closeWizard();
  }, [updatePermissionsResponse]);

  const getMailSearchText = (): EmailSearchTexts => {
    return {
      searchLabel: t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.search.label'),
      searchDrawer: (
        <HiddenInfoComponent
          title={t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.search.drawer.title')}
          text={t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.search.drawer.text')}
        />
      ),
      searchHint: t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.search.hint'),
      addressLabel: t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.label'),
      addressDrawer: (
        <HiddenInfoComponent
          title={t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.drawer.title')}
          text={t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.drawer.text')}
        />
      ),
    };
  };

  return (
    <GeneralFormWrapper name="page1" layout="vertical" form={form} onFinish={onFinish}>
      <Title className="title" level={2}>
        {t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.title')}
      </Title>
      <p className="ant-typography p-no-style requiredMessage">
        {t('lea.Wizard.SetDocumentReadRightsWizard.emailAddress.requiredMsg')}
      </p>
      <EmailsSearchComponent
        form={form}
        ersteller={compoundDocumentInfo.createdBy as UserEntityDTO}
        isStaticFreigabenText={true}
        name={'freigaben'}
        texts={getMailSearchText()}
        freigabeListProps={freigabenListProps}
        currentUserEmail={''}
        updateFormField={updateFormField}
        rules={[
          {
            pattern: Constants.EMAIL_REGEXP_PATTERN,
            message: t('enorm.vote.emailAddress.errorShouldBeEmail'),
          },
        ]}
        setUsersDataOnParent={setUsersList}
      />
    </GeneralFormWrapper>
  );
};
