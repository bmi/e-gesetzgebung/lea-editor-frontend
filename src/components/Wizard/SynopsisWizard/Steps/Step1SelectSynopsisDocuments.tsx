// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './Step1SelectDocument.less';

import { Button, Form } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  useFirstDocumentState,
  useSynopsisDocumentsDispatch,
  useSynopsisDocumentsState,
  RegulatoryProposalService,
  DocumentDTO,
  CompoundDocumentService,
  AenderungsvergleichCreateDTO,
  useFirstDocumentDispatch,
  DocumentService,
  HomepageCompoundDocumentDTO,
  PaginationDTO,
} from '../../../../api';

import { useForm } from 'antd/lib/form/Form';
import { HinweisComponent, displayMessage, PlusOutlined } from '@plateg/theme';

import { DocumentFormItem, OpenedDocumentFormItem } from '../../FormItems';
import { SynopsisType } from '../../../../constants';
import { useAppSelector } from '../../../Store';
import { PaginierungDTOSortDirectionEnum } from '@plateg/rest-api';
import { useAppInfoState } from '../../../App';
interface Props {
  name: string;
  clickedFinishButtonRef: React.MutableRefObject<boolean>;
  synopsisHeader: string;
  regulatoryProposalId: string;
}

export interface SynopsisDocument {
  compoundDocumentId?: string;
  compoundDocumentTitle?: string;
  documentTitle?: string;
  documentId: string;
}

export type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const pagination: PaginationDTO = {
  pageNumber: 0,
  pageSize: 100000,
  sortBy: 'CREATED_AT',
  sortDirection: PaginierungDTOSortDirectionEnum.Asc,
};

const initialSynopsisDocument: SynopsisDocument = {
  compoundDocumentId: '',
  compoundDocumentTitle: '',
  documentId: '',
  documentTitle: '',
};

export const Step1SelectSynopsisDocuments: FunctionComponent<Props> = ({
  name,
  clickedFinishButtonRef,
  synopsisHeader,
  regulatoryProposalId,
}) => {
  const { t } = useTranslation();

  const [form] = useForm();
  const { document } = useFirstDocumentState();
  const user = useAppSelector((state) => state.user.user);
  const { documents } = useSynopsisDocumentsState();
  const { synopsisType } = useAppInfoState();
  const synopsisDocument = useSynopsisDocumentsDispatch();
  const [rvId, setRvId] = useState(regulatoryProposalId);
  const [compoundDocumentsResponse, fetchCompoundDocuments] = RegulatoryProposalService.getVersionHistory(
    rvId,
    pagination,
  );
  const firstDocument = useFirstDocumentDispatch();
  const [compoundDocumentId, setCompundDocumentId] = useState(document?.compoundDocumentId ?? '');
  const [aenderungsvergleichData, setAenderungsvergleichData] = useState<AenderungsvergleichCreateDTO>({
    base: '',
    versions: [],
  });
  const [compoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getById(compoundDocumentId);
  const [aenderungsvergleichResponse, fetchAenderungsvergleich] =
    DocumentService.createAenderungsvergleich(aenderungsvergleichData);
  const [currentCompundDocument, setCurrentCompoundDocument] = useState<HomepageCompoundDocumentDTO>();
  const [compoundDocuments, setCompoundDocuments] = useState<HomepageCompoundDocumentDTO[]>([]);
  const [currentIndex, setCurrentIndex] = useState<number>();
  const [synopsisDocuments, setSynopsisDocuments] = useState<SynopsisDocument[]>();
  const [selectedDocumentIds, setSelectedDocumentIds] = useState<string[]>([]);
  const [fields, setFields] = useState<FieldData[]>([{ name: ['documentId', 'compoundDocumentId'], value: undefined }]);

  useEffect(() => {
    if (!rvId && compoundDocumentId) {
      fetchCompoundDocument();
    }
  }, [document, compoundDocumentId, rvId]);

  useEffect(() => {
    if (document) {
      setCompundDocumentId(document.compoundDocumentId ?? '');
    }
  }, [document]);

  useEffect(() => {
    if (!rvId) return;
    fetchCompoundDocuments();
  }, [rvId]);

  useEffect(() => {
    if (!compoundDocumentsResponse.data || compoundDocumentsResponse.hasError || synopsisDocuments !== undefined)
      return;
    const compoundDocuments = compoundDocumentsResponse.data.dtos.content?.[0].children || [];
    const currentCompundDocument = compoundDocuments.find(
      (compoundDocument) => compoundDocument.id === document?.compoundDocumentId,
    );
    setCurrentCompoundDocument(currentCompundDocument);

    setInitialSynopsisDocuments(compoundDocuments);
    setCompoundDocuments(compoundDocuments);
  }, [compoundDocumentsResponse]);

  useEffect(() => {
    if (!compoundDocumentResponse.data || compoundDocumentResponse.hasError) return;
    setRvId(compoundDocumentResponse.data.proposition.id);
  }, [compoundDocumentResponse]);

  useEffect(() => {
    if (synopsisDocuments && synopsisDocuments.length > 0) {
      for (let i = 0; i < synopsisDocuments.length; i++) {
        form.setFieldsValue({
          [`compoundDocumentId${i}`]: synopsisDocuments[i].compoundDocumentId,
          [`documentId${i}`]: synopsisDocuments[i].documentId,
        });
      }
    }
  }, [synopsisDocuments]);

  const onFinishHandler = useCallback(() => {
    clickedFinishButtonRef.current = true;
    if (!synopsisDocuments?.length) {
      synopsisDocument.removeDocument();
      return;
    }
    const synopsisDocumentIds = synopsisDocuments.map((document) => document.documentId);
    if (synopsisType === SynopsisType.Synopsis) {
      synopsisDocument.fetchDocuments(synopsisDocumentIds);
    } else if (synopsisType === SynopsisType.Aenderungsvergleich) {
      const aenderungsvergleichData: AenderungsvergleichCreateDTO = {
        base: document?.id ?? '',
        versions: synopsisDocumentIds,
      };
      setAenderungsvergleichData(aenderungsvergleichData);
    }
  }, [synopsisDocuments]);

  useEffect(() => {
    if (aenderungsvergleichData.base !== '') fetchAenderungsvergleich();
  }, [aenderungsvergleichData]);

  useEffect(() => {
    if (!aenderungsvergleichResponse.isLoading) {
      if (aenderungsvergleichResponse.hasError) {
        displayMessage(t('lea.messages.createAenderungsvergleich.error'), 'error');
      } else if (aenderungsvergleichResponse.data) {
        const data = aenderungsvergleichResponse.data;
        // extract the first document to overwrite it with new document
        const document = data.document;
        synopsisDocument.removeDocument();
        firstDocument.setDocument(document);
      }
    }
  }, [aenderungsvergleichResponse]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => setFields(allFields), []);

  useEffect(() => {
    if (!fields.length) return;
    if (currentIndex !== undefined) {
      if (synopsisDocuments && synopsisDocuments.length <= currentIndex) return;
      const previousDocumentId = synopsisDocuments?.[currentIndex]?.documentId;
      const documentId = fields.filter((field) => field.name.includes(`documentId${currentIndex}`))[0]?.value as string;
      const compoundDocumentId = fields.filter((field) => field.name.includes(`compoundDocumentId${currentIndex}`))[0]
        ?.value as string;
      const compoundDocument = compoundDocuments.find((compoundDocument) => compoundDocument.id === compoundDocumentId);
      const newDocument = compoundDocument?.children?.find(
        (document) => document.id === documentId,
      ) as unknown as DocumentDTO;
      const newSynopsisDocuments = synopsisDocuments || [];
      if (newSynopsisDocuments[currentIndex]) {
        newSynopsisDocuments[currentIndex] = {
          compoundDocumentId,
          compoundDocumentTitle: compoundDocument?.title,
          documentId,
          documentTitle: newDocument?.title,
        };
      } else {
        newSynopsisDocuments.push({
          compoundDocumentId,
          compoundDocumentTitle: compoundDocument?.title,
          documentId,
          documentTitle: newDocument?.title,
        });
      }
      setSynopsisDocuments(newSynopsisDocuments);

      if (previousDocumentId !== documentId) {
        const allSelectedDocumentIds = newSynopsisDocuments
          ? newSynopsisDocuments.map((document) => document.documentId)
          : [];
        document?.id && allSelectedDocumentIds.push(document?.id);
        setSelectedDocumentIds(allSelectedDocumentIds);
      }
    }
  }, [fields]);

  const handleDeleteDocumentClick = (deleteIndex: number, deleteId: string) => {
    setSynopsisDocuments(synopsisDocuments?.filter((_, index) => index !== deleteIndex));
    setSelectedDocumentIds(selectedDocumentIds?.filter((id) => id !== deleteId));
  };

  const handleAddColumn = () => {
    const newSynopsisDocument: SynopsisDocument = {
      compoundDocumentId: '',
      compoundDocumentTitle: '',
      documentId: '',
      documentTitle: '',
    };
    const oldSynopsisDocuments = synopsisDocuments || [];
    setSynopsisDocuments([...oldSynopsisDocuments, newSynopsisDocument]);
  };

  const setInitialSynopsisDocuments = (initialCompoundDocuments: HomepageCompoundDocumentDTO[]) => {
    const updatedSynopsisDocuments = documents?.map((synopsisDocument) => {
      const compoundDocument = initialCompoundDocuments?.find(
        (document) => document.id === synopsisDocument.compoundDocumentId,
      );
      const document: SynopsisDocument = {
        compoundDocumentTitle: compoundDocument?.title,
        compoundDocumentId: compoundDocument?.id,
        documentId: synopsisDocument.id,
        documentTitle: synopsisDocument.title,
      };
      return document;
    });

    const initialSynopsisDocuments =
      updatedSynopsisDocuments.length > 0 ? updatedSynopsisDocuments : [initialSynopsisDocument];

    const initialSelectedDocumentIds = initialSynopsisDocuments.map((synopsisDocument) => synopsisDocument.documentId);
    document && initialSelectedDocumentIds.push(document.id);
    setSelectedDocumentIds(initialSelectedDocumentIds);

    setSynopsisDocuments(initialSynopsisDocuments);
  };

  return (
    <>
      <Form
        form={form}
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="synopsisWizard-step1"
      >
        <p className="ant-typography p-no-style requiredMessage">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step1CompoundDocumentSpecifications.requiredMsg')}
        </p>
        <HinweisComponent
          title={synopsisHeader}
          content={t(
            `lea.Wizard.SynopsisWizard.Step1SelectDocument.${
              synopsisHeader === 'Parallelansicht' ? 'noteTextSynopsis' : 'noteTextAenderungsvergleich'
            }`,
          )}
        />
        <div className="column-configuration">
          <OpenedDocumentFormItem
            compoundDocumentName={currentCompundDocument?.title || ''}
            documentName={document?.title || ''}
            configLabel={t('lea.Document.FormItems.DocumentFormItem.headerLabel', { index: 'ersten' })}
          />

          {synopsisDocuments?.map((synopsisDocument: SynopsisDocument, index: number) => (
            <DocumentFormItem
              key={synopsisDocument.compoundDocumentId}
              fieldNames={['compoundDocumentId', 'documentId']}
              synopsisDocument={synopsisDocument}
              compoundDocuments={compoundDocuments}
              index={index}
              currentIndex={currentIndex === undefined ? -1 : currentIndex}
              setCurrentIndex={setCurrentIndex}
              handleDeleteDocumentClick={handleDeleteDocumentClick}
              selectedDocumentIds={selectedDocumentIds || []}
              setFields={setFields}
            />
          ))}
          <Button
            type="link"
            hidden={
              (user?.dto.gid === document?.createdBy.gid && !compoundDocumentsResponse.data) ||
              compoundDocuments.length === 0
            }
            icon={<PlusOutlined />}
            aria-label="Weitere Spalte hinzufügen"
            className="add-document-button"
            onClick={handleAddColumn}
          >
            {t('lea.Wizard.SynopsisWizard.Step1SelectDocument.addColumn')}
          </Button>
        </div>
      </Form>
    </>
  );
};
