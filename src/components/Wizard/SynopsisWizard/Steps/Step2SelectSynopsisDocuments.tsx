// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './Step1SelectDocument.less';

import { Button, Form } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  useFirstDocumentState,
  RegulatoryProposalService,
  DocumentDTO,
  CompoundDocumentService,
  HomepageCompoundDocumentDTO,
  PaginationDTO,
} from '../../../../api';

import { useForm } from 'antd/lib/form/Form';
import { HinweisComponent } from '@plateg/theme';

import { DocumentFormItem, FirstDocumentFormItem } from '../../FormItems';
import { useAppSelector } from '../../../Store';
import { PaginierungDTOSortDirectionEnum } from '@plateg/rest-api';
import { FieldData, SynopsisDocument } from './Step1SelectSynopsisDocuments';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
interface Props {
  name: string;
  clickedFinishButtonRef: React.MutableRefObject<boolean>;
  synopsisHeader: string;
  regulatoryProposalId: string;
}

const pagination: PaginationDTO = {
  pageNumber: 0,
  pageSize: 100000,
  sortBy: 'CREATED_AT',
  sortDirection: PaginierungDTOSortDirectionEnum.Asc,
};

const firstInitialSynopsisDocument: SynopsisDocument = {
  compoundDocumentId: '',
  compoundDocumentTitle: '',
  documentId: '',
  documentTitle: '',
};

export const Step2SelectSynopsisDocuments: FunctionComponent<Props> = ({
  name,
  clickedFinishButtonRef,
  synopsisHeader,
  regulatoryProposalId,
}) => {
  const { t } = useTranslation();

  const [form] = useForm();
  const { document } = useFirstDocumentState();
  const user = useAppSelector((state) => state.user.user);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SYNOPSIS);
  const [rvId, setRvId] = useState(regulatoryProposalId);
  const [compoundDocumentsResponse, fetchCompoundDocuments] = RegulatoryProposalService.getVersionHistory(
    rvId,
    pagination,
  );
  const [compoundDocumentId, setCompundDocumentId] = useState(document?.compoundDocumentId ?? '');
  const [compoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getById(compoundDocumentId);
  const [compoundDocuments, setCompoundDocuments] = useState<HomepageCompoundDocumentDTO[]>([]);
  const [currentIndex, setCurrentIndex] = useState<number>();
  const [synopsisDocuments, setSynopsisDocuments] = useState<SynopsisDocument[]>();
  const [selectedDocumentIds, setSelectedDocumentIds] = useState<string[]>([]);
  const [firstSynopsisDocumentId, setFirstSynopsisDocumentId] = useState<string>('');
  const [previousFirstSynopsisDocumentId, setPreviousFirstSynopsisDocumentId] = useState<string>('');
  const [fields, setFields] = useState<FieldData[]>([{ name: ['documentId', 'compoundDocumentId'], value: undefined }]);

  useEffect(() => {
    if (!rvId && compoundDocumentId) {
      fetchCompoundDocument();
    }
  }, [document, compoundDocumentId, rvId]);

  useEffect(() => {
    if (document) {
      setCompundDocumentId(document.compoundDocumentId ?? '');
    }
  }, [document]);

  useEffect(() => {
    if (!rvId) return;
    fetchCompoundDocuments();
  }, [rvId]);

  useEffect(() => {
    if (!compoundDocumentsResponse.data || compoundDocumentsResponse.hasError || synopsisDocuments !== undefined)
      return;
    const compoundDocuments = compoundDocumentsResponse.data.dtos.content?.[0].children || [];
    setSelectedDocumentIds([firstInitialSynopsisDocument.documentId]);
    setSynopsisDocuments([firstInitialSynopsisDocument]);
    setCompoundDocuments(compoundDocuments);
  }, [compoundDocumentsResponse]);

  useEffect(() => {
    if (!compoundDocumentResponse.data || compoundDocumentResponse.hasError) return;
    setRvId(compoundDocumentResponse.data.proposition.id);
  }, [compoundDocumentResponse]);

  useEffect(() => {
    if (synopsisDocuments && synopsisDocuments.length > 0) {
      for (let i = 0; i < synopsisDocuments.length; i++) {
        form.setFieldsValue({
          [`compoundDocumentId${i}`]: synopsisDocuments[i].compoundDocumentId,
          [`documentId${i}`]: synopsisDocuments[i].documentId,
        });
      }
    }
  }, [synopsisDocuments]);

  const onFinishHandler = useCallback(() => {
    clickedFinishButtonRef.current = true;
    const synopsisDocumentIds = synopsisDocuments?.map((document) => document.documentId);
    const synopsisIds = JSON.stringify(synopsisDocumentIds);
    const pathArray = location.hash.split('/');
    window.open(
      `${location.origin}${location.pathname}${pathArray[0]}/${pathArray[1]}/${pathArray[2]}/${firstSynopsisDocumentId}?synopsisDocuments=${encodeURIComponent(synopsisIds)}`,
    );
    closeWizard();
  }, [synopsisDocuments, firstSynopsisDocumentId]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => setFields(allFields), []);

  useEffect(() => {
    if (!fields.length) return;
    if (currentIndex !== undefined) {
      if (synopsisDocuments && synopsisDocuments.length <= currentIndex) return;
      const previousDocumentId = synopsisDocuments?.[currentIndex]?.documentId;
      const documentId = fields.filter((field) => field.name.includes(`documentId${currentIndex}`))[0]?.value as string;
      const compoundDocumentId = fields.filter((field) => field.name.includes(`compoundDocumentId${currentIndex}`))[0]
        ?.value as string;
      const compoundDocument = compoundDocuments.find((compoundDocument) => compoundDocument.id === compoundDocumentId);
      const newDocument = compoundDocument?.children?.find(
        (document) => document.id === documentId,
      ) as unknown as DocumentDTO;
      const newSynopsisDocuments = synopsisDocuments || [];
      if (newSynopsisDocuments[currentIndex]) {
        newSynopsisDocuments[currentIndex] = {
          compoundDocumentId,
          compoundDocumentTitle: compoundDocument?.title,
          documentId,
          documentTitle: newDocument?.title,
        };
      } else {
        newSynopsisDocuments.push({
          compoundDocumentId,
          compoundDocumentTitle: compoundDocument?.title,
          documentId,
          documentTitle: newDocument?.title,
        });
      }
      setSynopsisDocuments(newSynopsisDocuments);

      if (previousDocumentId !== documentId) {
        const allSelectedDocumentIds = newSynopsisDocuments
          ? newSynopsisDocuments.map((document) => document.documentId)
          : [];
        allSelectedDocumentIds.push(firstSynopsisDocumentId);
        setSelectedDocumentIds(allSelectedDocumentIds);
      }
    }
  }, [fields]);

  useEffect(() => {
    const allSelectedDocumentIds = [...selectedDocumentIds];

    if (previousFirstSynopsisDocumentId) {
      const idx = allSelectedDocumentIds.indexOf(previousFirstSynopsisDocumentId);
      allSelectedDocumentIds.splice(idx, 1);
      allSelectedDocumentIds.push(firstSynopsisDocumentId);
      setSelectedDocumentIds(allSelectedDocumentIds);
    } else {
      allSelectedDocumentIds.push(firstSynopsisDocumentId);
      setSelectedDocumentIds(allSelectedDocumentIds);
    }
    setPreviousFirstSynopsisDocumentId(firstSynopsisDocumentId);
  }, [firstSynopsisDocumentId]);

  const handleDeleteDocumentClick = (deleteIndex: number, deleteId: string) => {
    setSynopsisDocuments(synopsisDocuments?.filter((_, index) => index !== deleteIndex));
    setSelectedDocumentIds(selectedDocumentIds?.filter((id) => id !== deleteId));
  };

  const handleAddColumn = () => {
    const newSynopsisDocument: SynopsisDocument = {
      compoundDocumentId: '',
      compoundDocumentTitle: '',
      documentId: '',
      documentTitle: '',
    };
    const oldSynopsisDocuments = synopsisDocuments || [];
    setSynopsisDocuments([...oldSynopsisDocuments, newSynopsisDocument]);
  };

  return (
    <>
      <Form
        form={form}
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="synopsisWizard-step1"
      >
        <p className="ant-typography p-no-style requiredMessage">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step1CompoundDocumentSpecifications.requiredMsg')}
        </p>
        <HinweisComponent
          title={synopsisHeader}
          content={t(
            `lea.Wizard.SynopsisWizard.Step1SelectDocument.${
              synopsisHeader === 'Parallelansicht' ? 'noteTextSynopsis' : 'noteTextAenderungsvergleich'
            }`,
          )}
        />
        <div className="column-configuration">
          <FirstDocumentFormItem
            key={'firstSynopsisDocument'}
            fieldNames={['compoundDocumentId', 'documentId']}
            compoundDocuments={compoundDocuments}
            selectedDocumentIds={selectedDocumentIds || []}
            documentId={firstSynopsisDocumentId}
            setDocumentId={setFirstSynopsisDocumentId}
          />
          {synopsisDocuments?.map((synopsisDocument: SynopsisDocument, index: number) => (
            <DocumentFormItem
              key={synopsisDocument.compoundDocumentId}
              fieldNames={['compoundDocumentId', 'documentId']}
              synopsisDocument={synopsisDocument}
              compoundDocuments={compoundDocuments}
              index={index}
              currentIndex={currentIndex === undefined ? -1 : currentIndex}
              setCurrentIndex={setCurrentIndex}
              handleDeleteDocumentClick={handleDeleteDocumentClick}
              selectedDocumentIds={selectedDocumentIds || []}
              setFields={setFields}
            />
          ))}
          <Button
            type="link"
            hidden={
              (user?.dto.gid === document?.createdBy.gid && !compoundDocumentsResponse.data) ||
              compoundDocuments.length === 0
            }
            aria-label="Weitere Spalte hinzufügen"
            onClick={handleAddColumn}
          >
            {t('lea.Wizard.SynopsisWizard.Step1SelectDocument.addColumn')}
          </Button>
        </div>
      </Form>
    </>
  );
};
