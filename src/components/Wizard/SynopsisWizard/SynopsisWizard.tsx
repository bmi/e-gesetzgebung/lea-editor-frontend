// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';
import { Step1SelectSynopsisDocuments, Step2SelectSynopsisDocuments } from './Steps';

import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { useFirstDocumentDispatch, useFirstDocumentState, useSynopsisDocumentsState } from '../../../api';
import { useHistory, useLocation } from 'react-router';
import { useAppInfoDispatch, useAppInfoState } from '../../App';
import { SynopsisType } from '../../../constants';
import { DocumentInfo } from '../../Pages/HomePage/HomePage';
import { ParallelViewState } from '../../Pages/DocumentPage/DocumentPage';

interface Props {
  documentInfo: DocumentInfo;
}

export const SynopsisWizard: FunctionComponent<Props> = ({ documentInfo }) => {
  const { t } = useTranslation();
  let currentPathName = useLocation().pathname;
  currentPathName = currentPathName.endsWith('/') ? currentPathName.slice(0, -1) : currentPathName;
  const history = useHistory();
  const wizardVisible = useWizardVisibleState(Wizards.SYNOPSIS);
  const { basePath } = useAppInfoState();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.SYNOPSIS);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const { documents } = useSynopsisDocumentsState();
  const clickedFinishButtonRef = useRef<boolean>(false);
  const { synopsisType } = useAppInfoState();
  const [synopsisHeader, setSynopsisHeader] = useState<string>('Parallelansicht');
  const { setSynopsisType } = useAppInfoDispatch();
  const { document } = useFirstDocumentState();
  const firstDocument = useFirstDocumentDispatch();

  useEffect(() => {
    clickedFinishButtonRef.current = false;
    if (!wizardVisible && activePageIndex > 0) setActivePageIndex(0);
    setSynopsisHeader(
      synopsisType === SynopsisType.Synopsis || synopsisType === SynopsisType.NewSynopsis
        ? 'Parallelansicht'
        : 'Änderungsvergleich',
    );
  }, [wizardVisible]);

  useEffect(() => {
    if (wizardVisible && (currentPathName === basePath || currentPathName.includes('versionHistory'))) {
      document?.id &&
        history.push({
          pathname: `${basePath}/document/${document.id}`,
          state: {
            fromParallelViewWizard: true,
          } as ParallelViewState,
        });
    }
    return () => {
      if (clickedFinishButtonRef.current) {
        closeWizard();
        clickedFinishButtonRef.current = false;
      }
    };
  }, [documents]);

  const handleIsVisible = (isVisible: boolean) => {
    if (!isVisible) {
      closeWizard();
      setSynopsisType(SynopsisType.None);
      if (currentPathName === basePath) {
        firstDocument.removeDocument();
      }
    }
  };

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={handleIsVisible}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.SynopsisWizard.title', { header: synopsisHeader })}
      cancelBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content:
            synopsisType === SynopsisType.NewSynopsis ? (
              <Step2SelectSynopsisDocuments
                name="page1"
                clickedFinishButtonRef={clickedFinishButtonRef}
                synopsisHeader={synopsisHeader}
                regulatoryProposalId={documentInfo.regulatoryProposalId ?? ''}
              />
            ) : (
              <Step1SelectSynopsisDocuments
                name="page1"
                clickedFinishButtonRef={clickedFinishButtonRef}
                synopsisHeader={synopsisHeader}
                regulatoryProposalId={documentInfo.regulatoryProposalId ?? ''}
              />
            ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.SynopsisWizard.okBtnText', { header: synopsisHeader }),
          },
        },
      ]}
    />
  );
};
