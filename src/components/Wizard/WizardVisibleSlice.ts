// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { InitialWizardVisibleState, Wizards } from './WizardVisibleState';

const wizardVisibleSlice = createSlice({
  name: 'wizardVisible',
  initialState: InitialWizardVisibleState,
  reducers: {
    openWizard(state, action: PayloadAction<Wizards>) {
      state[action.payload] = true;
    },
    closeWizard(state, action: PayloadAction<Wizards>) {
      state[action.payload] = false;
    },
  },
});

export const { openWizard, closeWizard } = wizardVisibleSlice.actions;
export const WizardVisibleReducer = wizardVisibleSlice.reducer;
