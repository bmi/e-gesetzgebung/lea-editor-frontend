// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';
import { Step1SelectCompoundDocument } from './Steps';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';

type Props = {
  compoundDocumentInfo: CompoundDocumentInfo;
};

const ExportCompoundDocumentWizard: FunctionComponent<Props> = ({ compoundDocumentInfo }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.EXPORT_COMPOUNDDOCUMENT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EXPORT_COMPOUNDDOCUMENT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  useEffect(() => {
    if (!wizardVisible && activePageIndex > 0) setActivePageIndex(0);
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.ExportCompoundDocumentWizard.title')}
      cancelBtnText={t('lea.Wizard.ExportCompoundDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.ExportCompoundDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.ExportCompoundDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: <Step1SelectCompoundDocument name="page1" compoundDocument={compoundDocumentInfo} />,
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.ExportCompoundDocumentWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default ExportCompoundDocumentWizard;
