// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './Step1SelectCompoundDocument.less';

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CompoundDocumentService } from '../../../../api';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage } from '@plateg/theme';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';

const { Title } = Typography;
interface Props {
  name: string;
  compoundDocument: CompoundDocumentInfo;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

export const Step1SelectCompoundDocument: FunctionComponent<Props> = ({ name, compoundDocument }) => {
  const { t } = useTranslation();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EXPORT_COMPOUNDDOCUMENT);
  const [exportResponse, fetchExport] = CompoundDocumentService.exportCompoundDocument(compoundDocument.id);
  const [isLoading, setIsLoading] = useState(false);

  const [fields, setFields] = useState<FieldData[]>([{ name: ['compoundDocument'], value: undefined }]);

  const onFinishHandler = useCallback(() => {
    if (!isLoading) {
      fetchExport();
      setIsLoading(true);
    }
  }, [isLoading]);

  useEffect(() => {
    if (!exportResponse.isLoading) {
      setIsLoading(false);
      if (exportResponse.hasError) {
        displayMessage(t('lea.messages.exportCompoundDocument.error'), 'error');
        return;
      } else if (exportResponse.data) {
        const link = document.createElement('a');
        const currentTime = new Date();
        const currentYear: string = currentTime.getFullYear().toString();
        const currentMonth: string = ('0' + (currentTime.getMonth() + 1).toString()).slice(-2);
        const currentDay: string = ('0' + currentTime.getDate().toString()).slice(-2);
        const currentHours: string = ('0' + currentTime.getHours().toString()).slice(-2);
        const currentMinutes: string = ('0' + currentTime.getMinutes().toString()).slice(-2);
        link.download =
          currentYear +
          currentMonth +
          currentDay +
          '_' +
          currentHours +
          currentMinutes +
          '_Export_' +
          compoundDocument.title +
          '.zip';
        const zipDownload = exportResponse.data;
        link.href = URL.createObjectURL(zipDownload);
        link.click();
        link.remove();
        closeWizard();
        displayMessage(t('lea.messages.exportCompoundDocument.success'), 'success');
      }
    }
  }, [exportResponse]);

  const onFieldsChangeHandler = useCallback(
    (_: any, allFields: React.SetStateAction<FieldData[]>) => setFields(allFields),
    [],
  );

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="exportCompoundDocumentWizard-step1"
      >
        <Title level={3}>
          {t('lea.Wizard.ExportCompoundDocumentWizard.Step1SelectCompoundDocument.title', {
            title: compoundDocument.title,
          })}
        </Title>
      </Form>
    </>
  );
};
