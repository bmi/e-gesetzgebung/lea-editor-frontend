// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentType } from '../../api';
import { useAppDispatch, useAppSelector } from '../Store';
import { setSelectedDocumentType } from './WizardSlice';

type UseSelectedDocumentTypeDispatch = {
  setSelectedDocumentType: (documentType?: DocumentType) => void;
};

export const useSelectedDocumentTypeState = (): DocumentType | undefined => {
  return useAppSelector((state) => state.editorStatic.wizard.selectedDocumentType);
};

export const useSelectedDocumentTypeDispatch = (): UseSelectedDocumentTypeDispatch => {
  const dispatch = useAppDispatch();

  const setSelectedDocumentTypeCallback = (documentType?: DocumentType) => {
    dispatch(setSelectedDocumentType(documentType));
  };

  return { setSelectedDocumentType: setSelectedDocumentTypeCallback };
};
