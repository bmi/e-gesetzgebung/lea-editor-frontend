// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './Step1FormatAuthorialNote.less';

import { Form } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AuthorialNoteMarkerFormItem, AuthorialNoteTypeFormItem } from '../../FormItems';
import Title from 'antd/lib/typography/Title';
import { AuthorialNoteCommands } from '../../../../lea-editor/plugins/authorialNote/AuthorialNoteCommands';
import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { useWizardVisibleChange } from '../../../../lea-editor/plugin-core/editor-store/editor-wizards/useEditorWizardVisible';

import { useAuthorialNotePageChange } from '../../../../lea-editor/plugin-core/editor-store/workspace';
import { EditorWizards } from '../../../../lea-editor/plugin-core/editor-store/editor-wizards';
interface Props {
  name: string;
  editor: Editor;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

export const Step1FormatAuthorialNote: FunctionComponent<Props> = ({ name, editor }) => {
  const { t } = useTranslation();
  const { closeWizard } = useWizardVisibleChange(EditorWizards.AUTHORIALNOTE);
  const { openAuthorialNotePage } = useAuthorialNotePageChange();
  const [authorialNoteType, setAuthorialNoteType] = useState<string>('');

  const [fields, setFields] = useState<FieldData[]>([
    { name: ['authorialNoteType'], value: '0' },
    { name: ['authorialNoteMarker'], value: '' },
  ]);

  const onFinishHandler = useCallback(() => {
    if (authorialNoteType === '') return;
    openAuthorialNotePage();
    ReactEditor.focus(editor as ReactEditor);
    closeWizard();
    switch (authorialNoteType) {
      case '0': {
        AuthorialNoteCommands.addNumberedAuthorialNote(editor);
        break;
      }
      case '1': {
        const marker = fields.filter((field) => field.name.includes('authorialNoteMarker'))[0].value as string;
        AuthorialNoteCommands.addCustomAuthorialNote(editor, marker);
        break;
      }
      default:
        return;
    }
  }, [fields]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => {
    if (allFields.find((field) => field.name[0] === 'authorialNoteMarker')) {
      const index = allFields.findIndex((field) => field.name[0] === 'authorialNoteMarker');
      allFields[index].value = (allFields[index].value as string).replace(/\s/g, '');
    }
    setFields(allFields as React.SetStateAction<FieldData[]>);
  }, []);

  useEffect(() => {
    if (!fields.length) return;
    const authorialNoteTypeNew = fields.filter((field) => field.name.includes('authorialNoteType'))[0].value as string;
    setAuthorialNoteType(authorialNoteTypeNew);
  }, [fields]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_: any, allFields: any) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="format-authorialNote-step1"
      >
        <Title className="title" level={2}>
          {t('lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.title')}
        </Title>
        <p className="ant-typography p-no-style requiredMessage">
          {t('lea.Wizard.FormatAuthorialNoteWizard.Step1FormatAuthorialNote.requiredMsg')}
        </p>
        <AuthorialNoteTypeFormItem fieldName="authorialNoteType" />
        {authorialNoteType === '1' && <AuthorialNoteMarkerFormItem fieldName="authorialNoteMarker" />}
      </Form>
    </>
  );
};
