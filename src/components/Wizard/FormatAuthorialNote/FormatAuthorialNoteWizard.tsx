// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';

import { Step1FormatAuthorialNote } from './Steps/Step1FormatAuthorialNote';
import { Editor } from 'slate';
import { useWizardVisibleChange, useWizardVisibleState, EditorWizards } from '../../../lea-editor/plugin-core';

type Props = {
  editor: Editor;
};

const FormatAuthorialNoteWizard: FunctionComponent<Props> = ({ editor }) => {
  const { t } = useTranslation();
  const { wizardVisible } = useWizardVisibleState(EditorWizards.AUTHORIALNOTE);
  const { closeWizard } = useWizardVisibleChange(EditorWizards.AUTHORIALNOTE);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  return (
    <MultiPageModalComponent
      key={`format-authorialNote-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.FormatAuthorialNoteWizard.title')}
      cancelBtnText={t('lea.Wizard.FormatAuthorialNoteWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.FormatAuthorialNoteWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.FormatAuthorialNoteWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: <Step1FormatAuthorialNote name="page1" editor={editor} />,
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.FormatAuthorialNoteWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default FormatAuthorialNoteWizard;
