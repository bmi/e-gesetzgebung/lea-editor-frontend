// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useState } from 'react';
import { useWizardVisibleState, useWizardVisibleDispatch } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { useTranslation } from 'react-i18next';
import { displayMessage, MultiPageModalComponent } from '@plateg/theme';
import { Step1AssignBestandsrecht } from './Steps';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';
import { BestandsrechtResponseDTO, BestandsrechtService } from '../../../api/Bestandsrecht';

interface Props {
  compoundDocumentInfo: CompoundDocumentInfo;
}

export const AssignBestandsrechtWizard: FunctionComponent<Props> = ({ compoundDocumentInfo }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.ASSIGN_BESTANDSRECHT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.ASSIGN_BESTANDSRECHT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [bestandsrechte, setBestandsrechte] = useState<BestandsrechtResponseDTO[]>();
  const [bestandsrechtResponse, fetchBestandsrecht] = BestandsrechtService.getAll(compoundDocumentInfo.id ?? '');

  useEffect(() => {
    if (bestandsrechtResponse.isLoading) return;
    if (bestandsrechtResponse.hasError) {
      displayMessage(t('lea.messages.assignBestandsrecht.loadBestandsrecht.error'), 'error');
      return;
    }
    if (bestandsrechtResponse.data) {
      setBestandsrechte(bestandsrechtResponse.data);
    }
  }, [bestandsrechtResponse]);

  useEffect(() => {
    if (wizardVisible) {
      compoundDocumentInfo.id && fetchBestandsrecht();
    }
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`assign-bestandsrecht-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.AssignBestandsrechtWizard.title')}
      cancelBtnText={t('lea.Wizard.AssignBestandsrechtWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.AssignBestandsrechtWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.AssignBestandsrechtWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1AssignBestandsrecht
              name="page1"
              bestandsrechte={bestandsrechte ?? []}
              initialCompoundDocument={compoundDocumentInfo}
            />
          ),
          primaryInsteadNextBtn: { buttonText: t('lea.Wizard.AssignBestandsrechtWizard.okBtnText') },
        },
      ]}
    />
  );
};
