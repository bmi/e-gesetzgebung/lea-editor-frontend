// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, List, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { BestandsrechtResponseDTO, BestandsrechtService, BestandsrechtUpdateDTO } from '../../../../api/Bestandsrecht';
import { BestandsrechtSelectFormItem } from '../../FormItems';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage, ExclamationCircleFilled, HiddenInfoComponent } from '@plateg/theme';

const { Title } = Typography;
import './Step1AssignBestandsrecht.less';

interface Props {
  name: string;
  bestandsrechte: BestandsrechtResponseDTO[];
  initialCompoundDocument: CompoundDocumentInfo;
}

export const Step1AssignBestandsrecht: FunctionComponent<Props> = ({
  name,
  bestandsrechte,
  initialCompoundDocument,
}) => {
  const { t } = useTranslation();
  const [bestandsrechtLinks, setBestandsrechtLinks] = useState<BestandsrechtUpdateDTO[]>([]);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.ASSIGN_BESTANDSRECHT);
  const [isLoading, setIsLoading] = useState(false);
  const [updateBestandsrechtResponse, updateBestandsrecht] = BestandsrechtService.updateBestandrechtLinks(
    initialCompoundDocument.id ?? '',
    bestandsrechtLinks,
  );

  useEffect(() => {
    if (bestandsrechte.length > 0) {
      setBestandsrechtLinks(
        bestandsrechte.map((linkedBestandsrecht) => {
          return {
            id: linkedBestandsrecht.id,
            verknuepfteDokumentenMappeId: linkedBestandsrecht.verknuepfteDokumentenMappeId,
          } as BestandsrechtUpdateDTO;
        }),
      );
    }
  }, [bestandsrechte]);

  useEffect(() => {
    console.log('bestandsrechtLinks', bestandsrechtLinks);
  }, [bestandsrechtLinks]);

  useEffect(() => {
    if (updateBestandsrechtResponse.isLoading) return;
    setIsLoading(false);
    if (updateBestandsrechtResponse.hasError) {
      displayMessage(t('lea.messages.assignBestandsrecht.updateBestandsrecht.error'), 'error');
    }
    if (updateBestandsrechtResponse.data) {
      displayMessage(t('lea.messages.assignBestandsrecht.updateBestandsrecht.success'), 'success');
    }
    closeWizard();
  }, [updateBestandsrechtResponse]);

  const onFinishHandler = useCallback(() => {
    if (!isLoading) {
      updateBestandsrecht();
      setIsLoading(true);
    }
  }, [bestandsrechtLinks, isLoading]);

  return (
    <Form name={name} onFinish={onFinishHandler}>
      <Title className="assign-bestandsrecht-title" level={2}>
        {t('lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.title')}
        <span className="infoRequired">
          <HiddenInfoComponent
            title={t(`lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.infoComponent.title`)}
            text={t(`lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.infoComponent.text`)}
            customSelector=".assign-bestandsrecht-title"
          />
        </span>
      </Title>
      {bestandsrechte.length > 0 ? (
        <List className="bestandsrecht-list">
          <List.Item className="bestandsrecht-list-title-row">
            <span className="bestandsrecht-title-col">
              {t('lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.list.bestandsrechtTitleCol')}
            </span>
            <span className="compound-document-title-col">
              {t('lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.list.compoundDocumentTitleCol')}
            </span>
          </List.Item>
          {bestandsrechte.map((bestandsrecht) => {
            return (
              <List.Item key={bestandsrecht.id} className="bestandsrecht-list-entry">
                <BestandsrechtSelectFormItem
                  key={bestandsrecht.id}
                  initialValue={initialCompoundDocument}
                  bestandsrecht={bestandsrecht}
                  setBestandsrechtLinks={setBestandsrechtLinks}
                  bestandsrechtLinks={bestandsrechtLinks}
                />
              </List.Item>
            );
          })}
        </List>
      ) : (
        <div className="info-no-bestandsrecht">
          <ExclamationCircleFilled />
          <span>{t('lea.Wizard.AssignBestandsrechtWizard.Step1AssignBestandsrecht.noBestandsrecht')}</span>
        </div>
      )}
    </Form>
  );
};
