// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio, Space, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import { ExportMode } from '../ExportPdfWizard';
const { Title } = Typography;

interface Props {
  name: string;
  compoundDocumentInfo: CompoundDocumentInfo;
  exportMode: ExportMode;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
  setExportMode: React.Dispatch<React.SetStateAction<ExportMode>>;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

export const Step1SelectExportMode: FunctionComponent<Props> = ({
  name,
  compoundDocumentInfo,
  exportMode,
  setActivePageIndex,
  setExportMode,
}) => {
  const { t } = useTranslation();
  const [fields, setFields] = useState<FieldData[]>([
    { name: ['compoundDocument'], value: compoundDocumentInfo.id },
    { name: ['export-mode'], value: exportMode },
  ]);

  const onFieldsChangeHandler = useCallback(
    (_: any, allFields: React.SetStateAction<FieldData[]>) => setFields(allFields),
    [],
  );

  const onFinishHandler = useCallback(() => {
    setActivePageIndex((index) => ++index);
  }, []);

  useEffect(() => {
    if (!fields.length) return;
    setExportMode(fields.filter((field) => field.name.includes('export-mode'))[0].value as ExportMode);
  }, [fields]);

  return (
    <Form
      name={name}
      layout="vertical"
      fields={fields}
      onFinish={onFinishHandler}
      onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
    >
      <Title level={2}>{t('lea.Wizard.ExportPdfWizard.Step1ExportPdf.title')}</Title>
      <p className="ant-typography p-no-style">{t('lea.Wizard.ExportPdfWizard.Step1ExportPdf.requiredMsg')}</p>

      <Title level={3}>
        {t('lea.Wizard.ExportPdfWizard.Step1ExportPdf.compoundDocumentTitle', { title: compoundDocumentInfo.title })}
      </Title>

      <Form.Item
        name="export-mode"
        label={t('lea.Wizard.ExportPdfWizard.Step1ExportPdf.modeTitle')}
        rules={[{ required: true }]}
      >
        <Radio.Group>
          <Space direction="vertical">
            <Radio value={ExportMode.SINGLE_DOCUMENTS}>{ExportMode.SINGLE_DOCUMENTS.toString()}</Radio>
            <Radio value={ExportMode.COMBINED_DOCUMENT}>{ExportMode.COMBINED_DOCUMENT.toString()}</Radio>
          </Space>
        </Radio.Group>
      </Form.Item>
    </Form>
  );
};
