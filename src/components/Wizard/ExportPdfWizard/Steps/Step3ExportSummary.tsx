// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { ExportPdfService } from '../../../../api';
import { ExportMode, sortDocuments } from '../ExportPdfWizard';
import { Form, Row, Col, Typography } from 'antd';
import { useTranslation } from 'react-i18next';
import { DocumentTypeHelper } from '../../../../helpers/DocumentTypeHelper';
import { ExportPdfDTO } from '../../../../api/ExportPdf/ExportPdf';
import { displayMessage } from '@plateg/theme';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { DocumentList } from '../../../Pages/HomePage/HomePage';

const { Title } = Typography;

type Props = {
  name: string;
  compoundDocumentName: string;
  selectedDocuments: DocumentList | undefined;
  exportMode: ExportMode;
  exportName: string;
};

export const Step3ExportSummary: FunctionComponent<Props> = ({
  name,
  compoundDocumentName,
  selectedDocuments,
  exportMode,
  exportName,
}) => {
  const { t } = useTranslation();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EXPORT_PDF);
  const [exportInformation, setExportInformation] = useState<ExportPdfDTO>();
  const [exportPdfResponse, exportPdf] = ExportPdfService.exportPdf(exportInformation);

  const onFinishHandler = useCallback(() => {
    const exportInformation: ExportPdfDTO = {
      documents: selectedDocuments?.map((document) => document.id) ?? [],
      mergeDocuments: exportMode === ExportMode.COMBINED_DOCUMENT ? true : false,
      title: exportName,
    };
    setExportInformation(exportInformation);
  }, []);

  useEffect(() => {
    if (exportInformation) {
      exportPdf();
    }
  }, [exportInformation]);

  useEffect(() => {
    if (exportPdfResponse.isLoading) return;
    if (exportPdfResponse.hasError) {
      displayMessage(
        t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.error', {
          file: exportMode === ExportMode.COMBINED_DOCUMENT ? 'des PDF-Dokuments' : 'der PDF-Dokumente',
        }),
        'error',
      );
    } else if (exportPdfResponse.data) {
      const link = document.createElement('a');
      const downloadBlob = exportPdfResponse.data;
      link.href = URL.createObjectURL(downloadBlob);
      link.target = 'blank';
      link.download = `${exportName}.${exportMode === ExportMode.COMBINED_DOCUMENT ? 'pdf' : 'zip'}`;
      link.click();
      link.remove();
      closeWizard();
      displayMessage(
        t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.success', { title: compoundDocumentName }),
        'success',
      );
    }
  }, [exportPdfResponse]);

  return (
    <Form name={name} onFinish={onFinishHandler}>
      <Title level={2}>{t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.title')}</Title>
      <Row gutter={[0, 32]}>
        <Col span={10}>{t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.compoundDocumentTitle')}</Col>
        <Col span={14}>{compoundDocumentName}</Col>
        <Col span={10}>{t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.exportMode')}</Col>
        <Col span={14}>{exportMode}</Col>
        <Col span={10}>{t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.selectedDocuments')}</Col>
        <Col span={14}>
          {sortDocuments(selectedDocuments)?.map((document) => (
            <div key={document.id}>{DocumentTypeHelper(document.type)}</div>
          ))}
        </Col>
        <Col span={10}>{t('lea.Wizard.ExportPdfWizard.Step3ExportSummary.exportTitle')}</Col>
        <Col span={14}>{exportName}</Col>
      </Row>
    </Form>
  );
};
