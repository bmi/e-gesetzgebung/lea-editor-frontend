// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { Checkbox, CheckboxOptionType, Form, Space, Typography } from 'antd';
import { useTranslation } from 'react-i18next';
import { FieldData } from '../../SynopsisWizard/Steps';
import { DocumentType } from '../../../../api';
import { DocumentTypeHelper } from '../../../../helpers/DocumentTypeHelper';

const { Title } = Typography;

import './Step2SelectExportDocuments.less';
import { TitleFormItem } from '../../FormItems';
import { CompoundDocumentInfo, DocumentList } from '../../../Pages/HomePage/HomePage';
import { sortDocuments } from '../ExportPdfWizard';

type Props = {
  name: string;
  compoundDocument: CompoundDocumentInfo | undefined;
  selectedDocuments: DocumentList | undefined;
  exportName: string;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
  setSelectedDocuments: React.Dispatch<React.SetStateAction<DocumentList | undefined>>;
  setExportName: React.Dispatch<React.SetStateAction<string>>;
};

type CheckBoxProps = {
  children: string;
};

export const Step2SelectExportDocuments: FunctionComponent<Props> = ({
  name,
  compoundDocument,
  selectedDocuments,
  exportName,
  setActivePageIndex,
  setExportName,
  setSelectedDocuments,
}) => {
  const { t } = useTranslation();
  const [fields, setFields] = useState<FieldData[]>([
    { name: ['documents'], value: selectedDocuments?.map((document) => document.id) },
    { name: ['export-name'], value: exportName },
  ]);

  const onFieldsChangeHandler = useCallback(
    (_: any, allFields: React.SetStateAction<FieldData[]>) => setFields(allFields),
    [],
  );

  const onFinishHandler = useCallback(() => {
    setActivePageIndex((index) => ++index);
  }, []);

  const documents = (): CheckboxOptionType[] => {
    const hasAnlagen = compoundDocument?.documents?.find((document) => document.type === DocumentType.ANLAGE);
    const sortedDocuments = sortDocuments(compoundDocument?.documents);
    let documentTypes = sortedDocuments?.map((document) => {
      const singleDocument = document;
      return {
        label: DocumentTypeHelper(singleDocument.type),
        value: document.id,
        disabled: singleDocument.type === DocumentType.SYNOPSE || singleDocument.type === DocumentType.ANLAGE,
        checked: selectedDocuments?.includes(singleDocument) ?? false,
      };
    });
    documentTypes = documentTypes?.filter((dt) => (dt.label.props as CheckBoxProps).children !== 'Anlage');
    hasAnlagen &&
      documentTypes?.push({
        label: <>Anlagen</>,
        value: 'Anlagen',
        disabled: true,
        checked: false,
      });
    return documentTypes ?? [];
  };

  useEffect(() => {
    if (!fields.length) return;
    const selectedDocumentIds = fields.filter((field) => field.name.includes('documents'))[0].value as string[];
    setExportName(fields.filter((field) => field.name.includes('export-name'))[0].value as string);
    const documents = compoundDocument?.documents?.filter((document) => selectedDocumentIds?.includes(document.id)) as
      | DocumentList
      | undefined;
    setSelectedDocuments(documents);
  }, [fields]);

  return (
    <Form
      name={name}
      layout="vertical"
      fields={fields}
      onFinish={onFinishHandler}
      onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
    >
      <Title level={2}>{t('lea.Wizard.ExportPdfWizard.Step2SelectExportDocuments.title')}</Title>
      <p className="ant-typography p-no-style">
        {t('lea.Wizard.ExportPdfWizard.Step2SelectExportDocuments.requiredMsg')}
      </p>
      <Form.Item
        label={t('lea.Wizard.ExportPdfWizard.Step2SelectExportDocuments.chooseDocuments')}
        name={'documents'}
        rules={[
          {
            required: true,
            message: t('lea.Wizard.ExportPdfWizard.Step2SelectExportDocuments.chooseDocumentsRequiredMessage'),
          },
        ]}
        className="choose-document-label"
      >
        <Checkbox.Group className="documents-checkbox-group" options={documents()}>
          <Space direction="vertical"></Space>
        </Checkbox.Group>
      </Form.Item>
      <TitleFormItem currentWizard="ExportPdf" fieldName="export-name" />
    </Form>
  );
};
