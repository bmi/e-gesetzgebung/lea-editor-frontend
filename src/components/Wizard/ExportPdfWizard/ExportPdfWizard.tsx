// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Wizards } from '../WizardVisibleState';
import { useWizardVisibleState, useWizardVisibleDispatch } from '../useWizardVisible';
import { MultiPageModalComponent } from '@plateg/theme';
import { CompoundDocumentInfo, DocumentList } from '../../Pages/HomePage/HomePage';
import { Step1SelectExportMode, Step2SelectExportDocuments, Step3ExportSummary } from './Steps';
import { DocumentType } from '../../../api';

type Props = {
  compoundDocumentInfo: CompoundDocumentInfo;
};

export enum ExportMode {
  SINGLE_DOCUMENTS = 'Mehrere PDF-Dokumente',
  COMBINED_DOCUMENT = 'Einzelnes PDF-Dokument',
}

export const sortDocuments = (documents?: DocumentList) => {
  const documentOder = Object.values(DocumentType);
  if (documents) {
    let sortedDocuments = [...documents];
    sortedDocuments = sortedDocuments.sort((a, b) => documentOder.indexOf(a.type) - documentOder.indexOf(b.type));
    return sortedDocuments;
  }
  return undefined;
};

const ExportPdfWizard: FunctionComponent<Props> = ({ compoundDocumentInfo }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.EXPORT_PDF);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EXPORT_PDF);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [exportMode, setExportMode] = useState(ExportMode.SINGLE_DOCUMENTS);
  const [selectedDocuments, setSelectedDocuments] = useState<DocumentList>();
  const [exportName, setExportName] = useState<string>('');

  useEffect(() => {
    if (!selectedDocuments || selectedDocuments.length === 0) {
      const documents = compoundDocumentInfo.documents?.filter((document) => document.type !== DocumentType.ANLAGE);
      setSelectedDocuments(documents);
    }
  }, [compoundDocumentInfo?.documents]);

  useEffect(() => {
    if (!wizardVisible && activePageIndex > 0) setActivePageIndex(0);
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.ExportPdfWizard.title', { title: compoundDocumentInfo.title })}
      cancelBtnText={t('lea.Wizard.ExportPdfWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.ExportPdfWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.ExportPdfWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1SelectExportMode
              name="page1"
              compoundDocumentInfo={compoundDocumentInfo}
              exportMode={exportMode}
              setActivePageIndex={setActivePageIndex}
              setExportMode={setExportMode}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Step2SelectExportDocuments
              name="page2"
              compoundDocument={compoundDocumentInfo}
              selectedDocuments={selectedDocuments}
              exportName={exportName}
              setActivePageIndex={setActivePageIndex}
              setSelectedDocuments={setSelectedDocuments}
              setExportName={setExportName}
            />
          ),
          primaryInsteadNextBtn: { buttonText: t('lea.Wizard.ExportPdfWizard.checkBtnText') },
        },
        {
          formName: 'page3',
          content: (
            <Step3ExportSummary
              name="page3"
              compoundDocumentName={compoundDocumentInfo.title ?? ''}
              selectedDocuments={selectedDocuments}
              exportMode={exportMode}
              exportName={exportName}
            />
          ),
          primaryInsteadNextBtn: { buttonText: t('lea.Wizard.ExportPdfWizard.okBtnText') },
        },
      ]}
    />
  );
};

export default ExportPdfWizard;
