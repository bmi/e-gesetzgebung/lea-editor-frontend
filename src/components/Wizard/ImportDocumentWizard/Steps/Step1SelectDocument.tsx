// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { FunctionComponent, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UploadComponent } from '../../../UploadComponent/component.react';
//
const { Title } = Typography;
const standardMaximumSize = 20000000;

type Props = {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  setImportFile: (importFile: string) => void;
};

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const Step1SelectDocument: FunctionComponent<Props> = ({
  name,
  activePageIndex,
  setActivePageIndex,
  setImportFile,
}) => {
  const { t } = useTranslation();
  const uploadMessagesConfig = {
    uploadErrorFileType: t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadErrorFileType'),
    uploadErrorFileSize: t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadErrorFileSize'),
    uploadError: t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadError'),
    uploadBtn: t('lea.Wizard.ImportDocumentWizard.upload.uploadButton'),
    changeFileNameTitle: t('lea.Wizard.ImportDocumentWizard.upload.uploadChangeFileNameModalTitel'),
    changeFileNameCancelText: t('lea.Wizard.ImportDocumentWizard.upload.uploadChangeFileNameCancelTextButton'),
    changeFileNameItemLabel: t('lea.Wizard.ImportDocumentWizard.upload.uploadChangeFileNameLabel'),
    changeFileNameErrorMsg: t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadChangeFileNameError'),
  };

  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [fields, setFields] = useState<FieldData[]>([{ name: ['document-import'], value: fileList }]);

  const onFinishHandler = useCallback(() => {
    if (!fileList.length) return;
    const importFile = fileList[0];
    importFile.originFileObj
      ?.text()
      .then((x) => setImportFile(x))
      .catch(() => {
        return;
      });
    setActivePageIndex(activePageIndex + 1);
  }, [fields]);

  const onFieldsChangeHandler = useCallback(
    (_: any, allFields: React.SetStateAction<FieldData[]>) => setFields(allFields as FieldData[]),
    [],
  );

  return (
    <Form
      name={name}
      layout="vertical"
      fields={fields}
      onFinish={onFinishHandler}
      onFieldsChange={onFieldsChangeHandler}
    >
      <Title level={2}>{t('lea.Wizard.ImportDocumentWizard.Step1SelectDocument.title')}</Title>
      <p className="ant-typography p-no-style">
        {t('lea.Wizard.ImportDocumentWizard.Step1SelectDocument.requiredMsg')}
      </p>
      <UploadComponent
        fileList={fileList}
        setFileList={setFileList}
        fileFormat=".xml"
        fileSize={standardMaximumSize}
        onlySingleFileAllowed={true}
        restrictRenaming={false}
        messagesConfig={{
          ...uploadMessagesConfig,
        }}
        version={1}
        isBearbeiten={false}
        errorProps={{
          errorTitle: t('lea.Wizard.ImportDocumentWizard.upload.uploadFileTitle.title'),
          formatString: t('lea.Wizard.ImportDocumentWizard.upload.uploadFileFormatShort'),
        }}
      />
    </Form>
  );
};

export default Step1SelectDocument;
