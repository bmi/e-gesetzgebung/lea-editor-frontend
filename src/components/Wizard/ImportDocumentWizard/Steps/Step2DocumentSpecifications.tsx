// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CompoundDocumentService, CompoundDocumentTitleDTO } from '../../../../api';
import { ApiResponse } from '../../../../api/Api';
import { useAppInfoState } from '../../../App';
import { CompoundDocumentFormItem, TitleFormItem } from '../../FormItems';
import { ImportStatusFormItem } from '../../FormItems/Import.Status.FormItem';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage } from '@plateg/theme';
const { Title } = Typography;

type Props = {
  name: string;
  compoundDocumentResponse: ApiResponse<CompoundDocumentTitleDTO[]>;
  importFile: string;
};

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const Step2DocumentSpecifications: FunctionComponent<Props> = ({ name, compoundDocumentResponse, importFile }) => {
  const { t } = useTranslation();
  const { basePath } = useAppInfoState();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.IMPORT_DOCUMENT);
  const [documentTitle, setDocumentTitle] = useState<string | undefined>(undefined);
  const [compoundDocumentId, setCompoundDocumentId] = useState<string | undefined>(undefined);
  const [importDocumentInCompoundDocumentResponse, importDocumentInCompoundDocument] =
    CompoundDocumentService.importDocument(
      {
        title: documentTitle || '',
        xmlContent: importFile,
      },
      compoundDocumentId || '',
    );

  const [fields, setFields] = useState<FieldData[]>([
    /** inital FormValues */
    { name: ['compoundDocumentId'], value: compoundDocumentId },
    { name: ['import-document-status'], value: 'arbeitsentwurf' },
    { name: ['document-title'], value: documentTitle },
  ]);
  const [isLoading, setIsLoading] = useState(false);

  const onFinishHandler = useCallback(() => {
    if (!DocumentType) return alert('Cannot create Document -- no Title');
    if (!compoundDocumentId) return alert('Cannot create Document -- No compound document');
    if (!importFile) return alert('Cannot create Document -- No Import Document');
    if (!isLoading) {
      importDocumentInCompoundDocument();
      setIsLoading(true);
    }
  }, [documentTitle, compoundDocumentId, importFile, isLoading]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: any) => setFields(allFields as FieldData[]), []);

  useEffect(() => {
    if (!fields.length) return;
    setDocumentTitle(fields.filter((field) => field.name.includes('document-title'))[0].value as string);
    setCompoundDocumentId(fields.filter((field) => field.name.includes('compoundDocumentId'))[0].value as string);
  }, [fields]);

  useEffect(() => {
    if (!importDocumentInCompoundDocumentResponse.isLoading) {
      setIsLoading(false);
      if (importDocumentInCompoundDocumentResponse.hasError) {
        displayMessage(t('lea.messages.importDocument.error.default'), 'error');
        // Codes müssen überarbeitet werden
        // switch (importDocumentInCompoundDocumentResponse.status?.statusCode) {
        //   case 404:
        //     message.error(t('lea.messages.importDocument.error..noCompoundDocument'));
        //     break;
        //   case 415:
        //     message.error(t('lea.messages.importDocument.error.maxSizeOrWrongFormat'));
        //     break;
        //   case 422:
        //     message.error(t('lea.messages.importDocument.error.unprocessable'));
        //     break;

        //   default: {
        //     const compoundDocuments: CompoundDocumentDTO[] = compoundDocumentResponse.data || [];
        //     let compoundDocumentTitle = '';
        //     compoundDocuments.forEach((compoundDocument: CompoundDocumentDTO) => {
        //       if (compoundDocumentId === compoundDocument.id) {
        //         compoundDocumentTitle = compoundDocument.title;
        //       }
        //     });
        //     void message.error(
        //       t('lea.messages.importDocument.error.maxNumber', {
        //         compoundDocument: compoundDocumentTitle,
        //         //fehlender Type
        //         documentType: '',
        //       })
        //     );
        //   }
        // }
      } else if (importDocumentInCompoundDocumentResponse.data) {
        closeWizard();
        displayMessage(t('lea.messages.createDocument.success'), 'success');
        window.location.hash = `#${basePath}/compounddocument/${importDocumentInCompoundDocumentResponse.data.id}`;
      }
    }
  }, [importDocumentInCompoundDocumentResponse]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={onFieldsChangeHandler}
      >
        <Title level={2}>{t('lea.Wizard.ImportDocumentWizard.Step2DocumentSpecifications.title')}</Title>
        <p className="ant-typography p-no-style">
          {t('lea.Wizard.ImportDocumentWizard.Step2DocumentSpecifications.requiredMsg')}
        </p>

        <CompoundDocumentFormItem
          fieldName="compoundDocumentId"
          currentWizard="ImportDocumentWizard"
          compoundDocumentResponse={compoundDocumentResponse}
        />
        <ImportStatusFormItem fieldName="import-document-status" />
        <TitleFormItem currentWizard="Document" fieldName="document-title" />
      </Form>
    </>
  );
};

export default Step2DocumentSpecifications;
