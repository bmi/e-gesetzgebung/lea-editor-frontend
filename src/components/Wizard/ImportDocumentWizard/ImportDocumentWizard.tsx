// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';
import { Step1SelectDocument, Step2DocumentSpecifications } from './Steps';
import { CompoundDocumentService } from '../../../api';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';

const ImportDocumentWizard: FunctionComponent = () => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.IMPORT_DOCUMENT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.IMPORT_DOCUMENT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [importFile, setImportFile] = useState<string>('');
  const [compoundDocumentResponse, fetchCompoundDocuments] = CompoundDocumentService.getAllTitles();

  useEffect(() => {
    if (wizardVisible) {
      fetchCompoundDocuments();
    } else if (activePageIndex > 0) {
      setActivePageIndex(0);
    }
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.ImportDocumentWizard.title')}
      cancelBtnText={t('lea.Wizard.ImportDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.ImportDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.ImportDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1SelectDocument
              name="page1"
              activePageIndex={activePageIndex}
              setActivePageIndex={setActivePageIndex}
              setImportFile={setImportFile}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Step2DocumentSpecifications
              name="page2"
              compoundDocumentResponse={compoundDocumentResponse}
              importFile={importFile}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.ImportDocumentWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default ImportDocumentWizard;
