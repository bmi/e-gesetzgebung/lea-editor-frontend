// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ApiResponse, CompoundDocumentType, PropositionDTO } from '../../../../api';
import { RegelungsvorhabenFormItem, TitleFormItem, CompoundDocumentTypeFormItem } from '../../FormItems';
const { Title } = Typography;
interface Props {
  name: string;
  propositionsResponse: ApiResponse<PropositionDTO[]>;
  regelungsvorhabenId: string | undefined;
  title: string | undefined;
  type: CompoundDocumentType | undefined;
  setActivePageIndex: React.Dispatch<React.SetStateAction<number>>;
  setRegelungsvorhabenId: React.Dispatch<React.SetStateAction<string | undefined>>;
  setTitle: React.Dispatch<React.SetStateAction<string | undefined>>;
  setType: React.Dispatch<React.SetStateAction<CompoundDocumentType | undefined>>;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

export const Step1CompoundDocumentSpecifications: FunctionComponent<Props> = ({
  name,
  propositionsResponse,
  regelungsvorhabenId,
  title,
  type,
  setActivePageIndex,
  setRegelungsvorhabenId,
  setTitle,
  setType,
}) => {
  const { t } = useTranslation();

  const [fields, setFields] = useState<FieldData[]>([
    { name: ['regelungsvorhaben'], value: regelungsvorhabenId },
    { name: ['compound-document-title'], value: title },
    { name: ['type'], value: type },
  ]);

  const onFinishHandler = useCallback(() => {
    setActivePageIndex((index) => ++index);
  }, []);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => setFields(allFields), []);

  useEffect(() => {
    if (!fields.length) return;

    const compoundDocumentRegelungsvorhabenId = fields.filter((field) => field.name.includes('regelungsvorhaben'))[0]
      .value as string;
    const compoundDocumentTitle = fields.filter((field) => field.name.includes('compound-document-title'))[0]
      .value as string;
    const compoundDocumentType = fields.filter((field) => field.name.includes('type'))[0]
      .value as string as CompoundDocumentType;

    setRegelungsvorhabenId(compoundDocumentRegelungsvorhabenId);
    setTitle(compoundDocumentTitle);
    setType(compoundDocumentType);
  }, [fields]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="compoundDocumentWizard-step1"
      >
        <Title level={2}>
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step1CompoundDocumentSpecifications.title')}
        </Title>
        <p className="ant-typography p-no-style">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step1CompoundDocumentSpecifications.requiredMsg')}
        </p>

        <RegelungsvorhabenFormItem fieldName="regelungsvorhaben" propositionsResponse={propositionsResponse} />

        <TitleFormItem fieldName="compound-document-title" currentWizard="CompoundDocument" />

        <CompoundDocumentTypeFormItem fieldName="type" />
      </Form>
    </>
  );
};
