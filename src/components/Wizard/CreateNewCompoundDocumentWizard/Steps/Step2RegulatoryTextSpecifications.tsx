// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './Step2RegulatoryTextSpecifications.less';

import { Form, Typography } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CompoundDocumentService, CompoundDocumentType } from '../../../../api';
import { useAppInfoState } from '../../../App';
import { TitleFormItem, DocumentStatusFormItem } from '../../FormItems';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage } from '@plateg/theme';
const { Title } = Typography;

type Props = {
  name: string;
  regelungsvorhabenId: string | undefined;
  compoundDocumentTitle: string | undefined;
  compoundDocumentType: CompoundDocumentType | undefined;
  documentTitle: string;
  setDocumentTitle: React.Dispatch<React.SetStateAction<string>>;
};

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const Step2RegulatoryTextSpecifications: FunctionComponent<Props> = ({
  name,
  regelungsvorhabenId,
  compoundDocumentTitle,
  compoundDocumentType,
  documentTitle,
  setDocumentTitle,
}) => {
  const { t } = useTranslation();
  const { basePath } = useAppInfoState();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_COMPOUND_DOCUMENT);
  const [isLoading, setIsLoading] = useState(false);

  const [createdCompoundDocument, createCompoundDocument] = CompoundDocumentService.create({
    regelungsVorhabenId: regelungsvorhabenId ?? '',
    title: compoundDocumentTitle ?? '',
    type: compoundDocumentType ?? CompoundDocumentType.STAMMGESETZ,
    titleRegulatoryText: documentTitle ?? '',
    initialNumberOfLevels: 0,
  });

  const [fields, setFields] = useState<FieldData[]>([
    /** inital FormValues */
    { name: ['document-status'], value: 'referentenentwurf' },
    { name: ['document-title'], value: documentTitle },
  ]);

  useEffect(() => {
    if (!fields.length) return;
    setDocumentTitle(fields.filter((field) => field.name.includes('document-title'))[0].value as string);
  }, [fields]);

  const onFinishHandler = useCallback(() => {
    if (!documentTitle) return alert('Cannot create CompoundDocument with Document -- no Title');
    if (!regelungsvorhabenId) return alert('Cannot create CompoundDocument -- No regelungsvorhabenId');
    if (!compoundDocumentTitle) return alert('Cannot create CompoundDocument -- No title');
    if (!compoundDocumentType) return alert('Cannot create CompoundDocument -- No type');
    if (!isLoading) {
      createCompoundDocument();
      setIsLoading(true);
    }
  }, [documentTitle, regelungsvorhabenId, compoundDocumentTitle, compoundDocumentType, isLoading]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => {
    setFields(allFields);
  }, []);

  useEffect(() => {
    if (!createdCompoundDocument.isLoading) {
      setIsLoading(false);
      if (createdCompoundDocument.hasError) {
        displayMessage(
          t('lea.messages.createCompoundDocument.error', { compoundDocument: compoundDocumentTitle }),
          'error',
        );
      } else if (createdCompoundDocument.data) {
        closeWizard();
        displayMessage(t('lea.messages.createCompoundDocument.success'), 'success');
        window.location.hash = `#${basePath}/compounddocument/${createdCompoundDocument.data.id}`;
      }
    }
  }, [createdCompoundDocument]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_: any, allFields: any) => onFieldsChangeHandler(_, allFields as FieldData[])}
      >
        <Title level={2} className="titleWithSubtitle">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step2RegulatoryTextSpecifications.title')}
        </Title>
        <p className="ant-typography p-no-style subTitle">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step2RegulatoryTextSpecifications.subTitle')}
        </p>
        <p className="ant-typography p-no-style">
          {t('lea.Wizard.CreateNewCompoundDocumentWizard.Step2RegulatoryTextSpecifications.requiredMsg')}
        </p>
        <TitleFormItem fieldName="document-title" currentWizard="Document" />
        <DocumentStatusFormItem fieldName="document-status" />
      </Form>
    </>
  );
};

export default Step2RegulatoryTextSpecifications;
