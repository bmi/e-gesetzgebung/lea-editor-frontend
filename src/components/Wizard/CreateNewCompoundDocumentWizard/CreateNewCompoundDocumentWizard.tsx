// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';
import { Step1CompoundDocumentSpecifications } from './Steps';
import { CompoundDocumentType, PropositionService } from '../../../api';
import Step2RegulatoryTextSpecifications from './Steps/Step2RegulatoryTextSpecifications';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';

export const CreateNewCompoundDocumentWizard: FunctionComponent = () => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.NEW_COMPOUND_DOCUMENT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_COMPOUND_DOCUMENT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [regelungsvorhabenId, setRegelungsvorhabenId] = useState<string | undefined>(undefined);
  const [compoundDocumentTitle, setCompoundDocumentTitle] = useState<string | undefined>(undefined);
  const [compoundDocumentType, setCompoundDocumentType] = useState<CompoundDocumentType | undefined>(
    CompoundDocumentType.STAMMGESETZ,
  );
  const [documentTitle, setDocumentTitle] = useState<string>('');

  const [propositionsResponse, fetchPropositions] = PropositionService.getAll();

  useEffect(() => {
    if (!wizardVisible) {
      setActivePageIndex(0);
      setRegelungsvorhabenId(undefined);
      setCompoundDocumentTitle(undefined);
      setCompoundDocumentType(CompoundDocumentType.STAMMGESETZ);
      setDocumentTitle('');
    } else {
      fetchPropositions();
    }
  }, [wizardVisible]);

  return (
    <MultiPageModalComponent
      key={`new-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.CreateNewCompoundDocumentWizard.title')}
      cancelBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.CreateNewCompoundDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1CompoundDocumentSpecifications
              name="page1"
              propositionsResponse={propositionsResponse}
              regelungsvorhabenId={regelungsvorhabenId}
              title={compoundDocumentTitle}
              type={compoundDocumentType}
              setActivePageIndex={setActivePageIndex}
              setTitle={setCompoundDocumentTitle}
              setRegelungsvorhabenId={setRegelungsvorhabenId}
              setType={setCompoundDocumentType}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Step2RegulatoryTextSpecifications
              name="page2"
              compoundDocumentTitle={compoundDocumentTitle}
              compoundDocumentType={compoundDocumentType}
              regelungsvorhabenId={regelungsvorhabenId}
              documentTitle={documentTitle}
              setDocumentTitle={setDocumentTitle}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.CreateNewCompoundDocumentWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};
