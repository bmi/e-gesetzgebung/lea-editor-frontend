// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';

import { Step1CreateNewVersion } from './Steps';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';
import { PinnedCompoundDocumentsSettings } from '../../../api/User/UserSettings';

type Props = {
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
};

const NewVersionWizard: FunctionComponent<Props> = ({ updateTable, userSettingsRef }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.NEW_VERSION);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_VERSION);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  return (
    <MultiPageModalComponent
      key={`rename-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.NewVersionWizard.title')}
      cancelBtnText={t('lea.Wizard.NewVersionWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.NewVersionWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.NewVersionWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: <Step1CreateNewVersion name="page1" updateTable={updateTable} userSettingsRef={userSettingsRef} />,
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.NewVersionWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default NewVersionWizard;
