// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import Title from 'antd/lib/typography/Title';
import { DocumentList } from '../../../Pages/HomePage/HomePage';
import { TitleFormItem, SelectDocumentsFormItem } from '../../FormItems';
import { CompoundDocumentService, DocumentStateType, DocumentType } from '../../../../api';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage } from '@plateg/theme';
import PinPreviousVersionFormItem from '../../FormItems/PinPreviousVersion.FormItem';
import { UserSettingsService } from '../../../../api/User/UserService';
import { PinnedCompoundDocumentsSettings } from '../../../../api/User/UserSettings';
import { useDocumentsTableActionState } from '../../../Pages/useDocumentsTableAction';
import { HomePageTabs, useHomePageTabState } from '../../../Pages';
import { useHistory } from 'react-router';
import { useAppInfoState } from '../../../App';

interface Props {
  name: string;
  userSettingsRef: React.MutableRefObject<PinnedCompoundDocumentsSettings | null>;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

const newVersionTitleFieldName = 'newVersionTitle';
const pinPreviousVersionFormItemName = 'pinPreviousVersion';

export const Step1CreateNewVersion: FunctionComponent<Props> = ({ name, userSettingsRef, updateTable }) => {
  const { t } = useTranslation();
  let { compoundDocumentInfo } = useDocumentsTableActionState();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.NEW_VERSION);
  const [fields, setFields] = useState<FieldData[]>([
    { name: [newVersionTitleFieldName], value: compoundDocumentInfo.title },
    { name: [pinPreviousVersionFormItemName] },
  ]);
  const [isLoading, setIsLoading] = useState(false);
  const [newVersionTitle, setNewVersionTitle] = useState('');
  const [compoundDocumentResponse, fetchCompoundDocument] = CompoundDocumentService.getById(compoundDocumentInfo.id);
  const documents = compoundDocumentInfo.documents;
  const homePageTab = useHomePageTabState();
  const [sortedDocuments, setSortedDocuments] = useState<DocumentList>([]);
  const [userSettingsResponse, fetchUserSettings] = UserSettingsService.getUserSettings();
  const [userSettingsUpdateResponse, updateUserSettings] = UserSettingsService.updateUserSettings(
    userSettingsRef.current,
  );
  const { basePath } = useAppInfoState();
  const history = useHistory();
  const insideDocument = history.location.pathname.includes('/document/');
  const shouldJumpToVersionHistory =
    homePageTab === HomePageTabs.ABSTIMMUNGEN &&
    (!insideDocument || (insideDocument && compoundDocumentInfo.compoundDocumentState === DocumentStateType.FREEZE));

  const rvPinnedSettings = userSettingsRef.current
    ? userSettingsRef.current.pinnedDokumentenmappen.find((rv) => rv.rvId === compoundDocumentInfo.rvId)
    : undefined;
  const isPreviousVersionPinned =
    !!rvPinnedSettings && rvPinnedSettings.dmIds.some((dmId) => dmId === compoundDocumentInfo.id);

  useEffect(() => {
    fetchUserSettings();
  }, []);

  useEffect(() => {
    if (userSettingsResponse.hasError || userSettingsResponse.isLoading) return;
    if (!userSettingsResponse.data) return;
    userSettingsRef.current = { ...userSettingsResponse.data, lastChange: undefined };
  }, [userSettingsResponse]);

  useEffect(() => {
    if (compoundDocumentInfo.id && !compoundDocumentInfo.title) {
      fetchCompoundDocument();
    } else {
      documents && setDocuments(documents);
    }
  }, [compoundDocumentInfo]);

  const [documentIdList, setDocumentIdList] = useState<CheckboxValueType[]>(
    sortedDocuments.map((document) => document.id),
  );

  useEffect(() => {
    if (!compoundDocumentResponse.isLoading) {
      if (compoundDocumentResponse.hasError) return;
      if (!compoundDocumentResponse.data) return;
      compoundDocumentInfo = {
        id: compoundDocumentResponse.data.id,
        title: compoundDocumentResponse.data.title,
        type: compoundDocumentResponse.data.type,
        version: compoundDocumentResponse.data.version,
        documents: compoundDocumentResponse.data.documents.map((document) => {
          return { id: document.id, type: document.type, title: document.title, created: document.createdAt };
        }),
      };
      compoundDocumentInfo.documents && setDocuments(compoundDocumentInfo.documents);
      setFields([{ name: [newVersionTitleFieldName], value: compoundDocumentInfo.title }]);
    }
  }, [compoundDocumentResponse]);

  const sortAnlagen = (anlagen: DocumentList) => {
    return anlagen.sort((a, b) => new Date(a.created).getTime() - new Date(b.created).getTime());
  };

  const setDocuments = (documents: DocumentList) => {
    let documentsToSet: DocumentList = [];
    const vorblattDocument = documents?.find((document) => document.type.includes('VORBLATT'));
    vorblattDocument && documentsToSet.push(vorblattDocument);
    const regelungstextDocument = documents?.find((document) => document.type.includes('REGELUNGSTEXT'));
    regelungstextDocument && documentsToSet.push(regelungstextDocument);
    const begruendungDocument = documents?.find((document) => document.type.includes('BEGRUENDUNG'));
    begruendungDocument && documentsToSet.push(begruendungDocument);
    const anschreibenDocument = documents?.find((document) => document.type.includes('ANSCHREIBEN'));
    anschreibenDocument && documentsToSet.push(anschreibenDocument);
    const AnlageDocuments = documents?.filter((document) => document.type === DocumentType.ANLAGE);
    if (AnlageDocuments) documentsToSet = documentsToSet.concat(sortAnlagen(AnlageDocuments));
    setSortedDocuments(documentsToSet);
    setDocumentIdList(documentsToSet.map((document) => document.id));
  };

  const [createNewVersionResponse, createNewVersion] = CompoundDocumentService.createNewVersion(
    compoundDocumentInfo.id,
    documentIdList as string[],
    { title: newVersionTitle },
  );

  useEffect(() => {
    setNewVersionTitle(fields.filter((field) => field.name.includes(newVersionTitleFieldName))[0].value as string);
  }, [fields]);

  const onFieldsChangeHandler = useCallback(
    (_: FieldData[], allFields: FieldData[]) => setFields(allFields as React.SetStateAction<FieldData[]>),
    [],
  );

  const onFinishHandler = useCallback(() => {
    if (!isLoading) {
      createNewVersion();
      setIsLoading(true);
    }
  }, [fields, compoundDocumentInfo, isLoading]);

  useEffect(() => {
    if (createNewVersionResponse.isLoading) return;
    if (createNewVersionResponse.hasError) {
      displayMessage(t('lea.messages.createNewCompoundDocumentVersion.error'), 'error');
      return;
    }
    displayMessage(t('lea.messages.createNewCompoundDocumentVersion.success'), 'success');
    const pinPreviousVersion = fields.filter((field) => field.name.includes(pinPreviousVersionFormItemName))[0]
      .value as boolean;
    if (!userSettingsRef.current || !compoundDocumentInfo.rvId) return;
    if (pinPreviousVersion && !isPreviousVersionPinned) {
      if (rvPinnedSettings) {
        rvPinnedSettings.dmIds.push(compoundDocumentInfo.id);
      } else {
        userSettingsRef.current.pinnedDokumentenmappen.push({
          rvId: compoundDocumentInfo.rvId,
          dmIds: [compoundDocumentInfo.id],
        });
      }
      updateUserSettings();
    } else if (!pinPreviousVersion && isPreviousVersionPinned) {
      const dmIndex = rvPinnedSettings.dmIds.findIndex((dmId) => dmId === compoundDocumentInfo.id);
      dmIndex !== -1 && rvPinnedSettings.dmIds.splice(dmIndex, 1);
      updateUserSettings();
    } else {
      closeWizard();
      // In tab 'Freigegebene Dokument (2nd tab) it should load version history after create new version
      if (shouldJumpToVersionHistory) {
        history.push(`${basePath}/versionHistory/${compoundDocumentInfo.rvId}`);
      }
      updateTable?.(true);
    }
  }, [createNewVersionResponse]);

  useEffect(() => {
    if (!userSettingsUpdateResponse.isLoading && !userSettingsUpdateResponse.hasError) {
      closeWizard();
      if (shouldJumpToVersionHistory) {
        history.push(`${basePath}/versionHistory/${compoundDocumentInfo.rvId}`);
      }
      updateTable?.(true);
    }
  }, [userSettingsUpdateResponse]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_ as FieldData[], allFields as FieldData[])}
        className="rename-document-step1"
      >
        <Title className="title" level={2}>
          {t('lea.Wizard.NewVersionWizard.Step1CreateNewVersion.title')}
        </Title>
        <p className="ant-typography p-no-style requiredMessage">
          {t('lea.Wizard.NewVersionWizard.Step1CreateNewVersion.requiredMsg')}
        </p>
        <TitleFormItem currentWizard="NewVersion" fieldName={newVersionTitleFieldName} />
        <SelectDocumentsFormItem
          fieldName="documents"
          documents={sortedDocuments}
          documentIdList={documentIdList}
          setDocumentIdList={setDocumentIdList}
        />
        <PinPreviousVersionFormItem
          fieldName={pinPreviousVersionFormItemName}
          isPreviousVersionPinned={isPreviousVersionPinned}
        />
      </Form>
    </>
  );
};
