// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Wizards } from './WizardVisibleState';
import { closeWizard, openWizard } from './WizardVisibleSlice';
import { useAppDispatch, useAppSelector } from '../Store';

type UseWizardVisibleDispatch = {
  openWizard: () => void;
  closeWizard: () => void;
};

export const useWizardVisibleState = (wizard: Wizards): boolean =>
  useAppSelector((state) => state.editorStatic.wizardVisible[wizard]);

export const useWizardVisibleDispatch = (wizard: Wizards): UseWizardVisibleDispatch => {
  const dispatch = useAppDispatch();

  return {
    openWizard: () => dispatch(openWizard(wizard)),
    closeWizard: () => dispatch(closeWizard(wizard)),
  };
};
