// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { InitialWizardState } from './WizardState';
import { DocumentType } from '../../api';

const wizardSlice = createSlice({
  name: 'Wizard',
  initialState: InitialWizardState,
  reducers: {
    setSelectedDocumentType(state, action: PayloadAction<DocumentType | undefined>) {
      state.selectedDocumentType = action.payload;
    },
  },
});

export const { setSelectedDocumentType } = wizardSlice.actions;
export const WizardReducer = wizardSlice.reducer;
