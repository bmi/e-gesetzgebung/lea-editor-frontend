// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';

import { Step1RenameDocument } from './Steps';
import { DocumentInfo } from '../../Pages/HomePage/HomePage';
import { EntityType } from '../../../api';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';

type Props = {
  documentInfo: DocumentInfo;
  updateTable: React.Dispatch<React.SetStateAction<boolean>>;
};

const RenameDocumentWizard: FunctionComponent<Props> = ({ documentInfo, updateTable }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.RENAME_DOCUMENT);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.RENAME_DOCUMENT);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  return (
    <MultiPageModalComponent
      key={`rename-document-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.RenameDocumentWizard.title', {
        documentType: documentInfo.entity === EntityType.COMPOUND_DOCUMENT ? 'Dokumentenmappe' : 'Dokument',
      })}
      cancelBtnText={t('lea.Wizard.RenameDocumentWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.RenameDocumentWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.RenameDocumentWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: <Step1RenameDocument name="page1" documentInfo={documentInfo} updateTable={updateTable} />,
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.RenameDocumentWizard.okBtnText'),
          },
        },
      ]}
    />
  );
};

export default RenameDocumentWizard;
