// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import Title from 'antd/lib/typography/Title';
import { DocumentInfo } from '../../../Pages/HomePage/HomePage';
import { RenameDocumentFormItem } from '../../FormItems';
import { CompoundDocumentService, DocumentService, EntityType } from '../../../../api';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { displayMessage } from '@plateg/theme';
interface Props {
  name: string;
  documentInfo: DocumentInfo;
  updateTable: React.Dispatch<React.SetStateAction<boolean>>;
}

type FieldData = {
  name: (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
};

export const Step1RenameDocument: FunctionComponent<Props> = ({ name, documentInfo, updateTable }) => {
  const { t } = useTranslation();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.RENAME_DOCUMENT);
  const [documentTitle, setDocumentTitle] = useState(documentInfo.title);
  const [fields, setFields] = useState<FieldData[]>([{ name: ['renameDocument'], value: documentInfo.title }]);
  const [isLoading, setIsLoading] = useState(false);
  const [renamedDocumentResponse, renameDocument] = DocumentService.update(documentInfo.id, {
    title: documentTitle,
  });
  const [renamedCompundDocumentResponse, renameCompundDocument] = CompoundDocumentService.update(documentInfo.id, {
    title: documentTitle,
  });

  const onFinishHandler = useCallback(() => {
    if (!isLoading) {
      if (documentInfo.entity === EntityType.COMPOUND_DOCUMENT) {
        renameCompundDocument();
      } else {
        renameDocument();
      }
      setIsLoading(true);
    }
  }, [fields, documentInfo, isLoading]);

  const onFieldsChangeHandler = useCallback((_: any, allFields: FieldData[]) => {
    setDocumentTitle(allFields[0].value as string);
    setFields(allFields);
  }, []);

  useEffect(() => {
    if (!renamedDocumentResponse.isLoading) {
      setIsLoading(false);
      if (renamedDocumentResponse.hasError) {
        displayMessage(t('lea.messages.renameDocument.error'), 'error');
      } else if (renamedDocumentResponse.status?.statusCode === 200) {
        displayMessage(t('lea.messages.renameDocument.success'), 'success');
      }
      closeWizard();
      updateTable(true);
    }
  }, [renamedDocumentResponse]);

  useEffect(() => {
    if (!renamedCompundDocumentResponse.isLoading) {
      setIsLoading(false);
      if (renamedCompundDocumentResponse.hasError) {
        displayMessage(t('lea.messages.renameCompundDocument.error'), 'error');
      } else if (renamedCompundDocumentResponse.status?.statusCode === 200) {
        displayMessage(t('lea.messages.renameCompundDocument.success'), 'success');
      }
      closeWizard();
      updateTable(true);
    }
  }, [renamedCompundDocumentResponse]);

  return (
    <>
      <Form
        name={name}
        layout="vertical"
        fields={fields}
        onFinish={onFinishHandler}
        onFieldsChange={(_, allFields) => onFieldsChangeHandler(_, allFields as FieldData[])}
        className="rename-document-step1"
      >
        <Title className="title" level={2}>
          {t('lea.Wizard.RenameDocumentWizard.Step1RenameDocument.title')}
        </Title>
        <p className="ant-typography p-no-style requiredMessage">
          {t('lea.Wizard.RenameDocumentWizard.Step1RenameDocument.requiredMsg')}
        </p>
        <RenameDocumentFormItem documentInfo={documentInfo} fieldName="renameDocument" />
      </Form>
    </>
  );
};
