// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiPageModalComponent } from '@plateg/theme';

import { Step1ImportEgfaData } from './Steps/Step1ImportEgfaData';
import { CompoundDocumentInfo } from '../../Pages/HomePage/HomePage';
import { useWizardVisibleDispatch, useWizardVisibleState } from '../useWizardVisible';
import { Wizards } from '../WizardVisibleState';

type Props = {
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
};

const EgfaWizard: FunctionComponent<Props> = ({ compoundDocumentInfo, updateTable }) => {
  const { t } = useTranslation();
  const wizardVisible = useWizardVisibleState(Wizards.EGFA);
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EGFA);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [okBtnDisabled, setOkBtnDisabled] = useState<boolean>(true);

  return (
    <MultiPageModalComponent
      key={`import-egfa-data-${wizardVisible.toString()}`}
      isVisible={wizardVisible}
      setIsVisible={closeWizard}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('lea.Wizard.EgfaWizard.title')}
      cancelBtnText={t('lea.Wizard.EgfaWizard.cancelBtnText')}
      nextBtnText={t('lea.Wizard.EgfaWizard.nextBtnText')}
      prevBtnText={t('lea.Wizard.EgfaWizard.prevBtnText')}
      pages={[
        {
          formName: 'page1',
          content: (
            <Step1ImportEgfaData
              name="page1"
              compoundDocumentInfo={compoundDocumentInfo}
              updateTable={updateTable}
              setOkBtnDisabled={setOkBtnDisabled}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('lea.Wizard.EgfaWizard.okBtnText'),
            disabled: okBtnDisabled,
          },
        },
      ]}
    />
  );
};

export default EgfaWizard;
