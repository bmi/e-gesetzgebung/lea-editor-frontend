// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Checkbox } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CompoundDocumentInfo } from '../../../Pages/HomePage/HomePage';
import {
  CompoundDocumentService,
  DocumentService,
  EgfaModulInfo,
  EgfaModulStatus,
  EgfaModulType,
  useFirstDocumentDispatch,
  useFirstDocumentState,
} from '../../../../api';
import { useWizardVisibleDispatch } from '../../useWizardVisible';
import { Wizards } from '../../WizardVisibleState';
import { FormItemWithInfo, HinweisComponent, displayMessage } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';
import './Step1ImportEgfaData.less';
import { GfaNewVersionFormItem } from '../../FormItems/Gfa.NewVersion.FormItem';
import { useAppInfoState } from '../../../App';

interface Props {
  name: string;
  compoundDocumentInfo: CompoundDocumentInfo;
  updateTable?: React.Dispatch<React.SetStateAction<boolean>>;
  setOkBtnDisabled?: React.Dispatch<React.SetStateAction<boolean>>;
}

const getModuleName = (moduleId: EgfaModulType) => {
  switch (moduleId) {
    case EgfaModulType.ERFUELLUNGSAUFWAND:
      return 'Erfüllungsaufwand';
    case EgfaModulType.EA_OEHH:
      return 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte';
    case EgfaModulType.PREISE:
      return 'Auswirkungen auf Einzelpreise und Preisniveau';
    case EgfaModulType.KMU:
      return 'Auswirkungen auf mittelständische Unternehmen - KMU-Test';
    case EgfaModulType.SONSTIGE_KOSTEN:
      return 'Sonstige Kosten';
    case EgfaModulType.ENAP:
      return 'eNAP - elektronische Nachhaltigkeitsprüfung';
    case EgfaModulType.GLEICHWERTIGKEIT:
      return 'Gleichwertigkeits-Check';
    case EgfaModulType.DEMOGRAFIE:
      return 'Demografie-Check';
    case EgfaModulType.GLEICHSTELLUNG:
      return 'Gleichstellungsorientierte Gesetzesfolgenabschätzung';
    case EgfaModulType.DISABILITY:
      return 'Disability Mainstreaming';
    case EgfaModulType.VERBRAUCHER:
      return 'Auswirkungen auf Verbraucherinnen und Verbraucher';
    case EgfaModulType.WEITERE:
      return 'Weitere wesentliche Auswirkungen';
    case EgfaModulType.EVALUIERUNG:
      return 'Evaluierung';
  }
};

const getModulStatus = (moduleStatus: EgfaModulStatus) => {
  switch (moduleStatus) {
    case EgfaModulStatus.MODUL_NO_DATA:
      return 'Modul nicht fertiggestellt';
    case EgfaModulStatus.MODUL_NO_NEWER_DATA:
      return 'Keine Aktualisierung verfügbar';
    case EgfaModulStatus.MODUL_FINISHED_NOT_USED:
      return 'Modul fertiggestellt, noch nicht übernommen';
    case EgfaModulStatus.MODUL_DATA_NEW_UPDATE:
      return 'Aktualisierung verfügbar';
    default:
      return '';
  }
};

export const Step1ImportEgfaData: FunctionComponent<Props> = ({
  name,
  compoundDocumentInfo,
  updateTable,
  setOkBtnDisabled,
}) => {
  const { t } = useTranslation();
  const { closeWizard } = useWizardVisibleDispatch(Wizards.EGFA);
  const [isLoading, setIsLoading] = useState<boolean>();
  const { document } = useFirstDocumentState();
  const { setDocument } = useFirstDocumentDispatch();
  const [documentId, setDocumentId] = useState('');
  const [fetchDocumentResponse, fetchDocument] = DocumentService.getById(documentId);
  const [egfaInfoResponse, fetchEgfaInfo] = CompoundDocumentService.getEgfaData(compoundDocumentInfo.id);
  const [egfaModulsInfo, setEgfaModulsInfo] = useState<EgfaModulInfo[]>([]);
  const [selectedEgfaModules, setSelectedEgfaModules] = useState(new Set<string>());
  const [createCopy, setCreateCopy] = useState(false);
  const { basePath } = useAppInfoState();

  useEffect(() => {
    fetchEgfaInfo();
  }, [compoundDocumentInfo]);

  const [importEgfaDataResponse, importEgfaData] = CompoundDocumentService.importEgfaData(
    compoundDocumentInfo.id,
    Array.from(selectedEgfaModules),
    createCopy,
  );

  useEffect(() => {
    if (!egfaInfoResponse.isLoading && !egfaInfoResponse.hasError && egfaInfoResponse.data) {
      setEgfaModulsInfo(egfaInfoResponse.data);
    }
  }, [egfaInfoResponse]);

  const onFinishHandler = useCallback(() => {
    if (!isLoading) {
      importEgfaData();
      setIsLoading(true);
    }
  }, [selectedEgfaModules, isLoading, createCopy]);

  useEffect(() => {
    if (!importEgfaDataResponse.isLoading) {
      if (importEgfaDataResponse.hasError) {
        displayMessage(t('lea.messages.importEgfaData.error'), 'error');
      } else {
        displayMessage(t('lea.messages.importEgfaData.success'), 'success');

        if (document && !createCopy) {
          setDocumentId(document.id);
        } else if (document && createCopy) {
          window.location.hash = `#${basePath}`;
          closeWizard();
        } else {
          updateTable?.(true);
          closeWizard();
        }
      }
    }
  }, [importEgfaDataResponse]);

  useEffect(() => {
    if (documentId) {
      fetchDocument();
    }
  }, [documentId]);

  useEffect(() => {
    if (!fetchDocumentResponse.isLoading && fetchDocumentResponse.data) {
      setDocument(fetchDocumentResponse.data);
      closeWizard();
    }
  }, [fetchDocumentResponse]);

  return (
    <>
      <Form name={name} layout="vertical" onFinish={onFinishHandler} className="import-egfa-data-step1">
        <HinweisComponent
          title={t(`lea.Wizard.EgfaWizard.Hinweis.title`)}
          content={t(`lea.Wizard.EgfaWizard.Hinweis.content`)}
        />
        <FormItemWithInfo
          label={''}
          rules={[
            {
              required: false,
              validator: () => {
                return Promise.resolve();
              },
            },
          ]}
        >
          <table className="egfa-info-table" cellSpacing="0">
            <thead className="egfa-table-head">
              <tr>
                <th>{t('lea.Wizard.EgfaWizard.tableColumns.checkBox')}</th>
                <th>{t('lea.Wizard.EgfaWizard.tableColumns.module')}</th>
                <th>{t('lea.Wizard.EgfaWizard.tableColumns.status')}</th>
                <th>{t('lea.Wizard.EgfaWizard.tableColumns.lastUpdated')}</th>
              </tr>
            </thead>
            <tbody className="egfa-table-body">
              {egfaModulsInfo.map((modulInfo) => {
                const lastUpdateDate = Filters.dateFromString(modulInfo.egfaDmUpdateDate);
                const lastUpdateTime = Filters.timeFromString(modulInfo.egfaDmUpdateDate);
                return (
                  <tr key={modulInfo.egfaModuleId}>
                    <td>
                      <Checkbox
                        onChange={(e) => {
                          if (e.target.checked) {
                            selectedEgfaModules.add(modulInfo.egfaModuleId);
                          } else {
                            selectedEgfaModules.delete(modulInfo.egfaModuleId);
                          }
                          setSelectedEgfaModules(new Set(selectedEgfaModules));
                          setOkBtnDisabled?.(selectedEgfaModules.size > 0 ? false : true);
                        }}
                        checked={selectedEgfaModules.has(modulInfo.egfaModuleId)}
                        disabled={
                          modulInfo.egfaDataStatus === EgfaModulStatus.MODUL_NO_DATA ||
                          modulInfo.egfaDataStatus === EgfaModulStatus.MODUL_NO_NEWER_DATA
                        }
                      ></Checkbox>
                    </td>
                    <td>{getModuleName(modulInfo.egfaModuleId)}</td>
                    <td>{getModulStatus(modulInfo.egfaDataStatus)}</td>
                    <td>
                      <div className="last-update-date">
                        {lastUpdateDate && lastUpdateTime ? `${lastUpdateDate} · ${lastUpdateTime}` : ''}
                      </div>
                      <div className="last-update-user">{modulInfo.egfaDmUpdateUser}</div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </FormItemWithInfo>
        <GfaNewVersionFormItem fieldName="gfa-new-version" setCreateCopy={setCreateCopy} />
      </Form>
    </>
  );
};
