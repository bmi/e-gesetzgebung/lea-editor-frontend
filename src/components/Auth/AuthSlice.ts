// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { InitialAuthState } from './AuthState';
import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: InitialAuthState,
  reducers: {
    success(state) {
      state.isAuthenticated = true;
    },
  },
});
export const { success } = authSlice.actions;
export const AuthReducer = authSlice.reducer;
