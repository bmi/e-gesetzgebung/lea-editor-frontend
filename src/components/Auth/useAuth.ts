// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
//
import { success } from '.';
import { useAppDispatch, useAppSelector } from '../Store';
import { BaseUriKeycloak } from '../../constants';
import { UserService } from '../../api/User/UserService';

type UseAuth = {
  checkAuth: () => void;
};

export const useIsAuthenticated = () => useAppSelector((state) => state.editorStatic.auth.isAuthenticated);

export const useAuth = (): UseAuth => {
  const dispatch = useAppDispatch();

  const redirect = () => {
    const redirectUrl = `${BaseUriKeycloak}${window.location.href}`;
    window.location.href = redirectUrl;
  };

  const checkAuthHandler = useCallback(() => {
    UserService.getUser()
      .then((response) => {
        response.status !== 200 ? redirect() : dispatch(success());
      })
      .catch((error) => console.error('checkAuthHandler Error', error));
  }, []);

  return {
    checkAuth: checkAuthHandler,
  };
};
