// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  combineReducers,
  StateFromReducersMapObject,
  ReducersMapObject,
  Reducer,
  AnyAction,
  EntityState,
} from '@reduxjs/toolkit';
import { WizardVisibleReducer, WizardVisibleState } from '../Wizard';
import { DocumentDTO, DocumentReducer } from '../../api/Document';
import { AppInfoReducer, AppInfoState } from '../App';
import { SynopsisDocumentReducer } from '../../api/Document/SynopsisDocumentSlice';
import { WizardState } from '../Wizard/WizardState';
import { WizardReducer } from '../Wizard/WizardSlice';
import { HomePageReducer, HomePageState } from '../Pages';
import { EnhancedStoreAsync, RootState } from '@plateg/theme/src/components/store';
import { AuthReducer, AuthState } from '../Auth';
import { CellStructureState } from '../../lea-editor/plugins/synopsis/cellStructure/CellStructureState';
import { CellStructureReducer } from '../../lea-editor/plugins/synopsis/cellStructure/CellStructureSlice';
import { DocumentsTableActionReducer } from '../Pages/DocumentsTableActionSlice';
import { DocumentsTableActionState } from '../Pages/DocumentsTableActionState';

type StaticReducers = {
  wizardVisible: Reducer<WizardVisibleState, AnyAction>;
  wizard: Reducer<WizardState, AnyAction>;
  documents: Reducer<EntityState<DocumentDTO>, AnyAction>;
  synopsisDocuments: Reducer<EntityState<DocumentDTO>, AnyAction>;
  appInfo: Reducer<AppInfoState, AnyAction>;
  homePage: Reducer<HomePageState, AnyAction>;
  auth: Reducer<AuthState, AnyAction>;
  cells: Reducer<CellStructureState, AnyAction>;
  documentsTableAction: Reducer<DocumentsTableActionState, AnyAction>;
  [name: string]: Reducer;
};

const staticReducers: StaticReducers = {
  wizardVisible: WizardVisibleReducer,
  wizard: WizardReducer,
  documents: DocumentReducer,
  synopsisDocuments: SynopsisDocumentReducer,
  appInfo: AppInfoReducer,
  homePage: HomePageReducer,
  auth: AuthReducer,
  cells: CellStructureReducer,
  documentsTableAction: DocumentsTableActionReducer,
};

export const createReducer = (asyncReducers?: ReducersMapObject) => {
  return combineReducers({
    ...staticReducers,
    ...asyncReducers,
  });
};
export type AppDispatch = EnhancedStoreAsync['dispatch'];
// Infer the `RootState` and `AppDispatch` types from the store itself
export type EditorRootState = RootState & { editorStatic: StateFromReducersMapObject<typeof staticReducers> };
