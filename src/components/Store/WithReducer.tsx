// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { Reducer } from '@reduxjs/toolkit';
import { store } from '@plateg/theme/src/components/store';
interface WithReducerProps {
  keyProp: string;
  reducer: Reducer;
  children: React.ReactNode;
}
export const WithReducer = ({ keyProp, reducer, children }: WithReducerProps): ReactElement => {
  const [firstRender, setFirstRender] = useState(true);
  useMemo(() => {
    setFirstRender(true);
  }, [keyProp, reducer]);

  useMemo(() => {
    firstRender &&
      setTimeout(() => {
        store.injectReducer(keyProp, reducer);
        setFirstRender(false);
      });
  }, [firstRender]);

  useEffect(() => {
    return () => {
      store.removeReducer(keyProp);
    };
  }, []);

  return !firstRender ? <>{children}</> : <></>;
};
