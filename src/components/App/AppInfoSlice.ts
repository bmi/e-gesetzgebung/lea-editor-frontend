// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { InitialAppInfoState } from './AppInfoState';
import { SynopsisType } from '../../constants';

const appInfoSlice = createSlice({
  name: 'app',
  initialState: InitialAppInfoState,
  reducers: {
    setBasePath(state, action: PayloadAction<string>) {
      state.basePath = action.payload;
    },
    setSynopsisType(state, action: PayloadAction<SynopsisType>) {
      state.synopsisType = action.payload;
    },
  },
});

export const { setBasePath, setSynopsisType } = appInfoSlice.actions;
export const AppInfoReducer = appInfoSlice.reducer;
