// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useContext, useMemo } from 'react';
//
import { DocumentPage, HomePage, VersionHistoryPage, DrucksachePage } from '../Pages';
import { ProtectedRoute, Redirect, Route, Switch, useRouteMatch } from '../Router';
//
import NotificationProvider from '../Elements/notifications/NotificationProvider';
import LeaNotification from '../Elements/notifications/LeaNotification';

import './AppEmbedded.less';
import { StandaloneContext } from './App';
import { WithReducer, createReducer } from '../Store';
import AppInit from './AppInit';

export const AppEmbedded: FunctionComponent = () => {
  let { path } = useRouteMatch();
  const isStandalone = useContext(StandaloneContext);

  const classNameAppVersion = isStandalone ? 'lea-standalone' : 'lea-embedded';

  /**
   * Small workaround:
   * To get the correct paths for EmbeddedApp and StandaloneApp version.
   * - In StandaloneApp the path is "/"
   * - In EmbeddedApp (platform) the path is "/editor"
   */
  path = path === '/' ? '' : path;

  const getRedirectComponent = () => {
    return <Redirect to={`${path}/`} />;
  };

  const reducer = useMemo(() => createReducer(), []);
  return (
    <WithReducer reducer={reducer} keyProp="editorStatic">
      <AppInit path={path}>
        <NotificationProvider>
          <LeaNotification />
          <div className={`lea ${classNameAppVersion}`}>
            <Switch>
              <ProtectedRoute exact path={`${path}/`} component={HomePage} />
              {/** 
        <Route path={`${path}/stammgesetz-regelungstext`} component={StammgesetzRegelungstext} />
      */}
              <ProtectedRoute path={`${path}/document/:documentId`} component={DocumentPage} />
              <ProtectedRoute path={`${path}/versionHistory/:regulatoryProposalId`} component={VersionHistoryPage} />
              <ProtectedRoute path={`${path}/druck/:compoundDocumentId`} component={DrucksachePage} />
              <Route path={`${path}/*`} component={getRedirectComponent} />
            </Switch>
          </div>
        </NotificationProvider>
      </AppInit>
    </WithReducer>
  );
};
