// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import { Router } from '../Router';
import { AppEmbedded } from './AppEmbedded';
//

export const AppStandalone: FunctionComponent = () => {
  return (
    <Router>
      <AppEmbedded />
    </Router>
  );
};
