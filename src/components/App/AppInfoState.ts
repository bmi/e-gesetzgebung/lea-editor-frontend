// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { SynopsisType } from '../../constants';

type AppInfoState = {
  basePath: string;
  synopsisType: SynopsisType;
};

const InitialAppInfoState: AppInfoState = {
  basePath: '',
  synopsisType: SynopsisType.None,
};

export { AppInfoState, InitialAppInfoState };
