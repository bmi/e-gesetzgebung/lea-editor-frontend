// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, ReactElement, useEffect } from 'react';
//
//
import { useAppInfoDispatch } from './useAppInfo';

type Props = {
  path: string;
  children: ReactElement;
};

const AppInit: FunctionComponent<Props> = ({ children, path }) => {
  const { setBasePath } = useAppInfoDispatch();

  /**
   * Update/Set the global basePath to be able to use it later for routing.
   *
   * Example:
   * The link to the home page:
   *    In the standalone version => "/"
   *    In the embedded version => "/editor".
   *
   * Since we don't know the "basePath" of the platform we have to set it here on first load to be able to reference it later.
   *
   * Example: <Link to={`${basePath}/`}>Link to home page</Link>
   */
  useEffect(() => {
    setBasePath(path);
  }, []);
  return <>{children}</>;
};

export default AppInit;
