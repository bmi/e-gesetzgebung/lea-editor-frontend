// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useCallback } from 'react';
//
import { setBasePath, setSynopsisType } from './AppInfoSlice';
import { useAppSelector, useAppDispatch } from '../Store';
import { SynopsisType } from '../../constants';

type UseAppInfoState = {
  basePath: string;
  synopsisType: SynopsisType;
};

type UseAppInfoDispatch = {
  setBasePath: (basePath: string) => void;
  setSynopsisType: (synpsisType: SynopsisType) => void;
};

export const useAppInfoState = (): UseAppInfoState => {
  const { basePath, synopsisType } = useAppSelector((state) => state.editorStatic.appInfo);

  return {
    basePath: basePath,
    synopsisType: synopsisType,
  };
};

export const useAppInfoDispatch = (): UseAppInfoDispatch => {
  const dispatch = useAppDispatch();

  const setBasePathHandler = useCallback((basePath: string) => dispatch(setBasePath(basePath)), []);
  const setSynopsisTypeCallback = useCallback((synpsisType: SynopsisType) => {
    dispatch(setSynopsisType(synpsisType));
  }, []);

  return {
    setBasePath: setBasePathHandler,
    setSynopsisType: setSynopsisTypeCallback,
  };
};
