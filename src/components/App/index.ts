// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export * from './AppInfoSlice';
export * from './AppInfoState';
export * from './useAppInfo';
export * from './AppInit';

export { default as Application } from './App';
