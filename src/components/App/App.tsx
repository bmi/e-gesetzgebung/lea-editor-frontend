// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, createContext } from 'react';
//
import { AppEmbedded } from './AppEmbedded';
import { AppStandalone } from './AppStandalone';
import { StyleProvider, px2remTransformer } from '@ant-design/cssinjs';
//
import './App.css';
import './App.less';
import '../../print.less';

type Props = {
  isStandalone?: boolean;
};

const px2rem = px2remTransformer({
  rootValue: 10, // 10px = 1rem;
});

export const StandaloneContext = createContext(false);

const AppVersion = ({ isStandalone }: Props) => (isStandalone ? <AppStandalone /> : <AppEmbedded />);

const App: FunctionComponent<Props> = ({ isStandalone = false }) => {
  return (
    <StandaloneContext.Provider value={isStandalone}>
      <StyleProvider transformers={[px2rem]}>
        <AppVersion isStandalone={isStandalone} />
      </StyleProvider>
    </StandaloneContext.Provider>
  );
};

export default App;
