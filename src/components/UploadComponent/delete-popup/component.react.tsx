// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, ButtonProps, Popover } from 'antd';
import Text from 'antd/lib/typography/Text';
import FocusTrap from 'focus-trap-react';
import React, { ReactElement, useEffect, useRef, useState } from 'react';

import { ExclamationCircleFilled } from '@plateg/theme';
interface DeletePopupComponentProps {
  handleDelete: () => void;
  id: string;
  title: string;
  content: string;
  deleteBtnText: string;
  cancelBtnText: string;
  triggerBtnProps: ButtonProps;
}

export function DeletePopupComponent(props: DeletePopupComponentProps): ReactElement {
  const deleteRef = useRef<HTMLAnchorElement>(null);
  const [opened, setOpened] = useState(false);
  const [trapped, setTrapped] = useState(false);

  useEffect(() => {
    if (!opened) {
      setTrapped(opened);
    }
  }, [opened]);

  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <Text className="ant-popover-message-title">{props.title}</Text>
    </>
  );

  const popoverContent = (
    <FocusTrap active={!opened ? opened : trapped} autoFocus>
      <div style={{ maxWidth: '300px' }}>
        <p style={{ lineHeight: '20px', fontSize: '16px', textOverflow: 'ellipsis', overflow: 'hidden' }}>
          {props.content}
        </p>
        <div className="ant-popover-buttons">
          <Button
            id={`lea-handleFileDelete-btn-${props.id}`}
            onClick={() => {
              setOpened(false);
              props.handleDelete();
            }}
            type="primary"
          >
            {props.deleteBtnText}
          </Button>
          <Button id={`lea-abortFileDelete-btn-${props.id}`} onClick={() => setOpened(false)} type="default">
            {props.cancelBtnText}
          </Button>
        </div>
      </div>
    </FocusTrap>
  );

  return (
    <>
      <Popover
        title={popoverTitle}
        open={opened ? opened : trapped}
        placement="top"
        content={popoverContent}
        afterOpenChange={(visible) => {
          visible && setTrapped(visible);
        }}
      >
        <Button {...props.triggerBtnProps} onClick={() => setOpened(true)} ref={deleteRef}></Button>
      </Popover>
    </>
  );
}
