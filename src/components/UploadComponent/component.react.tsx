// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './upload-component.less';

import { Upload } from 'antd';
import Text from 'antd/lib/typography/Text';
import { UploadChangeParam } from 'antd/lib/upload';
import { UploadFile } from 'antd/lib/upload/interface';
import { UploadRequestOption } from 'rc-upload/lib/interface';
import React, { ReactElement, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { ExclamationMarkRed, FormItemWithInfo, HiddenInfoComponent, ModalWrapper, UploadOutlined } from '@plateg/theme';

import { FileRenameModalComponent } from './fileRenameModalComponent/component.react';
import { ItemRendererComponent } from './itemRendererComponent/component.react';

import { getFileExtension, incFileNameVersionNumber } from '../../lea-editor/plugin-core/utils';

interface UploadProps {
  fileList?: UploadFile[];
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  fileFormat: string;
  fileSize: number;
  onlySingleFileAllowed: boolean;
  fileNameSuggestion?: string;
  required?: boolean;
  restrictRenaming: boolean;
  messagesConfig: MessageItem;
  version?: number;
  isBearbeiten: boolean;
  uploadButtonRef?: React.Ref<HTMLElement>;
  errorProps: {
    errorTitle: string;
    formatString: string;
  };
}

interface MessageItem {
  [name: string]: string | ReactElement;
}

export function UploadComponent(props: UploadProps): ReactElement {
  const fileList = props.fileList;
  const setFileList = props.setFileList;
  const fileFormat = props.fileFormat;
  const fileSize = props.fileSize;
  const onlySingleFileAllowed = props.onlySingleFileAllowed;
  const restrictRenaming = props.restrictRenaming;
  const [uploadDisabled, setUploadDisabled] = useState<boolean>(false);
  const [renameModalVisibility, setRenameModalVisibility] = useState<boolean>(false);
  const [selectedFileForRenaming, setSelectedFileForRenaming] = useState<UploadFile>();
  const generatedID = uuidv4();
  const [errorModalVisivility, setErrorModalVisibility] = useState<boolean>(false);

  const { Dragger } = Upload;

  useEffect(() => {
    // Used to check if only a single upload it allowed, if yes disabled if needed
    if (fileList && fileList.length > 0 && onlySingleFileAllowed) {
      setUploadDisabled(true);
    } else {
      setUploadDisabled(false);
    }
  }, [fileList]);

  useEffect(() => {
    // Used to change the name of already choosen files
    const updateFileList = fileList?.map((file) => {
      file.name = incFileNameVersionNumber(file.name, true, props.fileNameSuggestion, props.version);
      return file;
    });
    setFileList(updateFileList ?? []);
  }, [props.fileNameSuggestion]);

  useEffect(() => {
    if (!renameModalVisibility) {
      document.getElementById(`lea-dateiUmbenennen-btn-${selectedFileForRenaming?.uid || ''}`)?.focus();
    }
  }, [renameModalVisibility]);

  const onFileChange = (info: UploadChangeParam) => {
    // If file size does not fit, the status will be error and we remove it from the list
    if (info.file.status === 'error') {
      info.fileList.splice(info.fileList.indexOf(info.file), 1);
    } else {
      // Will rename the file, when uploaded and a fileNameSuggestion is there
      info.file.name = incFileNameVersionNumber(info.file.name, true, props.fileNameSuggestion, props.version);
    }
    setFileList(info.fileList);
  };

  // dummy request to trick Upload component
  const dummyRequest = (options: UploadRequestOption) => {
    const dummy = new XMLHttpRequest();
    setTimeout(() => {
      if (options.onSuccess) {
        options.onSuccess({}, dummy);
      }
    }, 0);
  };

  const itemRender = (_originNode: ReactElement, file: UploadFile, fileListLocal?: UploadFile[]) => {
    return (
      <ItemRendererComponent
        file={file}
        fileList={fileListLocal}
        setFileList={setFileList}
        renameHandler={renameHandler}
        disabledRename={restrictRenaming && props.fileNameSuggestion === undefined}
        isBearbeiten={props.isBearbeiten}
      />
    );
  };

  function beforeUpload(uploadFile: UploadFile) {
    if (
      (uploadFile.size && uploadFile.size > fileSize) ||
      !fileFormat.split(',').includes(getFileExtension(uploadFile.name).toLowerCase())
    ) {
      uploadFile.status = 'error';
      setErrorModalVisibility(true);
      return false;
    }

    return true;
  }

  const renameHandler = (file: UploadFile) => {
    setRenameModalVisibility(true);
    setSelectedFileForRenaming(file);
  };

  const setFileListAfterRename = (file: UploadFile) => {
    const newFilenameFileList = props.fileList?.map((oldFile) => (oldFile.uid === file.uid ? file : oldFile));
    setFileList(newFilenameFileList ?? []);
  };

  const { t } = useTranslation();

  const label = (
    <span>
      {t('lea.Wizard.ImportDocumentWizard.upload.uploadFileTitle.title')}
      <span className="infoRequired">
        <HiddenInfoComponent
          title={t(`lea.Wizard.ImportDocumentWizard.upload.uploadFileTitle.infoComponent.title`)}
          text={t(`lea.Wizard.ImportDocumentWizard.upload.uploadFileTitle.infoComponent.text`)}
        />
      </span>
    </span>
  );

  return (
    <div className="upload-holder">
      <ModalWrapper
        title={
          <h3>
            <ExclamationMarkRed style={{ verticalAlign: 'top' }} /> {props.errorProps.errorTitle}
          </h3>
        }
        open={errorModalVisivility}
        onOk={() => {
          setErrorModalVisibility(false);
        }}
        okText={t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadErrorConfirm')}
        cancelButtonProps={{ style: { visibility: 'hidden' } }}
        closable={false}
      >
        <Text>
          {t('lea.Wizard.ImportDocumentWizard.upload.messages.uploadError', {
            fileSize: props.fileSize / 1000000,
            format: props.errorProps.formatString,
          })}
        </Text>
      </ModalWrapper>
      <div id={`file-name-modal${generatedID}`}>
        <FileRenameModalComponent
          setFileListAfterRename={setFileListAfterRename}
          fileForRenaming={selectedFileForRenaming}
          visibility={renameModalVisibility}
          setVisibility={setRenameModalVisibility}
          title={props.messagesConfig.changeFileNameTitle as string}
          cancelText={props.messagesConfig.changeFileNameCancelText as string}
          itemLabel={props.messagesConfig.changeFileNameItemLabel as string}
          errorMsg={props.messagesConfig.changeFileNameErrorMsg as string}
          id={`${generatedID}`}
        />
      </div>
      <FormItemWithInfo
        name={'document-import'}
        id={`upload${generatedID}`}
        rules={[
          {
            validator: () => {
              if (!fileList?.length) {
                return Promise.reject(t(`lea.Wizard.ImportDocumentWizard.upload.required`));
              } else {
                return Promise.resolve();
              }
            },
            validateTrigger: 'onBlur',
          },
        ]}
        label={label}
      >
        <Dragger
          fileList={fileList}
          name="file"
          customRequest={dummyRequest}
          onChange={onFileChange}
          itemRender={itemRender}
          accept={fileFormat}
          beforeUpload={beforeUpload}
          disabled={uploadDisabled}
          multiple={!onlySingleFileAllowed}
        >
          <p className="ant-upload-drag-icon">
            <UploadOutlined />
          </p>
          <p className="ant-upload-text">{props.messagesConfig.uploadBtn}</p>
          <p className="ant-upload-hint">{t('lea.Wizard.ImportDocumentWizard.upload.info')}</p>
        </Dragger>
      </FormItemWithInfo>
    </div>
  );
}
