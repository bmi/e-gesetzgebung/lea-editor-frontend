// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';
import { DocumentOutlined } from '@plateg/theme/src/components/icons/DocumentOutlined';
import { EditOutlined } from '@plateg/theme/src/components/icons/EditOutlined';

import { DeletePopupComponent } from '../delete-popup/component.react';
interface ItemRendererProps {
  file: UploadFile;
  fileList?: UploadFile[];
  setFileList: Function;
  renameHandler: Function;
  disabledRename: boolean;
  isBearbeiten: boolean;
}

export function ItemRendererComponent(props: ItemRendererProps): ReactElement {
  const file = props.file;
  const fileList = props.fileList;
  const setFileList = props.setFileList;
  const renameHandler = props.renameHandler;
  const { t } = useTranslation();

  const handleDelete = () => {
    if (fileList && fileList?.length > 0) {
      setFileList(
        fileList.filter((el) => {
          return el.uid !== file.uid;
        }),
      );
    }
  };

  const handleRename = () => {
    renameHandler(file);
  };

  return (
    <Row className="item-row">
      <Col className="ant-col-2 blue-text-button">
        <DocumentOutlined />
      </Col>
      <Col className="ant-col-19">
        <Text className="blue-text-button">{file.name}</Text>
      </Col>
      <Col className="ant-col-1">
        <Button
          id={`lea-dateiUmbenennen-btn-${file.uid}`}
          disabled={props.disabledRename || props.isBearbeiten}
          onClick={handleRename}
          type="text"
          aria-label={t('lea.Wizard.ImportDocumentWizard.upload.rename')}
          icon={<EditOutlined />}
        ></Button>
      </Col>
      <Col className="ant-col-1">
        <div className="vertical-seperator"></div>
      </Col>
      <Col className="ant-col-1">
        <DeletePopupComponent
          title={t('lea.Wizard.ImportDocumentWizard.upload.popover.title')}
          content={t('lea.Wizard.ImportDocumentWizard.upload.popover.text', { title: file.name })}
          deleteBtnText={t('lea.Wizard.ImportDocumentWizard.upload.popover.delete')}
          cancelBtnText={t('lea.Wizard.ImportDocumentWizard.upload.popover.cancel')}
          handleDelete={handleDelete}
          id={file.uid}
          triggerBtnProps={{
            id: `lea-hochgeladenDateiLoeschen-btn-${file.uid}`,
            disabled: props.isBearbeiten,
            type: 'text',
            icon: <DeleteOutlined />,
            'aria-label': t('lea.Wizard.ImportDocumentWizard.upload.delete'),
          }}
        />
      </Col>
    </Row>
  );
}
