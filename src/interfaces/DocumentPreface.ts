// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentType } from '../api';

export interface DocumentPrefaceProps {
  title: string;
  shortTitle: string;
  proponent: string;
  abbreviation: string;
  type?: DocumentType;
}
