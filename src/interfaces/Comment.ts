// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ReactElement } from 'react';
import { CommentResponseDTO, CreateCommentDTO, ReplyDTO } from '../api/Comment';

export interface CommentWithRef extends CommentResponseDTO {
  isActive?: boolean;
  serializedContent?: ReactElement;
  organizedReplies?: ReplyWithMetadata[];
}

export interface NewCommentWithRef extends CreateCommentDTO {
  startRef?: React.MutableRefObject<HTMLElement>;
  endRef?: React.MutableRefObject<HTMLElement>;
}

export interface ReplyWithMetadata extends ReplyDTO {
  serializedContent: ReactElement;
}

export interface IEditComment {
  id?: string;
  replyId?: string;
  content: string;
}
