// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseText } from 'slate';

export interface TextWrapper {
  type: string;
  text?: string;
  children: BaseText[];
}

export interface ChangeWrapper extends TextWrapper {
  'lea:changeType': string;
}

export interface CommentMarkerWrapper {
  type: string;
  children: CommentMarker[];
}

export interface CommentMarker {
  type: string;
  'lea:commentId': string;
  'lea:commentPosition': string;
  color?: string;
  children: BaseText[];
  isActive?: boolean;
  hidden?: boolean;
}
