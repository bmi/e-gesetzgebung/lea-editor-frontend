// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseText } from 'slate';
import { TextWrapper } from './TextWrapper';
import { Element } from './Element';

export type LeaNode = Element | TextWrapper | BaseText;
