// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BaseElement } from 'slate';
import { ChangeWrapper, TextWrapper } from './TextWrapper';
import { findingCategory } from '../lea-editor/plugins/hdrAssistent/HdrAssistentPlugin';

enum ChangeMark {
  ADDED = 'added',
  CHANGED = 'changed',
  DELETED = 'deleted',
  MOVED = 'moved',
  OMITTED = 'omitted',
  UNMODIFIED = 'unmodified',
  EMPTY = 'empty',
}

export interface Element extends BaseElement {
  GUID?: string;
  type: string;
  children: (Element | TextWrapper | ChangeWrapper)[];
  refersTo?: string;
  period?: string;
  href?: string;
  name?: string;
  marker?: string;
  placement?: string;
  placementBase?: string;
  value?: string;
  'lea:changeMark'?: ChangeMark;
  refGUID?: string;
  'lea:changeType'?: string;
  text?: string;
  style?: string;
  status?: string;
  'xml:id'?: string;
  'lea:editable'?: boolean;
  'lea:fromEgfa'?: boolean;
  'lea:hdrIssue'?: findingCategory;
  colspan?: number;
  'lea:isActive'?: boolean;
  'lea:commentColor'?: string;
  'lea:hidden'?: boolean;
  'lea:commentsOnElement'?: string[];
  startQuote?: string;
  endQuote?: string;
}
