// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { HistoryEditor } from 'slate-history';
import { ReactEditor } from 'slate-react';

export interface Editor {
  id: string;
  editor: ReactEditor & HistoryEditor;
  isRemote: boolean;
}
