// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { PropsWithChildren } from 'react';

interface IButtonProps {
  id: string;
  format: string;
  label: string;
  text?: string;
  disabled?: boolean;
  className?: string;
  tabIndex?: number;
  ref?:
    | ((instance: HTMLElement | null) => void)
    | React.RefObject<HTMLElement>
    | React.MutableRefObject<HTMLElement | undefined>
    | null;
}

export type ButtonProps = PropsWithChildren<IButtonProps>;

// change file name to FormElementProps

export type CheckboxValueType = string | number | boolean;
