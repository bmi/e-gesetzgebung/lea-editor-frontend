// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de } from './de';
import { de as editor_theme_de } from '@lea/ui';

export const messages = {
  de: {
    translation: { ...de, ...editor_theme_de },
  },
};
