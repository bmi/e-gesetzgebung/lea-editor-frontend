// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  IMAGE_RESIZE_MAX_HEIGHT_IN_CM,
  IMAGE_RESIZE_MAX_WIDTH_IN_CM,
  IMAGE_RESIZE_MIN_VALUE,
} from '../../../constants';

/**
 * The "plateg-cockpit" internally uses the module name "lea" for the editor,
 * which is why the scope "lea" is also used here.
 */
export const de = {
  lea: {
    Wizard: {
      CreateNewCompoundDocumentWizard: {
        title: 'Neue Dokumentenmappe anlegen',
        okBtnText: 'Dokumentenmappe jetzt anlegen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1CompoundDocumentSpecifications: {
          title: 'Schritt 1 von 2: Dokumentenmappe konfigurieren',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
        Step2RegulatoryTextSpecifications: {
          title: 'Schritt 2 von 2: Regelungstext konfigurieren',
          subTitle: 'Zur Vereinfachung wird ein Regelungstext zusammen mit Ihrer neuen Dokumentenmappe angelegt.',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
      },
      ExportCompoundDocumentWizard: {
        title: 'Dokumente exportieren',
        okBtnText: 'Exportieren',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1SelectCompoundDocument: {
          title: 'Dokumentenmappe: {{title}}',
        },
      },
      CreateNewDocumentWizard: {
        title: 'Neues Einzeldokument anlegen',
        okBtnText: 'Einzeldokument jetzt anlegen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1SelectDocument: {
          title: 'Schritt 1 von 2: Dokumentart auswählen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
        Step2DocumentSpecifications: {
          title: 'Schritt 2 von 2: Dokumentvorgaben machen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
      },
      ImportDocumentWizard: {
        title: 'Neues Dokument importieren',
        okBtnText: 'Dokument jetzt importieren',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1SelectDocument: {
          title: 'Schritt 1 von 2: Dokument auswählen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
        Step2DocumentSpecifications: {
          title: 'Schritt 2 von 2: Dokumentvorgaben machen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        },
        upload: {
          required: 'Bitte laden Sie ein Dokument hoch.',
          mainTitle: 'Dateien hochladen',
          delete: 'Datei löschen',
          rename: 'Datei umbenennen',
          uploadDrawerText:
            '<p>Mit Upload der Dokumente wird bestätigt, dass diese keine personenbezogenen Daten nach <strong>Art. 4 Nr. 1 DSGVO</strong> beinhalten, die personenbezogenen Daten anonymisiert wurden oder eine Einwilligung der betroffenen Person nach <strong>Art. 6 Nr. 1 lit. a DSGVO</strong> vorliegt.</p>',
          uploadButton: 'Dateien hochladen',
          info: 'Ziehen Sie die Dateien einfach in dieses Feld um den Upload zu starten oder klicken sie hinein um den Auswahldialog zu öffnen.',
          uploadFileTitle: {
            title: 'LegalDocML.de Dokument hochladen (XML-Dokument, max. 20 MB)',
            infoComponent: {
              title: 'Erklärung zu: LegalDocML.de Dokument hochladen',
              text: 'Hier kann ein XML Dokument hochgeladen werden, welches ein gültiges LegalDocML.de ist und zuvor im E-Gesetzgebung Editor exportiert wurde.',
            },
          },
          uploadFileFormatShort: 'XML',
          uploadChangeFileNameLabel: 'Neuer Dateiname',
          uploadChangeFileNameCancelTextButton: 'Abbrechen',
          uploadChangeFileNameModalTitel: 'Dateinamen ändern',
          messages: {
            uploadErrorConfirm: 'Okay, gelesen',
            uploadError:
              'Dieses Dateiformat kann nicht verarbeitet werden. Bitte laden Sie die Datei im Format {{format}} erneut hoch. Achten Sie dabei auf die maximale Dateigröße von {{fileSize}} MB.',
            uploadErrorFileNameLength: 'Maximale Länge der Dateiname {{maxChars}} wurde überschritten',
            uploadChangeFileNameError: 'Bitte geben Sie einen Dateinamen ein.',
          },
          popover: {
            title: 'Möchten Sie das Dokument löschen?',
            text: 'Wenn Sie das hochgeladene Dokument „{{title}}“ löschen, wird es hier entfernt.',
            delete: 'Löschen',
            cancel: 'Abbrechen',
          },
        },
        FormItems: {
          ImportStatusFormItem: {
            label: 'Arbeitsentwurf',
            requiredMsg: 'Bitte wählen Sie einen Arbeitsentwurf aus.',
            placeholder: 'Bitte auswählen',
            infoComponent: {
              arbeitsentwurf: {
                title: 'Erklärung zu: Arbeitsentwurf',
                text: 'Als Arbeitsentwurf werden jene Rechtsetzungsdokumente bezeichnet, die sich in der Erstellungsphase befinden und formal noch nicht durch das Kabinett gebilligt wurden.',
              },
            },
          },
        },
      },
      SynopsisWizard: {
        title: '{{header}} konfigurieren',
        okBtnText: '{{header}} jetzt starten',
        Step1SelectDocument: {
          noteTitle: 'Parallelansicht',
          noteTextSynopsis:
            'Sie können eine Parallelansicht mit mehreren Spalten anlegen oder eine bereits erstellte Parallelansicht neu konfigurieren.',
          noteTextAenderungsvergleich:
            'Sie können einen Änderungsvergleich mit mehreren Spalten anlegen oder einen bereits erstellten Änderungsvergleich neu konfigurieren.',
          addColumn: 'Weitere Spalte hinzufügen',
          deleteColumn: 'Dokument und Spalte entfernen',
        },
      },
      SynchronousScrollingWizard: {
        title: 'Synchrones Scrollen',
        okBtnText: 'Ok, fortfahren',
        cancelBtnText: 'Abbrechen und zurück',
        Step1SynchronousScrolling: {
          title: 'Synchrones Scrollen aktivieren',
          message:
            'Beim Aktivieren des Toggles kann sich ggf. die Anzeige ändern. Sind bis dahin Spalten unterschiedlich gescrollt, werden sie nach Aktivierung der Funktion auf die gleiche Höhe gebracht, sodass die Scrollleisten die gleiche Höhe haben.',
        },
      },
      FormatAuthorialNoteWizard: {
        title: 'Fußnotenformat bearbeiten',
        okBtnText: 'Fußnotenformat anpassen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',

        Step1FormatAuthorialNote: {
          title: 'Format der Fußnote',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          FormItems: {
            AuthorialNoteTypeFormItem: {
              label: 'Format auswählen',
              numbered: 'Nummerierte Fußnote (1, 2, 3, ...)',
              custom: 'Benutzerdefiniertes Symbol',
            },
            AuthorialNoteMarkerFormItem: {
              label: 'Benutzerdefiniertes Symbol eingeben',
              requiredMsg: 'Bitte geben Sie ein Symbol ein.',
            },
          },
        },
      },
      RenameDocumentWizard: {
        title: '{{documentType}} umbenennen',
        okBtnText: 'Neuen Titel speichern',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1RenameDocument: {
          title: 'Neuen Titel eingeben',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          FormItems: {
            RenameDocumentFormItem: {
              label: 'Neuen Titel eingeben',
              requiredMsg: 'Bitte geben Sie einen Titel ein.',
            },
          },
        },
      },
      NewVersionWizard: {
        title: 'Neue Version anlegen',
        okBtnText: 'Neue Version anlegen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        Step1CreateNewVersion: {
          title: 'Neue Dokumentenmappe benennen und Dokumente auswählen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          FormItems: {
            SelectDocumentsFormItem: {
              label: 'Bitte wählen Sie die Dokumente aus',
              requiredMsg: '',
              infoComponent: {
                title: 'Erklärung zu: Anlage einer neuen Version einer Dokumentenmappe',
                text: 'Sie haben die Möglichkeit eins oder mehrere Dokumente für die Übernahme in die neue Version auszuwählen.',
              },
            },
            PinPreviousVersionFormItem: {
              label: 'Versionen verwalten',
              options: {
                moveToVersionHistory: 'Bisherige Version in die Versionshistorie verschieben',
                pinToHomepage: 'Bisherige Version auf der Startseite anheften',
                remainPinned:
                  'Die vorherige Dokumentenmappe ist bereits angeheftet. Die Dokumentenmappe angeheftet lassen.',
              },
            },
          },
        },
      },
      EgfaWizard: {
        title: 'Gesetzesfolgenabschätzung Module in Dokumente übernehmen',
        okBtnText: 'Gesetzesfolgenabschätzung Daten übernehmen',
        Hinweis: {
          title: 'Gesetzesfolgenabschätzung Datenübernahme',
          content:
            'Stellen Sie bitte sicher, dass Sie vor Übernahme der Daten eine Speicherung aller geöffneten Dokumente in der Dokumentenmappe vorgenommen haben.',
        },
        tableColumns: {
          checkBox: 'Auswahl',
          module: 'Module',
          status: 'Status',
          lastUpdated: 'Zuletzt übertragen',
        },
        cancelBtnText: 'Abbrechen',
        newVersionRadio: {
          label: 'Weiterarbeit in einer neuen Dokumentenmappen-Version',
          infoComponent: {
            title: 'Erklärung zu: Weiterarbeit in einer neuen Dokumentenmappen-Version',
            text: 'Mit der Anlage einer neuen Dokumentenmappen-Version stellen Sie bei der Übernahme bzw. Aktualisierung von Gesetzesfolgenabschätzung-Inhalten sicher, dass keine Zwischenstände verloren gehen.',
          },
          options: { yes: 'Ja', no: 'Nein' },
        },
      },
      SetDocumentReadRightsWizard: {
        title: 'Dokumentenmappe "{{title}}": Leserechte vergeben',
        okBtnText: 'Leserechte jetzt vergeben',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        emailAddress: {
          title: 'Personen auswählen',
          requiredMsg: 'Pflichtfelder sind mit einem * markiert',
          label: 'Leseberechtigungen',
          search: {
            label: 'Suche nach Personen',
            drawer: {
              title: 'Suche nach Personen',
              text: '',
            },
            hint: '(z.B. Name, Ressort, Verteiler, E-Mail-Adresse)',
          },
          drawer: {
            title: 'Leseberechtigungen',
            text: '',
          },
        },
      },
      SetDocumentWriteRightsWizard: {
        title: 'Dokumentenmappe "{{title}}": Schreibrechte weitergeben',
        okBtnText: 'Schreibrechte jetzt weitergeben',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        cancelBtnText: 'Abbrechen',
        emailAddress: {
          title: {
            label: 'Schritt 1 von 3: Bearbeiter/innen auswählen',
            drawer: {
              title: 'Information',
              text: '',
            },
          },
          requiredMsg: 'Pflichtfelder sind mit einem * markiert',
          label: 'Bearbeiter/innen',
          search: {
            label: 'Suche nach Bearbeiter/innen*',
            drawer: {
              title: 'Suche nach Bearbeiter/innen',
              text: '',
            },
            hint: '(z.B. Name, Ressort, Verteiler, E-Mail-Adresse)',
          },
          drawer: {
            title: 'Schreibberechtigungen',
            text: '',
          },
        },
        Step2DocumentSpecifications: {
          title: 'Schritt 2 von 3: Frist und Anmerkung ergänzen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          remarks: { title: 'Anmerkungen' },
          timeLimit: { title: 'Zeitraum bis zum Fristablauf wählen', requiredMsg: 'Bitte geben Sie eine Frist an' },
        },
        Step3DocumentSpecifications: {
          title: 'Schritt 3 von 3: Weiterarbeit in neuer Version',
          okBtnText: 'Eingaben prüfen',
          createNewVersion: {
            label: 'Weiterarbeit in einer neuen Dokumentenmappen-Version',
            ariaLabel: 'Weiterarbeit in einer neuen Dokumentenmappen-Version',
          },
        },
        Step4DocumentSpecifications: {
          title: 'Eingaben prüfen',
          editor: 'Bearbeiterin oder Bearbeiter',
          timeLimit: 'Zeitraum bis zum Fristablauf',
          newVersion: 'Weiterarbeit in einer neuen Dokumentenmappen-Version',
          remarks: 'Anmerkungen',
        },
      },
      ExportPdfWizard: {
        title: 'Dokumentenmappe {{title}} exportieren',
        cancelBtnText: 'Abbrechen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        checkBtnText: 'Eingaben prüfen',
        okBtnText: 'Dokumentenmappe jetzt exportieren',
        Step1ExportPdf: {
          title: 'Schritt 1 von 2 Exportformat auswählen',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          compoundDocumentTitle: 'Dokumentenmappe: {{title}}',
          modeTitle: 'Exportieren als',
        },
        Step2SelectExportDocuments: {
          title: 'Schritt 2 von 2: Dokumente auswählen und Titel vergeben',
          requiredMsg: 'Pflichtfelder sind mit einem * gekennzeichnet.',
          chooseDocuments: 'Auswahl der Exportdokumente',
          chooseDocumentsRequiredMessage: 'Bitte wählen Sie Dokumente für den Export aus.',
          exportTitle: 'Titel',
        },
        Step3ExportSummary: {
          title: 'Eingaben Prüfen',
          compoundDocumentTitle: 'Ausgewählte Dokumentenmappe:',
          exportMode: 'Exportieren als:',
          selectedDocuments: 'Auswahl der Einzeldokumente:',
          exportTitle: 'Titel:',
          error: 'Fehler beim Export {{file}}',
          success: 'Die Dokumente der Dokumentenmappe {{title}} wurden erfolgreich exportiert.',
        },
      },
      AssignBestandsrechtWizard: {
        title: 'Impotiertes Bestandsrecht verknüpfen',
        cancelBtnText: 'Abbrechen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        okBtnText: 'Konfiguration jetzt abschließen',
        Step1AssignBestandsrecht: {
          title: 'Bestandsrechte mit Dokumentenmappen verknüpfen',
          infoComponent: {
            title: 'Bestandsrechte mit Dokumentenmappen verknüpfen',
            text: 'Hier können Sie bereits von Neu-RIS importiertes Bestandsrecht mit der ausgewählten Dokumentenmappe verknüpfen oder die bereits gesetzten Verknüpfungen lösen.',
          },
          list: {
            bestandsrechtTitleCol: 'Bestandsrecht',
            compoundDocumentTitleCol: 'Dokumentenmappe',
          },
          noBestandsrecht: 'Kein Bestandsrecht für dieses Regelungsvorhaben von Neu-RIS importiert.',
        },
        BestandsrechtChooseForm: {
          Toggle: {
            ariaLabel:
              'Verlinkung des Bestandsrechts: "{{bestandsrecht}}" zur Dokumentenmappe: "{{compoundDocument}}" {{active}}.',
            tooltip: 'Verlinkung des Bestandsrechts zur Dokumentenmappe {{active}}.',
          },
        },
      },
    },
    Document: {
      FormItems: {
        DocumentTypeFormItem: {
          label: 'Bitte wählen Sie aus',
          requiredMsg: 'Bitte wählen Sie eine Dokumentart aus.',
          radioValues: {
            REGELUNGSTEXT_STAMMGESETZ: 'Stammgesetz: Regelungstext',
            VORBLATT_STAMMGESETZ: 'Stammgesetz: Vorblatt',
            BEGRUENDUNG_STAMMGESETZ: 'Stammgesetz: Begründung',
            ANSCHREIBEN_STAMMGESETZ: 'Stammgesetz: Anschreiben',
            REGELUNGSTEXT_STAMMVERORDNUNG: 'Stammverordnung: Regelungstext',
            VORBLATT_STAMMVERORDNUNG: 'Stammverordnung: Vorblatt',
            BEGRUENDUNG_STAMMVERORDNUNG: 'Stammverordnung: Begründung',
            ANSCHREIBEN_STAMMVERORDNUNG: 'Stammverordnung: Anschreiben',
            REGELUNGSTEXT_MANTELGESETZ: 'Mantelgesetz: Regelungstext',
            BEGRUENDUNG_MANTELGESETZ: 'Mantelgesetz: Begründung',
            VORBLATT_MANTELGESETZ: 'Mantelgesetz: Vorblatt',
            ANSCHREIBEN_MANTELGESETZ: 'Mantelgesetz: Anschreiben',
            REGELUNGSTEXT_MANTELVERORDNUNG: 'Mantelverordnung: Regelungstext',
            BEGRUENDUNG_MANTELVERORDNUNG: 'Mantelverordnung: Begründung',
            VORBLATT_MANTELVERORDNUNG: 'Mantelverordnung: Vorblatt',
            ANSCHREIBEN_MANTELVERORDNUNG: 'Mantelverordnung: Anschreiben',
            ANLAGE: 'Anlage',
          },
          infoComponent: {
            REGELUNGSTEXT: {
              title: 'Erklärung zu: Regelungstext',
              text: 'Der Regelungstext ist ein wesentlicher Bestandteil eines Rechtsetzungsdokuments und beinhaltet dessen rechtsverbindliche Einzelvorschriften.',
            },
            VORBLATT: {
              title: 'Erklärung zu: Vorblatt',
              text: 'Ein Vorblatt, d. h. eine knappe Übersicht über Anlass, Inhalt, Haushaltsausgaben von Bund, Ländern und Gemeinden, über den Erfüllungsaufwand und weitere Kosten muss jedem Gesetzentwurf vorangestellt werden (vgl. § 42 Absatz 1 GGO). Der Aufbau bzw. die Gliederung eines solchen Vorblatts ist standardisiert (vgl. Anlage 3 zu § 42 Absatz 1 GGO).',
            },
            BEGRUENDUNG: {
              title: 'Erklärung zu: Begründung',
              text: 'Die Begründung ist obligatorischer Bestandteil des Regelungsentwurfs (vgl. §42 Abs. 1 GGO). Die Begründung wird üblicherweise in einen allgemeinen und einen besonderen Teil untergliedert.',
            },
            ANSCHREIBEN: {
              title: 'Erklärung zu: Anschreiben',
              text: 'Für ein Rechtsetzungsdokument in der Entwurfsfassung kann optional ein Anschreiben angelegt werden.',
            },
            ANLAGE: {
              title: 'Erklärung zu: Anlage',
              text: '',
            },
          },
        },
        CompoundDocumentFormItem: {
          label: 'Dokumentenmappe',
          requiredMsg: 'Bitte wählen Sie eine Dokumentenmappe aus.',
          placeholder: 'Bitte auswählen',
          infoComponent: {
            title: 'Erklärung zu: Dokumentenmappe',
            text: {
              CreateNewDocumentWizard:
                'Eine Dokumentenmappe ist ein Verzeichnis, in welchem Sie alle zu einem Rechtsetzungsdokument zugehörigen Dokumente anlegen können. Bevor Sie ein Dokument anlegen können, müssen Sie zuerst eine Dokumentenmappe anlegen. Es handelt sich hier nicht um ein Regelungsvorhaben gem. §§ 43, 44, 62 Absatz 2 und 70 Absatz 1 GGO das mit einer Gesetzesfolgenabschätzung zu versehen wäre.',
              ExportCompoundDocumentWizard:
                'Eine Dokumentenmappe ist ein Verzeichnis, in welchem Sie alle zu einem Rechtsetzungsdokument zugehörigen Dokumente anlegen können. Es werden alle Dokumente aus der von Ihnen ausgewählten Dokumentenmappe exportiert.',
              ImportDocumentWizard:
                'In einer Dokumentenmappe werden Einzeldokumente zu einem Rechtsetzungsdokument gebündelt.',
              ExportPdfWizard:
                'Eine Dokumentenmappe ist ein Verzeichnis, in welchem Sie alle zu einem Rechtsetzungsdokument zugehörigen Dokumente anlegen können. Im nächsten Schritt, können Sie die zu exportierenden Dokumente auswählen.',
              AssignBestandsrechtWizard:
                'In einer Dokumentenmappe werden Einzeldokumente zu einem Rechtsetzungsdokument gebündelt. Wählen Sie hier bitte die Dokumentenmappe aus, zu jener das Bestandsrecht zugewiesen werden soll.',
            },
          },
        },
        CompoundDocumentTypeFormItem: {
          label: 'Variante',
          requiredMsg: 'Bitte wählen Sie eine Dokumentenvariante aus.',
          placeholder: 'Bitte auswählen',
          infoComponent: {
            title: 'Erklärung zu: Variante',
            text: 'Für die Erstellung einer Dokumentenmappe ist es notwendig anzugeben, welche Dokumentenvariante mit der Dokumentenmappe abgebildet werden soll. Abhängig von dieser Wahl können unterschiedliche Dokumente in der Dokumentenmappe angelegt werden, welche gemeinsam ein gemäß LegalDocML.de gültiges Rechtsetzungsdokument bilden. Der aktuelle Stand der Entwicklung bietet die Anlage von Stamm- und Mantelgesetzen an.',
          },
        },
        CompoundDocumentRegelungsvorhabenFormItem: {
          label: 'Regelungsvorhaben',
          requiredMsg: 'Bitte wählen Sie ein Regelungsvorhaben aus.',
          placeholder: 'Bitte auswählen',
          infoComponent: {
            title: 'Erklärung zu: Regelungsvorhaben',
            text: 'Für die Erstellung einer Dokumentenmappe ist es notwendig, das entsprechende Regelungsvorhaben anzugeben. Ein Regelungsvorhaben beinhaltet ein Datenblatt mit allen Angaben, die für einen Rechtsakt relevant sind. Einige dieser Angaben werden in den der Dokumentenmappe zugehörigen Einzeldokumenten referenziert und automatisch eingefügt (z.B. die Bezeichnung). Diese Angaben können nur zentral in der Anwendung Regelungsvorhaben geändert werden.',
          },
        },
        CompoundDocumentTitleFormItem: {
          label: 'Titel der Dokumentenmappe',
          requiredMsg: 'Bitte geben Sie einen Titel an.',
          placeholder: 'Platzhalter für den Titel',
          emtyTitleMsg: 'Der Titel darf nicht leer sein.',
          infoComponent: {
            title: 'Erklärung zu: Titel der Dokumentenmappe',
            text: 'Geben Sie Ihrer Dokumentenmappe einen aussagekräftigen Titel. Der Titel sollte den Inhalt der Dokumentenmappe widerspiegeln.',
          },
        },
        DocumentTitleFormItem: {
          label: 'Titel des Dokuments',
          requiredMsg: 'Bitte geben Sie einen Titel an.',
          placeholder: 'Platzhalter für den Titel des Dokuments',
          emtyTitleMsg: 'Der Titel darf nicht leer sein.',
          infoComponent: {
            title: 'Erklärung zu: Titel des Dokuments',
            text: 'Geben Sie Ihrem Dokument einen aussagekräftigen Titel. Der Titel sollte den Inhalt des Dokumentes widerspiegeln.',
          },
        },
        NewVersionTitleFormItem: {
          label: 'Titel',
          requiredMsg: 'Bitte geben Sie einen Titel an.',
          placeholder: 'Platzhalter für den Titel',
          emtyTitleMsg: 'Der Titel darf nicht leer sein.',
          infoComponent: {
            title: 'Erklärung zu: Titel',
            text: '',
          },
        },
        ExportPdfTitleFormItem: {
          label: 'Titel',
          requiredMsg: 'Bitte geben Sie einen Titel für den Export an.',
          placeholder: 'Platzhalter für den Titel',
          emtyTitleMsg: 'Der Titel darf nicht leer sein.',
          infoComponent: {
            title: 'Erklärung zu: Titel',
            text: 'Geben Sie hier den Titel für den Export an',
          },
        },
        DocumentStatusFormItem: {
          label: 'Entwurfstyp',
          requiredMsg: 'Bitte wählen Sie einen Entwurfstyp aus.',
          infoComponent: {
            referentenentwurf: {
              title: 'Erklärung zu: Referentenentwurf',
              text: 'Als Referentenentwurf werden jene Dokumente bezeichnet, die sich in der Erstellungsphase befinden und formal noch nicht durch das Kabinett gebilligt wurden.',
            },
          },
        },
        DocumentRegelungsvorhabenFormItem: {
          label: 'Regelungsvorhaben',
          requiredMsg: 'Bitte wählen Sie ein Regelungsvorhaben aus.',
          placeholder: 'Bitte auswählen',
          infoComponent: {
            title: 'Erklärung zu: Regelungsvorhaben',
            text: 'Für die Erstellung eines rechtsförmlichen Dokuments ist es notwendig, das entsprechende Regelungsvorhaben anzugeben. Ein Regelungsvorhaben beinhaltet ein Datenblatt mit allen Angaben, die für einen Rechtsakt relevant sind. Einige dieser Angaben werden in Ihrem neuen Dokument referenziert und automatisch eingefügt (z.B. die Bezeichnung). Diese Angaben können nur zentral in der Anwendung Regelungsvorhaben geändert werden.',
          },
        },
        DocumentOutlineLevelFormItem: {
          label: 'Anzahl der Gliederungsebenen',
          requiredMsg: 'Bitte wählen Sie ein Gliederungsebene aus.',
          infoComponent: {
            REGELUNGSTEXT_STAMMGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Hier können Sie die Anzahl von Gliederungsebenen auswählen, die Ihr Dokument initial beinhalten soll. Ein Dokument mit einer Gliederungsebene enthält „Abschnitte“ und Paragraphen. Bei zwei Gliederungsebenen ist das Dokument zusätzlich in Kapitel unterteilt. Bei drei Gliederungsebenen enthält Ihr Dokument zusätzlich „Unterabschnitte“. Die Bearbeitung dieser Gliederungsebenen ist im geöffneten Dokument derzeit noch eingeschränkt und wird in nachfolgenden Releases erweitert.',
            },
            VORBLATT_STAMMGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Vorblatt als Stammgesetz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Vorblattes zu beginnen.',
            },
            BEGRUENDUNG_STAMMGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie eine Bergründung als Stammgesetz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen der Begründung zu beginnen.',
            },
            ANSCHREIBEN_STAMMGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Anschreiben als Stammgesetz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Anschreibens zu beginnen.',
            },
            REGELUNGSTEXT_STAMMVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie einen Regelungstext als Stammverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Regelungstextes zu beginnen.',
            },
            VORBLATT_STAMMVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Vorblatt als Stammverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Vorblattes zu beginnen.',
            },
            BEGRUENDUNG_STAMMVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie eine Bergründung als Stammverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen der Begründung zu beginnen.',
            },
            ANSCHREIBEN_STAMMVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Anschreiben als Stammverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Anschreibens zu beginnen.',
            },
            BEGRUENDUNG_MANTELGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie eine Bergründung als Mantelgestz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen der Bergründung zu beginnen.',
            },
            REGELUNGSTEXT_MANTELGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie einen Regelungstext als Mantelgestz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Regelungstextes zu beginnen.',
            },
            VORBLATT_MANTELGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Vorblatt als Mantelgestz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Vorblattes zu beginnen.',
            },
            ANSCHREIBEN_MANTELGESETZ: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Anschreiben als Mantelgestz erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Anschreibens zu beginnen.',
            },
            BEGRUENDUNG_MANTELVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie eine Bergründung als Mantelverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen der Begründung zu beginnen.',
            },
            REGELUNGSTEXT_MANTELVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie einen Regelungstext als Mantelverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Regelungstextes zu beginnen.',
            },
            VORBLATT_MANTELVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Vorblatt als Mantelverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Vorblattes zu beginnen.',
            },
            ANSCHREIBEN_MANTELVERORDNUNG: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie ein Anschreiben als Mantelverordnung erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen des Anschreibens zu beginnen.',
            },
            ANLAGE: {
              title: 'Erklärung zu: Anzahl der Gliederungsebenen',
              text: 'Wenn Sie eine Anlage erstellen, ist eine Auswahl von Gliederungsebenen nicht möglich. Klicken Sie auf "Dokument jetzt anlegen", um mit dem Erstellen der Anlage zu beginnen.',
            },
          },
        },
        OpenedDocumentFormItem: {
          headerLabel: 'Inhalt der {{index}} spalte',
          compoundDocumentLabel: 'Dokumentenmappe',
          documentLabel: 'Name des Dokuments',
        },
        DocumentFormItem: {
          headerLabel: 'Inhalt der {{index}} Spalte',
          compoundDocumentLabel: 'Dokumentenmappe',
          documentLabel: 'Name des Dokuments',
          label: 'Zweites Dokument (zweite Spalte)',
          requiredMsg: 'Bitte wählen Sie ein Dokument aus.',
          placeholder: 'Bitte auswählen',
        },
      },
      editorArea: {
        ariaLabel: '{{index}}. Dokument: {{documentTitle}}',
        collapseItems: {
          title: 'Titel',
          compoundDocument: 'Mappe',
          version: 'Version',
          lastSaved: 'zuletzt gespeichert',
          today: 'heute',
        },
        parseError: 'Das Dokument kann nicht angezeigt werden. Fehler im Inhalt des Dokumentes "{{title}}"',
      },
      authorialNoteArea: {
        mainAriaLabel: 'Fußnotenbereich',
        ariaLabel: '{{index}}. Fußnotenbereich: {{documentTitle}}',
      },
    },
    startseite: {
      help: {
        linkHelp: 'Hilfe',
        drawerTitle: 'Release April 2023',
        section1:
          '<p>Mit dem Release April 2023 gibt es zwei wichtige Veränderungen im Rahmen bestehender Funktionalitäten. Statt mit der Eingabetaste können Sie untergeordnete Gliederungseinheiten lediglich mit der Tastenkombination “Alt+L“ erstellen. Die Eingabetaste wird im Text ausschließlich für die Navigation zwischen den gekennzeichneten editierbaren Bereichen verwendet.</p>' +
          '<p>Im Rahmen der Strukturanzeige für das Mantelgesetz steht Ihnen die Funktion Drag-und-Drop (Ziehen und Ablegen) zur Verfügung. Damit können Sie die Reihenfolge der Artikel nachträglich verändern.</p>',
      },
      breadcrumb: {
        projectName: 'Editor',
        versionHistory: 'Versionshistorie der Dokumentenmappen ({{title}})',
      },
      drawer: {
        contact: 'Kontakt',
      },
      title: 'Editor für Rechtsetzungstexte',
      buttons: {
        createNewCompoundDocumentBtnText: 'Neue Dokumentenmappe anlegen',
      },
      homepageDocumentsTables: {
        userDocumentsTable: {
          title: 'Meine Dokumente',
          error: 'Fehler beim Laden der Daten für Meine Dokumente',
        },
        abstimmungenTable: {
          title: 'Freigegebene Dokumente',
          filter: {
            title: 'Titel',
            labelContent: 'Filtern nach',
          },
          error: 'Fehler beim Laden der Daten für Freigegebene Dokumente',
        },
        entwuerfeTable: { title: 'Entwürfe (künftig verfügbar)' },
        archivTable: { title: 'Archiv (künftig verfügbar)' },
        sorterOptions: {
          regelungsvorhaben: {
            titleAsc: 'Regelungsvorhaben (aufsteigend)',
            titleDesc: 'Regelungsvorhaben (absteigend)',
          },
          compoundDocument: {
            titleAsc: 'Dokumentenmappe (aufsteigend)',
            titleDesc: 'Dokumentenmappe (absteigend)',
          },
          status: {
            titleAsc: 'Zuletzt bearbeitet (neueste zuerst)',
            titleDesc: 'Zuletzt bearbeitet (älteste zuerst)',
          },
        },
        tableColumns: {
          regelungsvorhaben: {
            title: 'Regelungsvorhaben',
          },
          title: {
            title: 'Titel',
            button: {
              SHOW_ALL: 'Alle anzeigen',
              SHOW_LESS: 'Weniger anzeigen',
            },
          },
          compoundDocument: {
            title: 'Dokumentenmappe',
          },
          type: {
            title: 'Typ',
          },
          document: {
            title: 'Dokument',
          },
          version: {
            title: 'Version',
          },
          status: {
            title: 'Status',
            type: {
              freeze: 'in Abstimmung',
              final: 'Abstimmung abgeschlossen',
              draft: 'in Bearbeitung',
              readyForKabinettverfahren: 'Bereit für Kabinettverfahren',
              readyForBundestag: 'Zugeleitet an Bundestag',
              zugestelltPKP: 'zugestellt an PKP',
              bestandsrecht: 'Bestandsrecht',
              aenderungGewuenschtPKP: 'Änderungsbedarf durch BKAmt gemeldet',
              aenderungGewuenschtFederfuehrer: 'Änderungsbedarf durch Federführung gemeldet',
            },
          },
          properties: {
            title: 'Eigenschaften',
            pin: {
              isPinned: 'An die Startseite angeheftet',
              pinSuccess: 'Dokumentenmappe wurde angepinnt',
              releaseSuccess: 'Dokumentenmappe wurde gelöst',
              pinError: 'Dokumentenmappe konnte nicht angepinnt werden',
            },
            newestVersion: 'Aktuellste Version',
          },
          state: {
            title: 'Meine Zugriffsrechte',
            info: {
              title: 'Erklärung zu: Meine Zugriffsrechte',
              abschnitt1: {
                title: 'Mögliche Zugriffsrechte',
                paragraph1: 'Sie können folgende Zugriffsrechte an einem Dokument besitzen:',
                paragraph2: '"Nur Lesen": Sie können das Dokument öffnen und den Inhalt lesen.',
                paragraph3:
                  '"Schreiben": Sie können das Dokument vollumfänglich bearbeiten. Diese Berechtigung ist (vorerst) der Erstellerin oder dem Ersteller des Dokuments vorbehalten.',
              },
              abschnitt2: {
                title: 'Zugriffsrechte ändern',
                paragraph1:
                  'Im Release April 2022 kann nach der Erstellung eines Dokuments nur die Erstellerin oder der Ersteller dieses Dokument lesen und bearbeiten.',
                paragraph2:
                  'Sobald die Erstellerin oder der Ersteller das Dokument in einer Abstimmung hinzufügt, ändern sich die Zugriffsrechte für dieses Dokument. Die Erstellerin oder der Ersteller kann das Dokument nicht mehr bearbeiten und die Teilnehmenden der Abstimmung erhalten Leserechte.',
              },
            },
            type: {
              freeze: 'Lesen und Kommentieren',
              final: 'Nur Lesen',
              draft: 'Schreiben',
              readyForKabinettverfahren: 'Nur Lesen',
              zugestelltPKP: 'Nur Lesen',
              hasRead: 'Nur Lesen',
              hasWrite: 'Schreiben',
            },
          },
          updated: {
            title: 'Zuletzt bearbeitet',
          },
          action: {
            title: 'Aktionen',
            rename: 'Umbenennen',
            setStatusSubmenu: {
              title: 'Status zuweisen',
              readyForKabinettverfahren: 'Bereit für Kabinettverfahren',
              readyForBundestag: 'Bereit für Bundestag',
            },
            newVersion: 'Neue Version anlegen',
            createDrucksache: 'Drucksache erstellen',
            createNewDocument: 'Neues Einzeldokument anlegen',
            importDocument: 'Als XML importieren',
            export: 'Exportieren',
            exportPdf: 'Dokumentenmappe als PDF exportieren',
            exportCompoundDocument: 'Dokumentenmappe als XML exportieren',
            startSynopsisView: 'Parallelansicht mit diesem Dokument',
            setReadRights: 'Leserechte vergeben',
            setWriteRights: 'Schreibrechte vergeben',
            getWriteRights: 'Schreibrechte erhalten',
            archive: 'Archivieren',
            openVersionHistory: 'Versionshistorie öffnen',
            importEgfaData: 'Daten aus Gesetzesfolgenabschätzung übernehmen',
            setBestandsrecht: 'Als Bestandsrecht festlegen',
            assignBestandsrecht: 'Bestandsrecht mit Dokumentenmappe verknüpfen',
          },
        },
      },
      versionHistory: {
        title: 'Versionshistorie der Dokumentenmappen ({{title}})',
        backToHomepage: 'Zurück zur Startseite',
      },
      readme: {
        images: {
          imgText1: 'Dokumentenmappe und Einzeldokumente für Gesetze in Stamm- oder Mantelform anlegen',
          imgText2: 'Gesetze in Stamm- oder Mantelform Schritt für Schritt erarbeiten',
          imgText3: 'In Abstimmungen mit einer eigenen Dokumentenmappe antworten',
        },
        textWrapper: {
          text1:
            'Ab sofort ist es Ihnen als Federführerin oder Federführer möglich, alle Einzeldokumente für Gesetze und Verordnungen in Stamm- oder Mantelform anzulegen. Als Erstellerin oder Ersteller einer Dokumentenmappe können Sie Leserechte an andere Personen vergeben. Außerdem haben Sie die Möglichkeit, die Ergebnisse aller Module der Gesetzesfolgenabschätzung in die entsprechenden Einzeldokumente zu übertragen und die übertragenen Inhalte anschließend zu bearbeiten. In der Parallelansicht können mehrere Spalten gleichzeitig bearbeitet sowie einzelne Spalten ausgeblendet werden. Zudem können Sie im Fließtext nach Begriffen suchen und diese ersetzen.',
        },
      },
      footer: {
        text: 'Bundesministerium des Innern, für Bau und Heimat',
        link1: 'Impressum',
        link2: 'Erklärung der Barrierefreiheit',
        link3: 'Datenschutz',
        link4: 'Kontakt',
      },
    },
    messages: {
      comments: {
        save: {
          success: 'Kommentar gespeichert',
          defaultError: 'Fehler beim Speichern des Kommentars',
          rightsError: 'Fehlende Berechtigung zum Speichern des Kommentars',
        },
        loadError: 'Fehler beim Laden der Kommentare',
        state: {
          defaultError: 'Fehler beim markieren des Kommentars',
          rightsError: 'Fehlende Berechtigung zum markieren des Kommentars',
          success: 'Kommentar erfolgreich als {{state}} markiert',
        },
        delete: {
          deleteThread: {
            defaultError: 'Fehler beim löschen des Threads',
            rightsError: 'Fehlende Berechtigung zum löschen des Threads',
            success: 'Thread erfolgreich gelöscht',
          },
          deleteComment: {
            defaultError: 'Fehler beim löschen des Kommentars',
            rightsError: 'Fehlende Berechtigung zum löschen des Kommentars',
            success: 'Kommentar erfolgreich gelöscht',
          },
          deleteConfirmation: {
            deleteComment: 'Möchten Sie den Kommentar löschen',
            deleteAnswer: 'Möchten Sie die Antwort löschen',
            warningCommentText: 'Achtung, wenn Sie den Kommentar löschen, ist er nicht mehr verfügbar.',
            warningAnswerText: 'Achtung, wenn Sie die Antwort löschen, ist sie nicht mehr verfügbar.',
          },
          deleteReply: {
            defaultError: 'Fehler beim löschen der Antwort',
            rightsError: 'Fehlende Berechtigung zum löschen der Antwort',
            success: 'Antwort erfolgreich gelöscht',
          },
        },
        reply: {
          saveSuccess: 'Antwort gespeichert',
          saveError: 'Fehler beim Speichern der Antwort',
        },
      },
      createCompoundDocument: {
        success:
          'Dokumentenmappe erfolgreich erstellt. Sie können unter "Neues Einzeldokument anlegen" ein Dokument in der Dokumentenmappe anlegen.',
        error: 'Die Dokumentenmappe {{compoundDocument}} konnte nicht angelegt werden',
      },
      importDocument: {
        error: {
          noCompoundDocument: 'Es konnte keine DokumentenMappe gefunden werden',
          maxSizeOrWrongFormat: 'Falsche Dateigröße oder falsches Dateiformat',
          unprocessable: 'Invalider XML Inhalt',
          maxNumber:
            'Es kann für die Dokumentenmappe {{compoundDocument}} kein weiteres Dokument von der Art {{documentType}} angelegt werden',
          default: 'Der Import des Dokuments ist fehlgeschlagen.',
        },
      },
      createDocument: {
        success: 'Dokument erfolgreich erstellt',
        error:
          'Es kann für die Dokumentenmappe {{compoundDocument}} kein weiteres Dokument von der Art {{documentType}} angelegt werden',
      },
      stammgesetzRegelungstext: {
        loadAccessError: {
          title: 'Fehler',
          text: 'Das angeforderte Dokument existiert nicht oder Sie haben keine Berechtigung das Dokument anzuzeigen.',
        },
        typeError: {
          title: 'Fehler',
          text: 'Der angeforderte Dokumententyp kann nicht geöffnet werden.',
        },
        saveSuccess: {
          title: 'Speichern erfolgreich',
          text: 'Das Speichern des Dokuments war erfolgreich.',
        },
        saveError: {
          title: 'Speichern fehlgeschlagen',
          text: 'Das Speichern des Dokuments ist fehlgeschlagen.',
        },
      },
      save: {
        success: 'Dokument erfolgreich gespeichert',
        badRequestError: 'Der Inhalt des Dokuments ist fehlerhaft und konnte nicht gespeichert werden',
        unauthorizedError: 'Ihre Anmeldung ist abgelaufen. Das Dokument konnte nicht gespeichert werden',
        invalidRightsError: 'Sie besitzen nicht die benötigten Rechte, um dieses Dokument zu speichern.',
        notFoundError: 'Das Dokument, welches Sie bearbeiten, konnte nicht gefunden werden.',
        error: 'Fehler beim Speichern des Dokuments',
        allSuccess: 'Alle Dokumente wurden erfolgreich gespeichert',
        someError: 'Fehler beim Speichern',
      },
      export: {
        success: 'Dokument erfolgreich exportiert',
        error: 'Fehler beim Exportieren des Dokuments',
      },
      exportCompoundDocument: {
        success: 'Dokumentenmappe erfolgreich exportiert',
        error: 'Fehler beim Exportieren der Dokumentenmappe',
      },
      renameDocument: {
        success: 'Dokument erfolgreich umbenannt',
        error: 'Fehler beim umbenennen des Dokuments',
      },
      renameCompundDocument: {
        success: 'Dokumentenmappe erfolgreich umbenannt',
        error: 'Fehler beim umbenennen der Dokumentenmappe',
      },
      createNewCompoundDocumentVersion: {
        success: 'Neue Version erfolgreich erstellt',
        error: 'Fehler beim Erstellen der neuen Version',
      },
      importEgfaData: {
        success: 'Ausgewählte Gesetzesfolgenabschätzung Daten erfolgreich übernommen',
        error: 'Fehler bei der Datenübernahme der Gesetzesfolgenabschätzung',
      },
      setReadyForBundestag: {
        success: '{{compoundDocument}} ist erfolgreich auf "Bereit für Bundestag" gesetzt',
        error: 'Fehler beim setzen von {{compoundDocument}} auf "Bereit für Bundestag"',
      },
      setReadyForKabinettverfahren: {
        success: '{{compoundDocument}} ist erfolgreich auf "Bereit für Kabinettverfahren" gesetzt',
        error: 'Fehler beim setzen von {{compoundDocument}} auf "Bereit für Kabinettverfahren"',
      },
      setDocumentRights: {
        success: 'Leserechte für die Dokumentenmappe "{{title}}" wurden erfolgreich vergeben',
        error: 'Fehler bei der Vergabe der Leserechte für Dokumentenmappe "{{title}}"',
      },
      SetDocumentWriteRights: {
        success: 'Schreibrechte für die Dokumentenmappe "{{title}}" wurden erfolgreich vergeben',
        error: 'Fehler bei der Vergabe der Schreibrechte für Dokumentenmappe "{{title}}"',
      },
      createAenderungsvergleich: { error: 'Fehler beim Erstellen des Änderungsvergleiches.' },
      createAenderungsbefehl: { error: 'Fehler beim Erstellen der Änderungsbefehle.' },
      loadVersionshistorie: {
        error: 'Fehler beim Laden der Versionshistorie.',
      },
      createDrucksache: {
        success: 'Die Drucksache für die Dokumentenmappe: "{{title}}", wurde erfolgreich erstellt',
        error: 'Es ist ein Fehler bei der Erstellung der Drucksache zur Dokumentenmappe: "{{title}}" aufgetreten',
      },
      assignBestandsrecht: {
        loadCompoundDocuments: {
          error: 'Fehler beim Laden der Dokumentenmappen',
        },
        loadBestandsrecht: {
          error: 'Fehler beim Laden des Bestandsrechts',
        },
        updateBestandsrecht: {
          error: 'Fehler beim Verknüpfen der Bestandsrechte.',
          success:
            'Die importierten Bestandsrechte wurden erfolgreich mit den ausgewählten Dokumentenmappen verknüpft.',
        },
      },
    },
    images: {
      messages: {
        screenreader: {
          altInfoTooltip: 'Info: Bitte beschreiben Sie das Bild in wenigen Worten.',
        },
        validation: {
          onlyNumbersAllowed: 'Nur numerische Werte zulässig',
        },
      },
      tooltips: {
        altInfo: 'Bitte beschreiben Sie das Bild in wenigen Worten.',
        allowedImageWidth: `Bitte nur numerische Werte zwischen ${IMAGE_RESIZE_MIN_VALUE} und ${IMAGE_RESIZE_MAX_WIDTH_IN_CM} eingeben`,
        allowedImageHeight: `Bitte nur numerische Werte zwischen ${IMAGE_RESIZE_MIN_VALUE} und ${IMAGE_RESIZE_MAX_HEIGHT_IN_CM} eingeben`,
      },
      optionsDialog: {
        buttons: {
          close: 'Bildoptionen schließen',
          leftAlign: 'linksbündig',
          centerAlign: 'zentriert',
          rightAlign: 'rechtsbündig',
        },
        labels: {
          heading: 'Bildoptionen',
          width: 'Breite in cm',
          height: 'Höhe in cm',
          alternativeText: 'Alternativtext',
          labelling: 'Beschriftung',
          imageSubtext: 'Bildunterschrift',
          lockAspectRatio: 'Seitenverhältnis sperren',
          position: 'Position',
        },
      },
    },
    toolbar: {
      ariaLabel: 'Wekzeugleiste',
      labels: {
        tables: {
          insertTable: 'Tabelle einfügen',
          pleaseSelect: 'Bitte auswählen',
          ariaLabel: 'Tabellenwerkzeuge',
        },
        authorialNote: {
          numbered: 'Nummerierte Fußnote einsetzen',
          custom: 'Benutzerdefinierte Fußnote einsetzen',
          ariaLabel: 'Fußnotenwerkzeuge',
        },
        synopsis: {
          parallelView: 'Parallelansicht',
          generateAenderungsbefehl: 'Änderungsbefehle aktualisieren',
          parallelViewSubmenu: {
            configSynopsis: 'Parallelansicht konfigurieren',
            closeSynopsis: 'Parallelansicht schließen',
            newSynopsis: 'Weitere Parallelansicht konfigurieren',
          },
          aenderungsvergleich: 'Änderungsvergleich',
          aenderungsvergleichSubmenu: {
            aenderungsvergleichFromExisting: {
              title: 'Änderungsvergleich der bestehenden Spalten',
              hdrMode: 'HdR-konforme Anzeige',
              showChanges: 'Anzeige der Änderungen',
            },
            aenderungsvergleichConfigManually: 'Änderungsvergleich manuell konfigurieren',
            closeAenderungsvergleich: 'Änderungsvergleich schließen',
          },
        },
        file: {
          createNewVersion: 'Neue Version der Dokumentenmappe anlegen',
          egfa: {
            title: 'Gesetzesfolgenabschätzung',
            import: 'Übernahme der Gesetzesfolgenabschätzung-Ergebnisse',
            update: 'Aktualisierung der Gesetzesfolgenabschätzung-Ergebnisse',
          },
        },
        dynAenderungsvergleich: {
          title: 'Dynamische Änderungsnachverfolgung',
        },
        satzZaehlung: {
          title: 'Satzzählung',
        },
      },
    },
    topToolbar: {
      print: 'Drucken',
      export: 'Exportieren',
      save: 'Speichern',
      close: 'Schließen',
      saveConfirmation: {
        closeDocument: {
          warningText: 'Möchten Sie Ihre Änderungen speichern und das Dokument schließen?',
          close: 'Ja, speichern und schließen',
          abort: 'Abbrechen',
        },
        closeSynopsis: {
          warningText: 'Möchten Sie diese Ansicht schließen?',
          close: 'Ja, speichern und schließen',
          abort: 'Abbrechen',
        },
        configureSynopsis: {
          warningText: 'Möchten Sie diese Parallelansicht neu konfigurieren?',
          close: 'Ja, speichern und konfigurieren',
          abort: 'Abbrechen',
        },
      },
      closeConfirmation: {
        closeDocument: {
          warningTitle: 'Möchten Sie das Dokument schließen?',
          warningText: 'Nicht gespeicherte Änderungen gehen verloren.',
          close: 'Schließen',
        },
        leavePage: {
          warningTitle: 'Möchten Sie die Seite verlassen?',
          warningText: 'Nicht gespeicherte Änderungen gehen verloren.',
          close: 'Verlassen',
        },
        abort: 'Abbrechen',
      },
      readOnlyInfo: {
        label: {
          DRAFT: 'Mit Zugriffsrecht "Nur Lesen" geöffnet.',
          FREEZE: 'Mit Zugriffsrecht "Lesen und Kommentieren" geöffnet.',
          FINAL: 'Mit Zugriffsrecht "Nur Lesen" geöffnet.',
          ZUGELEITET_PKP: 'Mit Zugriffsrecht "Zugestellt an PKP" geöffnet.',
          BESTANDSRECHT: 'Mit Zugriffsrecht "Nur Lesen" geöffnet.',
          BEREIT_FUER_BUNDESTAG: 'Mit Zugriffsrecht "Nur Lesen" geöffnet.',
        },
        dialog: {
          title: 'Hinweis: Ihre Zugriffsrechte',
          text: 'Das Dokument wird/wurde in der Abstimmung abgestimmt. Deshalb kann das Dokument nur gelesen, aber nicht mehr bearbeitet werden.',
          button: 'Ok',
        },
        readyForKabinettverfahren: {
          label: 'Mit Status "Bereit für Kabinettverfahren" geöffnet',
          dialog: {
            title: 'Hinweis: Bereit für Kabinettverfahren',
            text: 'Das Dokument wurde auf den Status "Bereit für Kabinettverfahren" gesetzt. Deshalb kann das Dokument nur gelesen, aber nicht mehr bearbeitet werden.',
            button: 'Ok',
          },
        },
        readyForBundestag: {
          label: 'Zugeleitet an Bundestag',
          dialog: {
            title: 'Im Status "Zugeleitet an Bundestag" geöffnet',
            text: 'Das Dokument wurde auf den Status "Zugeleitet an Bundestag" gesetzt. Deshalb kann das Dokument nur gelesen, aber nicht mehr bearbeitet werden.',
            button: 'Ok',
          },
        },
        bestandsrecht: {
          label: 'Im Status "Bestandsrecht" geöffnet',
          dialog: {
            title: 'Hinweis: Bestandsrecht',
            text: 'Das Dokument ist als "Bestandsrecht" gesetzt. Deshalb kann das Dokument nur gelesen, aber nicht mehr bearbeitet werden.',
            button: 'Ok',
          },
        },
        aenderungsvergleich: {
          label: 'Änderungsvergleich',
          dialog: {
            title: 'Hinweis: Änderungsvergleich',
            text: 'Die Dokumente werden in einem Änderungsvergleich angezeigt. In diesem Status sind die Dokumente schreibgeschützt.',
            button: 'Ok',
          },
        },
      },
    },
    sideToolbar: {
      ariaLabel: 'Optionsleiste',
    },
    contextMenu: {
      tableOfContents: {
        addElement: 'Hinzufügen von',
        expandLevels: {
          title: 'Gliederung erweitern',
          subdivideElement: 'Unterteilen in',
          wrappElement: 'Einordnen in',
        },

        deleteElement: {
          title: 'Löschen',
          article: 'Einzelvorschrift',
          onlyElement: 'Nur Gliederungsebene',
          elementWithContent: 'Gliederungsebene mit Inhalte',
          allElements: 'Gliederungsebene im gesamten Dokument',
          allElementsInDocument: 'Gliederungsebene im gesamten Dokument',
        },
        drag: {
          title: '{{type}} verschieben',
          up: '{{type}} nach oben',
          down: '{{type}} nach unten',
          toTop: 'An den Anfang',
          toBottom: 'An den Schluss',
        },
        options: {
          types: {
            chapter: 'Kapitel',
            book: 'Buch',
            part: 'Teil',
            subchapter: 'Unterkapitel',
            section: 'Abschnitt',
            subsection: 'Unterabschnitt',
            title: 'Titel',
            subtitle: 'Untertitel',
            hcontainer: 'Begründungsabschnitt',
            asNext: {
              chapter: 'Nächstes Kapitel',
              book: 'Nächstes Buch',
              part: 'Nächster Teil',
              subchapter: 'Nächstes Unterkapitel',
              section: 'Nächster Abschnitt',
              subsection: 'Nächster Unterabschnitt',
              title: 'Nächster Titel',
              subtitle: 'Nächster Untertitel',
            },
          },
          stammform: {
            article: 'Einzelvorschrift',
            asNext: { article: 'Nächste Einzelvorschrift' },
          },
          mantelform: {
            article: 'Artikel',
          },
        },
      },
      tables: {
        add: 'Einfügen',
        delete: 'Löschen',
        deleteTable: 'Tabelle löschen',
        deleteRow: 'Zeile löschen',
        deleteColumn: 'Spalte löschen',
        addColumnRight: 'Spalte rechts einfügen',
        addColumnLeft: 'Spalte links einfügen',
        addRowAbove: 'Zeile darüber einfügen',
        addRowBelow: 'Zeile darunter einfügen',
        divideCell: 'Zelle teilen',
      },
      comments: {
        addComment: 'Kommentar einfügen',
      },
      authorialNote: {
        createNumbered: 'Nummerierte Fußnote einsetzen',
        createCustom: 'Benutzerdefinierte Fußnote einsetzen',
      },
    },
    comments: {
      newComment: {
        addCommentHeader: 'Kommentar verfassen',
        addReplyHeader: 'Antwort verfassen',
        addCommentButton: 'Hinzufügen',
        cancleAddCommentButton: 'Abbrechen',
      },
      editComment: {
        editCommentHeader: 'Kommentar bearbeiten',
        editAnswereHeader: 'Antwort bearbeiten',
      },
      actions: {
        setOpened: 'Als offen markieren',
        setClosed: 'Als erledigt markieren',
        setThreadOpened: 'Thread als offen markieren',
        setThreadClosed: 'Thread als erledigt markieren',
        deleteThread: 'Thread löschen',
        deleteComment: 'Löschen',
        deleteAnswer: 'Antwort löschen',
        cancelDelete: 'Abbrechen',
        edit: 'Bearbeiten',
        open: 'Kommentar öffnen',
      },
      replies: {
        header: 'Antwort(en):',
        replyCount: '{{count}} Antworten',
        replyButton: 'Antwort verfassen',
      },
      buttons: {
        showMore: 'Mehr anzeigen',
        showLess: 'Weniger anzeigen',
      },
      marker: {
        startMarker: 'Beginn Kommentarmarkierung',
        endMarker: 'Ende Kommentarmarkierung',
      },
    },
    tableOfContents: { header: 'Strukturanzeige: "{{title}}"', ariaLabel: 'Strukturanzeige von {{documentTitle}}' },
    commentOptions: {
      ariaLabel: 'Kommentaroptionen',
      jumpBetweenComments: 'Zwischen Kommmentaren springen',
      prevComment: 'Zurück',
      nextComment: 'Weiter',
      filter: {
        checkAll: 'Alle auswählen',
        selectedItemsLabel: '{{selectedItems}} von {{allItems}} ausgewählt',
        title: 'Kommentare filtern nach',
        category: 'Kategorien',
        origin: 'Personen',
      },
    },
    synopsisOptions: {
      ariaLabel: 'Parallelansichtsoptionen',
      synchronousScrolling: {
        title: 'Synchrones Scrollen',
        switch: {
          title: 'Synchrones Scrollen',
        },
      },
      visibleColumns: {
        title: 'Spalten ein- und ausblenden',
        select: {
          title: 'Eingeblendete Spalten',
        },
      },
      columnResizer: {
        title: 'Spaltenbreite anpassen',
        sizes: {
          SMALL: 'Schmal',
          MEDIUM: 'Mittel',
          LARGE: 'Breit',
        },
      },
    },
    searchReplaceOptions: {
      ariaLabel: 'Suchen / Ersetzen',
      searchFor: {
        title: 'Durchsuchen von',
        infoComponent: {
          title: 'Erklärung zu: Durchsuchen von',
          text: 'Sie können den Fließtext und (perspektivisch) Fußnoten durchsuchen und ersetzen. Kommentare und nicht bearbeitbare Bereiche im Fließtext können Sie (perspektivisch) durchsuchen, aber nicht ersetzen.',
        },
        options: {
          fliesstext: 'Fließtext (bearbeitbar)',
          fliesstextReadonly: 'Fließtext (nicht bearbeitbare Bereiche)',
          comments: 'Kommentarspalte',
          authorialNotes: 'Fußnotenbereich',
        },
      },
      search: {
        title: 'Suchen nach',
        results: {
          multiple: '{{count}} Ergebnisse',
          single: '1 Ergebnis',
          no: 'keine Treffer',
          from: 'Ergebnis {{searchPosition}} von {{count}}',
        },
        buttons: {
          prev: 'Zurück',
          next: 'Weiter',
        },
      },
      replace: {
        title: 'Ersetzen durch',
        buttons: {
          replace: 'Ersetzen',
          replaceAll: 'Alle Ersetzen',
        },
      },
    },
    daenvOptions: {
      title: 'Änderungsnachverfolgung',
      ariaLabel: 'Dynamische Änderungsnachverfolgung Optionen',
      switch: {
        title: 'Änderungsnachverfolgung',
        description: 'Änderungsnachverfolgung aktivieren',
      },
      accept: {
        title: 'Änderungen überprüfen',
        accept: 'Alle Änderungen annehmen',
        decline: 'Alle Änderungen ablehnen',
      },
    },
    hdrAssistent: {
      title: 'HdR-Assistent',
      ariaLabel: 'HdR-Assistent',
      buttonLabel: 'HdR-Konformitätsprüfung durchführen',
      buttonStart: 'HdR-Prüfung starten',
      buttonReset: 'HdR-Prüfung zurücksetzen',
      hdrFindingsText: 'Anzahl gefundener HdR-Verletzungen: ',
      jumpBetweenLabel: 'Zwischen Fundstellen springen:',
      backButton: 'Zurück',
      nextButton: 'Weiter',
      noFindings: 'Keine HdR-Verletzung im Dokument: "{{title}}" gefunden',
      finding: {
        noChildren: 'Kein Unterelement in {{elementType}}',
        onlyOneofType: 'Nur ein(e) {{elementType}} in {{parentType}}',
        noHeaderText: 'Keine Überschrift in {{elementType}}',
        moreThanFiveAbsaetze: 'Einzelvorschrift hat mehr als fünf juristische Absätze',
      },
    },
  },
  leaTheme: {
    multiChoice: {
      checkAll: 'Alle auswählen',
    },
    icon: {
      regulatoryProposalFolder: 'Regelungsvorhaben',
      compoundDocumentIcon: 'Dokumentenmappe',
    },
  },
};
