// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

function getBaseUrl() {
  return ['localhost', '127.0.0.1'].some((v) => window.location.hostname.includes(v))
    ? 'https://Platzhalter-URL.de'
    : '';
}

export const BaseUrl = getBaseUrl();
export const BasePath = '/LEA/API/v1';
export const BaseUriKeycloak = `${BaseUrl}${BasePath}/keycloak-Platzhalter-URL`;
