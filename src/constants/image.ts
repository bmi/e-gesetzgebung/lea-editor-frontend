// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const CM_PER_INCH = 2.54;
// IMAGE CONSTANTS
export const ALLOWED_IMAGE_FILE_TYPES = ['gif', 'png', 'jpg', 'jpeg', 'bmp', 'svg'];
export const DEFAULT_DPI = 96;
export const MAX_INITIAL_IMAGE_WIDTH_IN_CM = 7;
export const IMAGE_RESIZE_STEP = 0.1;
export const IMAGE_RESIZE_DECIMAL_SEPARATOR = ',';
export const IMAGE_RESIZE_MIN_VALUE = 1;
export const IMAGE_RESIZE_MAX_WIDTH_IN_CM = 16;
export const IMAGE_RESIZE_MAX_HEIGHT_IN_CM = 24.5;
export const IMAGE_RESIZE_MAX_WIDTH_IN_PX = (IMAGE_RESIZE_MAX_WIDTH_IN_CM / CM_PER_INCH) * DEFAULT_DPI;
export const IMAGE_RESIZE_MAX_HEIGHT_IN_PX = (IMAGE_RESIZE_MAX_HEIGHT_IN_CM / CM_PER_INCH) * DEFAULT_DPI;
export const IMAGE_RESIZE_PRECISION = 2;
export const DEFAULT_DOWNLOADED_IMAGE_NAME = 'lea-image';
export const ATTRIBUTE = {
  SRC: 'src',
  ALT: 'alt',
  WIDTH: 'width',
  HEIGHT: 'height',
};

export const ALIGNMENT = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};
