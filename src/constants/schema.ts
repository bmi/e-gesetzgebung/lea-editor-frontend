// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const SEARCH_PREV = 'search-prev';
export const SEARCH_NEXT = 'search-next';
export const REPLACE_ONE = 'replace-one';
export const REPLACE_ALL = 'replace-all';
