// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export enum SynopsisType {
  None,
  Synopsis,
  NewSynopsis,
  Aenderungsvergleich,
  Aenderungsbefehl,
}

export enum SynopsisMode {
  Hdr,
  Changes,
}
