// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const ColorCodes = [
  '#002766',
  '#690348',
  '#104D00',
  '#722C00',
  '#2E0588',
  '#004652',
  '#0050B3',
  '#9E1068',
  '#531DAB',
  '#005A60',
  '#703FAC',
  '#166300',
  '#8A4500',
];
