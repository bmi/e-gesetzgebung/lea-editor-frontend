// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../src/lea-editor/plugin-core';
import { HeaderPlugin as StammformHeaderPlugin } from '../../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import { HeaderPlugin as MantelformHeaderPlugin } from '../../../../../../../src/lea-editor/plugins/regelungstext/mantelform';
import { DocumentType } from '../../../../../../../src/api';

export const setupHeaderPluginRegelungstext = (element: Element) => {
  const registry = new PluginRegistry([
    new StammformHeaderPlugin({
      editorDocument: { ...InitialEditorDocument, documentType: DocumentType.REGELUNGSTEXT_STAMMGESETZ },
      isDynAenderungsvergleich: false,
    }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};

export const setupHeaderPluginRegelungstextMantelform = (element: Element) => {
  const registry = new PluginRegistry([
    new MantelformHeaderPlugin({
      editorDocument: { ...InitialEditorDocument, documentType: DocumentType.REGELUNGSTEXT_MANTELGESETZ },
      isDynAenderungsvergleich: false,
    }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};

export const setupHeaderPluginBegruendung = (element: Element) => {
  const registry = new PluginRegistry([
    new StammformHeaderPlugin({
      editorDocument: { ...InitialEditorDocument, documentType: DocumentType.BEGRUENDUNG_STAMMGESETZ },
      isDynAenderungsvergleich: false,
    }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};
