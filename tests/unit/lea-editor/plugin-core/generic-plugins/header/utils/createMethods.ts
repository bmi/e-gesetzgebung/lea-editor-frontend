// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/regelungstext';

// Methods for Regelungstext Stammform

const createParagraph = (text: string): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};

const createArticle = (text: string): Element => {
  const paragraphs = [createParagraph('Test')];
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper(text)]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [num, heading, ...paragraphs]);
};

export const createRegelungstextBody = (text: string): Element => {
  const article = createArticle(text);
  const body = createElementOfType(REGELUNGSTEXT_STRUCTURE.BODY, [article]);
  return body;
};

// Methods for Regelungstext Mantelform

const createParagraphMantelform = (text: string): Element => {
  const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, [createTextWrapper(text)]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};

const createArticleMantelform = (text: string): Element => {
  const paragraphs = [createParagraphMantelform('Test')];
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper(text)]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [num, heading, ...paragraphs]);
};

export const createRegelungstextMantelformBody = (text: string): Element => {
  const article = createArticleMantelform(text);
  const body = createElementOfType(REGELUNGSTEXT_STRUCTURE.BODY, [article]);
  return body;
};

// Methods for Begründung Stammform

export const createBegruendungHeader = (text: string): Element => {
  const textElement = createTextWrapper('Zu');
  const refElement = createElementOfType(BEGRUENDUNG_STRUCTURE.REF, [createTextWrapper(text)]);
  const heading = createElementOfType(BEGRUENDUNG_STRUCTURE.HEADING, [textElement, refElement]);
  const headingContent = createElementOfType('headingContent', [heading]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [headingContent]);
};
