// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import {
  setupHeaderPluginRegelungstext,
  setupHeaderPluginRegelungstextMantelform,
  setupHeaderPluginBegruendung,
} from './utils/setupMehods';
import {
  createRegelungstextBody,
  createRegelungstextMantelformBody,
  createBegruendungHeader,
} from './utils/createMethods';
import { BaseText, Editor, NodeEntry } from 'slate';
import { InitialEditorDocument } from '../../../../../../src/lea-editor/plugin-core';
import { HeaderPlugin as StammformHeaderPlugin } from '../../../../../../src/lea-editor/plugins/regelungstext/stammform';

const stammformPlugin = new StammformHeaderPlugin({ editorDocument: { ...InitialEditorDocument } });

describe('Regelungstext Stammform', () => {
  it('press backwards at end of header-element in regelungstext Stammform --> last character is removed', () => {
    const editor = setupHeaderPluginRegelungstext(createRegelungstextBody('Lorem ipsum'));
    editor.selection = {
      focus: { path: [0, 0, 1, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 1, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const header = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('Lorem ipsu');
  });

  it('press delete at beginning of header-element in regelungstext Stammform --> first character is removed', () => {
    const editor = setupHeaderPluginRegelungstext(createRegelungstextBody('Lorem ipsum'));
    editor.selection = {
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const header = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('orem ipsum');
  });

  it('keydown method in editable area', () => {
    const editor = setupHeaderPluginRegelungstext(createRegelungstextBody('Lorem ipsum'));
    editor.selection = {
      focus: { path: [0, 0, 1, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 1, 0, 0], offset: 11 },
    };
    stammformPlugin.keyDown(editor);
    const header = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('Lorem ipsum');
  });
});

describe('Regelungstext Mantelform', () => {
  it('press backwards at end of header-element --> last character is removed', () => {
    const editor = setupHeaderPluginRegelungstextMantelform(createRegelungstextMantelformBody('Lorem ipsum'));
    editor.selection = {
      focus: { path: [0, 0, 1, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 1, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const header = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('Lorem ipsu');
  });

  it('press delete at beginning of header-element --> first character is removed', () => {
    const editor = setupHeaderPluginRegelungstextMantelform(createRegelungstextMantelformBody('Lorem ipsum'));
    editor.selection = {
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const header = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('orem ipsum');
  });
});

describe('Begründung Stammform', () => {
  it('Press backward at end of header-Element --> last charachter is removed', () => {
    const editor = setupHeaderPluginBegruendung(createBegruendungHeader('§ [Platzhalter]'));
    editor.selection = {
      focus: { path: [0, 0, 0, 1, 0, 0], offset: 15 },
      anchor: { path: [0, 0, 0, 1, 0, 0], offset: 15 },
    };
    editor.deleteBackward('character');
    const header = Editor.node(editor, [0, 0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(header[0].text).be.equal('§ [Platzhalter');
  });
});
