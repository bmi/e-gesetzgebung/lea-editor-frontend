// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../src/lea-editor/plugin-core';
import {
  createElementOfType,
  createTextWrapper,
  isLastPosition,
} from '../../../../../../src/lea-editor/plugin-core/utils';
import { TabelleCommands, TabellePlugin } from '../../../../../../src/lea-editor/plugin-core/generic-plugins/tabelle';
import { TABELLE_STRUCTURE } from '../../../../../../src/lea-editor/plugin-core/generic-plugins/tabelle';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { Element } from '../../../../../../src/interfaces';
import { ReactEditor } from 'slate-react';
import { BaseElement, BaseText, Editor, NodeEntry, Transforms } from 'slate';
import { DocumentType } from '../../../../../../src/api';

describe('TabelleCommands', () => {
  const setupContainer = (withParagraph = true): [ReactEditor, PluginRegistry] => {
    const registry = new PluginRegistry([
      new TabellePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    if (withParagraph) {
      mainEditor.editor.insertNode(createContainer());
    } else {
      mainEditor.editor.insertNode(createContainerWithoutParagraph());
    }
    return [mainEditor.editor, registry];
  };

  const setupTable = (rows: number, columns: number, withParagraph = true): [ReactEditor, PluginRegistry] => {
    const [editor, _registry] = setupContainer();
    Transforms.select(editor, { path: [0, 0, 0, 0, 0], offset: 0 });
    TabelleCommands.insertTable(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ, rows, columns);
    if (withParagraph) {
      Transforms.select(editor, { path: [0, 0, 1, 0, 0, 0, 0, 0], offset: 0 });
      Transforms.insertText(editor, 'Lorem ipsum');
    }
    return [editor, _registry];
  };

  const setupContainerWithEmptyParagraph = (): [ReactEditor, PluginRegistry] => {
    const registry = new PluginRegistry([
      new TabellePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    mainEditor.editor.insertNode(createContainerWithEmptyParagraph());
    return [mainEditor.editor, registry];
  };

  it('should create new table with 2 rows an 2 columns -> table was created', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);

    // Get first table row node
    const tableRowNode = Editor.node(editor, [0, 0, 1, 0]) as NodeEntry<BaseElement>;

    // Check if node ist table row and has 2 childern
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children.length).equals(2);
  });

  it('should delete table with 2 rows an 2 columns -> table was deleted', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table
    if ((tableNode[0] as Element).type === TABELLE_STRUCTURE.TABLE) {
      TabelleCommands.deleteTable(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);

      // Check if table was deleted
      const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;
      expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.P);
    }
  });

  it('should add row to existing table -> table getting additional row', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    TabelleCommands.insertRow(editor, 2);

    const newTableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;
    expect((newTableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((newTableNode[0] as Element).children.length).equals(3);
  });

  it('should add column to existing table -> table getting additional column', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children.length).equals(2);
    TabelleCommands.insertColumn(editor, 2);

    const newTableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;
    expect((newTableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((newTableRowNode[0] as Element).children[0].type).equals(TABELLE_STRUCTURE.TABLE_CELL);
    expect((newTableRowNode[0] as Element).children.length).equals(3);
  });

  it('should delete row from existing table -> table deleted row', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    TabelleCommands.deleteRow(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);

    const newTableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;
    expect((newTableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((newTableNode[0] as Element).children.length).equals(1);
  });

  it('should delete column from existing table -> table deleted column', () => {
    const [editor, _registry] = setupContainer();
    Transforms.select(editor, { path: [0, 0, 0, 0, 0], offset: 0 });
    TabelleCommands.insertTable(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ, 2, 2);

    // Get Table Node by Path
    const tableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children.length).equals(2);
    TabelleCommands.deleteColumn(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);

    const newTableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;
    expect((newTableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((newTableRowNode[0] as Element).children[0].type).equals(TABELLE_STRUCTURE.TABLE_CELL);
    expect((newTableRowNode[0] as Element).children.length).equals(1);
  });

  it("shouldn't insert table if no paragraph is provided in editor -> no table created", () => {
    const [editor, _registry] = setupContainer(false);
    TabelleCommands.insertTable(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ, 2, 2);

    // Get Table Node by Path
    const tableRowNode = Editor.node(editor, [0, 0, 0]) as NodeEntry<BaseElement>;

    // Check if Node isn't Table
    expect((tableRowNode[0] as Element).type).not.equals(TABELLE_STRUCTURE.TABLE);
  });

  it('should create table in empty paragraph -> table create in empty paragraph', () => {
    const [editor, _registry] = setupContainerWithEmptyParagraph();
    TabelleCommands.insertTable(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ, 2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 0]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
  });

  it('should delete table if it has only 1 column -> table deleted', () => {
    const [editor, _registry] = setupTable(1, 1);

    // Get Table Node by Path
    const tableRowNode = Editor.node(editor, [0, 0, 1, 0]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children.length).equals(1);
    TabelleCommands.deleteColumn(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);

    const newTableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;
    expect((newTableNode[0] as Element).type).not.equals(TABELLE_STRUCTURE.TABLE);
    expect((newTableNode[0] as Element).type).equals(TABELLE_STRUCTURE.P);
  });

  it('should delete table if it has only 1 row -> table deleted', () => {
    const [editor, _registry] = setupTable(1, 1);

    // Get Table Node by Path
    const tableRowNode = Editor.node(editor, [0, 0, 1, 0]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children.length).equals(1);
    TabelleCommands.deleteRow(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);

    const newTableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;
    expect((newTableNode[0] as Element).type).not.equals(TABELLE_STRUCTURE.TABLE);
    expect((newTableNode[0] as Element).type).equals(TABELLE_STRUCTURE.P);
  });

  it('Insert new Absatz using Enter at end of text -> insert new Absatz', () => {
    const plugin = new TabellePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false });
    const [editor] = setupTable(2, 2, true);
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const isNewAbsatz = isLastPosition(editor);
    expect(isNewAbsatz).to.be.true;
  });

  it('Delete Absatz using Backspace at begin of 2nd text -> merge Absaetze', () => {
    const plugin = new TabellePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false });
    const [editor] = setupTable(2, 2);
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    editor.deleteBackward('character');

    const [nodes] = Editor.nodes(editor, { mode: 'lowest' });
    const [node] = nodes as NodeEntry<BaseText>;
    expect(node.text).be.equal('Lorem ipsum');
  });

  it('Split a cell-> two cells with colspan 1', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    Transforms.select(editor, [0, 0, 1, 0, 0]);
    TabelleCommands.splitCell(editor);

    const newSplitNodeOne = Editor.node(editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseElement>;
    const newSplitNodeTwo = Editor.node(editor, [0, 0, 1, 0, 1]) as NodeEntry<BaseElement>;
    expect((newSplitNodeOne[0] as Element).colspan).equals(1);
    expect((newSplitNodeTwo[0] as Element).colspan).equals(1);
  });

  it('Add a column from a left split cell -> new column with colspan 2 cells ', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    Transforms.select(editor, [0, 0, 1, 0, 0]);
    TabelleCommands.splitCell(editor);
    Transforms.select(editor, [0, 0, 1, 0, 2]);
    TabelleCommands.splitCell(editor);
    Transforms.select(editor, [0, 0, 1, 0, 0]);
    TabelleCommands.insertColumn(editor, 1);
    const newColumnNode = Editor.node(editor, [0, 0, 1, 0, 2]) as NodeEntry<BaseElement>;
    expect((newColumnNode[0] as Element).colspan).equals(2);
  });
  it('Add a column from a right split cell -> new column with colspan 2 cells ', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    Transforms.select(editor, [0, 0, 1, 0, 0]);
    TabelleCommands.splitCell(editor);
    const newSplitCellFirst = Editor.node(editor, [0, 0, 1, 0, 1]) as NodeEntry<BaseElement>;
    expect((newSplitCellFirst[0] as Element).colspan).equals(1);
    Transforms.select(editor, [0, 0, 1, 0, 2]);
    TabelleCommands.splitCell(editor);
    const newSplitCellSecond = Editor.node(editor, [0, 0, 1, 0, 3]) as NodeEntry<BaseElement>;
    expect((newSplitCellSecond[0] as Element).colspan).equals(1);
    Transforms.select(editor, [0, 0, 1, 0, 1]);
    TabelleCommands.insertColumn(editor, 1);
    const newColumnNode = Editor.node(editor, [0, 0, 1, 0, 2]) as NodeEntry<BaseElement>;
    expect((newColumnNode[0] as Element).colspan).equals(2);
  });

  it('Add row from a split cell -> new row ', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    Transforms.select(editor, [0, 0, 1, 1, 0]);
    TabelleCommands.splitCell(editor);
    Transforms.select(editor, [0, 0, 1, 1, 2]);
    TabelleCommands.splitCell(editor);
    const tableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;
    expect((tableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((tableRowNode[0] as Element).children[0].type).equals(TABELLE_STRUCTURE.TABLE_CELL);
    expect((tableRowNode[0] as Element).children.length).equals(4);
    Transforms.select(editor, [0, 0, 1, 1, 1]);
    TabelleCommands.insertRow(editor, 0);

    const newTableRowNode = Editor.node(editor, [0, 0, 1, 1]) as NodeEntry<BaseElement>;
    expect((newTableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((newTableRowNode[0] as Element).children[0].type).equals(TABELLE_STRUCTURE.TABLE_CELL);
    expect((newTableRowNode[0] as Element).children.length).equals(2);
  });

  it('Delete column from a split cell -> only one column left ', () => {
    const [editor, _registry] = setupTable(2, 2);

    // Get Table Node by Path
    const tableNode = Editor.node(editor, [0, 0, 1]) as NodeEntry<BaseElement>;

    // Check if Node is Table an has 2 children
    expect((tableNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE);
    expect((tableNode[0] as Element).children.length).equals(2);
    Transforms.select(editor, [0, 0, 1, 0, 0]);
    TabelleCommands.splitCell(editor);
    const newSplitCell = Editor.node(editor, [0, 0, 1, 0, 1]) as NodeEntry<BaseElement>;
    expect((newSplitCell[0] as Element).colspan).equals(1);
    Transforms.select(editor, [0, 0, 1, 0, 1]);
    TabelleCommands.deleteColumn(editor, DocumentType.BEGRUENDUNG_STAMMGESETZ);
    const newTableRowNode = Editor.node(editor, [0, 0, 1, 0]) as NodeEntry<BaseElement>;
    expect((newTableRowNode[0] as Element).type).equals(TABELLE_STRUCTURE.TABLE_ROW);
    expect((newTableRowNode[0] as Element).children[0].type).equals(TABELLE_STRUCTURE.TABLE_HEADERCELL);
    expect((newTableRowNode[0] as Element).children.length).equals(1);
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};

const createContainerWithEmptyParagraph = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};

const createContainerWithoutParagraph = (): Element => {
  const num = createElementOfType(BEGRUENDUNG_STRUCTURE.NUM, [createTextWrapper('1')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [num]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
