// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { DocumentStateType, DocumentType } from '../../../../src/api';
import { ColumnSize, EditorDocument } from '../../../../src/lea-editor/plugin-core';
import { HeaderPlugin } from '../../../../src/lea-editor/plugins/regelungstext/stammform';

describe('Plugin', () => {
  it('Should get viewToolItemTypes and equals null', () => {
    const editorDocument: EditorDocument = {
      documentStateType: DocumentStateType.DRAFT,
      documentType: DocumentType.REGELUNGSTEXT_STAMMGESETZ,
      id: '',
      index: 1,
      isAuthorialNoteEditable: false,
      isEditable: true,
      isReadonly: false,
      documentHasPreambel: false,
      documentTitle: 'Testdokument',
      size: ColumnSize.MEDIUM,
      visible: true,
    };
    const plugin = new HeaderPlugin({ editorDocument, isDynAenderungsvergleich: false });
    const viewToolItemTypes = plugin.viewToolItemTypes;

    expect(viewToolItemTypes).equals(null);
  });
});
