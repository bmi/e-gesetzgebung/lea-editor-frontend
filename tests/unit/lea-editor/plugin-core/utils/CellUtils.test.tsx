// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { setupEditorsForCellStrureComparison } from './utils/setupMehods';
import { createDocumentsForCellStrureComarison } from './utils/createMethods';
import { Transforms } from 'slate';
import { DiffTable, checkIfCellsVisible } from '../../../../../src/lea-editor/plugin-core/utils';
import {
  useCellStructureDispatch,
  useCellStructureState,
} from '../../../../../src/lea-editor/plugins/synopsis/cellStructure/UseCellStructure';
import React, { ReactElement, useEffect, useMemo } from 'react';
import { ReactEditor } from 'slate-react';
import { HistoryEditor } from 'slate-history';
import { Root, createRoot } from 'react-dom/client';
import { act } from 'react';
import { WithReducer, createReducer } from '../../../../../src/components/Store';
import { StoreProvider, store } from '@plateg/theme/src/components/store';
import { getDocumentDTOFromType } from '../../plugins/PluginFactory.test';
import { DocumentType } from '../../../../../src/api';

type Props = {
  firstEditor: ReactEditor & HistoryEditor;
  secondEditor: ReactEditor & HistoryEditor;
  diffTableRef: { current: DiffTable | null };
  updatedDiffTableRef: { current: DiffTable | null };
};

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

const TestComponent = ({ firstEditor, secondEditor, diffTableRef, updatedDiffTableRef }: Props) => {
  const { calculateCellStructure, updateCellStructure } = useCellStructureDispatch();
  const cellStructure = useCellStructureState();
  useEffect(() => {
    calculateCellStructure(firstEditor, secondEditor);
  }, []);
  useEffect(() => {
    if (!cellStructure) return;
    if (!diffTableRef.current) {
      diffTableRef.current = cellStructure;
      Transforms.removeNodes(secondEditor, { at: [0, 0, 4] });
      updateCellStructure(secondEditor, 1);
    } else if (!updatedDiffTableRef.current) {
      updatedDiffTableRef.current = cellStructure;
    }
  }, [cellStructure]);
  return <></>;
};

const Provider = ({ children }: { children: ReactElement }) => {
  const reducer = useMemo(() => createReducer(), []);
  return (
    <StoreProvider store={store}>
      <WithReducer reducer={reducer} keyProp="editorStatic">
        {children}
      </WithReducer>
    </StoreProvider>
  );
};

const container = document.createElement('div');
container.id = 'root';
let root: Root;

describe('Test Cell Structure', () => {
  it('Compare two editor documents with each other and generate table for cell structure', async () => {
    document.body.appendChild(container);
    root = createRoot(container);
    const [firstDocument, secondDocument] = createDocumentsForCellStrureComarison();
    const [firstEditor, secondEditor] = setupEditorsForCellStrureComparison(firstDocument, secondDocument);
    let diffTableRef: { current: DiffTable | null } = { current: null };
    let updatedDiffTableRef: { current: DiffTable | null } = { current: null };
    act(() => {
      root.render(
        <Provider>
          <TestComponent
            firstEditor={firstEditor}
            secondEditor={secondEditor}
            diffTableRef={diffTableRef}
            updatedDiffTableRef={updatedDiffTableRef}
          />
        </Provider>,
      );
    });
    await act(() => sleep(50));
    expect(JSON.stringify((diffTableRef.current as DiffTable)[1])).be.equal(
      JSON.stringify([true, true, true, true, true, true, true, false, false, false]),
    );
    expect(JSON.stringify((diffTableRef.current as DiffTable)[2])).be.equal(
      JSON.stringify([true, true, true, true, true, true, true, true, true, true]),
    );
    expect(JSON.stringify((updatedDiffTableRef.current as DiffTable)[1])).be.equal(
      JSON.stringify([true, true, true, true, true, true, true]),
    );
    expect(JSON.stringify((updatedDiffTableRef.current as DiffTable)[2])).be.equal(
      JSON.stringify([true, true, true, true, true, true, true]),
    );
    document.body.removeChild(container);
    container.remove();
    act(() => {
      root.unmount();
    });
  });
  it('Test if cell structure is shown for documents of same type and different types', () => {
    const visible1 = checkIfCellsVisible(true, getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ), [
      getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ),
    ]);
    const visible2 = checkIfCellsVisible(true, getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ), [
      getDocumentDTOFromType(DocumentType.BEGRUENDUNG_STAMMGESETZ),
    ]);
    expect(visible1).be.equal(true);
    expect(visible2).be.equal(false);
  });
});
