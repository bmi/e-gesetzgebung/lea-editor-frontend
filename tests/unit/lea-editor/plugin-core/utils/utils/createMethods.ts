// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createElementOfType, createTextWrapper } from '../../../../../../src/lea-editor/plugin-core/utils';
import { createArticle } from '../../../plugins/regelungstext/stammform/juristischer-absatz/utils/createMethods';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../src/lea-editor/plugins/regelungstext';
import { Element } from '../../../../../../src/interfaces';

export const createParagraph = (text: string[]): Element => {
  const p1 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text[0])]);
  const p2 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text[1])]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};

export const createDocumentsForCellStrureComarison = () => {
  const article1 = createArticle(['article1, absatz1', 'article1, absatz2']);
  const article2 = createArticle(['article2, absatz1', 'article2, absatz2']);
  const article3 = createArticle(['article3, absatz1', 'article3, absatz2']);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('Section 1')]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Section Header')]);
  const firstDocumentSection = createElementOfType(REGELUNGSTEXT_STRUCTURE.SECTION, [num, heading, article1, article2]);
  const secondDocumentSection = createElementOfType(REGELUNGSTEXT_STRUCTURE.SECTION, [
    num,
    heading,
    article1,
    article2,
    article3,
  ]);
  const firstDocument = createElementOfType(REGELUNGSTEXT_STRUCTURE.BODY, [firstDocumentSection]);
  const secondDocument = createElementOfType(REGELUNGSTEXT_STRUCTURE.BODY, [secondDocumentSection]);

  return [firstDocument, secondDocument];
};
