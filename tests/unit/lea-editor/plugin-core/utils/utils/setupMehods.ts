// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../src/lea-editor/plugin-core';
import { AnlageAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/anlage';
import { CellStructurePlugin } from '../../../../../../src/lea-editor/plugins/synopsis/cellStructure/CellStructurePlugin';

export const setupAnlagePlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new AnlageAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};

export const setupEditorsForCellStrureComparison = (element1: Element, element2: Element) => {
  const registry = new PluginRegistry([
    new CellStructurePlugin({ editorDocument: InitialEditorDocument, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor1 = registry.register();
  const mainEditor2 = registry.register();
  mainEditor1.editor.insertNode(element1);
  mainEditor2.editor.insertNode(element2);
  return [mainEditor1.editor, mainEditor2.editor];
};
