// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { describe } from 'mocha';
import { DocumentDTO, DocumentStateType, DocumentType } from '../../../../../src/api';
import { getReadonlyState } from '../../../../../src/lea-editor/plugin-core/utils/DocumentUtils';

const document: DocumentDTO = {
  id: '2bf75613-434e-4106-9fd0-b61b6925da8e', // Vorblatt Dokument
  title: 'Test Document',
  content: 'Initial content',
  type: DocumentType.VORBLATT_STAMMGESETZ,
  state: DocumentStateType.FINAL,
  version: '',
  createdBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  updatedBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  createdAt: '2022-09-29T14:10:50.673',
  updatedAt: '2022-09-29T14:10:50.673',
  documentPermissions: {
    hasRead: true,
    hasWrite: false,
  },
  commentPermissions: {
    hasRead: true,
    hasWrite: true,
  },
};

describe('getReadonlyState', () => {
  it('should return true if no write permission', () => {
    const readOnlyState = getReadonlyState(document);
    expect(readOnlyState).eq(true);
  });

  it('should return true if document state is not draft', () => {
    document.documentPermissions.hasWrite = true;
    const readOnlyState = getReadonlyState(document);
    expect(readOnlyState).eq(true);
  });

  it('should return false if document state is draft', () => {
    document.documentPermissions.hasWrite = true;
    document.state = DocumentStateType.DRAFT;
    const readOnlyState = getReadonlyState(document);
    expect(readOnlyState).eq(false);
  });
});
