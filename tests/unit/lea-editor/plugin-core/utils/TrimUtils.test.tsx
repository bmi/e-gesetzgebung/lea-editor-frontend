// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { setupAnlagePlugin } from './utils/setupMehods';
import { createParagraph } from './utils/createMethods';
import { BaseText, Editor, NodeEntry } from 'slate';
import { trimSpaces } from '../../../../../src/lea-editor/plugin-core/utils';

describe('TrimUtils', () => {
  it('Should trim text with multiple spaces', () => {
    const editor = setupAnlagePlugin(createParagraph(['  Lorem    ipsum  ', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 1, 0, 0, 0], offset: 18 },
      anchor: { path: [0, 1, 0, 0, 0], offset: 18 },
    };
    const target = document.createElement('div');
    trimSpaces(editor, target);
    const paragraph = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Lorem ipsum');
  });

  it('Should not trim text in case of prevented ID', () => {
    const editor = setupAnlagePlugin(createParagraph(['  Lorem    ipsum  ', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 1, 0, 0, 0], offset: 18 },
      anchor: { path: [0, 1, 0, 0, 0], offset: 18 },
    };
    const target = document.createElement('div');
    target.id = 'contextMenu';
    trimSpaces(editor, target);
    const paragraph = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('  Lorem    ipsum  ');
  });

  it('Should not trim text in case of no findings', () => {
    const editor = setupAnlagePlugin(createParagraph(['Lorem ipsum', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 1, 0, 0, 0], offset: 18 },
      anchor: { path: [0, 1, 0, 0, 0], offset: 18 },
    };
    const target = document.createElement('div');
    trimSpaces(editor, target);
    const paragraph = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Lorem ipsum');
  });

  it('Should trim text with non breaking space', () => {
    const editor = setupAnlagePlugin(createParagraph(['Lorem\xa0 ipsum', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 1, 0, 0, 0], offset: 18 },
      anchor: { path: [0, 1, 0, 0, 0], offset: 18 },
    };
    const target = document.createElement('div');
    trimSpaces(editor, target);
    const paragraph = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Lorem\xa0ipsum');
  });
});
