// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { EditorRegistry } from '../../../../../src/lea-editor/Editor/EditorRegistry';
import { InitialEditorDocument, PluginRegistry } from '../../../../../src/lea-editor/plugin-core';
import { createElementOfType, createTextWrapper } from '../../../../../src/lea-editor/plugin-core/utils';
import { BegruendungAbsatzPlugin } from '../../../../../src/lea-editor/plugins/begruendung';
import { Element } from '../../../../../src/interfaces';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { VorblattAbsatzPlugin } from '../../../../../src/lea-editor/plugins/vorblatt';

describe('EditorRegistry', () => {
  const setupEditor = () => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    return mainEditor;
  };

  const setupSecondEditor = () => {
    const registry = new PluginRegistry([
      new VorblattAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    return mainEditor;
  };

  it('should register insert node on second editor -> second editor has second container', () => {
    const mainEditor = setupEditor();
    const secondEditor = setupSecondEditor();
    secondEditor.editor.insertNode(createContainer());
    const er = new EditorRegistry([mainEditor, secondEditor]);
    expect(secondEditor.editor.children.length).equals(1);
    expect(secondEditor.isRemote).is.false;
    mainEditor.editor.insertNode(createContainer());
    er.apply(mainEditor);
    setTimeout(() => {
      expect(secondEditor.editor.children.length).equals(2);
      expect(secondEditor.isRemote).is.true;
    });
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
