// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { InitialEditorDocument, PluginRegistry } from '../../../../../../../src/lea-editor/plugin-core';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { BegruendungAbsatzPlugin } from '../../../../../../../src/lea-editor/plugins/begruendung';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { Element } from '../../../../../../../src/interfaces';
import { AuthorialNoteCommands } from '../../../../../../../src/lea-editor/plugins/authorialNote/AuthorialNoteCommands';
import { AuthorialNotePageQueries } from '../../../../../../../src/lea-editor/Editor/AuthorialNote/plugins';
import { Transforms } from 'slate';

describe('AuthorialNotePageQueries', () => {
  const setupContainer = () => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    mainEditor.editor.insertNode(createContainer());
    return mainEditor;
  };

  it('should check if editor is editable -> editor is editable', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 0 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 1, 0, 0, 0], offset: 0 });
    const isEditable = AuthorialNotePageQueries.isEditable(mainEditor.editor);
    expect(isEditable).is.true;
  });

  it('should check if authorial note is last element -> authorial note is last element', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 11 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 1, 0], offset: 0 });
    const isLastElement = AuthorialNotePageQueries.isLastElement(mainEditor.editor);
    expect(isLastElement).is.true;
  });

  it('should check if authorial note is first element -> authorial note is first element', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 0 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    const isFirstElement = AuthorialNotePageQueries.isFirstElement(mainEditor.editor);
    expect(isFirstElement).is.true;
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
