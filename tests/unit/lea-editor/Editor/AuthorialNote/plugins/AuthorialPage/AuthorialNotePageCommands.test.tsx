// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { Element } from '../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../src/lea-editor/plugin-core';
import { BegruendungAbsatzPlugin } from '../../../../../../../src/lea-editor/plugins/begruendung';
import { AuthorialNotePageCommands } from '../../../../../../../src/lea-editor/Editor/AuthorialNote/plugins/AuthorialPage/AuthorialNotePageCommands';
import { BaseText, Editor, NodeEntry, Transforms } from 'slate';

describe('AuthorialNotePageCommands', () => {
  const setupContainer = (multipleTextblock?: boolean) => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    if (multipleTextblock) {
      mainEditor.editor.insertNode(createContainerWithTwoTextblock());
    } else {
      mainEditor.editor.insertNode(createContainer());
    }
    return mainEditor;
  };

  it('should merge two paragraphs -> paragraphs are merged', () => {
    const mainEditor = setupContainer(true);
    const [container] = Editor.node(mainEditor.editor, [0, 0]) as NodeEntry<Element>;
    expect(container.children.length).equals(2);
    Transforms.select(mainEditor.editor, { path: [0, 0, 1, 0, 0], offset: 0 });
    AuthorialNotePageCommands.mergeAbsaetze(mainEditor.editor);
    const [paragraphTextNode] = Editor.node(mainEditor.editor, [0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    if (paragraphTextNode) {
      const text = paragraphTextNode.text;
      const [container] = Editor.node(mainEditor.editor, [0, 0]) as NodeEntry<Element>;
      expect(container.children.length).equals(1);
      expect(text).equals('Lorem ipsum');
    }
  });

  it('should insert new absatz -> new absatz added', () => {
    const mainEditor = setupContainer();
    const [container] = Editor.node(mainEditor.editor, [0, 0]) as NodeEntry<Element>;
    expect(container.children.length).equals(1);
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 11 });
    AuthorialNotePageCommands.insertAbsatz(mainEditor.editor);
    const [paragraphTextNode] = Editor.node(mainEditor.editor, [0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    if (paragraphTextNode) {
      const text = paragraphTextNode.text;
      const [container] = Editor.node(mainEditor.editor, [0, 0]) as NodeEntry<Element>;
      expect(container.children.length).equals(2);
      expect(text).equals('');
    }
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};

const createContainerWithTwoTextblock = (): Element => {
  const p1 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const p2 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
