// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CommentResponseDTO } from '../../../../../../src/api';
import { JuristischerAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import { PluginRegistry } from '../../../../../../src/lea-editor/plugin-core';
import { InitialEditorDocument } from '../../../../../../src/lea-editor/plugin-core/editor-store/editor-info/EditorInfoState';
import { Element } from '../../../../../../src/interfaces';
import { MarkerPlugin } from '../../../../../../src/lea-editor/plugin-core/generic-plugins/marker';
import { HeaderPlugin } from '../../../../../../src/lea-editor/plugins/vorblatt';
import {
  createCommentMarkerWrapper,
  createElementOfType,
  createTextWrapper,
} from '../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../src/lea-editor/plugins/regelungstext';

const plugins = [
  new HeaderPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  new MarkerPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
];
export const setupAbsatzPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new JuristischerAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  plugins.forEach((plugin) => plugin.useEffect(mainEditor.editor));
  return mainEditor.editor;
};
export const comments: CommentResponseDTO[] = [
  {
    commentId: '1234',
    content: 'Testkommentar',
    createdAt: '',
    createdBy: { email: 'test', gid: '', name: 'test' },
    deleted: false,
    documentId: '1243456465',
    replies: [],
    state: 'OPEN',
    updatedAt: '',
    updatedBy: { email: '', gid: '', name: '' },
  },
  {
    commentId: '4321',
    content: 'Testkommentar2',
    createdAt: '',
    createdBy: { email: 'test', gid: '', name: 'test' },
    deleted: true,
    documentId: '1243456465',
    replies: [],
    state: 'OPEN',
    updatedAt: '',
    updatedBy: { email: '', gid: '', name: '' },
  },
];

export const createParagraph = (text: string[]): Element => {
  const p1 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [
    createCommentMarkerWrapper('1234', 'start'),
    createTextWrapper(text[0]),
    createCommentMarkerWrapper('1234', 'start'),
  ]);
  const p2 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text[1])]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};
