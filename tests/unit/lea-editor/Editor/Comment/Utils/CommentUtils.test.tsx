// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { it } from 'mocha';
import { Element } from '../../../../../../src/interfaces';
import { comments, setupAbsatzPlugin, createParagraph } from './setupMethos';
import { handleSetActiveAndColorForNodesInCommentRange } from '../../../../../../src/lea-editor/Editor/Comment/Utils';

describe('CommentUtils', () => {
  it('should mark comment as active', () => {
    const editor = setupAbsatzPlugin(createParagraph(['Lorem ipsum', 'dolor sit']));
    handleSetActiveAndColorForNodesInCommentRange('1234', true, false, editor, comments);
    const elementOnComment = editor.node([0, 1, 0, 1])[0] as Element;
    expect(elementOnComment['lea:isActive']).true;
  });
});
