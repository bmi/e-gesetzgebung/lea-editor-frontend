// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { createElementOfType, createTextWrapper } from '../../../../../src/lea-editor/plugin-core/utils';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { Element } from '../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../src/lea-editor/plugin-core';
import { BegruendungAbsatzPlugin } from '../../../../../src/lea-editor/plugins/begruendung';
import { BaseText, Editor, NodeEntry, Transforms } from 'slate';
import { AuthorialNoteCommands } from '../../../../../src/lea-editor/plugins/authorialNote/AuthorialNoteCommands';
import { AuthorialNotePlugin } from '../../../../../src/lea-editor/plugins/authorialNote';

describe('AuthorialNoteCommands', () => {
  const setupContainer = () => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
      new AuthorialNotePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    mainEditor.editor.insertNode(createContainer());
    return mainEditor;
  };

  it('should add new numbered authorial note -> numbered authorial note created', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    const container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).children.length).equals(3);
    const authorialNote = Editor.node(mainEditor.editor, [0, 0, 0, 1]);
    expect((authorialNote[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE);
    expect((authorialNote[0] as Element).marker).equals('1');
  });

  it('should add new custom authorial note -> custom authorial note created', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addCustomAuthorialNote(mainEditor.editor, '*');
    const container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).children.length).equals(3);
    const authorialNote = Editor.node(mainEditor.editor, [0, 0, 0, 1]);
    expect((authorialNote[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE);
    expect((authorialNote[0] as Element).marker).equals('*');
  });

  it('should delete next numbered authorial note -> next numbered authorial note deleted', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    let container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).children.length).equals(3);
    const authorialNote = Editor.node(mainEditor.editor, [0, 0, 0, 1]);
    expect((authorialNote[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE);
    expect((authorialNote[0] as Element).marker).equals('1');
    AuthorialNoteCommands.deleteNextAuthorialNote(mainEditor.editor, () => {});
    container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.P);
    const textElement = Editor.node(mainEditor.editor, [0, 0, 0, 0, 0]);
    expect((textElement[0] as BaseText).text).equals('Lorem ipsum');
  });

  it('should delete previous numbered authorial note -> previous numbered authorial note deleted', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 9 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    let container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).children.length).equals(5);
    const authorialNote = Editor.node(mainEditor.editor, [0, 0, 0, 1]);
    expect((authorialNote[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.AUTHORIALNOTE);
    expect((authorialNote[0] as Element).marker).equals('1');
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 3, 0], offset: 0 });
    AuthorialNoteCommands.deletePreviousAuthorialNote(mainEditor.editor, () => {});
    container = Editor.node(mainEditor.editor, [0, 0, 0]);
    expect((container[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.P);
    const textElement = Editor.node(mainEditor.editor, [0, 0, 0, 0, 0]);
    expect((textElement[0] as BaseText).text).equals('Lorem');
  });

  it('should focus next authorial note', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    AuthorialNoteCommands.focusNextElement(mainEditor.editor, 0);
    expect(mainEditor.editor.selection).equals(null);
  });

  it('should focus previous authorial note', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 2, 0], offset: 0 });
    AuthorialNoteCommands.focusPreviousElement(mainEditor.editor, 0);
    expect(mainEditor.editor.selection).equals(null);
  });

  it('should focus before authorial note', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    const [authorialNote] = Editor.node(mainEditor.editor, [0, 0, 0, 1]) as NodeEntry<Element>;
    AuthorialNoteCommands.focusBeforeAuthorialNote(mainEditor.editor, authorialNote.GUID);
    expect(JSON.stringify(mainEditor.editor.selection)).equals(
      JSON.stringify({
        anchor: { path: [0, 0, 0, 0, 0], offset: 5 },
        focus: { path: [0, 0, 0, 0, 0], offset: 5 },
      }),
    );
  });

  it('should after before authorial note', () => {
    const mainEditor = setupContainer();
    Transforms.select(mainEditor.editor, { path: [0, 0, 0, 0, 0], offset: 5 });
    AuthorialNoteCommands.addNumberedAuthorialNote(mainEditor.editor);
    const [authorialNote] = Editor.node(mainEditor.editor, [0, 0, 0, 1]) as NodeEntry<Element>;
    AuthorialNoteCommands.focusAfterAuthorialNote(mainEditor.editor, authorialNote.GUID);
    expect(JSON.stringify(mainEditor.editor.selection)).equals(
      JSON.stringify({
        anchor: { path: [0, 0, 0, 2, 0], offset: 0 },
        focus: { path: [0, 0, 0, 2, 0], offset: 0 },
      }),
    );
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
