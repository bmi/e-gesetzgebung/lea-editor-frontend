// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../../src/lea-editor/plugin-core';
import {
  JuristischerAbsatzPlugin,
  ListeMitUntergliederungPlugin,
  HeaderPlugin,
} from '../../../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import { MarkerPlugin } from '../../../../../../../../src/lea-editor/plugin-core/generic-plugins/marker';

const plugins = [
  new HeaderPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  new MarkerPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
];

export const setupAbsatzPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new JuristischerAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  plugins.forEach((plugin) => plugin.useEffect(mainEditor.editor));
  return mainEditor.editor;
};

export const setupListenPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new ListeMitUntergliederungPlugin({
      editorDocument: { ...InitialEditorDocument },
      isDynAenderungsvergleich: false,
    }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  plugins.forEach((plugin) => plugin.useEffect(mainEditor.editor));
  return mainEditor.editor;
};

export const setupEmptyAbsatz = () => {
  const registry = new PluginRegistry([
    new JuristischerAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  return mainEditor.editor;
};
