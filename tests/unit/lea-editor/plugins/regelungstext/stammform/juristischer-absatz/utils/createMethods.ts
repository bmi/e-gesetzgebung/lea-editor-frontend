// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../../src/lea-editor/plugins/regelungstext';

export const createTextElement = (text: string): Element => {
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
};

export const createParagraph = (text: string): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};

export const createArticle = (texts: string[]): Element => {
  const paragraphs = texts.map((text: string) => {
    return createParagraph(text);
  });
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('')]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [num, heading, ...paragraphs]);
};

const createPoint = (text: string): Element => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('1.')]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
};

const createWrapUp = (text: string): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.WRAPUP, [p]);
};

export const createParagraphWithList = (
  introText: string,
  listPointTexts: string[],
  wrapUpElementText?: string,
): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  const points = listPointTexts.map((text: string) => {
    return createPoint(text);
  });
  const wrapUp = createWrapUp(wrapUpElementText ?? '');
  const list = createElementOfType(
    REGELUNGSTEXT_STRUCTURE.LIST,
    wrapUpElementText ? [intro, ...points, wrapUp] : [intro, ...points],
  );
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('(1)')]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, list]);
};

const createList = (introText: string, listPointTexts: string[]): Element => {
  const points = listPointTexts.map((text: string) => {
    return createPoint(text);
  });
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, ...points]);
};

export const createParagraphwithPSiblings = (introText: string, PElementTexts: string[]): Element => {
  const pointNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const pElements = PElementTexts.map((PElementText: string) => {
    return createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(PElementText)]);
  });
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, pElements);
  const point = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [pointNum, content]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);
  const paragraphNum = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [paragraphNum, list]);
};

export const createParagraphWithSubList = (
  introText: string,
  listPointText: string,
  subListPointTexts: string[],
): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  const subList = createList(listPointText, subListPointTexts);
  const numPoint = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const point = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [numPoint, subList]);
  const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);
  const numParagraph = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);

  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [numParagraph, list]);
};

export const createArticleWithParagraphWithList = (
  introText: string,
  listPointTexts: string[],
  wrapUpElementText?: string,
): Element => {
  const paragraphWithList = createParagraphWithList(introText, listPointTexts, wrapUpElementText);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('')]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [num, heading, paragraphWithList]);
};
