// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../../src/lea-editor/plugin-core';
import { JuristischerAbsatzPlugin } from '../../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import { createTextElement, createParagraph, createArticle } from './utils/createMethods';
import { setupAbsatzPlugin, setupEmptyAbsatz } from './utils/setupMehods';

const plugin = new JuristischerAbsatzPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});
describe('JuristischerAbsatz', () => {
  it('Delete character using Backspace at beginning of text -> no action', () => {
    const editor = setupAbsatzPlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 0 }, focus: { path: [0, 1, 0, 0, 0], offset: 0 } };
    editor.deleteBackward('character');

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('Lorem ipsum');
  });

  it('Delete character using Backspace at the end of text -> character deleted', () => {
    const editor = setupAbsatzPlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 11 }, focus: { path: [0, 1, 0, 0, 0], offset: 11 } };
    editor.deleteBackward('character');

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('Lorem ipsu');
  });

  it('Delete character using Delete at beginning of text -> character deleted', () => {
    const editor = setupAbsatzPlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 0 }, focus: { path: [0, 1, 0, 0, 0], offset: 0 } };
    editor.deleteForward('character');

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('orem ipsum');
  });

  it('Delete character using Delete at the end of text -> no action', () => {
    const editor = setupAbsatzPlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 11 }, focus: { path: [0, 1, 0, 0, 0], offset: 11 } };
    editor.deleteForward('character');

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('Lorem ipsum');
  });

  it('Delete at the beginning of non-empty paragraph -> no action', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 3, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete in empty paragraph -> merge paragraphs', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNodeExists = Node.has(editor, [0, 3, 1, 0, 0, 0]);
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNodeExists).be.equal(false);
  });

  it('Press enter in beginning of paragraph -> no action', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 2, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 2, 1, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const fisrtNode = Editor.node(editor, [0, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNodeExists = Node.has(editor, [0, 3, 1, 0, 0, 0]);
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNodeExists).be.equal(false);
  });

  it('Press enter at end of paragraph -> insert new paragraph', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 2, 1, 0, 0, 0], offset: 11 },
      focus: { path: [0, 2, 1, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const fisrtNode = Editor.node(editor, [0, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 3, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNode[0].text).be.equal('');
  });

  it('Press List Command in empty Paragraph --> insert list', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const intro = Editor.node(editor, [0, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const listPoint = Editor.node(editor, [0, 2, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).be.equal('Lorem ipsum');
    expect(listPoint[0].text).be.equal('');
  });

  it('Press List Command in at beginning of non-empty paragraph --> no action', () => {
    const editor = setupAbsatzPlugin(createArticle(['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 3, 1, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const intro = Editor.node(editor, [0, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const listPoint = Editor.node(editor, [0, 3, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).be.equal('Lorem ipsum');
    expect(listPoint[0].text).be.equal('dolor sit');
  });

  it('Delete character using Delete within div -> character deleted', () => {
    const editor = setupAbsatzPlugin(createTextElement('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 0, 0], offset: 5 }, focus: { path: [0, 0, 0], offset: 5 } };
    editor.deleteBackward('character');

    const node = Editor.node(editor, [0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('Lore ipsum');
  });

  it('Delete character using Delete within div -> character deleted', () => {
    const editor = setupAbsatzPlugin(createTextElement('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 0, 0], offset: 0 }, focus: { path: [0, 0, 0], offset: 0 } };
    editor.deleteForward('character');

    const node = Editor.node(editor, [0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('orem ipsum');
  });

  it('Delete character using Delete without text node -> return', () => {
    const editor = setupEmptyAbsatz();
    editor.deleteForward('character');
    expect(editor.children.length).be.equal(0);
  });
});
