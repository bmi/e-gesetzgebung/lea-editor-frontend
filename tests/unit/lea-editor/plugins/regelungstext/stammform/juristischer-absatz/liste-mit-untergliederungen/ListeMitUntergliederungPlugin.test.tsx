// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../../../src/lea-editor/plugin-core';
import { ListeMitUntergliederungPlugin } from '../../../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import {
  createParagraphWithList,
  createArticleWithParagraphWithList,
  createParagraphWithSubList,
  createParagraphwithPSiblings,
} from '../utils/createMethods';
import { setupListenPlugin } from '../utils/setupMehods';

const plugin = new ListeMitUntergliederungPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});
describe('Liste mit Untergliederung', () => {
  it('Delete character using Backspace at the end of text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 11 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lorem ipsu');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete character using Delete at beginning of text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('orem ipsum');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete character using Backspace within text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 2 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 2 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lrem ipsum');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete backwards using Backspace at beginning of empty point -> delete point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNodeExists = Node.has(editor, [0, 1, 2, 1, 0, 0, 0]);
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNodeExists).be.false;
  });

  it('Delete backwards using Backspace at beginning of point with text -> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('dolor sit');
  });

  it('Delete Bakwards in empty list with single point --> delete list and turn it into a regular paragraph', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const paragraph = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const previousPointExists = Node.has(editor, [0, 1, 1, 1]);
    expect(paragraph[0].text).to.be.equal('intro-text');
    expect(previousPointExists).to.be.false;
  });

  it('Delete Bakwards in the first empty point of a non-empty list --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const intro = Editor.node(editor, [0, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const firstPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).to.be.equal('intro-text');
    expect(firstPoint[0].text).to.be.equal('');
    expect(secondPoint[0].text).to.be.equal('dolor sit');
  });

  it('Delete Bakwards at the beginning of the only listpoint --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const intro = Editor.node(editor, [0, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const point = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).to.be.equal('intro-text');
    expect(point[0].text).to.be.equal('Lorem ipsum');
  });

  it('Press Enter at end of point --> insert new point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 11 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('');
  });

  it('Press Enter at beginning of point --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPointExists = Node.has(editor, [0, 1, 2, 1, 0, 0, 0]);
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPointExists).be.false;
  });

  it('Press enter in the last and empty point of a list with three points --> close list and add wrap-up', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit', '']));
    editor.selection = {
      anchor: { path: [0, 1, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 3, 1, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const wrapUp = Editor.node(editor, [0, 1, 3, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(wrapUp[0].text).be.equal('');
  });

  it('Press enter in the last and empty point of a sub-list with three points --> close the sub-list and jump to parent-list', () => {
    const editor = setupListenPlugin(
      createParagraphWithSubList('intro-text', 'PointText', ['Lorem ipsum', 'dolor sit', '']),
    );
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 3, 1, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const newPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(newPoint[0].text).be.equal('');
  });

  it('Press enter in the last and empty point of a list with two points --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const firstPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('');
  });

  it('Press enter at end of wrap-up --> jump to new paragraph', () => {
    const editor = setupListenPlugin(
      createArticleWithParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit'], '[Platzhalter]'),
    );
    editor.selection = {
      anchor: { path: [0, 2, 1, 3, 0, 0, 0], offset: 13 },
      focus: { path: [0, 2, 1, 3, 0, 0, 0], offset: 13 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const newParagraph = Editor.node(editor, [0, 3, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(newParagraph[0].text).be.equal('');
  });

  it('Press enter at beginning of wrap-up --> no action', () => {
    const editor = setupListenPlugin(
      createArticleWithParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit'], '[Platzhalter]'),
    );
    editor.selection = {
      anchor: { path: [0, 2, 1, 3, 0, 0, 0], offset: 0 },
      focus: { path: [0, 2, 1, 3, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const wrapUp = Editor.node(editor, [0, 2, 1, 3, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(wrapUp[0].text).be.equal('[Platzhalter]');
  });

  it('Press List Command in an empty list point that does have a higher sibling --> Turn point to a subpoint of the above point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit', '']));
    editor.selection = {
      anchor: { path: [0, 1, 3, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 3, 1, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const subPoint = Editor.node(editor, [0, 1, 2, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(subPoint[0].text).be.equal('');
  });

  it('Press List Command in an empty list point that is the only one in the list --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const point = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(point[0].text).be.equal('');
  });

  it('Press backspace in an empty p-Element --> delete element move selection to end of upper sibling', () => {
    const editor = setupListenPlugin(createParagraphwithPSiblings('', ['lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 1, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 1, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const firstPElement = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPElementExists = Node.has(editor, [0, 1, 1, 1, 1, 0, 0]);
    expect(firstPElement[0].text).be.equal('lorem ipsum');
    expect(secondPElementExists).be.false;
  });
});
