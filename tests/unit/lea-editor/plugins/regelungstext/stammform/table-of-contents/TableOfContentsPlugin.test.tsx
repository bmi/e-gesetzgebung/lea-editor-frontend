// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import {
  createChapter,
  createChapterWithoutSubsections,
  createBody,
  createChapterWithArticles,
  createWrapperWithChapter,
} from './utils/createMethods';
import { setupTableOfContentsPlugin } from './utils/setupMethods';
import { Editor, NodeEntry, BaseText, Node, Ancestor, Transforms } from 'slate';
import { TableOfContentsCommands } from '../../../../../../../src/lea-editor/plugins/regelungstext/stammform/table-of-contents/TableOfContentsCommands';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/regelungstext';
import { TableOfContentsQueries } from '../../../../../../../src/lea-editor/plugins/regelungstext/stammform/table-of-contents/TableOfContentsQueries';
import { Element } from '../../../../../../../src/interfaces';
import { getAllOfType } from '../../../../../../../src/lea-editor/plugin-core/utils';

describe('TableOfContents', () => {
  it('get options for insert below an article --> get option to insert a new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    const addOptions = TableOfContentsQueries.getOptionsForAddElement(editor, articleNode, () => {});
    expect(!!addOptions.find((option) => option.type === REGELUNGSTEXT_STRUCTURE.ARTICLE)).to.be.true;
  });

  it('get options for insert below a subsection --> get option to insert a new subsection', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subsectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const addOptions = TableOfContentsQueries.getOptionsForAddElement(editor, subsectionNode, () => {});
    expect(!!addOptions.find((option) => option.type === REGELUNGSTEXT_STRUCTURE.SUBSECTION)).to.be.true;
  });

  it('get options for insert element into a subsection --> get option to insert a new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subsectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const insertOptions = TableOfContentsQueries.getOptionsForAddElement(editor, subsectionNode, () => {});
    expect(!!insertOptions.find((option) => option.type === REGELUNGSTEXT_STRUCTURE.ARTICLE)).to.be.true;
  });

  it('get options for insert element into a section contisning no subsections --> get option to insert a new subsection or article', () => {
    const editor = setupTableOfContentsPlugin(createChapterWithoutSubsections(1));
    const sectionNode = Editor.node(editor, [0, 2]) as NodeEntry<Element>;
    const insertOptions = TableOfContentsQueries.getOptionsForAddElement(editor, sectionNode, () => {});
    expect(!!insertOptions.find((option) => option.type === REGELUNGSTEXT_STRUCTURE.ARTICLE)).to.be.true;
  });

  it('get options for subdivide subsection element with articles into a new section -> get options to subdivide into title', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subsectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    expect(
      TableOfContentsQueries.getOptionsForSubdivideElement(editor, subsectionNode, () => {})
        .map((option) => option.type)
        .toString(),
    ).to.be.equal([REGELUNGSTEXT_STRUCTURE.TITLE].toString());
  });

  it('get options for subdivide subsection element with articles into a new section -> get options to subdivide into title and subsection', () => {
    const editor = setupTableOfContentsPlugin(createChapterWithoutSubsections(1));
    const sectionNode = Editor.node(editor, [0, 2]) as NodeEntry<Element>;
    expect(
      TableOfContentsQueries.getOptionsForSubdivideElement(editor, sectionNode, () => {})
        .map((option) => option.type)
        .toString(),
    ).to.be.equal([REGELUNGSTEXT_STRUCTURE.SUBSECTION, REGELUNGSTEXT_STRUCTURE.TITLE].toString());
  });

  it('get options for subdivide chapter element with articles into a new section -> get options to subdivide', () => {
    const editor = setupTableOfContentsPlugin(createChapterWithArticles(1));
    const sectionNode = Editor.node(editor, [0]) as NodeEntry<Element>;
    expect(
      TableOfContentsQueries.getOptionsForSubdivideElement(editor, sectionNode, () => {})
        .map((option) => option.type)
        .toString(),
    ).to.be.equal(
      [REGELUNGSTEXT_STRUCTURE.SUBCHAPTER, REGELUNGSTEXT_STRUCTURE.SECTION, REGELUNGSTEXT_STRUCTURE.TITLE].toString(),
    );
  });

  it('get options for wrapp chapter element -> get options to wrapp', () => {
    const editor = setupTableOfContentsPlugin(createChapterWithoutSubsections(1));
    const sectionNode = Editor.node(editor, [0]) as NodeEntry<Element>;
    expect(
      TableOfContentsQueries.getOptionsForWrappElement(editor, sectionNode, () => {})
        .map((option) => option.type)
        .toString(),
    ).to.be.equal([REGELUNGSTEXT_STRUCTURE.BOOK, REGELUNGSTEXT_STRUCTURE.PART].toString());
  });

  it('get options for wrapp Section element -> get no options to wrapp', () => {
    const editor = setupTableOfContentsPlugin(createChapterWithoutSubsections(1));
    const sectionNode = Editor.node(editor, [0, 1]) as NodeEntry<Element>;
    expect(TableOfContentsQueries.getOptionsForWrappElement(editor, sectionNode, () => {}).length).to.be.equal(0);
  });

  it('get options for deleting a subsection and article --> get options in each case', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    const deleteSubSectionOptions = TableOfContentsQueries.getOptionForDeleteElement(editor, subSectionNode, () => {});
    const deleteArticleOptions = TableOfContentsQueries.getOptionForDeleteElement(editor, articleNode, () => {});
    expect(!!deleteSubSectionOptions.onClick).to.be.true;
    expect(!!deleteArticleOptions.type).to.be.true;
  });

  it('get options for deleting a section with subsection --> get options no option', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const sectionNode = Editor.node(editor, [0, 0, 2]) as NodeEntry<Element>;
    const deleteSectionOptions = TableOfContentsQueries.getOptionForDeleteElement(editor, sectionNode, () => {});
    expect(!deleteSectionOptions.onClick).to.be.true;
  });

  it('get options for deleting a section with subsection with content --> get options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const sectionNode = Editor.node(editor, [0, 0, 2]) as NodeEntry<Element>;
    const deleteSectionOptions = TableOfContentsQueries.getOptionForDeleteElementWithContent(
      editor,
      sectionNode,
      () => {},
    );
    expect(!!deleteSectionOptions.onClick).to.be.true;
  });

  it('get options for deleting a subsection with content --> get options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const deleteSubSectionOptions = TableOfContentsQueries.getOptionForDeleteElementWithContent(
      editor,
      subSectionNode,
      () => {},
    );
    expect(!!deleteSubSectionOptions.onClick).to.be.true;
  });

  it('get options for deleting a article with content --> get no options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    const deleteArticleOptions = TableOfContentsQueries.getOptionForDeleteElementWithContent(
      editor,
      articleNode,
      () => {},
    );
    expect(!deleteArticleOptions.onClick).to.be.true;
  });

  it('get options for deleting all subsections --> get options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const deleteSubSectionOptions = TableOfContentsQueries.getOptionForDeleteAllElements(
      editor,
      subSectionNode,
      () => {},
    );
    expect(!!deleteSubSectionOptions.onClick).to.be.true;
  });

  it('get options for deleting all sections, but with subsections inside --> get no options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const sectionNode = Editor.node(editor, [0, 0, 2]) as NodeEntry<Element>;
    const deleteSectionOptions = TableOfContentsQueries.getOptionForDeleteAllElements(editor, sectionNode, () => {});
    expect(!deleteSectionOptions.onClick).to.be.true;
  });

  it('get options for deleting all articles --> get no options', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2, 2]) as NodeEntry<Element>;
    const deleteArticleOptions = TableOfContentsQueries.getOptionForDeleteAllElements(editor, articleNode, () => {});
    expect(!deleteArticleOptions.onClick).to.be.true;
  });

  it('add below an article --> check text of old and new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    const addElementOption = TableOfContentsQueries.getOptionsForAddElement(editor, articleNode, () => {}).find(
      (option) => option.type === REGELUNGSTEXT_STRUCTURE.ARTICLE,
    );
    addElementOption?.onClick && addElementOption.onClick();
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(headerOfSecondArticleNode).to.be.equal('');
  });

  it('add below a section --> check text of old and new section', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const sectionNode = Editor.node(editor, [0, 0, 2]) as NodeEntry<Element>;
    const addElementOption = TableOfContentsQueries.getOptionsForAddElement(editor, sectionNode, () => {}).find(
      (option) => option.type === REGELUNGSTEXT_STRUCTURE.SECTION,
    );
    addElementOption?.onClick && addElementOption.onClick();
    const headerOfFirstSectionNode = (Editor.node(editor, [0, 0, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondSectionNode = (Editor.node(editor, [0, 0, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstSectionNode).to.be.equal('Section Header');
    expect(headerOfSecondSectionNode).to.be.equal('');
  });

  it('add below a subsection --> check text of old and new subsection', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const addElementOption = TableOfContentsQueries.getOptionsForAddElement(editor, subSectionNode, () => {}).find(
      (option) => option.type === REGELUNGSTEXT_STRUCTURE.SUBSECTION,
    );
    addElementOption?.onClick && addElementOption.onClick();
    const headerOfFirstSubSectionNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondSubSectionNode = (Editor.node(editor, [0, 0, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstSubSectionNode).to.be.equal('Subsection Header');
    expect(headerOfSecondSubSectionNode).to.be.equal('');
  });

  it('add subsection inside a section --> check text of old and new subsection', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const sectionNode = Editor.node(editor, [0, 0, 2]) as NodeEntry<Element>;
    const addElementOption = TableOfContentsQueries.getOptionsForAddElement(editor, sectionNode, () => {}).find(
      (option) => option.type === REGELUNGSTEXT_STRUCTURE.SUBSECTION,
    );
    addElementOption?.onClick && addElementOption.onClick();
    const headerOfFirstSubSectionNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondSubSectionNode = (Editor.node(editor, [0, 0, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstSubSectionNode).to.be.equal('');
    expect(headerOfSecondSubSectionNode).to.be.equal('Subsection Header');
  });

  it('delete first article of two --> only one article left', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.deleteEinzelvorschrift(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const secondArticleNodeExists = Node.has(editor, [0, 0, 2, 2, 3, 1, 0, 0]);
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(secondArticleNodeExists).to.be.false;
  });

  it('delete second subsection of two --> one subsection containing four articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 3]) as NodeEntry<Element>;
    TableOfContentsCommands.deleteGliederungsEbene(editor, subSectionNode);
    const headerOfFirstSubSectionNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const secondSubSectionNodeExists = Node.has(editor, [0, 0, 2, 3]);
    const newSubsectionNode = Node.get(editor, [0, 0, 2, 2]) as Ancestor;
    expect(headerOfFirstSubSectionNode).to.be.equal('Subsection Header');
    expect(newSubsectionNode.children.length).be.equal(6); // subsection has 6 cildren: num, header, 4 articles
    expect(secondSubSectionNodeExists).to.be.false;
  });

  it('delete second subsection of two with content --> one subsection containing still two articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = editor.node([0, 0, 2, 3]) as NodeEntry<Element>;
    TableOfContentsCommands.deleteGliederungsEbene(editor, subSectionNode, true);
    const headerOfFirstSubSectionNode = (editor.node([0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const secondSubSectionNodeExists = Node.has(editor, [0, 0, 2, 3]);
    const newSubsectionNode = Node.get(editor, [0, 0, 2, 2]) as Ancestor;
    expect(headerOfFirstSubSectionNode).to.be.equal('Subsection Header');
    expect(newSubsectionNode.children.length).be.equal(4); // subsection has 6 cildren: num, header, 4 articles
    expect(secondSubSectionNodeExists).to.be.false;
  });

  it('delete both subsections --> two subsections are replaced by their four articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.deleteGliederungsEbene(editor, subSectionNode); // delete first subsection
    TableOfContentsCommands.deleteGliederungsEbene(editor, subSectionNode); // delete second subsection
    const sectionNode = Node.get(editor, [0, 0, 2]) as Ancestor;
    expect(sectionNode.children.length).be.equal(6); // section has 6 cildren: num, header, 4 articles
  });

  it('delete all subsections of two --> sections containing four articles each', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 3]) as NodeEntry<Element>;
    TableOfContentsCommands.deleteAllGliederungsEbenen(editor, subSectionNode);
    const subsections = getAllOfType(editor, REGELUNGSTEXT_STRUCTURE.SUBSECTION);
    const firstSectionNode = Node.get(editor, [0, 0, 2]) as Ancestor;
    const secondSectionNode = Node.get(editor, [0, 0, 3]) as Ancestor;
    expect(subsections.length).be.equal(0);
    expect(firstSectionNode.children.length).be.equal(6); // subsection has 6 cildren: num, header, 4 articles
    expect(secondSectionNode.children.length).be.equal(6); // subsection has 6 cildren: num, header, 4 articles
  });

  it('wrapp chapter in book --> verify existance of new book', () => {
    const editor = setupTableOfContentsPlugin(createWrapperWithChapter());
    const chapter = Editor.node(editor, [0, 0]) as NodeEntry<Element>;
    TableOfContentsCommands.addWrapperGliederungsEbene(editor, chapter, REGELUNGSTEXT_STRUCTURE.BOOK);
    const [book] = Editor.node(editor, [0, 0]) as NodeEntry<Element>;
    expect(book.type).be.equal(REGELUNGSTEXT_STRUCTURE.BOOK);
    expect(book.children[2].type).be.equal(REGELUNGSTEXT_STRUCTURE.CHAPTER);
  });

  it('add article into subsection --> verify existance of new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const subSectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const addElementOption = TableOfContentsQueries.getOptionsForAddElement(editor, subSectionNode, () => {}).find(
      (option) => option.type === REGELUNGSTEXT_STRUCTURE.ARTICLE,
    );
    addElementOption?.onClick && addElementOption.onClick();
    const newsubSectionNode = Node.get(editor, [0, 0, 2, 2]) as Ancestor;
    const newArticleHeader = (Node.get(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as BaseText).text;
    expect(newsubSectionNode.children.length).be.equal(5); //Contains num, header and 3 articles
    expect(newArticleHeader).to.be.equal('');
  });

  it('get data for table of contents --> Check title of root node', () => {
    const editor = setupTableOfContentsPlugin(createChapter(1));
    const table = TableOfContentsCommands.getContentTableData(editor, false);
    expect(table[0].title).to.be.equal('1 Chapter Header');
  });
  it('article draggable query', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    expect(TableOfContentsQueries.nodeDraggable(editor, articleNode[1])).to.be.true;
  });
  it('subsection draggable query without articles during dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    expect(TableOfContentsQueries.nodeDraggable(editor, [0, 0, 2, 2], true)).to.be.true;
  });
  it('subsection draggable query without articles during not dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    expect(TableOfContentsQueries.nodeDraggable(editor, [0, 0, 2, 2], false)).to.be.false;
  });
  it('subsection draggable query without articles during dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2] });
    expect(TableOfContentsQueries.nodeDroppable(editor, [0, 0, 2])).to.be.true;
  });
  it('section draggable query without articles during not dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2] });
    expect(TableOfContentsQueries.nodeDraggable(editor, [0, 0, 2], false)).to.be.false;
  });
  it('subsection draggable query with articles during dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    expect(TableOfContentsQueries.nodeDraggable(editor, [0, 0, 2, 2], true)).to.be.true;
  });
  it('section draggable query with articles during dragging', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    expect(TableOfContentsQueries.nodeDroppable(editor, [0, 0, 2])).to.be.false;
  });
  it('move article one up', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 3, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 3]) as NodeEntry<Element>;
    TableOfContentsCommands.moveOneUp(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Test');
    expect(headerOfSecondArticleNode).to.be.equal('Article Header');
  });

  it('move article one down', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveOneDown(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(headerOfSecondArticleNode).to.be.equal('Test');
  });

  it('move article to Top', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 1, 2, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 1, 2, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveAsFirstOfType(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfOriginArticleNode = (Editor.node(editor, [0, 1, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfOriginArticleNode).to.be.equal('Article Header');
    expect(headerOfFirstArticleNode).to.be.equal('Test');
  });

  it('move article to Bottom', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveAsLastOfType(editor, articleNode);
    const headerOfLastArticleNode = (Editor.node(editor, [0, 1, 3, 3, 4, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfOriginArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfOriginArticleNode).to.be.equal('Article Header');
    expect(headerOfLastArticleNode).to.be.equal('Test');
  });

  it('move article to GliederungsEbene as child', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 3, 2, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 3, 2]) as NodeEntry<Element>;
    const subsectionNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveElement(editor, articleNode, subsectionNode, true, true);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 3, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Test');
    expect(headerOfSecondArticleNode).to.be.equal('Article Header');
  });

  it('check if can add article as children to subsection with articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    expect(TableOfContentsQueries.canDropAsChild(editor, [0, 0, 2, 2])).to.be.true;
  });

  it('check if can add article as children to subsection without articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    expect(TableOfContentsQueries.canDropAsChild(editor, [0, 0, 2, 2])).to.be.true;
  });

  it('check if can add article to existing children in subsection with articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    expect(TableOfContentsQueries.canDropToExistingChildren(editor, [0, 0, 2, 2])).to.be.true;
  });

  it('check if can add article to existing children in subsection without articles', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    Transforms.removeNodes(editor, { at: [0, 0, 2, 2, 2] });
    expect(TableOfContentsQueries.canDropToExistingChildren(editor, [0, 0, 2, 2])).to.be.false;
  });
});
