// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../../src/lea-editor/plugins/regelungstext';

// Creates a paragraph
const createParagraph = (paragraphNumber: number): Element => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(paragraphNumber.toString())]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper('Paragraph text')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};

// Creates article containing two paragraphs
const createArticle = (articleNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(articleNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Article Header')]);
  const paragraph1 = createParagraph(1);
  const paragraph2 = createParagraph(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.ARTICLE, [num, heading, paragraph1, paragraph2]);
};

// Creates subsection containing two articles
const createSubSection = (subSectionNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(subSectionNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Subsection Header')]);
  const article1 = createArticle(1);
  const article2 = createArticle(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.SUBSECTION, [num, heading, article1, article2]);
};

// Creates section containing two subsections
const createSection = (sectionNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(sectionNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Section Header')]);
  const subSetion1 = createSubSection(1);
  const subSetion2 = createSubSection(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.SECTION, [num, heading, subSetion1, subSetion2]);
};

// Creates section containing two articles, subsections do not occur
const createSectionWithoutSubsections = (sectionNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(sectionNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Section Header')]);
  const article1 = createArticle(1);
  const article2 = createArticle(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.SECTION, [num, heading, article1, article2]);
};

// Creates chapter containing two sections
export const createChapter = (chapterNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(chapterNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Chapter Header')]);
  const setion1 = createSection(1);
  const setion2 = createSection(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.CHAPTER, [num, heading, setion1, setion2]);
};

// Creates chapter containing two sections, subsections do not occur
export const createChapterWithoutSubsections = (chapterNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(chapterNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Chapter Header')]);
  const setion1 = createSectionWithoutSubsections(1);
  const setion2 = createSectionWithoutSubsections(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.CHAPTER, [num, heading, setion1, setion2]);
};

// Creates chapter containing two sections, subsections do not occur
export const createChapterWithArticles = (chapterNumber: number) => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper(chapterNumber.toString())]);
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Chapter Header')]);
  const article1 = createArticle(1);
  const article2 = createArticle(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.CHAPTER, [num, heading, article1, article2]);
};

export const createWrapperWithChapter = () => {
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.TEXTWRAPPER, [createChapterWithArticles(1)]);
};

// Creates body containing two chapters
export const createBody = () => {
  const chapter1 = createChapter(1);
  const chapter2 = createChapter(2);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.BODY, [chapter1, chapter2]);
};
