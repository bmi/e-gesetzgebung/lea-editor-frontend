// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { createBody } from './utils/createMethods';
import { setupTableOfContentsPlugin } from './utils/setupMethods';
import { Editor, NodeEntry, BaseText, Node, Transforms } from 'slate';
import { TableOfContentsCommands } from '../../../../../../../src/lea-editor/plugins/regelungstext/mantelform/table-of-contents/TableOfContentsCommands';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/regelungstext';
import { TableOfContentsQueries } from '../../../../../../../src/lea-editor/plugins/regelungstext/mantelform/table-of-contents/TableOfContentsQueries';
import { Element } from '../../../../../../../src/interfaces';
import { isFirstOfType, isLastOfType } from '../../../../../../../src/lea-editor/plugin-core/utils';
describe('TableOfContents', () => {
  it('get options for insert above and below an article --> get option to insert a new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    const addOptions = TableOfContentsQueries.getOptionsForAddElement(editor, articleNode, () => {});
    expect(
      JSON.stringify(addOptions.map((option) => option.type)) === JSON.stringify([REGELUNGSTEXT_STRUCTURE.ARTICLE]),
    ).to.be.true;
  });

  it('add below an article --> check text of old and new article', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    TableOfContentsQueries.getOptionsForAddElement(editor, articleNode, () => {})[0].onClick?.();
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(headerOfSecondArticleNode).to.be.equal('');
  });

  it('delete first article of two --> only one article left', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.handleDeleteArticle(editor, articleNode[1]);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const secondArticleNodeExists = Node.has(editor, [0, 0, 2, 3, 1, 0, 0]);
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(secondArticleNodeExists).to.be.false;
  });

  it('article draggable query', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    expect(TableOfContentsQueries.nodeDraggable(editor, articleNode[1])).to.be.true;
  });

  it('article is first query', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    expect(isFirstOfType(editor, articleNode[0].type, articleNode[1])).to.be.true;
  });

  it('article is last query', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const articleNode = Editor.node(editor, [0, 1, 3, 3, 3]) as NodeEntry<Element>;
    expect(isLastOfType(editor, articleNode[0].type, articleNode[1])).to.be.true;
  });

  it('move article one up', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 3, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 3]) as NodeEntry<Element>;
    TableOfContentsCommands.moveOneUp(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>)[0]
      .text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfFirstArticleNode).to.be.equal('Test');
    expect(headerOfSecondArticleNode).to.be.equal('Article Header');
  });

  it('move article one down', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveOneDown(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfSecondArticleNode = (Editor.node(editor, [0, 0, 2, 2, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>)[0]
      .text;
    expect(headerOfFirstArticleNode).to.be.equal('Article Header');
    expect(headerOfSecondArticleNode).to.be.equal('Artikel Text');
  });

  it('move article to Top', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 1, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 1, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveAsFirstOfType(editor, articleNode);
    const headerOfFirstArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfOriginArticleNode = (Editor.node(editor, [0, 0, 2, 3, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfOriginArticleNode).to.be.equal('Article Header');
    expect(headerOfFirstArticleNode).to.be.equal('Test');
  });

  it('move article to Bottom', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    Transforms.insertText(editor, 'Test', { at: [0, 0, 2, 2, 1] });
    const articleNode = Editor.node(editor, [0, 0, 2, 2]) as NodeEntry<Element>;
    TableOfContentsCommands.moveAsLastOfType(editor, articleNode);
    const headerOfLastArticleNode = (Editor.node(editor, [0, 1, 3, 4, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    const headerOfOriginArticleNode = (Editor.node(editor, [0, 0, 2, 2, 1, 0, 0]) as NodeEntry<BaseText>)[0].text;
    expect(headerOfOriginArticleNode).to.be.equal('Article Header');
    expect(headerOfLastArticleNode).to.be.equal('Test');
  });

  it('get data for table of contents --> Check title of root node', () => {
    const editor = setupTableOfContentsPlugin(createBody());
    const table = TableOfContentsCommands.getContentTableData(editor);
    expect(table[0].title).to.be.equal('1 Article Header');
  });
});
