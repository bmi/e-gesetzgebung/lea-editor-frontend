// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../../src/lea-editor/plugin-core';

import {
  AenderungsbefehlePlugin,
  AenderungsbefehleQueries,
} from '../../../../../../../src/lea-editor/plugins/regelungstext/mantelform';
import {
  createParagraphWithList,
  createParagraphWithSubList,
  createQuotedStructureWithParagraph,
} from './utils/createMethods';
import { setupListenPlugin } from './utils/setupMehods';

const plugin = new AenderungsbefehlePlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});
describe('Aenderungsbefehle', () => {
  it('Delete character using Backspace at the end of text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 11 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lorem ipsu');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete character using Delete at beginning of text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('orem ipsum');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete character using Backspace within text -> character deleted', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 2 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 2 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lrem ipsum');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Delete backwards using Backspace at beginning of empty point -> delete point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNodeExists = Node.has(editor, [0, 1, 2, 1, 0, 0, 0, 0]);
    expect(fisrtNode[0].text).be.equal('Lorem ipsum');
    expect(secondNodeExists).be.false;
  });

  it('Delete backwards using Backspace at beginning of point with text -> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('dolor sit');
  });

  it('Delete Bakwards in empty list with single point --> no Action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const paragraph = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).to.be.equal('');
  });

  it('Delete Bakwards in the first empty point of a non-empty list --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const intro = Editor.node(editor, [0, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const firstPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).to.be.equal('intro-text');
    expect(firstPoint[0].text).to.be.equal('');
    expect(secondPoint[0].text).to.be.equal('dolor sit');
  });

  it('Delete Bakwards at the beginning of the only listpoint --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');

    const intro = Editor.node(editor, [0, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const point = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(intro[0].text).to.be.equal('intro-text');
    expect(point[0].text).to.be.equal('Lorem ipsum');
  });

  it('Press Enter at end of point --> insert new point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 11 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('');
  });

  it('Press Enter at beginning of point --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const fisrtPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPointExists = Node.has(editor, [0, 1, 2, 1, 0, 0, 0, 0]);
    expect(fisrtPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPointExists).be.false;
  });

  it('Press enter in the last and empty point of a sub-list with three points --> close the sub-list and jump to parent-list', () => {
    const editor = setupListenPlugin(
      createParagraphWithSubList('intro-text', 'PointText', ['Lorem ipsum', 'dolor sit', '']),
    );
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 3, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 3, 1, 0, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const newPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(newPoint[0].text).be.equal('');
  });

  it('Press enter in the last and empty point of a list with two points --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', '']));
    editor.selection = {
      anchor: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 2, 1, 0, 0, 0, 0], offset: 0 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      key: 'Enter',
      code: 'Enter',
    });
    plugin.keyDown(editor, enterKey);

    const firstPoint = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondPoint = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstPoint[0].text).be.equal('Lorem ipsum');
    expect(secondPoint[0].text).be.equal('');
  });

  it('Press List Command in an empty list point that does have a higher sibling --> Turn point to a subpoint of the above point', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit', '']));
    editor.selection = {
      anchor: { path: [0, 1, 3, 1, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 3, 1, 0, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const subPoint = Editor.node(editor, [0, 1, 2, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(subPoint[0].text).be.equal('');
  });

  it('Press List Command in an empty list point that is the only one in the list --> no action', () => {
    const editor = setupListenPlugin(createParagraphWithList('intro-text', ['']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 0 },
    };
    const listCommand = new KeyboardEvent('keydown', {
      altKey: true,
      charCode: 0,
      code: 'KeyL',
      ctrlKey: false,
      key: 'l',
      keyCode: 76,
      metaKey: false,
      shiftKey: false,
      which: 76,
    });
    plugin.keyDown(editor, listCommand);

    const point = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(point[0].text).be.equal('');
  });
  it('Get quotes of first and last child in a quoted structure', () => {
    const editor = setupListenPlugin(createQuotedStructureWithParagraph());
    const startQuoteLocation = [0, 0, 0, 0];
    const endQuoteLocation = [0, 0, 1, 0, 0];
    const startQuoteElement = AenderungsbefehleQueries.getQuotes(editor, startQuoteLocation);
    const endQuoteElement = AenderungsbefehleQueries.getQuotes(editor, endQuoteLocation);
    expect(startQuoteElement.startQuote).be.equal('„');
    expect(endQuoteElement.endQuote).be.equal('“');
  });
});
