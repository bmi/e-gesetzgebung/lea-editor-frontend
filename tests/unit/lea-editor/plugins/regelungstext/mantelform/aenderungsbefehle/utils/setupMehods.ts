// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../../src/lea-editor/plugin-core';
import { AenderungsbefehlePlugin } from '../../../../../../../../src/lea-editor/plugins/regelungstext/mantelform';
import { HeaderPlugin } from '../../../../../../../../src/lea-editor/plugins/regelungstext/mantelform';
import { MarkerPlugin } from '../../../../../../../../src/lea-editor/plugin-core/generic-plugins/marker';

const plugins = [
  new HeaderPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  new MarkerPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
];

export const setupListenPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new AenderungsbefehlePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  plugins.forEach((plugin) => plugin.useEffect(mainEditor.editor));
  return mainEditor.editor;
};
