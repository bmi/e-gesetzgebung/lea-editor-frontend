// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../../src/lea-editor/plugins/regelungstext';
import { createParagraph as createParagraphStammform } from '../../../stammform/juristischer-absatz/utils/createMethods';

const createPoint = (text: string): Element => {
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const mod = createElementOfType(REGELUNGSTEXT_STRUCTURE.MOD, [createTextWrapper(text)]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [mod]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [num, content]);
};

export const createParagraphWithList = (introText: string, listPointTexts: string[]): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  const points = listPointTexts.map((text: string) => {
    return createPoint(text);
  });
  const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, ...points]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('(1)')]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, list]);
};

const createList = (introText: string, listPointTexts: string[]): Element => {
  const points = listPointTexts.map((text: string) => {
    return createPoint(text);
  });
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, ...points]);
};

export const createParagraphWithSubList = (
  introText: string,
  listPointText: string,
  subListPointTexts: string[],
): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(introText)]);
  const intro = createElementOfType(REGELUNGSTEXT_STRUCTURE.INTRO, [p]);
  const subList = createList(listPointText, subListPointTexts);
  const numPoint = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);
  const point = createElementOfType(REGELUNGSTEXT_STRUCTURE.POINT, [numPoint, subList]);
  const list = createElementOfType(REGELUNGSTEXT_STRUCTURE.LIST, [intro, point]);
  const numParagraph = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('')]);

  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [numParagraph, list]);
};

export const createQuotedStructureWithParagraph = (): Element => {
  const paragrah = createParagraphStammform('test');
  const quotedStructure = createElementOfType(REGELUNGSTEXT_STRUCTURE.QUOTED_STRUCTURE, [paragrah], true, {
    startQuote: '„',
    endQuote: '“',
  });
  return quotedStructure;
};
