// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { GesetzPraeambelPlugin } from '../../../../../../../src/lea-editor/plugins/regelungstext/preambel';
import { createBillWithRecticals, createBillWithoutRecticals } from './utils/createMethods';
import { setupPraeambelPlugin } from './utils/setupMehods';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../../src/lea-editor/plugin-core';

const plugin = new GesetzPraeambelPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});

describe('Präambel', () => {
  it('press backspace at ending of rectical --> delete last character', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['lorem ipsum', 'dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 1, 1, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 1, 1, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const firstRectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstRectical[0].text).be.equal('lorem ipsu');
  });

  it('press delete at beginning of rectical --> delete first character', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['lorem ipsum', 'dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const firstRectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstRectical[0].text).be.equal('orem ipsum');
  });

  it('press enter at end of rectical --> insert new rectical', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 0, 1, 1, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 1, 1, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const oldRectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const newRectical = Editor.node(editor, [0, 0, 1, 2, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(oldRectical[0].text).be.equal('lorem ipsum');
    expect(newRectical[0].text).be.equal('');
  });

  it('press backspace at beginning of empty rectical --> delete rectical', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['lorem ipsum', '']));
    editor.selection = {
      focus: { path: [0, 0, 1, 2, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 2, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const firstRectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondRecticalExists = Node.has(editor, [0, 0, 1, 2, 0, 0, 0]);
    expect(firstRectical[0].text).be.equal('lorem ipsum');
    expect(secondRecticalExists).be.false;
  });

  it('press backspace at beginning of first rectical --> no action', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const rectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(rectical[0].text).be.equal('lorem ipsum');
  });

  it('press enter at end of formula --> insert recticals', () => {
    const editor = setupPraeambelPlugin(
      createBillWithoutRecticals('Der Bundestag hat das folgende Gesetz beschlossen:'),
    );
    editor.selection = {
      focus: { path: [0, 0, 0, 0, 0, 0], offset: 50 },
      anchor: { path: [0, 0, 0, 0, 0, 0], offset: 50 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const heading = Editor.node(editor, [0, 0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const rectical = Editor.node(editor, [0, 0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(heading[0].text).be.equal('Präambel');
    expect(rectical[0].text).be.equal('');
  });

  it('press backspace at beginning of first empty rectical --> delete rectical and header', () => {
    const editor = setupPraeambelPlugin(createBillWithRecticals(['']));
    editor.selection = {
      focus: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 1, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const headerExists = Node.has(editor, [0, 0, 1, 0, 0, 0]);
    const recticalExists = Node.has(editor, [0, 0, 1, 1, 0, 0, 0]);
    expect(headerExists).be.false;
    expect(recticalExists).be.false;
  });
});
