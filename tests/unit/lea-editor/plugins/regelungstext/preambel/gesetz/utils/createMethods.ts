// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../../src/lea-editor/plugins/regelungstext';

const createTextElement = (text: string): Element => {
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
};

const createRectical = (text: string): Element => {
  const p = createTextElement(text);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.RECITAL, [p]);
};

const createPreambelWithRecticals = (texts: string[]): Element => {
  const recticalArray = texts.map((text: string) => {
    return createRectical(text);
  });
  const heading = createElementOfType(REGELUNGSTEXT_STRUCTURE.HEADING, [createTextWrapper('Präambel')]);
  const recticals = createElementOfType(REGELUNGSTEXT_STRUCTURE.RECITALS, [heading, ...recticalArray]);
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [
    createTextElement('Der Bundestag hat das folgende Gesetz beschlossen:'),
  ]);
  const formula = createElementOfType(REGELUNGSTEXT_STRUCTURE.FORMULA, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PREAMBLE, [formula, recticals]);
};

const createPreambelWithoutRecticals = (text: string): Element => {
  const p = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextElement(text)]);
  const formula = createElementOfType(REGELUNGSTEXT_STRUCTURE.FORMULA, [p]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PREAMBLE, [formula]);
};

export const createBillWithRecticals = (texts: string[]): Element => {
  const preambel = createPreambelWithRecticals(texts);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.BILL, [preambel]);
};

export const createBillWithoutRecticals = (text: string): Element => {
  const preambel = createPreambelWithoutRecticals(text);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.BILL, [preambel]);
};
