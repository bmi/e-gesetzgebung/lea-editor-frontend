// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { VerordnungPraeambelPlugin } from '../../../../../../../src/lea-editor/plugins/regelungstext/preambel';
import { createBillWithCitations } from './utils/createMethods';
import { setupPraeambelPlugin } from './utils/setupMehods';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../../src/lea-editor/plugin-core';

const plugin = new VerordnungPraeambelPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});

describe('Präambel', () => {
  it('press backspace at ending of citation --> delete last character', () => {
    const editor = setupPraeambelPlugin(createBillWithCitations(['lorem ipsum', 'dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 0, 0, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const firstCitation = Editor.node(editor, [0, 0, 0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstCitation[0].text).be.equal('lorem ipsu');
  });

  it('press delete at beginning of citation --> delete first character', () => {
    const editor = setupPraeambelPlugin(createBillWithCitations(['lorem ipsum', 'dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 0, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const firstCitation = Editor.node(editor, [0, 0, 0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(firstCitation[0].text).be.equal('orem ipsum');
  });

  it('press enter at end of citation --> insert new citation', () => {
    const editor = setupPraeambelPlugin(createBillWithCitations(['lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 0, 0, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const oldCitation = Editor.node(editor, [0, 0, 0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const newCitation = Editor.node(editor, [0, 0, 0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(oldCitation[0].text).be.equal('lorem ipsum');
    expect(newCitation[0].text).be.equal('');
  });

  it('press backspace at beginning of empty citation --> delete citation', () => {
    const editor = setupPraeambelPlugin(createBillWithCitations(['lorem ipsum', '']));
    editor.selection = {
      focus: { path: [0, 0, 0, 1, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 0, 1, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const firstCitation = Editor.node(editor, [0, 0, 0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondCitationExists = Node.has(editor, [0, 0, 0, 1, 0, 0, 0]);
    expect(firstCitation[0].text).be.equal('lorem ipsum');
    expect(secondCitationExists).be.false;
  });

  it('press backspace at beginning of first citation --> no action', () => {
    const editor = setupPraeambelPlugin(createBillWithCitations(['lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 0, 0, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const citation = Editor.node(editor, [0, 0, 0, 0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(citation[0].text).be.equal('lorem ipsum');
  });
});
