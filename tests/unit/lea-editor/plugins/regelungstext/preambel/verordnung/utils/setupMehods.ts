// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../../src/lea-editor/plugin-core';
import { VerordnungPraeambelPlugin } from '../../../../../../../../src/lea-editor/plugins/regelungstext/preambel';

export const setupPraeambelPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new VerordnungPraeambelPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};
