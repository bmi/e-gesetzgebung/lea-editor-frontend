// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../../../src/lea-editor/plugins/regelungstext';

const createTextElement = (text: string): Element => {
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
};

const createCitation = (text: string): Element => {
  const p = createTextElement(text);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.CITATION, [p]);
};

const createPreambelWithCitations = (texts: string[]): Element => {
  const citationArray = texts.map((text: string) => {
    return createCitation(text);
  });
  const citations = createElementOfType(REGELUNGSTEXT_STRUCTURE.CITATIONS, [...citationArray]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PREAMBLE, [citations]);
};

export const createBillWithCitations = (texts: string[]): Element => {
  const preambel = createPreambelWithCitations(texts);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.BILL, [preambel]);
};
