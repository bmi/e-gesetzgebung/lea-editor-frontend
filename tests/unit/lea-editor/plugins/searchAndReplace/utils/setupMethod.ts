// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PluginRegistry, InitialEditorDocument } from '../../../../../../src/lea-editor/plugin-core/';
import { JuristischerAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/regelungstext/stammform';
import { SearchReplacePlugin } from '../../../../../../src/lea-editor/plugins/searchAndReplace';
import { Element } from '../../../../../../src/interfaces';

export const setup = (element: Element) => {
  const registry = new PluginRegistry([
    new JuristischerAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    new SearchReplacePlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  const editor = mainEditor.editor;
  editor.insertNode(element);
  return editor;
};
