// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createElementOfType, createTextWrapper } from '../../../../../../src/lea-editor/plugin-core/utils';
import { REGELUNGSTEXT_STRUCTURE } from '../../../../../../src/lea-editor/plugins/regelungstext';
import { Element } from '../../../../../../src/interfaces';

export const createParagraph = (text: string): Element => {
  const p1 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  const p2 = createElementOfType(REGELUNGSTEXT_STRUCTURE.P, [createTextWrapper(text)]);
  const num = createElementOfType(REGELUNGSTEXT_STRUCTURE.NUM, [createTextWrapper('1')]);
  const content = createElementOfType(REGELUNGSTEXT_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(REGELUNGSTEXT_STRUCTURE.PARAGRAPH, [num, content]);
};
