// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { NodeEntry, BaseText, Editor, Transforms } from 'slate';
import { REPLACE_ONE, SEARCH_NEXT, REPLACE_ALL, SEARCH_PREV } from '../../../../../src/constants';
import { SearchReplaceCommands as Commands } from '../../../../../src/lea-editor/plugins/searchAndReplace';
import { createParagraph } from './utils/createMethods';
import { setup } from './utils/setupMethod';
import { SearchReplaceEditor } from '../../../../../src/lea-editor/plugins/searchAndReplace/SearchReplaceEditor';
import { SearchForTypes } from '../../../../../src/lea-editor/plugins/searchAndReplace/searchReplaceOptions/SearchReplaceOptions';

describe('SearchReplaceCommands', () => {
  it('should find next search result', () => {
    const editor = setup(createParagraph('Platzhalter')) as unknown as SearchReplaceEditor;
    editor.searchType = SearchForTypes.TextReadonly;
    Commands.search(editor, 'Platz');
    expect(editor.searchPosition).eq(0);
    Commands.selectNextSearchResult(editor, SEARCH_NEXT);
    expect(editor.searchCount).eq(2);
    expect(editor.searchPosition).eq(1);
  });

  it('should find previous search result', () => {
    const editor = setup(createParagraph('Platzhalter')) as unknown as SearchReplaceEditor;
    editor.searchType = SearchForTypes.TextReadonly;
    Commands.search(editor, 'Platz');
    expect(editor.searchPosition).eq(0);
    Commands.selectNextSearchResult(editor, SEARCH_PREV);
    expect(editor.searchCount).eq(2);
    expect(editor.searchPosition).eq(2);
  });

  it('should replace first search Result', () => {
    const editor = setup(createParagraph('Platzhalter')) as unknown as SearchReplaceEditor;
    editor.searchType = SearchForTypes.TextReadonly;
    Commands.search(editor, 'Platz');
    expect(editor.searchPosition).eq(0);
    Commands.selectNextSearchResult(editor, SEARCH_NEXT);
    Transforms.setSelection(editor, { focus: { path: [0, 1, 0, 0, 0], offset: 5 } });
    Commands.replace(editor, REPLACE_ONE, 'Test');
    const [paragraphTextNode] = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    if (paragraphTextNode) {
      const text = paragraphTextNode.text;
      expect(text).eq('Testhalter');
    }
  });

  it('should replace all search Results', () => {
    const editor = setup(createParagraph('Platzhalter')) as unknown as SearchReplaceEditor;
    editor.searchType = SearchForTypes.TextReadonly;
    Commands.search(editor, 'Platz');
    expect(editor.searchPosition).eq(0);
    Commands.selectNextSearchResult(editor, SEARCH_NEXT);
    Transforms.setSelection(editor, { focus: { path: [0, 1, 0, 0, 0], offset: 5 } });
    Commands.replace(editor, REPLACE_ALL, 'Test');
    const [firstParagraphTextNode] = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    if (firstParagraphTextNode) {
      const text = firstParagraphTextNode.text;
      expect(text).eq('Testhalter');
    }
    const [secondParagraphTextNode] = Editor.node(editor, [0, 1, 1, 0, 0]) as NodeEntry<BaseText>;
    if (secondParagraphTextNode) {
      const text = secondParagraphTextNode.text;
      expect(text).eq('Testhalter');
    }
  });
});
