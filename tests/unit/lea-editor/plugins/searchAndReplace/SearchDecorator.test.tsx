// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { Editor, NodeEntry, BaseText } from 'slate';
import { SearchReplaceEditor } from '../../../../../src/lea-editor/plugins/searchAndReplace/SearchReplaceEditor';
import { createParagraph } from './utils/createMethods';
import { SearchDecorator } from '../../../../../src/lea-editor/plugins/searchAndReplace';
import { setup } from './utils/setupMethod';

describe('SearchDecorator', () => {
  it('should decorate search result', () => {
    const editor = setup(createParagraph('Platzhalter')) as unknown as SearchReplaceEditor;
    const [paragraphTextNode, paragraphPath] = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const range = SearchDecorator(paragraphTextNode, paragraphPath, 'Platz');
    expect(range[0].highlight).eq(true);
  });
});
