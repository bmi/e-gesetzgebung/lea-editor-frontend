// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { Plugin, InitialEditorDocument, PluginProps } from '../../../../src/lea-editor/plugin-core';
import { DocumentDTO, DocumentStateType, DocumentType } from '../../../../src/api';
import { DocumentPrefaceProps } from '../../../../src/interfaces';
import { PluginFactory } from '../../../../src/lea-editor';
import { DocumentPrefacePlugin } from '../../../../src/lea-editor/plugins/document-preface';
import * as RegelungstextPlugin from '../../../../src/lea-editor/plugins/regelungstext/stammform';
import * as PreambelPlugin from '../../../../src/lea-editor/plugins/regelungstext/preambel';
import * as RegelungstextMantelformPlugin from '../../../../src/lea-editor/plugins/regelungstext/mantelform';
import * as BegruendungPlugin from '../../../../src/lea-editor/plugins/begruendung';
import * as VorblattPlugin from '../../../../src/lea-editor/plugins/vorblatt';
import * as AuthorialNotePlugin from '../../../../src/lea-editor/plugins/authorialNote';
import * as CommentPlugin from '../../../../src/lea-editor/plugins/comment';
import * as AuthorialNotePagePlugin from '../../../../src/lea-editor/Editor/AuthorialNote/plugins/AuthorialPage';
import * as CommentOptionsPlugin from '../../../../src/lea-editor/plugins/comment/comment-options';
import * as CopyAndPastePlugin from '../../../../src/lea-editor/plugins/copyAndPaste';
import * as ContentEditablePlugin from '../../../../src/lea-editor/plugins/contentEditable';
import { MarkerPlugin } from '../../../../src/lea-editor/plugin-core/generic-plugins/marker';
import { CellStructurePlugin } from '../../../../src/lea-editor/plugins/synopsis/cellStructure/CellStructurePlugin';
import { SynopsisOptionsPlugin } from '../../../../src/lea-editor/plugins/synopsis/synopsis-options';
import { TabellePlugin } from '../../../../src/lea-editor/plugin-core/generic-plugins/tabelle';
import { SearchReplacePlugin } from '../../../../src/lea-editor/plugins/searchAndReplace';
import { ProtectedWhiteSpacePlugin } from '../../../../src/lea-editor/plugin-core/generic-plugins/protected-whitespace';
// import { TrimSpacesPlugin } from '../../../../src/lea-editor/plugin-core/generic-plugins/trim-spaces/TrimSpacesPlugin';
import { DynAenderungsVergleichPlugin } from '../../../../src/lea-editor/plugins/dynAenderungsVergleich';
import { UndoRedoPlugin } from '../../../../src/lea-editor/plugins/undoRedo';
import { HdrAssistentPlugin } from '../../../../src/lea-editor/plugins/hdrAssistent/HdrAssistentPlugin';
import { SatzZaehlungPlugin } from '../../../../src/lea-editor/plugins/satzZaehlungPlugin/SatzZaehlungPlugin';

const prefaceProps: DocumentPrefaceProps = {
  title: '',
  shortTitle: '',
  proponent: '',
  abbreviation: '',
};
const prefaceSynopsisProps: DocumentPrefaceProps = {
  title: '',
  shortTitle: '',
  proponent: '',
  abbreviation: '',
};
export const getDocumentDTOFromType = (type: DocumentType): DocumentDTO => {
  return {
    id: 'string',
    title: 'string',
    content: 'string',
    type: type,
    state: DocumentStateType.DRAFT,
    version: 'string',
    createdBy: {
      gid: 'string',
      name: 'string',
      email: 'string',
    },
    updatedBy: {
      gid: 'string',
      name: 'string',
      email: 'string',
    },
    documentPermissions: {
      hasRead: true,
      hasWrite: true,
    },
    commentPermissions: {
      hasRead: true,
      hasWrite: false,
    },
    createdAt: 'string',
    updatedAt: 'string',
  };
};

const idGenerator = () => '123';

const pluginProps: PluginProps = {
  editorDocument: { ...InitialEditorDocument },
  idGenerator,
  isDynAenderungsvergleich: false,
};
const pluginPropsSynopsis: PluginProps = {
  editorDocument: { ...InitialEditorDocument, index: 1 },
  idGenerator,
  isDynAenderungsvergleich: false,
};

const documentPlugins: Plugin[] = [
  new CommentPlugin.CommentPlugin(pluginProps),
  new SatzZaehlungPlugin(pluginProps),
  new AuthorialNotePlugin.AuthorialNotePlugin(pluginProps),
  new CommentOptionsPlugin.CommentOptionsPlugin(pluginProps),
  new CopyAndPastePlugin.CopyAndPastePlugin(pluginProps),
  new ContentEditablePlugin.ContentEditablePlugin(pluginProps),
  new CellStructurePlugin(pluginProps),
  new SynopsisOptionsPlugin(pluginProps),
  new SearchReplacePlugin(pluginProps),
  new ProtectedWhiteSpacePlugin(pluginProps),
  new DynAenderungsVergleichPlugin(pluginProps),
];

const synopsisDocumentPlugins: Plugin[] = [
  new CommentPlugin.CommentPlugin(pluginPropsSynopsis),
  new AuthorialNotePlugin.AuthorialNotePlugin(pluginPropsSynopsis),
  new SatzZaehlungPlugin(pluginPropsSynopsis),
  new CopyAndPastePlugin.CopyAndPastePlugin(pluginPropsSynopsis),
  new ContentEditablePlugin.ContentEditablePlugin(pluginPropsSynopsis),
  new CellStructurePlugin(pluginPropsSynopsis),
  new SynopsisOptionsPlugin(pluginPropsSynopsis),
  new SearchReplacePlugin(pluginPropsSynopsis),
  new ProtectedWhiteSpacePlugin(pluginPropsSynopsis),
  // new TrimSpacesPlugin(pluginPropsSynopsis),
];

const regelungstextStammGesetz = [
  new UndoRedoPlugin(pluginProps),
  new RegelungstextPlugin.JuristischerAbsatzPlugin(pluginProps),
  new RegelungstextPlugin.ListeMitUntergliederungPlugin(pluginProps),
  new PreambelPlugin.GesetzPraeambelPlugin(pluginProps),
  new RegelungstextPlugin.TableOfContentsPlugin(pluginProps),
  new RegelungstextPlugin.HeaderPlugin(pluginProps),
  new HdrAssistentPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  new DocumentPrefacePlugin({ ...prefaceProps, type: DocumentType.REGELUNGSTEXT_STAMMGESETZ }, pluginProps),
  ...documentPlugins,
];
const regelungstextMantelGesetz = [
  new UndoRedoPlugin(pluginProps),
  new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(pluginProps),
  new PreambelPlugin.GesetzPraeambelPlugin(pluginProps),
  new RegelungstextMantelformPlugin.TableOfContentsPlugin(pluginProps),
  new RegelungstextMantelformPlugin.HeaderPlugin(pluginProps),
  new HdrAssistentPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  new DocumentPrefacePlugin({ ...prefaceProps, type: DocumentType.REGELUNGSTEXT_MANTELGESETZ }, pluginProps),
  ...documentPlugins,
];
const regelungstextStammVerordnung = [
  new UndoRedoPlugin(pluginProps),
  new RegelungstextPlugin.JuristischerAbsatzPlugin(pluginProps),
  new RegelungstextPlugin.ListeMitUntergliederungPlugin(pluginProps),
  new PreambelPlugin.VerordnungPraeambelPlugin(pluginProps),
  new RegelungstextPlugin.TableOfContentsPlugin(pluginProps),
  new RegelungstextPlugin.HeaderPlugin(pluginProps),
  new HdrAssistentPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  new DocumentPrefacePlugin({ ...prefaceProps, type: DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG }, pluginProps),
  ...documentPlugins,
];
const regelungstextMantelVerordnung = [
  new UndoRedoPlugin(pluginProps),
  new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(pluginProps),
  new PreambelPlugin.VerordnungPraeambelPlugin(pluginProps),
  new RegelungstextMantelformPlugin.TableOfContentsPlugin(pluginProps),
  new RegelungstextMantelformPlugin.HeaderPlugin(pluginProps),
  new HdrAssistentPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  new DocumentPrefacePlugin({ ...prefaceProps, type: DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG }, pluginProps),
  ...documentPlugins,
];

const begruendung = [
  new UndoRedoPlugin(pluginProps),
  new BegruendungPlugin.BegruendungAbsatzPlugin(pluginProps),
  new BegruendungPlugin.BegruendungAbsatzListePlugin(pluginProps),
  new BegruendungPlugin.TableOfContentsPlugin(pluginProps),
  new BegruendungPlugin.HeaderPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  ...documentPlugins,
];

const vorblatt = [
  new UndoRedoPlugin(pluginProps),
  new VorblattPlugin.VorblattAbsatzPlugin(pluginProps),
  new VorblattPlugin.TableOfContentsPlugin(pluginProps),
  new VorblattPlugin.HeaderPlugin(pluginProps),
  new MarkerPlugin(pluginProps),
  new TabellePlugin(pluginProps),
  new DocumentPrefacePlugin({ ...prefaceProps, type: DocumentType.VORBLATT_STAMMGESETZ }, pluginProps),
  ...documentPlugins,
];

const regelungstextStammGesetzSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.JuristischerAbsatzPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.ListeMitUntergliederungPlugin(pluginPropsSynopsis),
  new PreambelPlugin.GesetzPraeambelPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  new DocumentPrefacePlugin(
    { ...prefaceSynopsisProps, type: DocumentType.REGELUNGSTEXT_STAMMGESETZ },
    pluginPropsSynopsis,
  ),
  ...synopsisDocumentPlugins,
];

const regelungstextMantelGesetzSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(pluginPropsSynopsis),
  new PreambelPlugin.GesetzPraeambelPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  new DocumentPrefacePlugin(
    { ...prefaceSynopsisProps, type: DocumentType.REGELUNGSTEXT_MANTELGESETZ },
    pluginPropsSynopsis,
  ),
  ...synopsisDocumentPlugins,
];
const regelungstextStammVerordnungSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.JuristischerAbsatzPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.ListeMitUntergliederungPlugin(pluginPropsSynopsis),
  new PreambelPlugin.VerordnungPraeambelPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new RegelungstextPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  new DocumentPrefacePlugin(
    { ...prefaceSynopsisProps, type: DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG },
    pluginPropsSynopsis,
  ),
  ...synopsisDocumentPlugins,
];

const regelungstextMantelVerordnungSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.AenderungsbefehlePlugin(pluginPropsSynopsis),
  new PreambelPlugin.VerordnungPraeambelPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new RegelungstextMantelformPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  new DocumentPrefacePlugin(
    { ...prefaceSynopsisProps, type: DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG },
    pluginPropsSynopsis,
  ),
  ...synopsisDocumentPlugins,
];

const begruendungSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new BegruendungPlugin.BegruendungAbsatzPlugin(pluginPropsSynopsis),
  new BegruendungPlugin.BegruendungAbsatzListePlugin(pluginPropsSynopsis),
  new BegruendungPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new BegruendungPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  ...synopsisDocumentPlugins,
];

const vorblattSynopsis = [
  new UndoRedoPlugin(pluginPropsSynopsis),
  new VorblattPlugin.VorblattAbsatzPlugin(pluginPropsSynopsis),
  new VorblattPlugin.TableOfContentsPlugin(pluginPropsSynopsis),
  new VorblattPlugin.HeaderPlugin(pluginPropsSynopsis),
  new MarkerPlugin(pluginPropsSynopsis),
  new TabellePlugin(pluginPropsSynopsis),
  new DocumentPrefacePlugin({ ...prefaceSynopsisProps, type: DocumentType.VORBLATT_STAMMGESETZ }, pluginPropsSynopsis),
  ...synopsisDocumentPlugins,
];

const authProps = { ...pluginProps, isAuthorialNotePlugin: true };

const authPlugin = [
  new AuthorialNotePagePlugin.AuthorialPagePlugin(authProps),
  new CopyAndPastePlugin.CopyAndPastePlugin(authProps),
  new ContentEditablePlugin.ContentEditablePlugin(authProps),
  new SearchReplacePlugin(authProps),
  new ProtectedWhiteSpacePlugin(authProps),
  // new TrimSpacesPlugin(authProps),
];

describe('PluginFactory', () => {
  describe('Get SynopsisPage plugin', () => {
    it('no document', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(null, pluginPropsSynopsis);
      expect(JSON.stringify(factory)).be.equal('[]');
    });

    it('get regelungstext Stammgesetz', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextStammGesetzSynopsis));
    });

    it('get regelungstext Mantelgesetz', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_MANTELGESETZ),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextMantelGesetzSynopsis));
    });

    it('get regelungstext Stammverordnung', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextStammVerordnungSynopsis));
    });

    it('get regelungstext Mantelverordnung', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextMantelVerordnungSynopsis));
    });

    it('get begruendung', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.BEGRUENDUNG_STAMMGESETZ),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(begruendungSynopsis));
    });

    it('get vorblatt', () => {
      const factory = PluginFactory.getSynopsisPagePlugins(
        getDocumentDTOFromType(DocumentType.VORBLATT_STAMMGESETZ),
        pluginPropsSynopsis,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(vorblattSynopsis));
    });
  });

  describe('Get Normal plugin', () => {
    it('get regelungstext Stammgesetz', () => {
      const factory = PluginFactory.getPlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextStammGesetz));
    });

    it('get regelungstext Mantelgesetz', () => {
      const factory = PluginFactory.getPlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_MANTELGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextMantelGesetz));
    });

    it('get regelungstext Sammverordnung', () => {
      const factory = PluginFactory.getPlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextStammVerordnung));
    });

    it('get regelungstext Mantelverordnung', () => {
      const factory = PluginFactory.getPlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(regelungstextMantelVerordnung));
    });

    it('get begruendung', () => {
      const factory = PluginFactory.getPlugins(
        getDocumentDTOFromType(DocumentType.BEGRUENDUNG_STAMMGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(begruendung));
    });

    it('get vorblatt', () => {
      const factory = PluginFactory.getPlugins(getDocumentDTOFromType(DocumentType.VORBLATT_STAMMGESETZ), pluginProps);
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(vorblatt));
    });
  });

  describe('Get AuthorialNote Page plugin', () => {
    it('get regelungstext', () => {
      const factory = PluginFactory.getAuthorialNotePagePlugins(
        getDocumentDTOFromType(DocumentType.REGELUNGSTEXT_STAMMGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(authPlugin));
    });

    it('get begruendung', () => {
      const factory = PluginFactory.getAuthorialNotePagePlugins(
        getDocumentDTOFromType(DocumentType.BEGRUENDUNG_STAMMGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(authPlugin));
    });

    it('get vorblatt', () => {
      const factory = PluginFactory.getAuthorialNotePagePlugins(
        getDocumentDTOFromType(DocumentType.VORBLATT_STAMMGESETZ),
        pluginProps,
      );
      expect(JSON.stringify(factory)).be.equal(JSON.stringify(authPlugin));
    });
  });
});
