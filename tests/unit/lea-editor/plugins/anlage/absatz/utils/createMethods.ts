// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { ANLAGE_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/anlage/AnlageStructure';

export const createmainBody = (texts: string[]) => {
  const PElements = texts.map((text) => createElementOfType(ANLAGE_STRUCTURE.P, [createTextWrapper(text)]));
  const mainBody = createElementOfType(ANLAGE_STRUCTURE.MAIN_BODY, PElements);
  return mainBody;
};
