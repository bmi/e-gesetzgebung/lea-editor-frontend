// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { InitialEditorDocument, PluginRegistry } from '../../../../../../../src/lea-editor/plugin-core';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import {
  BegruendungAbsatzListeCommands as Commands,
  BegruendungAbsatzPlugin,
} from '../../../../../../../src/lea-editor/plugins/begruendung';
import { Element } from '../../../../../../../src/interfaces';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { Editor } from 'slate';
describe('BegruendungAbsatzListeCommands', () => {
  const setupContainer = () => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();

    mainEditor.editor.insertNode(createContainerWithTwoTextblock());

    return mainEditor;
  };

  it('Should insert a new list -> list created', () => {
    const mainEditor = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    Commands.insertListe(mainEditor.editor);
    const listElement = Editor.node(mainEditor.editor, [0, 0, 1]);
    expect((listElement[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.UNORDERED_LIST);
  });

  it('Should insert a new list item -> list item created', () => {
    const mainEditor = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    Commands.insertListe(mainEditor.editor);
    Editor.insertText(mainEditor.editor, 'Lorem ipsum');
    Commands.insertListItem(mainEditor.editor);
    const listItemElement = Editor.node(mainEditor.editor, [0, 0, 1, 1]);
    const listElement = Editor.node(mainEditor.editor, [0, 0, 1]);
    expect((listItemElement[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.LIST_ITEM);
    expect((listElement[0] as Element).children.length).equals(2);
  });

  it('Should delete an exsisting List -> list deleted', () => {
    const mainEditor = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    Commands.insertListe(mainEditor.editor);
    Commands.deleteList(mainEditor.editor);
    const listElement = Editor.node(mainEditor.editor, [0, 0, 1]);
    expect((listElement[0] as Element).type).not.to.equal(BEGRUENDUNG_STRUCTURE.UNORDERED_LIST);
  });

  it('Should merge list items in an exsisting list -> list should have only one list item', () => {
    const mainEditor = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    Commands.insertListe(mainEditor.editor);
    Editor.insertText(mainEditor.editor, 'Lorem ipsum');
    Commands.insertListItem(mainEditor.editor);
    const listBefore = Editor.node(mainEditor.editor, [0, 0, 1]);
    expect((listBefore[0] as Element).children.length).equals(2);
    Commands.mergeListItems(mainEditor.editor);
    const listAfter = Editor.node(mainEditor.editor, [0, 0, 1]);
    expect((listAfter[0] as Element).children.length).equals(1);
  });

  it('Should end list after two list items -> after the list element there is a p element', () => {
    const mainEditor = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    Commands.insertListe(mainEditor.editor);
    Editor.insertText(mainEditor.editor, 'Lorem ipsum');
    Commands.insertListItem(mainEditor.editor);
    Editor.insertText(mainEditor.editor, 'Lorem ipsum');
    Commands.insertListItem(mainEditor.editor);
    Commands.endList(mainEditor.editor);
    const listElement = Editor.node(mainEditor.editor, [0, 0, 1]);
    const newElement = Editor.node(mainEditor.editor, [0, 0, 2]);
    expect((newElement[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.P);
    expect((listElement[0] as Element).type).equals(BEGRUENDUNG_STRUCTURE.UNORDERED_LIST);
  });
});

const createContainerWithTwoTextblock = (): Element => {
  const p1 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const p2 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
