// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { InitialEditorDocument, PluginRegistry } from '../../../../../../src/lea-editor/plugin-core';
import {
  createElementOfType,
  createTextWrapper,
  isLastPosition,
} from '../../../../../../src/lea-editor/plugin-core/utils';
import { BegruendungAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/begruendung';
import { Element } from '../../../../../../src/interfaces';
import { BEGRUENDUNG_STRUCTURE } from '../../../../../../src/lea-editor/plugins/begruendung/BegruendungStructure';
import { BaseText, Editor, NodeEntry } from 'slate';
describe('BegruendungAbsatz', () => {
  const setupContainer = (multipleTextblock?: boolean) => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    if (multipleTextblock) {
      mainEditor.editor.insertNode(createContainerWithTwoTextblock());
    } else {
      mainEditor.editor.insertNode(createContainer());
    }
    return [registry, mainEditor] as const;
  };

  it('Should create BegruendungAbsatzPlugin editor -> editor is not undefined', () => {
    const registry = new PluginRegistry([
      new BegruendungAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
    ]);
    const mainEditor = registry.register();
    expect(mainEditor).exist;
  });

  it('Check correct node hierarchy -> isEditable return true', () => {
    const [registry, mainEditor] = setupContainer();
    mainEditor.editor.insertNode(createContainer());
    const isEditable = registry.isEditable(mainEditor.editor);
    expect(isEditable).to.be.true;
  });

  it('Send character "a" to editor -> keyDown return true', () => {
    const [, mainEditor] = setupContainer();
    const plugin = new BegruendungAbsatzPlugin({
      editorDocument: { ...InitialEditorDocument },
      isDynAenderungsvergleich: false,
    });
    mainEditor.editor.insertNode(createContainer());
    const aKey = new KeyboardEvent('keydown', {
      code: 'KeyA',
      key: 'KeyA',
    });
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 0, 0, 0, 0], offset: 0 },
    };
    const isKeyAllowed = plugin.keyDown(mainEditor.editor, aKey);
    expect(isKeyAllowed).to.be.true;
  });

  it('Send "Enter" to editor -> keyDown return true', () => {
    const [, mainEditor] = setupContainer();
    const plugin = new BegruendungAbsatzPlugin({
      editorDocument: { ...InitialEditorDocument },
      isDynAenderungsvergleich: false,
    });
    mainEditor.editor.insertNode(createContainer());
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 0, 0, 0], offset: 3 },
      focus: { path: [0, 0, 0, 0, 0], offset: 3 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    const isKeyAllowed = plugin.keyDown(mainEditor.editor, enterKey);
    expect(isKeyAllowed).to.be.true;
  });

  it('Delete character using Backspace at the end of text -> character deleted', () => {
    const [, mainEditor] = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 0, 0, 0], offset: 11 },
      focus: { path: [0, 0, 0, 0, 0], offset: 11 },
    };
    mainEditor.editor.deleteBackward('character');

    const [nodes] = Editor.nodes(mainEditor.editor, { mode: 'lowest' });
    const [node] = nodes as NodeEntry<BaseText>;
    expect(node.text).be.equal('Lorem ipsu');
  });

  it('Delete character using Delete at beginning of text -> character deleted', () => {
    const [, mainEditor] = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 0, 0, 0], offset: 0 },
      focus: { path: [0, 0, 0, 0, 0], offset: 0 },
    };
    mainEditor.editor.deleteForward('character');

    const [nodes] = Editor.nodes(mainEditor.editor, { mode: 'lowest' });
    const [node] = nodes as NodeEntry<BaseText>;
    expect(node.text).be.equal('orem ipsum');
  });

  it('Insert new Absatz using Enter at end of text -> insert new Absatz', () => {
    const [, mainEditor] = setupContainer();
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 0, 0, 0], offset: 11 },
      focus: { path: [0, 0, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    const plugin = new BegruendungAbsatzPlugin({
      editorDocument: { ...InitialEditorDocument },
      isDynAenderungsvergleich: false,
    });
    plugin.keyDown(mainEditor.editor, enterKey);
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    const isNewAbsatz = isLastPosition(mainEditor.editor);
    expect(isNewAbsatz).to.be.true;
  });

  it('Delete Absatz using Backspace at begin of 2nd text -> merge Absaetze', () => {
    const [, mainEditor] = setupContainer(true);
    mainEditor.editor.selection = {
      anchor: { path: [0, 0, 1, 0, 0], offset: 0 },
      focus: { path: [0, 0, 1, 0, 0], offset: 0 },
    };
    mainEditor.editor.deleteBackward('character');

    const [nodes] = Editor.nodes(mainEditor.editor, { mode: 'lowest' });
    const [node] = nodes as NodeEntry<BaseText>;
    expect(node.text).be.equal('Lorem ipsum');
  });
});

const createContainer = (): Element => {
  const p = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};

const createContainerWithTwoTextblock = (): Element => {
  const p1 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('Lorem ipsum')]);
  const p2 = createElementOfType(BEGRUENDUNG_STRUCTURE.P, [createTextWrapper('')]);
  const content = createElementOfType(BEGRUENDUNG_STRUCTURE.CONTENT, [p1, p2]);
  return createElementOfType(BEGRUENDUNG_STRUCTURE.HCONTAINER, [content]);
};
