// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { STRUCTURE_ELEMENTS } from '../../../../../../../src/lea-editor/plugin-core/utils/StructureElements';
import { VORBLATT_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/vorblatt';

const createTextElement = (text: string): Element => {
  return createElementOfType(VORBLATT_STRUCTURE.PARAGRAPH, [createTextWrapper(text)]);
};

const createHcontainer = (numText: string, headingText: string) => {
  const num = createElementOfType(VORBLATT_STRUCTURE.NUM, [createTextWrapper(numText)]);
  const heading = createElementOfType(VORBLATT_STRUCTURE.HEADING, [createTextWrapper(headingText)]);
  const paragraphs = [createTextElement('first Paragraph'), createTextElement('second Paragraph')];
  const content = createElementOfType(VORBLATT_STRUCTURE.CONTENT, paragraphs);
  return createElementOfType(VORBLATT_STRUCTURE.HCONTAINER, [num, heading, content]);
};

export const createMainBody = () => {
  const hContainers = [
    createHcontainer('A.', 'Heading 1'),
    createHcontainer('B.', 'Heading 2'),
    createHcontainer('C.', 'Heading 3'),
  ];
  return createElementOfType(STRUCTURE_ELEMENTS.MAIN_BODY, hContainers);
};
