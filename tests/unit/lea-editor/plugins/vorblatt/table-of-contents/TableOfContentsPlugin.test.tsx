// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { TableOfContentsCommands } from '../../../../../../src/lea-editor/plugins/vorblatt/table-of-contents/TableOfContentsCommands';
import { setupVorblattAbsatzPlugin } from '../vorblatt-absatz/utils/setupMehods';
import { createMainBody } from './utils/createMethods';

describe('Vorblatt TableOfContents', () => {
  it('press backwards at end of p-element --> last character is removed', () => {
    const editor = setupVorblattAbsatzPlugin(createMainBody());
    const tableData = TableOfContentsCommands.getContentTableData(editor);
    expect(tableData[1].title).be.equal('B. Heading 2');
    expect(tableData[2].key).be.equal('0-2');
  });
});
