// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../src/interfaces';
import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { VORBLATT_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/vorblatt';

const createTextElement = (text: string): Element => {
  return createElementOfType(VORBLATT_STRUCTURE.PARAGRAPH, [createTextWrapper(text)]);
};

export const createHcontainer = (texts: string[]) => {
  const num = createElementOfType(VORBLATT_STRUCTURE.NUM, [createTextWrapper('A')]);
  const heading = createElementOfType(VORBLATT_STRUCTURE.HEADING, [createTextWrapper('Heading 1')]);
  const paragraphs = texts.map((text: string) => {
    return createTextElement(text);
  });
  const content = createElementOfType(VORBLATT_STRUCTURE.CONTENT, paragraphs);
  return createElementOfType(VORBLATT_STRUCTURE.HCONTAINER, [num, heading, content]);
};
