// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Element } from '../../../../../../../src/interfaces';
import { InitialEditorDocument, PluginRegistry } from '../../../../../../../src/lea-editor/plugin-core';
import { VorblattAbsatzPlugin } from '../../../../../../../src/lea-editor/plugins/vorblatt';

export const setupVorblattAbsatzPlugin = (element: Element) => {
  const registry = new PluginRegistry([
    new VorblattAbsatzPlugin({ editorDocument: { ...InitialEditorDocument }, isDynAenderungsvergleich: false }),
  ]);
  const mainEditor = registry.register();
  mainEditor.editor.insertNode(element);
  return mainEditor.editor;
};
