// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { VorblattAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/vorblatt';
import { setupVorblattAbsatzPlugin } from './utils/setupMehods';
import { createHcontainer } from './utils/createMethods';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../src/lea-editor/plugin-core';

const plugin = new VorblattAbsatzPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});

describe('VorblattAbsatz', () => {
  it('press backwards at end of p-element --> last character is removed', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 2, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 2, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const paragraph = Editor.node(editor, [0, 2, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Lorem ipsu');
  });

  it('press delete at beginning of p-element --> first character is removed', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 2, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 2, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const paragraph = Editor.node(editor, [0, 2, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('orem ipsum');
  });

  it('press enter at end of paragraph --> insert new paragraph', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 2, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 2, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const secondParagraph = Editor.node(editor, [0, 2, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(secondParagraph[0].text).be.equal('');
  });

  it('press enter at middle of paragraph --> no action', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 2, 0, 0, 0], offset: 5 },
      anchor: { path: [0, 2, 0, 0, 0], offset: 5 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const secondParagraph = Node.has(editor, [0, 2, 1, 0, 0]);
    expect(secondParagraph).be.false;
  });

  it('press enter in empty paragraph --> no action', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum', '']));
    editor.selection = {
      focus: { path: [0, 2, 1, 0, 0], offset: 5 },
      anchor: { path: [0, 2, 1, 0, 0], offset: 5 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const secondParagraph = Node.has(editor, [0, 2, 2, 0, 0]);
    expect(secondParagraph).be.false;
  });

  it('press backspace in empty paragraph --> remove paragraph', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum', '']));
    editor.selection = {
      focus: { path: [0, 2, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 2, 1, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const paragraphText = Node.has(editor, [0, 2, 1, 0, 0]);
    expect(paragraphText).be.false;
  });

  it('press backspace at beginning of non-empty paragraph --> no action', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      focus: { path: [0, 2, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 2, 1, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const paragraphText = Editor.node(editor, [0, 2, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraphText[0].text).be.equal('dolor sit');
  });

  it('press backspace at first and ampty paragraph --> no action', () => {
    const editor = setupVorblattAbsatzPlugin(createHcontainer(['']));
    editor.selection = {
      focus: { path: [0, 2, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 2, 0, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const paragraphText = Editor.node(editor, [0, 2, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraphText[0].text).be.equal('');
  });
});
