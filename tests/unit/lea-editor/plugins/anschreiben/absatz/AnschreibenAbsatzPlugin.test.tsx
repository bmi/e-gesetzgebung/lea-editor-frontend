// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { AnschreibenAbsatzPlugin } from '../../../../../../src/lea-editor/plugins/anschreiben';
import { setupAnschreibenAbsatzPlugin } from './utils/setupMehods';
import { createmainBody, createPreface } from './utils/createMethods';
import { BaseText, Editor, NodeEntry, Node } from 'slate';
import { InitialEditorDocument } from '../../../../../../src/lea-editor/plugin-core';

const plugin = new AnschreibenAbsatzPlugin({
  editorDocument: { ...InitialEditorDocument },
  isDynAenderungsvergleich: false,
});

describe('AnschreibenAbsatz', () => {
  it('press backwards at end of p-element --> last character is removed', () => {
    const editor = setupAnschreibenAbsatzPlugin(createmainBody(['Lorem ipsum', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 0, 0], offset: 11 },
    };
    editor.deleteBackward('character');
    const paragraph = Editor.node(editor, [0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Lorem ipsu');
  });

  it('press delete at beginning of p-element --> first character is removed', () => {
    const editor = setupAnschreibenAbsatzPlugin(createmainBody(['Lorem ipsum', 'Dolor sit']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 0, 0], offset: 0 },
    };
    editor.deleteForward('character');
    const paragraph = Editor.node(editor, [0, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('orem ipsum');
  });

  it('press backward at beginning of empty p-element --> merge with previous p element', () => {
    const editor = setupAnschreibenAbsatzPlugin(createmainBody(['Lorem ipsum', '']));
    editor.selection = {
      focus: { path: [0, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 1, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const firstParagraph = Editor.node(editor, [0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondParagraphExists = Node.has(editor, [0, 1, 0, 0]);
    expect(firstParagraph[0].text).be.equal('Lorem ipsum');
    expect(secondParagraphExists).be.false;
  });

  it('press enter at end of p-element --> create new P-Element', () => {
    const editor = setupAnschreibenAbsatzPlugin(createmainBody(['Lorem ipsum']));
    editor.selection = {
      focus: { path: [0, 0, 0, 0], offset: 11 },
      anchor: { path: [0, 0, 0, 0], offset: 11 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const firstParagraph = Editor.node(editor, [0, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondParagraph = Editor.node(editor, [0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(firstParagraph[0].text).be.equal('Lorem ipsum');
    expect(secondParagraph[0].text).be.equal('');
  });

  it('press backwards at end of role Element in Preface --> last character is removed', () => {
    const editor = setupAnschreibenAbsatzPlugin(createPreface('Lorem ipsum', 'Dolor sit', 'amet'));
    editor.selection = {
      focus: { path: [0, 0, 0, 1, 0, 0], offset: 9 },
      anchor: { path: [0, 0, 0, 1, 0, 0], offset: 9 },
    };
    editor.deleteBackward('character');
    const paragraph = Editor.node(editor, [0, 0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('Dolor si');
  });

  it('Press Backspace at beginning of empty location Lement in Preface --> No action', () => {
    const editor = setupAnschreibenAbsatzPlugin(createPreface('Lorem ipsum', '', 'amet'));
    editor.selection = {
      focus: { path: [0, 0, 0, 1, 0, 0], offset: 0 },
      anchor: { path: [0, 0, 0, 1, 0, 0], offset: 0 },
    };
    editor.deleteBackward('character');
    const paragraph = Editor.node(editor, [0, 0, 0, 1, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('');
  });

  it('Press Enter at end of location Element in Preface --> No action', () => {
    const editor = setupAnschreibenAbsatzPlugin(createPreface('Lorem ipsum', 'Dolor sit', 'amet'));
    editor.selection = {
      focus: { path: [0, 0, 0, 1, 0, 0], offset: 9 },
      anchor: { path: [0, 0, 0, 1, 0, 0], offset: 9 },
    };
    const enterKey = new KeyboardEvent('keydown', {
      code: 'Enter',
      key: 'Enter',
    });
    plugin.keyDown(editor, enterKey);
    const paragraph = Editor.node(editor, [0, 0, 0, 2, 0, 0]) as NodeEntry<BaseText>;
    expect(paragraph[0].text).be.equal('amet');
  });
});
