// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createElementOfType, createTextWrapper } from '../../../../../../../src/lea-editor/plugin-core/utils';
import { ANSCHREIBEN_STRUCTURE } from '../../../../../../../src/lea-editor/plugins/anschreiben/AnschreibenStructure';

export const createmainBody = (texts: string[]) => {
  const PElements = texts.map((text) => createElementOfType(ANSCHREIBEN_STRUCTURE.P, [createTextWrapper(text)]));
  const mainBody = createElementOfType(ANSCHREIBEN_STRUCTURE.MAIN_BODY, PElements);
  return mainBody;
};

export const createPreface = (locationText: string, roleText: string, dateText: string) => {
  const location = createElementOfType(ANSCHREIBEN_STRUCTURE.LOCATION, [createTextWrapper(locationText)]);
  const role = createElementOfType(ANSCHREIBEN_STRUCTURE.ROLE, [createTextWrapper(roleText)]);
  const date = createElementOfType(ANSCHREIBEN_STRUCTURE.DATE, [createTextWrapper(dateText)]);

  const PElememnt = createElementOfType(ANSCHREIBEN_STRUCTURE.P, [location, role, date]);
  const TBlock = createElementOfType(ANSCHREIBEN_STRUCTURE.TBLOCK, [PElememnt]);
  const Preface = createElementOfType(ANSCHREIBEN_STRUCTURE.PREFACE, [TBlock]);

  return Preface;
};
