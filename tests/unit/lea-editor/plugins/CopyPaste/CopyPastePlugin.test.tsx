// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { BaseText, Editor, NodeEntry } from 'slate';
import { CopyAndPastePlugin } from '../../../../../src/lea-editor/plugins/copyAndPaste';
import {
  createParagraph,
  createParagraphWithList,
} from '../regelungstext/stammform/juristischer-absatz/utils/createMethods';
import { setupCopyPastePlugin } from './utils/setupMehods';
import { pasteMock, clipboardMock } from './utils/clipboardMock';
import { InitialEditorDocument } from '../../../../../src/lea-editor/plugin-core';
import { DocumentType } from '../../../../../src/api';
import { getPlainTextOfSelection } from '../../../../../src/lea-editor/plugins/copyAndPaste/utils';

const plugin = new CopyAndPastePlugin({
  editorDocument: { ...InitialEditorDocument, documentType: DocumentType.REGELUNGSTEXT_STAMMGESETZ },
  isDynAenderungsvergleich: false,
});

const readOnlyPlugin = new CopyAndPastePlugin({
  editorDocument: {
    ...InitialEditorDocument,
    documentType: DocumentType.REGELUNGSTEXT_STAMMGESETZ,
    isReadonly: true,
  },
  isDynAenderungsvergleich: false,
});

describe('JuristischerAbsatz', () => {
  it('Paste string at beginning of text -> new string is appended at beginning of text', () => {
    const editor = setupCopyPastePlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 0 }, focus: { path: [0, 1, 0, 0, 0], offset: 9 } };
    const event = new Event('paste') as unknown as ClipboardEvent;
    event.preventDefault = () => {};
    event.stopPropagation = () => {};
    const text = getPlainTextOfSelection(editor, true, false);
    if (text) {
      event.clipboardData = { getData: pasteMock as (text: string) => string };
    }
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 0 }, focus: { path: [0, 1, 0, 0, 0], offset: 0 } };

    plugin.paste(editor, event);

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(text).eq('Lorem ips');
    expect(node[0].text).be.equal('PastedStringLorem ipsum');
  });

  it('Paste string in readonly document -> no action', () => {
    const editor = setupCopyPastePlugin(createParagraph('Lorem ipsum'));
    editor.selection = { anchor: { path: [0, 1, 0, 0, 0], offset: 0 }, focus: { path: [0, 1, 0, 0, 0], offset: 0 } };
    const event = new Event('paste') as unknown as ClipboardEvent;
    event.preventDefault = () => {};
    event.stopPropagation = () => {};
    event.clipboardData = { getData: pasteMock as (text: string) => string };
    readOnlyPlugin.paste(editor, event);

    const node = Editor.node(editor, [0, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(node[0].text).be.equal('Lorem ipsum');
  });

  it('Cut text of a single list point --> Expect selected text to be removed', () => {
    const editor = setupCopyPastePlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 1, 0, 0, 0], offset: 2 },
      focus: { path: [0, 1, 1, 1, 0, 0, 0], offset: 11 },
    };
    const event = new Event('cut') as unknown as ClipboardEvent;
    event.preventDefault = () => {};
    event.stopPropagation = () => {};
    global.navigator.clipboard = clipboardMock;
    plugin.cut(editor, event);
    const fisrtNode = Editor.node(editor, [0, 1, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    const secondNode = Editor.node(editor, [0, 1, 2, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(fisrtNode[0].text).be.equal('Lo');
    expect(secondNode[0].text).be.equal('dolor sit');
  });

  it('Cut in non-editable area --> No action', () => {
    const editor = setupCopyPastePlugin(createParagraphWithList('intro-text', ['Lorem ipsum', 'dolor sit']));
    editor.selection = {
      anchor: { path: [0, 1, 1, 0, 0, 0], offset: 0 },
      focus: { path: [0, 1, 1, 0, 0, 0], offset: 2 },
    };
    const event = new Event('cut') as unknown as ClipboardEvent;
    event.preventDefault = () => {};
    event.stopPropagation = () => {};
    global.navigator.clipboard = clipboardMock;
    plugin.cut(editor, event);
    const numOfFirstListPoint = Editor.node(editor, [0, 1, 1, 0, 0, 0]) as NodeEntry<BaseText>;
    expect(numOfFirstListPoint[0].text).be.equal('1.');
  });
});
