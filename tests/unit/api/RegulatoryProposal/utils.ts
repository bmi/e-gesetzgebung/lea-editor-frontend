// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FarbeType, VorhabenStatusType } from '@plateg/rest-api';
import {
  CompoundDocumentType,
  DocumentStateType,
  DocumentType,
  EntityType,
  HomepageCompoundDocumentDTO,
  HomepageRegulatoryProposalDTO,
  HomepageSingleDocumentDTO,
} from '../../../../src/api';

const document: HomepageSingleDocumentDTO = {
  entityType: EntityType.SINGLE_DOCUMENT,
  id: '047b259c-4e36-4e6d-93e3-17a098a1cc12',
  regulatoryProposalId: 'd2a889c1-696e-4e0b-a4b6-f28d25cd62dd',
  title: 'Testdokument Regelungstext',
  type: DocumentType.REGELUNGSTEXT_STAMMGESETZ,
  version: '1',
  state: DocumentStateType.DRAFT,
  updatedAt: '2022-09-29T14:10:50.673',
  documentPermissions: {
    hasRead: true,
    hasWrite: true,
  },
  commentPermissions: {
    hasRead: true,
    hasWrite: true,
  },
  updatedBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  createdAt: '',
  createdBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
};

const compoundDocument: HomepageCompoundDocumentDTO = {
  entityType: EntityType.COMPOUND_DOCUMENT,
  id: 'b6f95093-1a30-4330-bfab-04eb8edd986f',
  regulatoryProposalId: 'd2a889c1-696e-4e0b-a4b6-f28d25cd62dd',
  title: 'Test',
  type: CompoundDocumentType.STAMMGESETZ,
  state: DocumentStateType.DRAFT,
  version: '1',
  children: [document],
  documentPermissions: {
    hasRead: true,
    hasWrite: false,
  },
  commentPermissions: {
    hasRead: false,
    hasWrite: false,
  },
  regulatoryProposalState: VorhabenStatusType.Entwurf,
  isNewest: false,
  isPinned: false,
};
const existingRegulatoryProposal: HomepageRegulatoryProposalDTO = {
  entityType: EntityType.REGULATORY_PROPOSAL,
  id: 'd2a889c1-696e-4e0b-a4b6-f28d25cd62dd',
  abbreviation: 'Test',
  farbe: FarbeType.Blau,
  children: [compoundDocument],
};

export function fetchMock(input: RequestInfo | URL, init?: RequestInit | undefined) {
  let response: HomepageRegulatoryProposalDTO[] | HomepageRegulatoryProposalDTO | undefined;
  let statusCode: number;
  let isOk: boolean;
  let calledFunction: string;

  // get regulatory proposals
  if (init?.method === 'GET') {
    if (input === '/LEA/API/v1/dokumenteAusAbstimmungen') {
      response = [existingRegulatoryProposal];
      statusCode = 200;
      isOk = true;
      calledFunction = 'getById';
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  } else if (init?.method === 'POST') {
    if (input === '/LEA/API/v1/meineDokumente') {
      response = [existingRegulatoryProposal];
      statusCode = 200;
      isOk = true;
      calledFunction = 'create';
    } else if (input === '/LEA/API/v1/regulatoryProposal/d2a889c1-696e-4e0b-a4b6-f28d25cd62dd') {
      response = existingRegulatoryProposal;
      statusCode = 200;
      isOk = true;
      calledFunction = 'create';
    }
  }

  return new Promise((resolve) =>
    setTimeout(() => {
      resolve({
        status: statusCode,
        statusText: '',
        ok: isOk,
        calledFunction: calledFunction,
        json: () => {
          return new Promise((resolve) => {
            if (isOk) resolve(response ?? {});
            else resolve({});
          });
        },
      });
    }, 20),
  );
}
