// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import React, { useEffect } from 'react';
import { createRoot, Root } from 'react-dom/client';
import { act } from 'react';
import { RegulatoryProposalService } from '../../../../src/api';
import { fetchMock } from './utils';

const container = document.createElement('div') as HTMLElement;
container.id = 'root';
let root: Root;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

function TestComponent() {
  const [regulatoryProposalResponse, fetchRegulatoryProposals] = RegulatoryProposalService.getHomepageMeineDokumente();
  useEffect(() => {
    fetchRegulatoryProposals();
  }, []);
  if (regulatoryProposalResponse.isLoading) {
    return <div>loading</div>;
  } else if (regulatoryProposalResponse.hasError) {
    return <div>{'Regulatory Proposals not found'}</div>;
  } else {
    return (
      <div>
        {regulatoryProposalResponse.data ? regulatoryProposalResponse.data[0].children[0].children[0].title : ''}
      </div>
    );
  }
}

function TestComponentAbstimmungen() {
  const [regulatoryProposalResponse, fetchRegulatoryProposals] =
    RegulatoryProposalService.getHomepageDokumenteAusAbstimmungen();
  useEffect(() => {
    fetchRegulatoryProposals();
  }, []);
  if (regulatoryProposalResponse.isLoading) {
    return <div>loading</div>;
  } else if (regulatoryProposalResponse.hasError) {
    return <div>{'Regulatory Proposals not found'}</div>;
  } else {
    return (
      <div>
        {regulatoryProposalResponse.data ? regulatoryProposalResponse.data[0].children[0].children[0].title : ''}
      </div>
    );
  }
}

function TestComponentVersionHistory() {
  const [regulatoryProposalResponse, fetchRegulatoryProposals] = RegulatoryProposalService.getVersionHistory(
    'd2a889c1-696e-4e0b-a4b6-f28d25cd62dd',
  );
  useEffect(() => {
    fetchRegulatoryProposals();
  }, []);
  if (regulatoryProposalResponse.isLoading) {
    return <div>loading</div>;
  } else if (regulatoryProposalResponse.hasError) {
    return <div>{'Regulatory Proposals not found'}</div>;
  } else {
    return (
      <div>{regulatoryProposalResponse.data ? regulatoryProposalResponse.data.children[0].children[0].title : ''}</div>
    );
  }
}

describe('Test Regulatory Proposal Service', () => {
  before(() => {
    global.fetch = fetchMock as unknown as (
      input: RequestInfo | URL,
      init?: RequestInit | undefined,
    ) => Promise<Response>;
  });

  beforeEach(() => {
    // set up a DOM element as a render target
    document.body.appendChild(container);
    root = createRoot(container);
  });

  it('Fetch meine Dokumente --> return meine Dokumente', async () => {
    act(() => {
      root.render(<TestComponent />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Testdokument Regelungstext');
  });

  it('Fetch dokumente aus abstimmungen --> return dokumente aus Abstimmungen', async () => {
    act(() => {
      root.render(<TestComponentAbstimmungen />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Testdokument Regelungstext');
  });

  it('Fetch version history --> return Versionshistorie', async () => {
    act(() => {
      root.render(<TestComponentVersionHistory />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Testdokument Regelungstext');
  });

  afterEach(() => {
    // cleanup on exiting
    document.body.removeChild(container);
    container.remove();
    act(() => {
      root.unmount();
    });
  });

  after(() => {});
});
