// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentDTO, DocumentService, DocumentStateType, DocumentType } from '../../../../src/api/Document';
import { Api } from '../../../../src/api/Api';

const existingDocument: DocumentDTO = {
  id: '2bf75613-434e-4106-9fd0-b61b6925da8e', // Vorblatt Dokument
  title: 'Test Document',
  content: 'Initial content',
  type: DocumentType.VORBLATT_STAMMGESETZ,
  state: DocumentStateType.FINAL,
  version: '',
  createdBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  updatedBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  createdAt: '2022-09-29T14:10:50.673',
  updatedAt: '2022-09-29T14:10:50.673',
  documentPermissions: {
    hasRead: true,
    hasWrite: true,
  },
  commentPermissions: {
    hasRead: true,
    hasWrite: false,
  },
};

export function fetchMock(input: RequestInfo | URL, init?: RequestInit | undefined) {
  let response: Partial<DocumentDTO> | undefined;
  let statusCode: number;
  let isOk: boolean;

  // get Document by its Id
  if (init?.method === 'GET') {
    if (input === '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8e') {
      response = existingDocument;
      statusCode = 200;
      isOk = true;
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  } else if (init?.method === 'PATCH') {
    if (input === '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8e') {
      const content = (JSON.parse(init?.body as string) as Partial<DocumentDTO>).content;
      response = {
        ...existingDocument,
        content: Api.decodeString(content || ''),
      };
      statusCode = 200;
      isOk = true;
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  } else if (init?.method === 'POST') {
    const content = (JSON.parse(init?.body as string) as Partial<DocumentDTO>).content;
    if (input === '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8f/export' && content !== '') {
      response = {
        content: Api.decodeString(content || ''),
      };
      statusCode = 200;
      isOk = true;
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  }

  return new Promise((resolve) =>
    setTimeout(() => {
      resolve({
        status: statusCode,
        statusText: '',
        ok: isOk,
        json: () => {
          return new Promise((resolve) => {
            if (isOk) resolve(DocumentService.encodeDocumentDTO(response ?? {}));
            else resolve({});
          });
        },
        text: () => {
          return new Promise<string>((resolve) => {
            if (isOk) resolve(response?.content ?? '');
            else resolve('');
          });
        },
      });
    }, 20),
  );
}
