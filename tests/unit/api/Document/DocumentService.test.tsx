// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React, { useEffect } from 'react';
import { act } from 'react';
import { DocumentDTO, DocumentService } from '../../../../src/api/Document';
import { fetchMock } from './utils';
import { createRoot, Root } from 'react-dom/client';

const container = document.createElement('div');
container.id = 'root';
let root: Root;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

function TestComponent({ documentId }: { documentId: string }) {
  const [documentResponse, fetchDocument] = DocumentService.getById(documentId);
  useEffect(() => {
    fetchDocument();
  }, []);
  if (documentResponse.isLoading) {
    return <div>loading</div>;
  } else if (documentResponse.hasError) {
    return <div>{'Document not found'}</div>;
  } else {
    return <div>{documentResponse.data?.content}</div>;
  }
}

function TestComponentForUpdate({
  documentId,
  documentUpdate,
}: {
  documentId: string;
  documentUpdate: Partial<DocumentDTO>;
}) {
  const [documentResponse, updateDocument] = DocumentService.update(documentId, documentUpdate);
  useEffect(() => {
    updateDocument();
  }, []);
  if (documentResponse.isLoading) {
    return <div>loading</div>;
  } else if (documentResponse.hasError) {
    return <div>{'Document not found'}</div>;
  } else {
    return <div>{documentResponse.data?.content}</div>;
  }
}

function TestComponentForExport({ documentId, value }: { documentId: string; value: string }) {
  const [exportDocumentResponse, exportDocument] = DocumentService.export(documentId, { content: value });
  useEffect(() => {
    exportDocument();
  }, []);
  if (exportDocumentResponse.isLoading) {
    return <div>loading</div>;
  } else if (exportDocumentResponse.hasError) {
    return <div>{'Error: Empty content'}</div>;
  } else {
    return <div>{exportDocumentResponse.data}</div>;
  }
}

describe('Get documents', () => {
  before(() => {
    global.fetch = fetchMock as unknown as (
      input: RequestInfo | URL,
      init?: RequestInit | undefined,
    ) => Promise<Response>;
  });

  beforeEach(() => {
    // set up a DOM element as a render target
    document.body.appendChild(container);
    root = createRoot(container);
  });

  it('Fetching a document by its ID --> Check if createdAt date is correct', async () => {
    act(() => {
      root.render(<TestComponent documentId="2bf75613-434e-4106-9fd0-b61b6925da8e" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Initial content');
  });

  it('Fetching a document by its Id -- No corresponding document --> Api request returns error', async () => {
    act(() => {
      root.render(<TestComponent documentId="2bf75613-434e-4106-9fd0-b61b6925da8f" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Document not found');
  });

  it('Updating an existing document --> return updated document', async () => {
    const updatedDocument: Partial<DocumentDTO> = {
      id: '2bf75613-434e-4106-9fd0-b61b6925da8e',
      content: 'Updated content',
    };
    act(() => {
      root.render(
        <TestComponentForUpdate documentId="2bf75613-434e-4106-9fd0-b61b6925da8e" documentUpdate={updatedDocument} />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Updated content');
  });

  it('Exporting a document -- expext content of exported document', async () => {
    act(() => {
      root.render(
        <TestComponentForExport documentId="2bf75613-434e-4106-9fd0-b61b6925da8f" value={'Exported content'} />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Exported content');
  });

  it('Exporting a document with empty content --> expect API to return error', async () => {
    act(() => {
      root.render(<TestComponentForExport documentId="2bf75613-434e-4106-9fd0-b61b6925da8f" value={''} />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Error: Empty content');
  });

  afterEach(() => {
    // cleanup on exiting
    document.body.removeChild(container);
    container.remove();
    act(() => {
      root.unmount();
    });
  });
});
