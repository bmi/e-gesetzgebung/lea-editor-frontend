// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Api } from '../../../../src/api/Api/Api';
import { CommentResponseDTO, CreateCommentDTO, PatchCommentDTO } from '../../../../src/api/Comment';

const existingComment: CommentResponseDTO = {
  documentId: '2bf75613-434e-4106-9fd0-b61b6925da8e', // Vorblatt Dokument
  commentId: '50d1f75b-5563-4d56-8368-32dae92d3c86',
  content: 'VGVzdGtvbW1lbnRhcg==',
  anchor: {
    path: [0, 0, 0, 1],
    offset: 0,
  },
  focus: {
    path: [0, 0, 0, 1],
    offset: 10,
  },
  createdBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  updatedBy: {
    email: 'user1.rechtsetzungsreferent1@example.com',
    gid: 'someRandomId',
    name: 'User1 Rechtsetzungsreferent1',
  },
  createdAt: '2022-09-29T14:10:50.673',
  updatedAt: '2022-09-29T14:10:50.673',
  state: 'OPEN',
  replies: [],
  deleted: false,
};

export const newComment: CreateCommentDTO = {
  anchor: { elementGuid: 'abc', offset: 0 },
  focus: { elementGuid: 'abc', offset: 5 },
  commentId: '50d1f75b-5563-4d56-8368-32dae92d3c86',
  documentContent: '["GUID"="abc","children":["type":"lea:textWrapper", "children":["text":"Initial content"]]',
  content: 'Testkommentar',
};

export function fetchMock(input: RequestInfo | URL, init?: RequestInit | undefined) {
  let fetchedComments: CommentResponseDTO[] | undefined;
  let fetchedComment: CommentResponseDTO | undefined;
  let statusCode: number;
  let isOk: boolean;
  if (input === '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8e/comments' && init?.method === 'GET') {
    fetchedComments = [existingComment];
    statusCode = 200;
    isOk = true;
  } else if (
    input === '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8e/commentsNEW' &&
    init?.method === 'POST'
  ) {
    fetchedComment = existingComment;
    statusCode = 200;
    isOk = true;
  } else if (
    input ===
      '/LEA/API/v1/documents/2bf75613-434e-4106-9fd0-b61b6925da8e/comments/50d1f75b-5563-4d56-8368-32dae92d3c86' &&
    init?.method === 'PATCH'
  ) {
    const content = (JSON.parse(init?.body as string) as Partial<PatchCommentDTO>).content;
    fetchedComment = {
      ...existingComment,
      content: Api.decodeString(content || ''),
    };
    statusCode = 200;
    isOk = true;
  } else {
    fetchedComments = undefined;
    statusCode = 400;
    isOk = false;
  }
  return new Promise((resolve) =>
    setTimeout(() => {
      resolve({
        status: statusCode,
        statusText: '',
        ok: isOk,
        json: () => {
          return new Promise((resolve) => {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-call
            if (isOk) resolve(fetchedComments || fetchedComment);
            else resolve({});
          });
        },
      });
    }, 20),
  );
}
