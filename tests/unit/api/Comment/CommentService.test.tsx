// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React, { useEffect } from 'react';
import { createRoot, Root } from 'react-dom/client';
import { act } from 'react';
import { CommentService, PatchCommentDTO } from '../../../../src/api/Comment';
import { fetchMock, newComment } from './utils';

const container = document.createElement('div');
container.id = 'root';
let root: Root;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

function TestComponent({ documentId }: { documentId: string }) {
  const [commentResponse, fetchComments] = CommentService.getAll(documentId);
  useEffect(() => {
    fetchComments();
  }, []);
  if (commentResponse.isLoading) {
    return <div>loading</div>;
  } else if (commentResponse.hasError) {
    return <div>{'Comment not found'}</div>;
  } else {
    return <div>{commentResponse.data ? commentResponse.data[0].content : ''}</div>;
  }
}

function TestCreateComponent({ documentId }: { documentId: string }) {
  const [commentResponse, fetchComments] = CommentService.create(documentId, newComment);
  useEffect(() => {
    fetchComments();
  }, []);
  if (commentResponse.isLoading) {
    return <div>loading</div>;
  } else if (commentResponse.hasError) {
    return <div>{'Comment not created'}</div>;
  } else {
    return <div>{commentResponse.data?.content}</div>;
  }
}

function TestDeleteComponent({ documentId, commentId }: { documentId: string; commentId: string }) {
  const [commentResponse, deleteComments] = CommentService.deleteComment(documentId, [commentId], { content: '' });
  useEffect(() => {
    deleteComments();
  }, []);
  if (commentResponse.isLoading) {
    return <div>loading</div>;
  } else if (commentResponse.hasError) {
    return <div>{'Comment not deleted'}</div>;
  } else {
    return <div>Success</div>;
  }
}

function TestUpdateComponent({
  documentId,
  commentId,
  updatedComment,
}: {
  documentId: string;
  commentId: string;
  updatedComment: Partial<PatchCommentDTO>;
}) {
  const [commentResponse, fetchComments] = CommentService.update(documentId, commentId, updatedComment);
  useEffect(() => {
    fetchComments();
  }, []);
  if (commentResponse.isLoading) {
    return <div>loading</div>;
  } else if (commentResponse.hasError) {
    return <div>{'Comment not updated'}</div>;
  } else {
    return <div>{commentResponse.data?.content}</div>;
  }
}

describe('Test Comment Service', () => {
  before(() => {
    global.fetch = fetchMock as unknown as (
      input: RequestInfo | URL,
      init?: RequestInit | undefined,
    ) => Promise<Response>;
  });

  beforeEach(() => {
    // set up a DOM element as a render target
    document.body.appendChild(container);
    root = createRoot(container);
  });

  it('Fetch all existing comments --> return all comments', async () => {
    act(() => {
      root.render(<TestComponent documentId="2bf75613-434e-4106-9fd0-b61b6925da8e" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Testkommentar');
  });

  it('Fetch all existing comments with wrong document ID --> return error', async () => {
    act(() => {
      root.render(<TestComponent documentId="2bf75613-434e-4106-9fd0-b61b6925dfff" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Comment not found');
  });

  it('Create new comment --> return new comment', async () => {
    act(() => {
      root.render(<TestCreateComponent documentId="2bf75613-434e-4106-9fd0-b61b6925da8e" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Testkommentar');
  });

  it('Create new comment with wrong document ID --> return error', async () => {
    act(() => {
      root.render(<TestCreateComponent documentId="2bf75613-434e-4106-9fd0-b61b6925ffff" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Comment not created');
  });

  it('Delete comment with wrong document ID --> return error', async () => {
    act(() => {
      root.render(
        <TestDeleteComponent
          documentId="2bf75613-434e-4106-9fd0-b61b6925ffff"
          commentId="50d1f75b-5563-4d56-8368-32dae92d3c86"
        />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Comment not deleted');
  });

  it('Updating an existing comment --> return updated comment', async () => {
    const updatedComment: Partial<PatchCommentDTO> = {
      content: 'Updated comment',
    };
    act(() => {
      root.render(
        <TestUpdateComponent
          documentId="2bf75613-434e-4106-9fd0-b61b6925da8e"
          commentId="50d1f75b-5563-4d56-8368-32dae92d3c86"
          updatedComment={updatedComment}
        />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Updated comment');
  });

  it('Updating an existing comment with wrong document ID --> return error', async () => {
    const updatedComment: Partial<PatchCommentDTO> = {
      content: 'Updated comment',
    };
    act(() => {
      root.render(
        <TestUpdateComponent
          documentId="2bf75613-434e-4106-9fd0-b61b6925ffff"
          commentId="50d1f75b-5563-4d56-8368-32dae92d3c86"
          updatedComment={updatedComment}
        />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Comment not updated');
  });

  afterEach(() => {
    // cleanup on exiting
    document.body.removeChild(container);
    container.remove();
    act(() => {
      root.unmount();
    });
  });

  after(() => {});
});
