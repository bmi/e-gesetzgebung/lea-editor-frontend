// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { DocumentStateType, DocumentType } from '../../../../src/api/Document/Document';
import {
  CompoundDocumentDTO,
  CompoundDocumentService,
  CompoundDocumentTitleDTO,
  CompoundDocumentType,
} from '../../../../src/api/CompoundDocument';

const compundDocuments: CompoundDocumentDTO[] = [
  {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    title: 'Test Compound Document',
    proposition: {
      id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
      title: 'Beispiel Regelungsvorhaben',
      shortTitle: 'Bsp. RegV',
      abbreviation: 'BspRV',
      state: 'IN_BEARBEITUNG',
      proponent: {
        id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
        title: 'BMI',
        designationGenitive: 'Bundesministeriums des Innern, für Bau und Heimat',
        designationNominative: 'Bundesministeriums des Innern, für Bau und Heimat',
        active: true,
      },
      initiant: 'BUNDESTAG',
      type: 'GESETZ',
    },
    type: CompoundDocumentType.STAMMGESETZ,
    documents: [
      {
        id: '2bf75613-434e-4106-9fd0-b61b6925da8e',
        title: 'Test Document',
        content: 'Content of first document',
        type: DocumentType.VORBLATT_STAMMGESETZ,
        state: DocumentStateType.FINAL,
        documentPermissions: { hasRead: true, hasWrite: true },
        commentPermissions: { hasRead: true, hasWrite: true },
        version: '',
        createdBy: {
          email: 'user1.rechtsetzungsreferent1@example.com',
          gid: 'someRandomId',
          name: 'User1 Rechtsetzungsreferent1',
        },
        updatedBy: {
          email: 'user1.rechtsetzungsreferent1@example.com',
          gid: 'someRandomId',
          name: 'User1 Rechtsetzungsreferent1',
        },
        createdAt: '2022-09-29T14:10:50.673',
        updatedAt: '2022-09-29T14:10:50.673',
      },
    ],
    createdAt: '2022-09-29T14:10:50.673',
    updatedAt: '2022-09-29T14:10:50.673',
    version: '1',
  },
  {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa7', // andere id als bei erster Dokumentenmappe
    title: 'Test Compound Document',
    proposition: {
      id: '3fa85f64-5717-4562-b3fc-2c963f66afa7',
      title: 'Beispiel Regelungsvorhaben',
      shortTitle: 'Bsp. RegV',
      abbreviation: 'BspRV',
      state: 'IN_BEARBEITUNG',
      proponent: {
        id: '3fa85f64-5717-4562-b3fc-2c963f66afa7',
        title: 'BMI',
        designationGenitive: 'Bundesministeriums des Innern, für Bau und Heimat',
        designationNominative: 'Bundesministeriums des Innern, für Bau und Heimat',
        active: true,
      },
      initiant: 'BUNDESTAG',
      type: 'GESETZ',
    },
    type: CompoundDocumentType.STAMMGESETZ,
    documents: [
      {
        id: '2bf75613-434e-4106-9fd0-b61b6925da8f', // andere ID als beim ersten Dokument
        title: 'Test Document',
        content: 'Content of second document',
        type: DocumentType.VORBLATT_STAMMGESETZ,
        state: DocumentStateType.FINAL,
        documentPermissions: { hasRead: true, hasWrite: true },
        commentPermissions: { hasRead: true, hasWrite: true },
        version: '',
        createdBy: {
          email: 'user1.rechtsetzungsreferent1@example.com',
          gid: 'someRandomId',
          name: 'User1 Rechtsetzungsreferent1',
        },
        updatedBy: {
          email: 'user1.rechtsetzungsreferent1@example.com',
          gid: 'someRandomId',
          name: 'User1 Rechtsetzungsreferent1',
        },
        createdAt: '2022-09-29T14:10:50.673',
        updatedAt: '2022-09-29T14:10:50.673',
      },
    ],
    createdAt: '2022-09-29T14:10:50.673',
    updatedAt: '2022-09-29T14:10:50.673',
    version: '1',
  },
];

const compundDocumentTitles: CompoundDocumentTitleDTO[] = [
  {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    title: 'Test Compound Document',
    type: CompoundDocumentType.STAMMGESETZ,
    anzahlAnlagen: 0,
  },
  {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa7', // andere id als bei erster Dokumentenmappe
    title: 'Test Compound Document',
    type: CompoundDocumentType.STAMMGESETZ,
    anzahlAnlagen: 0,
  },
];

export function fetchMock(input: RequestInfo | URL, init?: RequestInit | undefined) {
  let response: Partial<CompoundDocumentDTO> | CompoundDocumentTitleDTO[] | undefined;
  let statusCode: number;
  let isOk: boolean;
  let calledFunction: 'getById' | 'getAll' | 'export' | 'importDocument' | 'createNewVersion' | 'None' = 'None';

  if (init?.method === 'GET') {
    if (input === '/LEA/API/v1/compounddocuments/3fa85f64-5717-4562-b3fc-2c963f66afa6') {
      response = compundDocuments[0];
      statusCode = 200;
      isOk = true;
      calledFunction = 'getById';
    } else if (input === '/LEA/API/v1/compounddocumenttitles') {
      response = compundDocumentTitles;
      statusCode = 200;
      isOk = true;
      calledFunction = 'getAll';
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  } else if (init?.method === 'POST') {
    if (input === '/LEA/API/v1/compounddocuments/3fa85f64-5717-4562-b3fc-2c963f66afa6/export') {
      response = compundDocuments[0];
      statusCode = 200;
      isOk = true;
      calledFunction = 'export';
    } else if (input === '/LEA/API/v1/compounddocuments/3fa85f64-5717-4562-b3fc-2c963f66afa6/documents/import') {
      response = compundDocuments[0];
      statusCode = 200;
      isOk = true;
      calledFunction = 'importDocument';
    } else if (
      input ===
      '/LEA/API/v1/compounddocuments/3fa85f64-5717-4562-b3fc-2c963f66afa6/version?copyId=2bf75613-434e-4106-9fd0-b61b6925da8e'
    ) {
      response = { ...compundDocuments[0], version: '2' };
      statusCode = 200;
      isOk = true;
      calledFunction = 'createNewVersion';
    }
  }

  return new Promise((resolve) =>
    setTimeout(() => {
      resolve({
        status: statusCode,
        statusText: '',
        ok: isOk,
        json: () => {
          return new Promise((resolve) => {
            if (isOk) {
              if (calledFunction === 'getById' || calledFunction === 'export')
                resolve(
                  CompoundDocumentService.encodeCompoundDocumentDTO(
                    (response as unknown as Partial<CompoundDocumentDTO>) ?? {},
                  ),
                );
              else if (calledFunction === 'getAll') {
                let resultEncoded: CompoundDocumentDTO[] = [];
                if ((response as CompoundDocumentDTO[]).length > 0) {
                  resultEncoded = (response as CompoundDocumentDTO[]).map(
                    (resultDTO) => CompoundDocumentService.encodeCompoundDocumentDTO(resultDTO) as CompoundDocumentDTO,
                  );
                }
                resolve(resultEncoded);
              } else if (calledFunction === 'importDocument' || calledFunction === 'createNewVersion') {
                resolve(response);
              }
            } else resolve({});
          });
        },
        blob: () => {
          return new Promise((resolve) => {
            if (isOk) resolve(new Blob());
            else resolve(undefined);
          });
        },
      });
    }, 20),
  );
}
