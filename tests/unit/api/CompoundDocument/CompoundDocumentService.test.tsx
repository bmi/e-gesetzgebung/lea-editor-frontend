// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React, { useEffect } from 'react';
import { act } from 'react';
import { CompoundDocumentService, ImportCompoundDocumentDTO } from '../../../../src/api/CompoundDocument';
import { fetchMock } from './utils';
import { createRoot, Root } from 'react-dom/client';
import { CompoundDocumentDTO } from '../../../../src/api';

const container = document.createElement('div');
container.id = 'root';
let root: Root;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

function TestComponentForGetAll() {
  const [CompoundDocumentResponse, fetchCoumpoundDocument] = CompoundDocumentService.getAllTitles();
  useEffect(() => {
    fetchCoumpoundDocument();
  }, []);
  if (CompoundDocumentResponse.isLoading) {
    return <div>loading</div>;
  } else if (CompoundDocumentResponse.hasError) {
    return <div>{'Compound document not found'}</div>;
  } else {
    return <div>{CompoundDocumentResponse.data ? CompoundDocumentResponse.data[0].id : undefined}</div>;
  }
}

function TestComponentForGetById({ compoundDocumentId }: { compoundDocumentId: string }) {
  const [CompoundDocumentResponse, fetchCoumpoundDocument] = CompoundDocumentService.getById(compoundDocumentId);
  useEffect(() => {
    fetchCoumpoundDocument();
  }, []);
  if (CompoundDocumentResponse.isLoading) {
    return <div>loading</div>;
  } else if (CompoundDocumentResponse.hasError) {
    return <div>{'Compound document not found'}</div>;
  } else {
    return <div>{CompoundDocumentResponse.data?.documents[0].content}</div>;
  }
}

function TestComponentForCreateBewVersion({
  compoundDocumentId,
  documentIdList,
  compoundDocument,
}: {
  compoundDocumentId: string;
  documentIdList: string[];
  compoundDocument: Partial<CompoundDocumentDTO>;
}) {
  const [newVersionResponse, createNewVersion] = CompoundDocumentService.createNewVersion(
    compoundDocumentId,
    documentIdList,
    compoundDocument,
  );
  useEffect(() => {
    createNewVersion();
  }, []);
  if (newVersionResponse.isLoading) {
    return <div>loading</div>;
  } else if (newVersionResponse.hasError) {
    return <div>{'Compound document not found'}</div>;
  } else {
    return <div>{newVersionResponse.data?.version}</div>;
  }
}

function TestComponentForZIPExport({ compoundDocumentId }: { compoundDocumentId: string }) {
  const [exportResponse, fetchExport] = CompoundDocumentService.exportCompoundDocument(compoundDocumentId);
  useEffect(() => {
    fetchExport();
  }, []);
  if (exportResponse.isLoading) {
    return <div>loading</div>;
  } else if (exportResponse.hasError) {
    return <div>{'Compound document not found'}</div>;
  } else {
    let displayedText = 'None';
    if (exportResponse.data?.size === 0) {
      displayedText = 'Blob';
    }
    return <div>{displayedText}</div>;
  }
}

function TestComponentForImport({
  compoundDocumentId,
  importedDocument,
}: {
  compoundDocumentId: string;
  importedDocument: ImportCompoundDocumentDTO;
}) {
  const [importResponse, fetchImport] = CompoundDocumentService.importDocument(importedDocument, compoundDocumentId);
  useEffect(() => {
    fetchImport();
  }, []);
  if (importResponse.isLoading) {
    return <div>loading</div>;
  } else if (importResponse.hasError) {
    return <div>{'Compound document not found'}</div>;
  } else {
    return <div>{importResponse.data?.documents[0].content}</div>;
  }
}

describe('Tests for Compound Document Service', () => {
  before(() => {
    global.fetch = fetchMock as unknown as (
      input: RequestInfo | URL,
      init?: RequestInit | undefined,
    ) => Promise<Response>;
  });

  beforeEach(() => {
    // set up a DOM element as a render target
    document.body.appendChild(container);
    root = createRoot(container);
  });

  it('get all Compound Documents and access content of its first document in first Compound Document --> Expect the content of that document', async () => {
    act(() => {
      root.render(<TestComponentForGetAll />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('3fa85f64-5717-4562-b3fc-2c963f66afa6');
  });

  it('get Compound document by ID and access content of its first document --> Expect the content of that document', async () => {
    act(() => {
      root.render(<TestComponentForGetById compoundDocumentId="3fa85f64-5717-4562-b3fc-2c963f66afa6" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Content of first document');
  });

  it('create new version of compound document --> Expect a response with a version incremented by 1', async () => {
    act(() => {
      root.render(
        <TestComponentForCreateBewVersion
          compoundDocumentId="3fa85f64-5717-4562-b3fc-2c963f66afa6"
          documentIdList={['2bf75613-434e-4106-9fd0-b61b6925da8e']}
          compoundDocument={{ title: 'Test Compound Document V2' }}
        />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('2');
  });

  it('Export Compound document --> Expect ZIP file', async () => {
    act(() => {
      root.render(<TestComponentForZIPExport compoundDocumentId="3fa85f64-5717-4562-b3fc-2c963f66afa6" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Blob');
  });

  it('Try to export non-existing Compound document --> Expect error', async () => {
    act(() => {
      root.render(<TestComponentForZIPExport compoundDocumentId="3fa85f64-5717-4562-b3fc-2c963f66afa0" />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Compound document not found');
  });

  it('Import document into a compound document --> Expect DTO of the compound document', async () => {
    const importDocument: ImportCompoundDocumentDTO = {
      title: 'Imported Compound Document',
      xmlContent: 'xml content',
    };

    act(() => {
      root.render(
        <TestComponentForImport
          compoundDocumentId="3fa85f64-5717-4562-b3fc-2c963f66afa6"
          importedDocument={importDocument}
        />,
      );
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Content of first document');
  });

  afterEach(() => {
    // cleanup on exiting
    document.body.removeChild(container);
    container.remove();
    act(() => {
      root.unmount();
    });
  });
});
