// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PropositionDTO } from '../../../../src/api';

const existingProposition: PropositionDTO = {
  id: 'd55be669-7043-48b9-b148-7974bd25c0f1',
  title: 'Test Proposition',
  abbreviation: 'Test',
  state: 'IN_BEARBEITUNG',
  initiant: 'BUNDESRAT',
  type: 'RECHTSVERORDNUNG',
  shortTitle: 'Test',
};

export function fetchMock(input: RequestInfo | URL, init?: RequestInit | undefined) {
  let response: PropositionDTO[] | undefined;
  let statusCode: number;
  let isOk: boolean;

  // get regulatory proposals
  if (init?.method === 'GET') {
    if (input === '/LEA/API/v1/propositions') {
      response = [existingProposition];
      statusCode = 200;
      isOk = true;
    } else {
      response = undefined;
      statusCode = 400;
      isOk = false;
    }
  }

  return new Promise((resolve) =>
    setTimeout(() => {
      resolve({
        status: statusCode,
        statusText: '',
        ok: isOk,
        json: () => {
          return new Promise((resolve) => {
            if (isOk) resolve(response ?? {});
            else resolve({});
          });
        },
      });
    }, 20),
  );
}
