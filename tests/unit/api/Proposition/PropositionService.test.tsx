// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import React, { useEffect } from 'react';
import { createRoot, Root } from 'react-dom/client';
import { act } from 'react';
import { PropositionService } from '../../../../src/api';
import { fetchMock } from './utils';

const container = document.createElement('div');
container.id = 'root';
let root: Root;

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms)) as unknown as Promise<void>;
}

function TestComponent() {
  const [propositionResponse, fetchPropositions] = PropositionService.getAll();
  useEffect(() => {
    fetchPropositions();
  }, []);
  if (propositionResponse.isLoading) {
    return <div>loading</div>;
  } else if (propositionResponse.hasError) {
    return <div>{'Propositions not found'}</div>;
  } else {
    return <div>{propositionResponse.data ? propositionResponse.data[0].title : ''}</div>;
  }
}

describe('Test Propositions Service', () => {
  before(() => {
    global.fetch = fetchMock as unknown as (
      input: RequestInfo | URL,
      init?: RequestInit | undefined,
    ) => Promise<Response>;
  });

  beforeEach(() => {
    // set up a DOM element as a render target
    document.body.appendChild(container);
    root = createRoot(container);
  });

  it('Fetch all existing propositions --> return all propositions', async () => {
    void act(() => {
      root.render(<TestComponent />);
    });
    expect(container.textContent).be.equal('loading');
    await act(() => sleep(40));
    expect(container.textContent).be.equal('Test Proposition');
  });

  afterEach(() => {
    // cleanup on exiting
    document.body.removeChild(container);
    container.remove();
    void act(() => {
      root.unmount();
    });
  });

  after(() => {});
});
