// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { assert } from 'chai';
import { AppInfoReducer } from '../../../../src/components/App/AppInfoSlice';

describe('App', () => {
  it('AppReducer', function () {
    assert.isOk(AppInfoReducer);
  });
});
